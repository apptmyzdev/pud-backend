<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html ">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

  <link rel="icon" href="favicon.ico" type="image/x-icon">

   <!--      <script src="js/rtms/jquery.min.js"></script>
	<script type="text/javascript" src="js/rtms/bootstrap.js"></script>
 -->

    <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> 
 <!--    <link rel="stylesheet" href="font-awesome/css/font-awesome.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap Core Css -->
    <!-- <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- <link rel="stylesheet" href="css/rtms/bootstrap.css"> -->

    <!-- Waves Effect Css -->
    <link href="css/rtms/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="css/rtms/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="css/rtms/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/rtms/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/rtms/all-themes.css" rel="stylesheet" />
    
        <!-- Jquery Core Js -->
  <!--   <script src="jquery/jquery.min.js"></script> -->
  
  <link rel="stylesheet" href="css/dataTables.bootstrap.css">
  
   <link rel="stylesheet" href="css/rtmsAdmin.css">
   
   <link href="css/rtms/select2.css" rel="stylesheet" />
   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  
  <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAV7PDXnJCm8kw1fz63sttvLj3Qall0tEw"  async defer></script>
    
    <script type="text/javascript" src="js/jquery.js"></script>
	<!--  <script src="js/jquery.blockui.js"></script> -->
	<!-- <script type="text/javascript" src="js/jquery-ui.js"></script> -->
	<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>

<script type="text/javascript">
var flag=0;var userLocation;
var markersToFitBound=[];
    $(document).ready(function(){
    	
	    //Get veh locations. 
	    getVehicleLocation();
	    
	    //Click of Play/Pause button 
	    $('#play').on('click',function(){
	            prevvalue=parseInt(document.getElementById('seek-bar').value);
	            positionIndex=prevvalue;
		       debugger
		        if( $('#play').text().trim()=="Play"){
		        	$('#play').text('Pause');
		        	drawPath();
		        }
		        else{
		        	$('#play').text('Play');
		        	clearTimeout(setTimeoutId);
		        	return;
		        }
	        });
    });//doc
    
    
    var setTimeoutId ;
    function drawPath(){
    	debugger
    	
    	//set the seekbar move by 1 step
    	document.getElementById('seek-bar').stepUp(1);
    	
    	//Set location time.
    	//$('#locUpdatedTime').text(userLocation[parseInt(document.getElementById('seek-bar').value)].deviceTimestamp);
    	
    	
    	//Animate marker.
    	animateMarker();
    	
    	//setTimeout for drawPath method for marker animation.
    	setTimeoutId =  setTimeout(drawPath,1000);
    }
    
    //capture index of location for which the marker is being animated.
	var locationAsSource=0;
    
	function animateMarker(){
		debugger
		if (locationAsSource < userLocation.length) {
			 var result = [ userLocation[locationAsSource].latitude, userLocation[locationAsSource].longitude];
			 
			 transition(result);
			 locationAsSource++;
			 debugger
		}
		else if (locationAsSource==userLocation.length){
			//alert("Reached Destination");
			markerA.setVisible(false);
			clearTimeout(setTimeoutId);
		}
		debugger
	}

	
  var numDeltas = 100;
  var delay = 40; //milliseconds
  var i = 0;
  var deltaLat;
  var deltaLng;
  
function transition(result){
   debugger
	i = 0;
    deltaLat = (result[0] - position[0])/numDeltas;
    deltaLng = (result[1] - position[1])/numDeltas;
    moveMarker(position);
    debugger
}

function moveMarker(){
	debugger
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng = new google.maps.LatLng(position[0], position[1]);
    markerA.setPosition(latlng);
    if(i!=numDeltas){
        i++;
        setTimeout(moveMarker, delay);
    }
    debugger
}


//respond to manual changes in seekbar
function seekAnimation(){
	alert(parseInt(document.getElementById('seek-bar').value));
	$('#locUpdatedTime').text(userLocation[parseInt(document.getElementById('seek-bar').value)].deviceTimestamp);
}	

// get veh locs.
var userLocation;var position;
function getVehicleLocation(){
	  debugger
		   $.ajax({
		         url : 'getDeviceLocation?userId=572',contentType:"application/json", type : 'get', clearForm : true,
				 beforeSend : function(xhr){
				/* 	 $.blockUI({ message : "Please wait..." });  return true; */
			    },
			     success : function(response) {
			    	 debugger
			                 if (response.status) {
			                	 debugger
			                	 if(response.data!=null && response.data.length>0){
			                		 debugger
			                		 locationAsSource=0;
			                		 userLocation = response.data;
			                		  $('#seek-bar').prop('min',1);
			                		  $('#seek-bar').prop('max',response.data.length);
			                		  
			                		  $('#startTime').text(response.data[0].deviceTimestamp);
			                		  $('#endTime').text(response.data[response.data.length-1].deviceTimestamp);
			                		  
			                		   //Create a position array which always contains first location lat and lng. Position arrary is used for the marker animation.
			             			   position=[userLocation[0].latitude,userLocation[0].longitude ];
			                		   
			             			   //draw path
			             		       initVehiclePathMap();
			                		 }
			                	 else{
			                		 alert(response.message)
			                	 }
			                 }
			               /*   $.unblockUI(); */
			            },
				 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
			  });
}

var mapForAnim;
var infoContentForAnimatedMarker='';
var infow;
var markerA;
function initVehiclePathMap(){
		 debugger
		 locationList = userLocation;
	     
		 //Create a center for the map.
		 mapForAnim = new google.maps.Map(document.getElementById('ma_DivId'), {
	     center: {lat: locationList[0].latitude, lng:locationList[0].longitude},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		 mapForAnim.setCenter(new google.maps.LatLng(locationList[0].latitude, locationList[0].longitude));
		 
		 //Create the marker for the first location.
		  markerA =  new google.maps.Marker({
	          position : {lat: locationList[0].latitude, lng:locationList[0].longitude},
	         map : mapForAnim,
	      /*    icon : "images/markerWithVeh.jpg", */
	         //icon : "images/sgv.svg"
	   		 
	      });
		  markerA.setMap( mapForAnim ); 
		  
		 //Create a path on the 
		 showOnMap(locationList);
		 
		 //moveToLocation
		 moveToLocation(locationList[0].latitude, locationList[0].longitude);
	    
		 //To show all locs - avoid moving of pan.
		 var bounds = new google.maps.LatLngBounds();
		 $.each( locationList, function( key, value ) {
        	 debugger
        	 if(key==0 || key == locationList.length-1){
        		 //Create a marker.
           	    marker =  new google.maps.Marker({
	           	     position :{lat:value.latitude, lng:value.longitude},
	           	     map : mapForAnim,
	           	    icon : "images/blueMarker.png"
           	    }); 
           	    marker.setMap( mapForAnim );
        	 }
        	 else{
        		 //Create a marker.
           	    marker =  new google.maps.Marker({
	           	     position :{lat:value.latitude, lng:value.longitude},
           	     });
        	 }
        	  bounds.extend(marker.getPosition());
        	  mapForAnim.fitBounds(bounds);
         });
		 debugger
	}
	
function moveToLocation(lat, lng){
    var center = new google.maps.LatLng(lat, lng);
    mapForAnim.panTo(center);
}
	
function showOnMap(locationList){
	debugger
	var polyCoords=[];
	// for some reason each () is not working so using for loop. chage it after fixing issue with each ().
	for (var i = 0; i < locationList.length; i++) {
		//debugger
		if(locationList.length ==1){
			triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude}];
		}
		else if(i==0){
			triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude},{lat : locationList[i+1].latitude, lng : locationList[i+1].longitude}];
		}
		else{
			triangleCoords = [{lat: locationList[i-1].latitude, lng: locationList[i-1].longitude},{lat : locationList[i].latitude, lng : locationList[i].longitude}];
			polyCoords.push(triangleCoords);
		}
		mapForAnim.setCenter(new google.maps.LatLng(locationList[i].latitude, locationList[i].longitude));
		mapForAnim.setZoom(17); 
		drawPolyline(triangleCoords,i);
	}
	
	debugger
}

function drawPolyline(triangleCoords){
	 debugger
	  var bermudaTriangle = new google.maps.Polygon({
	         paths: triangleCoords,
	         strokeColor: '#FF0000',
	         strokeOpacity: 0.8,
	         strokeWeight: 3
	       });
	       bermudaTriangle.setMap(mapForAnim);
	       debugger
}

	
	
    </script>
    
</head>
<body>
 <div class="container-fluid">
    <br>
	<!-- <div class="w3-light-grey">
		<div id="myBar" class="w3-container w3-green" style="height:24px;width:20%"></div>
	</div>
	<p id="demo">20%</p>
	<button class="w3-button w3-green" onclick="move()">Start</button>  
	<button class="w3-button w3-green" onclick="stop()">Stop</button> -->
	 <input   type="range"  id="seek-bar" step="1" value="1" onchange="seekAnimation();">
	 <br>
	 <div class="row">
		 <div  class="col-lg-4">
		 <p id="startTime" align="left"></p>
		 </div>
		  <div  class="col-lg-4">
		  <p id="locUpdatedTime" align="center"></p>
		 </div>
		  <div  class="col-lg-4">
		 <p id="endTime" align="right"></p>
		 </div>
	 </div>
	  
	 
	  
	   <button style="font-size:18px" id="play" >Play</button>
	 <!--  <button style="font-size:18px" id="play" id="drawpathBtn" onclick="initVehiclePathMap()">Draw Path</button> -->
 <br><br>
	<div id="ma_DivId"   style="width:100%; height:460px;"></div>
 </div>

  






    <!-- Select Plugin Js -->
    <!-- <script src="js/rtms/bootstrap-select.js"></script> -->

    <!-- Slimscroll Plugin Js -->
    <script src="js/rtms/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="js/rtms/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="js/rtms/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="js/rtms/raphael.min.js"></script>
    <script src="js/rtms/morris.js"></script>

<!-- 
	<script type="text/javascript" src="js/highcharts.js"></script>
	 <script type="text/javascript" src="js/highcharts-more.js"></script>
	 <script type="text/javascript" src="js/rtmscharts.js"></script> -->



  <!--   <script src="js/rtms/admin.js"></script> -->
     <!--  <script src="js/rtms/index.js"></script> -->
      <script src="js/rtms/demo.js"></script>
       <script src="js/rtms/tooltips-popovers.js"></script>
     <!--    <script src="js/radmin.js"></script> -->
           <script src="js/commons.js"></script>
      
      <script type="text/javascript"
		src="js/datatable1.10.16/jquery.dataTables.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
<!--        <script src="js/rtms/select2.js"></script> -->

    <!-- ChartJs -->
    <!--  <script src="chartjs/Chart.bundle.js"></script>

   <script src="flot-charts/jquery.flot.js"></script>
    <script src="flot-charts/jquery.flot.resize.js"></script>
    <script src="flot-charts/jquery.flot.pie.js"></script>
    <script src="flot-charts/jquery.flot.categories.js"></script>
    <script src="flot-charts/jquery.flot.time.js"></script>

    Sparkline Chart Plugin Js
    <script src="jquery-sparkline/jquery.sparkline.js"></script> -->

    <!-- Custom Js -->
   <!--  <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script> -->

    <!-- Demo Js -->
  <!--   <script src="js/demo.js"></script>
    <script src="js/pages/tooltips-popovers.js"></script> -->


</body>
</html>