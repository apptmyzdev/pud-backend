var airFlightTb = null;
var airportTb = null;
var contentTb = null;
var dangerouscodeTb = null;
var pincodeTb = null;

$(document).ready(function(){
	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
		$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
		});

/*------------airFlight starts---------------*/
	$("#airFlightLi").click(function(){
		//console.log("in airflight_info");
		$.ajax({
			url : "pullAirFlightWeightMaster",
			method : "post",
			dataType : "json",
			success : function(Response)
			{
				//console.log(Response.msg);
				if(airFlightTb==null)
				{
					airFlightTb = $("#airflight_table").DataTable({
						columns : [
							{ data : 'vendorId'},
							{ data : 'vendorCode'},
							{ data : 'flightId'},
							{ data : 'flightDesc'},
							{ data : 'wtAllowed'},
						 ],
						data : Response.data,
						retrieve : true,
						fixedHeader : {
							header : true,
							footer : false
						},
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                fnAdjustColumnSizing:true,
		                "lengthMenu" : [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
				}
				else
				{
					airFlightTb.clear().draw();
					airFlightTb.rows.add(Response.data); // Add new data
					airFlightTb.columns.adjust().draw(); 
				} 
//				airFlightTb.fnAdjustColumnSizing();

			}
			
		});
		
	});
    /*------------airFlight ends---------------*/
	
	/*------------airport starts---------------*/
	$("#airportLi").click(function(){
		//console.log("in airflight_info");
		$.ajax({
			url : "pullAirportCodesMaster",
			method : "post",
			dataType : "json",
			success : function(Response)
			{
				//console.log(Response.msg);
				if(airportTb==null)
				{
					airportTb = $("#airport_table").DataTable({
						columns:[
							{data : 'airportId'},
							{data : 'airportCode'},
							{data : 'airportDescription'}
								
						],
						data : Response.data,
						retrieve : true,
						fixedHeader : {
							header : true,
							footer : false
						},
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					airportTb.clear().draw();
					airportTb.rows.add(Response.data); // Add new data
					airportTb.columns.adjust().draw(); 
				} 
				airportTb.columns.adjust().draw();
			}
			
		});
		
	});
	/*------------airport ends---------------*/
	
	/*------------content starts---------------*/
	$("#contentLi").click(function(){
		$.ajax({
			url : "pullContentMaster",
			method : "post",
			dataType : "json",
			success : function(Response)
			{
				if(contentTb==null)
					{
					contentTb = $("#content_table").DataTable({
						columns :[
							{ data : "dangerousId"},
							{ data : "dangerousDescription"}
						],
						data : Response.data,
						retrieve : true,
						fixedHeader : {
							header : true,
							footer : false
						}, 
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					});
					}
				else
					{
					contentTb.clear().draw();
					contentTb.rows.add(Response.data);
					contentTb.columns.adjust().draw();
					}
				contentTb.columns.adjust();
			}
		});
	});
	/*------------content ends---------------*/
	
	/*------------dangerouscode starts---------------*/
	$("#dangerouscodeLi").click(function(){
		console.log("in");
		$.ajax({
			url : "pullDangerousCodesMaster",
			method : "post",
			dataType : "json",
			success : function(Response){
				if(dangerouscodeTb==null)
					{
					dangerouscodeTb = $("#dangerouscode_table").DataTable({
						columns : [
							{ data : "dangeorusId"},
							{ data : "dangeorusCode"},
							{ data : "dangeorusDesc"}
						],
						data : Response.data,
						retrieve : true,
						fixedHeader : {
							header : true,
							footer : false
						}, 
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
				
					});
					}
				else
					{
					dangerouscodeTb.clear().draw();
					dangerouscodeTb.rows.add(Response.data);
					dangerouscodeTb.columns.adjust().draw();
					}
				dangerouscodeTb.columns.adjust();
			}
		});
	});
	/*------------dangerouscode ends---------------*/
	
	/*------------pincode starts---------------*/
	$("#pincodeLi").click(function(){
		console.log("in");
		$.ajax({
			url : "getPincodeMasterDataForPortal",
			method : "post",
			dataType : "json",
			success : function(Response){
				if(pincodeTb==null)
					{
					pincodeTb = $("#pincode_table").DataTable({
						columns : [
							{ data : "pinCode"},
							{ data : "isAirServicable"},
							{ data : "isAirServicableDel"},
							{ data : "servicable"},
							{ data : "branchCode"},
							{ data : "locationName"},
							{ data : "active"},
							{ data : "areaId"},
							{ data : "pincodeType"},
							{ data : "serviceType"},
							{ data : "version"}
						],
						columnDefs : [
					        { targets : [1],
					          render : function (data, type, row) {
					             return data == '1' ? 'Y' : 'N'
					          }  
					        },
					        {
					        	targets : [2],
					        	render : function (data, type, row)
					        	{
					        		return data == '1' ? 'Y' : 'N'
					        	}
					        }
					   ],
						data : Response.data,
						retrieve : true,
						fixedHeader : {
							header : true,
							footer : false
						}, 
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
				
					});
					}
				else
					{
					pincodeTb.clear().draw();
					pincodeTb.rows.add(Response.data);
					pincodeTb.columns.adjust().draw();
					}
			}
		});
	});
	/*------------pincode ends---------------*/
	

	
    /*---------------airflight_update starts--------*/
	$("#airflight_update").click(function(){
		console.log("airflight update")
		$.ajax({
			url : "downloadAirFlightWeightMaster",
			method : "post",
			dataType : "json",
			success : function(Response)
			{
				if(Response.status)
					{
					$("#airFlightLi").trigger("click");
					}
			}

		});
	});
    /*----------------airflight_update ends-----------*/
	
	 /*---------------dangerouscode_update starts--------*/
	$("#dangerouscode_update").click(function(){
	//	console.log("dangerouscode update")
		$.ajax({
			url : "downloadDangerousCodesMaster",
			method : "post",
			dataType : "json",
			success : function(Response)
			{
				if(Response.status)
				{
				$("#dangerouscodeLi").trigger("click");
				}

			}

		});
	});
    /*----------------dangerouscode_update ends-----------*/
	
	/*----------------content_update starts-----------*/
	$("#content_update").click(function(){
			$.ajax({
				url : "saveContentMaster",
				method : "post",
				dataType : "json",
				success : function(Response)
				{
					if(Response.status)
					{
					$("#contentLi").trigger("click");
					}

				}

			});
		});
	    /*----------------content_update ends-----------*/
	
	/*----------------airport_update starts-----------*/
	$("#airport_update").click(function(){
			$.ajax({
				url : "downloadAirportCodesMaster",
				method : "post",
				dataType : "json",
				success : function(Response)
				{
					if(Response.status)
					{
					$("#airportLi").trigger("click");
					}

				}

			});
		});
	    /*----------------airport_update ends-----------*/
	
	/*----------------pincode_update starts-----------*/
	$("#pincode_update").click(function(){
			$.ajax({
				url : "pincodeupdate",
				method : "post",
				dataType : "json",
				success : function(Response)
				{
					if(Response.status)
					{
					$("#pincodeLi").trigger("click");
					}

				}

			});
		});
	    /*----------------pincode_update ends-----------*/
});