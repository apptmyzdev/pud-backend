/**
 * 
 */



	var CONSTANTS_AJAX_TIMEOUT = 10000;
	var CONSTANTS_PUSH_APP_URL = "PUSH_APP_URL";
	var CONSTANTS_LOCK = "LOCK";
	var CONSTANTS_SCREAM = "SCREAM";
	var CONSTANTS_WIPE =  "WIPE";
	var CONSTANTS_APP_LIST = "APPS_LIST";
	var CONSTANTS_APP_UNINSTALL = "APP_UNINSTALL";
	
	
var userCurrentLocation ;
var geofenceMap;
var markersCreated=[],circlesCreated=[];
var markerForNewPointer;
var circle;finalRadius=20;
 
var loggedInUserLocation ;



$(document).ready(function(){
	
	 $("#loctionsHistoryOfDeviceMapModal").on("shown.bs.modal", function () {
         google.maps.event.trigger(loctionsHistoryOfDeviceMap, "resize");
     });
	
	$("#loggedInUserId").html(sessionStorage.getItem("userId"));
	
	
	$( "#signoutBtn" ).on('click',function() {
		debugger
		location.href="rtmsLogin.html";
		debugger
	});
	
	
	
	$( "#assignGeofence_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').show();
		$('#geofenceUpdates_section').hide();
		$('#geofenceCommands_section').hide();
		$('#scVehicles_section').hide();
		$('#dashboard_section').hide();
		$('#vehiclePath_section').hide();
		
		getAllDeviceDetails();
	});
	
	
	$( "#home_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').hide();
		$('#geofenceUpdates_section').hide();
		$('#geofenceCommands_section').hide();
		$('#scVehicles_section').hide();
		$('#dashboard_section').show();
		$('#vehiclePath_section').hide();
		
		getGeofenceUpdates();
	});
	
	
	$( "#geofenceUpdates_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').hide();
		$('#geofenceUpdates_section').show();
		$('#geofenceCommands_section').hide();
		$('#scVehicles_section').hide();
		$('#dashboard_section').hide();
		$('#vehiclePath_section').hide();
		
		getGeofenceUpdates();
	});
	
	$( "#geofenceCommands_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').hide();
		$('#geofenceUpdates_section').hide();
		$('#geofenceCommands_section').show();
		$('#scVehicles_section').hide();
		$('#dashboard_section').hide();
		$('#vehiclePath_section').hide();
		
		
		 //get all users with device id.
		 getAllUsersWithDeviceDetails();
		  
	});
	
	$( "#sVehicles_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').hide();
		$('#geofenceUpdates_section').hide();
		$('#geofenceCommands_section').hide();
		$('#scVehicles_section').show();
		$('#dashboard_section').hide();
		$('#vehiclePath_section').hide();
		
		  //Get the Sc codes and show them in dropdown.
		  getSCMasterData();
		 
		  //init map
		  initMap();
		  
	});
	
	$( "#vehiclePath_li" ).on('click',function() {
		debugger
		$('#assignGeofence_section').hide();
		$('#geofenceUpdates_section').hide();
		$('#geofenceCommands_section').hide();
		$('#scVehicles_section').hide();
		$('#dashboard_section').hide();
		$('#vehiclePath_section').show();
		
		// Get vehicles
		getVehicles();
		
		getLocation();
		
		loadMap(vehiclePath_DivId,loggedInUserLocation);
		
	});
	
	
	
	$("#geofenceModal").on("shown.bs.modal", function () {
        google.maps.event.trigger(geofenceMap, "resize");
    });
	
	$( "#geofenceRadiusId" ).keyup(function() {
		removeAllCircles();
		drawRadius(markerForNewPointer, $('#geofenceRadiusId').val());
    });
	
	$( "#saveGeofence_BtnId" ).on('click',function() {
		debugger
		if(validateGeofenceFormfields()){
			//Ask confirmaion.
			$('#confirmGeofenceName').text($('#geofenceNameId').val().trim());
			$('#confirmGeofenceLat').text($('#geofenceLatitudeId').val().trim());
			$('#confirmGeofenceLng').text($('#geofenceLangitudeId').val().trim());
			$('#confirmGeofenceRadius').text(finalRadius);

			$('#geofenceConfirmationModal').modal('show');
		}
    });
	
	$( "#confirmGeofence_BtnId" ).on('click',function() {
		debugger
			 var obj={
					    "name":$('#geofenceNameId').val().trim(),
						"latitude":$('#geofenceLatitudeId').val().trim(),
					    "longitude":$('#geofenceLangitudeId').val().trim(),
					    "radius":finalRadius
			         }
			 debugger
			 $.ajax({
				   url:"manageGeofence", dataType : 'json',type : 'post', data:JSON.stringify(obj),contentType: "application/json",
				   beforeSend : function(xhr){ /*$.blockUI({ message : "Please wait..." });  return true;*/ },
					success:function(response){
						if(response.status){
							//clse geofence creation n confirmtion modal and show success modal.
							$('#confirmGeofenceName').text('');
							$('#confirmGeofenceLat').text('');
							$('#confirmGeofenceLng').text('');
							$('#confirmGeofenceRadius').text('');
							$('#geofenceConfirmationModal').modal('hide');
							
							$('#geofenceNameId').val('');
							$('#geofenceLatitudeId').val('');
						    $('#geofenceLangitudeId').val('');
						    $('#geofenceRadiusId').val('');
							$('#geofenceModal').modal('hide');
							
							$('#responseSuccessMessage').text(response.message);
							$('#requestSuccessModal').modal('show');
						}
						else{
							$('#responseFailMessage').text(response.message);
							$('#requestFailModal').modal('show');
						}
						/*$.unblockUI();*/
					     },
			          error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		         });
    });
	
    //Click of Play/Pause button 
    $('#play').on('click',function(){
            prevvalue=parseInt(document.getElementById('seek-bar').value);
            positionIndex=prevvalue;
	       debugger
	        if( $('#play').text().trim()=="Play"){
	        	$('#play').text('Pause');
	        	drawPath();
	        }
	        else{
	        	$('#play').text('Play');
	        	clearTimeout(setTimeoutId);
	        	return;
	        }
	      // alert(alertMsg);
        });
	
	
});

function validateGeofenceFormfields(){
	debugger
	//Nme
 	if(stringIsEmpty($('#geofenceNameId').val())){
 		alert("Please provide Name");
		return false;
 	}
 	else if(stringConatinsOnlySpaces($('#geofenceNameId').val())){
 		alert("Please provide valid Name");
		return false;
 	}
	
	//lat
	if(stringIsEmpty($('#geofenceLatitudeId').val())){
 		alert("Please provide Latitude");
		return false;
 	}
	
	//lng
	if(stringIsEmpty($('#geofenceLangitudeId').val())){
 		alert("Please provide Longitude");
		return false;
 	}
	
	//Rad
	if(finalRadius<=0){
		alert("Please provide valid Radius");
		return false;
	}
	
	return true;
}


function geofence(){
	initMap_Geofence(userCurrentLocation);
}
function initMap_Geofence(userCurrentLocation){
	 debugger// get the user current loc.
	 //getLocation();
    //Create a center for the map.
	 geofenceMap = new google.maps.Map(document.getElementById('geofenceMap_DivId'), {
		 center: {lat:17.444377, lng:78.481153},// mandatory other wise map will not dispaly anything.
    zoom: 10
    });
	 geofenceMap.setCenter(new google.maps.LatLng( {lat:17.444377, lng:78.481153}));
	 
	 
	 
	 // Create the search box and link it to the UI element.
     var input = document.getElementById('pac-input');//$('#pac-input');
     var searchBox = new google.maps.places.SearchBox(input);
     geofenceMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

     // Bias the SearchBox results towards current map's viewport.
     geofenceMap.addListener('bounds_changed', function() {
       searchBox.setBounds(geofenceMap.getBounds());
     });
	 
     // Listen for the event fired when the user selects a prediction and retrieve
     // more details for that place.
     searchBox.addListener('places_changed', function() {
       var places = searchBox.getPlaces();

       if (places.length == 0) {
         return;
       }

       // For each place, get the icon, name and location.
       var bounds = new google.maps.LatLngBounds();
       places.forEach(function(place) {
         if (!place.geometry) {
           console.log("Returned place contains no geometry");
           return;
         }

         if (place.geometry.viewport) {
           // Only geocodes have viewport.
           bounds.union(place.geometry.viewport);
         } else {
           bounds.extend(place.geometry.location);
         }
       });
       geofenceMap.fitBounds(bounds);
     });
     
     // place marker on map on click event.
     google.maps.event.addListener(geofenceMap, 'click', function(event) {
         placeMarkerForNewPointer(event.latLng,geofenceMap);
     }); 
     
	 $('#geofenceModal').modal('show');
	 debugger
}


function placeMarkerForNewPointer(location,map) {
	
	debugger
	$('#searchInput').val('');
	
	// before pointing new marker remove all existing markers.
	removeMarkers();
	
	// if marker props are no set then creating new marker
	/* if(markerForNewPointer)
               markerForNewPointer.setMap(null);*/
          markerForNewPointer = new google.maps.Marker({
               position : location,
               map : map
           });
           
           
    // to center the pointed marker.
    map.setCenter(location);
    
    // capture the pointed marker.
    markersCreated.push(markerForNewPointer);
    
    // change the lat n long in from
    debugger
    $('#geofenceLatitudeId').val(location.lat());
    $('#geofenceLangitudeId').val(location.lng());
    
    
    drawRadius(markerForNewPointer,20);
}
	
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
       console.log("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
	userCurrentLocation ={lat: position.coords.latitude , lng:position.coords.longitude};
}
	
function showError(error) {
	userCurrentLocation={lat:17.444377, lng:78.481153};
}

// remove markers
function removeMarkers(){
    for(i=0; i<markersCreated.length; i++){
    	markersCreated[i].setMap(null);
    }
}

var circle;
function drawRadius(marker, radius){
	debugger
	removeAllCircles();
	// Add circle overlay and bind to marker
	circle = new google.maps.Circle({
	  map: geofenceMap,
	  radius: radius*1000,    // 10 miles in metres
	  fillColor: '#4FF096',
	   strokeColor: "#17C765",
	});
	circle.bindTo('center', marker, 'position');
	circlesCreated.push(circle);
	
	finalRadius=radius;
	debugger
}

function removeAllCircles(){
	 for(i=0; i<circlesCreated.length; i++){
		 circlesCreated[i].setMap(null);
	    }
}


function updateCircleRadius(circle,radius){
	debugger
	circle.setRadius(radius);
	circle.bindTo('center', markerForNewPointer, 'position');
	debugger
	
}

function getGeofenceUpdates(){
	debugger
	//Get all geofence.
	$.get("manageGeofenceUpdates",function(response){
    	if(response.status){	
    	   debugger
    		 debugger
        	 $('#geofenceUpdatesTable').DataTable().destroy();  $('#geofenceUpdatesTable tbody').empty();
			 if(response.data!=null && response.data.length>0){
				 var serviceDataTr='';
		    	 $.each(response.data,function(key,val){
		    		 debugger
		    		 serviceDataTr+=
		    	     '<tr >'+
    		    	    '<td class="tableTd">'+(key+1)+'</td>'+
    		    		'<td class="tableTd">'+val.deviceInfo.id+'</td>'+
    		    		'<td class="tableTd">'+val.geoFence.name+'</td>'+
    		    		'<td class="tableTd">'+val.deviceTimestamp+'</td>'+
    		    		'<td class="tableTd">'+(val.type==201?"Enter":"Exit")+'</td>'+
		    		 '</tr>';
		    	 });
		    	 debugger
		    	    $("#geofenceUpdatesTable tbody").html(serviceDataTr);
	        		$("#geofenceUpdatesTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
	        		
			 }
			 else{
				   $("#geofenceUpdatesTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
			 }
    	}
    	else{
    		alert(response.message)
    	}
	 }).fail(function(){alert("fail");});
	 debugger
}



function getAllDeviceDetails(){
	debugger
	   $.ajax({
	         url : 'getUserDetails', contentType:"application/json", type : 'get', clearForm : true,
			 beforeSend : function(xhr){ /*$.blockUI({ message : "Please wait..." });  return true;*/ },
			 success : function(response) {
			                 if (response.status) {
			                	 debugger
			                	 $('#devicesTableForAssignGeofence').DataTable().destroy();  $('#devicesTableForAssignGeofence tbody').empty();
			        			 if(response.data!=null && response.data.length>0){
			        				 var serviceDataTr='';
			        		    	 $.each(response.data,function(key,val){
			        		    		 debugger
			        		    		 serviceDataTr+=
			        		    	     '<tr id="'+key+'_'+val.AppPwd+'_'+val.id+'_'+val.AppUserName+'_assetsTr">'+
				        		    	    '<td class="tableTd">'+(key+1)+'</td>'+
				        		    		'<td id='+key+' class="tableTd" >'+
				        		    		     '<button class="btn btn-warning btn-xs" >'+
				        		    		          '<input type="checkbox" id="'+key+'_selDeviceToAssignGeofenceCheckboxId" class="selDeviceToAssignGeofenceCheckBox" >'+
				        		    		      '</button>'+
				        		    		'</td>'+
				        		    		'<td class="tableTd">'+val.AppPwd+'</td>'+
				        		    		'<td class="tableTd">'+val.AppUserName+'</td>'+
			        		    		 '</tr>';
			        		    	 });
			        		    	 debugger
			        		    	    $("#devicesTableForAssignGeofence tbody").html(serviceDataTr);
			        	        		$("#devicesTableForAssignGeofence").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
			        	        		
			        	        		//delete wo checkbox click(check / uncheck)
			        		           	  $('#devicesTableForAssignGeofence').DataTable().rows().nodes().to$().find('input[type="checkbox"]').on('click', function(event){
			        		           		  if ($('.selDeviceToAssignGeofenceCheckBox:checked').length == $('.selDeviceToAssignGeofenceCheckBox').length  ) {
			        		           			  $('.selOrDeselAllDevsForGeofenceAssignmnetCheckBox').prop('checked',true);
			        	       			       }
			        		           		  else   if ($('.selDeviceToAssignGeofenceCheckBox:checked').length > 1 ) {
			        		           			  $('.selOrDeselAllDevsForGeofenceAssignmnetCheckBox').prop('checked',false);
			        		           		  }
				        	       			  else{
				        	       				  $('.selOrDeselAllDevsForGeofenceAssignmnetCheckBox').prop('checked',false);
				        	       			  }
			        		           	  });
			        			 }
			        			 else{
			        				   $("#locsTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
			        				 $("#locsModal").modal('show');
			        			 }
			                 }
			               /*  $.unblockUI();*/
			            },
			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		  });
	debugger
}


var deviceIdsSelectforGeofenceAssignment = [];
function assignGeofence(){
	debugger
	if($('.selDeviceToAssignGeofenceCheckBox:checked').length ==0){
		alert('Select atleast one device')
		return false;
	}
	else{
		var selectedDeviceIdAndUserId='';
		$('.selDeviceToAssignGeofenceCheckBox:checked').each(function(i, k){ 
  			debugger
  			deviceIdsSelectforGeofenceAssignment.push({"id":k.parentElement.parentElement.parentElement.id.split('_')[1]});
  			selectedDeviceIdAndUserId+=(i==$('.selDeviceToAssignGeofenceCheckBox:checked').length-1)?k.parentElement.parentElement.parentElement.id.split('_')[1]+"-"+k.parentElement.parentElement.parentElement.id.split('_')[3]+".":k.parentElement.parentElement.parentElement.id.split('_')[1]+"-"+k.parentElement.parentElement.parentElement.id.split('_')[3]+",  ";
  		});
		
		//Get all geofence.
		$.get("getAllGeofences",function(response){
	    	if(response.status){	
	    	   debugger
	    	   $("#geofenceSelectId").select2();
	    	   $('#geofenceSelectId').find('option').remove().end();
		       $('#geofenceSelectId').append('<option value="" disabled selected>-- Select Geofence --</option>');
		  	   response.data.forEach(function(item, index){  
		  			  $("#geofenceSelectId").append("<option value='" +item.geoFenceId+ "' >" +item.name+ "</option>");
		  		 }); 
	    	}
		 }).fail(function(){alert("fail");});
		
		
		$('#devicesSelectedForAssignmentId').text(selectedDeviceIdAndUserId);
		$('#assignGeofenceModal').modal('show');
	}
	debugger
}

var assignGeofecnceRequestJson=[];
function saveGeofenceAssignment(){
	debugger
	  if( !stringIsEmpty($('#geofenceSelectId option:selected').val())){
		   debugger
		// create the req json
			var confirmationData="";
			$('#geofenceSelectId').val().forEach(function(item, index){  
				  debugger
				  $("#geofenceSelectId option[value='"+item+"']").text();
				  confirmationData+=
									'<div class="row">'+
									    '<label style="color: #18868E">'+(index+1)+'. '+$("#geofenceSelectId option[value='"+item+"']").text()+'</label>'+
									    '<div class="col-lg-12">"'+$('#devicesSelectedForAssignmentId').text()+'"</div>'+
									'</div>';
					
				  assignGeofecnceRequestJson.push({"deviceInfos":deviceIdsSelectforGeofenceAssignment,
													"geoFence": {"geoFenceId":item},
												    "type":1//assign
										            });
			 }); 
			debugger
			 $('#AssignGeofenceConfirmationModalBody').html(confirmationData);
			 $('#AssignGeofenceConfirmationModal').modal('show');
	   }
	  else{
		  alert("Please select geofence");
		  return false;
	  }
	debugger
}


$( "#confirmAssignGeofence_BtnId" ).on('click',function() {
		debugger
	 $.ajax({
		   url:"manageGeofenceAssignment", dataType : 'json',type : 'post', data:JSON.stringify({"geofenceDeviceMapModels" :assignGeofecnceRequestJson}),contentType: "application/json",
		   beforeSend : function(xhr){ /*$.blockUI({ message : "Please wait..." });  return true;*/ },
			   success : function(response) {
				                 if (response.status) {
				                	 debugger
				                	 /*$.unblockUI();*/
				                	$('#responseSuccessMessage').text(response.message);
									$('#requestSuccessModal').modal('show');
								}
								else{
									$('#responseFailMessage').text(response.message);
									$('#requestFailModal').modal('show');
								}
				                $('#assignGeofenceModal').modal('hide');
				                $('#AssignGeofenceConfirmationModal').modal('hide');
				 },
				 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError);  $.unblockUI();}
			  });
});




//s  of sc veh  **********************************************************************************/
function getSCMasterData(){
	 debugger
	    $.get("getSCMasterData",function(response){
	    	if(response.status){	
	    	   debugger
	    	   $("#scSelectId").select2();
	    	   $('#scSelectId').find('option').remove().end();
		         $('#scSelectId').append('<option value="" disabled selected>-- Select SC --</option>');
		  		 response.data.forEach(function(item, index){  
		  			  $("#scSelectId").append("<option value='" +item+ "' >" +item+ "</option>");
		  		 }); 
	    	}
		 }).fail(function(){alert("fail");});
}


function initMap(){
	 debugger
	 map = new google.maps.Map(document.getElementById('map_DivId'), {
		    center: {lat: 17.444785, lng:78.505353},// mandatory other wise map will not dispaly anything.
		    zoom: 17
		    });
	
	 debugger
}


//wo sel/desel all
$('.selOrDeselAllDevsForGeofenceAssignmnetCheckBox').click(function() {
	  debugger
	  if($(this).is(':checked')){
		  $('#devicesTableForAssignGeofence').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', true);
	  }
	  else{
		  $('#devicesTableForAssignGeofence').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', false);
	  }
})

var  scVehicles,map;
function getSCWiseVehicles(){
	  if( !stringIsEmpty($('#scSelectId option:selected').val())){
		   debugger
	 	       $.ajax({
	 	         url : 'rtmsVehicleInfo',data: "&scCode=" +$('#scSelectId option:selected').val(), dataType : "json", contentType:"application/json", type : 'get', clearForm : true,
	 			 beforeSend : function(xhr){ /*$.blockUI({ message : "Please wait..." });  return true;*/ },
	 			 success : function(response) {
	 			                 if (response.status) {
	 			                	 debugger
	 			                	 if(response.data!=null && response.data.length>0){
	 			                		 // capture list of vehicles for test.
	 	 			                //	scVehicles=response.data;
	 	 			                	ploVehiclesOnMap(response.data,map);
	 			                	 }
	 			                	 else{
	 			                		 alert("No Vehicles available");
	 			                		 //Clear the map
	 			                		$('#scVehiclesCount_font').text($('#scSelectId option:selected').val()+" ( Vehicle = 0)");
	 			                	 }
	 			                /* $.unblockUI();*/
	 			            }
	 			 },
	 			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); /* $.unblockUI();*/}
	 		  });
	   }
	  else{
		  alert("Please Select SC");
		  return false;
	  }
}

var allInfoWindows=[];
function ploVehiclesOnMap(scVehicles,map){
	debugger
     $('#scVehiclesCount_font').text($('#scSelectId option:selected').val()+" ( Vehicle Count : "+scVehicles.length+")");
	//Set the center of the map to the first vehicle location.
	 map.setCenter(new google.maps.LatLng(scVehicles[0].currentLocationLatitude,scVehicles[0].currentLocationLongitude));
	 var markersToFitBound=[];
	 var bounds = new google.maps.LatLngBounds();
	 $.each( scVehicles, function( key, value ) {
	        	 debugger
	        	 //Create a marker.
	        	  marker =  new google.maps.Marker({
	        	     position :{lat:value.currentLocationLatitude, lng:value.currentLocationLongitude},
	        	     map : map,
	        	     icon : "images/truck3.jpg"
	        	  });
	        	  marker.setMap( map ); 
	        	
	        	  bounds.extend(marker.getPosition());
	        	  
	        	  
        		 // Create a info Window for marker.
	        	  debugger
        		  infoWindow = new google.maps.InfoWindow();
        		  content= 
    	              ' <div >'+
    	                 '<label  style="color:blue;"><u>VEHICLE INFO</u></label><br>'+
    	                 '<label style="color:#D43F08;" >Vehicle : '+value.vehicleId+', Capacity = '+value.totalCapacity+'</label><br>'+
    	                 '<label  style="color:#D43F08;">Vendor : '+value.vendorDetails.vendorId+' - '+value.vendorDetails.appUsername+'</label><br>'+
    	                 '<label  style="color:#D43F08;">User : '+value.userDetails.appUserName+'</label><br>'+
    	                 '<label style="color:#D43F08;">Pickups : <br>&nbsp;&nbsp;Total Pickups = '+value.vehiclePickups.totalCount+ "<br>&nbsp;&nbsp;Total Weight = "+value.vehiclePickups.totalWeight+ "<br>&nbsp;&nbsp;Current Usage = "+value.vehiclePickups.currentUsage+ "<br>&nbsp;&nbsp;Projected Free = "+value.vehiclePickups.projectedFree+'</label><br>'+
	    	                 '&nbsp;&nbsp;<button type="button" class="btn bg-green btn-xs waves-effect"  id="'+value.vehicleId+'_'+value.currentLocationLatitude+'_'+value.currentLocationLongitude+'_pickup" onclick="getVehiclePickups(this)" style="background-color: #e91e63;color: white;border-radius: 5px 5px 5px 5px;" >'+
		                     '<span>Details</span>'+
		                     '</button><br><br>'+
    	                 '<label  style="color:#D43F08;">Deliveries : <br>&nbsp;&nbsp;Total Deliveries = '+value.vehicleDeliveries.totalCount+"<br>&nbsp;&nbsp;Total Weight = "+value.vehicleDeliveries.totalWeight+"<br>&nbsp;&nbsp;Current Usage = "+value.vehicleDeliveries.currentUsage+"<br>&nbsp;&nbsp;Projected Free = "+value.vehicleDeliveries.projectedFree+'</label><br>'+
	    	                 '&nbsp;&nbsp;<button type="button" class="btn bg-green btn-xs waves-effect" id="'+value.vehicleId+'_delivery"  onclick="getVehicleDeliveries(this)" style="background-color: #e91e63;color: white;border-radius: 5px 5px 5px 5px;" >'+
		                     '<span>Details</span>'+
		                     '</button><br>'+
    	               '</div>';
        		  
        		  allInfoWindows.push(infoWindow);
        		  
        		  
        		  google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
        		        return function() {
        		        	
        		           closeAllInfoWindows();
        		        	
        		           infowindow.setContent(content);
        		           infowindow.open(map,marker);
        		        };
        		    })(marker,content,infoWindow)); 
        		  
        		/*  google.maps.event.addListener(marker, 'mouseout', (function(marker, content, infoWindow) {
        		      return function() {
        		    	  infoWindow.close();
        		      };
        		    })(marker, content, infoWindow));*/

        		  
        		  map.fitBounds(bounds);
	 });
	debugger
}

function getVehiclePickups(event){
	debugger
	$.get("getVehicleWisePickups",function(response){
    	if(response.status){	
    	   debugger
        	 $('#vehPickupsTable').DataTable().destroy();  $('#vehPickupsTable tbody').empty();
				 var serviceDataTr='';
		    	 $.each(response.data,function(key,val){
		    		 debugger
		    		 // tr id = vid, vlat, vlng, psid, plat, plng.
		    		 serviceDataTr+=
		    	     '<tr id="'+event.id.split('_')[0]+'_'+event.id.split('_')[1]+'_'+event.id.split('_')[2]+'_'+val.gpsLat+'_'+val.gpsLon+'">'+
    		    		'<td class="tableTd">'+val.pickUpScheduleId+'</td>'+
    		    		'<td class="tableTd">'+val.customerCode+'</td>'+
    		    		'<td class="tableTd">'+val.customerName+'</td>'+
    		    		'<td class="tableTd">'+val.pickupAddress+'</td>';
		    		     if(val.status=="Pending"){
		    		    	 serviceDataTr+= 
		    		    		 '<td class="tableTd" style="color:red"><b>'+val.status+'</b></td>'+
		    		    		 '<td class="tableTd">'+
						    		  '<button type="button" class="btn bg-teal waves-effect btn-xs" onclick="showETA(this)">ETA</button><br>'+
						    		  '<P class="distanceduration" ></p>'+
			    		         '</td>';
		    		     }
		    		     else{
		    		    	 serviceDataTr+= 
		    		    		 '<td class="tableTd" style="color:green"><b>'+val.status+'</b></td>'+
		    		    		 '<td class="tableTd">'+
						    		  '<P> -- </p>'+
	   		    	         	'</td>';
		    		     }
		    		     serviceDataTr+= '</tr>';
		    		 debugger
		    	 });
		    	 debugger
		    	    $("#vehPickupsTable tbody").html(serviceDataTr);
	        		$("#vehPickupsTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
	        		
	        		debugger
	        		$('#vehiclePickupsModalHead').text("Pickups (Veh Id = "+event.id.split('_')[0]+")");
	        		$('#vehPickupsModal').modal('show');
	        		
	        		
    	}
    	else{
    		alert(response.message)
    	}
	 }).fail(function(){alert("fail");});
	debugger
}

var pickupForWhichETAIsBeingCalculated;
function showETA(event){
	debugger
	var origin = new google.maps.LatLng(event.parentElement.parentElement.id.split('_')[1], event.parentElement.parentElement.id.split('_')[2]);
	var destination = new google.maps.LatLng(event.parentElement.parentElement.id.split('_')[3], event.parentElement.parentElement.id.split('_')[4]);
	
	pickupForWhichETAIsBeingCalculated=event.nextSibling.nextSibling;
	
    var geocoder = new google.maps.Geocoder;
    var service = new google.maps.DistanceMatrixService;
    
    service.getDistanceMatrix({
      origins: [origin],
      destinations: [destination],
      travelMode: 'DRIVING',
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways :false,
      avoidTolls :false
    },distanceMatrixCallback); 
	debugger
}


function distanceMatrixCallback(response, status) {
	  if (status == 'OK') {
		  
		  $(".distanceduration").text("");
		  $(pickupForWhichETAIsBeingCalculated).text("Distance = "+response.rows[0].elements[0].distance.text+ ", Duration = "+response.rows[0].elements[0].duration.text);
		  
		  
		  
	   /* var origins = response.originAddresses;
	    var destinations = response.destinationAddresses;
        debugger
        
        
	    alert(response.rows[0].elements[0].distance.text+"\n"+response.rows[0].elements[0].duration.text);
	    
	    
	    
	    for (var i = 0; i < origins.length; i++) {
	      var results = response.rows[i].elements;
	      for (var j = 0; j < results.length; j++) {
	        var element = results[j];
	        var distance = element.distance.text;
	        var duration = element.duration.text;
	        var from = origins[i];
	        var to = destinations[j];
	        
	      }
	    }*/
	  }
	}

function closeAllInfoWindows() {
	  for (var i=0;i<allInfoWindows.length;i++) {
		  allInfoWindows[i].close();
	  }
	}

//e  of sc veh  **********************************************************************************/




function getAllUsersWithDeviceDetails(){
	debugger
	   $.ajax({
	         url : 'getUserDetails', contentType:"application/json", type : 'get', clearForm : true,
			 beforeSend : function(xhr){ /*$.blockUI({ message : "Please wait..." });  return true; */},
			 success : function(response) {
			                 if (response.status) {
			                	 debugger
			                	 $('#locsTable').DataTable().destroy();  $('#locsTable tbody').empty();
			        			 if(response.data!=null && response.data.length>0){
			        				 var serviceDataTr='';
			        		    	 $.each(response.data,function(key,val){
			        		    		 debugger
			        		    		 serviceDataTr+=
			        		    	     '<tr id="'+key+'_'+val.AppPwd+'_'+val.id+'_'+val.AppUserName+'_assetsTr">'+
				        		    	    '<td class="tableTd">'+(key+1)+'</td>'+
				        		    		'<td id='+key+' class="tableTd" style="text-align: center;">'+
				        		    		     '<button class="btn btn-warning btn-xs" >'+
				        		    		          '<input type="checkbox" id="'+key+'_deleteWOCheckboxId" class="deleteWOCheckBox" >'+
				        		    		      '</button>'+
				        		    		'</td>'+
				        		    		'<td class="tableTd">'+val.AppUserName+'<small>&nbsp;&nbsp;('+val.AppPwd+')</small></td>'+
				        		    		'<td style="text-align: center;">'+
				        		    		     '<button id="deleteWOBtn" onclick="lockCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
				        		    		          '&nbsp;&nbsp;<i class="fa fa-lock" ></i>&nbsp;&nbsp'+
				        		    		      '</button>'+
				        		    		 '</td>'+
				        		    		 '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="sirenCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-bullhorn fa-2x"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="appsListCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-list"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="wipeCmd(this)"  class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-trash"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="locUpdatesCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-map-marker"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="pushappCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-file"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		    /* '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="editWO(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-globe"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+*/
			        		    		 '</tr>';
			        		    	 });
			        		    	 debugger
			        		    	    $("#locsTable tbody").html(serviceDataTr);
			        	        		$("#locsTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
			        	        		$("#locsModal").modal('show');
			        	        		
			        	        		//delete wo checkbox click(check / uncheck)
			        		           	  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').on('click', function(event){
			        		           		  if ($('.deleteWOCheckBox:checked').length == $('.deleteWOCheckBox').length  ) {
			        		           			$('.selectOrDeselectAllWOCheckBox').prop('checked',true);
			        		           			$('#commands_div').show();
			        	       			       }
			        		           		  else   if ($('.deleteWOCheckBox:checked').length > 1 ) {
			        		           			  $('.selectOrDeselectAllWOCheckBox').prop('checked',false);
			        		           			$('#commands_div').hide();
			        		           		  }
				        	       			  else{
				        	       				  $('.selectOrDeselectAllWOCheckBox').prop('checked',false);
				        	       				$('#commands_div').hide();
				        	       			  }
			        		           	  });
			        			 }
			        			 else{
			        				 /* alert("Assets not available"); */
			        				   $("#locsTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
			        				 $("#locsModal").modal('show');
			        			 }
			                 }
			              /*   $.unblockUI();*/
			            },
			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		  });
	debugger
}


//wo sel/desel all
$('.selectOrDeselectAllWOCheckBox').click(function() {
	  debugger
	  if($(this).is(':checked')){
		  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', true);
		  $('#commands_div').show();
	  }
	  else{
		  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', false);
		  $('#commands_div').hide();
	  }
})

 //del wo
	 $('#deleteWOBtn').click(function() {
			var woIdsToBeDeleted=[],woNosToBeDeleted='';
			 $('#servicesTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(key,val){
				 debugger
				 woIdsToBeDeleted.push({"id":allServiesData[this.id.split('_')[0]].id});
				 woNosToBeDeleted=(key==0)?allServiesData[this.id.split('_')[0]].woNo:woNosToBeDeleted+", "+allServiesData[this.id.split('_')[0]].woNo;
			 }) 
			 if(woNosToBeDeleted==''){
				 alert("Please select atleast one Work Order");
			 }
			 else{
			 }
	 });

//lock 
/*selectedUsers+=(selectedUsers.length>0)?event.parentNode.parentElement.id.split("_")[3] +",":event.parentNode.parentElement.id.split("_")[3] ;
selectedUserDeviceImeis+=(selectedUserDeviceImeis.length>0)?event.parentNode.parentElement.id.split("_")[1] +",":event.parentNode.parentElement.id.split("_")[1] ;*/

function lockCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send lock command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_LOCK,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');
	}
	else{
		return false;
	}
}


function sirenCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send Siren command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_SCREAM,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');
	}
	else{
		return false;
	}
}

function wipeCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send Siren command to \n"+selectedUsers+"\n\n NOTE* : THIS WILL PERMANENTLY REMOVE ALL THE DATA.");
	if (confirmation == true) {
		//send push
		debugger
		alert("Request Completed")
		/*$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_WIPE,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');*/
	}
	else{
		return false;
	}
}

function appsListCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
	 var confirmation = confirm("Send Apps List command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.ajax({
			url : 'allOperations',
			dataType : 'json',
			data: {'deviceid[]': event.parentNode.parentElement.id.split("_")[1], deviceid:event.parentNode.parentElement.id.split("_")[1], operationtype: CONSTANTS_APP_LIST, type:"command"},
			type : 'post',
			beforeSubmit : function() {
			/*	$.blockUI({
					message : "Please Wait sending message to device to synch its data...."
				});*/
			},
			success : function(response,status,request,form) {					
				/*$.unblockUI();	*/
					if(response.status){				        			
	        			$.ajax({
							url: "allApplications",
		                	type: "POST",
		                	dataType : 'json',
		                	data: { deviceid :  event.parentNode.parentElement.id.split("_")[1] },
		                	beforeSend : function(jqXHR, settings){
		                		/*$.blockUI({
		    						message : "Data synched now taking data from server...."
		    					});*/
		                	},
		                	success: function (response) {
		                		/*$.unblockUI();*/
		                		if(response.status){   		
		                			$("#appsListHead_h4").text("Apps List for Device id = "+event.parentNode.parentElement.id.split("_")[1]);
		                			 $('#assetsTable').DataTable().destroy();  $('#assetsTable tbody').empty();
		                			 debugger
		                			 if(response.data!=null && response.data.data.length>0){
		                				 var serviceDataTr='';
		                		    	 $.each(response.data.data,function(key,val){
		                		    		 debugger
		                		    		 serviceDataTr+='<tr id="'+key+'_'+event.parentNode.parentElement.id.split("_")[1]+'_'+val.applicationPackageName+'_assetsTr">'+
		                		    		 '<td class="tableTd">'+(key+1)+'</td>'+
		                		    		 '<td class="tableTd">'+(val.applicationName?val.applicationName:"--")+'</td>'+
		                		    		 '<td class="tableTd">'+(val.applicationVersion?val.applicationVersion:"--")+'</td>'+
		                		    		 '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="uninstallAppCmd(this)" class="btn btn-danger btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-trash"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+		                		    	
			        		    		     '</tr>';
		                			    	
		                		    	 });
		                		    	 debugger
		                		    	    $("#assetsTable tbody").html(serviceDataTr);
		                	        		$("#assetsTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
		                				      $("#deleteAssetBtn").prop('disabled', false);
		                	        		$("#assetsModal").modal('show');
		                	        		
		                	        		 //delete asset checkbox click(check / uncheck)
		                	        		  $('#assetsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').on('click', function(event){
		                	        			  debugger
		                	        			  if ($('.deleteAssetCheckBox:checked').length == $('.deleteAssetCheckBox').length) {
		                	        				  debugger
		                	        		           $('.selectOrDeselectAllAssetCheckBox').prop('checked',true);
		                	        			    }else{
		                	        			    	 debugger
		                	        			    	  $('.selectOrDeselectAllAssetCheckBox').attr('checked',false);
		                	                			  captureAssetsIdsToBeDeleted(event.currentTarget.id.split('_')[0]);
		                	        			    }
		                	        	       });
		                			 }
		                			 else{
		                				   $("#assetsTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
		                				   $("#deleteAssetBtn").prop('disabled', true);
		                				 $("#assetsModal").modal('show');
		                			 }
		                		}else{
		                			alert(response.message);
		                		}
		                	},
			                error: function(xhr, ajaxOptions, thrownError){
			                	/*$.unblockUI();*/
		                	},
	    	                timeout :CONSTANTS_AJAX_TIMEOUT 
						});
	        		}
					else{
						alert(response.message);
					}
				//}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert('error');
				alert(xhr.status);
				alert(thrownError);
				/*$.unblockUI();*/
			},
			timeout :CONSTANTS_AJAX_TIMEOUT
		});
		
		
	}
	else{
		return false;
	}
	
}

function uninstallAppCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[1]  ;
	debugger
    var confirmation = confirm("Send Uninstall command to \nDeviceId : "+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		
		$.ajax({
     		url:"allOperations",
     		data:"uninstall_apps="+event.parentNode.parentElement.id.split("_")[2]+"&operationtype="+CONSTANTS_APP_UNINSTALL+"&deviceid="+event.parentNode.parentElement.id.split("_")[1]+"&type="+"command",
     		dataType : 'json',
     		type: "POST",
     		beforeSend: function(){      			
     		},
     		success : function(response){
     			alert("sucess");
     			if (response.status) {
     				 $("#assetsModal").modal('hide');
				} else {
					alert(response.message);
				}
     			},
     	});
		
	}
	else{
		return false;
	}
}


function locUpdatesCmd(event){
	debugger
	  $.ajax({
	         url : 'getUserLocationHistory?userId=6&from=90&to=89',contentType:"application/json", type : 'get', clearForm : true,
			 beforeSend : function(xhr){
				/* $.blockUI({ message : "Please wait..." });  return true;*/
		    },
		     success : function(response) {
		    	 debugger
		                 if (response.status) {
		                	 debugger
		                	 if(response.data!=null && response.data.length>0){
		                		 debugger
		                		 //openMapinModal(event.parentNode.parentElement.id.split("_")[1],response.data);
		                		 showlocationHistory(response.data,event.parentNode.parentElement.id.split("_")[1]);
		                		 
		                		 }
		                	 else{
		                		 alert(response.message)
		                	 }
		                 }
		              /*   $.unblockUI();*/
		            },
			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		  });
	
	
}


var loctionsHistoryOfDeviceMap;
var markerArray = [];
var directionsService;
var directionsDisplay;
var stepDisplay;
function openMapinModal(did,locations){
	debugger
	//$('#loctionsHistoryOfDeviceMapModal').one('click', function(){
		$('#loctionsHistoryOfDeviceMapModal').on('shown', function(){
		showlocationHistory(locations,did);
		});
}


function showlocationHistory(locations, did){
	debugger
	loctionsHistoryOfDeviceMap = new google.maps.Map(document.getElementById('loctionsHistoryOfDeviceMap_DivId'), {
		    center: {lat: locations[0].latitude, lng:locations[0].longitude},// mandatory other wise map will not dispaly anything.
		    zoom: 17
     });
	 loctionsHistoryOfDeviceMap.setCenter(new google.maps.LatLng(locations[0].latitude,locations[0].longitude));
	  // Instantiate a directions service.
	    directionsService = new google.maps.DirectionsService;
		  // Create a renderer for directions and bind it to the map.
	     directionsDisplay = new google.maps.DirectionsRenderer({map: loctionsHistoryOfDeviceMap});
	    // Instantiate an info window to hold step text.
	     stepDisplay = new google.maps.InfoWindow;
	 
	drawDirectionPath(locations, directionsService,directionsDisplay,stepDisplay,markerArray);
	
	loctionsHistoryOfDeviceMap.setCenter(new google.maps.LatLng({lat: locations[0].latitude, lng:locations[0].longitude}));
	
	// bind info window data to table.
	var serviceDataTr='';
	 $.each(locations,function(key,val){
		 debugger
		 serviceDataTr+='<tr >'+
		 '<td class="tableTd">A</td>'+
		 '<td class="tableTd">'+(val.address?val.address:"--")+'</td>'+
	     '</tr>';
	 });
	$("#loctionsHistoryOfDeviceMapTables tbody").html(serviceDataTr);
	
	 $("#loctionsHistoryOfDeviceMapModalHead_h4").text("Location of Device id = "+did);
	     $('#loctionsHistoryOfDeviceMapModal').modal('show');
	     
	
}

function  drawDirectionPath(locationList, directionsService,directionsDisplay,stepDisplay,markerArray ) {
	   var origin = {lat: locationList[0].latitude, lng: locationList[0].longitude};
	   var destination= {lat: locationList[locationList.length-1].latitude, lng: locationList[locationList.length-1].longitude};
	   
	   //waypts
	   var waypts = [];
	   for (var i = 1; i < locationList.length-1; i++) {
        waypts.push({
          location: {lat: locationList[i].latitude, lng: locationList[i].longitude},
          stopover: true
        });
      }
	   
	   calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map,origin, destination,waypts );
 }


   function calculateAndDisplayRoute(directionsDisplay, directionsService,
       markerArray, stepDisplay, map, origin, destination,waypts) {
     // First, remove any existing markers from the map.
     for (var i = 0; i < markerArray.length; i++) {
       markerArray[i].setMap(null);
     }

     // Retrieve the start and end locations and create a DirectionsRequest using
     // WALKING directions.
     directionsService.route({
      /*  origin: document.getElementById('start').value,
       destination: document.getElementById('end').value, */
       origin: origin,
       destination:destination,
       waypoints: waypts,
       travelMode: 'DRIVING'
     }, function(response, status) {
       // Route the directions and pass the response to a function to create
       // markers for each step.
       if (status === 'OK') {
     	console.log( '<b>' + response.routes[0].warnings + '</b>');
        /*  document.getElementById('warnings-panel').innerHTML =
             '<b>' + response.routes[0].warnings + '</b>'; */
         directionsDisplay.setDirections(response);
         //showSteps(response, markerArray, stepDisplay, map);
       } else {
         window.alert('Directions request failed due to ' + status);
       }
     });
   }

   function showSteps(directionResult, markerArray, stepDisplay, map) {
     // For each step, place a marker, and add the text to the marker's infowindow.
     // Also attach the marker to an array so we can keep track of it and remove it
     // when calculating new routes.
     var myRoute = directionResult.routes[0].legs[0];
     for (var i = 0; i < myRoute.steps.length; i++) {
       var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
       marker.setMap(map);
       marker.setPosition(myRoute.steps[i].start_location);
       attachInstructionText(
           stepDisplay, marker, myRoute.steps[i].instructions, map);
     }
   }

   function attachInstructionText(stepDisplay, marker, text, map) {
     google.maps.event.addListener(marker, 'click', function() {
       // Open an info window when the marker is clicked on, containing the text
       // of the step.
       stepDisplay.setContent(text);
       stepDisplay.open(map, marker);
     });
   }

   
   
   // animation
   var userLocation;
   function getVehicleLocation(){
	  debugger
	 /*  if( $('#vehicleSelectId').val()==null || $('#vehicleSelectId').val().length <=0){
			alert("Please Select Vehicle");
			return false;
		}
	   else{*/
		   $.ajax({
		         url : 'getUserLocationHistory?userId=16',contentType:"application/json", type : 'get', clearForm : true,
				 beforeSend : function(xhr){
					/* $.blockUI({ message : "Please wait..." });  return true;*/
			    },
			     success : function(response) {
			    	 debugger
			                 if (response.status) {
			                	 debugger
			                	 // set the user name and date for which the animaton is being processed.
	                			// $("#userNameSelected").text($('#userSelectId option:selected').text());
	                			 //$("#dateSelected").text($('#dateId').val());
			                	 if(response.data!=null && response.data.length>0){
			                		 debugger
			                		 locationAsSource=0;
			                		 userLocation = response.data;
			                		 initVehiclePathMap(response.data);
			                		 }
			                	 else{
			                		 alert(response.message)
			                	 }
			                 }
			                /* $.unblockUI();*/
			            },
				 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
			  });
	 /*  }*/
   }
   
   
 
   var mapForAnim;
   var infoContentForAnimatedMarker='';
   var infow;
   function initVehiclePathMap(locationList){
		
		   
		   debugger
		 
	     //Create a center for the map.
		 mapForAnim = new google.maps.Map(document.getElementById('vehiclePathMap_DivId'), {
	     center: {lat: locationList[0].latitude, lng:locationList[0].longitude},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		
		 //Create the marker for the first location.
		  markerA =  new google.maps.Marker({
	          position : {lat: locationList[0].latitude, lng:locationList[0].longitude},
	         map : mapForAnim,
	         icon : "images/v.png"
	      //   icon:icon
	      });
		  markerA.setMap( mapForAnim ); 
		 
		
		 //Create a position array which always contains first location lat and lng. Position arrary is used for the marker animation.
		 position=[locationList[0].latitude,locationList[0].longitude ];
		
		 
		 infoContentForAnimatedMarker= 
             ' <div >'+
                '<label style="color:#D43F08;" >'+locationList[0].latitude+','+locationList[0].longitude+'</label><br>'+
                '<label  style="color:#D43F08;">'+locationList[0].address+'</label><br>'+
                '<label  style="color:blue;">'+locationList[0].deviceTimestamp+'</label><br>'+
              '</div>';
		 
		 
		  infow=new google.maps.InfoWindow({content : infoContentForAnimatedMarker});
		 infow.open(mapForAnim,markerA);
		 
		 //Create a path on the 
		 showOnMap(locationList);
		
		 //Clear the var which holds the location to which the marker is beign animated
		// locationAsSource=0;
		 
		 
		 
		/* //USed for direcs api
		 
		   markerArray = [];
		 
		  // Instantiate a directions service.
	    directionsService = new google.maps.DirectionsService;
		 
		  // Create a renderer for directions and bind it to the map.
	     directionsDisplay = new google.maps.DirectionsRenderer({map: map});
		 
	    // Instantiate an info window to hold step text.
	     stepDisplay = new google.maps.InfoWindow;*/
	    
		 debugger
	}
   
   function showOnMap(locationList){
		debugger
		var polyCoords=[];
		// for some reason each () is not working so using for loop. chage it after fixing issue with each ().
		for (var i = 0; i < locationList.length; i++) {
			//debugger
			if(locationList.length ==1){
				triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude}];
			}
			else if(i==0){
				triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude},{lat : locationList[i+1].latitude, lng : locationList[i+1].longitude}];
			}
			else{
				triangleCoords = [{lat: locationList[i-1].latitude, lng: locationList[i-1].longitude},{lat : locationList[i].latitude, lng : locationList[i].longitude}];
				polyCoords.push(triangleCoords);
			}
			mapForAnim.setCenter(new google.maps.LatLng(locationList[i].latitude, locationList[i].longitude));
			//mapForAnim.setZoom(17); 
			  drawPolyline(triangleCoords,i);
		}
		
		debugger
	}

   function drawPolyline(triangleCoords){
		 debugger
		  var bermudaTriangle = new google.maps.Polygon({
		         paths: triangleCoords,
		         strokeColor: '#FF0000',
		         strokeOpacity: 0.8,
		         strokeWeight: 3
		       //icon: lineSymbol,
		       });
		       bermudaTriangle.setMap(mapForAnim);
		       debugger
	}
   
   
   var numDeltas = 100;
   var delay = 40; //milliseconds
   var i = 0;
   var deltaLat;
   var deltaLng;
   function transition(result){
	   debugger
		i = 0;
	    deltaLat = (result[0] - position[0])/numDeltas;
	    deltaLng = (result[1] - position[1])/numDeltas;
	    moveMarker(position);
	    debugger
	}

	function moveMarker(){
		debugger
	    position[0] += deltaLat;
	    position[1] += deltaLng;
	    var latlng = new google.maps.LatLng(position[0], position[1]);
	    markerA.setPosition(latlng);
	    if(i!=numDeltas){
	        i++;
	        setTimeout(moveMarker, delay);
	    }
	    debugger
	}

	//capture index of location for which the marker is being animated.
	var locationAsSource=0;
	function animateMarker(){
		debugger
		if (locationAsSource < userLocation.length) {
			 infoContentForAnimatedMarker= 
	             ' <div >'+
	                '<label style="color:#D43F08;" >'+userLocation[locationAsSource].latitude+','+userLocation[locationAsSource].longitude+'</label><br>'+
	                '<label  style="color:#D43F08;">'+userLocation[locationAsSource].address+'</label><br>'+
	                '<label  style="color:blue;">'+userLocation[locationAsSource].deviceTimestamp+'</label><br>'+
	              '</div>';
			 infow.setContent(infoContentForAnimatedMarker);
		    infow.open(mapForAnim,markerA);
			 var result = [ userLocation[locationAsSource].latitude, userLocation[locationAsSource].longitude];
			    transition(result);
			    
			   
			    
			    
			    //capture index of next location(source) for wich marker should be animated animated.
			    locationAsSource++;
			    debugger
			   
		}
		else if (locationAsSource==userLocation.length){
			alert("Reached Destination");
		}
		debugger
	}
   
	
	function pushappCmd(){
		$("#pushappModal").modal("show");
	}
	
	
	 function initMapForGeofence(){
		 debugger
	     //Create a center for the map.
		 geofenceMap = new google.maps.Map(document.getElementById('geofenceMap_DivId'), {
			 center: {lat: 17.444785, lng:78.505353},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		 geofenceMap.setCenter(new google.maps.LatLng({lat: 17.444785, lng:78.505353}));
		 $('#geofenceModal').modal('show');
		 debugger
	}
	
//end - mdm commands
/////////////////////////////////////////////////////////////////////////////////
	 
	 
	 
	 
//////////////////////////////  vehicle path animation /////////////////////////////////////////////

	// get veh locs.
	 var userLocation;var position;
	 function getVehicleLocationHistory(){
	 	  debugger
	 		   $.ajax({
	 		         url : 'getDeviceLocation?userId=572',contentType:"application/json", type : 'get', clearForm : true,
	 				 beforeSend : function(xhr){
	 				/* 	 $.blockUI({ message : "Please wait..." });  return true; */
	 			    },
	 			     success : function(response) {
	 			    	 debugger
	 			                 if (response.status) {
	 			                	 debugger
	 			                	 if(response.data!=null && response.data.length>0){
	 			                		 debugger
	 			                		 locationAsSource=0;
	 			                		 userLocation = response.data;
	 			                		  $('#seek-bar').prop('min',1);
	 			                		  $('#seek-bar').prop('max',response.data.length);
	 			                		  
	 			                		  $('#startTime').text(response.data[0].deviceTimestamp);
	 			                		  $('#endTime').text(response.data[response.data.length-1].deviceTimestamp);
	 			                		  
	 			                		   //Create a position array which always contains first location lat and lng. Position arrary is used for the marker animation.
	 			             			   position=[userLocation[0].latitude,userLocation[0].longitude ];
	 			                		   
	 			             			   //draw path
	 			             		       initVehiclePathMap();
	 			                		 }
	 			                	 else{
	 			                		 alert(response.message)
	 			                	 }
	 			                 }
	 			               /*   $.unblockUI(); */
	 			            },
	 				 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
	 			  });
	 }
	 
	 
	 var mapForAnim;
	 var infoContentForAnimatedMarker='';
	 var infow;
	 var markerA;
	 function initVehiclePathMap(){
	 		 debugger
	 		 locationList = userLocation;
	 	     
	 		
	 		 mapForAnim.setCenter(new google.maps.LatLng(locationList[0].latitude, locationList[0].longitude));
	 		 
	 		 //Create the marker for the first location.
	 		  markerA =  new google.maps.Marker({
	 	          position : {lat: locationList[0].latitude, lng:locationList[0].longitude},
	 	         map : mapForAnim,
	 	     //  icon : "images/markerWithVeh.jpg", 
	 	         icon : "images/v.png"
	 	         //icon:icon
	 	   		 
	 	      });
	 		  markerA.setMap( mapForAnim ); 
	 		  
	 		 //Create a path on the 
	 		 showOnMap(locationList);
	 		 
	 		 //moveToLocation
	 		 moveToLocation(locationList[0].latitude, locationList[0].longitude);
	 	    
	 		 //To show all locs - avoid moving of pan.
	 		 var bounds = new google.maps.LatLngBounds();
	 		 $.each( locationList, function( key, value ) {
	         	 debugger
	         	 if(key==0 || key == locationList.length-1){
	         		 //Create a marker.
	            	    marker =  new google.maps.Marker({
	 	           	     position :{lat:value.latitude, lng:value.longitude},
	 	           	   //  map : mapForAnim,
	 	           	   // icon : "images/blueMarker.png"
	            	    }); 
	            	   // marker.setMap( mapForAnim );
	         	 }
	         	 else{
	         		 //Create a marker.
	            	    marker =  new google.maps.Marker({
	 	           	     position :{lat:value.latitude, lng:value.longitude},
	            	     });
	         	 }
	         	  bounds.extend(marker.getPosition());
	         	  mapForAnim.fitBounds(bounds);
	          });
	 		 debugger
	 	}
	 
	 
	 function moveToLocation(lat, lng){
		    var center = new google.maps.LatLng(lat, lng);
		    mapForAnim.panTo(center);
		}
			
		function showOnMap(locationList){
			debugger
			var polyCoords=[];
			// for some reason each () is not working so using for loop. chage it after fixing issue with each ().
			for (var i = 0; i < locationList.length; i++) {
				//debugger
				if(locationList.length ==1){
					triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude}];
				}
				else if(i==0){
					triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude},{lat : locationList[i+1].latitude, lng : locationList[i+1].longitude}];
				}
				else{
					triangleCoords = [{lat: locationList[i-1].latitude, lng: locationList[i-1].longitude},{lat : locationList[i].latitude, lng : locationList[i].longitude}];
					polyCoords.push(triangleCoords);
				}
				mapForAnim.setCenter(new google.maps.LatLng(locationList[i].latitude, locationList[i].longitude));
				mapForAnim.setZoom(17); 
				drawPolyline(triangleCoords,i);
			}
			
			debugger
		}

		function drawPolyline(triangleCoords){
			 debugger
			  var bermudaTriangle = new google.maps.Polygon({
			         paths: triangleCoords,
			         strokeColor: '#FF0000',
			         strokeOpacity: 0.8,
			         strokeWeight: 3
			       });
			       bermudaTriangle.setMap(mapForAnim);
			       debugger
		}

		
		
		function getVehicles(){
			 debugger
			 
			  $("#vehicleSelectId").select2();
	    	  $('#vehicleSelectId').find('option').remove().end();
		      $('#vehicleSelectId').append('<option value="" disabled selected>-- Select Vehicle --</option>');
			 
			  $("#vehicleSelectId").append("<option value=1>1401</option>");
			  $("#vehicleSelectId").append("<option value=2>1402</option>");
			  $("#vehicleSelectId").append("<option value=3>1403</option>");
			  $("#vehicleSelectId").append("<option value=4>1404</option>");
			 
		/*	    $.get("getSCMasterData",function(response){
			    	if(response.status){	
			    	   debugger
			    	   $("#vehicleSelectId").select2();
			    	   $('#vehicleSelectId').find('option').remove().end();
				         $('#vehicleSelectId').append('<option value="" disabled selected>-- Select Vehicle --</option>');
				  		 response.data.forEach(function(item, index){  
				  			  $("#vehicleSelectId").append("<option value='" +item+ "' >" +item+ "</option>");
				  		 }); 
			    	}
				 }).fail(function(){alert("fail");});*/
		}
		
		
		
		function loadMap(mapdivId, center){
			if(center==''){
				center = {lat:17.444377, lng:78.481153}
			}
			
			 //Create a center for the map.
	 		 mapForAnim = new google.maps.Map(document.getElementById('vehiclePath_DivId'), {
	 	     center: center// mandatory other wise map will not display anything.
	 	  //   zoom: 17
	 	     });
		}
		
		
		function getLocation() {
		    if (navigator.geolocation) {
		        navigator.geolocation.getCurrentPosition(showPosition, showError);
		    } else {
		        x.innerHTML = "Geolocation is not supported by this browser.";
		    }
		}

		function showPosition(position) {
		    loggedInUserLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		}

		function showError(error) {
		    switch(error.code) {
		        case error.PERMISSION_DENIED:
		            	console.log("User denied the request for Geolocation.")
		            break;
		        case error.POSITION_UNAVAILABLE:
		            	console.log("Location information is unavailable.");
		            break;
		        case error.TIMEOUT:
		            	console.log("The request to get user location timed out.");
		            break;
		        case error.UNKNOWN_ERROR:
		            	console.log("An unknown error occurred.");
		            break;
		    }
		}
		
		function getVehicleLocations(){
			debugger
        if( $('#vehicleSelectId').val()==null || $('#vehicleSelectId').val().length <=0){
			alert("Please Select Vehicle");
			return false;
		}
	   else{
		   
		   $('#seekbar_div').show();
		   //Get Veh locations.
			getVehicleLocationHistory();
			
			debugger
	   }
		}
		
		
		//respond to manual changes in seekbar
		function seekAnimation(){
			//alert(parseInt(document.getElementById('seek-bar').value));
			//$('#locUpdatedTime').text(userLocation[parseInt(document.getElementById('seek-bar').value)].deviceTimestamp);
		}	
		
		  
	    var setTimeoutId ;
	    var alertMsg='';
	    function drawPath(){
	    	debugger
	    	
		    	//alertMsg+=document.getElementById('seek-bar').value+", "
		    	
		    	/*console.log(document.getElementById('seek-bar').value);
	    	    alert(document.getElementById('seek-bar').value);*/
	    	    
	    	    locationAsSource++;
	    	    
	    	    if(document.getElementById('seek-bar').value==userLocation.length){
	    	    	//Stop animation.
	    	    }
	    	    else{
	    	    	//continue animation.
	    	    	
			    	$('#locUpdatedTime').text(userLocation[parseInt(document.getElementById('seek-bar').value)].deviceTimestamp);
			    	//Set location time.
			    	//$('#locUpdatedTime').text(userLocation[parseInt(document.getElementById('seek-bar').value)].deviceTimestamp);
			    	
			    	//Animate marker.
			    	animateMarker();
			    	
			    	//setTimeout for drawPath method for marker animation.
			    	setTimeoutId =  setTimeout(drawPath,1000);
			    	
			    	//set the seekbar move by 1 step
			    	document.getElementById('seek-bar').stepUp(1);
	    	    }
	    	    
	    	    
	    	    
		    	
	    	
	    	
	    }
	    
	    //capture index of location for which the marker is being animated.
		var locationAsSource=0;
	    
		function animateMarker(){
			debugger
			if (locationAsSource < userLocation.length) {
				
				var result = [ userLocation[locationAsSource].latitude, userLocation[locationAsSource].longitude];
				 
				 transition(result);
//				 locationAsSource++;
				 
				 debugger
			}
			else if (locationAsSource==userLocation.length){
				//alert("Reached Destination");
				//markerA.setVisible(false);
				clearTimeout(setTimeoutId);
			}
			debugger
		}

		
	  var numDeltas = 100;
	  var delay = 40; //milliseconds
	  var i = 0;
	  var deltaLat;
	  var deltaLng;
	  
	function transition(result){
	   debugger
		i = 0;
	    deltaLat = (result[0] - position[0])/numDeltas;
	    deltaLng = (result[1] - position[1])/numDeltas;
	    moveMarker(position);
	    debugger
	}

	function moveMarker(){
		debugger
	    position[0] += deltaLat;
	    position[1] += deltaLng;
	    var latlng = new google.maps.LatLng(position[0], position[1]);
	    markerA.setPosition(latlng);
	    if(i!=numDeltas){
	        i++;
	        setTimeout(moveMarker, delay);
	        //rotate
	       /* if(locationAsSource==0){
	        	 var lastPsn = new google.maps.LatLng({lat: userLocation[locationAsSource].latitude, lng:userLocation[locationAsSource].longitude});
	 		    var nextPsn = new google.maps.LatLng({lat: userLocation[locationAsSource].latitude, lng:userLocation[locationAsSource].longitude});
	 		    rotateMarker(lastPsn,nextPsn);
	        }
	        else{
	        	 var lastPsn = new google.maps.LatLng({lat: userLocation[locationAsSource].latitude, lng:userLocation[locationAsSource].longitude});
	 		    var nextPsn = new google.maps.LatLng({lat: userLocation[locationAsSource+1].latitude, lng:userLocation[locationAsSource+1].longitude});
	 		    rotateMarker(lastPsn,nextPsn);
	        }*/
		   
	    }
	    debugger
	}

	
	 debugger
	 
	/*	//test
		   var car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
		   var icon = {
		       path: car,
		       scale: .7,
		       strokeColor: 'white',
		       strokeWeight: .10,
		       fillOpacity: 1,
		       fillColor: '#404040',
		       offset: '5%',
		       // rotation: parseInt(heading[i]),
		       anchor: new google.maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
		   };
	

		   
		  
		   function rotateMarker(lastPosn,nextPosn){
			   debugger
			  // alert();
			    var heading = google.maps.geometry.spherical.computeHeading(lastPosn, nextPosn);
			    icon.rotation = heading;
			    markerA.setIcon(icon);
		   }*/
		   
		   
////////////////////////////////////////////////////////////////////////////////////////////////////