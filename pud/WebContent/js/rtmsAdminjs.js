/**
 * 
 */
	var CONSTANTS_AJAX_TIMEOUT = 10000;
	var CONSTANTS_PUSH_APP_URL = "PUSH_APP_URL";
	var CONSTANTS_LOCK = "LOCK";
	var CONSTANTS_SCREAM = "SCREAM";
	var CONSTANTS_WIPE =  "WIPE";
	var CONSTANTS_APP_LIST = "APPS_LIST";
	var CONSTANTS_APP_UNINSTALL = "APP_UNINSTALL";
	
	
var pushconsArr = [];
var currentApkVer;
var todelRowIndex;
var pullPickupsTableObj;
var pulldeliveriesTableObj;
  var geofenceMap;




$(document).ready(function () {
	
	  $("#loctionsHistoryOfDeviceMapModal").on("shown.bs.modal", function () {
          google.maps.event.trigger(loctionsHistoryOfDeviceMap, "resize");
         /* loctionsHistoryOfDeviceMap.setCenter(latlng);
          loctionsHistoryOfDeviceMap.setZoom(17); */
      });
	
	// click of SC Vehicles.
	$('#SCVehicles_li').click(function(event){
		  $('#firstRow_div').hide();
		  $('#loggedInUsersRow').hide();
		  $('#SCVehicles_div').show();
		  $('#mdm_div').hide();
		  $('#vehiclesPath_div').hide();
		 
		  //Get the Sc codes and show them in dropdown.
		  getSCMasterData();
		 
		  //init map
		  initMap();
	  });
	
	// click of MDM. 
	$('#commands_li').click(function(event){
		  $('#firstRow_div').hide();
		  $('#loggedInUsersRow').hide();
		  $('#SCVehicles_div').hide();
		  $('#mdm_div').show();
		  $('#vehiclesPath_div').hide();
		  
		 //get all users with device id.
		  getAllUsersWithDeviceDetails();
		  
		  
		  
	  });
	
	// click of MDM. 
	$('#vehiclePath_li').click(function(event){
		  $('#firstRow_div').hide();
		  $('#loggedInUsersRow').hide();
		  $('#SCVehicles_div').hide();
		  $('#mdm_div').hide();
		  
		  $('#vehiclesPath_div').show();
		  
		  
		  $("#vehicleSelectId").select2();
		  
		 //get all users with device id.
		  getAllUsersWithDeviceDetails();
		  
		  
		  
	  });
	  $("#geofenceModal").on("shown.bs.modal", function () {
          google.maps.event.trigger(geofenceMap, "resize");
      });
	
	// click of MDM. 
	$('#geofence_li').click(function(event){
		   //open geofence modal
	   //initMapForGeofence();
		 geofenceMap = new google.maps.Map(document.getElementById('geofenceMap_DivId'), {
			 center: {lat: 17.444785, lng:78.505353},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		 geofenceMap.setCenter(new google.maps.LatLng({lat: 17.444785, lng:78.505353}));
		 $('#geofenceModal').modal('show');
		 debugger
	  });
	
	
	
	
	
	// cklose btn fo rmodal
	   $('#closeWoModelBtnId').click(function(event) {
		   debugger
		   event.preventDefault(); 
           $("#geofenceModal").modal('hide');
	   });
	
	
	var charttitle = "Today's Hourly Pickups";
	var delChartTitle = "Today's Hourly Deliveries"

//	myVar = setInterval(getCountFun, 90000);
	function getCountFun() {
		$.ajax({
			url: "loggedInUsers",
			dataType: "json",
			type: "get",
			success: function (response) {
				var log_val = $(".statvalueone").text();

				if (response.data.logList.length > Number(log_val)) {
					$(".statvalueone").text(response.data.logList.length);
					$(".statvalueone").addClass("statvalueone");
					$("#snackbar").text("A new login has been detected!");
					var x = document.getElementById("snackbar")
					x.className = "show";
					setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
				}

				else {
					$(".statvalueone").text(response.data.logList.length);
					$(".statvalueone").addClass("statvalueone");
				}

				$("#value_two").text(response.data.todaysPickups);
				$("#attemptPk").text(response.data.attemptedPickups);
				$("#successPk").text(response.data.successPickups);
				$("#pendingPk").text(response.data.pendingPickups);
				$("#value_four").text(response.data.totRegPickups);

				$("#value_three").text(response.data.todaysDeliveries);
				$("#attemptDel").text(response.data.delsAttempted);
				$("#successDel").text(response.data.delsSuccess);
				$("#pendingDel").text(response.data.delsPending);
				$("#logDiv").children().remove();
				for (var i = 0; i < response.data.auditsList.length; i++) {

					audits = '<div class=logPara>' +
						'<p><i class="fa fa-clock-o"></i> :' + response.data.auditsList[i].timeStamp + '</p>' +
						'<p><i class="fa fa-user"></i> :' + response.data.auditsList[i].userId + '</p>' +
						'<p><i class="fa fa-gears"></i> :' + response.data.auditsList[i].serviceType + '</p>' +
						'<p><i class="fa fa-reply"></i> :' + response.data.auditsList[i].serviceResponse + '</p></div>';
					$("#logDiv").append(audits);
				}
				var res_data = response.data.logList;
				var arrdata = response.data.monthlySPickupsList;
				/*hourly pickups chart begins*/
				Highcharts.chart('barChart', {
					chart: {
						type: 'column'
					},
					credits: {
						enabled: false
					},
					title: {
						text: charttitle,
						style: {
							color: 'rgb(242,88,34)',
							fontFamily: '"Source Sans Pro", sans-serif',
							fontSize: '14px'
						}
					},
					//				    subtitle: {
					//				        text: 'Source: WorldClimate.com'
					//				    },
					xAxis: {
						categories: [
							'00:00-00:59',
							'01:00-01:59',
							'02:00-02:59',
							'03:00-03:59',
							'04:00-04:59',
							'05:00-05:59',
							'06:00-06:59',
							'07:00-07:59',
							'08:00-08:59',
							'09:00-09:59',
							'10:00-10:59',
							'11:00-11:59',
							'12:00-12:59',
							'13:00-13:59',
							'14:00-14:59',
							'15:00-15:59',
							'16:00-16:59',
							'17:00-17:59',
							'18:00-18:59',
							'19:00-19:59',
							'20:00-20:59',
							'21:00-21:59',
							'22:00-22:59',
							'23:00-23:59'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Pickups Done'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
						'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: [{
						name: 'Pickups Count',
						data: arrdata
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom',
									layout: 'horizontal'
								},
								yAxis: {
									labels: {
										align: 'left',
										x: 0,
										y: -5
									},
									title: {
										text: null
									}
								},
								subtitle: {
									text: null
								},
								credits: {
									enabled: false
								}
							}
						}]
					}
				});
				/*hourly pickups chart ends*/
				var deldata = response.data.hourlyDeliveries;
				/*hourly deliveries chart begins*/
				Highcharts.chart('delChart', {
					chart: {
						type: 'column'
					},
					credits: {
						enabled: false
					},
					title: {
						text: delChartTitle,
						style: {
							color: 'rgb(242,88,34)',
							fontFamily: '"Source Sans Pro", sans-serif',
							fontSize: '14px'
						}
					},
					//				    subtitle: {
					//				        text: 'Source: WorldClimate.com'
					//				    },
					xAxis: {
						categories: [
							'00:00-00:59',
							'01:00-01:59',
							'02:00-02:59',
							'03:00-03:59',
							'04:00-04:59',
							'05:00-05:59',
							'06:00-06:59',
							'07:00-07:59',
							'08:00-08:59',
							'09:00-09:59',
							'10:00-10:59',
							'11:00-11:59',
							'12:00-12:59',
							'13:00-13:59',
							'14:00-14:59',
							'15:00-15:59',
							'16:00-16:59',
							'17:00-17:59',
							'18:00-18:59',
							'19:00-19:59',
							'20:00-20:59',
							'21:00-21:59',
							'22:00-22:59',
							'23:00-23:59'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Pickups Done'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
						'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: [{
						name: 'Deliveries Count',
						data: deldata
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom',
									layout: 'horizontal'
								},
								yAxis: {
									labels: {
										align: 'left',
										x: 0,
										y: -5
									},
									title: {
										text: null
									}
								},
								subtitle: {
									text: null
								},
								credits: {
									enabled: false
								}
							}
						}]
					}
				});
				/*hourly deliveries chart ends*/
				setTimeout(function () {
					var l_table = $('#logged_in_users_table').DataTable({
						columns: [
							{ data: 'imei' },
							{ data: 'userId' },
							{ data: 'apkVersion' },
							{ data: 'loginTime' },
							{ data: 'location' }
						],
						data: res_data,
						retrieve: true,
						fixedHeader: {
							header: true,
							footer: true
						},
						paging: false,
						searching: false,
						ordering: true,
						scrollX: true,
						"bScrollInfinite": false,
						"bScrollCollapse": false,
						"sScrollY": "150px",
						"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

					});

					l_table.clear().draw();
					l_table.rows.add(response.data.logList); // Add new data
					l_table.columns.adjust().draw(); // Redraw the DataTable
					//$("#logged_in_users_table  thead  tr  th").addClass("afterload_logs");
				}, 2000);
			},
			complete: function () {
				$("#logged_in_users_table").css({ "width": "100%", "height": "100px", "overflow-y": "scroll", "font-size": "12px", "font-family": "'Titillium Web', sans-serif", " background-color": "#1fb5ad" });
				$('body #ulist').find('.dataTables_scrollBody').addClass("scrolllist");
				$('body #ulist').find('.dataTables_info').css({ "display": "none" });
				//$("#logged_in_users_table").addClass("w3-card-2");
			}
		});
	}


	/*$.ajax({
		url: "loggedInUsers",
		dataType: "json",
		type: "get",
		success: function (response) {
			var log_val = $(".statvalueone").text();

			if (response.data.logList.length > Number(log_val)) {
				$(".statvalueone").text(response.data.logList.length);
				$(".statvalueone").addClass("statvalueone");
			}

			else {
				$(".statvalueone").text(response.data.logList.length);
				$(".statvalueone").addClass("statvalueone");
			}

			$("#value_two").text(response.data.todaysPickups);
			$("#attemptPk").text(response.data.attemptedPickups);
			$("#successPk").text(response.data.successPickups);
			$("#pendingPk").text(response.data.pendingPickups);
			$("#value_four").text(response.data.totRegPickups);
			$("#value_three").text(response.data.todaysDeliveries);
			$("#attemptDel").text(response.data.delsAttempted);
			$("#successDel").text(response.data.delsSuccess);
			$("#pendingDel").text(response.data.delsPending);
			$("#logDiv").children().remove();

			for (var i = 0; i < response.data.auditsList.length; i++) {
				audits = '<div class=logPara>' +
					'<p><i class="fa fa-clock-o"></i> : ' + response.data.auditsList[i].timeStamp + '</p>' +
					'<p><i class="fa fa-user"></i> : ' + response.data.auditsList[i].userId + '</p>' +
					'<p><i class="fa fa-gears"></i> : ' + response.data.auditsList[i].serviceType + '</p>' +
					'<p><i class="fa fa-reply"></i> : ' + response.data.auditsList[i].serviceResponse + '</p></div>';
				$("#logDiv").append(audits);
			}

			var res_data = response.data.logList;
			var arrdata = response.data.monthlySPickupsList;
			hourly pickups start
			Highcharts.chart('barChart', {
				chart: {
					type: 'column'
				},
				credits: {
					enabled: false
				},
				title: {
					text: charttitle,
					style: {
						color: 'rgb(242,88,34)',
						fontFamily: '"Source Sans Pro", sans-serif',
						fontSize: '14px'
					}
				},
				//			    subtitle: {
				//			        text: 'Source: WorldClimate.com'
				//			    },
				xAxis: {
					categories: [
						'00:00-00:59',
						'01:00-01:59',
						'02:00-02:59',
						'03:00-03:59',
						'04:00-04:59',
						'05:00-05:59',
						'06:00-06:59',
						'07:00-07:59',
						'08:00-08:59',
						'09:00-09:59',
						'10:00-10:59',
						'11:00-11:59',
						'12:00-12:59',
						'13:00-13:59',
						'14:00-14:59',
						'15:00-15:59',
						'16:00-16:59',
						'17:00-17:59',
						'18:00-18:59',
						'19:00-19:59',
						'20:00-20:59',
						'21:00-21:59',
						'22:00-22:59',
						'23:00-23:59'
					],
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Pickups Done'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [{
					name: 'Pickups Count',
					colorByPoint: true,
					data: arrdata
				}],
				responsive: {
					rules: [{
						condition: {
							maxWidth: 900
						},
						chartOptions: {
							legend: {
								align: 'center',
								verticalAlign: 'bottom',
								layout: 'horizontal'
							},
							yAxis: {
								labels: {
									align: 'left',
									x: 0,
									y: -5
								},
								title: {
									text: null
								}
							},
							subtitle: {
								text: null
							},
							credits: {
								enabled: false
							}
						}
					}]
				}
			});
			hourly pickups end
			var deldata = response.data.hourlyDeliveries;
			hourly deliveries chart begins
			Highcharts.chart('delChart', {
				chart: {
					type: 'column'
				},
				credits: {
					enabled: false
				},
				title: {
					text: delChartTitle,
					style: {
						color: 'rgb(242,88,34)',
						fontFamily: '"Source Sans Pro", sans-serif',
						fontSize: '14px'
					}
				},
				//				    subtitle: {
				//				        text: 'Source: WorldClimate.com'
				//				    },
				xAxis: {
					categories: [
						'00:00-00:59',
						'01:00-01:59',
						'02:00-02:59',
						'03:00-03:59',
						'04:00-04:59',
						'05:00-05:59',
						'06:00-06:59',
						'07:00-07:59',
						'08:00-08:59',
						'09:00-09:59',
						'10:00-10:59',
						'11:00-11:59',
						'12:00-12:59',
						'13:00-13:59',
						'14:00-14:59',
						'15:00-15:59',
						'16:00-16:59',
						'17:00-17:59',
						'18:00-18:59',
						'19:00-19:59',
						'20:00-20:59',
						'21:00-21:59',
						'22:00-22:59',
						'23:00-23:59'
					],
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Pickups Done'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [{
					name: 'Deliveries Count',
					data: deldata
				}],
				responsive: {
					rules: [{
						condition: {
							maxWidth: 900
						},
						chartOptions: {
							legend: {
								align: 'center',
								verticalAlign: 'bottom',
								layout: 'horizontal'
							},
							yAxis: {
								labels: {
									align: 'left',
									x: 0,
									y: -5
								},
								title: {
									text: null
								}
							},
							subtitle: {
								text: null
							},
							credits: {
								enabled: false
							}
						}
					}]
				}
			});
			hourly deliveries chart ends
			var l_table = $('#logged_in_users_table').DataTable({
				columns: [
					{ data: 'imei' },
					{ data: 'userId' },
					{ data: 'apkVersion' },
					{ data: 'loginTime' },
					{ data: 'location' }
				],
				data: res_data,
				retrieve: true,
				fixedHeader: {
					header: true,
					footer: true
				},
				paging: false,
				searching: false,
				ordering: true,
				scrollX: true,
				"bScrollInfinite": false,
				"bScrollCollapse": false,
				"sScrollY": "150px",
				"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

			});

			l_table.clear().draw();
			//$("#logged_in_users_table  thead  tr  th").addClass("afterload_logs");
			l_table.rows.add(response.data.logList); // Add new data
			l_table.columns.adjust().draw(); // Redraw the DataTable
		},
		complete: function () {
			$("#logged_in_users_table").css({ "height": "100px", "overflow-y": "scroll", "font-size": "12px", "font-family": "'Titillium Web', sans-serif", " background-color": "#1fb5ad" });
			$('body #ulist').find('.dataTables_scrollBody').addClass("scrolllist");
			$('body #ulist').find('.dataTables_info').css({ "display": "none" });
			//$("#logged_in_users_table").addClass("w3-card-2");
		}
	});*/


	function format(d) {
		// `d` is the original data object for the row
		return '<div style="background-color:rgb(245,245,245);border-bottom:solid 1px rgb(242,88,34);color:grey;padding:5px;">'
			+ '<p>Con count : ' + d.conCount + '</p>'
			+ '<p>Pieces count : ' + d.pieceCount + '</p>'
			+ '</div>';
	}

	function drawRowDetails(tr, row) {
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	}

	$("#reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#del_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#reqBdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#bdel_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#pkSumDate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#dsum_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#pushPKDate").datepicker({
		dateFormat: 'dd/mm/yy'
	});
	var d = new Date();
	var lDate = moment(d).format("DD/MM/YYYY");

	$("#pushPKDate").val(lDate);


	$(".loginlist").click(function () {
		var child = $(".nav-pills").children(':eq(1)');
		var child_one = child.children(':eq(1)');
		for (var i = 0; i < child_one.children().length; i++) {
			var re_child = child_one.children(':eq(' + i + ')');
			re_child.removeClass("active");
			/*collapsing dropdown*/
			$("#ddwn").addClass("collapsed");
			$("#ddwn").attr("aria-expanded", "false");
			$("#pdlist").removeClass("collapse in");
			$("#pdlist").addClass("collapse");
		}
	});
	/*end*/

	/*removing active class for tab-pills*/
	$("#pdlist").click(function () {
		var child = $(".nav-pills").children(':eq(0)');
		child.removeClass("active");
	});
	/*end*/

	/*----------pickups info begins--------------*/
	$("#getdata").click(function () {

		if ($("#reqdate").val() == "") {
			alert("Please enter date");
		}
		else {

			$.ajax({
				url: "getPickupsDataReport",
				dataType: "json",
				data: "&reqDate=" + $("#reqdate").val() + "&cusCode=" + $("#cuscode").val() + "&userName=" + $("#username").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						debugger;
						document.getElementById("msg_para").style.display = 'none';
						$("#example").css({ "display": "block" });

						var d_table = $('#example').DataTable({

							columns: [
								{
									"className": 'details-control',
									"orderable": false,
									"data": null,
									"defaultContent": ''
								},
								{ data: 'pickUpScheduleId' },
								{ data: 'customerCode' },
								{ data: 'customerName' },
								{ data: 'userId' },
								{ data: 'status' }
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						d_table.clear().draw();
						d_table.rows.add(response.data); // Add new data
						d_table.columns.adjust().draw(); // Redraw the DataTable

						$('#example tbody .details-control').click(function () {
							var tr = $(this).closest('tr');
							var row = d_table.row(tr);

							drawRowDetails(tr, row);


						});

					}
					else {
						$("#msg_para").text(response.message);
						document.getElementById("msg_para").style.display = 'block';
						$("#example").css({ "display": "none" });

					}


				},
				complete: function () {
					$('body #demotab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*-----------------pickups info end------------- */

	/*-----------------Device pickups Push Clicked------------- */
	
	$("#sendPickupPush").click(function () {
		
		

		if ($("#pickupScheduleId").val() == ""||$("#pickupUsername").val() == "") {
			alert("Please enter All Values");
		}else{
			
			$.ajax({
				url: "sendPushAndGetDeviceData",
				dataType: "json",
				data: "&username=" + $("#pickupUsername").val() + "&status=2" + "&pickupScheduleId=" + $("#pickupScheduleId").val(),
				type: "get",
				success: function (response) {
                        var username=$("#pickupUsername").val();
                        var pickupScheduleId=$("#pickupScheduleId").val();
					if (response.status == true) {
						debugger;var i=0;
						$("#pickuppush_msg_para").text(response.message);
						var eventSource = new EventSource("pickupUpdationServiceV2?userId="+username+"&pickupScheduleId="+$("#pickupScheduleId").val());
						
						eventSource.onmessage = function(event) {
						debugger;
						console.log("event.data "+event.data);
						if(event.data=="Pickup is in progress" || "Pickup is pending"){
							event.target.close();
							$("#pickuppush_msg_para").text(event.data);
							$("#pickupDataTable").css({ "display": "none" });
						}else if(!(event.data=="Not Yet Found")){
							$("#pickupPushPKLoader").css({ "display": "none" });
						var data=JSON.parse(event.data);
						
							event.target.close();
							console.log("data "+data);
						   $("#pickuppush_msg_para").text("Thank You For Waiting.Your data is received");
						   document.getElementById("pickupDataTable").style.display = 'block';
							var insert="";var conEntryId="";
						   $.each(data.conDetails,function(key,val){
							   conEntryId=val.conEntryId;
								insert+='<tr><td>'+val.conNumber+'</td><td>'+val.pickupScheduleId+'</td><td>'+val.userName+'</td><td>'+val.orderNo+'</td><td>'+val.pickupDate+'</td><td>'+val.noOfPackage+'</td><td>'+val.customerCode+'</td><td>'+val.paymentBasis+'</td><td>'+val.originPinCode+'</td><td>'+val.destinationPinCode+'</td><td>'+val.erpUpdated+'</td></tr>';
							   /* if(val.erpUpdated==1){
							    	insert+='<td>'+"Updated to ERP"+'</td></tr>';
							    }  else{
							    	insert+='<td><button class="btn btn-primary" onclick=pushToErp('+val.conEntryId+')>Push To Erp</button></td></tr>';
							    }*/
						   });
						   $("#devicePickupsTableTbody").html(insert);
							pullPickupsTableObj = $('#devicePickupsTable').DataTable({
							
								/*columns: [
									{ data: 'conNumber' },
									{ data: 'pickupScheduleId' },
									{ data: 'userName' },
									{ data: 'orderNo' },
									{ data: 'pickupDate' },
									{ data: 'noOfPackage' },
									{ data: 'customerCode' },
									{ data: 'paymentBasis' },
									{ data: 'originPinCode' },
									{ data: 'destinationPinCode' },
									{
										
										data: null,
										defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
									}

								],*/
								
								retrieve: true,
								fixedHeader: {
									header: true,
									footer: true
								},
								paging: true,
								searching: true,
								ordering: true,
								scrollX: true,
								"bScrollInfinite": false,
								"bScrollCollapse": false,
								"sScrollY": "200px",
								"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

							});

							
						   
						}else{
							$("#pickupPushPKLoader").css({ "display": "block" });
							i++;
							console.log("i value "+i);
							$("#pickupDataTable").css({ "display": "none" });
							if(i==60){
								$("#pickupPushPKLoader").css({ "display": "none" });
								event.target.close();
								 $("#pickuppush_msg_para").text("Sorry,We Cannot Pull Your Data..Please Try Later");
								 
							}
							
						}
						}
						document.getElementById("pickuppush_msg_para").style.display = 'block';
                     }
					else {
						debugger;
						$("#pickupDataTable").css({ "display": "none" });
						$("#pickuppush_msg_para").text(response.message);
						document.getElementById("pickuppush_msg_para").style.display = 'block';
						//$("#example").css({ "display": "none" });

					}


				}
				
			});
			
			
		}
		
		
	});
	
	
/*-----------------Device Deliveries Push Clicked------------- */
	
	$("#sendDeliveryPush").click(function () {
		
		

		if ($("#conNumber").val() == ""||$("#deliveryUsername").val() == "") {
			alert("Please enter All Values");
		}else{
			
			$.ajax({
				url: "sendPushAndGetDeviceData",
				dataType: "json",
				data: "&username=" + $("#deliveryUsername").val() + "&status=1" + "&conNumber=" + $("#conNumber").val() + "&pdcNumber=" + $("#pdcNumber").val(),
				type: "get",
				success: function (response) {
                        var username=$("#deliveryUsername").val();
                        var pdcNumber=$("#pdcNumber").val();
					if (response.status == true) {
						debugger;
						var i=0;
						$("#deliverypush_msg_para").text(response.message);
						
						var eventSource = new EventSource("deliveryUpdationServiceV2?userId="+username+"&pdcNumber="+$("#pdcNumber").val()+"&conNumber="+$("#conNumber").val());
						
						eventSource.onmessage = function(event) {
							debugger;
							console.log("event.data "+event.data);
						if(event.data=="Delivery is Not Yet Updated"){
							event.target.close();
							$("#deliveryPushPKLoader").css({ "display": "none" });
							$("#deliveryDataTable").css({ "display": "none" });
							$("#deliverypush_msg_para").text("Delivery is Not Yet Updated In Device");
							$("#deliveryDataTable").css({ "display": "none" });
							
						}else if(!(event.data=="Not Yet Found")){
						var data=JSON.parse(event.data);
						$("#deliveryPushPKLoader").css({ "display": "none" });
							event.target.close();
							console.log("data "+data);
						   $("#deliverypush_msg_para").text("Thank You For Waiting.Your data is received");
						   
						   document.getElementById("deliveryDataTable").style.display = 'block';
							var insert="";
						   $.each(data.deliveryEntity,function(key,val){
							   conEntryId=val.conEntryId;
								insert+='<tr><td>'+val.awbNo+'</td><td>'+val.transactionDate+'</td><td>'+val.deliveredTo+'</td><td>'+val.status+'</td><td>'+val.fieldEmployeeName+'</td><td>'+val.pdcNumber+'</td><td>'+val.receiverMobileNo+'</td><td>'+val.erpUpdated+'</td></tr>';
							   /* if(val.erpUpdated==1){
							    	insert+='<td>'+"Updated to ERP"+'</td></tr>';
							    }  else{
							    	insert+='<td><button class="btn btn-primary" onclick=pushToErp('+val.conEntryId+')>Push To Erp</button></td></tr>';
							    }*/
						   });
						   $("#devicedeliveriesTableTbody").html(insert);
							pulldeliveriesTableObj = $('#devicedeliveriesTable').DataTable({
							
								/*columns: [
									{ data: 'conNumber' },
									{ data: 'pickupScheduleId' },
									{ data: 'userName' },
									{ data: 'orderNo' },
									{ data: 'pickupDate' },
									{ data: 'noOfPackage' },
									{ data: 'customerCode' },
									{ data: 'paymentBasis' },
									{ data: 'originPinCode' },
									{ data: 'destinationPinCode' },
									{
										
										data: null,
										defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
									}

								],
								*/
								retrieve: true,
								fixedHeader: {
									header: true,
									footer: true
								},
								paging: true,
								searching: true,
								ordering: true,
								scrollX: true,
								"bScrollInfinite": false,
								"bScrollCollapse": false,
								"sScrollY": "200px",
								"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

							});

							//$("#deliverypush_msg_para").css({ "display": "none" });
						   
						}else{
							$("#deliveryPushPKLoader").css({ "display": "block" });
							 $("#deliveryDataTable").css({ "display": "none" });
							i++;
							console.log("i value "+i);
							if(i==60){
								$("#deliveryPushPKLoader").css({ "display": "none" });
								event.target.close();
								 $("#deliverypush_msg_para").text("Sorry,We Cannot Pull Your Data..Please Try Later");
								
							}
							
						}
						
						}
						document.getElementById("deliverypush_msg_para").style.display = 'block';
                     }
					else {
						debugger;
						$("#deliverypush_msg_para").text(response.message);
						document.getElementById("deliverypush_msg_para").style.display = 'block';
						$("#deliveryDataTable").css({ "display": "none" });
						//$("#example").css({ "display": "none" });

					}


				}
				
			});
			
			
		}
		
		
	});
	


	/*---------------force logout user js-------------------*/
	$("#logoutbtn").click(function () {
		if ($("#userName").val() == "") {
			alert("Please enter username!");
		}
		else {
			$.ajax({
				url: "logoutUser",
				data: "&userName=" + $("#userName").val(),
				dataType: "json",
				type: "get",
				beforeSend: function () {

					$.blockUI.defaults.css = {};
					$.blockUI({ message: '<div class="loader"></div><p style="color:white;font-weight:bold;">Please wait</p>' });
				},
				success: function (response) {
					$.unblockUI();
					if (response.status == true) {
						$("#snackbar").text("User logged out successfully.");
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
					}
					else {
						$("#snackbar").text(response.message);
						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
					}
				}

			});
		}
	});
	/*force logout user end */

	/*deliveries info*/

	$("#del_getdata").click(function () {
		if ($("#del_reqdate").val() == "") {
			alert("Please enter date");
		}
		else {
			$.ajax({
				url: "getDeliveriesDataReport",
				dataType: "json",
				data: "&reqDate=" + $("#del_reqdate").val() + "&agent=" + $("#del_username").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("del_msg_para").style.display = 'none';
						$("#del_info_table").css({ "visibility": "visible" });
						var del_table = $('#del_info_table').DataTable({
							columns: [
								{ data: 'conNo' },
								{ data: 'pdc' },
								{ data: 'Status' },
								{ data: 'createdTime' },
								{ data: 'assignedTo' },
								{ data: 'consigneeCode' },
								{ data: 'consignorAddress' },
								{ data: 'consigneeAddress' }
							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						del_table.clear().draw();
						del_table.rows.add(response.data); // Add new data
						del_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#del_msg_para").text(response.message);
						document.getElementById("del_msg_para").style.display = 'block';
						$("#del_info_table").css({ "visibility": "hidden" });
						$("#del_info_table").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #del_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});

	/*deliveries info end*/

	/*--------------branch pickups report------------*/
	$("#getBpks").click(function () {

		if ($("#reqBdate").val() == "") {
			alert("Please enter date");
		}
		else if ($("#branchCode").val() == "") {
			alert("Please enter branch code");
		}
		else {

			$.ajax({
				url: "getBranchPickups",
				dataType: "json",
				data: "&reqDate=" + $("#reqBdate").val() + "&branchCode=" + $("#branchCode").val() + "&userName=" + $("#userBname").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("brmsg_para").style.display = 'none';
						$("#branchPT").css({ "display": "block" });

						var dbr_table = $('#branchPT').DataTable({

							columns: [
								{
									"className": 'details-control',
									"orderable": false,
									"data": null,
									"defaultContent": ''
								},
								{ data: 'pickUpScheduleId' },
								{ data: 'customerCode' },
								{ data: 'customerName' },

								{ data: 'userId' },
								{ data: 'status' }
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						dbr_table.clear().draw();
						dbr_table.rows.add(response.data); // Add new data
						dbr_table.columns.adjust().draw(); // Redraw the DataTable

						$('#branchPT tbody .details-control').click(function () {
							var tr = $(this).closest('tr');
							var row = dbr_table.row(tr);

							drawRowDetails(tr, row);
						});

					}
					else {
						$("#brmsg_para").text(response.message);
						document.getElementById("brmsg_para").style.display = 'block';
						$("#branchPT").css({ "display": "none" });

					}
				},
				complete: function () {
					$('body #bpkTab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*--------------branch pickups report end------------*/

	/* branch deliveries report*/
	$("#bdel_data").click(function () {
		if ($("#bdel_reqdate").val() == "") {
			alert("Please enter date");
		}

		else if ($("#branchdelCode").val() == "") {
			alert("please enter branch code");
		}
		else {
			$.ajax({
				url: "getBranchDeliveries",
				dataType: "json",
				data: "&reqDate=" + $("#bdel_reqdate").val() + "&agent=" + $("#bdel_username").val() + "&branchCode=" + $("#branchdelCode").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("bdel_msg").style.display = 'none';
						$("#bdel_table").css({ "visibility": "visible" });
						var bdel_table = $('#bdel_table').DataTable({
							columns: [
								{ data: 'conNo' },
								{ data: 'pdc' },
								{ data: 'Status' },
								{ data: 'createdTime' },
								{ data: 'assignedTo' },
								{ data: 'consigneeCode' },
								{ data: 'consignorAddress' },
								{ data: 'consigneeAddress' }
							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						bdel_table.clear().draw();
						bdel_table.rows.add(response.data); // Add new data
						bdel_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#bdel_msg").text(response.message);
						document.getElementById("bdel_msg").style.display = 'block';
						$("#bdel_table").css({ "visibility": "hidden" });
						$("#bdel_table").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #bdel_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});
	/*bracnch deliveries report end*/

	/*branch pickups summmary*/
	$("#getPkSum").click(function () {

		if ($("#pkSumDate").val() == "") {
			alert("Please enter date");
		}
		else {

			$.ajax({
				url: "getBranchPickupsSummary",
				dataType: "json",
				data: "&reqDate=" + $("#pkSumDate").val(),
				type: "get",
				beforeSend: function () {
					document.getElementById('loaderDiv').style.display = 'block';
				},
				success: function (response) {
					document.getElementById('loaderDiv').style.display = 'none';
					if (response.status == true) {
						document.getElementById("pkSumMsg").style.display = 'none';
						$("#pkSumTable").css({ "display": "block" });

						var psum_table = $('#pkSumTable').DataTable({

							columns: [
								{ data: 'branch' },
								{ data: 'pickupSuccessCount' },
								{ data: 'attemptedPickups' },
								{ data: 'pendingCount' },
								{ data: 'totalPickups' },
								{
									data: 'productivity',
									render: function (data, type, row) {
										return data.toFixed(2) + '%'
									}
								}
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						psum_table.clear().draw();
						psum_table.rows.add(response.data); // Add new data
						psum_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#pkSumMsg").text(response.message);
						document.getElementById("pkSumMsg").style.display = 'block';
						$("#pkSumTable").css({ "display": "none" });

					}
				},
				complete: function () {
					$('body #pkSumTab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*branch pickups summary ends*/

	/*branch deliveries summary */
	$("#getDelSum").click(function () {
		if ($("#dsum_reqdate").val() == "") {
			alert("Please enter date");
		}

		else {
			$.ajax({
				url: "getBranchDeliveriesSummary",
				dataType: "json",
				data: "&reqDate=" + $("#dsum_reqdate").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("delSum_msg").style.display = 'none';
						$("#delSumTable").css({ "visibility": "visible" });
						var delsum_table = $('#delSumTable').DataTable({
							columns: [
								{ data: 'branch' },
								{ data: 'totalPickups' },
								{ data: 'pickupSuccessCount' },
								{ data: 'attemptedPickups' },
								{ data: 'pendingCount' },
								{
									data: 'productivity',
									render: function (data, type, row) {
										return data.toFixed(2) + '%'
									}
								},

							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						delsum_table.clear().draw();
						delsum_table.rows.add(response.data); // Add new data
						delsum_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#delSum_msg").text(response.message);
						document.getElementById("delSum_msg").style.display = 'block';
						$("#delSumTable").css({ "visibility": "hidden" });
						$("#delSumTable").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #delSum_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});

	/*branch deliveries summary ends */

	// "render": function (data, type, row, meta) {
	// 	return type === 'display' && data.length > 25 ?
	// 		'<span title="' + data + '">' + data.substr(0, 20) + '...</span>' :
	// 		data;
	// }

	/*refreshing user and docket series data*/

	$(".cronLink").click(function () {
		$.ajax({
			url: "getDocketSeriesData",
			type: "get",
			dataType: "json",
			beforeSend: function () {
				$("#spinRow").css({
					"display": "block"
				});
			},
			success: function (response) {
				$("#spinRow").css({
					"display": "none"
				});

				$("#snackbar").text(response.message);
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
			}
		})
	})

	$(".cronLinkuser").click(function () {
		$.ajax({
			url: "userData",
			type: "get",
			dataType: "json",
			beforeSend: function () {
				$("#spinRow").css({
					"display": "block"
				});
			},
			success: function (response) {
				$("#spinRow").css({
					"display": "none"
				});

				$("#snackbar").text(response.message);
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
			}
		})
	})
	/*refreshing user and docket series data ends*/

	/*get open pickups */
	$("#getPushPickupsBtn").click(function () {
		currentApkVer = "";
		if (openPkConsData != null) {
			openPkConsData.clear().draw();
			
			$("#pushConsBtn").prop('disabled', true);
			$("#openPickupId").text("Schedule ID : ");
		}

		// console.log(ldate);
		if ($("#pushPKDate").val() == "") {
			alert("please enter date");
		}
		else if ($("#pushPKName").val() == "") {
			alert("Please enter username");
		}
		else {
			var fdate = $("#pushPKDate").val();
			var sdate = moment(fdate, "DD/MM/YYYY");
			var ldate = moment(sdate).format("YYYY-MM-DD");
			// console.log(ldate);
			console.log("before getting open pickups :" + currentApkVer);
			$.ajax({
				url: "getOpenPickupsOfUser",
				type: "get",
				dataType: "json",
				data: "&pickupDate=" + ldate + "&userName=" + $("#pushPKName").val(),
				beforeSend: function () {
					$("#pushPKLoader").css({ "display": "block" });
				},
				success: function (response) {
					$("#pushPKLoader").css({ "display": "none" });
					if (response.status == true) {
						$("#openPickupsDiv").css({ "visibility": "visible" });
						$("#openPickupsConDiv").css({ "visibility": "visible" });
						$("#pushPksResponse").css({ "display": "none" });
						currentApkVer = response.data[0].currentApkVersion;
						// console.log("after getting open pickups :" + currentApkVer);
						pushPickupsTableObj = $('#openPickupsTable').DataTable({
							columns: [
								{ data: 'pickupScheduleId' },
								{ data: 'pickupOrderNumber' },
								{ data: 'pickupDate' },
								{ data: 'userName' },
								{
									data: null,
									defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
								}

							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "200px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						pushPickupsTableObj.clear().draw();
						pushPickupsTableObj.rows.add(response.data); // Add new data
						pushPickupsTableObj.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#openPickupsDiv").css({ "visibility": "hidden" });
						$("#openPickupsConDiv").css({ "visibility": "hidden" });
						// console.log(response);
						$("#pushPksResponse").text(response.message);
						$("#pushPksResponse").css({ "display": "block" });
					}
				}

			});
		}
	});
	/*get open pickups ends */
	/*pushing cons */
	$("#pushConsBtn").click(function () {
		// console.log(openPkConsData.rows().data());
		//console.log("push cons table length :"+openPkConsData.rows().data().length);
		//console.log("on push cons :"+currentApkVer);
		$("#pushedConsResponse").css({ "display": "none" });
		$("#pushedConsResponse").text("");

		for (i = 0; i < openPkConsData.rows().data().length; i++) {
			pushconsArr.push(openPkConsData.row(i).data());
		}

		if (pushconsArr.length > 0) {

			var conObj = { conDetails: pushconsArr };

			var finalObj = JSON.stringify(conObj);

			$.ajax({
				url: "pickupUpdationService",
				headers: { 'apkVersion': currentApkVer },
				type: "post",
				dataType: "json",
				data: finalObj,
				beforeSend: function () {
					$("#pushingConsLoader").text("Closing pickup...");
					$("#pushingConsLoader").css({ "display": "block" });
				},
				success: function (response) {
					$("#pushingConsLoader").css({ "display": "none" });
					// $("#pushConsBtn").prop('disabled', true);
					// $("#pushedConsResponse").text(response.message);
					// $("#pushedConsResponse").css({ "display": "block" });
					if (response.status == true) {

						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
						// pushPickupsTableObj.row().remove().draw();
						// console.log(pushPickupsTableObj.row(todelRowIndex).data());
						pushPickupsTableObj.rows(todelRowIndex).remove().draw();
						openPkConsData.clear().draw();

					}
					else {
						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);

					}
				}
			});
		}
		else {
			alert("No pickups to close");
		}

	});
	/*pushing cons ends */

});


function getOpenCons(alpha) {
	// console.log($(alpha).closest('tr').children().length);
	var rowIndex = $(alpha).closest('tr').index();
	todelRowIndex = rowIndex;
	// console.log(rowIndex);
	var rowData = pushPickupsTableObj.row(rowIndex).data();
	var foxtrot = pushPickupsTableObj.rows().data();

	// console.log(pushPickupsTableObj.cell( rowIndex, 5 ).data());
	// console.log(rowData.pickupScheduleId);
	if (rowData.pickupScheduleId == "" && rowData.userName == "") {
		alert("Incomplete Pickup details to get cons");
	}
	else {
		$.ajax({
			url: "getOpenPickupsCons",
			type: "get",
			dataType: "json",
			data: "&pickupScheduleId=" + rowData.pickupScheduleId + "&userName=" + rowData.userName,
			beforeSend: function () {
				$("#getpidCon").text(rowData.pickupScheduleId);
				$("#pushPKconLoader").css({ "display": "block" });
			},
			success: function (response) {
				$("#getpidCon").text("");
				$("#pushPKconLoader").css({ "display": "none" });


				if (response.status == true) {
					// for(i=0;i<response.data.conDetails.length;i++)
					// {
					// 	consArr.push(response.data.conDetails[i]);
					// }
					// console.log(JSON.stringify(consArr));
					$("#openPickupsConDiv").css({ "visibility": "visible" });
					$("#pushConsBtn").prop('disabled', false);
					pushconsArr = [];
					$("#openPickupId").text("Schedule ID : " + rowData.pickupScheduleId);
					openPkConsData = $("#openPkConsTable").DataTable({
						columns: [
							{ data: 'conNumber' },
							{ data: 'orderNo' },
							{ data: 'noOfPackage' },
							{ data: 'pickupDate'}
						],
						data: response.data.conDetails,
						retrieve: true,
						destroy: true,
						fixedHeader: {
							header: true,
							footer: true
						},
						paging: true,
						searching: true,
						ordering: true,
						scrollX: true,
						"bScrollInfinite": false,
						"bScrollCollapse": false,
						"sScrollY": "150px",
						"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]
					});

					openPkConsData.clear().draw();
					openPkConsData.rows.add(response.data.conDetails); // Add new data
					openPkConsData.columns.adjust().draw(); // Redraw the DataTable
				}
			}
		});
	}
}


function pushToErp(conEntryId){
	
}

function getSCMasterData(){
	 debugger
	    $.get("getSCMasterData",function(response){
	    	if(response.status){	
	    	   debugger
	    	   $("#scSelectId").select2();
	    	   $('#scSelectId').find('option').remove().end();
		         $('#scSelectId').append('<option value="" disabled selected>-- Select SC --</option>');
		  		 response.data.forEach(function(item, index){  
		  			  $("#scSelectId").append("<option value='" +item+ "' >" +item+ "</option>");
		  		 }); 
	    	}
		 }).fail(function(){alert("fail");});
}

var  scVehicles,map;
function getSCWiseVehicles(){
	  if( !stringIsEmpty($('#scSelectId option:selected').val())){
		   debugger
	 	       $.ajax({
	 	         url : 'rtmsVehicleInfo',data: "&scCode=" +$('#scSelectId option:selected').val(), dataType : "json", contentType:"application/json", type : 'get', clearForm : true,
	 			 beforeSend : function(xhr){ $.blockUI({ message : "Please wait..." });  return true; },
	 			 success : function(response) {
	 			                 if (response.status) {
	 			                	 debugger
	 			                	 if(response.data!=null && response.data.length>0){
	 			                		 // capture list of vehicles for test.
	 	 			                //	scVehicles=response.data;
	 	 			                	ploVehiclesOnMap(response.data,map);
	 			                	 }
	 			                	 else{
	 			                		 alert("No Vehicles available");
	 			                		 //Clear the map
	 			                		$('#scVehiclesCount_font').text($('#scSelectId option:selected').val()+" ( Vehicle = 0)");
	 			                	 }
	 			                 $.unblockUI();
	 			            }
	 			 },
	 			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError);  $.unblockUI();}
	 		  });
	   }
	  else{
		  alert("Please Select SC");
		  return false;
	  }
}

function initMap(){
	 debugger
    //Create a center for the map.
	/*map = new google.maps.Map(document.getElementById('map_DivId'), {
    center: {lat: scVehicles[0].latitude, lng:scVehicles[0].longitude},// mandatory other wise map will not dispaly anything.
    zoom: 17
    });*/
	
	 //Test
	 map = new google.maps.Map(document.getElementById('map_DivId'), {
		    center: {lat: 17.444785, lng:78.505353},// mandatory other wise map will not dispaly anything.
		    zoom: 17
		    });
	
	/* //Create the marker for the first location.
	  marker =  new google.maps.Marker({
         position : {lat: scVehicles[0].latitude, lng:scVehicles[0].longitude},
         map : map
     });
	 marker.setMap( map ); 
	
	 //Create a position array which always contains first location lat and lng. Position arrary is used for the marker animation.
	 position=[scVehicles[0].latitude,scVehicles[0].longitude ];
	
	 //Create a path on the 
	 showOnMap(scVehicles);
	
	 //Clear the var which holds the location to which the marker is beign animated
	 locationAsSource=0;
	 
	 
	 
	 //USed for direcs api
	 
	   markerArray = [];
	 
	  // Instantiate a directions service.
   directionsService = new google.maps.DirectionsService;
	 
	  // Create a renderer for directions and bind it to the map.
    directionsDisplay = new google.maps.DirectionsRenderer({map: map});
	 
   // Instantiate an info window to hold step text.
    stepDisplay = new google.maps.InfoWindow;
   */
	 debugger
	 //ploVehiclesOnMap(scVehicles,map);
}


function ploVehiclesOnMap(scVehicles,map){
	debugger
	//Create the marker for the first location.
/*	 marker =  new google.maps.Marker({
     position :{lat: 17.444785, lng:78.505353},
     map : map,
       icon : "images/truck2.jpg"
     });
     marker.setMap( map ); 
	 infoWindowForPointer = new google.maps.InfoWindow({ 
	       content:  
                 '<div class="modal-dialog modal-md">'+
                   ' <div class="modal-content" >'+
                     '     <div class="modal-header">'+
                      '      <h4 class="modal-title" id="createSPModalHead" align="center">Info</h4>'+
                        '</div>'+ 
                       ' <div class="modal-body" style="width:100%;">'+
                       ' </div>'+
                        '</div>'+
                        '</div>'+
            '</div>'
	 });
     
     marker =  new google.maps.Marker({
     position :{lat: 17.448956, lng:78.507356},
     map : map,
       icon : "images/truck2.jpg"
     });
     marker.setMap( map ); 
	 infoWindowForPointer = new google.maps.InfoWindow({ 
	    
	       content:  
	               ' <div >'+
	                 '     <div >'+
	                  '      <h4 class="modal-title" id="createSPModalHead" align="center">Info</h4>'+
	                    '</div>'+ 
	                   ' <div class="modal-body" style="width:100%;">'+
	                   ' </div>'+
	                    '</div>'+
	        '</div>'
	 });
     
     marker =  new google.maps.Marker({
     position :{lat:17.441122, lng: 78.485873},
     map : map,
       icon : "images/truck2.jpg"
     });
     marker.setMap( map ); 
	 infoWindowForPointer = new google.maps.InfoWindow({ 
		  content:  
              ' <div >'+
                 '<label class="modal-title" id="createSPModalHead" align="center" style="color:blue;"><u>Info</u></label><br>'+
                 '<label class="modal-title" id="createSPModalHead" align="center">Vehicle : </label><br>'+
                 '<label class="modal-title" id="createSPModalHead" align="center">Vendor : </label><br>'+
                 '<label class="modal-title" id="createSPModalHead" align="center">User : </label><br>'+
                 '<label class="modal-title" id="createSPModalHead" align="center">Pickups : </label><br>'+
                 '<label class="modal-title" id="createSPModalHead" align="center">Deliveries : </label><br>'+
               '</div>'
	 });
	
           
   google.maps.event.addListener(marker, 'mouseover', function() {
    	debugger
        infoWindowForPointer.open(map,marker);
    });
    google.maps.event.addListener(marker, 'mouseout', function() {
    	debugger
        infoWindowForPointer.close();
    });*/
	
     $('#scVehiclesCount_font').text($('#scSelectId option:selected').val()+" ( Vehicle Count : "+scVehicles.length+")");
	//Set the center of the map to the first vehicle location.
	 map.setCenter(new google.maps.LatLng(scVehicles[0].currentLocationLatitude,scVehicles[0].currentLocationLongitude));
	 var markersToFitBound=[];
	 var bounds = new google.maps.LatLngBounds();
	 $.each( scVehicles, function( key, value ) {
	        	 debugger
	        	 //Create a marker.
	        	  marker =  new google.maps.Marker({
	        	     position :{lat:value.currentLocationLatitude, lng:value.currentLocationLongitude},
	        	     map : map,
	        	     icon : "images/truck3.jpg"
	        	  });
	        	  marker.setMap( map ); 
	        	
	        	  bounds.extend(marker.getPosition());
	        	  
	        	  
        		 // Create a info Window for marker.
	        	  debugger
        		  infoWindow = new google.maps.InfoWindow();
        		  content= 
    	              ' <div >'+
    	                 '<label  style="color:blue;"><u>VEHICLE INFO</u></label><br>'+
    	                 '<label style="color:#D43F08;" >Vehicle : '+value.vehicleId+', Capacity = '+value.totalCapacity+'</label><br>'+
    	                 '<label  style="color:#D43F08;">Vendor : '+value.vendorDetails.vendorId+' - '+value.vendorDetails.appUsername+'</label><br>'+
    	                 '<label  style="color:#D43F08;">User : '+value.userDetails.appUserName+'</label><br>'+
    	                 '<label style="color:#D43F08;">Pickups : <br>&nbsp;&nbsp;Total Pickups = '+value.vehiclePickups.totalCount+ "<br>&nbsp;&nbsp;Total Weight = "+value.vehiclePickups.totalWeight+ "<br>&nbsp;&nbsp;Current Usage = "+value.vehiclePickups.currentUsage+ "<br>&nbsp;&nbsp;Projected Free = "+value.vehiclePickups.projectedFree+'</label><br>'+
    	                 '<label  style="color:#D43F08;">Deliveries : <br>&nbsp;&nbsp;Total Deliveries = '+value.vehicleDeliveries.totalCount+"<br>&nbsp;&nbsp;Total Weight = "+value.vehicleDeliveries.totalWeight+"<br>&nbsp;&nbsp;Current Usage = "+value.vehicleDeliveries.currentUsage+"<br>&nbsp;&nbsp;Projected Free = "+value.vehicleDeliveries.projectedFree+'</label><br>'+
    	               '</div>';
        		  
        		  google.maps.event.addListener(marker,'mouseover', (function(marker,content,infowindow){ 
        		        return function() {
        		           infowindow.setContent(content);
        		           infowindow.open(map,marker);
        		        };
        		    })(marker,content,infoWindow)); 
        		  
        		  
    		/*   google.maps.event.addListener(marker, 'mouseover', function() {
    		    	debugger
    		    	infoWindow.open(map,marker);
    		    });*/
        		/*  google.maps.event.addListener(marker,'mouseout', (function(marker,content,infowindow){ 
      		        return function() {
      		         infoWindow.close();
      		        };
      		    })(marker,content,infoWindow));*/ 
        		  
        		  google.maps.event.addListener(marker, 'mouseout', (function(marker, content, infoWindow) {
        		      return function() {
        		    	  infoWindow.close();
        		      };
        		    })(marker, content, infoWindow));

        		  
        		  map.fitBounds(bounds);
        		  
    		  /*  google.maps.event.addListener(marker, 'mouseout', function() {
    		    	debugger
    		    	infoWindow.close();
    		    });	*/
	 
	 });
	debugger
}

function placeMarker( loc ) {
    var latLng = new google.maps.LatLng( loc[1], loc[2]);
    var marker = new google.maps.Marker({
      position : latLng,
      map      : map
    });
    google.maps.event.addListener(marker, 'click', function(){
        infowindow.close(); // Close previously opened infowindow
        infowindow.setContent( "<div id='infowindow'>"+ loc[0] +"</div>");
        infowindow.open(map, marker);
    });
  }
  

////////////////////////////////////////////////////////////////////////////////
//start - mdm commands

function getAllUsersWithDeviceDetails(){
	debugger
	   $.ajax({
	         url : 'getUserDetails', contentType:"application/json", type : 'get', clearForm : true,
			 beforeSend : function(xhr){ $.blockUI({ message : "Please wait..." });  return true; },
			 success : function(response) {
			                 if (response.status) {
			                	 debugger
			                	 $('#locsTable').DataTable().destroy();  $('#locsTable tbody').empty();
			        			 if(response.data!=null && response.data.length>0){
			        				 var serviceDataTr='';
			        		    	 $.each(response.data,function(key,val){
			        		    		 debugger
			        		    		 serviceDataTr+=
			        		    	     '<tr id="'+key+'_'+val.AppPwd+'_'+val.id+'_'+val.AppUserName+'_assetsTr">'+
				        		    	    '<td class="tableTd">'+(key+1)+'</td>'+
				        		    		'<td id='+key+' class="tableTd" style="text-align: center;">'+
				        		    		     '<button class="btn btn-warning btn-xs" >'+
				        		    		          '<input type="checkbox" id="'+key+'_deleteWOCheckboxId" class="deleteWOCheckBox" >'+
				        		    		      '</button>'+
				        		    		'</td>'+
				        		    		'<td class="tableTd">'+val.AppUserName+'<small>&nbsp;&nbsp;('+val.AppPwd+')</small></td>'+
				        		    		'<td style="text-align: center;">'+
				        		    		     '<button id="deleteWOBtn" onclick="lockCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
				        		    		          '&nbsp;&nbsp;<i class="fa fa-lock" ></i>&nbsp;&nbsp'+
				        		    		      '</button>'+
				        		    		 '</td>'+
				        		    		 '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="sirenCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-bullhorn"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="appsListCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-list"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="wipeCmd(this)"  class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-trash"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="locUpdatesCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-map-marker"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		     '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="pushappCmd(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-file"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+
			        		    		    /* '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="editWO(this)" class="btn btn-success btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-globe"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+*/
			        		    		 '</tr>';
			        		    	 });
			        		    	 debugger
			        		    	    $("#locsTable tbody").html(serviceDataTr);
			        	        		$("#locsTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
			        	        		$("#locsModal").modal('show');
			        	        		
			        	        		//delete wo checkbox click(check / uncheck)
			        		           	  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').on('click', function(event){
			        		           		  if ($('.deleteWOCheckBox:checked').length == $('.deleteWOCheckBox').length  ) {
			        		           			$('.selectOrDeselectAllWOCheckBox').prop('checked',true);
			        		           			$('#commands_div').show();
			        	       			       }
			        		           		  else   if ($('.deleteWOCheckBox:checked').length > 1 ) {
			        		           			  $('.selectOrDeselectAllWOCheckBox').prop('checked',false);
			        		           			$('#commands_div').hide();
			        		           		  }
				        	       			  else{
				        	       				  $('.selectOrDeselectAllWOCheckBox').prop('checked',false);
				        	       				$('#commands_div').hide();
				        	       			  }
			        		           	  });
			        			 }
			        			 else{
			        				 /* alert("Assets not available"); */
			        				   $("#locsTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
			        				 $("#locsModal").modal('show');
			        			 }
			                 }
			                 $.unblockUI();
			            },
			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		  });
	debugger
}


//wo sel/desel all
$('.selectOrDeselectAllWOCheckBox').click(function() {
	  debugger
	  if($(this).is(':checked')){
		  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', true);
		  $('#commands_div').show();
	  }
	  else{
		  $('#locsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').prop('checked', false);
		  $('#commands_div').hide();
	  }
})

 //del wo
	 $('#deleteWOBtn').click(function() {
			var woIdsToBeDeleted=[],woNosToBeDeleted='';
			 $('#servicesTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(key,val){
				 debugger
				 woIdsToBeDeleted.push({"id":allServiesData[this.id.split('_')[0]].id});
				 woNosToBeDeleted=(key==0)?allServiesData[this.id.split('_')[0]].woNo:woNosToBeDeleted+", "+allServiesData[this.id.split('_')[0]].woNo;
			 }) 
			 if(woNosToBeDeleted==''){
				 alert("Please select atleast one Work Order");
			 }
			 else{
			 }
	 });

//lock 
/*selectedUsers+=(selectedUsers.length>0)?event.parentNode.parentElement.id.split("_")[3] +",":event.parentNode.parentElement.id.split("_")[3] ;
selectedUserDeviceImeis+=(selectedUserDeviceImeis.length>0)?event.parentNode.parentElement.id.split("_")[1] +",":event.parentNode.parentElement.id.split("_")[1] ;*/

function lockCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send lock command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_LOCK,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');
	}
	else{
		return false;
	}
}


function sirenCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send Siren command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_SCREAM,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');
	}
	else{
		return false;
	}
}

function wipeCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
    var confirmation = confirm("Send Siren command to \n"+selectedUsers+"\n\n NOTE* : THIS WILL PERMANENTLY REMOVE ALL THE DATA.");
	if (confirmation == true) {
		//send push
		debugger
		alert("Request Completed")
		/*$.post("allOperations", {
			'deviceid[]' : event.parentNode.parentElement.id.split("_")[1],
			deviceid : event.parentNode.parentElement.id.split("_")[1],
			operationtype : CONSTANTS_WIPE,
			type : "command"
		}, function(response) {
			alert(response.message);
		}, 'json');*/
	}
	else{
		return false;
	}
}

function appsListCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[3]  ;
	debugger
	 var confirmation = confirm("Send Apps List command to \n"+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		$.ajax({
			url : 'allOperations',
			dataType : 'json',
			data: {'deviceid[]': event.parentNode.parentElement.id.split("_")[1], deviceid:event.parentNode.parentElement.id.split("_")[1], operationtype: CONSTANTS_APP_LIST, type:"command"},
			type : 'post',
			beforeSubmit : function() {
				$.blockUI({
					message : "Please Wait sending message to device to synch its data...."
				});
			},
			success : function(response,status,request,form) {					
				$.unblockUI();	
					if(response.status){				        			
	        			$.ajax({
							url: "allApplications",
		                	type: "POST",
		                	dataType : 'json',
		                	data: { deviceid :  event.parentNode.parentElement.id.split("_")[1] },
		                	beforeSend : function(jqXHR, settings){
		                		$.blockUI({
		    						message : "Data synched now taking data from server...."
		    					});
		                	},
		                	success: function (response) {
		                		$.unblockUI();
		                		if(response.status){   		
		                			$("#appsListHead_h4").text("Apps List for Device id = "+event.parentNode.parentElement.id.split("_")[1]);
		                			 $('#assetsTable').DataTable().destroy();  $('#assetsTable tbody').empty();
		                			 debugger
		                			 if(response.data!=null && response.data.data.length>0){
		                				 var serviceDataTr='';
		                		    	 $.each(response.data.data,function(key,val){
		                		    		 debugger
		                		    		 serviceDataTr+='<tr id="'+key+'_'+event.parentNode.parentElement.id.split("_")[1]+'_'+val.applicationPackageName+'_assetsTr">'+
		                		    		 '<td class="tableTd">'+(key+1)+'</td>'+
		                		    		 '<td class="tableTd">'+(val.applicationName?val.applicationName:"--")+'</td>'+
		                		    		 '<td class="tableTd">'+(val.applicationVersion?val.applicationVersion:"--")+'</td>'+
		                		    		 '<td style="text-align: center;">'+
			        		    		     '<button id="deleteWOBtn" onclick="uninstallAppCmd(this)" class="btn btn-danger btn-xs deleteWOBtnClass" style="cursor: pointer">'+
			        		    		          '&nbsp;&nbsp; <i class="fa fa-trash"></i>&nbsp;&nbsp'+
			        		    		      '</button>'+
			        		    		     '</td>'+		                		    	
			        		    		     '</tr>';
		                			    	
		                		    	 });
		                		    	 debugger
		                		    	    $("#assetsTable tbody").html(serviceDataTr);
		                	        		$("#assetsTable").DataTable( { destroy:true, "order": [], "columnDefs": [ { "targets"  : 'no-sort', "orderable": false, }] });
		                				      $("#deleteAssetBtn").prop('disabled', false);
		                	        		$("#assetsModal").modal('show');
		                	        		
		                	        		 //delete asset checkbox click(check / uncheck)
		                	        		  $('#assetsTable').DataTable().rows().nodes().to$().find('input[type="checkbox"]').on('click', function(event){
		                	        			  debugger
		                	        			  if ($('.deleteAssetCheckBox:checked').length == $('.deleteAssetCheckBox').length) {
		                	        				  debugger
		                	        		           $('.selectOrDeselectAllAssetCheckBox').prop('checked',true);
		                	        			    }else{
		                	        			    	 debugger
		                	        			    	  $('.selectOrDeselectAllAssetCheckBox').attr('checked',false);
		                	                			  captureAssetsIdsToBeDeleted(event.currentTarget.id.split('_')[0]);
		                	        			    }
		                	        	       });
		                			 }
		                			 else{
		                				   $("#assetsTable tbody").html('<tr><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td><td align="center">--No Data--</td></tr>');
		                				   $("#deleteAssetBtn").prop('disabled', true);
		                				 $("#assetsModal").modal('show');
		                			 }
		                		}else{
		                			alert(response.message);
		                		}
		                	},
			                error: function(xhr, ajaxOptions, thrownError){
			                	$.unblockUI();
		                	},
	    	                timeout :CONSTANTS_AJAX_TIMEOUT 
						});
	        		}
					else{
						alert(response.message);
					}
				//}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert('error');
				alert(xhr.status);
				alert(thrownError);
				$.unblockUI();
			},
			timeout :CONSTANTS_AJAX_TIMEOUT
		});
		
		
	}
	else{
		return false;
	}
	
}

function uninstallAppCmd(event){
	debugger
	var selectedUserIds='', selectedUsers='';
	selectedUsers=event.parentNode.parentElement.id.split("_")[1]  ;
	debugger
    var confirmation = confirm("Send Uninstall command to \nDeviceId : "+selectedUsers);
	if (confirmation == true) {
		//send push
		debugger
		
		$.ajax({
     		url:"allOperations",
     		data:"uninstall_apps="+event.parentNode.parentElement.id.split("_")[2]+"&operationtype="+CONSTANTS_APP_UNINSTALL+"&deviceid="+event.parentNode.parentElement.id.split("_")[1]+"&type="+"command",
     		dataType : 'json',
     		type: "POST",
     		beforeSend: function(){      			
     		},
     		success : function(response){
     			alert("sucess");
     			if (response.status) {
     				 $("#assetsModal").modal('hide');
				} else {
					alert(response.message);
				}
     			},
     	});
		
	}
	else{
		return false;
	}
}


function locUpdatesCmd(event){
	debugger
	  $.ajax({
	         url : 'getUserLocationHistory?userId=6&from=90&to=89',contentType:"application/json", type : 'get', clearForm : true,
			 beforeSend : function(xhr){
				 $.blockUI({ message : "Please wait..." });  return true;
		    },
		     success : function(response) {
		    	 debugger
		                 if (response.status) {
		                	 debugger
		                	 if(response.data!=null && response.data.length>0){
		                		 debugger
		                		 //openMapinModal(event.parentNode.parentElement.id.split("_")[1],response.data);
		                		 showlocationHistory(response.data,event.parentNode.parentElement.id.split("_")[1]);
		                		 
		                		 }
		                	 else{
		                		 alert(response.message)
		                	 }
		                 }
		                 $.unblockUI();
		            },
			 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
		  });
	
	
}


var loctionsHistoryOfDeviceMap;
var markerArray = [];
var directionsService;
var directionsDisplay;
var stepDisplay;
function openMapinModal(did,locations){
	debugger
	//$('#loctionsHistoryOfDeviceMapModal').one('click', function(){
		$('#loctionsHistoryOfDeviceMapModal').on('shown', function(){
		showlocationHistory(locations,did);
		});
}


function showlocationHistory(locations, did){
	debugger
	loctionsHistoryOfDeviceMap = new google.maps.Map(document.getElementById('loctionsHistoryOfDeviceMap_DivId'), {
		    center: {lat: locations[0].latitude, lng:locations[0].longitude},// mandatory other wise map will not dispaly anything.
		    zoom: 17
     });
	 loctionsHistoryOfDeviceMap.setCenter(new google.maps.LatLng(locations[0].latitude,locations[0].longitude));
	  // Instantiate a directions service.
	    directionsService = new google.maps.DirectionsService;
		  // Create a renderer for directions and bind it to the map.
	     directionsDisplay = new google.maps.DirectionsRenderer({map: loctionsHistoryOfDeviceMap});
	    // Instantiate an info window to hold step text.
	     stepDisplay = new google.maps.InfoWindow;
	 
	drawDirectionPath(locations, directionsService,directionsDisplay,stepDisplay,markerArray);
	
	loctionsHistoryOfDeviceMap.setCenter(new google.maps.LatLng({lat: locations[0].latitude, lng:locations[0].longitude}));
	
	// bind info window data to table.
	var serviceDataTr='';
	 $.each(locations,function(key,val){
		 debugger
		 serviceDataTr+='<tr >'+
		 '<td class="tableTd">A</td>'+
		 '<td class="tableTd">'+(val.address?val.address:"--")+'</td>'+
	     '</tr>';
	 });
	$("#loctionsHistoryOfDeviceMapTables tbody").html(serviceDataTr);
	
	 $("#loctionsHistoryOfDeviceMapModalHead_h4").text("Location of Device id = "+did);
	     $('#loctionsHistoryOfDeviceMapModal').modal('show');
	     
	
}

function  drawDirectionPath(locationList, directionsService,directionsDisplay,stepDisplay,markerArray ) {
	   var origin = {lat: locationList[0].latitude, lng: locationList[0].longitude};
	   var destination= {lat: locationList[locationList.length-1].latitude, lng: locationList[locationList.length-1].longitude};
	   
	   //waypts
	   var waypts = [];
	   for (var i = 1; i < locationList.length-1; i++) {
        waypts.push({
          location: {lat: locationList[i].latitude, lng: locationList[i].longitude},
          stopover: true
        });
      }
	   
	   calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, map,origin, destination,waypts );
 }


   function calculateAndDisplayRoute(directionsDisplay, directionsService,
       markerArray, stepDisplay, map, origin, destination,waypts) {
     // First, remove any existing markers from the map.
     for (var i = 0; i < markerArray.length; i++) {
       markerArray[i].setMap(null);
     }

     // Retrieve the start and end locations and create a DirectionsRequest using
     // WALKING directions.
     directionsService.route({
      /*  origin: document.getElementById('start').value,
       destination: document.getElementById('end').value, */
       origin: origin,
       destination:destination,
       waypoints: waypts,
       travelMode: 'DRIVING'
     }, function(response, status) {
       // Route the directions and pass the response to a function to create
       // markers for each step.
       if (status === 'OK') {
     	console.log( '<b>' + response.routes[0].warnings + '</b>');
        /*  document.getElementById('warnings-panel').innerHTML =
             '<b>' + response.routes[0].warnings + '</b>'; */
         directionsDisplay.setDirections(response);
         //showSteps(response, markerArray, stepDisplay, map);
       } else {
         window.alert('Directions request failed due to ' + status);
       }
     });
   }

   function showSteps(directionResult, markerArray, stepDisplay, map) {
     // For each step, place a marker, and add the text to the marker's infowindow.
     // Also attach the marker to an array so we can keep track of it and remove it
     // when calculating new routes.
     var myRoute = directionResult.routes[0].legs[0];
     for (var i = 0; i < myRoute.steps.length; i++) {
       var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
       marker.setMap(map);
       marker.setPosition(myRoute.steps[i].start_location);
       attachInstructionText(
           stepDisplay, marker, myRoute.steps[i].instructions, map);
     }
   }

   function attachInstructionText(stepDisplay, marker, text, map) {
     google.maps.event.addListener(marker, 'click', function() {
       // Open an info window when the marker is clicked on, containing the text
       // of the step.
       stepDisplay.setContent(text);
       stepDisplay.open(map, marker);
     });
   }

   
   
   // animation
   var userLocation;
   function getVehicleLocation(){
	  debugger
	 /*  if( $('#vehicleSelectId').val()==null || $('#vehicleSelectId').val().length <=0){
			alert("Please Select Vehicle");
			return false;
		}
	   else{*/
		   $.ajax({
		         url : 'getUserLocationHistory?userId=16',contentType:"application/json", type : 'get', clearForm : true,
				 beforeSend : function(xhr){
					 $.blockUI({ message : "Please wait..." });  return true;
			    },
			     success : function(response) {
			    	 debugger
			                 if (response.status) {
			                	 debugger
			                	 // set the user name and date for which the animaton is being processed.
	                			// $("#userNameSelected").text($('#userSelectId option:selected').text());
	                			 //$("#dateSelected").text($('#dateId').val());
			                	 if(response.data!=null && response.data.length>0){
			                		 debugger
			                		 locationAsSource=0;
			                		 userLocation = response.data;
			                		 initVehiclePathMap(response.data);
			                		 }
			                	 else{
			                		 alert(response.message)
			                	 }
			                 }
			                 $.unblockUI();
			            },
				 error : function(xhr, ajaxOptions, thrownError) { alert('error'+", "+xhr.status+", "+thrownError); }
			  });
	 /*  }*/
   }
   
   
   var mapForAnim;
   var infoContentForAnimatedMarker='';
   var infow;
   function initVehiclePathMap(locationList){
		 debugger
		 
	     //Create a center for the map.
		 mapForAnim = new google.maps.Map(document.getElementById('vehiclePathMap_DivId'), {
	     center: {lat: locationList[0].latitude, lng:locationList[0].longitude},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		
		 //Create the marker for the first location.
		  markerA =  new google.maps.Marker({
	          position : {lat: locationList[0].latitude, lng:locationList[0].longitude},
	         map : mapForAnim,
	        // icon : "images/truck3.jpg",
	         //icon : "images/sgv.svg"
	        	 path: 'M -1,0 A 1,1 0 0 0 -3,0 1,1 0 0 0 -1,0M 1,0 A 1,1 0 0 0 3,0 1,1 0 0 0 1,0M -3,3 Q 0,5 3,3',
	             strokeColor: '#00F',
	             rotation: 90
	   		 
	      });
		  markerA.setMap( mapForAnim ); 
		  
		  
		 
		
		 //Create a position array which always contains first location lat and lng. Position arrary is used for the marker animation.
		 position=[locationList[0].latitude,locationList[0].longitude ];
		
		 
		 infoContentForAnimatedMarker= 
             ' <div >'+
                '<label style="color:#D43F08;" >'+locationList[0].latitude+','+locationList[0].longitude+'</label><br>'+
                '<label  style="color:#D43F08;">'+locationList[0].address+'</label><br>'+
                '<label  style="color:blue;">'+locationList[0].deviceTimestamp+'</label><br>'+
              '</div>';
		 
		 
		  infow=new google.maps.InfoWindow({content : infoContentForAnimatedMarker});
		 infow.open(mapForAnim,markerA);
		 
		 //Create a path on the 
		 showOnMap(locationList);
		
		 //Clear the var which holds the location to which the marker is beign animated
		// locationAsSource=0;
		 
		 
		 
		/* //USed for direcs api
		 
		   markerArray = [];
		 
		  // Instantiate a directions service.
	    directionsService = new google.maps.DirectionsService;
		 
		  // Create a renderer for directions and bind it to the map.
	     directionsDisplay = new google.maps.DirectionsRenderer({map: map});
		 
	    // Instantiate an info window to hold step text.
	     stepDisplay = new google.maps.InfoWindow;*/
	    
		 debugger
	}
   
   function showOnMap(locationList){
		debugger
		var polyCoords=[];
		// for some reason each () is not working so using for loop. chage it after fixing issue with each ().
		for (var i = 0; i < locationList.length; i++) {
			//debugger
			if(locationList.length ==1){
				triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude}];
			}
			else if(i==0){
				triangleCoords = [{lat: locationList[i].latitude, lng: locationList[i].longitude},{lat : locationList[i+1].latitude, lng : locationList[i+1].longitude}];
			}
			else{
				triangleCoords = [{lat: locationList[i-1].latitude, lng: locationList[i-1].longitude},{lat : locationList[i].latitude, lng : locationList[i].longitude}];
				polyCoords.push(triangleCoords);
			}
			mapForAnim.setCenter(new google.maps.LatLng(locationList[i].latitude, locationList[i].longitude));
			mapForAnim.setZoom(17); 
			  drawPolyline(triangleCoords,i);
		}
		
		debugger
	}

   function drawPolyline(triangleCoords){
		 debugger
		  var bermudaTriangle = new google.maps.Polygon({
		         paths: triangleCoords,
		         strokeColor: '#FF0000',
		         strokeOpacity: 0.8,
		         strokeWeight: 3
		       //icon: lineSymbol,
		       });
		       bermudaTriangle.setMap(mapForAnim);
		       debugger
	}
   
   
   var numDeltas = 100;
   var delay = 40; //milliseconds
   var i = 0;
   var deltaLat;
   var deltaLng;
   function transition(result){
	   debugger
		i = 0;
	    deltaLat = (result[0] - position[0])/numDeltas;
	    deltaLng = (result[1] - position[1])/numDeltas;
	    moveMarker(position);
	    debugger
	}

	function moveMarker(){
		debugger
		//  1.
		  //var prevPosn = userLocation[locationAsSource].latitude, userLocation[locationAsSource].longitude
	      var prevPosn = new google.maps.LatLng(userLocation[0].latitude, userLocation[0].longitude)
//		 markerA.setMarker({
//		      //  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
//		        strokeColor: 'red',
//		        strokeWeight: 3,
//		        scale: 6,
//		        rotation:105// google.maps.geometry.spherical.computeHeading(prevPosn, markerA.getPosition())
//		      });
//		
		
	/*	//2
		var icon = markerA.getIcon();
	    var rotation = icon.rotation;
	    rotation += 5;
	    icon.rotation = rotation;
	    markerA.setIcon(icon);*/
		
		
		
		//3.
		markerA.setIcon('images/markerWithVeh.jpg'); // set image path here...
		
	    position[0] += deltaLat;
	    position[1] += deltaLng;
	    var latlng = new google.maps.LatLng(position[0], position[1]);
	    markerA.setPosition(latlng);
	    if(i!=numDeltas){
	        i++;
	        setTimeout(moveMarker, delay);
	    }
	    
	   
	    debugger
	}

	//capture index of location for which the marker is being animated.
	var locationAsSource=0;
	function animateMarker(){
		debugger
		if (locationAsSource < userLocation.length) {
			 infoContentForAnimatedMarker= 
	             ' <div >'+
	                '<label style="color:#D43F08;" >'+userLocation[locationAsSource].latitude+','+userLocation[locationAsSource].longitude+'</label><br>'+
	                '<label  style="color:#D43F08;">'+userLocation[locationAsSource].address+'</label><br>'+
	                '<label  style="color:blue;">'+userLocation[locationAsSource].deviceTimestamp+'</label><br>'+
	              '</div>';
			 infow.setContent(infoContentForAnimatedMarker);
		    infow.open(mapForAnim,markerA);
			 var result = [ userLocation[locationAsSource].latitude, userLocation[locationAsSource].longitude];
			    transition(result);
			    //capture index of next location(source) for wich marker should be animated animated.
			    locationAsSource++;
			    debugger
			   
		}
		else if (locationAsSource==userLocation.length){
			alert("Reached Destination");
		}
		debugger
	}
   
	
	function pushappCmd(){
		$("#pushappModal").modal("show");
	}
	
	
	 function initMapForGeofence(){
		 debugger
	     //Create a center for the map.
		 geofenceMap = new google.maps.Map(document.getElementById('geofenceMap_DivId'), {
			 center: {lat: 17.444785, lng:78.505353},// mandatory other wise map will not dispaly anything.
	     zoom: 17
	     });
		 geofenceMap.setCenter(new google.maps.LatLng({lat: 17.444785, lng:78.505353}));
		 $('#geofenceModal').modal('show');
		 debugger
	}
	
//end - mdm commands
/////////////////////////////////////////////////////////////////////////////////
