var iataTable,vehicleMasterTable,vendorMasterTable,coloaderConfigMasterTable,coloaderMasterTable,
     airlineConStatusCodesTable,flightDetailsMasterTable,gateInbagsTableVar,retrievedBagsTableVar,rejectedBagsTableVar,cdDetailsTableVar;

var destDebagTableVar,destdebagConTableVar,destdebagPacksTableVar;
var bagsHandoverTableVar;
var retFromColTableVar;

var airFlightTb = null;
var airportTb = null;
var contentTb = null;
var dangerouscodeTb = null;
var pincodeTb = null;
var createdBagsTb = null;
var bagNumber = null;
var Data;
var BagNoTb = null;
var conNumber = null;
var PacketTb = null;
var debaggedBagNoTb;
var debaggedPacketTb ;
var packetData;
var debaggedBagsTb = null;
var  debaggedbagNumber, debaggedconNumber;

//function to show airlinne details 
// called on the change of the origin code
function showAirlineDetails(originCode){
	$.ajax({
		 url:"http://14.192.16.55:8080/plt/getAirlineDetails?originCode="+originCode,
//		url:"http://localhost:8082/plt/getAirlineDetails?originCode="+originCode,
		// dataType: "json",
		type: "get",
	    dataType: "json",
	    processData: false,
	    crossDomain: true,
		beforeSend:function(){
			$.blockUI({ message : "Please wait..." });  return true
		},
		error:function(httpObj, textStatus, response){
	         alert("Error");
            console.log(httpObj);
            console.log(textStatus);
            console.log(response);
	         $.unblockUI();
	     },
		success: function (response) {
			$.unblockUI();
			if(flightDetailsMasterTable==null)
			{
				flightDetailsMasterTable= $("#flightDetailsMasterTable").DataTable({
					columns:[
						{data : 'flightNumber'},
						{data:'airlineCode'},
						{data:'airlineDescription'},
						{data:'origin'},
						{data:'destination'},
						{data:'daysOperated'},
						{data:'flightType'},
						{data:'departureTime'},
						{data:'arrivalTime'}
					],
					data:response.data,
					retrieve:true,
					fixedHeader:{
						header:true,
						footer:false
					},
					paging: true,
	                searching: true,
	                ordering: true,
	                scrollX: true,
	                scrollCollapse:true,
	                scrollY: "330px", 
	                responsive: true,
	                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
				});
							
			}
			else
			{
				flightDetailsMasterTable.clear().draw();
				flightDetailsMasterTable.rows.add(response.data); // Add new data
				flightDetailsMasterTable.columns.adjust().draw(); 
			}
			
		}
	});
	
}

function showConModal(event){
	$("#BagNoDisplay").modal("show");
	$('#BagNoDisplay').on('shown.bs.modal', function () {
	       var table = $('#BagNo_table').DataTable();
	       table.columns.adjust();
	   });
//	bagNumber=$(this).text();
	bagNumber = createdBagsTb.row($(event).parent().parent()).data().bagNumber;
	$("#BagNo").html("Bag Number : "+bagNumber);
	$.ajax({
		
		url : "http://14.192.16.55:8080/plt/getCreatedBagCons",
		data : "bagNumber="+bagNumber,
		dataType : "json",
		method : "post",
		 processData: false,
		    crossDomain: true,
		    beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true;
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
                 console.log(httpObj);
                 console.log(textStatus);
                 console.log(response);
		         $.unblockUI();
		     } ,
			
		success :function(Response)
		{
			$.unblockUI();
			console.log(Response);
           debugger
			if(BagNoTb==null)
			{
				BagNoTb = $("#BagNo_table").DataTable({
				columns : [
					{ data : "conNo",
						"render": function ( data, type, full, meta ) {
						
				            console.log(conNumber);
				    return '<a  class="test" style="color:#1976D2;">'+data+'</a>';
			    
						}
			    },
					
					{ data : "nextDest"},
					{ data : "originCode"},
					{ data : "destinationCode"},
					{ data : "conWeight"},
					{ data : "scannedPktCount"},
					{ data : "noOfPkgs"},
					{ data : "scanTime"},
					{ data : "scanCompleted"},
					{ data : "productType"},
					{ data : "isInDangerList"},
					{ data : "isHeavy"},
					/*{ data : "packets",
						render: function ( data, type, row ) {
						    return '<button type="button" class="btn btn-success btn-sm" id="getPacketsBtn">GetPackets</button>';
						 }
					}*/
				],
				/*columnDefs : [
			        { targets : [1],
			          render : function (data, type, row) {
			             return data == '1' ? 'Y' : 'N'
			          }  
			        },
			        {
			        	targets : [2],
			        	render : function (data, type, row)
			        	{
			        		return data == '1' ? 'Y' : 'N'
			        	}
			        }
			   ],*/
				data : Response.data,
				retrieve : true,
				responsive: true,
				fixedHeader : {
					header : true,
					footer : false
				}, 
				paging : true,
                searching : true,
                ordering : true,
                scrollX : true,
                scrollCollapse : true,
                scrollY : "330px", 
                responsive : true,
                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
		
			});
			}
		else
			{
			BagNoTb.clear().draw();
			BagNoTb.rows.add(Response.data);
			BagNoTb.columns.adjust().draw();
			}
			$(".test").click(function(){
				conNumber=$(this).text();
				console.log("in getPacketsBtn function");
				$("#PacketDisplay").modal("show");
				$('#PacketDisplay').on('shown.bs.modal', function () {
				       var table = $('#packet_table').DataTable();
				       table.columns.adjust();
				   });
				
				$("#ConNo").html("Con Number : "+conNumber);
				$("#BagNoDisplay").modal("hide");
				$.ajax({
					url : "http://14.192.16.55:8080/plt/getCreatedBagPackets",
					data : "conNumber="+conNumber,
					dataType : "json",
					method : "post",
					 processData: false,
					    crossDomain: true,
					    beforeSend:function(){
							$.blockUI({ message : "Please wait..." });  return true;
						},
						error:function(httpObj, textStatus, response){
					         alert("Error");
			                 console.log(httpObj);
			                 console.log(textStatus);
			                 console.log(response);
					         $.unblockUI();
					     } ,
						
					success :function(Response)
					{
						$.unblockUI();
						console.log(Response);
                     debugger
						if(PacketTb==null)
						{
							PacketTb = $("#packet_table").DataTable({
							columns : [
								{ data : "pktNo"},
								{ data : "scanned"},
								{ data : "manuallyScanned"},
								{ data : "scanTime"},
								{ data : "pktWt"}
								
							],
							/*columnDefs : [
						        { targets : [1],
						          render : function (data, type, row) {
						             return data == '1' ? 'Y' : 'N'
						          }  
						        },
						        {
						        	targets : [2],
						        	render : function (data, type, row)
						        	{
						        		return data == '1' ? 'Y' : 'N'
						        	}
						        }
						   ],*/
							data : Response.data,
							retrieve : true,
							responsive: true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						PacketTb.clear().draw();
						PacketTb.rows.add(Response.data);
						PacketTb.columns.adjust().draw();
						}
				}
			});	

			});
			
	}
});	
}

function removeActive(element){
	console.log(element);
	console.log($(element));
	var id;
	$(".active").each(function(){
		
		$(this).removeClass("active");
		/*id=$(this).children().first().attr('href');
		console.log($(this).children().first().attr('href'));
		
		$(""+id).removeClass('active');*/
	});
	id=$(element).children().first().attr('href');
	console.log($(id+""));
	$(""+id).addClass('active');
	$(element).addClass("active");
	
}


function format ( d ) {
    // `d` is the original data object for the row
	
	console.log(d.bagNumber);
	var bagNumber=d.bagNumber;
	var header='<div><h4 style="margin-left:-1600px;"><b>Bag Numbers<h4></b></div>';
		header=header+'<table cellpadding="5" cellspacing="0" border="0">';
	
	for(var i=0;i<bagNumber.length;i++)
		{
		header=header+"<tr><td>"+bagNumber[i]+"</td></tr>";
		}
	header=header+"</table>";
	console.log(header);
    return header;
}




function showdestDebagConsModal(data){
//	console.log(data);
	data=destDebagTableVar.row($(data).parent().parent()).data();
//	data=data.debaggingCons;
	console.log(data);
	$("#destDebagNo").html("Bag Number : "+data.bagNo);
	$('#destdebagConsModal').on('shown.bs.modal', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });
	
	$("#destdebagConsModal").modal('show');

			if (destdebagConTableVar == null) {
				destdebagConTableVar = $("#destdebagConTable").DataTable({
					columns : [
					           {
					        	 data:'conNo' ,
					        	 render : function(data, type, row) {
					        		 return "<a onclick='destdebagPacksModal(this)' style='color:#1976D2;'>"+data+"</a>";
					        		 }
					           },
						{
							data : 'nextDest'
						},
						{
							data : 'originCode'
						},
						{
							data : 'destinationCode'
						},
						{
							data : 'conWeight'
						},
						{
							data : 'noOfPkgs'
						},
						{
							data : 'scanTime'
						},
						{
							data : 'isHeavy'
						},
						{
							data : 'isDG'
						}
					],
					data : data.debaggingCons,
					retrieve : true,
					fixedHeader : {
						header : true,
						footer : false
					},
					paging : true,
					searching : true,
					ordering : true,
					scrollX : true,
					scrollCollapse : true,
					scrollY : "330px",
					responsive : true,
					 "oLanguage": {
		                "sEmptyTable": "No Data Available"
		            },
					"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
				});

			} else {
				destdebagConTableVar.clear().draw();
				destdebagConTableVar.rows.add(data.debaggingCons); // Add new data
				destdebagConTableVar.columns.adjust().draw();
			}


		
	
}

function destdebagPacksModal(element){

//	console.log(data);
	data=destdebagConTableVar.row($(element).parent().parent()).data();
//	data=data.debaggingCons;

	$("#destDebagconNo").html("Consignment No: "+data.conNo);
	console.log(data);
	$('#destdebagPacksModal').on('shown.bs.modal', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });
	
	$("#destdebagPacksModal").modal('show');

			if (destdebagPacksTableVar == null) {
				destdebagPacksTableVar  = $("#destdebagPacksTable").DataTable({
					columns : [
						{
							data : 'pktNo'
						},
						{
							data : 'pktWt'
						}
					],
					data : data.debaggingPacket,
					retrieve : true,
					fixedHeader : {
						header : true,
						footer : false
					},
					paging : true,
					searching : true,
					ordering : true,
					scrollX : true,
					scrollCollapse : true,
					scrollY : "330px",
					responsive : true,
					 "oLanguage": {
		                "sEmptyTable": "No Data Available"
		            },
					"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
				});

			} else {
				destdebagPacksTableVar.clear().draw();
				destdebagPacksTableVar.rows.add(data.debaggingPacket); // Add new data
				destdebagPacksTableVar.columns.adjust().draw();
			}


}



function showdebaggedConModal(event)
{
	 conpacketData=debaggedBagsTb.row($(event).parent().parent()).data();
	$("#debaggedBagNoDisplay").modal("show");
	$('#debaggedBagNoDisplay').on('shown.bs.modal', function () {
	       var table = $('#debaggedBagNo_table').DataTable();
	       table.columns.adjust();
	   });
	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
		$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
		});
	
//	debaggedbagNumber = conpacketData.bagNumber;
//	debaggedbagNumber = $(event).text();
	debaggedbagNumber = debaggedBagsTb.row($(event).parent().parent()).data().bagNumber;
//	console.log(debaggedbagNumber);
	console.log(conpacketData);
	$("#debaggedBagNo").html("Bag Number : "+debaggedbagNumber);

	if(debaggedBagNoTb==null)
	{
		console.log(Response);
		debaggedBagNoTb = $("#debaggedBagNo_table").DataTable({
		columns : [
			{ data : "conNo",
				"render": function ( data, type, full, meta ) {
					//debaggedconNumber = data;
		    return '<a  class="debaggedBagtest" style="color:#1976D2;">'+data+'</a>';
	    
				}
	    },
			
			{ data : "nextDest"},
			{ data : "originCode"},
			{ data : "destinationCode"},
			{ data : "conWeight"},
			{ data : "scannedPktCount"},
			{ data : "noOfPkgs"},
			{ data : "scanTime"},
			{ data : "scanCompleted"},
			
		],
		data : conpacketData.debaggedCons,
		retrieve : true,
		responsive: true,
		fixedHeader : {
			header : true,
			footer : false
		}, 
		paging : true,
        searching : true,
        ordering : true,
        scrollX : true,
        scrollCollapse : true,
        scrollY : "330px", 
        responsive : true,
        "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]

	});
	}
else
	{
	debaggedBagNoTb.clear().draw();
	debaggedBagNoTb.rows.add(conpacketData.debaggedCons);
	debaggedBagNoTb.columns.adjust().draw();
	}

	           $(".debaggedBagtest").click(function(){
	        	   packetData=debaggedBagNoTb.row($(this).parent().parent()).data();
	        	   console.log(packetData);
	        	   $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
	        			$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
	        			});

				console.log(packetData.debaggingPacket);
				debaggedconNumber = $(this).text();
				$("#debaggedPacketDisplay").modal("show");
				$('#debaggedPacketDisplay').on('shown.bs.modal', function () {
				       var table = $('#debaggedpacket_table').DataTable();
				       table.columns.adjust();
				   });
				
				$("#debaggedConNo").html("Con Number : "+debaggedconNumber);
				$("#debaggedBagNoDisplay").modal("hide");
						console.log(Response);
						if(debaggedPacketTb==null)
						{
							debaggedPacketTb = $("#debaggedpacket_table").DataTable({
							columns : [
								{ data : "pktNo"},
								{ data : "scanned"},
								{ data : "excep"},
								{ data : "resolved"},
								{ data : "manuallyScanned"},
								{ data : "scanTime"},
								{ data : "excepType"},
								{ data : "excpDtls"},
								{ data : "resolvType"},
								{ data : "resolvDtls"},
								{ data : "pktWt"}
								
							],
							data : packetData.debaggingPacket,
							retrieve : true,
							responsive: true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						debaggedPacketTb.clear().draw();
						debaggedPacketTb.rows.add(packetData.debaggingPacket);
						debaggedPacketTb.columns.adjust().draw();
						}
					
			});
			

}

$(document).ready(function(){
	
	
	
	$('#retrievedBagsDate,#gateInbagsDate,#rejectedBagDate,#cdDetailsDate').datepicker({
		dateFormat: 'yy-mm-dd',
		maxDate:new Date()
	 });
	$("#retrievedBagsDate").val("");
	$("#gateInbagsDate").val("");




	$("#gateInbagsBtn").click(function() {

		if ($("#gateInbagNum").val() != "" || $("#gateInbagsDate").val()!="") 
		{
			$.ajax({
				url: "http://14.192.16.55:8080/plt/gateInCreatedBags",
//				url : "http://localhost:8080/plt/gateInCreatedBags",
				type : "get",
				data : "&bagNumber=" + $("#gateInbagNum").val() + "&gateInCreatedDate=" + $("#gateInbagsDate").val(),
				dataType : "json",
				processData : false,
				crossDomain : true,
				beforeSend : function() {
					$.blockUI({
						message : "Please wait..."
					});  return true;
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(response) {
					$.unblockUI();
					console.log(response);
					/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
							$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
							});*/
					debugger
					if (gateInbagsTableVar == null) {
						gateInbagsTableVar = $("#gateInbagsTable").DataTable({
							columns : [
								{
									data : 'vendorCode'
								},
								{
									data : 'vehicleNumber'
								},
								{
									data : 'bagNumber'
								},
								{
									data : 'null',
									render : function(data, type, row) {
										console.log(row.empCode);
										if (row.empCode == null)
											return "null";
										else
											return row.empCode;
									}
								},
								{
									data : 'gateInDateTime'
								}
							],
							data : response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
							searching : true,
							ordering : true,
							scrollX : true,
							scrollCollapse : true,
							scrollY : "330px",
							responsive : true,
							 "oLanguage": {
				                "sEmptyTable": "No Data Available"
				            },
							"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
						});

					} else {
						gateInbagsTableVar.clear().draw();
						gateInbagsTableVar.rows.add(response.data); // Add new data
						gateInbagsTableVar.columns.adjust().draw();
					}


				}
			});
		}
		else
			{
			alert("Please enter either Date or Bag Number");
			}
			
	});

	
	
	$("#retrievedBagsBtn").click(function(){
		if($("#retrievedBagsDate").val()!="" || $("#retrivedBagNum").val()!="")
			{
		$.ajax({
			url: "http://14.192.16.55:8080/plt/getRetrievedBags",
                        type: "get",
			data:"&retrievedDate="+$("#retrievedBagsDate").val()+"&bagNumber="+$("#retrivedBagNum").val(),
			dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true;
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
                 console.log(httpObj);
                 console.log(textStatus);
                 console.log(response);
		         $.unblockUI();
		     } ,
			success: function (response) {
				$.unblockUI();
				console.log(response);
			/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});*/
				debugger
				if(retrievedBagsTableVar==null)
				{
					retrievedBagsTableVar = $("#retrievedBagsTable").DataTable({
						columns:[
							{data : 'coloaderCode'},
							{data:'empCode'},
							{data:'bagNumber'},
							{data:'retrievedDateTime'}
						],
						data:response.data,
						retrieve:true,
						fixedHeader:{
							header:true,
							footer:false
						},
						paging: true,
		                searching: true,
		                ordering: true,
		                scrollX: true,
		                scrollCollapse:true,
		                scrollY: "330px", 
		                responsive: true,
		                "oLanguage": {
		                "sEmptyTable": "No Data Available"
		            },
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					retrievedBagsTableVar.clear().draw();
					retrievedBagsTableVar.rows.add(response.data); // Add new data
					retrievedBagsTableVar.columns.adjust().draw(); 
				}

				
			}
		});	
	}
		else
			alert("Please enter either Date or Bag Number")
	});
	
	
	$("#rejectedBtn").click(function() {

		if ($("#rejectedBagDate").val() != "" || $("#rejectedBagNum").val()!="") 
		{
			$.ajax({
				url: "http://14.192.16.55:8080/plt/getRejectedBagDetails",
//				
				type : "get",
				data : "&bagNumber=" + $("#rejectedBagNum").val() + "&gateInCreatedDate=" + $("#rejectedBagDate").val(),
				dataType : "json",
				processData : false,
				crossDomain : true,
				beforeSend : function() {
					$.blockUI({
						message : "Please wait..."
					});  return true;
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(response) {
					$.unblockUI();
					console.log(response);
					/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
							$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
							});*/
					debugger
					if (rejectedBagsTableVar == null) {
						rejectedBagsTableVar = $("#rejectedBagsTable").DataTable({
							columns : [
								{
									data : 'bagNumber'
								},
								{
									data : 'empCode'
								},
								{
									data : 'isBagRejectedByAirline'
								},
								{
									data : 'reasonForRejection'
									
								},
								{
									data : 'offloadRejectFlag'
								},
								{
									data : 'userId'
								}
							],
							data : response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
							searching : true,
							ordering : true,
							scrollX : true,
							scrollCollapse : true,
							scrollY : "330px",
							responsive : true,
							 "oLanguage": {
				                "sEmptyTable": "No Data Available"
				            },
							"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
						});

					} else {
						rejectedBagsTableVar.clear().draw();
						rejectedBagsTableVar.rows.add(response.data); // Add new data
						rejectedBagsTableVar.columns.adjust().draw();
					}


				}
			});
		}
		else
			{
			alert("Please enter either Date or Bag Number");
			}
			
	});



	$("#cdDetailsBtn").click(function() {

		if ($("#cdDetailsDate").val() != "" || $("#cdDetailsBagNum").val()!="") 
		{
			$.ajax({
				url: "http://14.192.16.55:8080/plt/getCdDetails",
//				
				type : "get",
				data : "&bagNumber=" + $("#cdDetailsBagNum").val() + "&cdCreatedDate=" + $("#cdDetailsDate").val(),
				dataType : "json",
				processData : false,
				crossDomain : true,
				beforeSend : function() {
					$.blockUI({
						message : "Please wait..."
					});  return true;
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(response) {
					$.unblockUI();
					console.log(response);
					/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
							$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
							});*/
					debugger
					if (cdDetailsTableVar == null) {
						cdDetailsTableVar = $("#cdDetailsTable").DataTable({
							columns : [
									{
										"className": 'showBagNumbers',
										"orderable": false,
										"data": null,
										"defaultContent": '<p style="color:#1976D2">Bag Numbers</p>'
									},
								{
									data : 'cdNumber',
								},
								{
									data : 'airMode'
								},
								{
									data : 'coloaderCode'
								},
								{
									data : 'empCode'
									
								},
								{
									data : 'eta'
								},
								{
									data : 'etd'
								},
								{
									data : 'flightNumber'
								},
								{
									data : 'iataCode'
									
								},
								{
									data : 'imagePath'
								},
								{
									data : 'remarks'
								},
								{
									data : 'updateType'
								},
								{
									data : 'updatedFlightNumber'
								},
								{
									data : 'updatedRemarks'
								}
							],
							data : response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
							searching : true,
							ordering : true,
							scrollX : true,
							scrollCollapse : true,
							scrollY : "330px",
							responsive : true,
							 "oLanguage": {
				                "sEmptyTable": "No Data Available"
				            },
							"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
						});

					} else {
						cdDetailsTableVar.clear().draw();
						cdDetailsTableVar.rows.add(response.data); // Add new data
						cdDetailsTableVar.columns.adjust().draw();
					}
					$('#cdDetailsTable tbody .showBagNumbers').click(function () {
						var tr = $(this).closest('tr');
						var row = cdDetailsTableVar.row(tr);

//						drawRowDetails(tr, row);
						
						
					
							if (row.child.isShown()) {
								// This row is already open - close it
								row.child.hide();
								tr.removeClass('shown');
							}
							else {
								// Open this row
								row.child(format(row.data())).show();
								tr.addClass('shown');
							}
						
					});



				}
			});
		}
		else
			{
			alert("Please enter either Date or Bag Number");
			}
			
	});
	
	
	$("#destDebagTab,#retFromColTab,#bagsHandoverTab").click(function(){
		$.ajax({
			url: "http://14.192.16.55:8080/plt/getAllIataCodes",
//			
			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
			   if(response.status)
				   {
				  /* showAirlineDetails(response.data[0]);// show the flight details table with the first respone origin as the origin
*/	
				   $("#destIataCodes").html("");
				   $("#destIataCodes").append("<option value='0' selected disabled>Select an Iata code</option>");
				   
				   $("#retIataCode").html("");
				   $("#retIataCode").append("<option value='0' selected disabled>Select an Iata code</option>");
				   
				   $("#bagsHandOverIataCode").html("");
				   $("#bagsHandOverIataCode").append("<option value='0' selected disabled>Select an Iata code</option>");
				   
				   for(var i=0;i<response.data.length;i++)
					   {
					  
					   $("#destIataCodes").append("<option >"+response.data[i]+"</option>");
					   $("#retIataCode").append("<option >"+response.data[i]+"</option>");
					   $("#bagsHandOverIataCode").append("<option >"+response.data[i]+"</option>");
					   }
				   
				   }
			}
		});
		
		
	
		
		
	});
	
	$("#retFromColTab").click(function(){
		
		$.ajax({
			url: "http://14.192.16.55:8080/plt/pullColoaderMaster",
//			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},			
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				console.log(response);
				debugger
				$("#retFromColId").html("");
				   $("#retFromColId").append("<option value='0' selected disabled>Select an Coloader code</option>");
				   
				   for(var i=0;i<response.data.length;i++)
					   {
					   $("#retFromColId").append("<option >"+response.data[i].vendorName+"-"+response.data[i].vendorCode+"</option>");
					   }
				   
				
			}
		});
		
		
	})
	
	$("#destIataCodes").change(function(){
		console.log($("#destIataCodes").val());
		if($("#destIataCodes").val()!=0)
			{
			$.ajax({
				url: "http://14.192.16.55:8080/plt/destinationDebaggingList",
//				
				type : "get",
				data : "iataCode="+$("#destIataCodes").val(),
				dataType : "json",
				processData : false,
				crossDomain : true,
				beforeSend : function() {
					$.blockUI({
						message : "Please wait..."
					});  return true;
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(response) {
					$.unblockUI();
					console.log(response);
					/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
							$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
							});*/
					debugger
					if (destDebagTableVar == null) {
						
//						destdebagConsModal
						destDebagTableVar = $("#destDebagTable").DataTable({
							columns : [
									{
										data:'bagNo',
										render : function(data, type, row) {

											return '<a onclick=showdestDebagConsModal(this) style="color:#1976D2;">'+data+'</a>';
											}
									},
								{
									data : 'bagOrigin',
								},
								{
									data : 'bagDestination'
								},
								{
									data : 'bagType'
								},
								{
									data : 'bagTotalWeight'
									
								},
								{
									data : 'createdBy'
								},
								{
									data : 'createdOn'
								},
								{
									data : 'isDebagged'
								},
								{
									data : 'IsGatePassOut'
									
								},
								{
									data : 'IsGatePassIn'
								},
								{
									data : 'vehicleNumber'
								},
								{
									data : 'vendorId'
								},
								{
									data : 'driverId'
								},
								{
									data : 'sealNumber'
								},
								{
									data : 'flightNumber'
								},
								{
									data : 'debaggedOn'
								},
								{
									data : 'debaggedBy'
								}
							],
							data : response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
							searching : true,
							ordering : true,
							scrollX : true,
							scrollCollapse : true,
							scrollY : "330px",
							responsive : true,
							 "oLanguage": {
				                "sEmptyTable": "No Data Available"
				            },
							"lengthMenu" : [ [ 10, 25, 50, -1 ], [ 10, 25, 50, "All" ] ]
						});

					} else {
						destDebagTableVar.clear().draw();
						destDebagTableVar.rows.add(response.data); // Add new data
						destDebagTableVar.columns.adjust().draw();
					}
					



				}
			});
		
			
			
			}
		
	})


	$("#bagsHandOverIataCode").change(function(){
		
	if($("#bagsHandOverIataCode").val()!=0)
			{
			//var json={"iataCode":$("#bagsHandOverIataCode").val(),"branchType":"ORG"};
		var json={"IataCode":"BLR","BranchType":"ORG"};
			$.ajax({
				//url: "http://14.192.16.55:8080/plt/bagListForCdHandover",
				url: "http://10.109.230.99/PLTAIRWebAPI/api/AirColoaderBagList",
//				 dataType: "json",
				type: "post",
//				 jsonp: "callback",
				 dataType: "json",
				 data:JSON.stringify(json),
//			    contentType: 'application/json',
			    processData: false,
			    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true;
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
	                 console.log(httpObj);
	                 console.log(textStatus);
	                 console.log(response);
			         $.unblockUI();
			     } ,
				success: function (response) {
					$.unblockUI();
					console.log("handover res "+response);
				/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});*/
					if(bagsHandoverTableVar==null)
					{
						bagsHandoverTableVar= $("#bagsHandoverTable").DataTable({
							columns:[
								{data : 'bagNumber'},
								{data:'cdNumber'},
								{data:'coLoaderId'},
								{data:'airMode'},
								{data:'flightNumber'},
								{data : 'bagOrigin'},
								{data:'bagDestination'},
								{data:'bagTotalWeight'},
								{data:'nextDestination'},
								{data:'isHeavy'},
								{data : 'isDg'},
								{data:'originCd'},
								{data:'destinationCd'},
								{data:'originCode'},
								{data:'originCodeName'},
								{data:'destinationCode'},
								{data:'destinationCodeName'},
								{data:'etd'},
								{data:'eta'},
								{data:'bagCreatedDate'}
							],
							data:response.data,
							retrieve:true,
							fixedHeader:{
								header:true,
								footer:false
							},
							paging: true,
			                searching: true,
			                ordering: true,
			                scrollX: true,
			                scrollCollapse:true,
			                scrollY: "330px", 
			                responsive: true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
						});
									
					}
					else
					{
						bagsHandoverTableVar.clear().draw();
						bagsHandoverTableVar.rows.add(response.data); // Add new data
						bagsHandoverTableVar.columns.adjust().draw(); 
					}

					
				}
			});	

			
			}
		
	});
	
	$("#retFromColBtn").click(function(){
	     if($("#retFromColId").val()=="" || $("#retFromColId").val()==undefined)
	    	 {
	    	 alert("Please Enter Coloader Id");
	    	 }
	     else if( $("#retIataCode").val()==0)
	     {
		alert("Please Select Iata Code")
	     }
	     else
	    	 {
	    	 var json={
	    			 coLoaderId:$("#retFromColId").val(),
	    			 iataCode:$("#retIataCode").val(),
	    			 branchType:"DES"
	    	 }
	    	 $.ajax({
					url: "http://14.192.16.55:8080/plt/bagListBasedOnVendorAndVehicle",


					type: "post",

					 dataType: "json",
					 data:JSON.stringify(json),

				    processData: false,
				    crossDomain: true,
					beforeSend:function(){
						$.blockUI({ message : "Please wait..." });  return true;
					},
					error:function(httpObj, textStatus, response){
				         alert("Error");
		                 console.log(httpObj);
		                 console.log(textStatus);
		                 console.log(response);
				         $.unblockUI();
				     } ,
					success: function (response) {
						$.unblockUI();
						
						if(response.status)
							{
						$("#retFromColTable  tbody").html("");
						for(var i=0;i<response.data.length;i++)
							{
							console.log(response.data[i]);
						$("#retFromColTable  tbody").append("<tr><td>"+response.data[i]+"</td></tr>");
							}
					 if(retFromColTableVar==null)
						 {
						 retFromColTableVar= $("#retFromColTable").DataTable({});
						 }
				     }
					  else
						{
						  alert("No bags available");
						}
					}
				});	

	    	 
	    	 }
	
		
	})
	
	$("#flightDetailsUpdate").click(function(){
		
	
	if(confirm("Are you sure to update the data?\n It takes bit time to update the data."))
		{
			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveAirlineDetails",
				//dataType: "json",
				type: "get",
				
			    dataType: "jsonp",
			    processData: false,
			    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				success: function (response) {
					$.unblockUI();
					if(response.status)
						{
						alert("Successfully Updated");
						showAirlineDetails($("#iataCodes").val())
						}
					
				}
			});		
		}

	
	
	});
	
	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
		$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
		});

	
	
		
		//showing the iata Master Table
		$("#iataMasterTab").click(function(){
			
			$.ajax({
				url: "http://14.192.16.55:8080/plt/pullIataMaster",

				type: "get",

				 dataType: "json",

			    processData: false,
			    crossDomain: true,
			   /* headers: {
			        'Access-Control-Allow-Credentials' : true,
			        'Access-Control-Allow-Origin':'*',
			        'Access-Control-Allow-Methods':'GET',
			        'Access-Control-Allow-Headers':'application/json',
			      },*/
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true;
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                     console.log(httpObj);
                     console.log(textStatus);
                     console.log(response);
			         $.unblockUI();
			     } ,
				success: function (response) {
					$.unblockUI();
					console.log(response);
				/*	$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});*/
					if(iataTable==null)
					{
						iataTable = $("#iataMasterTable").DataTable({
							columns:[
								{data : 'iataId'},
								{data:'iataCode'},
								{data:'iataDescription'},
								{data:'branchCode'},
								{data:'branchName'}
							],
							data:response.data,
							retrieve:true,
							fixedHeader:{
								header:true,
								footer:false
							},
							paging: true,
			                searching: true,
			                ordering: true,
			                scrollX: true,
			                scrollCollapse:true,
			                scrollY: "330px", 
			                responsive: true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
						});
									
					}
					else
					{
						iataTable.clear().draw();
						iataTable.rows.add(response.data); // Add new data
						iataTable.columns.adjust().draw(); 
					}

					
				}
			});	
			
		})

	//vehicle master table	
	$("#vehicleMasterTab").click(function(){
		$.ajax({
			url: "http://14.192.16.55:8080/plt/pullVehicleMaster",

			// dataType: "json",
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			error:function(e){
		         alert("Error");
		         console.log(e);
		         $.unblockUI();
		     } ,

			success: function (response) {
				console.log(response);
				
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
				if(vehicleMasterTable==null)
				{
					vehicleMasterTable = $("#vehicleMasterTable").DataTable({
						columns:[
							{data : 'vehicleNumber'},
							{data:'routeCd'},
							{data : 'vendorBranch'},
							{data:'vendorCode'},
							{data : 'vehicleTons'},
							{data : 'vehicleMake'}

						],
						data:response.data,
						retrieve:true,
						fixedHeader:{
							header:true,
							footer:false
						},
						paging: true,
		                searching: true,
		                ordering: true,
		                scrollX: true,
		                scrollCollapse:true,
		                scrollY: "330px", 
		                responsive: true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					vehicleMasterTable.clear().draw();
					vehicleMasterTable.rows.add(response.data); // Add new data
					vehicleMasterTable.columns.adjust().draw(); 
				}

				
			}
		});	
		
	})

		//vendor master table
		$("#vendorMasterTab").click(function(){
			$.ajax({
				url: "http://14.192.16.55:8080/plt/pullVendorMasterData",

				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});
					console.log(response);

					if(vendorMasterTable==null)
					{
						vendorMasterTable = $("#vendorMasterTable").DataTable({
							columns:[
								{data : 'branchCode'},
								{data:'vendorCode'},
								{data:'vendorName'},
								{data:'sapVendorCd'},
							],
							data:response.data,
							retrieve:true,
							fixedHeader:{
								header:true,
								footer:false
							},
							paging: true,
			                searching: true,
			                ordering: true,
			                scrollX: true,
			                scrollCollapse:true,
			                scrollY: "330px", 
			                responsive: true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
						});
									
					}
					else
					{
						vendorMasterTable.clear().draw();
						vendorMasterTable.rows.add(response.data); // Add new data
						vendorMasterTable.columns.adjust().draw(); 
					}

					
				}
			});
			
		
			
		})
		
		
		//coloaderconfig master table
	$("#coloaderConfigMasterTab").click(function(){
		$.ajax({
			url: "http://14.192.16.55:8080/plt/pullColoaderConfigMaster",

			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				console.log(response);
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
				if(coloaderConfigMasterTable==null)
				{
					coloaderConfigMasterTable = $("#coloaderConfigMasterTable").DataTable({
						columns:[
							{data : 'configCode'},
							{data:'configName'},
						],
						data:response.data,
						retrieve:true,
						fixedHeader:{
							header:true,
							footer:false
						},
						paging: true,
		                searching: true,
		                ordering: true,
		                scrollX: true,
		                scrollCollapse:true,
		                scrollY: "330px", 
		                responsive: true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					coloaderConfigMasterTable.clear().draw();
					coloaderConfigMasterTable.rows.add(response.data); // Add new data
					coloaderConfigMasterTable.columns.adjust().draw(); 
				}

				
			}
		});

		
	})		
			
	//coloader master table
	$("#coloaderMasterTab").click(function(){
		$.ajax({
			url: "http://14.192.16.55:8080/plt/pullColoaderMaster",

		
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
				if(coloaderMasterTable==null)
				{
					coloaderMasterTable = $("#coloaderMasterTable").DataTable({
						columns:[
							{data : 'vendorCode'},
							{data:'vendorName'}
						],
						data:response.data,
						retrieve:true,
						fixedHeader:{
							header:true,
							footer:false
						},
						paging: true,
		                searching: true,
		                ordering: true,
		                scrollX: true,
		                scrollCollapse:true,
		                scrollY: "330px", 
		                responsive: true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					coloaderMasterTable.clear().draw();
					coloaderMasterTable.rows.add(response.data); // Add new data
					coloaderMasterTable.columns.adjust().draw(); 
				}

				
			}
		});

		
	})	
			
	//airline status code table
	$("#airlineConStatusCodesTab").click(function(){
		$.ajax({
			url: "http://14.192.16.55:8080/plt/pullAirlineConStatusCodes",

			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
				if(airlineConStatusCodesTable==null)
				{
					airlineConStatusCodesTable = $("#airlineConStatusCodesTable").DataTable({
						columns:[
							{data : 'opCode'},
							{data:'description'},
							{data:'code'},
						],
						data:response.data,
						retrieve:true,
						fixedHeader:{
							header:true,
							footer:false
						},
						paging: true,
		                searching: true,
		                ordering: true,
		                scrollX: true,
		                scrollCollapse:true,
		                scrollY: "330px", 
		                responsive: true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
					});
								
				}
				else
				{
					airlineConStatusCodesTable.clear().draw();
					airlineConStatusCodesTable.rows.add(response.data); // Add new data
					airlineConStatusCodesTable.columns.adjust().draw(); 
				}

				
			}
		});

		
	})	
		
	//get the all the origin codes for flight details table
	$("#flightDetailsMasterTab").click(function(){
		
		$.ajax({
			url: "http://14.192.16.55:8080/plt/getAllIataCodes",

			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait..." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
			   if(response.status)
				   {
				   showAirlineDetails(response.data[0]);// show the flight details table with the first respone origin as the origin
				   $("#getAllIataCodes").html("");
				   for(var i=0;i<response.data.length;i++)
					   {
					   $("#iataCodes").append("<option >"+response.data[i]+"</option>");
					   }
				   
				   }
			}
		});
		
		
	})		

	// on changing the origin codes 
	//calls the function to show the flight details table for changed origin
	$("#iataCodes").change(function(e){
		console.log($("#iataCodes").val());
		showAirlineDetails($("#iataCodes").val());
		
	})

	//iata table update button
	$("#iataUpdate").click(function(){
		
		$.ajax({
			url: " http://14.192.16.55:8080/plt/saveIataMaster",

			
			type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
			beforeSend:function(){
				$.blockUI({ message : "Please wait Updating.." });  return true
			},
			error:function(httpObj, textStatus, response){
		         alert("Error");
	            console.log(httpObj);
	            console.log(textStatus);
	            console.log(response);
		         $.unblockUI();
		     },
			success: function (response) {
				$.unblockUI();
				$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
					$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
					});
				if(response.status)
					{
					alert("Successfully Updated");
					$("#iataMasterTab").trigger("click");
					
					}
				else
					alert(response.message);
			}
		});
		
	});
		
		//vehice table uptate button
		$("#vehicelUpdate").click(function(){

			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveVehicleMasterData",

				
				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					
					if(response.status)
					{
						alert("Successfully Updated");
						$("#vehicleMasterTab").trigger("click");
					
					}
				else
					alert(response.message);
					
				}
			});
			
			
		
			
		})
			

		

		//vendor table update button
		$("#vendorUpdate").click(function(){

			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveVendorCodeMaster",

				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});
					if(response.status)
					{
						alert("Successfully Updated");		
					$("#vendorMasterTab").trigger("click");
					
					}
				else
					alert(response.message);
					
				}
			});
			
			
		
			
		})

		//coloader config update table update button
		$("#coloaderConfigUpdate").click(function(){

			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveColoaderConfigMaster",

				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});
					if(response.status)
					{
						alert("Successfully Updated");
					$("#coloaderConfigMasterTab").trigger("click");
					
					}
				else
					alert(response.message);
					
				}
			});
			
			
		
		})

	//coloader master table update button	
		$("#coloaderMasterUpdate").click(function(){

			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveColoaderConfigMaster",

				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});
					if(response.status)
					{
						alert("Successfully Updated");
						$("#coloaderMasterTab").trigger("click");
					
					}
				else
					alert(response.message);
					
				}
			});
			
			
		
		})

		//airline cons status table update button
		$("#airlineConsStatusUpdate").click(function(){

			$.ajax({
				url: "http://14.192.16.55:8080/plt/saveAirlineConStatusCodes",

				type: "get",
		    dataType: "json",
		    processData: false,
		    crossDomain: true,
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
		            console.log(httpObj);
		            console.log(textStatus);
		            console.log(response);
			         $.unblockUI();
			     },
				success: function (response) {
					$.unblockUI();
					$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
						$.fn.dataTable.tables( { visible: true, api: true } ).columns.adjust();
						});
					if(response.status)
					{
						alert("Successfully Updated");
						$("#airlineConStatusCodesTab").trigger("click");
					
					}
				else
					alert(response.message);
					
				}
			});
			
			
		
		})


		

		/*------------airFlight starts---------------*/
		$("#airFlightLi").click(function(){
			//console.log("in airflight_info");
			$.ajax({
				url : "pullAirFlightWeightMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                    console.log(httpObj);
                    console.log(textStatus);
                    console.log(response);
			         $.unblockUI();
			     } ,
				success : function(Response)
				{
					$.unblockUI();
					//console.log(Response.msg);
					if(airFlightTb==null)
					{
						airFlightTb = $("#airflight_table").DataTable({
							columns : [
								{ data : 'vendorId'},
								{ data : 'vendorCode'},
								{ data : 'flightId'},
								{ data : 'flightDesc'},
								{ data : 'wtAllowed'},
							 ],
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                fnAdjustColumnSizing:true,
			                "lengthMenu" : [[10,25,50,-1], [10, 25, 50, "All"]]		
						});
					}
					else
					{
						airFlightTb.clear().draw();
						airFlightTb.rows.add(Response.data); // Add new data
						airFlightTb.columns.adjust().draw(); 
					} 
//					airFlightTb.fnAdjustColumnSizing();

				}
				
			});
			
		});
	    /*------------airFlight ends---------------*/
		
		/*------------airport starts---------------*/
		$("#airportLi").click(function(){
			//console.log("in airflight_info");
			$.ajax({
				url : "pullAirportCodesMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                    console.log(httpObj);
                    console.log(textStatus);
                    console.log(response);
			         $.unblockUI();
			     } ,
				success : function(Response)
				{
					$.unblockUI();
					//console.log(Response.msg);
					if(airportTb==null)
					{
						airportTb = $("#airport_table").DataTable({
							columns:[
								{data : 'airportId'},
								{data : 'airportCode'},
								{data : 'airportDescription'}
									
							],
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							},
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]		
						});
									
					}
					else
					{
						airportTb.clear().draw();
						airportTb.rows.add(Response.data); // Add new data
						airportTb.columns.adjust().draw(); 
					} 
					airportTb.columns.adjust().draw();
				}
				
			});
			
		});
		/*------------airport ends---------------*/
		
		/*------------content starts---------------*/
		$("#contentLi").click(function(){
			$.ajax({
				url : "pullContentMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                    console.log(httpObj);
                    console.log(textStatus);
                    console.log(response);
			         $.unblockUI();
			     } ,
				success : function(Response)
				{
					$.unblockUI();
					if(contentTb==null)
						{
						contentTb = $("#content_table").DataTable({
							columns :[
								{ data : "dangerousId"},
								{ data : "dangerousDescription"}
							],
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
						});
						}
					else
						{
						contentTb.clear().draw();
						contentTb.rows.add(Response.data);
						contentTb.columns.adjust().draw();
						}
					contentTb.columns.adjust();
				}
			});
		});
		/*------------content ends---------------*/
		
		/*------------dangerouscode starts---------------*/
		$("#dangerouscodeLi").click(function(){
			console.log("in");
			$.ajax({
				url : "pullDangerousCodesMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                    console.log(httpObj);
                    console.log(textStatus);
                    console.log(response);
			         $.unblockUI();
			     } ,
				success : function(Response){
					$.unblockUI();
					if(dangerouscodeTb==null)
						{
						dangerouscodeTb = $("#dangerouscode_table").DataTable({
							columns : [
								{ data : "dangeorusId"},
								{ data : "dangeorusCode"},
								{ data : "dangeorusDesc"}
							],
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						dangerouscodeTb.clear().draw();
						dangerouscodeTb.rows.add(Response.data);
						dangerouscodeTb.columns.adjust().draw();
						}
					dangerouscodeTb.columns.adjust();
				}
			});
		});
		/*------------dangerouscode ends---------------*/
		
		/*------------pincode starts---------------*/
		$("#pincodeLi").click(function(){
			console.log("in");
			$.ajax({
				url : "getPincodeMasterDataForPortal",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error:function(httpObj, textStatus, response){
			         alert("Error");
                    console.log(httpObj);
                    console.log(textStatus);
                    console.log(response);
			         $.unblockUI();
			     } ,
				success : function(Response){
					$.unblockUI();
					if(pincodeTb==null)
						{
						pincodeTb = $("#pincode_table").DataTable({
							columns : [
								{ data : "pinCode"},
								{ data : "isAirServicable"},
								{ data : "isAirServicableDel"},
								{ data : "servicable"},
								{ data : "branchCode"},
								{ data : "locationName"},
								{ data : "active"},
								{ data : "areaId"},
								{ data : "pincodeType"},
								{ data : "serviceType"},
								{ data : "version"}
							],
							columnDefs : [
						        { targets : [1],
						          render : function (data, type, row) {
						             return data == '1' ? 'Y' : 'N'
						          }  
						        },
						        {
						        	targets : [2],
						        	render : function (data, type, row)
						        	{
						        		return data == '1' ? 'Y' : 'N'
						        	}
						        }
						   ],
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						pincodeTb.clear().draw();
						pincodeTb.rows.add(Response.data);
						pincodeTb.columns.adjust().draw();
						}
				}
			});
		});
		/*------------pincode ends---------------*/
		

		
	    /*---------------airflight_update starts--------*/
		$("#airflight_update").click(function(){
			console.log("airflight update")
			$.ajax({
				url : "downloadAirFlightWeightMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				    },
				    error:function(httpObj, textStatus, response){
				         alert("Error");
	                     console.log(httpObj);
	                     console.log(textStatus);
	                     console.log(response);
				         $.unblockUI();
				     } ,
				success : function(Response)
				{
					$.unblockUI();
					if(Response.status)
						{
						alert("Updated Successfully");
						$("#airFlightLi").trigger("click");
						}
					else
					{
						alert("Failed To Update");
					}

				}

			});
		});
	    /*----------------airflight_update ends-----------*/
		
		 /*---------------dangerouscode_update starts--------*/
		$("#dangerouscode_update").click(function(){
		//	console.log("dangerouscode update")
			$.ajax({
				url : "downloadDangerousCodesMaster",
				method : "post",
				dataType : "json",
				beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				    },
				    error:function(httpObj, textStatus, response){
				         alert("Error");
	                     console.log(httpObj);
	                     console.log(textStatus);
	                     console.log(response);
				         $.unblockUI();
				     } ,
				success : function(Response)
				{
					$.unblockUI();
					if(Response.status)
					{
					alert("Updated Successfully");
					$("#dangerouscodeLi").trigger("click");
					}
					else
					{
						alert("Failed To Update");
					}


				}

			});
		});
	    /*----------------dangerouscode_update ends-----------*/
		
		/*----------------content_update starts-----------*/
		$("#content_update").click(function(){
				$.ajax({
					url : "saveContentMaster",
					method : "post",
					dataType : "json",
					beforeSend:function(){
						$.blockUI({ message : "Please wait Updating.." });  return true
					    },
					    error:function(httpObj, textStatus, response){
					         alert("Error");
		                     console.log(httpObj);
		                     console.log(textStatus);
		                     console.log(response);
					         $.unblockUI();
					     } ,
					success : function(Response)
					{
						$.unblockUI();
						if(Response.status)
						{
						alert("Updated Successfully");
						$("#contentLi").trigger("click");
						}
						else
						{
							alert("Failed To Update");
						}


					}

				});
			});
		    /*----------------content_update ends-----------*/
		
		/*----------------airport_update starts-----------*/
		$("#airport_update").click(function(){
				$.ajax({
					url : "downloadAirportCodesMaster",
					method : "post",
					dataType : "json",
					beforeSend:function(){
						$.blockUI({ message : "Please wait Updating.." });  return true
					    },
					    error:function(httpObj, textStatus, response){
					         alert("Error");
		                     console.log(httpObj);
		                     console.log(textStatus);
		                     console.log(response);
					         $.unblockUI();
					     } ,
					success : function(Response)
					{
						$.unblockUI();
						if(Response.status)
						{
						alert("Updated Successfully");
						$("#airportLi").trigger("click");
						}
						else
						{
							alert("Failed To Update");
						}


					}

				});
			});
		    /*----------------airport_update ends-----------*/
		
		/*----------------pincode_update starts-----------*/
		$("#pincode_update").click(function(){
			
			if(confirm("Are you sure to update the data?\n It takes bit time to update the data."))
			{
				$.ajax({
					url : "pincodeupdate",
					method : "post",
					dataType : "json",
					beforeSend:function(){
					$.blockUI({ message : "Please wait Updating.." });  return true
				    },
				    error:function(httpObj, textStatus, response){
				         alert("Error");
	                     console.log(httpObj);
	                     console.log(textStatus);
	                     console.log(response);
				         $.unblockUI();
				     } ,
					success : function(Response)
					{
						$.unblockUI();
						if(Response.status)
						{
						alert("Updated Successfully");
						$("#pincodeLi").trigger("click");
						}
						else
						{
							alert("Failed To Update");
						}

					}
                
				});
		}
			});
		    /*----------------pincode_update ends-----------*/
		
		
	      /*------------------createdBags starts------------*/
		$("#getBagData").click(function(){
			console.log("in");
			if(($("#bagDate").val() == null ||  $("#bagDate").val() == "") && ($("#bagNumber").val() == null || $("#bagNumber").val() =="" ))
				{
				alert("Enter the details");
				}
			
			else /*if($("#bagDate").val()!=null || $("#bagNumber").val()!=null )*/
				{
				if(($("#bagDate").val()!=null || $("#bagDate").val()!="") && (( $("#bagNumber").val()==null || $("#bagNumber").val()== "" )))
					{
					console.log("here date is not null but bag no. is null");
					Data="bagDate="+$("#bagDate").val();
					}
				else if(($("#bagNumber").val()!=null || $("#bagNumber").val()!="")&& ( $("#bagDate").val()==null || $("#bagDate").val()== "" ))
					{
					//Url="http://localhost:8082/plt/getCreatedBagsData?bagNumber=";
					Data="bagNumber="+ $.trim($("#bagNumber").val());
					}
				else if($("#bagDate").val()!=null &&  $("#bagNumber").val()!=null)
					{
					console.log("here both are   not null ");
					Data="bagDate="+$("#bagDate").val()+"&bagNumber="+$.trim($("#bagNumber").val());
					}
				
			$.ajax({
//				url : "http://localhost:8082/plt/getCreatedBagsData",
//				data : "bagDate="+$("#bagDate").val()+"&bagNumber="+ $("#bagNumber").val(),
				url : "http://14.192.16.55:8080/plt/getCreatedBagsData",
				data : Data,
				method : "post",
				dataType : "json",
				 processData: false,
				    crossDomain: true,
				
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(Response){
					$.unblockUI();
					console.log(Response.status);
					
					if(createdBagsTb==null)
						{
						createdBagsTb = $("#createdBags_table").DataTable({
							columns : [
								{ data : "bagNumber",
									
									/*"render": function ( data, type, full, meta) {
						                 return '<a href='+link+'></a>';},
						 */
								"render": function ( data, type, full, meta ) {
							    return '<a  onclick=showConModal(this) style="color:#1976D2;">'+data+'</a>';
						          }
									
								},
								{ data : "bagOrigin"},
								{ data : "bagTotalWeight"},
								{ data : "bagDestination"},
					 			{ data : "bagLockNumber"},
								{ data : "bagType"},
								{ data : "branchCode"},
								{ data : "isHeavy"},
								{ data : "isInDangerList"},
								{ data : "noOfPieces"},
								{ data : "isCdUpdated"},
								{ data : "isDebaggingCompleted"},
								{ data : "isPartiallyDebagged"},
								{ data : "isGatePassInCreated"},
								{ data : "isRetrieved"},
								{ data : "isRejected"}
							],
							columnDefs : [
								{
									targets : [ 2 ],
									render : function(data, type, row) {
										return data.toFixed(2);
									}
								},

								{
									targets : [ 7, 8 ],
									render : function(data, type, row) {
										return data == '1' ? 'True' : 'False'
									}
								},
								{
									targets : [ 10, 11, 12, 13, 14, 15 ],
									render : function(data, type, row) {
										return data == '1' ? 'True' : 'False'
									}
								}

							],

							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						createdBagsTb.clear().draw();
						createdBagsTb.rows.add(Response.data);
						createdBagsTb.columns.adjust().draw();
						}
						
				}
			});
				}
		});
		
		
		$("#bagDate").datepicker({
			dateFormat:'yy-mm-dd',
			maxDate:new Date()
		});
		   	   // endDate: moment(),
		   	    
		   

		$("#bagNumberTb").click(function(){
			//console.log("bagNumberTb");
			alert("bagNumberTb");
		});
		
		/*$("#getPacketsBtn").click(function(){
			console.log("in getPacketsBtn function");
			$("#PacketDisplay").modal("show");
			$.ajax({
				url : "http://14.192.16.55:8080/plt/getCreatedBagPackets",
				data : "conNumber="+conNumber,
				dataType : "json",
				method : "post",
				 processData: false,
				    crossDomain: true,
				    beforeSend:function(){
						$.blockUI({ message : "Please wait..." });  return true;
					},
					error:function(httpObj, textStatus, response){
				         alert("Error");
		                 console.log(httpObj);
		                 console.log(textStatus);
		                 console.log(response);
				         $.unblockUI();
				     } ,
					
				success :function(Response)
				{
					$.unblockUI();
					console.log(Response);

					if(PacketTb==null)
					{
						PacketTb = $("#packet_table").DataTable({
						columns : [
							{ data : "pktNo"},
							{ data : "scanned"},
							{ data : "manuallyScanned"},
							{ data : "scanTime"},
							{ data : "pktWt"}
							
						],
						
						columnDefs : [
					        { targets : [1],
					          render : function (data, type, row) {
					             return data == '1' ? 'Y' : 'N'
					          }  
					        },
					        {
					        	targets : [2],
					        	render : function (data, type, row)
					        	{
					        		return data == '1' ? 'Y' : 'N'
					        	}
					        }
					   ],
						data : Response.data,
						retrieve : true,
						responsive: true,
						fixedHeader : {
							header : true,
							footer : false
						}, 
						paging : true,
		                searching : true,
		                ordering : true,
		                scrollX : true,
		                scrollCollapse : true,
		                scrollY : "330px", 
		                responsive : true,
		                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
				
					});
					}
				else
					{
					PacketTb.clear().draw();
					PacketTb.rows.add(Response.data);
					PacketTb.columns.adjust().draw();
					}
			}
		});	

		});
		*/
		$("#packetCloseBtn").click(function(){
			$("#PacketDisplay").modal("hide");
			$("#BagNoDisplay").modal("show");
			
		});
		$("#PacketDisplayClose").click(function(){
			$("#PacketDisplay").modal("hide");
			$("#BagNoDisplay").modal("show");
			
		});
		/*$("#createdBagsLi").click(function(){
			console.log("createdBagsLi clicked")
			$("#createdBags_info").show();
		});
		*/  /*------------------createdBags ends------------*/
		

		/*---------------------debaggedBags starts------------*/
		
		$("#debaggedbagDate").datepicker({
			dateFormat:'yy-mm-dd',
				maxDate: new Date()
		});
		
		$("#getdebaggedBagData").click(function(){
			console.log("in");
			if( ($("#debaggedbagNumber").val()==null  ||  $("#debaggedbagNumber").val()=="") && ($("#debaggedbagDate").val()==null  ||  $("#debaggedbagDate").val()==""))
			{
			alert("Please enter the details");
			}
		
		else /*if($("#bagDate").val()!=null || $("#bagNumber").val()!=null )*/
			{
			if(($("#debaggedbagDate").val()!=null || $("#debaggedbagDate").val()!="") && ( $("#debaggedbagNumber").val()==null || $("#debaggedbagNumber").val()== "" ))
				{
				//Url="http://localhost:8080/plt/getCreatedBagsData?bagDate=";
				console.log("here date is not null but bag no. is null");
				Data="bagDate="+$("#debaggedbagDate").val();
				}
			else if(($("#debaggedbagNumber").val()!=null || $("#debaggedbagNumber").val()!="") && ( $("#debaggedbagDate").val()==null || $("#debaggedbagDate").val()== "" ))
				{
				console.log("here date is  null AND bag no. NOT is null");
				//Url="http://localhost:8080/plt/getCreatedBagsData?bagNumber=";
				
				Data="bagNumber="+ $.trim($("#debaggedbagNumber").val());
				}
			else if($("#debaggedbagDate").val()!=null &&  $("#debaggedbagNumber").val()!=null)
				{
				console.log("here both are   not null ");
				Data="bagDate="+$("#debaggedbagDate").val()+"&bagNumber="+ $.trim($("#debaggedbagNumber").val());
				}
			console.log("DATA = "+Data);
			$.ajax({
//				url : "http://localhost:8082/plt/getCreatedBagsData",
//				data : "bagDate="+$("#bagDate").val()+"&bagNumber="+ $("#bagNumber").val(),
//				url : "http://localhost:8080/plt/debaggedBags",
				url : "http://14.192.16.55:8080/plt/debaggedBags",
				data : Data,
				method : "post",
				dataType : "json",
				 processData: false,
				    crossDomain: true,
				
				beforeSend:function(){
					$.blockUI({ message : "Please wait..." });  return true
				},
				error : function(httpObj, textStatus, response) {
					alert("Error");
					console.log(httpObj);
					console.log(textStatus);
					console.log(response);
					$.unblockUI();
				},
				success : function(Response){
					$.unblockUI();
					console.log(Response.status);
					/*if(Response.status)
						{*/
					if(debaggedBagsTb==null)
						{
						debaggedBagsTb = $("#debaggedBags_table").DataTable({
					     columns : [
								{ data : "bagNumber",
									
									/*"render": function ( data, type, full, meta) {
						                 return '<a href='+link+'></a>';},
						 */"render": function ( data, type, full, meta ) {
							       /*debaggedbagNumber = data;*/
							           // console.log(bagNumber);
							    return '<a  onclick=showdebaggedConModal(this) style="color:#1976D2;">'+data+'</a>';
						          }
									
								},
								{ data : "empCode"},
								{ data : "debaggingType"},
								{ data : "tabUserId"},
					 			{ data : "debaggingCompleted"},
							],	
							data : Response.data,
							retrieve : true,
							fixedHeader : {
								header : true,
								footer : false
							}, 
							paging : true,
			                searching : true,
			                ordering : true,
			                scrollX : true,
			                scrollCollapse : true,
			                scrollY : "330px", 
			                responsive : true,
			                "lengthMenu": [[10,25,50,-1], [10, 25, 50, "All"]]
					
						});
						}
					else
						{
						debaggedBagsTb.clear().draw();
						debaggedBagsTb.rows.add(Response.data);
						debaggedBagsTb.columns.adjust().draw();
						}
						}
				//}
			});
				}
		});

		$("#debaggedpacketCloseBtn").click(function(){
			$("#debaggedPacketDisplay").modal("hide");
			$("#debaggedBagNoDisplay").modal("show");
			
		});
		$("#debaggedPacketDisplayClose").click(function(){
			$("#debaggedPacketDisplay").modal("hide");
			$("#debaggedBagNoDisplay").modal("show");
			
		});
		/*---------------------debaggedBags ends------------*/
		
});