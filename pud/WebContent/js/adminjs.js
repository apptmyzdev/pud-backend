/**
 * 
 */
var pushPickupsTableObj;
var openPkConsData;
var pushconsArr = [];
var currentApkVer;
var todelRowIndex;
var pullPickupsTableObj;
var pulldeliveriesTableObj
$(document).ready(function () {
	var charttitle = "Today's Hourly Pickups";
	var delChartTitle = "Today's Hourly Deliveries"

	myVar = setInterval(getCountFun, 90000);
	function getCountFun() {
		$.ajax({
			url: "loggedInUsers",
			dataType: "json",
			type: "get",
			success: function (response) {
				var log_val = $(".statvalueone").text();

				if (response.data.logList.length > Number(log_val)) {
					$(".statvalueone").text(response.data.logList.length);
					$(".statvalueone").addClass("statvalueone");
					$("#snackbar").text("A new login has been detected!");
					var x = document.getElementById("snackbar")
					x.className = "show";
					setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
				}

				else {
					$(".statvalueone").text(response.data.logList.length);
					$(".statvalueone").addClass("statvalueone");
				}

				$("#value_two").text(response.data.todaysPickups);
				$("#attemptPk").text(response.data.attemptedPickups);
				$("#successPk").text(response.data.successPickups);
				$("#pendingPk").text(response.data.pendingPickups);
				$("#value_four").text(response.data.totRegPickups);

				$("#value_three").text(response.data.todaysDeliveries);
				$("#attemptDel").text(response.data.delsAttempted);
				$("#successDel").text(response.data.delsSuccess);
				$("#pendingDel").text(response.data.delsPending);
				$("#logDiv").children().remove();
				for (var i = 0; i < response.data.auditsList.length; i++) {

					audits = '<div class=logPara>' +
						'<p><i class="fa fa-clock-o"></i> :' + response.data.auditsList[i].timeStamp + '</p>' +
						'<p><i class="fa fa-user"></i> :' + response.data.auditsList[i].userId + '</p>' +
						'<p><i class="fa fa-gears"></i> :' + response.data.auditsList[i].serviceType + '</p>' +
						'<p><i class="fa fa-reply"></i> :' + response.data.auditsList[i].serviceResponse + '</p></div>';
					$("#logDiv").append(audits);
				}
				var res_data = response.data.logList;
				var arrdata = response.data.monthlySPickupsList;
				/*hourly pickups chart begins*/
				Highcharts.chart('barChart', {
					chart: {
						type: 'column'
					},
					credits: {
						enabled: false
					},
					title: {
						text: charttitle,
						style: {
							color: 'rgb(242,88,34)',
							fontFamily: '"Source Sans Pro", sans-serif',
							fontSize: '14px'
						}
					},
					//				    subtitle: {
					//				        text: 'Source: WorldClimate.com'
					//				    },
					xAxis: {
						categories: [
							'00:00-00:59',
							'01:00-01:59',
							'02:00-02:59',
							'03:00-03:59',
							'04:00-04:59',
							'05:00-05:59',
							'06:00-06:59',
							'07:00-07:59',
							'08:00-08:59',
							'09:00-09:59',
							'10:00-10:59',
							'11:00-11:59',
							'12:00-12:59',
							'13:00-13:59',
							'14:00-14:59',
							'15:00-15:59',
							'16:00-16:59',
							'17:00-17:59',
							'18:00-18:59',
							'19:00-19:59',
							'20:00-20:59',
							'21:00-21:59',
							'22:00-22:59',
							'23:00-23:59'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Pickups Done'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
						'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: [{
						name: 'Pickups Count',
						data: arrdata
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom',
									layout: 'horizontal'
								},
								yAxis: {
									labels: {
										align: 'left',
										x: 0,
										y: -5
									},
									title: {
										text: null
									}
								},
								subtitle: {
									text: null
								},
								credits: {
									enabled: false
								}
							}
						}]
					}
				});
				/*hourly pickups chart ends*/
				var deldata = response.data.hourlyDeliveries;
				/*hourly deliveries chart begins*/
				Highcharts.chart('delChart', {
					chart: {
						type: 'column'
					},
					credits: {
						enabled: false
					},
					title: {
						text: delChartTitle,
						style: {
							color: 'rgb(242,88,34)',
							fontFamily: '"Source Sans Pro", sans-serif',
							fontSize: '14px'
						}
					},
					//				    subtitle: {
					//				        text: 'Source: WorldClimate.com'
					//				    },
					xAxis: {
						categories: [
							'00:00-00:59',
							'01:00-01:59',
							'02:00-02:59',
							'03:00-03:59',
							'04:00-04:59',
							'05:00-05:59',
							'06:00-06:59',
							'07:00-07:59',
							'08:00-08:59',
							'09:00-09:59',
							'10:00-10:59',
							'11:00-11:59',
							'12:00-12:59',
							'13:00-13:59',
							'14:00-14:59',
							'15:00-15:59',
							'16:00-16:59',
							'17:00-17:59',
							'18:00-18:59',
							'19:00-19:59',
							'20:00-20:59',
							'21:00-21:59',
							'22:00-22:59',
							'23:00-23:59'
						],
						crosshair: true
					},
					yAxis: {
						min: 0,
						title: {
							text: 'Pickups Done'
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
						'<td style="padding:0"><b>{point.y}</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
					},
					plotOptions: {
						column: {
							pointPadding: 0.2,
							borderWidth: 0
						}
					},
					series: [{
						name: 'Deliveries Count',
						data: deldata
					}],
					responsive: {
						rules: [{
							condition: {
								maxWidth: 900
							},
							chartOptions: {
								legend: {
									align: 'center',
									verticalAlign: 'bottom',
									layout: 'horizontal'
								},
								yAxis: {
									labels: {
										align: 'left',
										x: 0,
										y: -5
									},
									title: {
										text: null
									}
								},
								subtitle: {
									text: null
								},
								credits: {
									enabled: false
								}
							}
						}]
					}
				});
				/*hourly deliveries chart ends*/
				setTimeout(function () {
					var l_table = $('#logged_in_users_table').DataTable({
						columns: [
							{ data: 'imei' },
							{ data: 'userId' },
							{ data: 'apkVersion' },
							{ data: 'loginTime' },
							{ data: 'location' }
						],
						data: res_data,
						retrieve: true,
						fixedHeader: {
							header: true,
							footer: true
						},
						paging: false,
						searching: false,
						ordering: true,
						scrollX: true,
						"bScrollInfinite": false,
						"bScrollCollapse": false,
						"sScrollY": "150px",
						"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

					});

					l_table.clear().draw();
					l_table.rows.add(response.data.logList); // Add new data
					l_table.columns.adjust().draw(); // Redraw the DataTable
					//$("#logged_in_users_table  thead  tr  th").addClass("afterload_logs");
				}, 2000);
			},
			complete: function () {
				$("#logged_in_users_table").css({ "width": "100%", "height": "100px", "overflow-y": "scroll", "font-size": "12px", "font-family": "'Titillium Web', sans-serif", " background-color": "#1fb5ad" });
				$('body #ulist').find('.dataTables_scrollBody').addClass("scrolllist");
				$('body #ulist').find('.dataTables_info').css({ "display": "none" });
				//$("#logged_in_users_table").addClass("w3-card-2");
			}
		});
	}


	$.ajax({
		url: "loggedInUsers",
		dataType: "json",
		type: "get",
		success: function (response) {
			var log_val = $(".statvalueone").text();

			if (response.data.logList.length > Number(log_val)) {
				$(".statvalueone").text(response.data.logList.length);
				$(".statvalueone").addClass("statvalueone");
			}

			else {
				$(".statvalueone").text(response.data.logList.length);
				$(".statvalueone").addClass("statvalueone");
			}

			$("#value_two").text(response.data.todaysPickups);
			$("#attemptPk").text(response.data.attemptedPickups);
			$("#successPk").text(response.data.successPickups);
			$("#pendingPk").text(response.data.pendingPickups);
			$("#value_four").text(response.data.totRegPickups);
			$("#value_three").text(response.data.todaysDeliveries);
			$("#attemptDel").text(response.data.delsAttempted);
			$("#successDel").text(response.data.delsSuccess);
			$("#pendingDel").text(response.data.delsPending);
			$("#logDiv").children().remove();

			for (var i = 0; i < response.data.auditsList.length; i++) {
				audits = '<div class=logPara>' +
					'<p><i class="fa fa-clock-o"></i> : ' + response.data.auditsList[i].timeStamp + '</p>' +
					'<p><i class="fa fa-user"></i> : ' + response.data.auditsList[i].userId + '</p>' +
					'<p><i class="fa fa-gears"></i> : ' + response.data.auditsList[i].serviceType + '</p>' +
					'<p><i class="fa fa-reply"></i> : ' + response.data.auditsList[i].serviceResponse + '</p></div>';
				$("#logDiv").append(audits);
			}

			var res_data = response.data.logList;
			var arrdata = response.data.monthlySPickupsList;
			/*hourly pickups start*/
			Highcharts.chart('barChart', {
				chart: {
					type: 'column'
				},
				credits: {
					enabled: false
				},
				title: {
					text: charttitle,
					style: {
						color: 'rgb(242,88,34)',
						fontFamily: '"Source Sans Pro", sans-serif',
						fontSize: '14px'
					}
				},
				//			    subtitle: {
				//			        text: 'Source: WorldClimate.com'
				//			    },
				xAxis: {
					categories: [
						'00:00-00:59',
						'01:00-01:59',
						'02:00-02:59',
						'03:00-03:59',
						'04:00-04:59',
						'05:00-05:59',
						'06:00-06:59',
						'07:00-07:59',
						'08:00-08:59',
						'09:00-09:59',
						'10:00-10:59',
						'11:00-11:59',
						'12:00-12:59',
						'13:00-13:59',
						'14:00-14:59',
						'15:00-15:59',
						'16:00-16:59',
						'17:00-17:59',
						'18:00-18:59',
						'19:00-19:59',
						'20:00-20:59',
						'21:00-21:59',
						'22:00-22:59',
						'23:00-23:59'
					],
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Pickups Done'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [{
					name: 'Pickups Count',
					colorByPoint: true,
					data: arrdata
				}],
				responsive: {
					rules: [{
						condition: {
							maxWidth: 900
						},
						chartOptions: {
							legend: {
								align: 'center',
								verticalAlign: 'bottom',
								layout: 'horizontal'
							},
							yAxis: {
								labels: {
									align: 'left',
									x: 0,
									y: -5
								},
								title: {
									text: null
								}
							},
							subtitle: {
								text: null
							},
							credits: {
								enabled: false
							}
						}
					}]
				}
			});
			/*hourly pickups end*/
			var deldata = response.data.hourlyDeliveries;
			/*hourly deliveries chart begins*/
			Highcharts.chart('delChart', {
				chart: {
					type: 'column'
				},
				credits: {
					enabled: false
				},
				title: {
					text: delChartTitle,
					style: {
						color: 'rgb(242,88,34)',
						fontFamily: '"Source Sans Pro", sans-serif',
						fontSize: '14px'
					}
				},
				//				    subtitle: {
				//				        text: 'Source: WorldClimate.com'
				//				    },
				xAxis: {
					categories: [
						'00:00-00:59',
						'01:00-01:59',
						'02:00-02:59',
						'03:00-03:59',
						'04:00-04:59',
						'05:00-05:59',
						'06:00-06:59',
						'07:00-07:59',
						'08:00-08:59',
						'09:00-09:59',
						'10:00-10:59',
						'11:00-11:59',
						'12:00-12:59',
						'13:00-13:59',
						'14:00-14:59',
						'15:00-15:59',
						'16:00-16:59',
						'17:00-17:59',
						'18:00-18:59',
						'19:00-19:59',
						'20:00-20:59',
						'21:00-21:59',
						'22:00-22:59',
						'23:00-23:59'
					],
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Pickups Done'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">Time interval {point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y}</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				plotOptions: {
					column: {
						pointPadding: 0.2,
						borderWidth: 0
					}
				},
				series: [{
					name: 'Deliveries Count',
					data: deldata
				}],
				responsive: {
					rules: [{
						condition: {
							maxWidth: 900
						},
						chartOptions: {
							legend: {
								align: 'center',
								verticalAlign: 'bottom',
								layout: 'horizontal'
							},
							yAxis: {
								labels: {
									align: 'left',
									x: 0,
									y: -5
								},
								title: {
									text: null
								}
							},
							subtitle: {
								text: null
							},
							credits: {
								enabled: false
							}
						}
					}]
				}
			});
			/*hourly deliveries chart ends*/
			var l_table = $('#logged_in_users_table').DataTable({
				columns: [
					{ data: 'imei' },
					{ data: 'userId' },
					{ data: 'apkVersion' },
					{ data: 'loginTime' },
					{ data: 'location' }
				],
				data: res_data,
				retrieve: true,
				fixedHeader: {
					header: true,
					footer: true
				},
				paging: false,
				searching: false,
				ordering: true,
				scrollX: true,
				"bScrollInfinite": false,
				"bScrollCollapse": false,
				"sScrollY": "150px",
				"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

			});

			l_table.clear().draw();
			//$("#logged_in_users_table  thead  tr  th").addClass("afterload_logs");
			l_table.rows.add(response.data.logList); // Add new data
			l_table.columns.adjust().draw(); // Redraw the DataTable
		},
		complete: function () {
			$("#logged_in_users_table").css({ "height": "100px", "overflow-y": "scroll", "font-size": "12px", "font-family": "'Titillium Web', sans-serif", " background-color": "#1fb5ad" });
			$('body #ulist').find('.dataTables_scrollBody').addClass("scrolllist");
			$('body #ulist').find('.dataTables_info').css({ "display": "none" });
			//$("#logged_in_users_table").addClass("w3-card-2");
		}
	});


	function format(d) {
		// `d` is the original data object for the row
		return '<div style="background-color:rgb(245,245,245);border-bottom:solid 1px rgb(242,88,34);color:grey;padding:5px;">'
			+ '<p>Con count : ' + d.conCount + '</p>'
			+ '<p>Pieces count : ' + d.pieceCount + '</p>'
			+ '</div>';
	}

	function drawRowDetails(tr, row) {
		if (row.child.isShown()) {
			// This row is already open - close it
			row.child.hide();
			tr.removeClass('shown');
		}
		else {
			// Open this row
			row.child(format(row.data())).show();
			tr.addClass('shown');
		}
	}

	$("#reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#del_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#reqBdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#bdel_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#pkSumDate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#dsum_reqdate").datepicker({
		dateFormat: 'dd/mm/yy'
	});

	$("#pushPKDate").datepicker({
		dateFormat: 'dd/mm/yy'
	});
	var d = new Date();
	var lDate = moment(d).format("DD/MM/YYYY");

	$("#pushPKDate").val(lDate);


	$(".loginlist").click(function () {
		var child = $(".nav-pills").children(':eq(1)');
		var child_one = child.children(':eq(1)');
		for (var i = 0; i < child_one.children().length; i++) {
			var re_child = child_one.children(':eq(' + i + ')');
			re_child.removeClass("active");
			/*collapsing dropdown*/
			$("#ddwn").addClass("collapsed");
			$("#ddwn").attr("aria-expanded", "false");
			$("#pdlist").removeClass("collapse in");
			$("#pdlist").addClass("collapse");
		}
	});
	/*end*/

	/*removing active class for tab-pills*/
	$("#pdlist").click(function () {
		var child = $(".nav-pills").children(':eq(0)');
		child.removeClass("active");
	});
	/*end*/

	/*----------pickups info begins--------------*/
	$("#getdata").click(function () {

		if ($("#reqdate").val() == "") {
			alert("Please enter date");
		}
		else {

			$.ajax({
				url: "getPickupsDataReport",
				dataType: "json",
				data: "&reqDate=" + $("#reqdate").val() + "&cusCode=" + $("#cuscode").val() + "&userName=" + $("#username").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						debugger;
						document.getElementById("msg_para").style.display = 'none';
						$("#example").css({ "display": "block" });

						var d_table = $('#example').DataTable({

							columns: [
								{
									"className": 'details-control',
									"orderable": false,
									"data": null,
									"defaultContent": ''
								},
								{ data: 'pickUpScheduleId' },
								{ data: 'customerCode' },
								{ data: 'customerName' },
								{ data: 'userId' },
								{ data: 'status' }
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						d_table.clear().draw();
						d_table.rows.add(response.data); // Add new data
						d_table.columns.adjust().draw(); // Redraw the DataTable

						$('#example tbody .details-control').click(function () {
							var tr = $(this).closest('tr');
							var row = d_table.row(tr);

							drawRowDetails(tr, row);


						});

					}
					else {
						$("#msg_para").text(response.message);
						document.getElementById("msg_para").style.display = 'block';
						$("#example").css({ "display": "none" });

					}


				},
				complete: function () {
					$('body #demotab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*-----------------pickups info end------------- */

	/*-----------------Device pickups Push Clicked------------- */
	
	$("#sendPickupPush").click(function () {
		
		

		if ($("#pickupScheduleId").val() == ""||$("#pickupUsername").val() == "") {
			alert("Please enter All Values");
		}else{
			
			$.ajax({
				url: "sendPushAndGetDeviceData",
				dataType: "json",
				data: "&username=" + $("#pickupUsername").val() + "&status=2" + "&pickupScheduleId=" + $("#pickupScheduleId").val(),
				type: "get",
				success: function (response) {
                        var username=$("#pickupUsername").val();
                        var pickupScheduleId=$("#pickupScheduleId").val();
					if (response.status == true) {
						debugger;var i=0;
						$("#pickuppush_msg_para").text(response.message);
						var eventSource = new EventSource("pickupUpdationServiceV2?userId="+username+"&pickupScheduleId="+$("#pickupScheduleId").val());
						
						eventSource.onmessage = function(event) {
						debugger;
						console.log("event.data "+event.data);
						if(event.data=="Pickup is in progress" || "Pickup is pending"){
							event.target.close();
							$("#pickuppush_msg_para").text(event.data);
							$("#pickupDataTable").css({ "display": "none" });
						}else if(!(event.data=="Not Yet Found")){
							$("#pickupPushPKLoader").css({ "display": "none" });
						var data=JSON.parse(event.data);
						
							event.target.close();
							console.log("data "+data);
						   $("#pickuppush_msg_para").text("Thank You For Waiting.Your data is received");
						   document.getElementById("pickupDataTable").style.display = 'block';
							var insert="";var conEntryId="";
						   $.each(data.conDetails,function(key,val){
							   conEntryId=val.conEntryId;
								insert+='<tr><td>'+val.conNumber+'</td><td>'+val.pickupScheduleId+'</td><td>'+val.userName+'</td><td>'+val.orderNo+'</td><td>'+val.pickupDate+'</td><td>'+val.noOfPackage+'</td><td>'+val.customerCode+'</td><td>'+val.paymentBasis+'</td><td>'+val.originPinCode+'</td><td>'+val.destinationPinCode+'</td><td>'+val.erpUpdated+'</td></tr>';
							   /* if(val.erpUpdated==1){
							    	insert+='<td>'+"Updated to ERP"+'</td></tr>';
							    }  else{
							    	insert+='<td><button class="btn btn-primary" onclick=pushToErp('+val.conEntryId+')>Push To Erp</button></td></tr>';
							    }*/
						   });
						   $("#devicePickupsTableTbody").html(insert);
							pullPickupsTableObj = $('#devicePickupsTable').DataTable({
							
								/*columns: [
									{ data: 'conNumber' },
									{ data: 'pickupScheduleId' },
									{ data: 'userName' },
									{ data: 'orderNo' },
									{ data: 'pickupDate' },
									{ data: 'noOfPackage' },
									{ data: 'customerCode' },
									{ data: 'paymentBasis' },
									{ data: 'originPinCode' },
									{ data: 'destinationPinCode' },
									{
										
										data: null,
										defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
									}

								],*/
								
								retrieve: true,
								fixedHeader: {
									header: true,
									footer: true
								},
								paging: true,
								searching: true,
								ordering: true,
								scrollX: true,
								"bScrollInfinite": false,
								"bScrollCollapse": false,
								"sScrollY": "200px",
								"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

							});

							
						   
						}else{
							$("#pickupPushPKLoader").css({ "display": "block" });
							i++;
							console.log("i value "+i);
							$("#pickupDataTable").css({ "display": "none" });
							if(i==60){
								$("#pickupPushPKLoader").css({ "display": "none" });
								event.target.close();
								 $("#pickuppush_msg_para").text("Sorry,We Cannot Pull Your Data..Please Try Later");
								 
							}
							
						}
						}
						document.getElementById("pickuppush_msg_para").style.display = 'block';
                     }
					else {
						debugger;
						$("#pickupDataTable").css({ "display": "none" });
						$("#pickuppush_msg_para").text(response.message);
						document.getElementById("pickuppush_msg_para").style.display = 'block';
						//$("#example").css({ "display": "none" });

					}


				}
				
			});
			
			
		}
		
		
	});
	
	
/*-----------------Device Deliveries Push Clicked------------- */
	
	$("#sendDeliveryPush").click(function () {
		
		

		if ($("#conNumber").val() == ""||$("#deliveryUsername").val() == "") {
			alert("Please enter All Values");
		}else{
			
			$.ajax({
				url: "sendPushAndGetDeviceData",
				dataType: "json",
				data: "&username=" + $("#deliveryUsername").val() + "&status=1" + "&conNumber=" + $("#conNumber").val() + "&pdcNumber=" + $("#pdcNumber").val(),
				type: "get",
				success: function (response) {
                        var username=$("#deliveryUsername").val();
                        var pdcNumber=$("#pdcNumber").val();
					if (response.status == true) {
						debugger;
						var i=0;
						$("#deliverypush_msg_para").text(response.message);
						
						var eventSource = new EventSource("deliveryUpdationServiceV2?userId="+username+"&pdcNumber="+$("#pdcNumber").val()+"&conNumber="+$("#conNumber").val());
						
						eventSource.onmessage = function(event) {
							debugger;
							console.log("event.data "+event.data);
						if(event.data=="Delivery is Not Yet Updated"){
							event.target.close();
							$("#deliveryPushPKLoader").css({ "display": "none" });
							$("#deliveryDataTable").css({ "display": "none" });
							$("#deliverypush_msg_para").text("Delivery is Not Yet Updated In Device");
							$("#deliveryDataTable").css({ "display": "none" });
							
						}else if(!(event.data=="Not Yet Found")){
						var data=JSON.parse(event.data);
						$("#deliveryPushPKLoader").css({ "display": "none" });
							event.target.close();
							console.log("data "+data);
						   $("#deliverypush_msg_para").text("Thank You For Waiting.Your data is received");
						   
						   document.getElementById("deliveryDataTable").style.display = 'block';
							var insert="";
						   $.each(data.deliveryEntity,function(key,val){
							   conEntryId=val.conEntryId;
								insert+='<tr><td>'+val.awbNo+'</td><td>'+val.transactionDate+'</td><td>'+val.deliveredTo+'</td><td>'+val.status+'</td><td>'+val.fieldEmployeeName+'</td><td>'+val.pdcNumber+'</td><td>'+val.receiverMobileNo+'</td><td>'+val.erpUpdated+'</td></tr>';
							   /* if(val.erpUpdated==1){
							    	insert+='<td>'+"Updated to ERP"+'</td></tr>';
							    }  else{
							    	insert+='<td><button class="btn btn-primary" onclick=pushToErp('+val.conEntryId+')>Push To Erp</button></td></tr>';
							    }*/
						   });
						   $("#devicedeliveriesTableTbody").html(insert);
							pulldeliveriesTableObj = $('#devicedeliveriesTable').DataTable({
							
								/*columns: [
									{ data: 'conNumber' },
									{ data: 'pickupScheduleId' },
									{ data: 'userName' },
									{ data: 'orderNo' },
									{ data: 'pickupDate' },
									{ data: 'noOfPackage' },
									{ data: 'customerCode' },
									{ data: 'paymentBasis' },
									{ data: 'originPinCode' },
									{ data: 'destinationPinCode' },
									{
										
										data: null,
										defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
									}

								],
								*/
								retrieve: true,
								fixedHeader: {
									header: true,
									footer: true
								},
								paging: true,
								searching: true,
								ordering: true,
								scrollX: true,
								"bScrollInfinite": false,
								"bScrollCollapse": false,
								"sScrollY": "200px",
								"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

							});

							//$("#deliverypush_msg_para").css({ "display": "none" });
						   
						}else{
							$("#deliveryPushPKLoader").css({ "display": "block" });
							 $("#deliveryDataTable").css({ "display": "none" });
							i++;
							console.log("i value "+i);
							if(i==60){
								$("#deliveryPushPKLoader").css({ "display": "none" });
								event.target.close();
								 $("#deliverypush_msg_para").text("Sorry,We Cannot Pull Your Data..Please Try Later");
								
							}
							
						}
						
						}
						document.getElementById("deliverypush_msg_para").style.display = 'block';
                     }
					else {
						debugger;
						$("#deliverypush_msg_para").text(response.message);
						document.getElementById("deliverypush_msg_para").style.display = 'block';
						$("#deliveryDataTable").css({ "display": "none" });
						//$("#example").css({ "display": "none" });

					}


				}
				
			});
			
			
		}
		
		
	});
	


	/*---------------force logout user js-------------------*/
	$("#logoutbtn").click(function () {
		if ($("#userName").val() == "") {
			alert("Please enter username!");
		}
		else {
			$.ajax({
				url: "logoutUser",
				data: "&userName=" + $("#userName").val(),
				dataType: "json",
				type: "get",
				beforeSend: function () {

					$.blockUI.defaults.css = {};
					$.blockUI({ message: '<div class="loader"></div><p style="color:white;font-weight:bold;">Please wait</p>' });
				},
				success: function (response) {
					$.unblockUI();
					if (response.status == true) {
						$("#snackbar").text("User logged out successfully.");
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
					}
					else {
						$("#snackbar").text(response.message);
						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
					}
				}

			});
		}
	});
	/*force logout user end */

	/*deliveries info*/

	$("#del_getdata").click(function () {
		if ($("#del_reqdate").val() == "") {
			alert("Please enter date");
		}
		else {
			$.ajax({
				url: "getDeliveriesDataReport",
				dataType: "json",
				data: "&reqDate=" + $("#del_reqdate").val() + "&agent=" + $("#del_username").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("del_msg_para").style.display = 'none';
						$("#del_info_table").css({ "visibility": "visible" });
						var del_table = $('#del_info_table').DataTable({
							columns: [
								{ data: 'conNo' },
								{ data: 'pdc' },
								{ data: 'Status' },
								{ data: 'createdTime' },
								{ data: 'assignedTo' },
								{ data: 'consigneeCode' },
								{ data: 'consignorAddress' },
								{ data: 'consigneeAddress' }
							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						del_table.clear().draw();
						del_table.rows.add(response.data); // Add new data
						del_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#del_msg_para").text(response.message);
						document.getElementById("del_msg_para").style.display = 'block';
						$("#del_info_table").css({ "visibility": "hidden" });
						$("#del_info_table").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #del_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});

	/*deliveries info end*/

	/*--------------branch pickups report------------*/
	$("#getBpks").click(function () {

		if ($("#reqBdate").val() == "") {
			alert("Please enter date");
		}
		else if ($("#branchCode").val() == "") {
			alert("Please enter branch code");
		}
		else {

			$.ajax({
				url: "getBranchPickups",
				dataType: "json",
				data: "&reqDate=" + $("#reqBdate").val() + "&branchCode=" + $("#branchCode").val() + "&userName=" + $("#userBname").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("brmsg_para").style.display = 'none';
						$("#branchPT").css({ "display": "block" });

						var dbr_table = $('#branchPT').DataTable({

							columns: [
								{
									"className": 'details-control',
									"orderable": false,
									"data": null,
									"defaultContent": ''
								},
								{ data: 'pickUpScheduleId' },
								{ data: 'customerCode' },
								{ data: 'customerName' },

								{ data: 'userId' },
								{ data: 'status' }
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						dbr_table.clear().draw();
						dbr_table.rows.add(response.data); // Add new data
						dbr_table.columns.adjust().draw(); // Redraw the DataTable

						$('#branchPT tbody .details-control').click(function () {
							var tr = $(this).closest('tr');
							var row = dbr_table.row(tr);

							drawRowDetails(tr, row);
						});

					}
					else {
						$("#brmsg_para").text(response.message);
						document.getElementById("brmsg_para").style.display = 'block';
						$("#branchPT").css({ "display": "none" });

					}
				},
				complete: function () {
					$('body #bpkTab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*--------------branch pickups report end------------*/

	/* branch deliveries report*/
	$("#bdel_data").click(function () {
		if ($("#bdel_reqdate").val() == "") {
			alert("Please enter date");
		}

		else if ($("#branchdelCode").val() == "") {
			alert("please enter branch code");
		}
		else {
			$.ajax({
				url: "getBranchDeliveries",
				dataType: "json",
				data: "&reqDate=" + $("#bdel_reqdate").val() + "&agent=" + $("#bdel_username").val() + "&branchCode=" + $("#branchdelCode").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("bdel_msg").style.display = 'none';
						$("#bdel_table").css({ "visibility": "visible" });
						var bdel_table = $('#bdel_table').DataTable({
							columns: [
								{ data: 'conNo' },
								{ data: 'pdc' },
								{ data: 'Status' },
								{ data: 'createdTime' },
								{ data: 'assignedTo' },
								{ data: 'consigneeCode' },
								{ data: 'consignorAddress' },
								{ data: 'consigneeAddress' }
							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						bdel_table.clear().draw();
						bdel_table.rows.add(response.data); // Add new data
						bdel_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#bdel_msg").text(response.message);
						document.getElementById("bdel_msg").style.display = 'block';
						$("#bdel_table").css({ "visibility": "hidden" });
						$("#bdel_table").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #bdel_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});
	/*bracnch deliveries report end*/

	/*branch pickups summmary*/
	$("#getPkSum").click(function () {

		if ($("#pkSumDate").val() == "") {
			alert("Please enter date");
		}
		else {

			$.ajax({
				url: "getBranchPickupsSummary",
				dataType: "json",
				data: "&reqDate=" + $("#pkSumDate").val(),
				type: "get",
				beforeSend: function () {
					document.getElementById('loaderDiv').style.display = 'block';
				},
				success: function (response) {
					document.getElementById('loaderDiv').style.display = 'none';
					if (response.status == true) {
						document.getElementById("pkSumMsg").style.display = 'none';
						$("#pkSumTable").css({ "display": "block" });

						var psum_table = $('#pkSumTable').DataTable({

							columns: [
								{ data: 'branch' },
								{ data: 'pickupSuccessCount' },
								{ data: 'attemptedPickups' },
								{ data: 'pendingCount' },
								{ data: 'totalPickups' },
								{
									data: 'productivity',
									render: function (data, type, row) {
										return data.toFixed(2) + '%'
									}
								}
							],
							"order": [[1, 'asc']],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						psum_table.clear().draw();
						psum_table.rows.add(response.data); // Add new data
						psum_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#pkSumMsg").text(response.message);
						document.getElementById("pkSumMsg").style.display = 'block';
						$("#pkSumTable").css({ "display": "none" });

					}
				},
				complete: function () {
					$('body #pkSumTab').find('.dataTables_scrollBody').addClass("scrolllist");

				}
			});
		}
	});
	/*branch pickups summary ends*/

	/*branch deliveries summary */
	$("#getDelSum").click(function () {
		if ($("#dsum_reqdate").val() == "") {
			alert("Please enter date");
		}

		else {
			$.ajax({
				url: "getBranchDeliveriesSummary",
				dataType: "json",
				data: "&reqDate=" + $("#dsum_reqdate").val(),
				type: "get",
				success: function (response) {

					if (response.status == true) {
						document.getElementById("delSum_msg").style.display = 'none';
						$("#delSumTable").css({ "visibility": "visible" });
						var delsum_table = $('#delSumTable').DataTable({
							columns: [
								{ data: 'branch' },
								{ data: 'totalPickups' },
								{ data: 'pickupSuccessCount' },
								{ data: 'attemptedPickups' },
								{ data: 'pendingCount' },
								{
									data: 'productivity',
									render: function (data, type, row) {
										return data.toFixed(2) + '%'
									}
								},

							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "250px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						delsum_table.clear().draw();
						delsum_table.rows.add(response.data); // Add new data
						delsum_table.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#delSum_msg").text(response.message);
						document.getElementById("delSum_msg").style.display = 'block';
						$("#delSumTable").css({ "visibility": "hidden" });
						$("#delSumTable").DataTable().destroy();
					}
				},
				complete: function () {
					$('body #delSum_tab').find('.dataTables_scrollBody').addClass("del_scrolllist");

				}
			});
		}
	});

	/*branch deliveries summary ends */

	// "render": function (data, type, row, meta) {
	// 	return type === 'display' && data.length > 25 ?
	// 		'<span title="' + data + '">' + data.substr(0, 20) + '...</span>' :
	// 		data;
	// }

	/*refreshing user and docket series data*/

	$(".cronLink").click(function () {
		$.ajax({
			url: "getDocketSeriesData",
			type: "get",
			dataType: "json",
			beforeSend: function () {
				$("#spinRow").css({
					"display": "block"
				});
			},
			success: function (response) {
				$("#spinRow").css({
					"display": "none"
				});

				$("#snackbar").text(response.message);
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
			}
		})
	})

	$(".cronLinkuser").click(function () {
		$.ajax({
			url: "userData",
			type: "get",
			dataType: "json",
			beforeSend: function () {
				$("#spinRow").css({
					"display": "block"
				});
			},
			success: function (response) {
				$("#spinRow").css({
					"display": "none"
				});

				$("#snackbar").text(response.message);
				var x = document.getElementById("snackbar")
				x.className = "show";
				setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
			}
		})
	})
	/*refreshing user and docket series data ends*/

	/*get open pickups */
	$("#getPushPickupsBtn").click(function () {
		currentApkVer = "";
		if (openPkConsData != null) {
			openPkConsData.clear().draw();
			
			$("#pushConsBtn").prop('disabled', true);
			$("#openPickupId").text("Schedule ID : ");
		}

		// console.log(ldate);
		if ($("#pushPKDate").val() == "") {
			alert("please enter date");
		}
		else if ($("#pushPKName").val() == "") {
			alert("Please enter username");
		}
		else {
			var fdate = $("#pushPKDate").val();
			var sdate = moment(fdate, "DD/MM/YYYY");
			var ldate = moment(sdate).format("YYYY-MM-DD");
			// console.log(ldate);
			console.log("before getting open pickups :" + currentApkVer);
			$.ajax({
				url: "getOpenPickupsOfUser",
				type: "get",
				dataType: "json",
				data: "&pickupDate=" + ldate + "&userName=" + $("#pushPKName").val(),
				beforeSend: function () {
					$("#pushPKLoader").css({ "display": "block" });
				},
				success: function (response) {
					$("#pushPKLoader").css({ "display": "none" });
					if (response.status == true) {
						$("#openPickupsDiv").css({ "visibility": "visible" });
						$("#openPickupsConDiv").css({ "visibility": "visible" });
						$("#pushPksResponse").css({ "display": "none" });
						currentApkVer = response.data[0].currentApkVersion;
						// console.log("after getting open pickups :" + currentApkVer);
						pushPickupsTableObj = $('#openPickupsTable').DataTable({
							columns: [
								{ data: 'pickupScheduleId' },
								{ data: 'pickupOrderNumber' },
								{ data: 'pickupDate' },
								{ data: 'userName' },
								{
									data: null,
									defaultContent: '<button class="btn btn-primary getOpenConsBtn" onclick=getOpenCons(this)>Get Cons</button>'
								}

							],
							data: response.data,
							retrieve: true,
							fixedHeader: {
								header: true,
								footer: true
							},
							paging: true,
							searching: true,
							ordering: true,
							scrollX: true,
							"bScrollInfinite": false,
							"bScrollCollapse": false,
							"sScrollY": "200px",
							"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]

						});

						pushPickupsTableObj.clear().draw();
						pushPickupsTableObj.rows.add(response.data); // Add new data
						pushPickupsTableObj.columns.adjust().draw(); // Redraw the DataTable
					}
					else {
						$("#openPickupsDiv").css({ "visibility": "hidden" });
						$("#openPickupsConDiv").css({ "visibility": "hidden" });
						// console.log(response);
						$("#pushPksResponse").text(response.message);
						$("#pushPksResponse").css({ "display": "block" });
					}
				}

			});
		}
	});
	/*get open pickups ends */
	/*pushing cons */
	$("#pushConsBtn").click(function () {
		// console.log(openPkConsData.rows().data());
		//console.log("push cons table length :"+openPkConsData.rows().data().length);
		//console.log("on push cons :"+currentApkVer);
		$("#pushedConsResponse").css({ "display": "none" });
		$("#pushedConsResponse").text("");

		for (i = 0; i < openPkConsData.rows().data().length; i++) {
			pushconsArr.push(openPkConsData.row(i).data());
		}

		if (pushconsArr.length > 0) {

			var conObj = { conDetails: pushconsArr };

			var finalObj = JSON.stringify(conObj);

			$.ajax({
				url: "pickupUpdationService",
				headers: { 'apkVersion': currentApkVer },
				type: "post",
				dataType: "json",
				data: finalObj,
				beforeSend: function () {
					$("#pushingConsLoader").text("Closing pickup...");
					$("#pushingConsLoader").css({ "display": "block" });
				},
				success: function (response) {
					$("#pushingConsLoader").css({ "display": "none" });
					// $("#pushConsBtn").prop('disabled', true);
					// $("#pushedConsResponse").text(response.message);
					// $("#pushedConsResponse").css({ "display": "block" });
					if (response.status == true) {

						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
						// pushPickupsTableObj.row().remove().draw();
						// console.log(pushPickupsTableObj.row(todelRowIndex).data());
						pushPickupsTableObj.rows(todelRowIndex).remove().draw();
						openPkConsData.clear().draw();

					}
					else {
						$("#snackbar").text(response.message);
						var x = document.getElementById("snackbar")
						x.className = "show";
						setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);

					}
				}
			});
		}
		else {
			alert("No pickups to close");
		}

	});
	/*pushing cons ends */

});


function getOpenCons(alpha) {
	// console.log($(alpha).closest('tr').children().length);
	var rowIndex = $(alpha).closest('tr').index();
	todelRowIndex = rowIndex;
	// console.log(rowIndex);
	var rowData = pushPickupsTableObj.row(rowIndex).data();
	var foxtrot = pushPickupsTableObj.rows().data();

	// console.log(pushPickupsTableObj.cell( rowIndex, 5 ).data());
	// console.log(rowData.pickupScheduleId);
	if (rowData.pickupScheduleId == "" && rowData.userName == "") {
		alert("Incomplete Pickup details to get cons");
	}
	else {
		$.ajax({
			url: "getOpenPickupsCons",
			type: "get",
			dataType: "json",
			data: "&pickupScheduleId=" + rowData.pickupScheduleId + "&userName=" + rowData.userName,
			beforeSend: function () {
				$("#getpidCon").text(rowData.pickupScheduleId);
				$("#pushPKconLoader").css({ "display": "block" });
			},
			success: function (response) {
				$("#getpidCon").text("");
				$("#pushPKconLoader").css({ "display": "none" });


				if (response.status == true) {
					// for(i=0;i<response.data.conDetails.length;i++)
					// {
					// 	consArr.push(response.data.conDetails[i]);
					// }
					// console.log(JSON.stringify(consArr));
					$("#openPickupsConDiv").css({ "visibility": "visible" });
					$("#pushConsBtn").prop('disabled', false);
					pushconsArr = [];
					$("#openPickupId").text("Schedule ID : " + rowData.pickupScheduleId);
					openPkConsData = $("#openPkConsTable").DataTable({
						columns: [
							{ data: 'conNumber' },
							{ data: 'orderNo' },
							{ data: 'noOfPackage' },
							{ data: 'pickupDate'}
						],
						data: response.data.conDetails,
						retrieve: true,
						destroy: true,
						fixedHeader: {
							header: true,
							footer: true
						},
						paging: true,
						searching: true,
						ordering: true,
						scrollX: true,
						"bScrollInfinite": false,
						"bScrollCollapse": false,
						"sScrollY": "150px",
						"lengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]]
					});

					openPkConsData.clear().draw();
					openPkConsData.rows.add(response.data.conDetails); // Add new data
					openPkConsData.columns.adjust().draw(); // Redraw the DataTable
				}
			}
		});
	}
}


function pushToErp(conEntryId){
	
}