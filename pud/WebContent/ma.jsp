<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>RTMS</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

   <!--      <script src="js/rtms/jquery.min.js"></script>
	<script type="text/javascript" src="js/rtms/bootstrap.js"></script>
 -->

    <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> 
 <!--    <link rel="stylesheet" href="font-awesome/css/font-awesome.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap Core Css -->
    <!-- <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <!-- <link rel="stylesheet" href="css/rtms/bootstrap.css"> -->

    <!-- Waves Effect Css -->
    <link href="css/rtms/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="css/rtms/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="css/rtms/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/rtms/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/rtms/all-themes.css" rel="stylesheet" />
    
        <!-- Jquery Core Js -->
  <!--   <script src="jquery/jquery.min.js"></script> -->
  
  <link rel="stylesheet" href="css/dataTables.bootstrap.css">
  
   <link rel="stylesheet" href="css/rtmsAdmin.css">
   
   <link href="css/rtms/select2.css" rel="stylesheet" />
  
  <script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAV7PDXnJCm8kw1fz63sttvLj3Qall0tEw"  async defer></script>
    
    <style>
    .sidebar .user-info {
    padding: 13px 15px 12px 15px;
    white-space: nowrap;
    position: relative;
    border-bottom: 1px solid #e9e9e9;
    height: 135px; }
    
    
        .sidebar .user-info .info-container .name {
        white-space: nowrap;
        -ms-text-overflow: ellipsis;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        overflow: hidden;
        font-size: 16px;
        max-width: 200px;
        color: #fff; 
        text-align: center;
        }
      .sidebar .user-info .info-container .email {
        white-space: nowrap;
        -ms-text-overflow: ellipsis;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        overflow: hidden;
        font-size: 18px;
        max-width: 200px;
        color: #fff; 
          text-align: center;
          }
          
          
          
      .sidebar .user-info .info-container {
      cursor: default;
      display: block;
      position: relative;
      top: 10px; }
      
      
      
         .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }
      
    </style>
    <script>
/*    */
</script>
    
    <script type="text/javascript">
    
    $(document).ready(function(){
    	
    });
    
    function move() {
    	  var elem = document.getElementById("myBar");   
    	  var width = 20;
    	  var id = setInterval(frame, 10);
    	  function frame() {
    	    if (width >= 100) {
    	      clearInterval(id);
    	    } else {
    	      width++; 
    	      elem.style.width = width + '%'; 
    	      document.getElementById("demo").innerHTML = width * 1  + '%';
    	    }
    	  }
    	}
    </script>
    
    
    
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar"style="background-color: #01723e;" >
        <div class="container-fluid">
            <div class="navbar-header" >
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">RTMS</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                     <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>-->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Make new buttons
                                                <small>45%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Create new dashboard
                                                <small>54%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Solve transition issue
                                                <small>65%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Answer GitHub questions
                                                <small>92%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background-color: #FD5C3A;">
             <!--  <div class="image">
                </div> -->
                <div class="info-container"  align="center" >
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome</div>
                    <div class="email" id="loggedInUserId"></div>
                    <br>
                    <div  class="row">
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                           <button type="button" class="btn bg-yellow btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="bottom" title="Password Reset">
                                    <i class="material-icons">vpn_key</i>
                           </button>
                       </div>
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                           <button type="button" class="btn bg-blue btn-circle waves-effect waves-circle waves-float" data-toggle="tooltip" data-placement="bottom" title="Profile">
                                    <i class="material-icons">person</i>
                           </button>
                       </div>
                      <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                          <button type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float"  data-toggle="tooltip" data-placement="bottom" title="Sign Out">
                                    <i class="material-icons">input</i>
                          </button>
                       </div>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                   <!-- <li class="header">MAIN NAVIGATION</li> -->
                    <li class="active"  id="home_li">
                        <a >
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">local_shipping</i>
                            <span>Vehicle</span>
                        </a>
                        <ul class="ml-menu">
                            <li id="sVehicles_li">
                                <a href="#">SC Vehicles</a>
                            </li>
                            <li>
                                <a href="#">Vehicles Path</a>
                            </li>
                        </ul>
                    </li>
                    
                    
                      <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">stay_primary_portrait</i>
                            <span>MDM</span>
                        </a>
                        <ul class="ml-menu">
                            <li id="geofenceCommands_li">
                                <a >
                                    <span>Commands</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="menu-toggle">
                                    <span>Geofence</span>
                                </a>
                                <ul class="ml-menu">
                                    <li  onclick="geofence()">
                                        <a >
                                            <span>Create</span>
                                        </a>
                                    </li>
                                     <li id="assignGeofence_li">
                                        <a>
                                            <span>Assign</span>
                                        </a>
                                    </li>
                                    <!--  <li>
                                        <a>
                                            <span>Geofence Device Map</span>
                                        </a>
                                    </li> -->
                                     <li id="geofenceUpdates_li">
                                        <a >
                                            <span>Reports</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                    <ul class="demo-choose-skin">
                        <li data-theme="red" class="active">
                            <div class="red"></div>
                            <span>Red</span>
                        </li>
                        <li data-theme="pink">
                            <div class="pink"></div>
                            <span>Pink</span>
                        </li>
                        <li data-theme="purple">
                            <div class="purple"></div>
                            <span>Purple</span>
                        </li>
                        <li data-theme="deep-purple">
                            <div class="deep-purple"></div>
                            <span>Deep Purple</span>
                        </li>
                        <li data-theme="indigo">
                            <div class="indigo"></div>
                            <span>Indigo</span>
                        </li>
                        <li data-theme="blue">
                            <div class="blue"></div>
                            <span>Blue</span>
                        </li>
                        <li data-theme="light-blue">
                            <div class="light-blue"></div>
                            <span>Light Blue</span>
                        </li>
                        <li data-theme="cyan">
                            <div class="cyan"></div>
                            <span>Cyan</span>
                        </li>
                        <li data-theme="teal">
                            <div class="teal"></div>
                            <span>Teal</span>
                        </li>
                        <li data-theme="green">
                            <div class="green"></div>
                            <span>Green</span>
                        </li>
                        <li data-theme="light-green">
                            <div class="light-green"></div>
                            <span>Light Green</span>
                        </li>
                        <li data-theme="lime">
                            <div class="lime"></div>
                            <span>Lime</span>
                        </li>
                        <li data-theme="yellow">
                            <div class="yellow"></div>
                            <span>Yellow</span>
                        </li>
                        <li data-theme="amber">
                            <div class="amber"></div>
                            <span>Amber</span>
                        </li>
                        <li data-theme="orange">
                            <div class="orange"></div>
                            <span>Orange</span>
                        </li>
                        <li data-theme="deep-orange">
                            <div class="deep-orange"></div>
                            <span>Deep Orange</span>
                        </li>
                        <li data-theme="brown">
                            <div class="brown"></div>
                            <span>Brown</span>
                        </li>
                        <li data-theme="grey">
                            <div class="grey"></div>
                            <span>Grey</span>
                        </li>
                        <li data-theme="blue-grey">
                            <div class="blue-grey"></div>
                            <span>Blue Grey</span>
                        </li>
                        <li data-theme="black">
                            <div class="black"></div>
                            <span>Black</span>
                        </li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="settings">
                    <div class="demo-settings">
                        <p>GENERAL SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Report Panel Usage</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Email Redirect</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>SYSTEM SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Notifications</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Auto Updates</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                        <p>ACCOUNT SETTINGS</p>
                        <ul class="setting-list">
                            <li>
                                <span>Offline</span>
                                <div class="switch">
                                    <label><input type="checkbox"><span class="lever"></span></label>
                                </div>
                            </li>
                            <li>
                                <span>Location Permission</span>
                                <div class="switch">
                                    <label><input type="checkbox" checked><span class="lever"></span></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </aside>
        <!-- #END# Right Sidebar -->
</section>
 

<section class="content" id="scVehicles_section">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="body">
                        <div class="row">
                        <div>
                        <!--  pb  -->
                        <div class="w3-light-grey">
							<div id="myBar" class="w3-container w3-green" style="height:24px;width:20%"></div>
						</div>
							<p id="demo">20%</p>
							<button class="w3-button w3-green" onclick="move()">Click Me</button> 
                        </div>
                        
                        
                        </div>
                            <div id="ma_DivId"   style="width:100%; height:440px;"></div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
</section>
	
	
	

	<script type="text/javascript" src="js/jquery.js"></script>
	 <script src="js/jquery.blockui.js"></script>
	<!-- <script type="text/javascript" src="js/jquery-ui.js"></script> -->
	<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <!-- <script src="js/rtms/bootstrap-select.js"></script> -->

    <!-- Slimscroll Plugin Js -->
    <script src="js/rtms/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="js/rtms/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="js/rtms/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="js/rtms/raphael.min.js"></script>
    <script src="js/rtms/morris.js"></script>


	<script type="text/javascript" src="js/highcharts.js"></script>
	 <script type="text/javascript" src="js/highcharts-more.js"></script>
	 <script type="text/javascript" src="js/rtmscharts.js"></script>



    <script src="js/rtms/admin.js"></script>
     <!--  <script src="js/rtms/index.js"></script> -->
      <script src="js/rtms/demo.js"></script>
       <script src="js/rtms/tooltips-popovers.js"></script>
        <script src="js/radmin.js"></script>
           <script src="js/commons.js"></script>
      
      <script type="text/javascript"
		src="js/datatable1.10.16/jquery.dataTables.js"></script>
	<script type="text/javascript" src="js/dataTables.bootstrap.js"></script>
       <script src="js/rtms/select2.js"></script>

    <!-- ChartJs -->
    <!--  <script src="chartjs/Chart.bundle.js"></script>

   <script src="flot-charts/jquery.flot.js"></script>
    <script src="flot-charts/jquery.flot.resize.js"></script>
    <script src="flot-charts/jquery.flot.pie.js"></script>
    <script src="flot-charts/jquery.flot.categories.js"></script>
    <script src="flot-charts/jquery.flot.time.js"></script>

    Sparkline Chart Plugin Js
    <script src="jquery-sparkline/jquery.sparkline.js"></script> -->

    <!-- Custom Js -->
   <!--  <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script> -->

    <!-- Demo Js -->
  <!--   <script src="js/demo.js"></script>
    <script src="js/pages/tooltips-popovers.js"></script> -->
</body>

</html>