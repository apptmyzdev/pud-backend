# for audit logs
CREATE TABLE `audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_imei` varchar(128) DEFAULT NULL,
  `service_type` varchar(128) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `sheet_no` varchar(128) DEFAULT NULL,
  `remarks` varchar(128) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `ipadd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# For saving and updating pin code master
CREATE TABLE `pin_code_master` (
  `pin_code` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `servicable` varchar(10) DEFAULT NULL,
  `branch_code` varchar(45) DEFAULT NULL,
  `location_name` varchar(45) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `active_status` varchar(45) DEFAULT NULL,
  `service_type` varchar(45) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pin_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# user_device map table.
CREATE TABLE `user_device_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) NOT NULL,
  `imei` varchar(256) NOT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  `active_flag` int(11) DEFAULT NULL,
  `apk_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

# table used to check updates.
CREATE TABLE `device_apk_versions` (
  `version` int(11) NOT NULL,
  `url` varchar(1024) DEFAULT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# For device registration
CREATE TABLE `device_info` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `active_flag` int(1) DEFAULT NULL,
  `app_version` varchar(45) DEFAULT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `device_available_memory` varchar(256) DEFAULT NULL,
  `device_battery` varchar(256) DEFAULT NULL,
  `device_brand` varchar(256) DEFAULT NULL,
  `device_imei` varchar(256) DEFAULT NULL,
  `device_mac` varchar(256) DEFAULT NULL,
  `device_mobile_number` varchar(45) DEFAULT NULL,
  `device_model` varchar(45) DEFAULT NULL,
  `device_name` varchar(45) DEFAULT NULL,
  `device_os_version` varchar(45) DEFAULT NULL,
  `device_service_provider` varchar(45) DEFAULT NULL,
  `device_total_memory` varchar(256) DEFAULT NULL,
  `gps_status` int(2) DEFAULT NULL,
  `pushToken` varchar(256) DEFAULT NULL,
  `sim_card` varchar(45) DEFAULT NULL,
  `uniqueKey` varchar(256) DEFAULT NULL,
  `updated_ip_address` varchar(256) DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# For pickup registration
CREATE TABLE `pickup_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile_user_id` varchar(45) NOT NULL,
  `Customer_code` varchar(256) DEFAULT NULL,
  `pickup_date` datetime DEFAULT NULL,
  `pickup_pincode` int(255) DEFAULT NULL,
  `pieces` int(11) DEFAULT NULL,
  `gps_lat` double DEFAULT NULL,
  `gps_lon` double DEFAULT NULL,
  `cft` float DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `customer_name` varchar(256) DEFAULT NULL,
  `contact_person_name` varchar(256) DEFAULT NULL,
  `contact_person_number` varchar(256) DEFAULT NULL,
  `pickup_address` varchar(256) DEFAULT NULL,
  `pickup_order_no` varchar(256) DEFAULT NULL,
  `pickup_schedule_id` int(255) DEFAULT NULL,
  `transaction_result` varchar(256) DEFAULT NULL,
  `transaction_remarks` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# To save pud data
CREATE TABLE `pud_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickup_schedule_id` int(11) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `pickup_order_no` varchar(45) DEFAULT NULL,
  `pickup_register_id` int(11) DEFAULT NULL,
  `customer_code` varchar(45) DEFAULT NULL,
  `customer_name` varchar(256) DEFAULT NULL,
  `pieces` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `pickup_date` datetime DEFAULT NULL,
  `contact_person` varchar(45) DEFAULT NULL,
  `contact_mobile` varchar(45) DEFAULT NULL,
  `pickup_address` varchar(256) DEFAULT NULL,
  `pickup_city` varchar(45) DEFAULT NULL,
  `pickup_pincode` int(11) DEFAULT NULL,
  `delivery_address` varchar(256) DEFAULT NULL,
  `delivery_city` varchar(45) DEFAULT NULL,
  `delivery_pincode` int(11) DEFAULT NULL,
  `customer_ref_number` varchar(45) DEFAULT NULL,
  `cft` float DEFAULT NULL,
  `min_cfg_weight` float DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_device_id_idx` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#to save UserMaster data
CREATE TABLE `user_data_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `app_user_name` varchar(45) DEFAULT NULL,
  `app_pwd` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `is_pickup_app_active` int(11) DEFAULT NULL,
  `is_con_entry` int(11) DEFAULT NULL,
  `is_pieces_entry` int(11) DEFAULT NULL,
  `is_offline_printing` int(11) DEFAULT NULL,
  `printer_sl_no` varchar(45) DEFAULT NULL,
  `currently_used` varchar(45) DEFAULT NULL,
  `is_9digit_part_no` int(11) DEFAULT NULL,
  `is_delivery` int(11) DEFAULT NULL,
  `verison_id` varchar(45) DEFAULT NULL,
  `login_status` int(11) DEFAULT NULL,
  `is_pickup_registration` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `tab_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=573 DEFAULT CHARSET=utf8;

#to save docket series master
CREATE TABLE `docket_series_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dkt_user_key` varchar(70) DEFAULT NULL,
  `docket_key` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(55) DEFAULT NULL,
  `vendor_code` varchar(55) DEFAULT NULL,
  `branch_code` varchar(45) DEFAULT NULL,
  `from_series` varchar(45) DEFAULT NULL,
  `to_series` varchar(45) DEFAULT NULL,
  `last_sr_number` varchar(55) DEFAULT NULL,
  `total_leaf` varchar(55) DEFAULT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2469482 DEFAULT CHARSET=utf8;

#to save data of logged_in users
CREATE TABLE `user_session_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(256) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `imei` varchar(45) DEFAULT NULL,
  `apk_version` varchar(45) DEFAULT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `updated_timestamp` datetime DEFAULT NULL,
  `active_flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id_UNIQUE` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;



# to save pickup acknowledgement result
CREATE TABLE `pickup_data_acknowledgement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickup_schedule_id` int(11) DEFAULT NULL,
  `reva_pickup_id` int(11) DEFAULT NULL,
  `transaction_result` varchar(45) DEFAULT NULL,
  `transaction_remarks` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_reva_pickup_id_idx` (`reva_pickup_id`),
  CONSTRAINT `fk_reva_pickup_id` FOREIGN KEY (`reva_pickup_id`) REFERENCES `pud_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

# to save piece info
CREATE TABLE `piece_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pkey` int(11) DEFAULT NULL,
  `start_no` varchar(45) DEFAULT NULL,
  `end_no` varchar(45) DEFAULT NULL,
  `total_isssued` varchar(45) DEFAULT NULL,
  `total_used` varchar(45) DEFAULT NULL,
  `complete` varchar(45) DEFAULT NULL,
  `last_piece_no` varchar(45) DEFAULT NULL,
  `series_type` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

ALTER TABLE `pud`.`piece_master` 
CHANGE COLUMN `pkey` `pkey` INT(11) NOT NULL ,
ADD UNIQUE INDEX `pkey_UNIQUE` (`pkey` ASC);


	CREATE TABLE `con_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `con_entry_id` int(11) DEFAULT NULL,
  `con_number` varchar(45) DEFAULT NULL,
  `ref_number` varchar(45) DEFAULT NULL,
  `pickup_date` datetime DEFAULT NULL,
  `product` varchar(45) DEFAULT NULL,
  `origin_pincode` varchar(45) DEFAULT NULL,
  `destination_pincode` varchar(45) DEFAULT NULL,
  `payment_basis` varchar(45) DEFAULT NULL,
  `customer_code` varchar(45) DEFAULT NULL,
  `no_of_package` varchar(45) DEFAULT NULL,
  `actual_weight` varchar(45) DEFAULT NULL,
  `apply_vtv` varchar(45) DEFAULT NULL,
  `vtc_amount` varchar(45) DEFAULT NULL,
  `apply_dc` varchar(45) DEFAULT NULL,
  `apply_nf_form` varchar(45) DEFAULT NULL,
  `risk_type` varchar(45) DEFAULT NULL,
  `declared_value` varchar(45) DEFAULT NULL,
  `special_instruction` varchar(45) DEFAULT NULL,
  `lat_value` varchar(45) DEFAULT NULL,
  `long_value` varchar(45) DEFAULT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `order_no` varchar(45) DEFAULT NULL,
  `crm_schedule_id` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `receiver_name` varchar(45) DEFAULT NULL,
  `receiver_phone_no` varchar(45) DEFAULT NULL,
  `package_type` varchar(45) DEFAULT NULL,
  `consignment_type` varchar(45) DEFAULT NULL,
  `total_vol_weight` varchar(45) DEFAULT NULL,
  `vol_type` varchar(45) DEFAULT NULL,
  `tin_no` varchar(45) DEFAULT NULL,
  `pan_no` varchar(45) DEFAULT NULL,
  `image1_url` varchar(45) DEFAULT NULL,
  `image2_url` varchar(45) DEFAULT NULL,
  `shipment_image_url` varchar(45) DEFAULT NULL,
  `gate_pass_time` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `pud`.`con_details` 
CHANGE COLUMN `image1_url` `image1_url` VARCHAR(256) NULL DEFAULT NULL ,
CHANGE COLUMN `image2_url` `image2_url` VARCHAR(256) NULL DEFAULT NULL ,
CHANGE COLUMN `shipment_image_url` `shipment_image_url` VARCHAR(256) NULL DEFAULT NULL ;

CREATE TABLE `pieces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pcr_key` int(11) DEFAULT NULL,
  `total_used` varchar(45) DEFAULT NULL,
  `last_piece_no` varchar(45) DEFAULT NULL,
  `con_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_con_id_idx` (`con_id`),
  CONSTRAINT `fk_con_id` FOREIGN KEY (`con_id`) REFERENCES `con_details` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
CREATE TABLE `pud`.`piece_entry` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pcr_key` INT(11) NULL DEFAULT NULL,
  `from_piece_no` VARCHAR(45) NULL DEFAULT NULL,
  `to_piece_no` VARCHAR(45) NULL DEFAULT NULL,
  `total_pieces` VARCHAR(45) NULL DEFAULT NULL,
  `con_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pcon_id_idx` (`con_id` ASC),
  CONSTRAINT `fk_pcon_id`
    FOREIGN KEY (`con_id`)
    REFERENCES `pud`.`con_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `pud`.`piece_volume` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `no_of_pieces` INT(11) NULL DEFAULT NULL,
  `vol_length` DOUBLE NULL DEFAULT NULL,
  `vol_breadth` DOUBLE NULL DEFAULT NULL,
  `vol_height` DOUBLE NULL DEFAULT NULL,
  `total_vol_weight` DOUBLE NULL DEFAULT NULL,
  `con_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_pvcon_id_idx` (`con_id` ASC),
  CONSTRAINT `fk_pvcon_id`
    FOREIGN KEY (`con_id`)
    REFERENCES `pud`.`con_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
  CREATE TABLE `pud`.`pickup_updation_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_result` VARCHAR(256) NULL DEFAULT NULL,
  `con_entry_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));
    
    
    CREATE TABLE `pud`.`pickup_cancellation_master` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `status_id` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `pud`.`pickup_cancellation_reason_master` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `reason_code` VARCHAR(45) NULL DEFAULT NULL,
  `reason_desc` VARCHAR(45) NULL DEFAULT NULL,
  `updated_by` VARCHAR(45) NULL DEFAULT NULL,
  `updated_on` DATETIME NULL DEFAULT NULL,
  `failure_category` VARCHAR(45) NULL DEFAULT NULL,
  `active_status` INT(11) NULL DEFAULT NULL,
  `pickup_status_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `payment_modes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mode` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `pud`.`payment_modes` (`id`, `payment_mode`) VALUES ('1', 'Cash');
INSERT INTO `pud`.`payment_modes` (`id`, `payment_mode`) VALUES ('2', 'Credit Sender');
INSERT INTO `pud`.`payment_modes` (`id`, `payment_mode`) VALUES ('3', 'Credit Receiver');
INSERT INTO `pud`.`payment_modes` (`id`, `payment_mode`) VALUES ('4', 'FTC');


ALTER TABLE `pud`.`pickup_cancellation_reason_master` 
CHANGE COLUMN `reason_desc` `reason_desc` VARCHAR(256) NULL DEFAULT NULL ;

CREATE TABLE `pickup_reschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pickup_schedule_id` int(11) DEFAULT NULL,
  `ref_no` varchar(45) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `app_mobile_user_name` varchar(45) DEFAULT NULL,
  `reschedule_datetime` datetime DEFAULT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL,
  `con_number` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `gps_lat` double DEFAULT NULL,
  `gps_lon` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pud`.`pickup_reschedule_update_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pickup_schedule_id` INT(11) NULL DEFAULT NULL,
  `transaction_result` VARCHAR(45) NULL DEFAULT NULL,
 PRIMARY KEY (`id`));
  
  
  CREATE TABLE `pud`.`pieces_images` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `pieces_images_url` VARCHAR(256) NULL DEFAULT NULL,
  `con_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_images_con_id_idx` (`con_id` ASC),
  CONSTRAINT `fk_images_con_id`
    FOREIGN KEY (`con_id`)
    REFERENCES `pud`.`con_details` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
    CREATE TABLE `delivery_updation` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `awb_no` varchar(45) DEFAULT NULL,
  `transaction_date` varchar(45) DEFAULT NULL,
  `delivered_to` varchar(45) DEFAULT NULL,
  `relationship` varchar(45) DEFAULT NULL,
  `remarks` varchar(45) DEFAULT NULL,
  `receiver_mobile_no` varchar(45) DEFAULT NULL,
  `signature_url` varchar(256) DEFAULT NULL,
  `reason` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `long_value` float DEFAULT NULL,
  `lat_vlue` float DEFAULT NULL,
  `field_employee_name` varchar(45) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  CREATE TABLE `pud`.`delivery_updation_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_result` VARCHAR(45) NULL DEFAULT NULL,
  `transaction_remarks` VARCHAR(256) NULL DEFAULT NULL,
  `transaction_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `pud`.`con_details` 
ADD COLUMN `is_scanned` INT(1) NULL DEFAULT NULL AFTER `gate_pass_time`;
  PRIMARY KEY (`id`));
  
 #For Deliveries
 CREATE TABLE `agent_pdc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pdc` varchar(128) DEFAULT NULL,
  `agent_id` varchar(45) DEFAULT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  `pushed_time` datetime DEFAULT NULL,
  `pulled_time` datetime DEFAULT NULL,
  `sccode` varchar(45) DEFAULT NULL,
  `pdc_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_agent_pdc_agent` (`agent_id`),
  KEY `idx_agent_pdc_date` (`created_timestamp`),
  KEY `idx_agent_pdc_pulled_time` (`pulled_time`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `delivery_con_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `con` int(11) DEFAULT NULL,
  `pdc` varchar(128) DEFAULT NULL,
  `ref1` varchar(45) DEFAULT NULL,
  `ref2` varchar(45) DEFAULT NULL,
  `pcs_count` int(11) DEFAULT NULL,
  `con_wt` decimal(10,3) DEFAULT NULL,
  `consignee_name` varchar(128) DEFAULT NULL,
  `consignee_company` varchar(128) DEFAULT NULL,
  `consignee_address` varchar(1024) DEFAULT NULL,
  `consignee_city` varchar(128) DEFAULT NULL,
  `consignee_pincode` varchar(45) DEFAULT NULL,
  `consignee_mobile` varchar(45) DEFAULT NULL,
  `consignee_emailid` varchar(128) DEFAULT NULL,
  `consignor_city` varchar(128) DEFAULT NULL,
  `consignor_mobile` varchar(45) DEFAULT NULL,
  `consignor_email` varchar(128) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_con_pdc` (`pdc`),
  KEY `idx_con_created_date` (`created_date`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;


CREATE TABLE `del_undel_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `del_undel_code` int(11) DEFAULT NULL COMMENT '1 - Delivery, 2 - Undelivered',
  `code` int(11) DEFAULT NULL,
  `short_code` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `relationships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

ALTER TABLE `pud`.`pickup_updation_status` 
ADD INDEX `fk_con_entry_id_idx` (`con_entry_id` ASC);
ALTER TABLE `pud`.`pickup_updation_status` 
ADD CONSTRAINT `fk_con_entry_id`
  FOREIGN KEY (`con_entry_id`)
  REFERENCES `pud`.`con_details` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `pud`.`pickup_reschedule_update_status` 
ADD COLUMN `timestamp` DATETIME NULL DEFAULT NULL AFTER `transaction_result`;

ALTER TABLE `pud`.`delivery_updation_status` 
ADD INDEX `fk_transacion_id_idx` (`transaction_id` ASC);
ALTER TABLE `pud`.`delivery_updation_status` 
ADD CONSTRAINT `fk_transacion_id`
  FOREIGN KEY (`transaction_id`)
  REFERENCES `pud`.`delivery_updation` (`transaction_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  
ALTER TABLE `pud`.`delivery_updation` 
ADD COLUMN `pdc_number` VARCHAR(128) NULL DEFAULT NULL AFTER `image_url`;

 ALTER TABLE `pud`.`pickup_reschedule` 
CHANGE COLUMN `reason_id` `reason_id` VARCHAR(256) NULL DEFAULT NULL ;

ALTER TABLE `pud`.`con_details` 
ADD COLUMN `pickup_schedule_id` INT(255) NULL DEFAULT NULL AFTER `is_scanned`;

ALTER TABLE `pud`.`con_details` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `pickup_schedule_id`;

ALTER TABLE `pud`.`con_details` 
ADD COLUMN `customer_name` VARCHAR(256) NULL DEFAULT NULL AFTER `created_timestamp`;


ALTER TABLE `pud`.`delivery_updation` 
ADD COLUMN `created_time` DATETIME NULL DEFAULT NULL AFTER `pdc_number`;

ALTER TABLE `pud`.`delivery_updation_status` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `transaction_id`;


ALTER TABLE `pud`.`pickup_data_acknowledgement` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `transaction_remarks`;

ALTER TABLE `pud`.`pickup_registration` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `transaction_remarks`;

ALTER TABLE `pud`.`pickup_reschedule` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `gps_lon`;

ALTER TABLE `pud`.`pickup_updation_status` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `con_entry_id`;

ALTER TABLE `pud`.`piece_entry` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `con_id`;

ALTER TABLE `pud`.`piece_master` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `user_id`;

ALTER TABLE `pud`.`piece_volume` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `con_id`;

ALTER TABLE `pud`.`pieces` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `con_id`;

ALTER TABLE `pud`.`pieces_images` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `con_id`;

ALTER TABLE `pud`.`pud_data` 
ADD COLUMN `created_timestamp` DATETIME NULL DEFAULT NULL AFTER `min_cfg_weight`;

ALTER TABLE `pud`.`pickup_reschedule` 
ADD COLUMN `customer_code` VARCHAR(45) NULL DEFAULT NULL AFTER `created_timestamp`,
ADD COLUMN `customer_name` VARCHAR(256) NULL DEFAULT NULL AFTER `customer_code`;

ALTER TABLE `pud`.`delivery_updation` 
ADD COLUMN `consignee_name` VARCHAR(128) NULL DEFAULT NULL AFTER `created_time`;

ALTER TABLE `pud`.`pud_data` 
ADD COLUMN `pulled_time` DATETIME NULL DEFAULT NULL AFTER `created_timestamp`;

ALTER TABLE `pud`.`pickup_registration` 
ADD COLUMN `pulled_time` DATETIME NULL DEFAULT NULL AFTER `created_timestamp`;

CREATE TABLE `pulled_data_acknowledgement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `download_type` int(1) DEFAULT NULL,
  `download_date` datetime DEFAULT NULL,
  `result_flag` int(1) DEFAULT NULL,
  `result_remarks` varchar(45) DEFAULT NULL,
  `result_transaction_id` int(11) DEFAULT NULL,
  `created_timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `pud`.`user_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user` VARCHAR(45) NULL DEFAULT NULL,
  `version` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`));

  ALTER TABLE `pud`.`user_version` 
ADD COLUMN `url` VARCHAR(1024) NULL DEFAULT NULL AFTER `version`;


ALTER TABLE `pud`.`current_updation_status` 
CHANGE COLUMN `pickup_schedule_id/con_number` `pickup_schedule_idorcon_number` VARCHAR(45) NULL DEFAULT NULL ;

