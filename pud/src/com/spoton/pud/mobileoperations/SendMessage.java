package com.spoton.pud.mobileoperations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;

import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;

public class SendMessage {
	
	public static MulticastResult sendMessage(String pushToken, String message){
		
		List<String> pushTokens = new ArrayList<String>();
			pushTokens.add(pushToken);
			
		return sendMessage(pushTokens,message);
	}
	
		
	public static MulticastResult sendMessage(List<String> pushTokens, String message){
		MulticastResult result = null;
		
		//removing the duplicates using the set
		Set<String> setItems = new LinkedHashSet<String>(pushTokens);
			pushTokens.clear();
			pushTokens.addAll(setItems);
		
			
		Sender sender = new Sender(FilesUtil.getProperty("gcm.app_key"));
		Message msg = new Message.Builder().addData("message", message).build();
		try {
			//send the message only when the list has the pushtokens in it
			if(pushTokens.size()>0)
				result = sender.send(msg, pushTokens, 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static boolean ANDROID_sendMessage(String pushToken, String message){		
		List<String> pushTokens = new ArrayList<String>();
			pushTokens.add(pushToken);		
		return SendMessage.ANDROID_sendMessage(pushTokens, message);		
	}
	
	
	public static boolean ANDROID_sendMessage(List<String> pushTokens, String message){  
		  boolean result = false;  
		  Sender sender = new Sender(FilesUtil.getProperty("gcm.app_key"));
		  Message msg = new Message.Builder().addData("message", message).build();
		  try {
		   sender.send(msg, pushTokens, 1);
		   result = true;
		  } catch (IOException e) { e.printStackTrace(); result = false; }
		  return result;
		 }
	
	public static Map<String,String> getPushTokens()
	 {
	  ManageTransaction manageTransaction = null;
	  Map<String , String> userTokenMap = new HashMap<String , String>();
	  try{
	   manageTransaction = new ManageTransaction();
	   String query = "SELECT p.user_id,p.pushToken FROM device_info p where p.active_flag = 1";
	   Query nativeQuery = manageTransaction.createNativeQuery(query);
	   List <Object []> userList = nativeQuery.getResultList();
	   if(userList != null && userList.size() > 0)
	   {
	    for(Object [] obj : userList)
	    {
	     userTokenMap.put((String) obj[0], (String) obj[1]);
	    }
	   }
	  }
	  catch(Exception e)
	  {
	   e.printStackTrace();
	   return null;
	  }
	  finally{
	   manageTransaction.close();
	  }
	  return userTokenMap;
	 }
	
	public static void main(String[] args) {
	}
}
