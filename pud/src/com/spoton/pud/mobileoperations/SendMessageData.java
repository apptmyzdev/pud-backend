package com.spoton.pud.mobileoperations;

public class SendMessageData {

	public SendMessageData() {
		super();
	}
	public SendMessageData(String message, String deviceId) {
		super();
		this.message = message;
		this.deviceId = deviceId;
	}
	private String message;
	private String deviceId;
	private String severity;
	
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
