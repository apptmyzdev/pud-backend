package com.spoton.pud.adminmodels;

public class BranchPickupsSummaryModel 
{
	private String branch;
	private double totalPickups;
	private double attemptedPickups;
	private double pickupSuccessCount;
	private double pendingCount;
	private double productivity;
	
	
	public double getPendingCount() {
		return pendingCount;
	}
	public void setPendingCount(double pendingCount) {
		this.pendingCount = pendingCount;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public double getTotalPickups() {
		return totalPickups;
	}
	public void setTotalPickups(double totalPickups) {
		this.totalPickups = totalPickups;
	}
	public double getAttemptedPickups() {
		return attemptedPickups;
	}
	public void setAttemptedPickups(double attemptedPickups) {
		this.attemptedPickups = attemptedPickups;
	}
	public double getPickupSuccessCount() {
		return pickupSuccessCount;
	}
	public void setPickupSuccessCount(double pickupSuccessCount) {
		this.pickupSuccessCount = pickupSuccessCount;
	}
	public double getProductivity() {
		return productivity;
	}
	public void setProductivity(double productivity) {
		this.productivity = productivity;
	}
	
}
