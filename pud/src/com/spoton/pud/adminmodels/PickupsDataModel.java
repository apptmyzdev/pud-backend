package com.spoton.pud.adminmodels;

public class PickupsDataModel
{
	private int srNo;
	private int pickUpScheduleId;
	private String userId;
	private String customerCode;
	private String customerName;
	private String pickupAddress;
	private String status;
	private long conCount;
	private double pieceCount;
	private String notes;
	private Double totalWeight;
	
	
	public double getPieceCount() {
		return pieceCount;
	}
	public void setPieceCount(double pieceCount) {
		this.pieceCount = pieceCount;
	}
	public long getConCount() {
		return conCount;
	}
	public void setConCount(long conCount) {
		this.conCount = conCount;
	}
	
	public Double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public int getPickUpScheduleId() {
		return pickUpScheduleId;
	}
	public void setPickUpScheduleId(int pickUpScheduleId) {
		this.pickUpScheduleId = pickUpScheduleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPickupAddress() {
		return pickupAddress;
	}
	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
}

