package com.spoton.pud.adminmodels;

import java.util.List;

public class OnLoadDataModel
{
	private List<CurrentLoginsModel> logList;
	private List<HourlyPickupsBarModel> monthlySPickupsList;
	private List<HourlyPickupsBarModel> hourlyDeliveries;
	private List<UAuditsDataModel> auditsList;
	private long totRegPickups;
	private long todaysPickups;
	private long todaysDeliveries;
	private long pendingPickups;
	private long attemptedPickups;
	private long successPickups;
	private long delsAttempted;
	private long delsSuccess;
	private long delsPending;
	
	
	
	public List<HourlyPickupsBarModel> getHourlyDeliveries() {
		return hourlyDeliveries;
	}
	public void setHourlyDeliveries(List<HourlyPickupsBarModel> hourlyDeliveriesGraphData) {
		this.hourlyDeliveries = hourlyDeliveriesGraphData;
	}
	public List<UAuditsDataModel> getAuditsList() {
		return auditsList;
	}
	public void setAuditsList(List<UAuditsDataModel> auditsList) {
		this.auditsList = auditsList;
	}
	public List<HourlyPickupsBarModel> getMonthlySPickupsList() {
		return monthlySPickupsList;
	}
	public void setMonthlySPickupsList(List<HourlyPickupsBarModel> hourlyPickupsGraphData) {
		this.monthlySPickupsList = hourlyPickupsGraphData;
	}
	public long getDelsAttempted() {
		return delsAttempted;
	}
	public void setDelsAttempted(long delsAttempted) {
		this.delsAttempted = delsAttempted;
	}
	public long getDelsSuccess() {
		return delsSuccess;
	}
	public void setDelsSuccess(long delsSuccess) {
		this.delsSuccess = delsSuccess;
	}
	public long getDelsPending() {
		return delsPending;
	}
	public void setDelsPending(long delsPending) {
		this.delsPending = delsPending;
	}
	public long getPendingPickups() {
		return pendingPickups;
	}
	public void setPendingPickups(long pendingPickups) {
		this.pendingPickups = pendingPickups;
	}
	public long getAttemptedPickups() {
		return attemptedPickups;
	}
	public void setAttemptedPickups(long attemptedPickups) {
		this.attemptedPickups = attemptedPickups;
	}
	public long getSuccessPickups() {
		return successPickups;
	}
	public void setSuccessPickups(long successPickups) {
		this.successPickups = successPickups;
	}
	public List<CurrentLoginsModel> getLogList() {
		return logList;
	}
	public void setLogList(List<CurrentLoginsModel> logList) {
		this.logList = logList;
	}
	public long getTotRegPickups() {
		return totRegPickups;
	}
	public void setTotRegPickups(long totRegPickups) {
		this.totRegPickups = totRegPickups;
	}
	public long getTodaysPickups() {
		return todaysPickups;
	}
	public void setTodaysPickups(long todaysPickups) {
		this.todaysPickups = todaysPickups;
	}
	public long getTodaysDeliveries() {
		return todaysDeliveries;
	}
	public void setTodaysDeliveries(long todaysDeliveries) {
		this.todaysDeliveries = todaysDeliveries;
	}
}
