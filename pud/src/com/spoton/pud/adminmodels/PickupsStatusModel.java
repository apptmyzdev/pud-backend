package com.spoton.pud.adminmodels;

public class PickupsStatusModel 
{
	private int conEntryId;
	private String conNum;
	private int pickupScheduleId;
	private String status;
	public int getConEntryId() {
		return conEntryId;
	}
	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}
	public String getConNum() {
		return conNum;
	}
	public void setConNum(String conNum) {
		this.conNum = conNum;
	}
	public int getPickupScheduleId() {
		return pickupScheduleId;
	}
	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
