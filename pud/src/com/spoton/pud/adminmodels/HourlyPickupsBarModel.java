package com.spoton.pud.adminmodels;

public class HourlyPickupsBarModel 
{
	private long count;
	private long hour;
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public long getHour() {
		return hour;
	}
	public void setHour(long hour) {
		this.hour = hour;
	}
	
}
