package com.spoton.pud.adminmodels;

public class ConsInfoData {
	private long conCount;
	private Double totalWeight;
	public long getConCount() {
		return conCount;
	}
	public void setConCount(long conCount) {
		this.conCount = conCount;
	}
	public Double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}
	
	
}
