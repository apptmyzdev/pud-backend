package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModalV3;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupRescheduleModalList;
import com.spoton.pud.data.PickupRescheduleResponse;
import com.spoton.pud.data.PickupRescheduleResponseList;
import com.spoton.pud.data.ReassignPickupErpInputModal;
import com.spoton.pud.data.ReassignPickupErpInputModalList;
import com.spoton.pud.data.ReassignPickupInputModal;
import com.spoton.pud.jpa.PickupReassigned;
import com.spoton.pud.jpa.PickupRegistration;
import com.spoton.pud.jpa.PickupRescheduleUpdateStatus;
import com.spoton.pud.jpa.PudData;

/**
 * Servlet implementation class SubmitReassignedPickup
 */
@WebServlet("/submitReassignedPickup")
public class SubmitReassignedPickup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SubmitReassignedPickup");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmitReassignedPickup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START**************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("pickUpReassignUrl");
		String datasent="";ManageTransaction mt=null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String userId=""; String resultMessage="";
		String ipadd = request.getRemoteAddr();
		logger.info("deviceImei:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		try{
			datasent=request.getReader().readLine();
			logger.info("DataSent from Mobile"+datasent);
			Type type = new TypeToken<ArrayList<ReassignPickupInputModal>>(){}.getType();
			List<ReassignPickupInputModal> dataList=gson.fromJson(datasent,type);
			if(dataList!=null&&dataList.size()>0) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
				mt=new ManageTransaction();
				Map<Integer,String> assignedPickupsMap=new HashMap<Integer,String>();
				Map<Integer,String> registeredPickupsMap=new HashMap<Integer,String>();
				Map<Integer,Integer> savedEntitiesMap=new HashMap<Integer,Integer>();
				List<ReassignPickupErpInputModal> erpDataList=new ArrayList<ReassignPickupErpInputModal>();
				ReassignPickupErpInputModalList erpData=new ReassignPickupErpInputModalList();
				for(ReassignPickupInputModal im:dataList) {
					PickupReassigned pr=new PickupReassigned();
					pr.setCreatedTimestamp(new Date());
					pr.setGpsValueLat(im.getGpsValueLat());
					pr.setGpsValueLon(im.getGpsValueLon());
					pr.setPickupScheduleId(im.getPickupScheduleId());
					pr.setPickupType(im.getPickupType());
					pr.setReassignDatetime(im.getReassignDateTime()!=null?format.parse(im.getReassignDateTime()):null);
					pr.setRefNumber(im.getRefNo());
					pr.setRemarks(im.getRemarks());
					pr.setToUserId(im.getToUserId());
					pr.setUserId(im.getUserId());
					mt.persist(pr);
					mt.commit();
					savedEntitiesMap.put(im.getPickupScheduleId(), pr.getId());
					ReassignPickupErpInputModal rp=new ReassignPickupErpInputModal();
					rp.setGPSValue_LAT(im.getGpsValueLat());
					rp.setGPSValue_LON(im.getGpsValueLon());
					rp.setPickUpScheduleID(im.getPickupScheduleId());
					rp.setRefNo(im.getRefNo());
					rp.setRemarks(im.getRemarks());
					rp.setRescheduleDateTime(im.getReassignDateTime());
					rp.setToUserID(im.getToUserId());
					rp.setUserID(im.getUserId());
					userId=im.getUserId();
					if(im.getPickupType()!=null&&im.getPickupType().equalsIgnoreCase("A")) {
						assignedPickupsMap.put(im.getPickupScheduleId(),im.getToUserId()+"/"+im.getReassignDateTime());
					}else if(im.getPickupType()!=null&&im.getPickupType().equalsIgnoreCase("R")) {
						registeredPickupsMap.put(im.getPickupScheduleId(),im.getToUserId()+"/"+im.getReassignDateTime());
					}
					erpDataList.add(rp);
				}
				erpData.setPickUpReassign(erpDataList);
				 logger.info("DataSent to spoton "+gson.toJson(erpData));
				
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				con.setConnectTimeout(5000);
    			  con.setReadTimeout(60000);
				  OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(erpData));
				  streamWriter.flush();
				  streamWriter.close();
				  StringBuilder stringBuilder = null;
				  logger.info("Response Code : " + con.getResponseCode());
				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		                logger.info("Response From Spoton"+stringBuilder.toString());
		                PickupRescheduleResponseList responseList=gson.fromJson(stringBuilder.toString(),PickupRescheduleResponseList.class);
						logger.info("ResponseList "+gson.toJson(responseList));
						if(responseList!=null&&responseList.getPickUpResheduleUpdateStatus()!=null&&responseList.getPickUpResheduleUpdateStatus().size()>0){
							for(PickupRescheduleResponse p:responseList.getPickUpResheduleUpdateStatus()){
								PickupReassigned entity=mt.find(PickupReassigned.class, savedEntitiesMap.get(p.getPickUpScheduleID()));
								entity.setTransactionResult(p.getTransactionResult());
								mt.persist(entity);
								if(p.getTransactionResult().equalsIgnoreCase("false")) {
									if(assignedPickupsMap.containsKey(p.getPickUpScheduleID())) {
										assignedPickupsMap.remove(p.getPickUpScheduleID());
									}
									if(registeredPickupsMap.containsKey(p.getPickUpScheduleID())) {
										registeredPickupsMap.remove(p.getPickUpScheduleID());
									}
								}
							}
		            	   if(assignedPickupsMap.values()!=null&&assignedPickupsMap.values().size()>0) {
		            		   String query="Select p from PudData p where p.pickupScheduleId in :pickupScheduleIdList and p.userId=?1";
		            	       TypedQuery<PudData> tq=mt.createQuery(PudData.class, query).setParameter(1,dataList.get(0).getUserId());
		            	       tq.setParameter("pickupScheduleIdList",assignedPickupsMap.keySet());
		            	       List<PudData> pudList=tq.getResultList();
		            	       if(pudList!=null&&pudList.size()>0) {
		            	    	   for(PudData p:pudList) {
		            	    		  String reassignedUser=assignedPickupsMap.get(p.getPickupScheduleId()).split("/")[0];
		            	    		  String reassignedTime=assignedPickupsMap.get(p.getPickupScheduleId()).split("/")[1];
		            	    		  p.setUserId(reassignedUser);
		            	    		  p.setReassignDateTime(reassignedTime!=null?format.parse(reassignedTime):null);
		            	    		 p.setIsReassigned(1);
		            	    		 p.setUserBeforeReassign(userId);
		            	    		  mt.persist(p);
		            	    	   }
		            	       }
		            	   }
		            	   if(registeredPickupsMap.values()!=null&&registeredPickupsMap.values().size()>0) {
		            		   String query="Select p from PickupRegistration p where p.pickupScheduleId in :pickupScheduleIdList and p.mobileUserId=?1";
		            	       TypedQuery<PickupRegistration> tq=mt.createQuery(PickupRegistration.class, query).setParameter(1,dataList.get(0).getUserId());
		            	       tq.setParameter("pickupScheduleIdList",registeredPickupsMap.keySet());
		            	       List<PickupRegistration> registeredList=tq.getResultList();
		            	       if(registeredList!=null&&registeredList.size()>0) {
		            	    	   for(PickupRegistration p:registeredList) {
		            	    		  String reassignedUser=registeredPickupsMap.get(p.getPickupScheduleId()).split("/")[0];
		            	    		  String reassignedTime=registeredPickupsMap.get(p.getPickupScheduleId()).split("/")[1];
		            	    		  p.setMobileUserId(reassignedUser);
		            	    		  p.setReassignDateTime(reassignedTime!=null?format.parse(reassignedTime):null);
			            	    	  p.setIsReassigned(1);
			            	    	  p.setUserBeforeReassign(userId);
		            	    		  mt.persist(p);
		            	    	   }
		            	       }
		            	   }
		            	   
		               mt.commit();
		               result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,responseList));
						}
		    			
		    			
		    		}else{
		           
		            logger.info("Response From Spoton Failed");
		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		            resultMessage="Response Failed From ERP";
		            }
				
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
	            resultMessage="Response Failed From ERP";
			}
		}catch (SocketTimeoutException e){		
			e.printStackTrace();
			
			result = gson.toJson(new GeneralResponse(
					 Constants.FALSE,
					 "ERP did not respond. Please try again", 
   					null));
			resultMessage="ERP did not respond. Please try again";
			  
	}catch(Exception e){
		e.printStackTrace();
		logger.error("EXCEPTION_IN_SERVER",e);
		result = gson.toJson(new GeneralResponse(Constants.FALSE,
				Constants.ERRORS_EXCEPTION_IN_SERVER,null));
		resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
	}finally{
		if(mt!=null){mt.close();}
		
		if(con!=null){con.disconnect();}
	}
		logger.info("PickupReAssignService result"+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Pickup Reassigned", null,userId,null,ipadd,resultMessage,null);
		logger.info("auditlogStatus "+auditlogStatus);
		
		logger.info("**************END*******************");
		response.getWriter().write(result);
	}

}
