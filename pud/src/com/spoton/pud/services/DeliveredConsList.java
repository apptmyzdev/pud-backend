package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DeliveryUpdationModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.DeliveryUpdation;

/**
 * Servlet implementation class DeliveredConsList
 */
@WebServlet("/deliveredConsList")
public class DeliveredConsList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger deliveredConsLogger = Logger.getLogger("DeliveredConsList");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeliveredConsList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		deliveredConsLogger.info("**************START*******************");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ManageTransaction mt=null;String result = "";
		String userId =request.getParameter("userId");
		String status =request.getParameter("status");
		deliveredConsLogger.info("userId "+userId+" status "+status);
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String ipadd = request.getRemoteAddr();String resultMessage="";
		String serviceType="";
		Map<Integer,String> conMap=new HashMap<Integer, String>();
		deliveredConsLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		if(CommonTasks.check(userId,status)){
			int statusId=Integer.parseInt(status);
			String deliveryStatus="";
		try{
			 mt=new ManageTransaction();
			Calendar c=Calendar.getInstance();
			c.setTime(new Date());
			if(statusId==0){
				deliveryStatus="Delivery Attempted";
				serviceType="Viewed Delivery Attempted Cons List";
			}
			if(statusId==1){
				deliveryStatus="Delivered";
				serviceType="Viewed Delivered Cons List";
			}
			deliveredConsLogger.info("deliveryStatus "+deliveryStatus);
			/*String query="select d from DeliveryUpdation d,DeliveryUpdationStatus s where d.fieldEmployeeName=?1 and d.transactionId=s.transactionId and d.status=?6 "
					     + "and s.transactionResult=?2 and FUNC('DAY', d.transactionDate)=?3 and FUNC('MONTH', d.transactionDate)=?4 and FUNC('YEAR', d.transactionDate)=?5";*///removing joins
			String query="select d from DeliveryUpdation d where d.fieldEmployeeName=?1 and d.status=?6 "
				     + "and FUNC('DAY', d.createdTime)=?3 and FUNC('MONTH', d.createdTime)=?4 and FUNC('YEAR', d.createdTime)=?5";
			TypedQuery<DeliveryUpdation> typedquery=mt.createQuery(DeliveryUpdation.class, query);
		     typedquery.setParameter(1, userId);typedquery.setParameter(6, deliveryStatus);
			 typedquery.setParameter(3, c.getTime().getDate());typedquery.setParameter(4, c.getTime().getMonth()+1);typedquery.setParameter(5, c.get(Calendar.YEAR)); 
			 List<DeliveryUpdation> list=typedquery.getResultList();
			 List<DeliveryUpdationModal> deliveriesUpdated=new ArrayList<DeliveryUpdationModal>();
			if(list!=null&&list.size()>0){
				//deliveredConsLogger.info("list size "+list.size());
				 DeliveryUpdationModal dm=null;
				  for(DeliveryUpdation d:list){
					  dm=new DeliveryUpdationModal();
					  dm.setAwbNo(d.getAwbNo());
					  dm.setDeliveredTo(d.getDeliveredTo());
					  dm.setFieldEmployeeName(d.getFieldEmployeeName());
					  dm.setImageURL(d.getImageUrl());
					  dm.setLatValue(d.getLatVlue());
					  dm.setLongValue(d.getLongValue());
					  dm.setPdcNumber(d.getPdcNumber());
					  dm.setReason(d.getReason());
					  dm.setReceiverMobileNo(d.getReceiverMobileNo());
					  dm.setRelationship(d.getRelationship());
					  dm.setRemarks(d.getRemarks());
					  dm.setSignatureURL(d.getSignatureUrl());
					  dm.setStatus(d.getStatus());
					  dm.setTransactionDate(format.format(d.getTransactionDate()));
					  dm.setTransactionId(d.getTransactionId());
					  dm.setConsigneeName(d.getConsigneeName());
					  deliveriesUpdated.add(dm);
					  conMap.put(d.getTransactionId(), d.getAwbNo());
				  }
				  result = gson.toJson(new GeneralResponse(Constants.TRUE,
							Constants.REQUEST_COMPLETED, deliveriesUpdated));
				  resultMessage=Constants.REQUEST_COMPLETED;
			  }else{
		    	 result = gson.toJson(new GeneralResponse(Constants.FALSE,
							Constants.ERROR_NO_DATA_AVAILABLE, null));
		    	  resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
		    }
		}catch(Exception e){
				e.printStackTrace();
				deliveredConsLogger.error("EXCEPTION_IN_SERVER ",e);
				resultMessage="EXCEPTION_IN_SERVER "+e;
				result = gson.toJson(new GeneralResponse(
						Errors.status, 
						Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
						null));
			}finally{
				mt.close();
			}
		 
	}else{
		result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		resultMessage=Constants.ERROR_INCOMPLETE_DATA;
	}
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,serviceType, null,userId,null,ipadd,resultMessage,null);
		deliveredConsLogger.info("auditlogStatus "+auditlogStatus);
		deliveredConsLogger.info("DeliveredConsList result "+result);
		deliveredConsLogger.info("**************END*******************");
		//System.out.println("DeliveredConsList result "+result);
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

}
