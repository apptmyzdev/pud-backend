package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupExecutionErpResponseModal;
import com.spoton.pud.data.SubmitBoxPieceMappingErpModal;
import com.spoton.pud.data.SubmitBoxPieceMappingErpModalList;
import com.spoton.pud.data.SubmitBoxPieceMappingInputModal;
import com.spoton.pud.data.SubmitPickupExecutionErpModal;
import com.spoton.pud.data.SubmitPickupExecutionModal;
import com.spoton.pud.jpa.BoxPieceMappingDetail;
import com.spoton.pud.jpa.PickupExecutionDetail;

/**
 * Servlet implementation class SubmitBoxPieceMapping
 */
@WebServlet("/submitBoxPieceMapping")
public class SubmitBoxPieceMapping extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SubmitBoxPieceMapping");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmitBoxPieceMapping() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String deviceImei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("submitBoxPieceMapping");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceImei+" apkVersion "+apkVersion);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ManageTransaction mt=null;
		try{
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			Type type = new TypeToken<ArrayList<SubmitBoxPieceMappingInputModal>>(){}.getType();
			List<SubmitBoxPieceMappingInputModal> inputDataList = gson.fromJson(datasent,type);
			if (inputDataList != null&inputDataList.size()>0) {
				List<SubmitBoxPieceMappingErpModal> erpDataList=new ArrayList<SubmitBoxPieceMappingErpModal>();
				SubmitBoxPieceMappingErpModalList erpData=new SubmitBoxPieceMappingErpModalList();
				mt=new ManageTransaction();
				Map<String,Integer> idMap=new HashMap<String,Integer>();
				for(SubmitBoxPieceMappingInputModal sp:inputDataList) {
					BoxPieceMappingDetail entity=new BoxPieceMappingDetail();
					entity.setBoxNumber(sp.getBoxNumber());
					entity.setConNumber(sp.getConNumber());
					entity.setCreatedTimestamp(new Date());
					entity.setIsBoxPieceMapping(sp.getIsBoxPieceMapping());
					entity.setPieceNumber(sp.getPieceNumber());
					entity.setScannedDatetime(sp.getScannedDatetime()!=null?format.parse(sp.getScannedDatetime()):null);
					entity.setUserId(sp.getUserId());
					mt.persist(entity);
					mt.commit();
					idMap.put(sp.getPieceNumber(),entity.getId());
					SubmitBoxPieceMappingErpModal ed=new SubmitBoxPieceMappingErpModal();
					ed.setBoxNo(sp.getBoxNumber());
					ed.setConNumber(sp.getConNumber());
					ed.setIsBoxMapping(sp.getIsBoxPieceMapping());
					ed.setPieceNo(sp.getPieceNumber());
					ed.setScannedDateTime(sp.getScannedDatetime());
					ed.setUserID(sp.getUserId());
					erpDataList.add(ed);
				}
				erpData.setAPIPiecesUpdate_Input(erpDataList);
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);

				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				streamWriter.write(gson.toJson(erpData));
				streamWriter.flush();
				streamWriter.close();
				logger.info("Datasent to spoton " + gson.toJson(erpData));
				logger.info("Response Code : " + con.getResponseCode());
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stringBuilder = new StringBuilder();
					InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
					BufferedReader bufferedReader = new BufferedReader(streamReader);
					String response1 = null;
					while ((response1 = bufferedReader.readLine()) != null) {
						stringBuilder.append(response1);
					}
					bufferedReader.close();
					streamReader.close();
					logger.info("Response From Spoton : " + stringBuilder.toString());
					PickupExecutionErpResponseModal erpResponse=gson.fromJson(stringBuilder.toString(),PickupExecutionErpResponseModal.class);
    				if(erpResponse!=null&&erpResponse.getTransationResult()){
    					for(String pieceNumber:idMap.keySet()) {
    					BoxPieceMappingDetail pe=mt.find(BoxPieceMappingDetail.class, idMap.get(pieceNumber));
    					if(pe!=null) {
    						pe.setErpUpdated(1);
    						pe.setTransactionResult(String.valueOf(erpResponse.getTransationResult()));
    						pe.setTransactionMessge(erpResponse.getTransationMessage());
    						mt.persist(pe);
    					}
    					}
    					//}
    					mt.commit();
    					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
    				}
					
				} else {
					logger.info("Response failed from Spoton");
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Response failed from Spoton with Response code as " + con.getResponseCode(), null));
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
			if(con!=null) {
				con.disconnect();
			}
		}
		logger.info("result " + result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
