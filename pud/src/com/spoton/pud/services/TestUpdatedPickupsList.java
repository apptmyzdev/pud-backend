package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModalV3;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.PieceEntry;

/**
 * Servlet implementation class TestUpdatedPickupsList
 */
@WebServlet("/testUpdatedPickupsList")
public class TestUpdatedPickupsList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger updatedPickupsLogger = Logger.getLogger("TestUpdatedPickupsList");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestUpdatedPickupsList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		updatedPickupsLogger.info("**************START*******************");
		Gson gson = new GsonBuilder().serializeNulls().create();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ManageTransaction mt=null;String result = "";
		String userId =request.getParameter("userId");
		updatedPickupsLogger.info("userId"+userId);
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		 String resultMessage="";
		String ipadd = request.getRemoteAddr();
		updatedPickupsLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		if(CommonTasks.check(userId)){
			 mt=new ManageTransaction();
		try{
			Calendar c=Calendar.getInstance();
			c.setTime(new Date());
			ConDetailsModalV3 cm=null;
			/*String query3=" select p.conDetail.customerCode,p.conDetail.pickupDate,p.conDetail.originPincode,p.conDetail.noOfPackage,"
					      + "p.conDetail.crmScheduleId,p.conDetail.conNumber,p.conDetail.pieceEntries,p.conDetail.orderNo,p.conDetail.gatePassTime "
					      + "from PickupUpdationStatus p where p.conDetail.userName=?1 and p.transactionResult=?2 and FUNC('DAY', p.conDetail.pickupDate)=?3 and FUNC('MONTH', p.conDetail.pickupDate)=?4 and FUNC('YEAR', p.conDetail.pickupDate)=?5";*/
			/*String query3="select p from PickupUpdationStatus p where p.conDetail.userName=?1 and p.transactionResult=?2 "
					    + "and FUNC('DAY', p.conDetail.pickupDate)=?3 and FUNC('MONTH', p.conDetail.pickupDate)=?4 and FUNC('YEAR', p.conDetail.pickupDate)=?5";
			TypedQuery<PickupUpdationStatus> typedquery=mt.createQuery(PickupUpdationStatus.class, query3);
		     typedquery.setParameter(1, userId);typedquery.setParameter(2, "Success");
			 typedquery.setParameter(3, c.getTime().getDate());typedquery.setParameter(4, c.getTime().getMonth()+1);typedquery.setParameter(5, c.get(Calendar.YEAR));
		    List<PickupUpdationStatus> list=typedquery.getResultList();*/
			/*String query3="SELECT c.con_number,c.customer_code,c.pickup_date,c.order_no,c.origin_pincode,c.crm_schedule_id,c.gate_pass_time,c.special_instruction,c.customer_name,c.no_of_package "
					     +"FROM pud.con_details c, pud.pickup_updation_status p WHERE p.con_entry_id = c.id AND p.transaction_result=?2 AND DATE(c.pickup_date)=CURDATE() AND c.user_name =?1";
			*/
			String query="select pe from PieceEntry pe,ConDetail c where c.conEntryId=pe.conDetail.conEntryId and c.userName=?1 and FUNC('DAY', pe.createdTimestamp)=?3 and FUNC('MONTH', pe.createdTimestamp)=?4 and FUNC('YEAR', pe.createdTimestamp)=?5";	
			TypedQuery<PieceEntry> typedquery1=mt.createQuery(PieceEntry.class,query).setParameter(1, userId);
			typedquery1.setParameter(3, c.getTime().getDate());typedquery1.setParameter(4, c.getTime().getMonth()+1);typedquery1.setParameter(5, c.get(Calendar.YEAR));
			List<PieceEntry> pieceList=typedquery1.getResultList();
		
		    String query3="select c from ConDetail c where c.userName=?1 and c.pickupType!=4 and c.pickupType!=3 and c.pickupType!=1 "
				    + "and FUNC('DAY', c.createdTimestamp)=?3 and FUNC('MONTH', c.createdTimestamp)=?4 and FUNC('YEAR', c.createdTimestamp)=?5";
		    TypedQuery<ConDetail> typedquery=mt.createQuery(ConDetail.class, query3);
		     typedquery.setParameter(1, userId);
			 typedquery.setParameter(3, c.getTime().getDate());typedquery.setParameter(4, c.getTime().getMonth()+1);typedquery.setParameter(5, c.get(Calendar.YEAR));
		     List<ConDetail> list=typedquery.getResultList();
		    /*Query nativeQuery=mt.createNativeQuery(query3);
			List<Object[]> list=nativeQuery.getResultList();*/
		    List<ConDetailsModalV3> consList=new ArrayList<ConDetailsModalV3>();
		    List<PieceEntryModal> pieceEntry=null;
		    if(list!=null&&list.size()>0){
		    	updatedPickupsLogger.info(" list size "+list.size());
		    for(ConDetail o:list){
		    	cm=new ConDetailsModalV3();
		    	//updatedPickupsLogger.info(" object o "+o);
		    	if(o!=null){
		    	cm.setConNumber(o.getConNumber());
		    	cm.setCustomerCode(o.getCustomerCode());
		    	if(o.getPickupDate()!=null)
		    	cm.setPickupDate(format.format(o.getPickupDate()));
		    	cm.setOrderNo(o.getOrderNo());
		        cm.setOriginPinCode(o.getOriginPincode());
		        cm.setCrmScheduleId(o.getCrmScheduleId());
		        cm.setGatePassTime(o.getGatePassTime());
		        cm.setSpecialInstruction(o.getSpecialInstruction());
		        cm.setCustomerName(o.getCustomerName());
		    	cm.setNoOfPackage(o.getNoOfPackage());
		        cm.setProductType(o.getProductType());
		    	pieceEntry=new ArrayList<PieceEntryModal>();
		    	if(pieceList!=null&&pieceList.size()>0)
		    	for(PieceEntry p:pieceList){
		    		if((p.getConDetail().getConEntryId())==o.getConEntryId()){
		    			PieceEntryModal pm=new PieceEntryModal();
			    		pm.setFromPieceNo(p.getFromPieceNo());
			    	    pm.setToPieceNo(p.getToPieceNo());
			    	    pm.setTotalPieces(p.getTotalPieces());
			    	    pieceEntry.add(pm);
		    		}
		    	}
		    	/*if(o.getPieceEntries()!=null)
		    	for(PieceEntry p:o.getPieceEntries()){
		    		PieceEntryModal pm=new PieceEntryModal();
		    		pm.setFromPieceNo(p.getFromPieceNo());
		    	    pm.setToPieceNo(p.getToPieceNo());
		    	    pm.setTotalPieces(p.getTotalPieces());
		    	    pieceEntry.add(pm);
		    	}*/
		    	cm.setPieceEntry(pieceEntry);
		    	consList.add(cm);
		    }
		    }
		    }
		    String cashBookedCons="select c from ConDetail c,CashBookingResponse r where c.userName=?1 and c.pickupType=3 and c.conEntryId=r.conEntryId and r.flag=?2 and c.pickupScheduleId!=0 "
				                  + "and FUNC('DAY', c.createdTimestamp)=?3 and FUNC('MONTH', c.createdTimestamp)=?4 and FUNC('YEAR', c.createdTimestamp)=?5";
		    
		    TypedQuery<ConDetail> typedquery2=mt.createQuery(ConDetail.class, cashBookedCons);
		    typedquery2.setParameter(1, userId);typedquery2.setParameter(2, "Y");
		    typedquery2.setParameter(3, c.getTime().getDate());typedquery2.setParameter(4, c.getTime().getMonth()+1);typedquery2.setParameter(5, c.get(Calendar.YEAR));
		     List<ConDetail> cashBookedList=typedquery2.getResultList();
		     if(cashBookedList!=null&&cashBookedList.size()>0){
			    	updatedPickupsLogger.info(" cash booked size "+cashBookedList.size());
			    	
			    	 for(ConDetail o:cashBookedList){
			    		 cm=new ConDetailsModalV3();
			    			cm.setConNumber(o.getConNumber());
					    	cm.setCustomerCode(o.getCustomerCode());
					    	if(o.getPickupDate()!=null)
					    	cm.setPickupDate(format.format(o.getPickupDate()));
					    	cm.setOrderNo(o.getOrderNo());
					        cm.setOriginPinCode(o.getOriginPincode());
					        cm.setCrmScheduleId(o.getCrmScheduleId());
					        cm.setGatePassTime(o.getGatePassTime());
					        cm.setSpecialInstruction(o.getSpecialInstruction());
					        cm.setCustomerName(o.getCustomerName());
					    	cm.setNoOfPackage(o.getNoOfPackage());
					    	cm.setProductType(o.getProductType());
					    	pieceEntry=new ArrayList<PieceEntryModal>();
					    	if(pieceList!=null&&pieceList.size()>0)
					    	for(PieceEntry p:pieceList){
					    		if((p.getConDetail().getConEntryId())==o.getConEntryId()){
					    			PieceEntryModal pm=new PieceEntryModal();
						    		pm.setFromPieceNo(p.getFromPieceNo());
						    	    pm.setToPieceNo(p.getToPieceNo());
						    	    pm.setTotalPieces(p.getTotalPieces());
						    	    pieceEntry.add(pm);
					    		}
					    	}
					    	cm.setPieceEntry(pieceEntry);
					    	consList.add(cm);
			    	 }
			    	
			    	
			    	}
		    
		    updatedPickupsLogger.info(" consList "+gson.toJson(consList));
		    result = gson.toJson(new GeneralResponse(Constants.TRUE,
					Constants.REQUEST_COMPLETED, consList));
		    resultMessage=Constants.REQUEST_COMPLETED;
		    
		   if(list.size()==0&&cashBookedList.size()==0){
		    	 result = gson.toJson(new GeneralResponse(Constants.FALSE,
							Constants.ERROR_NO_DATA_AVAILABLE, null));
		    	 resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
		    }
		}catch(Exception e){
				e.printStackTrace();
				updatedPickupsLogger.error("EXCEPTION_IN_SERVER ",e);
				result = gson.toJson(new GeneralResponse(
						Errors.status, 
						Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
						null));
				 resultMessage="EXCEPTION_IN_SERVER "+e;
			}finally{
				if(mt!=null)
				mt.close();
			}
		 
	}else{
		result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		resultMessage=Constants.ERROR_INCOMPLETE_DATA;
	}
		 updatedPickupsLogger.info(" UpdatedPickupsList result "+result);
		 System.out.println(" UpdatedPickupsList result "+result);
		 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Viewed Updated Pickups", null,userId,null,ipadd,resultMessage,null);
		 updatedPickupsLogger.info("auditlogStatus "+auditlogStatus);
		 updatedPickupsLogger.info("**************END*******************");
		response.getWriter().write(result);
		
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
