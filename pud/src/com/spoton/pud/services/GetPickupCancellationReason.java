package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupCancellationMasterModal;
import com.spoton.pud.data.PickupCancellationReasonModal;
import com.spoton.pud.jpa.PickupCancellationReasonMaster;

/**
 * Servlet implementation class SavePickupCancellationReason
 */
@WebServlet("/getPickupCancellationReason")
public class GetPickupCancellationReason extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPickupCancellationReason() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		String result = "";
		ManageTransaction mt = null;
		String urlString = FilesUtil.getProperty("PickupCancellationReasonMasterUrl");
		URL obj = new URL(urlString);
		BufferedReader in = null;
		StringBuffer response1 = new StringBuffer();
		HttpURLConnection con=null;
		try{
			 con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			int responseCode = con.getResponseCode();
			//System.out.println("responseCode "+responseCode);
	        in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	
			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			//System.out.println("response1 "+response1.toString());
			Type type=new TypeToken<ArrayList<PickupCancellationReasonModal>>(){}.getType();
			List<PickupCancellationReasonModal> reasonList=gson.fromJson(response1.toString(),type);
			List<PickupCancellationReasonMaster> list=new ArrayList<PickupCancellationReasonMaster>();
			int activeStatus=0;
			if(reasonList!=null&&reasonList.size()>0){
			PickupCancellationReasonMaster rm=null;
			mt=new ManageTransaction();
			String query="Select pm.reasonCode from PickupCancellationReasonMaster pm";
			TypedQuery<String> pquery=mt.createQuery(query,String.class);
			List<String> dataList=pquery.getResultList();
			for(PickupCancellationReasonModal p:reasonList){
				rm=new PickupCancellationReasonMaster();
				rm.setFailureCategory(p.getFailureCategory());
				rm.setPickupStatusId(p.getPickupStatusId());
				rm.setReasonCode(p.getReasonCode());
				rm.setReasonDesc(p.getReasonDesc());
				rm.setUpdatedBy(p.getUpdatedBy());
				rm.setUpdatedOn(p.getUpdatedOn());
				if(p.isActiveStatus()){activeStatus=1;}
				rm.setActiveStatus(activeStatus);
				list.add(rm);
				if(!dataList.contains(p.getReasonCode())){
					mt.persist(rm);	
				}
				
			}
			mt.commit();
			result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, list));
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
			}
		}catch(Exception e){
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null){mt.close();}
			if(in!=null){in.close();}
			if(con!=null){con.disconnect();}
		}
	   response.getWriter().write(result);
	  // System.out.println("result "+result);
	}

}
