package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.BranchDepotRegionModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PinCodeSpotonResponse;
import com.spoton.pud.jpa.AllBranch;
import com.spoton.pud.jpa.AllDepot;
import com.spoton.pud.jpa.AllRegion;

/**
 * Servlet implementation class BranchDepotRegion
 */
@WebServlet("/branchDepotRegion")
public class BranchDepotRegion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BranchDepotRegion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con1=null; URL obj1 =null;
		
		String urlString="";
		ManageTransaction mt=null;
		StringBuilder stringBuilder = null;
		try{
			mt=new ManageTransaction();
			String query="Select r.regionCode from AllRegion r";
			TypedQuery<String> tq=mt.createQuery(String.class, query);
			List<String> regionList=tq.getResultList();
			urlString=FilesUtil.getProperty("branchRegionDepot") +"R";
			 obj1 = new URL(urlString);
			 con1 = (HttpURLConnection) obj1.openConnection();
			 con1.setRequestMethod("GET");
			 con1.setConnectTimeout(5000);
			 con1.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			 if (con1.getResponseCode() == HttpURLConnection.HTTP_OK){
	            	stringBuilder=new StringBuilder();
	                InputStreamReader streamReader = new InputStreamReader(con1.getInputStream());
	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	                String response1 = null;
	                while ((response1 = bufferedReader.readLine()) != null) {
	                    stringBuilder.append(response1);
	                }
	                bufferedReader.close();
	                streamReader.close();
	                Type regionListType = new TypeToken<ArrayList<BranchDepotRegionModal>>(){}.getType();
	    			List<BranchDepotRegionModal> regions =  gson.fromJson(stringBuilder.toString(),	regionListType);
	    			if(regions!=null&&regions.size()>0){
	    				for(BranchDepotRegionModal m:regions){
	    				if(!regionList.contains(m.getRegion_Code())){
	    					AllRegion ar=new AllRegion();
	    					ar.setErrMsg(m.getErrMsg());
	    					ar.setResult(m.getResult());
	    					ar.setRegionCode(m.getRegion_Code());
	    					ar.setRegionName(m.getRegion_Name());
	    					mt.persist(ar);
	    					mt.commit();
	    				}
	    			}
	    			}
			 }
	    			String query2="Select d.depotCode from AllDepot d";
	    			TypedQuery<String> tq1=mt.createQuery(String.class, query2);
	    			List<String> depotList=tq1.getResultList();
	    			urlString=FilesUtil.getProperty("branchRegionDepot") +"D";
	    			 obj1 = new URL(urlString);
	    			 con1 = (HttpURLConnection) obj1.openConnection();
	    			 con1.setRequestMethod("GET");
	    			 con1.setConnectTimeout(5000);
	    			 con1.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
	    			 if (con1.getResponseCode() == HttpURLConnection.HTTP_OK){
	 	            	stringBuilder=new StringBuilder();
	 	                InputStreamReader streamReader = new InputStreamReader(con1.getInputStream());
	 	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	 	                String response1 = null;
	 	                while ((response1 = bufferedReader.readLine()) != null) {
	 	                    stringBuilder.append(response1);
	 	                }
	 	                bufferedReader.close();
	 	               streamReader.close();
	 	                Type depotListType = new TypeToken<ArrayList<BranchDepotRegionModal>>(){}.getType();
	 	    			List<BranchDepotRegionModal> depots =  gson.fromJson(stringBuilder.toString(),	depotListType);
	 	    			if(depots!=null&&depots.size()>0){
	 	    				for(BranchDepotRegionModal m:depots){
	 	    				if(!depotList.contains(m.getDepot_Code())){
	 	    					AllDepot ad=new AllDepot();
	 	    					ad.setErrMsg(m.getErrMsg());
	 	    					ad.setResult(m.getResult());
	 	    					ad.setRegionCode(m.getRegion_Code());
	 	    					ad.setRegionName(m.getRegion_Name());
	 	    					ad.setDepotCode(m.getDepot_Code());
	 	    					ad.setDepotName(m.getDepot_Name());
	 	    					mt.persist(ad);
		    					mt.commit();
	 	    				}
	 	    			}
	 	    			}
	 			 }
	    			 
	    			 
	    			 String query3="Select b.branchCode from AllBranch b";
		    			TypedQuery<String> tq2=mt.createQuery(String.class, query3);
		    			List<String> branchList=tq2.getResultList();
		    			urlString=FilesUtil.getProperty("branchRegionDepot") +"B";
		    			 obj1 = new URL(urlString);
		    			 con1 = (HttpURLConnection) obj1.openConnection();
		    			 con1.setRequestMethod("GET");
		    			 con1.setConnectTimeout(5000);
		    			 con1.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
		    			 if (con1.getResponseCode() == HttpURLConnection.HTTP_OK){
		 	            	stringBuilder=new StringBuilder();
		 	                InputStreamReader streamReader = new InputStreamReader(con1.getInputStream());
		 	                BufferedReader bufferedReader = new BufferedReader(streamReader);
		 	                String response1 = null;
		 	                while ((response1 = bufferedReader.readLine()) != null) {
		 	                    stringBuilder.append(response1);
		 	                }
		 	                bufferedReader.close();
		 	               streamReader.close();
		 	                Type branchListType = new TypeToken<ArrayList<BranchDepotRegionModal>>(){}.getType();
		 	    			List<BranchDepotRegionModal> branches =  gson.fromJson(stringBuilder.toString(),	branchListType);
		 	    			if(branches!=null&&branches.size()>0){
		 	    				for(BranchDepotRegionModal m:branches){
		 	    				if(!branchList.contains(m.getBranch_Code())){
		 	    					AllBranch ab=new AllBranch();
		 	    					ab.setErrMsg(m.getErrMsg());
		 	    					ab.setResult(m.getResult());
		 	    					ab.setRegionCode(m.getRegion_Code());
		 	    					ab.setRegionName(m.getRegion_Name());
		 	    					ab.setDepotCode(m.getDepot_Code());
		 	    					ab.setDepotName(m.getDepot_Name());
		 	    					ab.setBranchCode(m.getBranch_Code());
		 	    					ab.setBranchName(m.getBranch_Name());
		 	    					mt.persist(ab);
			    					mt.commit();
		 	    				}
		 	    			}
		 	    			}
		 			 }
		    			
		    			 result = gson.toJson(new GeneralResponse(
		 						Constants.TRUE,
		 						Constants.REQUEST_COMPLETED, 
		 						null));
			
		}catch(Exception e){
			
			e.printStackTrace();
		
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			
		}finally{
			if(mt!=null){mt.close();}
			
			if(con1!=null){con1.disconnect();}
		}
		
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
