package com.spoton.pud.services;

import java.io.IOException;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class BackupUserLocation
 */
@WebServlet("/backupUserLocation")
public class BackupUserLocation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("BackupUserLocation");
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BackupUserLocation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START*******************");
		ManageTransaction mt = new ManageTransaction();
		String result="";
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {
		
        int cnt = 0;
		int cumcnt = 0;
		int moveSuccessFlag=0;
		//get data before last 2 days 
		try {
		//String sql = "insert into user_location_old (select * from user_location where timestamp<Date(NOW() - INTERVAL 2 DAY) and id >(select max(id) from user_location_old) order by id limit 10000) ";
			String sql = "insert into user_location_old (select * from user_location where timestamp<Date(NOW() - INTERVAL 2 DAY) and timestamp >(select max(timestamp) from user_location_old) order by timestamp asc limit 10000) ";
		Query q = mt.createNativeQuery(sql);
		do {
            mt.begin();//needs to begin new transaction every time
			cnt = q.executeUpdate();
			cumcnt += cnt;
			if(cumcnt>0) {
			mt.commit();//on commit only data gets saved
			moveSuccessFlag=1;
			}
			logger.info("Total records inserted till now in iteration:- " + cumcnt);
		} while (cnt == 10000);
		logger.info("Total records inserted Finally :- " + cumcnt);
		cumcnt=0;
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception while moving data", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			moveSuccessFlag=0;
		}
		if(moveSuccessFlag==1) {
		String sql1="delete from user_location where timestamp<Date(NOW() - INTERVAL 2 DAY) order by timestamp asc limit 10000";
		Query q1 = mt.createNativeQuery(sql1);
		do {
            mt.begin();//needs to begin new transaction every time
			cnt = q1.executeUpdate();
			cumcnt += cnt;
			mt.commit();
			logger.info("Total records deleted till now in iteration :- " + cumcnt);
		} while (cnt == 10000);
		logger.info("Total records deleted Finally :- " + cumcnt);
		result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
		}
		
		
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		logger.info("**************END*******************");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
