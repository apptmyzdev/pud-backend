package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PullPudDataModal;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import com.spoton.pud.jpa.PickupRegistration;
import com.spoton.pud.jpa.PudData;
import com.spoton.pud.jpa.UserSessionData;
/**
 * Servlet implementation class PullPudData
 */
@WebServlet("/pullPudDataOLD")
public class PullPudDataOLD extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pullPudDataLogger = Logger.getLogger("PullPudData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullPudDataOLD() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pullPudDataLogger.info("**************START*******************");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		String result = "";
	    String userId=request.getParameter("userId");
		ManageTransaction mt=null;
		pullPudDataLogger.info("userId "+userId);
		String deviceIMEI =request.getHeader("imei");
		String apkVersion=request.getHeader("apkVersion");
		String resultMessage="";
		String ipadd = request.getRemoteAddr();
		pullPudDataLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		
		try{
		if((CommonTasks.check(userId))){
			boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mt=new ManageTransaction();
			Calendar c=Calendar.getInstance();
			c.setTime(new Date());
			c.set(Calendar.HOUR_OF_DAY, 23);
			c.set(Calendar.MINUTE, 59);
			c.set(Calendar.SECOND, 59);
			c.set(Calendar.MILLISECOND, 0);
			Map<Integer, PullPudDataModal> map = new HashMap<Integer, PullPudDataModal>();
			
			/*String versionQuery="select id,apk_version from user_session_data where user_name=?1 and active_flag=1 and imei=?2";
			Query q=mt.createNativeQuery(versionQuery).setParameter(1, userId).setParameter(2, deviceIMEI);
			try{
			Object[] o=(Object[]) q.getSingleResult();
			if(o!=null){
				if(Float.parseFloat((String) o[1])<Float.parseFloat(apkVersion)){
					UserSessionData u=mt.find(UserSessionData.class, (int) o[0]);
					u.setApkVersion(apkVersion);
					mt.persist(u);
					mt.commit();
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}*/
			
			
			
			String query="Select d from PudData d where d.userId=?1 and FUNC('DAY', d.pickupDate)=?2 and FUNC('MONTH', d.pickupDate)=?3 and FUNC('YEAR', d.pickupDate)=?4";
			TypedQuery<PudData> typedQuery=mt.createQuery(PudData.class, query);
			typedQuery.setParameter(1, userId);
			typedQuery.setParameter(2, c.getTime().getDate());typedQuery.setParameter(3, c.getTime().getMonth()+1);typedQuery.setParameter(4, c.get(Calendar.YEAR));
		
			List<PudData> list=typedQuery.getResultList();
			PullPudDataModal pd=null;
		

			if(list!=null&&list.size()!=0){
				pullPudDataLogger.info("PudData List size fom db "+list.size());
				//System.out.println("PudData List size fom db "+list.size());
				for(Object s:list){
					PudData p=(PudData)s;
					pd=new PullPudDataModal();
					pd.setId(p.getId());
					pd.setCft(p.getCft());
					pd.setContactMobile(p.getContactMobile());
					pd.setContactPerson(p.getContactPerson());
					pd.setCustomerCode(p.getCustomerCode());
					pd.setCustomerName(p.getCustomerName());
					pd.setCustomerRefNumber(p.getCustomerRefNumber());
					pd.setDeliveryAddress(p.getDeliveryAddress());
					pd.setDeliveryCity(p.getDeliveryCity());
					pd.setDeliveryPincode(p.getDeliveryPincode());
					pd.setMinCfgWeight(p.getMinCfgWeight());
					pd.setPickupAddress(p.getPickupAddress());
					pd.setPickupCity(p.getPickupCity());
					if(p.getPickupDate()!=null)
					pd.setPickupDate(format.format(p.getPickupDate()));
					pd.setPickupOrderNo(p.getPickupOrderNo());
					pd.setPickupPincode(p.getPickupPincode());
					pd.setPickupRegisterId(p.getPickupRegisterId());
					pd.setPickupScheduleId(p.getPickupScheduleId());
					pd.setPieces(p.getPieces());
					pd.setUserId(p.getUserId());
					pd.setWeight(p.getWeight());
					
					
					map.put(p.getPickupScheduleId(), pd);
					pullPudDataLogger.info("PickupScheduleId from pudData "+p.getPickupScheduleId());
					
				}
			}
				String query2="Select p from PickupRegistration p where p.mobileUserId=?1 and p.transactionResult=1 and FUNC('DAY', p.pickupDate)=?2 and FUNC('MONTH', p.pickupDate)=?3 and FUNC('YEAR', p.pickupDate)=?4";
				TypedQuery<PickupRegistration> typedQuery2=mt.createQuery(PickupRegistration.class, query2).setParameter(1, userId);
				typedQuery2.setParameter(2, c.getTime().getDate());typedQuery2.setParameter(3, c.getTime().getMonth()+1);typedQuery2.setParameter(4, c.get(Calendar.YEAR));
				List<PickupRegistration> list1=typedQuery2.getResultList();
				if(list1!=null&&list1.size()>0){
					for(PickupRegistration pr:list1){
						pd=new PullPudDataModal();
						pd.setId(pr.getId());
						pd.setCft(pr.getCft());
						pd.setContactMobile(pr.getContactPersonNumber());
						pd.setContactPerson(pr.getContactPersonName());
						pd.setCustomerCode(pr.getCustomerCode());
						pd.setCustomerName(pr.getCustomerName());
						pd.setPickupAddress(pr.getPickupAddress());
						if(pr.getPickupDate()!=null)
						pd.setPickupDate(format.format(pr.getPickupDate()));
						pd.setPickupOrderNo(pr.getPickupOrderNo());
						pd.setPickupPincode(pr.getPickupPincode());
						pd.setPickupScheduleId(pr.getPickupScheduleId());
						pd.setPieces(pr.getPieces());
						pd.setUserId(pr.getMobileUserId());
						pd.setWeight((float)pr.getWeight());
						map.put(pr.getPickupScheduleId(), pd);
						pullPudDataLogger.info("PickupScheduleId from PickupRegistration "+pr.getPickupScheduleId());
						
					}
				}
				/*String query3="select p.conDetail.pickupScheduleId from PickupUpdationStatus p where p.transactionResult=?1 and p.conDetail.userName=?2 and FUNC('DAY', p.conDetail.pickupDate)=?3 "
						      +"and FUNC('MONTH', p.conDetail.pickupDate)=?4 and FUNC('YEAR', p.conDetail.pickupDate)=?5";*/
				String query3="select p.pickupScheduleId from ConDetail p where p.userName=?2 and FUNC('DAY', p.pickupDate)=?3 "
					      +"and FUNC('MONTH', p.pickupDate)=?4 and FUNC('YEAR', p.pickupDate)=?5";
				TypedQuery<Integer> tq=mt.createQuery(Integer.class, query3);
				tq.setParameter(2, userId);
				tq.setParameter(3, c.getTime().getDate());tq.setParameter(4, c.getTime().getMonth()+1);tq.setParameter(5, c.get(Calendar.YEAR));
				List<Integer> updatedPickups=tq.getResultList();
				if(updatedPickups!=null&&updatedPickups.size()!=0)
				for(Integer o:updatedPickups){
					pullPudDataLogger.info(" PickupScheduleId from PickupUpdation "+o);
					if(map.containsKey(o)){
						map.remove(o);
					}
				}
				
				String query4="Select r.pickupScheduleId from PickupReschedule r,PickupRescheduleUpdateStatus s where s.pickupScheduleId=r.pickupScheduleId "
						     + "and r.appMobileUserName=?1 and FUNC('DAY', s.timestamp)=?2 and FUNC('MONTH', s.timestamp)=?3 and FUNC('YEAR', s.timestamp)=?4 and s.transactionResult=?5";
				TypedQuery<Integer> tquery=mt.createQuery(Integer.class, query4);
				tquery.setParameter(5, "true");tquery.setParameter(1, userId);
				tquery.setParameter(2, c.getTime().getDate());tquery.setParameter(3, c.getTime().getMonth()+1);tquery.setParameter(4, c.get(Calendar.YEAR));
				List<Integer> attempedPickups=tquery.getResultList();
				if(attempedPickups!=null&&attempedPickups.size()!=0)
				for(Integer o:attempedPickups){
					pullPudDataLogger.info("PickupScheduleId from PickupReschedule "+o);
					if(map.containsKey(o)){
						map.remove(o);
					}
				}
				if(map.values()!=null&&map.values().size()!=0){
				result = gson.toJson(new GeneralResponse(Constants.TRUE,
						Constants.REQUEST_COMPLETED,map.values()));
				resultMessage=Constants.REQUEST_COMPLETED;
				}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_NO_DATA_AVAILABLE,null));
				resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
				}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.UPDATE_TO_LATEST_VERSION, null));
				
			}
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERROR_INCOMPLETE_DATA,null));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
		}
		}catch(Exception e){
			e.printStackTrace();
			//System.out.println("e.getStackTrace()"+e.getStackTrace());
			pullPudDataLogger.info("EXCEPTION_IN_SERVER "+e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			resultMessage="EXCEPTION_IN_SERVER "+e;
		}finally{
			if(mt!=null)
			mt.close();
		}
		pullPudDataLogger.info("PullPudData result "+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Pulled Pud Data", null,userId,null,ipadd,resultMessage,null);
		pullPudDataLogger.info("auditlogStatus "+auditlogStatus);
		//System.out.println("result "+result);
		response.getWriter().write(result);
	
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
		

}
