package com.spoton.pud.services;

import java.io.IOException;
import java.util.Date;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DeviceInfoModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.DeviceInfo;

/**
 * Servlet implementation class DeviceRegistration
 */
@WebServlet("/deviceRegistration")
public class DeviceRegistration extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger deviceRegistrationLogger = Logger.getLogger("DeviceRegistration");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeviceRegistration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		deviceRegistrationLogger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		response.setCharacterEncoding(Constants.CHARACTER_ENCODING_UTF_8);

		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String dataSent = "";
		String ipAddress = request.getRemoteHost();
		deviceRegistrationLogger.info(" ipAddress "+ipAddress);
		dataSent = request.getReader().readLine();
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		 String resultMessage="";
		String ipadd = request.getRemoteAddr();
		deviceRegistrationLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		DeviceInfoModal deviceInfoData = null;
		if (CommonTasks.check(dataSent)) {
			deviceRegistrationLogger.info(" DataSent from mobile "+dataSent);
			try {
				deviceInfoData = gson.fromJson(dataSent, DeviceInfoModal.class);
				deviceRegistrationLogger.info(" Device Info Data "+gson.toJson(deviceInfoData));
			} catch (Exception e) {
				e.printStackTrace();
				deviceRegistrationLogger.info(" ERROR PARSING REQUEST "+e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_PARSING_REQUEST_DATA,0));
				resultMessage=Constants.ERROR_PARSING_REQUEST_DATA;
			}
			ManageTransaction manageTransaction =null;
			try{
			if (deviceInfoData != null && result.length() == 0) {
                
				String uniqueKey =null;
				//uniqueKey = deviceInfoData.getDeviceMac().concat(deviceInfoData.getDeviceModel()).concat(deviceInfoData.getDeviceName());
				uniqueKey=deviceInfoData.getDeviceImei();
				deviceRegistrationLogger.info(" uniqueKey "+uniqueKey);
				DeviceInfo deviceInfo = null;
				manageTransaction= new ManageTransaction();
				try {
					//String query = "select di from DeviceInfo di  where di.uniqueKey like ?1";
					String query = "select di from DeviceInfo di  where di.deviceImei=?1 order by di.deviceId desc";
					TypedQuery<DeviceInfo> query1 = manageTransaction.createQuery(DeviceInfo.class, query).setMaxResults(1);
					query1.setParameter(1, uniqueKey);

					deviceInfo = query1.getSingleResult();
					deviceRegistrationLogger.info(" Query Result "+deviceInfo);
				} catch (Exception e) {
				//	e.printStackTrace();
					deviceRegistrationLogger.info(" No result exception "+e);
					deviceInfo = null;
				}
				if (deviceInfo == null) {
					deviceInfo = new DeviceInfo();
				}
				   if(deviceInfo!=null){
					deviceInfo.setActiveFlag(1);
					deviceInfo.setAppVersion(deviceInfoData.getAppVersion());
					deviceInfo.setDeviceAvailableMemory(deviceInfoData.getDeviceAvailableMemory());
					deviceInfo.setDeviceBattery(deviceInfoData.getDeviceBattery());
					deviceInfo.setDeviceBrand(deviceInfoData.getDeviceBrand());
					deviceInfo.setDeviceImei(deviceInfoData.getDeviceImei());
					deviceInfo.setDeviceMac(deviceInfoData.getDeviceMac());
					deviceInfo.setDeviceMobileNumber(deviceInfoData.getDeviceMobileNumber());
					deviceInfo.setDeviceModel(deviceInfoData.getDeviceModel());
					deviceInfo.setDeviceName(deviceInfoData.getDeviceName());
					deviceInfo.setDeviceOsVersion(deviceInfoData.getDeviceOsVersion());
					deviceInfo.setDeviceServiceProvider(deviceInfoData.getDeviceServiceProvider());
					deviceInfo.setDeviceTotalMemory(deviceInfoData.getDeviceTotalMemory());
					deviceInfo.setGpsStatus(deviceInfoData.getGpsStatus() == true ? 1: 0);
					deviceInfo.setPushToken(deviceInfoData.getPushToken());
					deviceInfo.setSimCard(deviceInfoData.getSimCard());
					deviceInfo.setCreatedTimestamp(new Date());
					deviceInfo.setUniqueKey(uniqueKey);
					deviceInfo.setUserId(deviceInfoData.getUserId());
					deviceInfo.setUpdatedIpAddress(ipAddress);
					deviceInfo.setUpdatedTimestamp(new Date());
					manageTransaction.persist(deviceInfo);
					manageTransaction.commit();
					deviceInfoData.setId(deviceInfo.getDeviceId());
					deviceRegistrationLogger.info(" Device Id"+deviceInfo.getDeviceId()+" Active Flag "+deviceInfo.getActiveFlag());
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,deviceInfoData.getId()));
					resultMessage=Constants.REQUEST_COMPLETED;
				   }
			}
			}catch (Exception e) {
				e.printStackTrace();
				deviceRegistrationLogger.error(" EXCEPTION IN SERVER",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,0));
				resultMessage=" EXCEPTION IN SERVER"+e;
			} finally {
				if(manageTransaction!=null)
				manageTransaction.close();
			}
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA,0));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
		}
		//System.out.println("Device Registration Result:" + result);
		 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Device Registration", null,deviceInfoData.getUserId(),null,ipadd,resultMessage,null);
		 deviceRegistrationLogger.info("auditlogStatus "+auditlogStatus);
		deviceRegistrationLogger.info(" Device Registration Result:" + result);
		deviceRegistrationLogger.info("**************END*******************");
		response.getWriter().write(result);
	}

}
