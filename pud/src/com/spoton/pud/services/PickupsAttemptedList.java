package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupRescheduleModal;

/**
 * Servlet implementation class PickupsAttemptedList
 */
@WebServlet("/pickupsAttemptedList")
public class PickupsAttemptedList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pickupsAttemptedLogger = Logger.getLogger("PickupsAttemptedList");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PickupsAttemptedList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pickupsAttemptedLogger.info("START");
		response.setContentType("application/json");
		
		Gson gson = new GsonBuilder().serializeNulls().create();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ManageTransaction mt=null;String result = "";
		String userId =request.getParameter("userId");
		pickupsAttemptedLogger.info("userId "+userId);
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		 String resultMessage="";
		String ipadd = request.getRemoteAddr();
		pickupsAttemptedLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		try{
		if(CommonTasks.check(userId)){
			
			 mt=new ManageTransaction();
			 Map<String,String> map=new HashMap<String, String>();
			 String q="select reason_code,reason_desc from pickup_cancellation_reason_master";
			 Query qry=mt.createNativeQuery(q);
			 List<Object[]> reasonList=qry.getResultList();
			 if(reasonList!=null&&reasonList.size()>0){
				 for(Object[] o:reasonList){
					 map.put((String) o[0], (String) o[1]);
				 }
			 }
		//	 pickupsAttemptedLogger.info("map "+map);
			/* String query="select  p.con_number,p.status_id,p.remarks,p.ref_no,p.reschedule_datetime,p.reason_id,p.customer_code,p.customer_name,p.created_timestamp,p.product_type FROM pud.pickup_reschedule p,pud.pickup_reschedule_update_status s "
			 		      +"where date(p.created_timestamp)=curdate() and p.app_mobile_user_name=?1 and p.pickup_schedule_id=s.pickup_schedule_id and s.transaction_result=?2";*/
			 String query="select  p.con_number,p.status_id,p.remarks,p.ref_no,p.reschedule_datetime,p.reason_id,p.customer_code,p.customer_name,p.created_timestamp,p.product_type FROM pud.pickup_reschedule p "
		 		      +"where date(p.created_timestamp)=curdate() and p.app_mobile_user_name=?1  and p.transaction_result=?2";
		     Query nativeqry=mt.createNativeQuery(query);
		     nativeqry.setParameter(1, userId);nativeqry.setParameter(2, "true");
		     List<Object[]> list=nativeqry.getResultList();
		     List<PickupRescheduleModal> pickupsAttempted=new ArrayList<PickupRescheduleModal>();
		     if(list!=null&&list.size()>0){
		    	 pickupsAttemptedLogger.info("list.size "+list.size());
		     for(Object[] o:list){
		    	 PickupRescheduleModal pr=new PickupRescheduleModal();
		    	 pr.setConNumbers((String) o[0]);
		    	 pr.setStatusId((int) o[1]);
		    	 pr.setRemarks((String) o[2]);
		    	 pr.setRefNo((String) o[3]);
		    	 if(o[4]!=null)
		    	 pr.setRescheduleDateTime(format.format((Date) o[4]));
		    	 pr.setReasonId(map.get((String) o[5]));
		    	 pr.setCustomerCode((String) o[6]);
		    	 pr.setCustomerName((String) o[7]);
		    	 pr.setProductType(o[9]!=null?(String)o[9]:"");
		    	 if(o[8]!=null)
		    		 pr.setUpdatedTimestamp(format.format((Date) o[8]));
		    	 pickupsAttempted.add(pr);
		     
		     }
		     }
		     result = gson.toJson(new GeneralResponse(Constants.TRUE,
						Constants.REQUEST_COMPLETED, pickupsAttempted));
		     resultMessage=Constants.REQUEST_COMPLETED;
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
		}
		}catch(Exception e){
			e.printStackTrace();
			pickupsAttemptedLogger.info("EXCEPTION_IN_SERVER "+e);
			 resultMessage="EXCEPTION_IN_SERVER "+e;
			result = gson.toJson(new GeneralResponse(
					Errors.status, 
					Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
					null));
		}finally{
			mt.close();
		}
			// System.out.println(" PickupsAttemptedList result "+result);
			 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Viewed Attempted Pickups", null,userId,null,ipadd,resultMessage,null);
			 pickupsAttemptedLogger.info("auditlogStatus "+auditlogStatus);
			 pickupsAttemptedLogger.info(" PickupsAttemptedList result "+result);
			response.getWriter().write(result);
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
