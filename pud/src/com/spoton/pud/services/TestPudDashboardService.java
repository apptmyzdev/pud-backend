package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PudDashboardModal;

/**
 * Servlet implementation class TestPudDashboardService
 */
@WebServlet("/testPudDashboardService")
public class TestPudDashboardService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pudDashboardLogger = Logger.getLogger("TestPudDashboardService");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestPudDashboardService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @SuppressWarnings("deprecation")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;String result = "";
		String userId =request.getParameter("userId");
		pudDashboardLogger.info("userId "+userId);
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String resultMessage="";String ipadd = request.getRemoteAddr();
		if(CommonTasks.check(userId)){
			 mt=new ManageTransaction();
			 /*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
				if(checkapkVersion){*/
		try{
			Calendar currentDate=Calendar.getInstance();
			currentDate.setTime(new Date());
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 0);
			
			Calendar midnightCurrentDate=Calendar.getInstance();
			midnightCurrentDate.setTime(new Date());
			midnightCurrentDate.set(Calendar.HOUR_OF_DAY, 06);
			midnightCurrentDate.set(Calendar.MINUTE, 00);
			midnightCurrentDate.set(Calendar.SECOND, 00);
			midnightCurrentDate.set(Calendar.MILLISECOND, 0);
			
			/*Calendar threeDaysBackDate=Calendar.getInstance();
			threeDaysBackDate.add(Calendar.DATE, -3);
			threeDaysBackDate.set(Calendar.HOUR_OF_DAY, 0);
			threeDaysBackDate.set(Calendar.MINUTE, 0);
			threeDaysBackDate.set(Calendar.SECOND, 0);
			threeDaysBackDate.set(Calendar.MILLISECOND, 0);
			
			Calendar twoDaysBackDate=Calendar.getInstance();
			twoDaysBackDate.add(Calendar.DATE, -2);
			twoDaysBackDate.set(Calendar.HOUR_OF_DAY, 0);
			twoDaysBackDate.set(Calendar.MINUTE, 0);
			twoDaysBackDate.set(Calendar.SECOND, 0);
			twoDaysBackDate.set(Calendar.MILLISECOND, 0);
			 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");*/
			 Date todaysDate=new Date();
			
			 Calendar yesterday=Calendar.getInstance();
				yesterday.add(Calendar.DATE, -1);
				yesterday.set(Calendar.HOUR_OF_DAY, 00);
				yesterday.set(Calendar.MINUTE, 00);
				yesterday.set(Calendar.SECOND, 00);
				yesterday.set(Calendar.MILLISECOND, 0);
			 

				Calendar todaysDatetillSix=Calendar.getInstance();
				Calendar tomorrowDate=Calendar.getInstance();
				if(new Date().getHours()>6) {
				
					todaysDatetillSix.setTime(new Date());
					todaysDatetillSix.set(Calendar.HOUR_OF_DAY, 06);
					todaysDatetillSix.set(Calendar.MINUTE, 00);
					todaysDatetillSix.set(Calendar.SECOND, 00);
					todaysDatetillSix.set(Calendar.MILLISECOND, 0);
					
					tomorrowDate.add(Calendar.DATE, +1);
					tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
					tomorrowDate.set(Calendar.MINUTE, 00);
					tomorrowDate.set(Calendar.SECOND, 00);
					tomorrowDate.set(Calendar.MILLISECOND, 0);
				}else {
					todaysDatetillSix.add(Calendar.DATE, -1);
					todaysDatetillSix.set(Calendar.HOUR_OF_DAY, 06);
					todaysDatetillSix.set(Calendar.MINUTE, 00);
					todaysDatetillSix.set(Calendar.SECOND, 00);
					todaysDatetillSix.set(Calendar.MILLISECOND, 0);
					
					tomorrowDate.setTime(new Date());
					tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
					tomorrowDate.set(Calendar.MINUTE, 00);
					tomorrowDate.set(Calendar.SECOND, 00);
					tomorrowDate.set(Calendar.MILLISECOND, 0);
				}
				
				Long totalPickups=(long) 0;Long pickupsPending=(long) 0;
			 Long deliveriesPending=(long) 0; Long pickupsAttempted=(long) 0;
			 Long pickupsSucceeded=(long) 0;
			
			 
			String assignedPickupsQuery="Select p.updatedAttemptedFlag,p.updatedAttemptedTime from PudData p where p.userId=?1 and p.validTill>?2 ";
			TypedQuery<Object[]> tq1=mt.createQuery(Object[].class, assignedPickupsQuery);
			tq1.setParameter(1, userId).setParameter(2, todaysDatetillSix.getTime());
			 List<Object[]> assignedPickupsList=tq1.getResultList();
			 pudDashboardLogger.info("assignedPickupsList size "+assignedPickupsList.size());
			 if(assignedPickupsList!=null&&assignedPickupsList.size()>0) {
				 for(Object[] s:assignedPickupsList) {
					 if(s[0]!=null&&s[1]!=null) {
						 /*System.out.println(" not null case");
						 System.out.println((((Date)s[1]).before(tomorrowDate.getTime())));
						 System.out.println((((Date)s[1]).after(todaysDatetillSix.getTime())));*/
					 if((((Date)s[1]).before(tomorrowDate.getTime()))&&(((Date)s[1]).after(todaysDatetillSix.getTime()))) {
					// if(s[0]!=null&&s[1]!=null&&(((Date)s[1]).getDate()==todaysDatetillSix.getTime().getDate()||((Date)s[1]).getDate()==tomorrowDate.getTime().getDate()))
					 if(s[0]!=null&&(((String)s[0]).equalsIgnoreCase("U"))) {
						 pickupsSucceeded++;
					 }else  if(s[0]!=null&&(((String)s[0]).equalsIgnoreCase("A"))) {
						 pickupsAttempted++;
					 }
					 }
					 }else {
						 pickupsPending++;
					 }
					 }//end of for
			 }
			 
			 String registeredPickupsQuery="Select p.updatedAttemptedFlag from PickupRegistration p where p.mobileUserId=?1 and p.validTill>?2 and  p.validTill<=?3 and  p.transactionResult=1";
				TypedQuery<String> tq2=mt.createQuery(String.class, registeredPickupsQuery);
				tq2.setParameter(1, userId).setParameter(2, todaysDatetillSix.getTime()).setParameter(3, tomorrowDate.getTime());
				List<String> registeredPickupsList=tq2.getResultList();
				 pudDashboardLogger.info("registeredPickupsList size "+registeredPickupsList.size());
				if(registeredPickupsList!=null&&registeredPickupsList.size()>0) {
					 for(String s:registeredPickupsList) {
						 if(s!=null&&s.equalsIgnoreCase("U")) {
							 pickupsSucceeded++;
						 }else if(s!=null&&s.equalsIgnoreCase("A")) {
							 pickupsAttempted++;
						 }else {
							 pickupsPending++;
						 }
					 }
				 }
				 /*pickupsAttempted=assignedPickupsAttemptedCount+registeredPickupsAttemptedCount;*/
				 
				 
			 
			 /*-------------------------------Pickups Calculation----------------------------------
			  * 
			  * String updatedPickupsQuery="Select distinct(p.pickupScheduleId) from ConDetail p where p.userName=?1 and "
			 		         + "p.pickupDate>?3 and p.pickupDate<=?4 and p.pickupType!=3";
			 TypedQuery<Integer> tq= mt.createQuery(Integer.class, updatedPickupsQuery);
			 tq.setParameter(1, userId);
			 tq.setParameter(3, twoDaysBackDate.getTime());tq.setParameter(4, currentDate.getTime());
			 List<Integer> updatedPickupsList=tq.getResultList();

			// List<Integer> updatedCashPickupsList=new ArrayList<Integer>();
			 String cashPickupsQuery="Select distinct(p.pickupScheduleId) from ConDetail p,CashBookingResponse r where p.userName=?1 and "
	 		         + "p.conEntryId=r.conEntryId and r.flag=?5 and p.pickupDate>?3  and p.pickupDate<=?4 and p.pickupType=3 and r.createdTimestamp>?3 and r.createdTimestamp<=?4";
					
	         TypedQuery<Integer> q2= mt.createQuery(Integer.class, cashPickupsQuery);
	         q2.setParameter(1, userId).setParameter(5, "Y");
	         q2.setParameter(3, twoDaysBackDate.getTime());q2.setParameter(4, currentDate.getTime());
	         List<Integer> updatedCashPickupsList=q2.getResultList();
			 
			 
			 String attemptedPickupsQuery="select distinct(r.pickupScheduleId) from PickupRescheduleUpdateStatus r,PickupReschedule pr where pr.appMobileUserName=?1 and r.transactionResult=?2 "
		 		        + "and r.timestamp>?3 and r.timestamp<=?4 and pr.pickupScheduleId=r.pickupScheduleId";
			 TypedQuery<Integer> tq1=mt.createQuery(Integer.class, attemptedPickupsQuery);
			 tq1.setParameter(1, userId);tq1.setParameter(2, "true");
			 tq1.setParameter(3, twoDaysBackDate.getTime());tq1.setParameter(4, currentDate.getTime());
			 List<Integer> attemptedPickupsList=tq1.getResultList();
			 
			 
			 //To get pickups from pickup registration table
			 String query="select pickup_schedule_id FROM pud.pickup_registration where mobile_user_id=?1 and transaction_result=1 and date(pickup_date)=?2";
			 Query nativeQuery=mt.createNativeQuery(query).setParameter(1,userId);
			 if(todaysDate.getHours()<=6){
					 nativeQuery.setParameter(2, format.format(yesterday.getTime()));
			 }else 
			 {
				 nativeQuery.setParameter(2, format.format(new Date()));
			 }
			 List<Integer> pickupsRegistered = nativeQuery.getResultList();
			for(Integer i:pickupsRegistered){
				if(!updatedPickupsList.contains(i)&&!attemptedPickupsList.contains(i)&&!updatedCashPickupsList.contains(i)){
					pudDashboardLogger.info("pickup schedule id of reg "+(int)i);
					pickupsPending++;
				}
			}	
			 
			 
			 //To get pickups from pud data
			 String query2="select pickup_schedule_id,pickup_date,is_oda FROM pud.pud_data where user_id=?1 and pickup_date>=?2 and pickup_date<=?3";
			 Query nativeQuery2=mt.createNativeQuery(query2).setParameter(1,userId).setParameter(2,threeDaysBackDate.getTime()).setParameter(3,currentDate.getTime());
			 
			
			 Long pickupsDownloaded =(long) 0;
			 List<Object[]> list=nativeQuery2.getResultList();//oda is 2 days only but getting 3 days data as after 12am the diff of days will be 3
			 
			
			 for(Object[] o:list){
				
				 int pickupsOda=o[2]!=null?(int) o[2]:0;
				
				 if(pickupsOda==1&&todaysDate.getHours()>6 && o[1]!=null){//oda true and not midnight so allow only 2 days data
						Date startDate = twoDaysBackDate.getTime();Date endDate = (Date)o[1];
						long diff = endDate.getTime() - startDate.getTime();
						long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
						if(diffIndays>=0&&diffIndays<=2){
							
							if(!updatedPickupsList.contains(o[0])&&!attemptedPickupsList.contains(o[0])&&!updatedCashPickupsList.contains(o[0])){
								pudDashboardLogger.info("pickup schedule id of pud oda=1>6"+(int)o[0]);
								pickupsPending++;
							}
							
						}
					}else if(pickupsOda==1&&todaysDate.getHours()<=6 && o[1]!=null){//oda true but after 12 am we need to check for 3 days
						Date startDate = threeDaysBackDate.getTime();Date endDate = (Date)o[1];
					
						long diff = endDate.getTime() - startDate.getTime();
						long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
						if(diffIndays>=0&&diffIndays<3){
						
							if(!updatedPickupsList.contains(o[0])&&!attemptedPickupsList.contains(o[0])){
								pudDashboardLogger.info("pickup schedule id of pud oda=1<6 "+(int)o[0]);
								pickupsPending++;
							}
					}
					 
					 
				 } else if(pickupsOda==0 &&  todaysDate.getHours()>6 && o[1]!=null && ((format.format(todaysDate)).equals(format.format((Date)o[1])))){
					 
					
						if(!updatedPickupsList.contains(o[0])&&!attemptedPickupsList.contains(o[0])){
							pudDashboardLogger.info("pickup schedule id of pud oda=0>6 "+(int)o[0]);
							pickupsPending++;
						}//oda zero but todays data
				 }else if(pickupsOda==0 && todaysDate.getHours()<=6 && o[1]!=null &&((format.format(yesterday.getTime())).equals(format.format((Date)o[1])))){ //after 12am todaysDate changes //till 6am following will work 
					
						if(!updatedPickupsList.contains(o[0])&&!attemptedPickupsList.contains(o[0])){
							pudDashboardLogger.info("pickup schedule id of pud oda=0<6 "+(int)o[0]);
							pickupsPending++;
						}
				 }
				 }
			
			 pudDashboardLogger.info("pickupsDownloaded "+pickupsDownloaded);
			 totalPickups=totalPickups+pickupsDownloaded;
			 pudDashboardLogger.info("totalPickups "+totalPickups);
			
			 
			 //To get pickups succeeded
			
			 String query3=" select COUNT(distinct(p.pickupScheduleId)) from ConDetail p where p.userName=?1 and "
			 		         + "p.pickupDate>?3 and p.pickupDate<=?4 and p.pickupType!=4 and p.pickupType!=3 and p.pickupType!=1";
			
			 TypedQuery<Long> typedquery=mt.createQuery(Long.class, query3);
			 typedquery.setParameter(1, userId);//typedquery.setParameter(2, "Success");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
			 typedquery.setParameter(3, midnightCurrentDate.getTime());typedquery.setParameter(4, currentDate.getTime());
			 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
				 typedquery.setParameter(3, yesterday.getTime());typedquery.setParameter(4, midnightCurrentDate.getTime());
			 }
			 Long pickupsSucceeded=typedquery.getSingleResult();
		
			 pudDashboardLogger.info("pickupsSucceeded "+pickupsSucceeded);
			 
			 //to get cash booked cons
			 String cashBookConsQuery="select COUNT(distinct(p.pickupScheduleId)) from ConDetail p,CashBookingResponse r where p.userName=?1 and p.conEntryId=r.conEntryId "
			 		+ "and p.erpUpdated=1 and p.pickupType=3 and r.flag=?2 "
			 		+ "and p.pickupDate>?3 and p.pickupDate<=?4 and p.pickupScheduleId!=0 and r.createdTimestamp>?3 and r.createdTimestamp<=?4";
			 TypedQuery<Long> typedquery1=mt.createQuery(Long.class, cashBookConsQuery);
			 typedquery1.setParameter(1, userId);typedquery1.setParameter(2, "Y");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
				 typedquery1.setParameter(3, midnightCurrentDate.getTime());typedquery1.setParameter(4, currentDate.getTime());
			 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
				 typedquery1.setParameter(3, yesterday.getTime());typedquery1.setParameter(4, midnightCurrentDate.getTime());
			 }
			 Long cashPickupsSucceeded=typedquery1.getSingleResult();
			
			 pudDashboardLogger.info("cashPickupsSucceeded "+cashPickupsSucceeded);
			 
			//to get direct cash booked cons
			 String directCashBookConsQuery="select COUNT(p.pickupScheduleId) from ConDetail p,CashBookingResponse r where p.userName=?1 and p.conEntryId=r.conEntryId "
			 		+ "and p.erpUpdated=1 and p.pickupType=3 and r.flag=?2 "
			 		+ "and p.pickupDate>?3 and p.pickupDate<=?4 and p.pickupScheduleId=0 and r.createdTimestamp>?3 and r.createdTimestamp<=?4";
			 TypedQuery<Long> tq2=mt.createQuery(Long.class, directCashBookConsQuery);
			 tq2.setParameter(1, userId);tq2.setParameter(2, "Y");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
				 tq2.setParameter(3, midnightCurrentDate.getTime());tq2.setParameter(4, currentDate.getTime());
			 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
				 tq2.setParameter(3, yesterday.getTime());tq2.setParameter(4, midnightCurrentDate.getTime());
			 }
			 Long directCashPickupsSucceeded=tq2.getSingleResult();
			
			 pudDashboardLogger.info("directCashPickupsSucceeded "+directCashPickupsSucceeded);
			 
			 //To get pickups Attempted
			
			 String query4="select COUNT(distinct(r.pickupScheduleId)) from PickupRescheduleUpdateStatus r,PickupReschedule pr where pr.appMobileUserName=?1 and r.transactionResult=?2 "
		 		        + "and r.timestamp>?3 and r.timestamp<=?4 and pr.pickupScheduleId=r.pickupScheduleId";
			 TypedQuery<Long> typedquery2=mt.createQuery(Long.class, query4);
			 typedquery2.setParameter(1, userId);typedquery2.setParameter(2, "true");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
				 typedquery2.setParameter(3, midnightCurrentDate.getTime());typedquery2.setParameter(4, currentDate.getTime());
				 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
					 typedquery2.setParameter(3, yesterday.getTime());typedquery2.setParameter(4, midnightCurrentDate.getTime());
				 }
			 Long pickupsAttempted=typedquery2.getSingleResult();
			pudDashboardLogger.info("pickupsAttempted "+pickupsAttempted);*/
			 
			 
			 
			 
			 
			 //To get deliveries succeeded
			 /*String query5=" select COUNT((Distinct)d) from DeliveryUpdation d where d.fieldEmployeeName=?1 and d.status=?2 and FUNC('DAY', d.transactionDate)=?3 and FUNC('MONTH', d.transactionDate)=?4 and FUNC('YEAR',d.transactionDate)=?5";*/
			/* String query5=" select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.field_employee_name=?1 and d.status=?2 "
			 		        + "and d.created_time > curdate()";*/
			 String query5=" select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.field_employee_name=?1 and d.status=?2 "
		 		        + "and d.created_time > ?3 and  d.created_time <= ?4";
			 //TypedQuery<Long> typedquery3=mt.createQuery(Long.class, query5);
			 Query nativeqry=mt.createNativeQuery(query5);
			 nativeqry.setParameter(1, userId);nativeqry.setParameter(2, "DELIVERED");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
				 nativeqry.setParameter(3, midnightCurrentDate.getTime());nativeqry.setParameter(4, currentDate.getTime());
				 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
					 nativeqry.setParameter(3, yesterday.getTime());nativeqry.setParameter(4, midnightCurrentDate.getTime());
				 }
			 
			// typedquery3.setParameter(3, c.getTime().getDate());typedquery3.setParameter(4, c.getTime().getMonth()+1);typedquery3.setParameter(5, c.get(Calendar.YEAR));
			 Long deliveriesSucceeded=(Long) nativeqry.getSingleResult();
			 //System.out.println("deliveriesSucceeded "+deliveriesSucceeded);
			 pudDashboardLogger.info("deliveriesSucceeded "+deliveriesSucceeded);
			 
			 //To get deliveries Attempted
			 //String query6=" select COUNT(d) from DeliveryUpdation d where d.fieldEmployeeName=?1 and d.status!=?2 and FUNC('DAY', d.transactionDate)=?3 and FUNC('MONTH', d.transactionDate)=?4 and FUNC('YEAR',d.transactionDate)=?5";
			/* String query6=" select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.field_employee_name=?1 and d.status!=?2 "
		 		        + "and d.created_time > curdate()";*/
			 String query6=" select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.field_employee_name=?1 and d.status!=?2 "
					 + "and d.created_time > ?3 and  d.created_time <= ?4";
			 Query nativeqry2=mt.createNativeQuery(query6);
			 nativeqry2.setParameter(1, userId);nativeqry2.setParameter(2, "DELIVERED");
			 if(todaysDate.getHours()>6){ //after 6am to night 12 am
				 nativeqry2.setParameter(3, midnightCurrentDate.getTime());nativeqry2.setParameter(4, currentDate.getTime());
				 }else if(todaysDate.getHours()<=6){ //yesterday to next day 6am
					 nativeqry2.setParameter(3, yesterday.getTime());nativeqry2.setParameter(4, midnightCurrentDate.getTime());
				 }
			 /*TypedQuery<Long> typedquery4=mt.createQuery(Long.class, query6);
			 typedquery4.setParameter(1, userId);typedquery4.setParameter(2, "DELIVERED");
			 typedquery4.setParameter(3, c.getTime().getDate());typedquery4.setParameter(4, c.getTime().getMonth()+1);typedquery4.setParameter(5, c.get(Calendar.YEAR));*/
			 Long deliveriesAttempted=(Long) nativeqry2.getSingleResult();
			// System.out.println("deliveriesAttempted "+deliveriesAttempted);
			 pudDashboardLogger.info("deliveriesAttempted "+deliveriesAttempted);
			 
			 
			/* String updationQuery="Select distinct(concat(d.awb_no,d.pdc_number)) from delivery_updation d where d.field_employee_name=?1 and "
				 		+ "d.created_time > ?3 and  d.created_time <= ?4";
				 Query q=mt.createNativeQuery(updationQuery);
				 q.setParameter(1, userId); q.setParameter(3, twoDaysBackDate.getTime());q.setParameter(4, currentDate.getTime());
				 List<String> updatedCons=q.getResultList();*/
			 
			 
			 //To get total deliveries
			/* String query7 = "select C.con, C.pdc, C.ref1, C.ref2, C.pcs_count, C.con_wt, C.consignee_name, C.consignee_company, C.consignee_address, "
						+ " C.consignee_city, C.consignee_pincode, C.consignee_mobile, C.consignee_emailid, C.consignor_city, C.consignor_mobile, C.consignor_email "
						+ " from agent_pdc A, delivery_con_details C where A.pdc = C.pdc and A.pulled_time > curdate() and C.created_date > curdate() and A.agent_id = ?1";*/
			 
			 //String query7 ="select (distinct(concat(C.con,C.pdc))) from agent_pdc A, delivery_con_details C where A.pdc = C.pdc and A.created_timestamp > curDate() and C.created_date > curdate() and A.agent_id = ?1";
			 
			//String query7 ="select (concat(C.con,C.pdc)),C.created_date,C.is_oda from agent_pdc A, delivery_con_details C where A.pdc = C.pdc and A.created_timestamp >= Date(NOW() - INTERVAL 2 DAY) and A.created_timestamp <Date(NOW()+ INTERVAL 1 DAY)  and C.created_date  >=Date( NOW() - INTERVAL 2 DAY) and C.created_date  < Date(NOW()+ INTERVAL 1 DAY) and A.agent_id = ?1";
			/* String query7 ="select DISTINCT(concat(C.con,C.pdc)),C.createdDate,C.isOda from AgentPdc A, DeliveryConDetail C where A.pdc = C.pdc "
			 		+ "and A.createdTimestamp>=?2 and A.createdTimestamp<=?3  and C.createdDate>=?2 and C.createdDate<=?3 and A.agentId =?1";
			 TypedQuery<Object[]> qry = mt.createQuery(Object[].class, query7);*/
			 /*String query7 ="SELECT distinct(concat(d.con,d.pdc)),d.created_date,d.is_oda FROM pud.delivery_con_details d,pud.agent_pdc a "
			 		+ "where a.pdc=d.pdc and a.agent_id=?1 and d.created_date>=?2 and d.created_date<=?3 and a.created_timestamp>=?2 and a.created_timestamp<=?3";*/
				 String query7 ="SELECT count(distinct(concat(d.con,d.pdc))) FROM pud.delivery_con_details d,pud.agent_pdc a "
					 		+ "where a.pdc=d.pdc and a.agent_id=?1 and d.valid_till>?4 and d.updated_flag=0";
			 Query qry=mt.createNativeQuery(query7);
			 qry.setParameter(1, userId);
			 qry.setParameter(4, todaysDatetillSix.getTime());
			 //qry.setParameter(2,threeDaysBackDate.getTime());qry.setParameter(3,currentDate.getTime());
//			 List<Object[]> list2=qry.getResultList();
			// System.out.println("list2 "+list2.size());
			 deliveriesPending=(Long) qry.getSingleResult();
//			 Long totalDeliveries=(long) 0;
		/*	 if(list2!=null&&list2.size()>0){
				 List<String> conPdcList=new ArrayList<String>();
			 for(Object[] o2:list2){
				 String key=(String) o2[0];
				// pudDashboardLogger.info("con+pdc "+o2[0]+" created date "+o2[1]+" oda "+o2[2]);
				 if(!conPdcList.contains(key)){
				 
				 int deliveriesOda=o2[2]!=null?(int) o2[2]:0;
				 if(deliveriesOda==1&&todaysDate.getHours()>6&&o2[1]!=null){ //oda true and not midnight so allow only 2 days data
					 Date startDate = twoDaysBackDate.getTime();Date endDate = (Date) o2[1];
						long diff = endDate.getTime() - startDate.getTime();
						long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
						if(diffIndays>=0&&diffIndays<=2){
							if(!updatedCons.contains(o2[0])){
								deliveriesPending++;}
						}
				 }else if(deliveriesOda == 1&&todaysDate.getHours()<=6&&o2[1]!=null){//oda true but after 12 am we need to check for 3 days
					 Date startDate = threeDaysBackDate.getTime();Date endDate= (Date) o2[1];
							long diff = endDate.getTime() - startDate.getTime();
							long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
							if(diffIndays>=0&&diffIndays<3){
								if(!updatedCons.contains(o2[0])){
									deliveriesPending++;}
								}
							
					}else if(deliveriesOda==0 && todaysDate.getHours()>6 && o2[1]!=null&& ((format.format(new Date())).equals(format.format((Date)o2[1])))){
						if(!updatedCons.contains(o2[0])){
							deliveriesPending++;} 
						//totalDeliveries++;
				 }else if (deliveriesOda == 0 && todaysDate.getHours()<=6&& ((format.format(yesterday.getTime())).equals(format.format(((Date) o2[1]))))){
					 if(!updatedCons.contains(o2[0])){
						 deliveriesPending++;} 
				 }
				 conPdcList.add(key);
				 }
			 }
			 }*/
			 //System.out.println("totalDeliveries "+totalDeliveries);
			 //pudDashboardLogger.info("totalDeliveries "+totalDeliveries);
			/* List<Object[]> cons = qry.getResultList();
			 if(cons!=null&&cons.size()>0){
				 totalDeliveries=(long)cons.size();
				
				 
			 }*/
			 
			/* if(totalPickups!=0)
			 pickupsPending=totalPickups-pickupsSucceeded-pickupsAttempted;*/
			/* if(totalDeliveries!=0)
			 deliveriesPending=totalDeliveries-deliveriesSucceeded-deliveriesAttempted;*/
			 
			 pudDashboardLogger.info("pickupsPending "+pickupsPending);
			 pudDashboardLogger.info("deliveriesPending "+deliveriesPending);
			 
			 
			 PudDashboardModal data=new PudDashboardModal();
			 data.setPickupsPending(pickupsPending);
			 data.setPickupsSucceeded(pickupsSucceeded);
			// data.setPickupsSucceeded(pickupsSucceeded+cashPickupsSucceeded+directCashPickupsSucceeded);
			 data.setPickupsAttempted(pickupsAttempted);
			 data.setDeliveriesPending(deliveriesPending);
		     data.setDeliveriesSucceeded(deliveriesSucceeded);
		     data.setDeliveriesAttempted(deliveriesAttempted);
		     result = gson.toJson(new GeneralResponse(Constants.TRUE,
						Constants.REQUEST_COMPLETED, data));
		     pudDashboardLogger.info("result "+result);
		     resultMessage=Constants.REQUEST_COMPLETED;
		}catch(Exception e){
					e.printStackTrace();
					 pudDashboardLogger.info("EXCEPTION_IN_SERVER "+e);
					 resultMessage="EXCEPTION_IN_SERVER "+e;
					result = gson.toJson(new GeneralResponse(
							Errors.status, 
							Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
							null));
				}finally{
					mt.close();
				}
				/*}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							Constants.UPDATE_TO_LATEST_VERSION, null));
				}*/
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
		}
	
		//System.out.println("PudDashboardService result"+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Viewed PUD Dashboard", null,userId,null,ipadd,resultMessage,null);
		pudDashboardLogger.info("auditlogStatus "+auditlogStatus);
		pudDashboardLogger.info("PudDashboardService result"+result);
		response.getWriter().write(result);
	
	
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
