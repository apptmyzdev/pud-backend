package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DelUndelCodes;
import com.spoton.pud.data.Relationships;
import com.spoton.pud.data.RelationshipsModel;

/**
 * Servlet implementation class GetRelationships
 */
@WebServlet("/getrelationships")
public class GetRelationships extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Logger getRelationships = Logger.getLogger("GetRelationships");  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetRelationships() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String imei = request.getHeader("imei");
		String deviceId = request.getHeader("device_id");
		String version = request.getHeader("app_version");
		String agent = request.getParameter("agent");
		String ip = request.getRemoteAddr();
		getRelationships.info("Agent: " + agent + " ,IMEI : " + imei + " , Version: " + version + " , IP : " + ip);
		
		ManageTransaction mt = new ManageTransaction();
		List<Relationships> codes = new ArrayList<Relationships>();
		RelationshipsModel result = new RelationshipsModel();
		
		String query = "Select code, description from relationships order by sort_order";
		Query nqry = mt.createNativeQuery(query);
		try{
			List<Object[]> results =  nqry.getResultList();
			for(Object[] o:results ){
				Relationships code = new Relationships();
				code.setCode((String) o[0]);
				code.setDescription((String) o[1]);
				codes.add(code);
			}
			result.setResult(true);
			result.setCodes(codes);
		}catch(Exception e){
			e.printStackTrace();
			result.setResult(false);
			result.setErrMsg(e.getMessage());
		}finally{
			mt.close();
		}
		Gson gson = new GsonBuilder().serializeNulls().create();
		String output = gson.toJson(result);
		getRelationships.info("Output: " + output);
		response.getWriter().write(output);
	}

//	/**
//	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
//	 */
//	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//	}

}
