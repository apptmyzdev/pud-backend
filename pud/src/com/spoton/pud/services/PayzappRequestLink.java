package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PayzappRequestLinkModal;
import com.spoton.pud.data.PayzappRequestLinkResponseModal;
import com.spoton.pud.jpa.PayzappRequestLinkInput;
import com.spoton.pud.jpa.PayzappRequestLinkResponse;

/**
 * Servlet implementation class PayzappRequestLink
 */
@WebServlet("/payzappRequestLink")
public class PayzappRequestLink extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PayzappRequestLink");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayzappRequestLink() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("payzappRequestLink");
		String datasent="";ManageTransaction mt=null;
		StringBuilder stringBuilder = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String agentId= "";
		String ip = request.getRemoteAddr();String resultMessage="";
		try{
			
		
			datasent=request.getReader().readLine();
			logger.info("Datasent From mobile : " +datasent);
			PayzappRequestLinkModal pm=gson.fromJson(datasent, PayzappRequestLinkModal.class);
			if(pm!=null){
				 mt=new ManageTransaction();
				 PayzappRequestLinkInput input=new PayzappRequestLinkInput();
				 input.setCreatedTimestamp(new Date());
				 input.setCustomerCode(pm.getCustomerCode());
				 input.setFlag(pm.getFlag());
				 input.setInvoiceNumber(pm.getInvoiceNumber());
				 input.setMobileNo(pm.getMobileNumber());
				 input.setPtmsEmail(pm.getPtmsEmail());
				 input.setPtmsPtnm(pm.getPtmsPtnm());
				 input.setPudAgentMobileNumber(pm.getPudAgentMobileNumber());
				 input.setRemarks(pm.getRemarks());
				 input.setTotalInvoiceAmount(pm.getTotalInvoiceAmount());
				 mt.persist(input);
				 mt.commit();
				 agentId=pm.getAgentId();
					    JSONObject jsonObj = new JSONObject();
						jsonObj.put("CustomerCode", pm.getCustomerCode());
						jsonObj.put("PTMSPTNM", pm.getPtmsPtnm());
						jsonObj.put("PTMSEMAIL", pm.getPtmsEmail());
						jsonObj.put("MOBILENO", pm.getMobileNumber());
						jsonObj.put("InvoiceNumber", pm.getInvoiceNumber());
						jsonObj.put("TotalInvoiceAmount", pm.getTotalInvoiceAmount());
						jsonObj.put("Remarks", pm.getRemarks());
						jsonObj.put("PUDAgentMobNo", pm.getPudAgentMobileNumber());
						jsonObj.put("Flag", pm.getFlag());
						logger.info("Spoton url : " +urlString);
						logger.info("Datasent to Spoton : " +jsonObj.toJSONString());
						
						obj = new URL(urlString);
	    				con = (HttpURLConnection) obj.openConnection();
	    				con.setConnectTimeout(5000);
	    				//con.setReadTimeout(10000);
	    				con.setRequestMethod("POST");
	    				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
	    				con.setRequestProperty("Accept", "application/json");
	    				con.setDoOutput(true);
	    				
	    				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
	    				  streamWriter.write(jsonObj.toJSONString());
	    				  streamWriter.flush();
	    				  logger.info("Response Code: " +con.getResponseCode());
	    				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
	    					 
	    		            	stringBuilder=new StringBuilder();
	    		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	    		                BufferedReader bufferedReader = new BufferedReader(streamReader);
	    		                String response1 = null;
	    		                while ((response1 = bufferedReader.readLine()) != null) {
	    		                    stringBuilder.append(response1);
	    		                }
	    		                bufferedReader.close();
	    		               // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
	    		                logger.info("Response From Spoton : " +stringBuilder.toString());
	    		                Type responseType = new TypeToken<ArrayList<PayzappRequestLinkResponseModal>>(){}.getType();
	    		    			List<PayzappRequestLinkResponseModal> erpResponse=gson.fromJson(stringBuilder.toString(), responseType);
	    		    			if(erpResponse!=null&&erpResponse.size()>0){
	    		    				for(PayzappRequestLinkResponseModal pr:erpResponse){
	    		    				PayzappRequestLinkResponse p=new PayzappRequestLinkResponse();
	    		    				p.setCustomerCode(pr.getCustomerCode());
	    		    				p.setCustomerName(pr.getCustomerName());
	    		    				p.setErrorMsg(pr.getErrorMsg());
	    		    				p.setErrorStatus(pr.getErrorStatus());
	    		    				p.setInvoiceNumber(pr.getInvoiceNumber());
	    		    				p.setMobileNumber(pr.getMOBILENO());
	    		    				p.setPayzappRequestLinkInputId(input.getId());
	    		    				p.setPtmsEmail(pr.getPTMSEMAIL());
	    		    				p.setResponseRemarks(pr.getResponseRemarks());
	    		    				p.setTimestamp(new Date());
	    		    				p.setTotalInvoiceAmount(pr.getTotalInvoiceAmount());
	    		    				mt.persist(p);
	    		    				mt.commit();
	    		    				if(pr.getErrorStatus().equals("Y")){
	    		    					 result = gson.toJson(new GeneralResponse(Constants.TRUE,"Payment Link Sent to "+input.getMobileNo(), null));
	    		    					 resultMessage=pr.getErrorMsg();
	    		    				}else{
	    		    					result = gson.toJson(new GeneralResponse(Constants.FALSE,pr.getErrorMsg(), null));
	    		    					resultMessage=pr.getErrorMsg();
	    		    				}
	    		    				}
	    		    			}else{
	    		    				 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
	    		    				 resultMessage="Response Failed From ERP";
	    		    			}
	    		               
	    		                
	    				  }else{
	    		            	
		    		            //System.out.println("Response failed");
		    		            logger.info("Response failed from spoton");
		    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		    		            resultMessage="Response Failed From ERP";
		    		            }
		    				
				 
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_INCOMPLETE_DATA, null));
			}
			
		}catch (SocketTimeoutException e){		
			e.printStackTrace();
			logger.info("SocketTimeoutException "+e);
			resultMessage="SocketTimeoutException "+e;
			response.setStatus(503);
			response.sendError(503, "SocketTimeoutException");
		}catch(Exception e){
				
				e.printStackTrace();
				logger.info("EXCEPTION_IN_SERVER "+e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERRORS_EXCEPTION_IN_SERVER,null));
				resultMessage="EXCEPTION_IN_SERVER "+e;
			}finally{
				if(mt!=null){mt.close();}
				
				if(con!=null){con.disconnect();}
			}
		System.out.println("result "+result);
		  logger.info("result "+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"PayzappRequestLink", null,agentId,null,ip,resultMessage,null);
		logger.info("auditlogStatus "+auditlogStatus);
		
		response.getWriter().write(result);
	}

}
