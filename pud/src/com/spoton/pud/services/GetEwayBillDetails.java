package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.EwayBillDashboardErpModal;
import com.spoton.pud.data.EwayBillDashboardModal;
import com.spoton.pud.data.EwayBillNumberInfo;
import com.spoton.pud.data.EwayBillNumberInfoErpModal;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class GetEwayBillDetails
 */
@WebServlet("/getEwayBillDetails")
public class GetEwayBillDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetEwayBillDetails");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEwayBillDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String ewayBillNumber= request.getParameter("ewayBillNumber");
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion+" ewayBillNumber "+ewayBillNumber);
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("getEwayBill");
		StringBuilder stringBuilder = null;
		try{
			/*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			logger.info("checkapkVersion "+checkapkVersion);
			if(checkapkVersion){*/
				if(CommonTasks.check(ewayBillNumber)){
					urlString=urlString+"/"+ewayBillNumber;
					obj = new URL(urlString);
					con = (HttpURLConnection) obj.openConnection();
					con.setRequestMethod("GET");
					con.setConnectTimeout(5000);
					 con.setReadTimeout(60000);
					con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
					logger.info("url called "+urlString);
					 logger.info("ResponseCode "+con.getResponseCode());
					 if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
						   stringBuilder=new StringBuilder();
			                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
			                BufferedReader bufferedReader = new BufferedReader(streamReader);
			                String response1 = null;
			                while ((response1 = bufferedReader.readLine()) != null) {
			                    stringBuilder.append(response1 + "\n");
			                }
			                bufferedReader.close();
			                streamReader.close();
			                logger.info("Response From Spoton : " +stringBuilder.toString());
			                Type responseListType = new TypeToken<ArrayList<EwayBillNumberInfoErpModal>>(){}.getType();
			                List<EwayBillNumberInfoErpModal> responseList=gson.fromJson(stringBuilder.toString(),responseListType);
			                if(responseList!=null&&responseList.size()>0){
			                	List<EwayBillNumberInfo> list=new ArrayList<EwayBillNumberInfo>();
			                	for(EwayBillNumberInfoErpModal e:responseList){
			                		EwayBillNumberInfo ei=new EwayBillNumberInfo();
			                		ei.setDelPincode(e.getDelPincode());
			                		ei.setInvoiceAmount(e.getInvoiceAmount());
			                		ei.setInvoiceDate(e.getInvoiceDate());
			                		ei.setInvoiceNo(e.getInvoiceNo());
			                		ei.setRejectStatus(e.getRejectStatus());
			                		ei.setUserGstin(e.getUserGSTIN());
			                		ei.setValidUpto(e.getValidUpto());
			                		ei.setSenderName(e.getSenderName());
			                		ei.setSenderAddress(e.getSenderAddress());
			                		ei.setSenderCity(e.getSenderCity());
			                		ei.setReceiverName(e.getReceiverName());
			                		ei.setReceiverAddress(e.getReceiverAddress());
			                		ei.setReceiverCity(e.getReceiverCity());
			                		ei.setOriginPincode(e.getOriginPincode());
			                		list.add(ei);
			                	}
			                	
			                	result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, list));
			                }else {
			                	 result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
			                }
					 }else{
			            	
				            
							   logger.info("Response failed from spoton");
				            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
				            
				            }
				
				}else{
				 result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			}
				
				/*}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.UPDATE_TO_LATEST_VERSION, null));
			}	*/
				
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Exception e ",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}finally{
				if(mt!=null)
					mt.close();
				if(con!=null) {
					con.disconnect();
				}
			}
		    
			
			logger.info("result "+result);
			logger.info("**********END OF GET METHOD***********");
			response.getWriter().write(result);

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
