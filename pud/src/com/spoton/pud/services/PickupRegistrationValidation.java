package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.PickupRegistration;

/**
 * Servlet implementation class PickupRegistrationValidation
 */
@WebServlet("/pickupRegistrationValidation")
public class PickupRegistrationValidation extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pickupValidationLogger = Logger.getLogger("PickupRegistrationValidation");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PickupRegistrationValidation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pickupValidationLogger.info("**************START*******************");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String result = "";
	     String userId=request.getParameter("userId");
	     String customerCode=request.getParameter("customerCode");
	     String pickupDate=request.getParameter("pickupDate");
	    
	     String deviceIMEI = request.getHeader("imei");
			String apkVersion= request.getHeader("apkVersion");
			String resultMessage="";
			String ipadd = request.getRemoteAddr();
			pickupValidationLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
	   /* String pickupDateConcat=pickupDate.concat(" 00:00:00");*/
	   
	     String pickupPincode=request.getParameter("pickupPincode");
	     String inputParameters="customerCode "+customerCode+" pickupDate "+pickupDate+" pickupPincode "+pickupPincode;
	     //System.out.println("inputParameters "+inputParameters);
	     pickupValidationLogger.info("userId "+userId+" customerCode "+customerCode+" pickupDate "+pickupDate+" pickupPincode "+pickupPincode);
	     ManageTransaction mt=null;
	    
	    	 if(CommonTasks.check(customerCode,pickupDate,pickupPincode,userId)){
	    		 try{
	    	 mt=new ManageTransaction();
	    	 int pincode=Integer.parseInt(pickupPincode);
	     String query="select * from pickup_registration p where p.Customer_code=?1 and p.pickup_pincode=?2 and date(p.pickup_date)=?3 and p.mobile_user_id=?4";
	      Query nativeQuery=mt.createNativeQuery(query);
	      nativeQuery.setParameter(1, customerCode);nativeQuery.setParameter(2, pincode);
	      nativeQuery.setParameter(3, format.parse(pickupDate)); nativeQuery.setParameter(4,userId);
	      @SuppressWarnings("unchecked")
		List<PickupRegistration> list=nativeQuery.getResultList();
	      if(list!=null&&list.size()!=0){
	    	  pickupValidationLogger.info("list size"+list.size());
	    	  result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Pickup Already Available For this Data", null));
	    	  resultMessage="Pickup Already Available For this Data";
	      }else{
	    	  result = gson.toJson(new GeneralResponse(Constants.TRUE,
						"Verification Succeeded", null));
	    	  resultMessage="Verification Succeeded";
	      }
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 pickupValidationLogger.error("EXCEPTION_IN_SERVER",e);
	    	  resultMessage= Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
	    	 result = gson.toJson(new GeneralResponse(
	    			 Constants.FALSE, 
	    			 Constants.ERRORS_EXCEPTION_IN_SERVER,
						null));
	     }finally{
	    	 if(mt!=null){
	    		 mt.close();
	    	 }
	     }
	     }else{
	    	 result = gson.toJson(new GeneralResponse(
	    			 Constants.FALSE, 
	    			 Constants.ERROR_INCOMPLETE_DATA,
						null));
	    	 resultMessage=Constants.ERROR_INCOMPLETE_DATA;
	     }
	    	 //System.out.println("result "+result);
	    	 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Validating Pickup Registration", null,userId,null,ipadd,resultMessage,inputParameters);
	    	 pickupValidationLogger.info("auditlogStatus "+auditlogStatus);
	    	 pickupValidationLogger.info("PickupRegistrationValidation result"+result);
	    	 pickupValidationLogger.info("**************END*******************");
	response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
