package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PaymentModeModal;
import com.spoton.pud.jpa.PaymentMode;
import com.spoton.pud.jpa.PinCodeMaster;

/**
 * Servlet implementation class GetPaymentModes
 */
@WebServlet("/getPaymentModes")
public class GetPaymentModes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPaymentModes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		String result = "";
		ManageTransaction mt = null;
		List<PaymentModeModal> list=new ArrayList<PaymentModeModal>();
		try{
			mt=new ManageTransaction();
			String qry = "Select pm from PaymentMode pm";
			TypedQuery<PaymentMode> pmQuery = mt.createQuery(PaymentMode.class, qry);
			List<PaymentMode> paymentModes = pmQuery.getResultList();
			if(paymentModes!=null&&paymentModes.size()>0){
				System.out.println("Size " + paymentModes.size());
				PaymentModeModal p1=new PaymentModeModal();
				p1.setId(0);
				p1.setPaymentMode("Please select a payment Mode");
				list.add(p1);
				for(PaymentMode p:paymentModes){
					PaymentModeModal pmm=new PaymentModeModal();
					pmm.setId(p.getId());
					pmm.setPaymentMode(p.getPaymentMode());
					list.add(pmm);
				}
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, list));
				}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
				}
			}catch(Exception e){
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}finally{
				if(mt!=null){mt.close();}
			}
		
		response.getWriter().write(result);
		System.out.println("result "+result);
	}

}
