package com.spoton.pud.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.NotificationModel;
import com.spoton.pud.jpa.ConsStatus;

/**
 * Servlet implementation class SaveConSubscriptionRequest
 */
@WebServlet("/saveConSubscriptionRequest")
public class SaveConSubscriptionRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 Logger logger = Logger.getLogger("SaveConSubscriptionRequest");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveConSubscriptionRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
  		String result = "";
  		Gson gson = new GsonBuilder().serializeNulls().create();
  		ManageTransaction mt=new ManageTransaction();
		try {
		String dataSent = request.getReader().readLine();
 		logger.info("Data sent is "+dataSent);
 		if (CommonTasks.check(dataSent)) {
 			Type type = new TypeToken<ArrayList<NotificationModel>>(){}.getType();
 			List<NotificationModel> data=gson.fromJson(dataSent, type);
 			if(data!=null) {
 				Map<String,Integer> conIdMap=new HashMap<String,Integer>();
 				String conStatus="Select c.conNumber,c.id from ConsStatus c where c.isClosed=0";
 				TypedQuery<Object[]> conStatusQuery=mt.createQuery(Object[].class, conStatus);
				List<Object[]> subscribedConsList=conStatusQuery.getResultList();
				if(subscribedConsList!=null&&subscribedConsList.size()>0) {
					for(Object[] o:subscribedConsList) {
					conIdMap.put((String)o[0] ,(int) o[1]);
				}
				}
					ConsStatus con=null;
 				for(NotificationModel n:data) {
 					if(!conIdMap.containsKey(n.getCon())) {
 					con=new ConsStatus();
 					}else {
 						con=mt.find(ConsStatus.class, conIdMap.get(n.getCon()));
 						}
 					con.setConNumber(n.getCon());
 					con.setCreatedTimestamp(new Date());
 					con.setNotificationPreference(n.getNotificationPreference());
 					con.setSubscriptionFlag(n.getSub());
 					mt.persist(con);
 					mt.commit();
 					}//end of for loop
 				
 				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,null));
 			}
 			
 		}else {
 			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
 		}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("EXCEPTION IN SERVER",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
  					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		logger.info("result "+result);
		logger.info("*************END******************");
		response.getWriter().write(result);
		
	}

}
