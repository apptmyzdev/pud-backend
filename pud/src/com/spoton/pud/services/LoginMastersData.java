package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DeliveryConditionMasterModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.ResponseDataV2;
import com.spoton.pud.jpa.DeliveryConditionMaster;

/**
 * Servlet implementation class LoginMastersData
 */
@WebServlet("/loginMastersData")
public class LoginMastersData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("LoginMastersData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginMastersData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START**************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction  = null;
		ResponseDataV2 data = new ResponseDataV2();
		String userName = request.getParameter("userName");
		String deviceImei = request.getHeader("imei");
		String apkVersion = request.getHeader("apkVersion");
		String ipadd = request.getRemoteAddr();String resultMessage="";
		try {
			manageTransaction=new ManageTransaction();
		String deliveryConditionMasterQry="Select b from DeliveryConditionMaster b";
		TypedQuery<DeliveryConditionMaster> tq3=manageTransaction.createQuery(DeliveryConditionMaster.class, deliveryConditionMasterQry);
		List<DeliveryConditionMaster> deliveryConditionMasterList=tq3.getResultList();
		if(deliveryConditionMasterList!=null&&deliveryConditionMasterList.size()>0) {
			List<DeliveryConditionMasterModal> deliveryConditionMasterDataList=new ArrayList<DeliveryConditionMasterModal>();
			DeliveryConditionMasterModal defaultCondition=new DeliveryConditionMasterModal();
			defaultCondition.setConditionId(8);
			defaultCondition.setConditionName("Ok");//we need ok as first in list so hardcoded 
			deliveryConditionMasterDataList.add(defaultCondition);
			for(DeliveryConditionMaster b:deliveryConditionMasterList) {
				if(!b.getConditionName().equalsIgnoreCase("Ok")) {
				DeliveryConditionMasterModal dc=new DeliveryConditionMasterModal();
				dc.setConditionId(b.getConditionId());
				dc.setConditionName(b.getConditionName());
				deliveryConditionMasterDataList.add(dc);
				}
				
			}
			data.setDeliveryConditionMasterList(deliveryConditionMasterDataList);
			
		}
		String vendorQuery="SELECT app_username FROM pud.vendor_master where vendor_id=(select vendor_id from pud.vendor_master where app_username=?1) and app_username like ?2";
		Query q1=manageTransaction.createNativeQuery(vendorQuery).setParameter(1, userName).setParameter(2, userName.substring(0, 4)+"%");
		List<String> userList=q1.getResultList();//need userlist for pickup reassign.so get users of the logged in vendor
		userList.add("Other Pud Partner");
		if(userList!=null&&userList.size()>0) {
			if(userList.contains(userName)) {
			userList.remove(userName);
			}
			data.setUserList(userList);
		}
		result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,data));
		resultMessage=Constants.SUCCESSFUL;
		}catch(Exception e)
		{
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER;
			logger.error("Exception in server :",e);
		}finally{
			if(manageTransaction!=null)
			manageTransaction.close();
		}
		
        boolean auditlogStatus = CommonTasks.saveAuditLog(deviceImei,apkVersion,"PulledLoginMastersData", null,userName,null,ipadd,resultMessage,null);
		
        logger.info("auditlogStatus "+auditlogStatus);
        logger.info("LoginMastersData Result: "+result);
        logger.info("*************END*************");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
