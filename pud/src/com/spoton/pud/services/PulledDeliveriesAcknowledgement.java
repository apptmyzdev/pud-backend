package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;



import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.MobileDataDownloadResult;
import com.spoton.pud.data.MobileDataDownloadResultList;
import com.spoton.pud.data.MobileDataDownloaded;
import com.spoton.pud.data.MobileDataDownloadedList;
import com.spoton.pud.data.PulledDeliveriesModal;
import com.spoton.pud.jpa.AgentPdc;
import com.spoton.pud.jpa.DeliveryConDetail;
import com.spoton.pud.jpa.PulledDataAcknowledgement;

/**
 * Servlet implementation class PulledDeliveriesAcknowledgement
 */
@WebServlet("/pulledDeliveriesAcknowledgement")
public class PulledDeliveriesAcknowledgement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pulledDeliveriesLogger = Logger.getLogger("PulledDeliveriesAcknowledgement");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PulledDeliveriesAcknowledgement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		pulledDeliveriesLogger.info("********START*********");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		String datasent=request.getReader().readLine();
		pulledDeliveriesLogger.info("Datasent from Mobile "+datasent);
		String result = "";PulledDeliveriesModal pd=null; int flag=0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 HttpURLConnection con = null;URL obj = null;StringBuilder stringBuilder = null;
		 String urlString = FilesUtil.getProperty("downloadedApplicationData");
	
		try{
			if(datasent!=null&&datasent!=""){
				pd=gson.fromJson(datasent, PulledDeliveriesModal.class);
				//pulledDeliveriesLogger.info("parsed json of datasent  "+gson.toJson(pd));
			}
		if(pd!=null&&CommonTasks.check(pd.getAgentId())&&pd.getPdcNumbers()!=null&&pd.getPdcNumbers().size()>0){
			
			mt=new ManageTransaction();
			Calendar currentDate=Calendar.getInstance();
			currentDate.setTime(new Date());
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 0);
			
			 Calendar yesterday=Calendar.getInstance();
				yesterday.add(Calendar.DATE, -1);
				yesterday.set(Calendar.HOUR_OF_DAY, 00);
				yesterday.set(Calendar.MINUTE, 00);
				yesterday.set(Calendar.SECOND, 00);
				yesterday.set(Calendar.MILLISECOND, 0);
			String query="select id, pdc FROM pud.agent_pdc where agent_id=?1 and created_timestamp between ?3 and ?4 and pulled_time is NULL";
			Query nq=mt.createNativeQuery(query).setParameter(1, pd.getAgentId());
			nq.setParameter(3, yesterday.getTime());nq.setParameter(4,currentDate.getTime());
			List<Object[]> list=nq.getResultList();
			
			if(list!=null&&list.size()>0){
				//pulledDeliveriesLogger.info("Agent data  "+gson.toJson(list));
				for(Object[] o:list){
					for(String s:pd.getPdcNumbers()){
						if(s.equals((String) o[1])){
							AgentPdc ap=mt.find(AgentPdc.class, (int) o[0]);
							ap.setPulledTime(new Date());
							mt.persist(ap);
							mt.commit();
							flag=1;
						}
					}
				}
			}
				String query3="select id, pdc FROM pud.delivery_con_details where agent_id=?1 and created_date between ?3 and ?4 and pulled_time is NULL";
				Query nq3=mt.createNativeQuery(query3).setParameter(1, pd.getAgentId());
				nq3.setParameter(3, yesterday.getTime());nq3.setParameter(4,currentDate.getTime());
				List<Object[]> list3=nq3.getResultList();
				
				if(list3!=null&&list3.size()>0){
					//pulledDeliveriesLogger.info("Agent data  "+gson.toJson(list));
					for(Object[] o:list3){
						for(String s:pd.getPdcNumbers()){
							if(s.equals((String) o[1])){
								DeliveryConDetail ap=mt.find(DeliveryConDetail.class, (int) o[0]);
								ap.setPulledTime(new Date());
								mt.persist(ap);
								mt.commit();
								flag=1;
							}
						}
					}
				
				
				MobileDataDownloadedList downloadedData=new MobileDataDownloadedList();
				//getting pulled deliveries ids
				if(flag!=0){
				/*String query2="select c.id,a.pulled_time,a.pdc from agent_pdc a,delivery_con_details c where a.pulled_time!=?1 and a.pdc=c.pdc "
						+ "and a.agent_id=?2 and c.created_date between ?3 and ?4";*/ //removing joins
					String query2="select c.id,c.pulled_time,c.pdc from delivery_con_details c where c.pulled_time!=?1  "
							+ "and c.agent_id=?2 and c.created_date between ?3 and ?4";
				Query nq2=mt.createNativeQuery(query2).setParameter(1, "NULL").setParameter(2,pd.getAgentId());
				nq2.setParameter(3, yesterday.getTime());nq2.setParameter(4,currentDate.getTime());
				List<Object[]> list2=nq2.getResultList();
				List<MobileDataDownloaded> mobDataDownload=null;
				if(list2!=null&&list2.size()>0){
					mobDataDownload=new ArrayList<MobileDataDownloaded>();
					List<Integer> idsList=new ArrayList<Integer>();
					//downloadedApplicationLogger.info("delivery conId list "+gson.toJson(list2));
					for(Object[] o:list2){
						for(String s:pd.getPdcNumbers()){
							if(s.equals((String) o[2])){
								if(!idsList.contains((int) o[0])){
								idsList.add((int) o[0]);
						MobileDataDownloaded md=new MobileDataDownloaded();
					    md.setTransactionId(Integer.toString((int) o[0]));
						md.setDownloadType("1");
						md.setDownloadDate(format.format((Date) o[1]));
						mobDataDownload.add(md);
								}
							}
						}
					}
					}
				
				if(mobDataDownload!=null&&mobDataDownload.size()>0){
					
			          try{
					downloadedData.setMobDataDownload(mobDataDownload);
					pulledDeliveriesLogger.info("urlString  "+urlString);
					pulledDeliveriesLogger.info("DataSent to Spoton "+gson.toJson(downloadedData));
					//System.out.println("DataSent to Spoton "+gson.toJson(downloadedData));
					obj = new URL(urlString);
		    		con = (HttpURLConnection) obj.openConnection();
		    		con.setRequestMethod("POST");
		    		con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
		    		con.setDoOutput(true);
		    		con.setConnectTimeout(5000);
		    		con.setReadTimeout(60000);
		    		OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
		    		  streamWriter.write(gson.toJson(downloadedData));
		    		  streamWriter.flush();
		    		  streamWriter.close();
		    		  pulledDeliveriesLogger.info("Response Code "+con.getResponseCode());
		    		  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
			              	stringBuilder=new StringBuilder();
			                  InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
			                  BufferedReader bufferedReader = new BufferedReader(streamReader);
			                  String response1 = null;
			                  while ((response1 = bufferedReader.readLine()) != null) {
			                      stringBuilder.append(response1 + "\n");
			                  }
			                  bufferedReader.close();
			                  streamReader.close();
			                  //Response received from spoton and saving in db
			                 // System.out.println("Data Received From Spoton "+stringBuilder.toString());
			                  pulledDeliveriesLogger.info("Data Received From Spoton "+stringBuilder.toString());
			                  MobileDataDownloadResultList downloadResultList=gson.fromJson(stringBuilder.toString(), MobileDataDownloadResultList.class);
			                  //pulledPickupsLogger.info("downloadResultList "+gson.toJson(downloadResultList));
			                  if(downloadResultList!=null&&downloadResultList.getMobDataDownloadResult()!=null&&downloadResultList.getMobDataDownloadResult().size()>0){
			                	int i=0;
			                	  for(MobileDataDownloadResult r:downloadResultList.getMobDataDownloadResult()){
			                		  PulledDataAcknowledgement p=new PulledDataAcknowledgement();
			                			
			                		  p.setCreatedTimestamp(new Date());
			                		  p.setDownloadDate(format.parse(mobDataDownload.get(i).getDownloadDate()));
			                		  p.setDownloadType(1);
			                		  p.setTransactionId(Integer.parseInt(mobDataDownload.get(i).getTransactionId()));
			                		  p.setResultRemarks(r.getResultRemarks());
			                		  p.setResultTransactionId(r.getTransactionId());
			                		  p.setResultFlag(r.isResultFlag()?1:0);
			                		 
			                		  mt.persist(p);
			      					  mt.commit();
			      					i++;
			                			 
			                	  }
				}
			else{
				pulledDeliveriesLogger.info("Null Response From ERP");
            }
			}else{
            	
				pulledDeliveriesLogger.info("Response failed from ERP");
         }
			}catch (SocketTimeoutException e){		
							e.printStackTrace();
							pulledDeliveriesLogger.info("SocketTimeoutException "+e);
				 }
				
			}else{
				pulledDeliveriesLogger.info("No New data Found to be Sent to Spoton");
			}
				
				 result = gson.toJson(new GeneralResponse(Constants.TRUE,
							Constants.REQUEST_COMPLETED, null));
				}else{
					 result = gson.toJson(new GeneralResponse(Constants.FALSE,
								"Data is Already Downloaded in Application", null));
				}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Deliveries found", null));
			}
			
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
	}catch(Exception e){
		e.printStackTrace();
		pulledDeliveriesLogger.error(" EXCEPTION IN SERVER",e);
		result = gson.toJson(new GeneralResponse(
				Errors.status, 
				Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
				null));
	}finally{
		if(mt!=null)
		mt.close();
		if(con!=null) {
			con.disconnect();
		}
	}
		pulledDeliveriesLogger.info("PulledDeliveriesAcknowledgement result"+result);
		//System.out.println("PulledDeliveriesAcknowledgement result"+result);
		pulledDeliveriesLogger.info("********END*********");
		response.getWriter().write(result);
	}

}
