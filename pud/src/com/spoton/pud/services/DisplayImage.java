package com.spoton.pud.services;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.spoton.common.utilities.FilesUtil;

/**
 * Servlet implementation class DisplayImage
 */
//@WebServlet("/displayImage")
public class DisplayImage extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.
	private String imagePath;
	Logger imageLogger = Logger.getLogger("DisplayImage");
	/*
	 * public void init() throws ServletException {
	 * 
	 * this.imagePath = FilesUtil.getProperty("ImagePath"); }
	 */

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
//		System.out.println("in displayImage Servlet");
		// Get requested image by path info.
		imagePath = FilesUtil.getProperty("ImagePath");
		String requestedImage = request.getPathInfo();
      //  System.out.println(" imagePath "+imagePath+" requestedImage "+requestedImage);
        imageLogger.info(" imagePath "+imagePath+" requestedImage "+requestedImage);
//		System.out.println(requestedImage);
		// Check if file name is actually supplied to the request URI.
		if (requestedImage == null) {
			// Do your thing if the image is not supplied to the request URI.
			// Throw an exception, or send 404, or show default/warning image,
			// or just ignore it.
			response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}

		// Decode the file name (might contain spaces and on) and prepare file
		// object.
		File image = new File(imagePath, URLDecoder.decode(requestedImage,"UTF-8"));
        //System.out.println("image "+image);
        imageLogger.info("image :- "+image);
		// Check if file actually exists in filesystem.

		// Get content type by filename.
		String contentType = getServletContext().getMimeType(image.getName());

		// Init servlet response.
		response.reset();
		response.setBufferSize(DEFAULT_BUFFER_SIZE);
		response.setContentType(contentType);
		response.setHeader("Content-Length", String.valueOf(image.length()));
		response.setHeader("Content-Disposition","inline; filename=\"" + image.getName() + "\"");

		// Prepare streams.
		BufferedInputStream input = null;
		BufferedOutputStream output = null;

		try {
			// Open streams.
			input = new BufferedInputStream(new FileInputStream(image),
					DEFAULT_BUFFER_SIZE);
			output = new BufferedOutputStream(response.getOutputStream(),
					DEFAULT_BUFFER_SIZE);

			// Write file contents to response.
			byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
			int length;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
		} finally {
			// Gently close streams.
			close(output);
			close(input);
		}
	}

	private static void close(Closeable resource) {
		if (resource != null) {
			try {
				resource.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}