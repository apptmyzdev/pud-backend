package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PostCashHandoverDetailsErpModal;
import com.spoton.pud.data.PostCashHandoverDetailsErpResponseModal;
import com.spoton.pud.data.PostCashHandoverDetailsModal;


/**
 * Servlet implementation class PostCashHandoverDetails
 */
@WebServlet("/postCashHandoverDetails")
public class PostCashHandoverDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PostCashHandoverDetails");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostCashHandoverDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost( request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String deviceImei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("postCashHandoverDetails");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceImei+" apkVersion "+apkVersion);
		try{
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			PostCashHandoverDetailsModal inputData=gson.fromJson(datasent,PostCashHandoverDetailsModal.class);
			if(inputData!=null) {
				PostCashHandoverDetailsErpModal erpDataToPost=new PostCashHandoverDetailsErpModal();
				erpDataToPost.setDockno(inputData.getDocketNumbersList());
				erpDataToPost.setEmpCode(inputData.getEmpCode());
				erpDataToPost.setUserID(inputData.getUserId());
				
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				
				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(erpDataToPost));
				  streamWriter.flush();
				  streamWriter.close();
				  logger.info("Datasent to spoton "+gson.toJson(erpDataToPost));
				  logger.info("Response Code : " +con.getResponseCode());
				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1);
		                }
		                bufferedReader.close();
		                streamReader.close();
		               // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
		                logger.info("Response From Spoton : " +stringBuilder.toString());
		                PostCashHandoverDetailsErpResponseModal erpResponse=gson.fromJson(stringBuilder.toString(),PostCashHandoverDetailsErpResponseModal.class);
		                if(erpResponse!=null) {
		                if(erpResponse.isResult()) {
		                	result = gson.toJson(new GeneralResponse(Constants.TRUE,erpResponse.getMessage(), null));
		                }else {
		                	result = gson.toJson(new GeneralResponse(Constants.FALSE,erpResponse.getMessage(), null));
		                }
		                }
				  }else {
					    logger.info("Response failed from Spoton");
		  		        result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response failed from Spoton with Response code as "+con.getResponseCode(), null));
				 }
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(con!=null) {
				con.disconnect();
			}
		}
		logger.info("result "+result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	}

}
