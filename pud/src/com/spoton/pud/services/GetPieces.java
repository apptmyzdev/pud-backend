package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Correct;
import com.spoton.common.utilities.Errors;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.GetPiecesModal;
import com.spoton.pud.data.PieceMasterModal;
import com.spoton.pud.data.PinCodeSpotonResponse;
import com.spoton.pud.jpa.ConsumedPiece;
import com.spoton.pud.jpa.PieceMaster;

/**
 * Servlet implementation class GetPieces
 */
@WebServlet("/getPieces")
public class GetPieces extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Logger getPiecesLogger = Logger.getLogger("GetPieces");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPieces() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("request.getRemoteAddr() "+request.getRemoteAddr());
		getPiecesLogger.info("*******************START*************************");
		String userId=request.getParameter("userId"); 
		getPiecesLogger.info("userId "+userId);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String deviceIMEI = request.getHeader("imei");
		String apkVersion=request.getHeader("apkVersion");
		 String resultMessage="";
		String ipadd = request.getRemoteAddr();
		getPiecesLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);

        String result="";
		if(userId!=null){
			/*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){
			*/
		String urlString = FilesUtil.getProperty("pieceMasterUrl")+userId;
		URL obj = new URL(urlString);
		getPiecesLogger.info("URL called "+urlString);
		
		BufferedReader in = null;
		StringBuffer response1 = new StringBuffer();
		ManageTransaction mt = null;
		HttpURLConnection con=null;
		try{
			 con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			 con.setReadTimeout(60000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			int responseCode = con.getResponseCode();
			getPiecesLogger.info("ResponseCode "+responseCode);
			
	
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	
			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			String receivedData = null;
			receivedData = response1.toString();
			//System.out.println("dataReceived "+receivedData);
			getPiecesLogger.info("DataReceived from spoton "+receivedData);
			if(receivedData!=null){
			Type piecesListType = new TypeToken<ArrayList<PieceMasterModal>>(){}.getType();
			List<PieceMasterModal> piecesList =  gson.fromJson(response1.toString(),piecesListType);
			List<PieceMaster> list=new ArrayList<PieceMaster>();
			List<GetPiecesModal> piecesModalList=new ArrayList<GetPiecesModal>();
			Map<Integer,Integer> map=new HashMap<Integer, Integer>();
			mt=new ManageTransaction();
			
			if(piecesList!=null&&piecesList.size()>0){
				//System.out.println("piecesList "+gson.toJson(piecesList));
				//getPiecesLogger.info("PiecesList after converting to json "+gson.toJson(piecesList));
				//System.out.println("piecesList size"+piecesList.size());
				
				String consumedPiecesQuery="Select c from ConsumedPiece c where c.userName=?1";
				TypedQuery<ConsumedPiece> tq=mt.createQuery(ConsumedPiece.class, consumedPiecesQuery).setParameter(1, userId);
				List<ConsumedPiece> consumedList=tq.getResultList();
				Map<Integer,String> consumedMap=new HashMap<Integer, String>();
				if(consumedList!=null&&consumedList.size()>0){
					for(ConsumedPiece c:consumedList){
						consumedMap.put(c.getPcrKey(), c.getLastPieceNo()+"-"+c.getTotalUsed());
					}
				}
				
				
				
				String query="Select pm from PieceMaster pm where pm.userId=?1";
				 TypedQuery<PieceMaster> pquery=mt.createQuery(query,PieceMaster.class).setParameter(1, userId);
				List<PieceMaster> dataList=pquery.getResultList();
				List<Integer> pkeyList=new ArrayList<Integer>();
				PieceMaster pm=null;GetPiecesModal piecesModal=null;
				//System.out.println("dataList "+gson.toJson(dataList));
				//getPiecesLogger.info("DataList from db"+gson.toJson(dataList));
				for(PieceMaster pm1:dataList){
					list.add(pm1);
					pkeyList.add(pm1.getPkey());
					map.put(pm1.getPkey(), pm1.getId());
				}
				//getPiecesLogger.info("pkeyList"+gson.toJson(pkeyList));
				for(PieceMasterModal p:piecesList){
					if(!pkeyList.contains(p.getPkey())){
						getPiecesLogger.info("New pkey series");
					pm=new PieceMaster();
					pm.setComplete(p.getComplete());
					pm.setEndNo(p.getEndNo());
					pm.setLastPieceNo(p.getLastPieceNo());
					//System.out.println("pkey "+p.getPkey());
					pm.setPkey(p.getPkey());
					pm.setSeriesType(p.getSeriesType());
					pm.setStartNo(p.getStartNo());
					pm.setTotalIsssued(p.getTotalIsssued());
					pm.setTotalUsed(p.getTotalUsed());
					pm.setUserId(userId);
					pm.setCreatedTimestamp(new Date());
					mt.persist(pm);
					piecesModal=new GetPiecesModal();
					piecesModal.setId(pm.getId());
					piecesModal.setComplete(p.getComplete());
					piecesModal.setEndNo(p.getEndNo());
					piecesModal.setLastPieceNo(p.getLastPieceNo());
					piecesModal.setPkey(p.getPkey());
					piecesModal.setSeriesType(p.getSeriesType());
					piecesModal.setStartNo(p.getStartNo());
					piecesModal.setTotalIsssued(p.getTotalIsssued());
					piecesModal.setTotalUsed(p.getTotalUsed());
					piecesModal.setUserId(userId);
					piecesModalList.add(piecesModal);
					}else{
						if(p.getSeriesType().equalsIgnoreCase("S")&&consumedMap.containsKey(p.getPkey())){
							getPiecesLogger.info("Consumed case");
							String lastPieceNumber=consumedMap.get(p.getPkey()).split("-")[0];
							String totalUsed=consumedMap.get(p.getPkey()).split("-")[1];
							getPiecesLogger.info("Pkey "+p.getPkey()+" lastPieceNumber "+lastPieceNumber+" totalUsed "+totalUsed);
						if(!lastPieceNumber.equals(p.getEndNo())){
							if(lastPieceNumber!=null&&p.getLastPieceNo()!=null&&Integer.parseInt(lastPieceNumber) >Integer.parseInt(p.getLastPieceNo())){
								getPiecesLogger.info("Greater case");
								pm=mt.find(map.get(p.getPkey()), PieceMaster.class);
								piecesModal=new GetPiecesModal();
								piecesModal.setId(pm.getId());
								piecesModal.setComplete(pm.getComplete());
								piecesModal.setEndNo(pm.getEndNo());
								piecesModal.setLastPieceNo(lastPieceNumber);
								piecesModal.setPkey(pm.getPkey());
								piecesModal.setSeriesType(pm.getSeriesType());
							
								piecesModal.setTotalIsssued(pm.getTotalIsssued());
								piecesModal.setTotalUsed(totalUsed);
								piecesModal.setUserId(userId);
								//int startSeriesNo=Integer.parseInt(pm.getStartNo());
								/*if(totalUsed!=null){
									int totalUsedCount=Integer.parseInt(totalUsed);
									startSeriesNo+=totalUsedCount;
								}*/
								piecesModal.setStartNo(pm.getStartNo());
								
								piecesModalList.add(piecesModal);
							}else{
								getPiecesLogger.info("Less than or equal case");
								pm=mt.find(map.get(p.getPkey()), PieceMaster.class);
								pm.setComplete(p.getComplete());
								pm.setEndNo(p.getEndNo());
								pm.setLastPieceNo(p.getLastPieceNo());
							    pm.setPkey(p.getPkey());
								pm.setSeriesType(p.getSeriesType());
								pm.setStartNo(p.getStartNo());
								pm.setTotalIsssued(p.getTotalIsssued());
								pm.setTotalUsed(p.getTotalUsed());
								pm.setUserId(userId);
								pm.setCreatedTimestamp(new Date());
								mt.persist(pm);
								piecesModal=new GetPiecesModal();
								piecesModal.setId(pm.getId());
								piecesModal.setComplete(p.getComplete());
								piecesModal.setEndNo(p.getEndNo());
								piecesModal.setLastPieceNo(p.getLastPieceNo());
								piecesModal.setPkey(p.getPkey());
								piecesModal.setSeriesType(p.getSeriesType());
								piecesModal.setStartNo(p.getStartNo());
								piecesModal.setTotalIsssued(p.getTotalIsssued());
								piecesModal.setTotalUsed(p.getTotalUsed());
								piecesModal.setUserId(userId);
								piecesModalList.add(piecesModal);
							}
						}
						}else{
							getPiecesLogger.info("Not yet consumed case or Manual series");
							pm=mt.find(map.get(p.getPkey()), PieceMaster.class);
							pm.setComplete(p.getComplete());
							pm.setEndNo(p.getEndNo());
							pm.setLastPieceNo(p.getLastPieceNo());
						    pm.setPkey(p.getPkey());
							pm.setSeriesType(p.getSeriesType());
							pm.setStartNo(p.getStartNo());
							pm.setTotalIsssued(p.getTotalIsssued());
							pm.setTotalUsed(p.getTotalUsed());
							pm.setUserId(userId);
							pm.setCreatedTimestamp(new Date());
							mt.persist(pm);
							piecesModal=new GetPiecesModal();
							piecesModal.setId(pm.getId());
							piecesModal.setComplete(p.getComplete());
							piecesModal.setEndNo(p.getEndNo());
							piecesModal.setLastPieceNo(p.getLastPieceNo());
							piecesModal.setPkey(p.getPkey());
							piecesModal.setSeriesType(p.getSeriesType());
							piecesModal.setStartNo(p.getStartNo());
							piecesModal.setTotalIsssued(p.getTotalIsssued());
							piecesModal.setTotalUsed(p.getTotalUsed());
							piecesModal.setUserId(userId);
							piecesModalList.add(piecesModal);
						}
						
						
						
					}
					mt.commit();
					
				
					
				
					
				}
			}
			if(piecesModalList!=null&&piecesModalList.size()>0){	
				result = gson.toJson(new GeneralResponse(
						Constants.TRUE,
						Constants.REQUEST_COMPLETED, 
						piecesModalList));
				resultMessage=Constants.REQUEST_COMPLETED;
			}else{
				 result = gson.toJson(new GeneralResponse(
						 Constants.FALSE,
						 Constants.ERROR_NO_DATA_AVAILABLE, 
	    					null));
				 resultMessage= Constants.ERROR_NO_DATA_AVAILABLE;
			}
			
			}
		}catch (SocketTimeoutException e){		
			e.printStackTrace();
			getPiecesLogger.info("SocketTimeoutException "+e);
			 result = gson.toJson(new GeneralResponse(
					 Constants.FALSE,
					 "ERP did not respond. Please try again", 
    					null));
			 resultMessage="ERP did not respond. Please try again";
			
		}catch(Exception e){
			e.printStackTrace();
			getPiecesLogger.error("EXCEPTION_IN_SERVER ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
					 resultMessage="EXCEPTION_IN_SERVER "+e;
		}finally{
			if(in!=null)
				in.close();
			if(con!=null)
				con.disconnect();
			if(mt!=null)
			mt.close();
		}
		/*}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.UPDATE_TO_LATEST_VERSION, null));
		}*/
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERROR_INCOMPLETE_DATA, null));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
		}
		 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Getting Pieces", null,userId,null,ipadd,resultMessage,null);
		 getPiecesLogger.info("auditlogStatus "+auditlogStatus);
		getPiecesLogger.info("Result "+result);
		//System.out.println("result "+result);
		getPiecesLogger.info("*****************END**************************");
		response.getWriter().write(result);
		
	}

}
