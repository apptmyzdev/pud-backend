package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DelUndelCodes;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PaymentModeModal;
import com.spoton.pud.data.PickupCancelModel;
import com.spoton.pud.data.PickupCancelReasonModel;
import com.spoton.pud.data.PickupCancellationReasonModal;
import com.spoton.pud.data.Relationships;
import com.spoton.pud.data.RelationshipsModel;
import com.spoton.pud.data.ResponseData;
import com.spoton.pud.jpa.PaymentMode;
import com.spoton.pud.jpa.PickupCancellationMaster;
import com.spoton.pud.jpa.PickupCancellationReasonMaster;
import com.spoton.pud.jpa.UserDataMaster;
import com.spoton.pud.jpa.UserEwayInfo;
import com.spoton.pud.jpa.UserSessionData;

/**
 * Servlet implementation class LoginAuthenticationService
 */
@WebServlet("/loginAuthenticationService")
public class LoginAuthenticationService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger loginAuthenticationLogger = Logger.getLogger("LoginAuthenticationService");
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginAuthenticationService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		
		loginAuthenticationLogger.info("**************START**************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		// TODO Auto-generated method stub
		
		String userName = request.getParameter("userName");
		String deviceImei = request.getParameter("imei");
		String apkVersion = request.getParameter("apkVersion");
		String password = request.getParameter("password");
		int activeFlag;String resultMessage="";
		ResponseData data = new ResponseData();
		System.out.println(userName + "-" + deviceImei+"-"+apkVersion);
		String ipadd = request.getRemoteAddr();
		if (CommonTasks.check(deviceImei, password)) 
		{
			loginAuthenticationLogger.info("logging in with username:"+userName+" ***IMEI :"+deviceImei+" ***APK Version :"+apkVersion+" password "+password);
			activeFlag = 1;
			ManageTransaction manageTransaction  = null;
			UserSessionData sessionData = null;
			UserDataMaster master = null;
			List<PaymentModeModal> list=new ArrayList<PaymentModeModal>();
			
			try{
				manageTransaction = new ManageTransaction();
				String masterQuery = "select d from UserDataMaster d where d.appUserName = ?1 and d.appPwd = ?2";
				TypedQuery<UserDataMaster> typedMasterQuery = manageTransaction.createQuery(UserDataMaster.class,masterQuery);
				typedMasterQuery.setParameter(1, userName);
				typedMasterQuery.setParameter(2, password);
				master = typedMasterQuery.getSingleResult();
			try{
				HttpSession session = request.getSession();
				String query = "select d from UserSessionData d where d.userName = ?1 and d.activeFlag = 1";
				TypedQuery<UserSessionData> typedQuery = manageTransaction.createQuery(UserSessionData.class, query);
				
				typedQuery.setParameter(1, master.getAppUserName());
				try{
					sessionData = typedQuery.getSingleResult();
					loginAuthenticationLogger.info("User :"+userName+" is already logged in another device.Login Denied");
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.LOGIN_DENIED,null));
					resultMessage=Constants.LOGIN_DENIED;
				}
				catch(Exception e){
					e.printStackTrace();
					sessionData = new UserSessionData();
					sessionData.setSessionId(session.getId());
					sessionData.setUserName(userName);
					sessionData.setImei(deviceImei);
					sessionData.setApkVersion(apkVersion);
					sessionData.setActiveFlag(activeFlag);
					sessionData.setCreatedTimestamp(new Date());
					manageTransaction.begin();
					manageTransaction.persist(sessionData);
					manageTransaction.commit();
					data.setBranch(master.getLocation());
					data.setSessionId(session.getId());
					loginAuthenticationLogger.info("Login by user "+userName+" is successful on device with IMEI-"+deviceImei);
					
					/*//checking eway flag
					String ewayQuery="Select u.ewayFlag from UserEwayInfo u where u.userName=?2";
					TypedQuery<UserEwayInfo> etq=manageTransaction.createQuery(UserEwayInfo.class, ewayQuery);
					etq.setParameter(2, userName);
					try{
					Object o=etq.getSingleResult();
					loginAuthenticationLogger.info("eway :"+(int) o);
					data.seteWayFlag((int) o);
					}catch(Exception e2){
						loginAuthenticationLogger.info("No Result found related to eway :"+e2);
					}*/
					//getting payment mode list 
					String qry = "Select pm from PaymentMode pm";
					TypedQuery<PaymentMode> pmQuery = manageTransaction.createQuery(PaymentMode.class, qry);
					List<PaymentMode> paymentModes = pmQuery.getResultList();
					if(paymentModes!=null&&paymentModes.size()>0){
						System.out.println("Size " + paymentModes.size());
						for(PaymentMode p:paymentModes){
							PaymentModeModal pmm=new PaymentModeModal();
							pmm.setId(p.getId());
							pmm.setPaymentMode(p.getPaymentMode());
							list.add(pmm);
						}
						data.setPaymentModeList(list);//payment mode list set
						loginAuthenticationLogger.info("payment modes list :"+gson.toJson(list));

					}
					else{
						data.setPaymentModeList(null);
						loginAuthenticationLogger.info("-----payment modes list is empty------");
					}
					
					//getting relationships
					String ip = request.getRemoteAddr();
					loginAuthenticationLogger.info("Agent: " + userName + " ,IMEI : " + deviceImei + " , Version: " + apkVersion + " , IP : " + ip);
					List<Relationships> codes = new ArrayList<Relationships>();
					String relquery = "Select code, description from relationships order by sort_order";
					Query nqry = manageTransaction.createNativeQuery(relquery);
					List<Object[]> relList =  nqry.getResultList();
					if(relList != null && relList.size() > 0)
					{
					for(Object[] o:relList ){
						Relationships code = new Relationships();
						code.setCode((String) o[0]);
						code.setDescription((String) o[1]);
						codes.add(code);
					}
					data.setRelationshipList(codes);//relationships list set
					loginAuthenticationLogger.info("relationships list :"+gson.toJson(codes));
					}
					else{
						data.setRelationshipList(null);
						loginAuthenticationLogger.info("------relationships list is empty-----");
					}
					
					
					//pick up cancellation master list
					List<DelUndelCodes> delundelList = new ArrayList<DelUndelCodes>();
					String dquery = "Select del_undel_code, code, short_code, description from del_undel_codes order by sort_order";
					Query delqry = manageTransaction.createNativeQuery(dquery);
					List<Object[]> dellist =  delqry.getResultList();
					if(dellist != null && dellist.size() > 0)
					{
					for(Object[] o:dellist){
						DelUndelCodes delObj = new DelUndelCodes();
						delObj.setCode((int) o[1]);
						delObj.setDel_undel_code((int) o[0]);
						delObj.setShort_code((String)o[2]);
						delObj.setDescription((String) o[3]);
						delundelList.add(delObj);
					}
					data.setDelUndelCodes(delundelList);//del and undel codes set
					loginAuthenticationLogger.info("DelUndelCodes retrieved :"+gson.toJson(delundelList));
					}
					else{
						data.setDelUndelCodes(null);
						loginAuthenticationLogger.info("delundel codes list is empty");
					}
					
					//pick up cancellation reasons
					String rQuery = "select r from PickupCancellationReasonMaster r";
					List<PickupCancelReasonModel> reasonsList = new ArrayList<PickupCancelReasonModel>();
					TypedQuery<PickupCancellationReasonMaster> reasonQuery = manageTransaction.createQuery(PickupCancellationReasonMaster.class, rQuery);
					List<PickupCancellationReasonMaster> cancelReasonsList = reasonQuery.getResultList();
					if(cancelReasonsList != null && cancelReasonsList.size() > 0)
					{
						PickupCancelReasonModel reasonModel = null;
						for(PickupCancellationReasonMaster obj : cancelReasonsList)
						{
							reasonModel = new PickupCancelReasonModel();
							reasonModel.setFailureCategory(obj.getFailureCategory());
							reasonModel.setPickupStatusId(obj.getPickupStatusId());
							reasonModel.setReasonCode(obj.getReasonCode());
							reasonModel.setReasonDesc(obj.getReasonDesc());
							reasonModel.setUpdatedBy(obj.getUpdatedBy());
							reasonModel.setUpdatedOn(obj.getUpdatedOn());
							reasonModel.setActiveStatus(obj.getActiveStatus());
							reasonsList.add(reasonModel);
						}
						data.setCancelReasonsList(reasonsList);// pick up cancel reasons list set
						loginAuthenticationLogger.info("pick up cancellation resaons list retrieved :"+gson.toJson(reasonsList));
					}
					else
					{
						data.setCancelReasonsList(null);
						loginAuthenticationLogger.info("pick up cancellation resaons list is empty");
					}
					
					//PickupCancellationMaster
					String pickQuery = "select r from PickupCancellationMaster r";
					List<PickupCancelModel> cancelPickupList = new ArrayList<PickupCancelModel>();
					TypedQuery<PickupCancellationMaster> cancelPickupQuery = manageTransaction.createQuery(PickupCancellationMaster.class, pickQuery);
					List<PickupCancellationMaster> pickupCancelList = cancelPickupQuery.getResultList();
					if(pickupCancelList != null && pickupCancelList.size() > 0)
					{
						PickupCancelModel objModel = null;
						for(PickupCancellationMaster o : pickupCancelList)
						{
							objModel = new PickupCancelModel();
							objModel.setStatus(o.getStatus());
							objModel.setStatusId(o.getStatusId());
							cancelPickupList.add(objModel);
						}
						data.setPickupCancelMasterList(cancelPickupList);
						loginAuthenticationLogger.info("pick up cancellation master :"+gson.toJson(cancelPickupList));
					}
					else{
						data.setPickupCancelMasterList(null);
						loginAuthenticationLogger.info("pick up cancellation master list is empty");
					}
					
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,data));
					resultMessage=Constants.SUCCESSFUL;
				}//catch block end
			}
				catch(Exception e)
				{
					e.printStackTrace();
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,null));
					resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
					loginAuthenticationLogger.info("Response sent :"+result);
					loginAuthenticationLogger.info("Exception in server :"+e);
				}
			}
			catch(Exception e){
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_AUTH_FAILED,null));
				resultMessage=Constants.ERROR_AUTH_FAILED;
				loginAuthenticationLogger.info("Response sent :"+result);
				loginAuthenticationLogger.info("Exception in server :"+e);
			}
			finally{
				manageTransaction.close();
			}
		} 
		else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
			loginAuthenticationLogger.info("Response sent :"+result);
			
		}
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceImei,apkVersion,"Login", null,userName,null,ipadd,resultMessage,null);
		loginAuthenticationLogger.info("auditlogStatus "+auditlogStatus);
		response.getWriter().write(result);
		loginAuthenticationLogger.info("*************END*************");
	}
}
