package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.CustomerServiceMasterModel;
import com.spoton.pud.data.GeneralResponse;

import com.spoton.pud.jpa.CustomerServiceMaster;


/**
 * Servlet implementation class GetCustomerServiceMaster
 */
@WebServlet("/getCustomerServiceMaster")
public class GetCustomerServiceMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetCustomerServiceMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCustomerServiceMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*****************Start***************");
 
		String apkVersion = "";
		apkVersion = request.getHeader("apkVersion");
		Gson gson = new GsonBuilder().serializeNulls().create();
		logger.info("ver::"+apkVersion);
		int version = 0;
		int pagelimit = 10000;
		int pageno = 0;
		int offset = 0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		if(CommonTasks.check(request.getParameter("page")))
			pageno = Integer.parseInt(request.getParameter("page"));
		else
			pageno = 1;
		
		offset = (pageno - 1) * 10000;
		
		
		try{
			version = Integer.parseInt(request.getParameter("version"));
		}catch (Exception e){
			version = 0;
		}
		logger.info("Version "+version);
		String result = "";
		//StringBuilder toCsv=new StringBuilder();
		
		ManageTransaction mt = new ManageTransaction();

		try{
		String qry = "Select pcm from CustomerServiceMaster pcm where pcm.version > ?1 and pcm.activeFlag=1";
		TypedQuery<CustomerServiceMaster> pcmQuery = mt.createQuery(CustomerServiceMaster.class, qry);
		pcmQuery.setParameter(1, version);
		pcmQuery.setFirstResult(offset).setMaxResults(pagelimit);
		List<CustomerServiceMaster> dataList = pcmQuery.getResultList();
		List<CustomerServiceMasterModel> list=new ArrayList<CustomerServiceMasterModel>();
		if(dataList!=null&&dataList.size()>0) {
			logger.info("DataList Size " + dataList.size());
			
		for (CustomerServiceMaster csm:dataList){
		CustomerServiceMasterModel cm=new CustomerServiceMasterModel();
		cm.setAccountCode(csm.getId().getAccountCode());
		cm.setDestination(csm.getId().getDestination());
		cm.setOrigin(csm.getId().getOrigin());
		cm.setVersion(csm.getVersion());
		cm.setCreatedTimestamp(csm.getCreatedTime()!=null?format.format(csm.getCreatedTime()):null);
		cm.setActiveFlag(csm.getActiveFlag());
		list.add(cm);
		}
		result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,list));
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
		}
		
		
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			
		}
		finally{
			mt.close();
		}
		
		logger.info("Result "+result);
		logger.info("*****************End***************");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
