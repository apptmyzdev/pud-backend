package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.VehicleEtaSheetDetailsModal;
import com.spoton.pud.data.VehicleEtaSheetsModal;

/**
 * Servlet implementation class GetVehicleEtaSheetsData
 */
@WebServlet("/getVehicleEtaSheetsData")
public class GetVehicleEtaSheetsData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetVehicleEtaSheetsData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetVehicleEtaSheetsData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START**************");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		ManageTransaction mt = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		String scCode=request.getParameter("scCode");
		String vehicleNumber=request.getParameter("vehicleNumber");
		logger.info(" scCode "+scCode+" vehicleNumber "+vehicleNumber);
		try {
			mt=new ManageTransaction();
			if(CommonTasks.check(scCode,vehicleNumber)){
				Calendar currentDate=Calendar.getInstance();
				currentDate.setTime(new Date());
				currentDate.set(Calendar.HOUR_OF_DAY, 23);
				currentDate.set(Calendar.MINUTE, 59);
				currentDate.set(Calendar.SECOND, 59);
				currentDate.set(Calendar.MILLISECOND, 0);
				
				 Calendar yesterday=Calendar.getInstance();
					yesterday.add(Calendar.DATE, -1);
					yesterday.set(Calendar.HOUR_OF_DAY, 00);
					yesterday.set(Calendar.MINUTE, 00);
					yesterday.set(Calendar.SECOND, 00);
					yesterday.set(Calendar.MILLISECOND, 0);
		   String query="SELECT pickup_unloading_sheet_number,total_cons_count,total_pieces_count,total_con_weight FROM pud.auto_pickup_unloading a "
		   		+ "where a.transaction_result='true' and a.vehicle_number=?1 and a.user_id like ?2 and a.is_unloading_started=0  and a.created_timestamp>?3 and  a.created_timestamp<?4";
			Query q=mt.createNativeQuery(query).setParameter(1, vehicleNumber).setParameter(2, scCode+"%");
			q.setParameter(3, yesterday.getTime()).setParameter(4, currentDate.getTime());
			List<Object[]> list=q.getResultList();
			if(list!=null&&list.size()>0) {
				List<VehicleEtaSheetsModal> finalList=new ArrayList<VehicleEtaSheetsModal>();
				for(Object[] o:list) {
					VehicleEtaSheetsModal vm=new VehicleEtaSheetsModal();
					vm.setPickupUnloadingSheetNumber((String) o[0]);
					VehicleEtaSheetDetailsModal sheetDetails=new VehicleEtaSheetDetailsModal();
					sheetDetails.setConCount((int) o[1]);
					sheetDetails.setConWeight((double) o[3]);
					sheetDetails.setPiecesCount((int) o[2]);
					vm.setSheetDetails(sheetDetails);
					finalList.add(vm);
				}
				
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,finalList));
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
			}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA,null));
				}
			
		
		}catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				
			} finally {
				if (mt != null)
					mt.close();
			}
			logger.info("result "+result);
			logger.info("**************END**************");
			response.getWriter().write(result);
			
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
