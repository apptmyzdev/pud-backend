package com.spoton.pud.services;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModal;
import com.spoton.pud.data.ConDetailsModalV2;
import com.spoton.pud.data.EwayBillDetails;
import com.spoton.pud.data.EwayBillNo;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.PieceVolumeModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesImages;
import com.spoton.pud.jpa.AddedCon;
import com.spoton.pud.jpa.AddedEwayBillNumber;
import com.spoton.pud.jpa.AddedPiece;
import com.spoton.pud.jpa.AddedPieceEntry;
import com.spoton.pud.jpa.AddedPieceVolume;
import com.spoton.pud.jpa.AddedPiecesImage;
import com.spoton.pud.jpa.ConsumedPiece;
import com.spoton.pud.jpa.UpdatedEwayBillNumber;

/**
 * Servlet implementation class AddedConsV2
 */
@WebServlet("/addedConsV2")
public class AddedConsV2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pickupUpdationLogger = Logger.getLogger("AddedConsV2");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddedConsV2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pickupUpdationLogger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String datasent="";ManageTransaction mt=null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String pickupScheduleId=null; String resultMessage="";String userName="";
		String ipadd = request.getRemoteAddr();
		pickupUpdationLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		Map<Integer,String> conMap=new HashMap<Integer, String>();
		try{
			datasent=request.getReader().readLine();
			//System.out.println("datasent "+datasent);
			pickupUpdationLogger.info("Datasent from Mobile "+datasent);
			Type type = new TypeToken<ConDetailsModalV2>(){}.getType();
			ConDetailsModalV2 conList=gson.fromJson(datasent,type);
	    			//System.out.println("conList "+gson.toJson(conList));
	    			pickupUpdationLogger.info("conList "+gson.toJson(conList));
	    			AddedCon cd=null;AddedCon cd1=null;
	    			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    			
	    			if(conList!=null)
	    			{
	    				mt=new ManageTransaction();userName=conList.getUserName();
	    				pickupScheduleId=Integer.toString(conList.getPickupScheduleId());
	    				 
	    				
	    				 Map<Integer,Integer> consumedPieceMap=new HashMap<Integer, Integer>();
	     			    String consumedPieceQuery="Select c.id,c.pcrKey from ConsumedPiece c where c.userName=?1";
	     			    TypedQuery<Object[]> pq=mt.createQuery(Object[].class, consumedPieceQuery).setParameter(1, userName);
	     			    List<Object[]> consumedPiecesList=pq.getResultList();
	     			    for(Object[] o:consumedPiecesList){
	     			    	consumedPieceMap.put((int) o[1],(int) o[0]);
	     			    }
	    				
	    				
	    				
	    				String query="select con_number from pud.con_details where user_name=?1";
	    			     Query q=mt.createNativeQuery(query).setParameter(1,userName);
	    			     List<String> queryList=q.getResultList();
	    			     //System.out.println(conList.getConNumber()+" exists:- "+queryList.contains(conList.getConNumber()));
	    			     pickupUpdationLogger.info(conList.getConNumber()+" exists:- "+queryList.contains(conList.getConNumber()));
	    			    if(!queryList.contains(conList.getConNumber()))
	    			  { 
	    			    cd=new AddedCon();
	    				cd.setActualWeight(conList.getActualWeight());
	    				cd.setApplyDc(conList.getApplyDC());
	    				cd.setApplyNfForm(conList.getApplyNfForm());
	    				cd.setApplyVtv(conList.getApplyVTV());
	    				cd.setConNumber(conList.getConNumber());
	    				cd.setConsignmentType(conList.getConsignmentType());
	    				cd.setCrmScheduleId(conList.getCrmScheduleId());
	    				cd.setCustomerCode(conList.getCustomerCode());
	    				cd.setCustomerName(conList.getCustomerName());
	    				cd.setDeclaredValue(conList.getDeclaredValue());
	    				cd.setDestinationPincode(conList.getDestinationPinCode());
	    				cd.setGatePassTime(conList.getGatePassTime());
	    				/*cd.setImage1Url(conList.getImage1URL());
	    				cd.setImage2Url(conList.getImage2URL());*/
	    				//System.out.println("CommonTasks.toBase64 "+CommonTasks.toBase64(conList.getImage1URL()));
	    				if(conList.getImage1URL()!=null&&(!conList.getImage1URL().isEmpty())){
	    				cd.setImage1Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage1URL(),conList.getConNumber(),"jpg",1,"Image1"));
	    				}else{cd.setImage1Url("");}
	    				if(conList.getImage2URL()!=null&&(!conList.getImage2URL().isEmpty())){
	    				cd.setImage2Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage2URL(),conList.getConNumber(),"jpg",1,"Image2"));
	    				}else{cd.setImage2Url("");}
	    				cd.setLatValue(conList.getLatValue());
	    				cd.setLongValue(conList.getLongValue());
	    				cd.setNoOfPackage(conList.getNoOfPackage());
	    				cd.setOrderNo(conList.getOrderNo());
	    				cd.setOriginPincode(conList.getOriginPinCode());
	    				cd.setPackageType(conList.getPackageType());
	    				cd.setPanNo(conList.getPanNo());
	    				cd.setPaymentBasis(conList.getPaymentBasis());
	    				cd.setPickupScheduleId(conList.getPickupScheduleId());
	    				cd.setEwayBillNumber(conList.geteWayBillNumber());
	    			    if(conList.getPickupDate()!=null)
	    				cd.setPickupDate(format.parse(conList.getPickupDate()));
	    				cd.setProduct(conList.getProduct());
	    				cd.setReceiverName(conList.getReceiverName());
	    				cd.setReceiverPhoneNo(conList.getReceiverPhoneNo());
	    				cd.setRefNumber(conList.getRefNumber());
	    				cd.setRiskType(conList.getRiskType());
	    				/*cd.setShipmentImageUrl(conList.getShipmentImageURL());*/
	    				if(conList.getShipmentImageURL()!=null&&(!conList.getShipmentImageURL().isEmpty())){
	    				cd.setShipmentImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getShipmentImageURL(),conList.getConNumber(),"jpg",1,"Shipment"));
	    				}else{cd.setShipmentImageUrl("");}
	    				cd.setSpecialInstruction(conList.getSpecialInstruction());
	    				cd.setTinNo(conList.getTinNo());
	    				cd.setTotalVolWeight(conList.getTotalVolWeight());
	    				cd.setUserId(conList.getUserId());
	    				cd.setUserName(conList.getUserName());
	    				cd.setVehicleNumber(conList.getVehicleNumber());
	    				cd.setVolType(conList.getVolType());
	    				cd.setVtcAmount(conList.getVtcAmount());
	    				cd.setIsScanned(conList.isScanned()==true?1:0);
	    				cd.setCreatedTimestamp(new Date());
	    				mt.persist(cd);
	    				
	    				AddedPiece p=null;
	    				
	    				if(conList.getPieces()!=null&&conList.getPieces().size()>0)
	    				for(Pieces pc:conList.getPieces()){
	    				p=new AddedPiece();
	    				
	    				p.setLastPieceNo(pc.getLastPieceNumber());
	    				p.setPcrKey(Integer.parseInt(pc.getPcrKey()));
	    				p.setTotalUsed(pc.getTotalUsed());
	    				p.setAddedCon(cd);
	    				p.setCreatedTimestamp(new Date());
	    				mt.persist(p);
	    				ConsumedPiece cp=null;
	    				if(consumedPieceMap.containsKey(p.getPcrKey())){
	    					cp=mt.find(ConsumedPiece.class,consumedPieceMap.get(p.getPcrKey()));
	    					if(Integer.parseInt(cp.getLastPieceNo())<Integer.parseInt(pc.getLastPieceNumber())){
	    					cp.setCreatedTimestamp(new Date());
		    				cp.setLastPieceNo(pc.getLastPieceNumber());
		    				cp.setPcrKey(Integer.parseInt(pc.getPcrKey()));
		    				cp.setUserName(userName);
		    				cp.setTotalUsed(pc.getTotalUsed());
		    				mt.persist(cp);
	    					}
	    				}else{
	    					cp=new ConsumedPiece();
	    					cp.setCreatedTimestamp(new Date());
		    				cp.setLastPieceNo(pc.getLastPieceNumber());
		    				cp.setPcrKey(Integer.parseInt(pc.getPcrKey()));
		    				cp.setUserName(userName);
		    				cp.setTotalUsed(pc.getTotalUsed());
		    				mt.persist(cp);
	    				}
	    				
	    				}
	    				AddedPieceEntry pieceEntry=null;
	    				
	    				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
	    				for(PieceEntryModal pe:conList.getPieceEntry()){
	    					pieceEntry=new AddedPieceEntry();
	    					
	    					pieceEntry.setAddedCon(cd);
	    					pieceEntry.setFromPieceNo(pe.getFromPieceNo());
	    					pieceEntry.setPcrKey(Integer.parseInt(pe.getPcrKey()));
	    					pieceEntry.setToPieceNo(pe.getToPieceNo());
	    					pieceEntry.setTotalPieces(pe.getTotalPieces());
	    					pieceEntry.setCreatedTimestamp(new Date());
		    				mt.persist(pieceEntry);
		    				
		    				}
	    				AddedPieceVolume pieceVolume=null;
	    				
	    				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
	    				for(PieceVolumeModal pv:conList.getPieceVolume()){
	    					pieceVolume=new AddedPieceVolume();
	    					
	    					pieceVolume.setAddedCon(cd);
	    					pieceVolume.setNoOfPieces(pv.getNoOfPieces());
	    					pieceVolume.setTotalVolWeight(pv.getTotalVolWeight());
	    					pieceVolume.setVolBreadth(pv.getVolBreadth());
	    					pieceVolume.setVolHeight(pv.getVolHeight());
	    					pieceVolume.setVolLength(pv.getVolLength());
	    					pieceVolume.setCreatedTimestamp(new Date());
		    				mt.persist(pieceVolume);
		    				
		    				}
	    				String piecesImagesURL="";
	    				List<PiecesImages> piecesImagesList=new ArrayList<PiecesImages>();
	    				
	    				
	    				
	    				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
	    					int k=0;
	    				for(PiecesImages pi:conList.getPiecesImages()){
	    					k++;
	    					AddedPiecesImage pieceImage=new AddedPiecesImage();
	    				 
	    				   pieceImage.setAddedCon(cd);
	    				   pieceImage.setCreatedTimestamp(new Date());
	    				   piecesImagesURL=FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(pi.getPiecesImagesURL(),conList.getConNumber(),"jpg",1,"pieceImage"+Integer.toString(k));
	    				   pieceImage.setPiecesImagesUrl(piecesImagesURL);
	    				   mt.persist(pieceImage);
	    				   pi.setPiecesImagesURL(piecesImagesURL);
	    				   piecesImagesList.add(pi);
	    				  
	    			   }
	    				}
	    				
	    				
	    				
	    				
	    				if(conList.geteWayBillNumberList()!=null&&conList.geteWayBillNumberList().size()>0){
	    					for(String s:conList.geteWayBillNumberList()){
	    						AddedEwayBillNumber ae=new AddedEwayBillNumber();
	    						ae.setAddedCon(cd);
	    						ae.setCreatedTimestamp(new Date());
	    						/*ae.setEwayBillNumber(s.getEwayBillNum());
	    						ae.setInvoiceAmount(s.getInvoiceAmount());
	    						ae.setInvoiceNumber(s.getInvoiceNo());
	    						ae.setIsExempted(s.getIsExempted());*/
	    						mt.persist(ae);
	    					}
	    				}
	    				
	    				
	    				mt.commit();
	    				//System.out.println("cd id "+cd.getId());
	    				conList.setConEntryId(cd.getId());
	    				conList.setImage1URL(cd.getImage1Url());
	    				conList.setImage2URL(cd.getImage2Url());
	    				conList.setShipmentImageURL(cd.getShipmentImageUrl());
	    				conList.setPiecesImages(piecesImagesList);
	    			
	    				cd1=mt.find(AddedCon.class, cd.getId());
	    				if(cd1!=null){
	    					conMap.put(cd.getId(), cd.getConNumber());
	    				cd1.setConEntryId(cd.getId());
	    				mt.persist(cd1);mt.commit();
	    				result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",conList.getConEntryId()));
	    				resultMessage="Success";
	    				}else{
	    					 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Adding Con failed",null));
	    					 resultMessage="Adding Con failed";
	    				}
	    			  }else{
	    				  result = gson.toJson(new GeneralResponse(Constants.TRUE,"Con Already Exists for Con Numbers "+conList.getConNumber(), null));
		    				resultMessage="Con Already Exists for Con Numbers "+conList.getConNumber();
	    			  }
	    			}else{
	    				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
	    				resultMessage=Constants.ERROR_INCOMPLETE_DATA;
	    			}
	    			
	    			
	    			
	    			
	            
		}catch(Exception e){
			e.printStackTrace();
			pickupUpdationLogger.error("EXCEPTION_IN_SERVER", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
			
		}finally{
			if(mt!=null){mt.close();}
			
			
		}
		
		//System.out.println("Added Cons result"+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Adding Con", conMap.values(),userName,pickupScheduleId,ipadd,resultMessage,null);
		pickupUpdationLogger.info("auditlogStatus "+auditlogStatus);
		pickupUpdationLogger.info("Added Cons result"+result);
		pickupUpdationLogger.info("**************END*******************");
		response.getWriter().write(result);
	
	}

}
