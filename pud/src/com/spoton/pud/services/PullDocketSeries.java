package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DocketSeriesResponseModel;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class PullDocketSeries
 */
@WebServlet("/pullDocketSeries")
public class PullDocketSeries extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pullDocketSeriesLogger = Logger.getLogger("PullDocketSeries");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullDocketSeries() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doPost(request , response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		pullDocketSeriesLogger.info("******START***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		String userId = request.getParameter("userId");
		pullDocketSeriesLogger.info("Request from user : "+userId);
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		Gson gson = new GsonBuilder().serializeNulls().create();
		String resultMessage="";String ipadd = request.getRemoteAddr();
		ManageTransaction manageTransaction = null;
		try{
			manageTransaction = new ManageTransaction();
			List <DocketSeriesResponseModel> docketList = new ArrayList<DocketSeriesResponseModel>();
			Map <String,String> usedConsMap = new HashMap<String, String>();
			
 			if(CommonTasks.check(userId))
			{
 				boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
				if(checkapkVersion){
				Calendar calendar = Calendar.getInstance();
				calendar.set(Calendar.HOUR, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				 calendar.set(Calendar.HOUR_OF_DAY, 0);
				 
				Date date = calendar.getTime();
				//String uQuery = "select d.con_number from con_details d where user_name = ?1";
				//String query = "select * from docket_series_master where user_name = ?1 and created_timestamp > ?2";
				String query = "select * from docket_series_master where user_name = ?1";
				Query nativeQuery = manageTransaction.createNativeQuery(query);
				nativeQuery.setParameter(1, userId);
				//nativeQuery.setParameter(2, date);
				/*Query nativeQuery1 = manageTransaction.createNativeQuery(uQuery);
				nativeQuery1.setParameter(1, userId);*/
				List <Object []> dataList = nativeQuery.getResultList();
				/*List<Object []> usedConList = nativeQuery1.getResultList();*/
				/*for(Object o : usedConList)
				{
					usedConsMap.put((String)o,(String)o);
				}
				pullDocketSeriesLogger.info("usedCons :"+usedConsMap.size());*/
				
				if(dataList != null && dataList.size() > 0)
				{
					for(Object [] obj : dataList)
					{
						DocketSeriesResponseModel dataobj = new DocketSeriesResponseModel();
						dataobj.setDocketKey((int) obj[2]);
						dataobj.setUserId((int) obj[3]);
						dataobj.setUserName((String) obj[4]);
						dataobj.setVendorCode((String) obj[5]);
						dataobj.setBranchCode((String) obj[6]);
						dataobj.setFromSeries((String) obj[7]);
						dataobj.setToSeries((String) obj[8]);
						dataobj.setLastSrNumber((String) obj[9]);
						dataobj.setTotalLeaf((String) obj[10]);
						dataobj.setCreatedTimestamp((Date) obj[11]);
						dataobj.setProductType((String) obj[12]);
						docketList.add(dataobj);
					}
					
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,docketList));
				//	pullDocketSeriesLogger.info(result);
					resultMessage=Constants.SUCCESSFUL;
				}
				
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
					//pullDocketSeriesLogger.info(result);
					resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
				}
				}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							Constants.UPDATE_TO_LATEST_VERSION, null));
				}
			}
			else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA,null));
			//	pullDocketSeriesLogger.info(result);
				resultMessage=Constants.ERROR_INCOMPLETE_DATA;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			pullDocketSeriesLogger.error("EXCEPTION IN SERVER :",e);
			pullDocketSeriesLogger.info(result);
		}
		finally{
			manageTransaction.close();
		}
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Pulled Docket Series", null,userId,null,ipadd,resultMessage,null);
		pullDocketSeriesLogger.info("auditlogStatus "+auditlogStatus);
		pullDocketSeriesLogger.info(result);
		response.getWriter().write(result);
		pullDocketSeriesLogger.info("***************** END ***************");
	}

}
