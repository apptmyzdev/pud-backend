package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PaymentStatusModal;
import com.spoton.pud.data.PaymentStausErpResonseModal;
import com.spoton.pud.data.PaymentStausResonseModal;
import com.spoton.pud.data.PickupRegistrationData;

/**
 * Servlet implementation class CheckPaymentStatus
 */
@WebServlet("/checkPaymentStatus")
public class CheckPaymentStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("CheckPaymentStatus"); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckPaymentStatus() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String datasent="";ManageTransaction mt=null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String ipadd = request.getRemoteAddr();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("checkPaymentStatus");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		try{
			
			datasent=request.getReader().readLine();
			logger.info("DataSent From Mobile "+datasent);
			PaymentStatusModal input=gson.fromJson(datasent, PaymentStatusModal.class);
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("ConNumber",input.getConNumber() );
			jsonObj.put("InvoiceNumber",input.getInvoiceNumber());
			jsonObj.put("UserID",input.getUserId());
			obj = new URL(urlString);
			con = (HttpURLConnection) obj.openConnection();
			
			con.setConnectTimeout(5000);
			con.setReadTimeout(60000);
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			con.setRequestProperty("Accept", "application/json");
			con.setDoOutput(true);
			
			OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
			  streamWriter.write(gson.toJson(jsonObj));
			  streamWriter.flush();
			  streamWriter.close();
			  logger.info("Response Code : " +con.getResponseCode());
			  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
	            	stringBuilder=new StringBuilder();
	                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	                String response1 = null;
	                while ((response1 = bufferedReader.readLine()) != null) {
	                    stringBuilder.append(response1 + "\n");
	                }
	                bufferedReader.close();
	                streamReader.close();
	               logger.info("Response From Spoton : " +stringBuilder.toString());
	               Type type = new TypeToken<ArrayList<PaymentStausErpResonseModal>>(){}.getType();
	    			List<PaymentStausErpResonseModal> responseList=gson.fromJson(stringBuilder.toString(), type);
	                if(responseList!=null&&responseList.size()>0){
	                	List<PaymentStausResonseModal> responseData=new ArrayList<PaymentStausResonseModal>();
	    				for(PaymentStausErpResonseModal p:responseList){
	    					PaymentStausResonseModal  data=new PaymentStausResonseModal();
	    					data.setAmount(p.getAmount());
	    					data.setConNo(p.getConNo());
	    					data.setCreatedOn(p.getCreatedOn());
	    					data.setCustomerCode(p.getCustomerCode());
	    					data.setCustomerName(p.getCustomerName());
	    					data.setInvoiceNo(p.getInvoiceNo());
	    					data.setPgTransactionId(p.getPGTransactionId());
	    					data.setSuccess(p.getSuccess());
	    					responseData.add(data);
	    				}
	    				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,responseData));
	    			}
	    			
	    		}else{
	            	
	            //System.out.println("Response failed");
	           logger.info("Response failed from spoton");
	            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
	            
	            }
		}catch(Exception e){
			
			e.printStackTrace();
			logger.error("EXCEPTION_IN_SERVER", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			
		}finally{
			if(mt!=null){mt.close();}
			
			if(con!=null){con.disconnect();}
		}
		logger.info("Result "+result);
		logger.info("**************END*******************");
		response.getWriter().write(result);
	
	}

}
