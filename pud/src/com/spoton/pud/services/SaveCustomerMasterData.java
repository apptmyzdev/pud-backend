package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.pud.data.CustomerMasterErpModal;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PinCodeSpotonResponse;
import com.spoton.pud.jpa.CustomerMasterData;

/**
 * Servlet implementation class SaveCustomerMasterData
 */
@WebServlet("/saveCustomerMasterData")
public class SaveCustomerMasterData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveCustomerMasterData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.setContentType(Constants.CONTENT_TYPE_JSON);
  		String result = "";
  		Gson gson = new GsonBuilder().serializeNulls().create();
  		ManageTransaction mt=new ManageTransaction();
  		HttpURLConnection con=null; URL obj =null;
  		try {
  			String query="Select c.customerCode,c.type from CustomerMasterData c";
  			TypedQuery<Object[]> tq=mt.createQuery(Object[].class, query);
  			List<Object[]> list= tq.getResultList();
  			List<String> customerList=new ArrayList<String>();
  			if(list!=null&&list.size()>0){
  				for(Object[] o:list){
  					String key=(String) o[0]+"-"+(String) o[1];
  					customerList.add(key);
  				}
  			}
  			
  			String urlString=FilesUtil.getProperty("customerMasterData");
  			StringBuilder stringBuilder = new StringBuilder();	BufferedReader in = null;
  			      obj=new URL(urlString);
  				 con = (HttpURLConnection) obj.openConnection();
  				// add reuqest header
  				con.setRequestMethod("GET");
  				//con.setConnectTimeout(5000);
  				 //con.setReadTimeout(10000);
  				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
  				
  				 if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
  				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
  				String inputLine;
  		
  				while ((inputLine = in.readLine()) != null) {
  					stringBuilder.append(inputLine);
  				}
  				in.close();
  				Type customerListType = new TypeToken<ArrayList<CustomerMasterErpModal>>(){}.getType();
  				List<CustomerMasterErpModal> customers =  gson.fromJson(stringBuilder.toString(),customerListType);
  				if(customers!=null&&customers.size()>0){
  					for(CustomerMasterErpModal c:customers){
  						if(!customerList.contains((c.getCustomerCode()+"-"+c.getType()))){
  						CustomerMasterData cm=new CustomerMasterData();
  						cm.setCustomer_emailId(c.getCustomerEmailId());
  						cm.setCustomerAddress(c.getCustomerAddress());
  						cm.setCustomerBranch(c.getCustomerBranch());
  						cm.setCustomerCity(c.getCustomerCity());
  						cm.setCustomerCode(c.getCustomerCode());
  						cm.setCustomerMobileNumber(c.getCustomerMobileNumber());
  						cm.setCustomerName(c.getCustomerName());
  						cm.setCustomerType(c.getCustomerType());
  						cm.setLatitude(c.getCustomerLocationLatitude());
  						cm.setLongitude(c.getCustomerLocationLongitude());
  						cm.setTimestamp(new Date());
  						cm.setType(c.getType());
  						mt.persist(cm);
  						mt.commit();
  						}
  					}
  					result = gson.toJson(new GeneralResponse(
  							Constants.TRUE,
  							Constants.REQUEST_COMPLETED, 
  							null));
  				}
  				 }else{
		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		            
		            }
  		}catch (Exception e) {
  			e.printStackTrace();
  			result = gson.toJson(new GeneralResponse(Constants.FALSE,
  					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
  			
  		} finally {
  		if(mt!=null){
  			mt.close();
  			if(con!=null) {
  				con.disconnect();
  			}
  				
  		}
  		}
  		
  		response.getWriter().write(result);
  	}
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
