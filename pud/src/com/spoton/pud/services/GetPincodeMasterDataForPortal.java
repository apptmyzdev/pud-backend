package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.Pincode;
import com.spoton.pud.jpa.PinCodeMaster;

/**
 * Servlet implementation class GetPincodeMasterDataForPortal
 */


//this api is to show pincode master on pud admin portal
@WebServlet("/getPincodeMasterDataForPortal")
public class GetPincodeMasterDataForPortal extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetPincodeMasterDataForPortal");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPincodeMasterDataForPortal() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		List<Pincode> pins = new ArrayList<Pincode>();
		try {
			mt = new ManageTransaction();
			String qry = "Select pcm from PinCodeMaster pcm";
			TypedQuery<PinCodeMaster> pcmQuery = mt.createQuery(PinCodeMaster.class, qry);
			List<PinCodeMaster> pinCodes = pcmQuery.getResultList();
			logger.info("Pincodes Size " + pinCodes.size());
			if(pinCodes!=null&&pinCodes.size()>0) {
			for (PinCodeMaster pincode:pinCodes){
				Pincode pin = new Pincode();
				pin.setActive(pincode.getActive() == 1?true:false);
                pin.setBranchCode(pincode.getId().getBranchCode());
				pin.setLocationName(pincode.getLocationName());
				pin.setPinCode(pincode.getId().getPinCode());
				pin.setServicable(pincode.getServicable());
				pin.setServiceType(pincode.getServiceType());
				pin.setVersion(pincode.getVersion());
				if(pincode.getIsAirServicable()!=null)
				pin.setIsAirServicable(pincode.getIsAirServicable().equalsIgnoreCase("N")?0:1);
				if(pincode.getIsAirServicableDel()!=null)
				pin.setIsAirServicableDel(pincode.getIsAirServicableDel().equalsIgnoreCase("N")?0:1);
				pin.setAreaId(pincode.getAreaId());
				pin.setPincodeType(pincode.getId().getPincodeType());
				pins.add(pin);
               }
			result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED,pins));
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_NO_DATA_AVAILABLE, null));
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if (mt != null) {
				mt.close();
			}
		}
		
		logger.info("result " + result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
