package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.Pincode;
import com.spoton.pud.data.PincodeMasterDevResponse;
import com.spoton.pud.jpa.PinCodeMaster;

/**
 * Servlet implementation class GetPincodeMaster
 */
@WebServlet("/getpincodemaster")
public class GetPincodeMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPincodeMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("Start " + new Date());
		PincodeMasterDevResponse res = new PincodeMasterDevResponse();
		res.setStatus(true);
		int version = 0;
		try{
			version = Integer.parseInt(request.getParameter("version"));
		}catch (Exception e){
			version = 0;
		}
		String result = "";
		ManageTransaction mt = new ManageTransaction();
		List<Pincode> pins = new ArrayList<Pincode>();
		try{
		String qry = "Select pcm from PinCodeMaster pcm where pcm.version > ?1";
		TypedQuery<PinCodeMaster> pcmQuery = mt.createQuery(PinCodeMaster.class, qry);
		pcmQuery.setParameter(1, version);
		List<PinCodeMaster> pinCodes = pcmQuery.getResultList();
		System.out.println("Size " + pinCodes.size());
		for (PinCodeMaster pincode:pinCodes){
			Pincode pin = new Pincode();
			pin.setActive(pincode.getActive() == 1?true:false);
//			pin.setActiveStatus(pincode.getActiveStatus());
			pin.setBranchCode(pincode.getId().getBranchCode());
			pin.setLocationName(pincode.getLocationName());
			pin.setPinCode(pincode.getId().getPinCode());
			pin.setServicable(pincode.getServicable());
			pin.setServiceType(pincode.getServiceType());
			pin.setVersion(pincode.getVersion());
			pins.add(pin);
//			System.out.println("Added " + pin.getPinCode());
		}
		}catch(Exception e){
			res.setStatus(false);
			res.setMessage(e.getLocalizedMessage());
			result = new Gson().toJson(res);
			response.getWriter().write(result);
		}
		finally{
			mt.close();
		}
		System.out.println("Done " );
		res.setStatus(true);
		res.setData(pins);
		result = new Gson().toJson(res);
		System.out.println("End " + new Date());
		response.getWriter().write(result);
	}

}
