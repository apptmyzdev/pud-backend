package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.UnloadingSheetsDataModal;
import com.spoton.pud.jpa.AutoPickupUnloading;

/**
 * Servlet implementation class GetUnloadingSheetsData
 */
@WebServlet("/getUnloadingSheetsData")
public class GetUnloadingSheetsData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetUnloadingSheetsData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUnloadingSheetsData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		try {
		String userName=request.getParameter("userName");
		String deviceImei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		logger.info("userName "+userName+" deviceImei "+deviceImei+" apkVersion "+apkVersion);
		
		if(CommonTasks.check(userName)) {
			mt=new ManageTransaction();
			Calendar todaysDate=Calendar.getInstance();
			todaysDate.setTime(new Date());
			todaysDate.set(Calendar.HOUR_OF_DAY, 23);
			todaysDate.set(Calendar.MINUTE, 59);
			todaysDate.set(Calendar.SECOND, 59);
			todaysDate.set(Calendar.MILLISECOND, 0);
			

			Calendar yesterday=Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			yesterday.set(Calendar.HOUR_OF_DAY, 0);
			yesterday.set(Calendar.MINUTE, 0);
			yesterday.set(Calendar.SECOND, 0);
			yesterday.set(Calendar.MILLISECOND, 0);
			String query="Select a from AutoPickupUnloading a where a.createdTimestamp>?1 and a.createdTimestamp<=?2 and a.userId=?3 and a.transactionResult=?4";
			TypedQuery<AutoPickupUnloading> tq=mt.createQuery(AutoPickupUnloading.class, query);
			tq.setParameter(1, yesterday.getTime()).setParameter(2, todaysDate.getTime()).setParameter(3, userName).setParameter(4, "true");
			List<AutoPickupUnloading> list=tq.getResultList();
			if(list!=null&&list.size()>0) {
				Map<String,UnloadingSheetsDataModal> dataList=new HashMap<String,UnloadingSheetsDataModal>();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for(AutoPickupUnloading a:list) {
					UnloadingSheetsDataModal sd=new UnloadingSheetsDataModal();
					sd.setGeneratedTime(a.getCreatedTimestamp()!=null?format.format(a.getCreatedTimestamp()):null);
					sd.setNumberOfCons(a.getTotalConsCount());
					sd.setNumberOfPieces(a.getTotalPiecesCount());
					sd.setSheetNumber(a.getPickupUnloadingSheetNumber());
					sd.setTotalWeight(a.getTotalConWeight());
					sd.setVehicleNumber(a.getVehicleNumber());
					dataList.put(a.getPickupUnloadingSheetNumber(),sd);
				}
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, dataList.values()));
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
			}
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
