package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.ReassignPickupInputModal;
import com.spoton.pud.jpa.PickupReassigned;

/**
 * Servlet implementation class GetPickupsReassignedData
 */
@WebServlet("/getPickupsReassignedData")
public class GetPickupsReassignedData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetPickupsReassignedData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPickupsReassignedData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		try {
		String userName=request.getParameter("userName");
		String deviceImei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		logger.info("userName "+userName+" deviceImei "+deviceImei+" apkVersion "+apkVersion);
		
		if(CommonTasks.check(userName)) {
			mt=new ManageTransaction();
			Calendar todaysDatetillSix=Calendar.getInstance();
			todaysDatetillSix.setTime(new Date());
			todaysDatetillSix.set(Calendar.HOUR_OF_DAY, 06);
			todaysDatetillSix.set(Calendar.MINUTE, 00);
			todaysDatetillSix.set(Calendar.SECOND, 00);
			todaysDatetillSix.set(Calendar.MILLISECOND, 0);
			
			String query="Select p from PickupReassigned p where p.userId=?1 and p.reassignDatetime>?2 and p.transactionResult=?3";
			TypedQuery<PickupReassigned> tq=mt.createQuery(PickupReassigned.class, query);
			tq.setParameter(1, userName).setParameter(2,todaysDatetillSix.getTime()).setParameter(3, "true");
			List<PickupReassigned> list=tq.getResultList();
			if(list!=null&&list.size()>0) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				List<ReassignPickupInputModal> dataList=new ArrayList<ReassignPickupInputModal>();
				for(PickupReassigned p:list) {
					ReassignPickupInputModal rp=new ReassignPickupInputModal();
					rp.setGpsValueLat(p.getGpsValueLat());
					rp.setGpsValueLon(p.getGpsValueLon());
					rp.setPickupScheduleId(p.getPickupScheduleId());
					rp.setPickupType(p.getPickupType());
					rp.setReassignDateTime(p.getReassignDatetime()!=null?format.format(p.getReassignDatetime()):null);
					rp.setRefNo(p.getRefNumber());
					rp.setRemarks(p.getRemarks());
					rp.setToUserId(p.getToUserId());
					rp.setUserId(p.getUserId());
					dataList.add(rp);
					
					
				}
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, dataList));
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
			}
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
