package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsData;
import com.spoton.pud.data.ConDetailsModal;
import com.spoton.pud.data.ConDetailsModalV2;
import com.spoton.pud.data.EwayBillNo;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupUpdateStatusModal;
import com.spoton.pud.data.PickupUpdatinResponse;
import com.spoton.pud.data.PickupUpdationData;
import com.spoton.pud.data.PickupUpdationModal;
import com.spoton.pud.data.PickupUpdationModalV2;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.PieceVolumeData;
import com.spoton.pud.data.PieceVolumeModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesImages;
import com.spoton.pud.data.PiecesImagesData;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.CurrentUpdationStatus;
import com.spoton.pud.jpa.CustomerMasterData;
import com.spoton.pud.jpa.CustomerOptimalPath;
import com.spoton.pud.jpa.DeviceInfo;
import com.spoton.pud.jpa.PickupUpdationStatus;
import com.spoton.pud.jpa.Piece;
import com.spoton.pud.jpa.PieceEntry;
import com.spoton.pud.jpa.PieceVolume;
import com.spoton.pud.jpa.PiecesImage;
import com.spoton.pud.jpa.UpdatedEwayBillNumber;
import com.spoton.pud.jpa.UserDataMaster;
import com.spoton.pud.jpa.UserLocation;

/**
 * Servlet implementation class PickupUpdationServiceV4
 */
@WebServlet("/pickupUpdationServiceV4")
public class PickupUpdationServiceV4 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pickupUpdationLogger = Logger.getLogger("PickupUpdationServiceV4");
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PickupUpdationServiceV4() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	

		pickupUpdationLogger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("pickupUpdationUrl");
		String datasent="";ManageTransaction mt=null;
		StringBuilder stringBuilder = null;int responseCode =0;int totalCons=0;int totalPieces=0;
		String customerName="";String customerAddress="";double totalWeight=0;
		double latitude=0;double longitude=0;String updatedTime="";
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		/*String userId="";String pickupScheduleId="";*/ String resultMessage="";
		String ipadd = request.getRemoteAddr();
		String imagesFlag=request.getParameter("imagesFlag");
		String status=request.getParameter("status");
		String pickupScheduleId=request.getParameter("pickupScheduleId");
		String userId=request.getParameter("userId");
		pickupUpdationLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd+" imagesFlag "+imagesFlag);
		Map<Integer,String> conMap=new HashMap<Integer, String>();int dbFlag=0; String conNumber="";
		try{
			 mt=new ManageTransaction();
			
			boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){
				if(status!=null&&Integer.parseInt(status)==4){
			datasent=request.getReader().readLine();
			//System.out.println("datasent "+datasent);
			pickupUpdationLogger.info("Datasent from Mobile "+datasent);
			Type type = new TypeToken<PickupUpdationModalV2>(){}.getType();
			PickupUpdationModalV2 updationList=gson.fromJson(datasent,type);
	    			/*System.out.println("conList "+gson.toJson(updationList));*/
	        if(updationList!=null&&updationList.getConDetails()!=null&&updationList.getConDetails().size()>0)
			{
	        	PickupUpdationData pickupUpdationData=new PickupUpdationData();
				int flag=0;
				 userId=updationList.getConDetails().get(0).getUserName();
				List<String> conNumbersList=new ArrayList<String>();
				 String query="select c.con_number,c.id from pud.con_details c where c.user_name=?1 and c.pickup_schedule_id=?2 ";
			      Query q=mt.createNativeQuery(query).setParameter(1,userId).setParameter(2, updationList.getConDetails().get(0).getPickupScheduleId());
		          List<Object []> queryList=q.getResultList();
		          List<String> cons=new ArrayList<String>();
		          Map<String,Integer> conIdMap=new HashMap<String, Integer>();
		          for(Object [] o:queryList){
		        	  cons.add((String) o[0]);
		        	  conIdMap.put((String) o[0],(int) o[1] );
		          }
		         
	    			List<ConDetailsModalV2> list1=new ArrayList<ConDetailsModalV2>();
	    			List<ConDetailsData> conDetailslList=new ArrayList<ConDetailsData>();
	    			ConDetail cd=null;ConDetail cd1=null;ConDetailsData conData=null;
	    		
                 if(imagesFlag!=null&&Integer.parseInt(imagesFlag)==0){
                
			     for(ConDetailsModalV2 conList:updationList.getConDetails())
 				{
 				 // System.out.println(conList.getConNumber()+" exists:-"+queryList.contains(conList.getConNumber()));
 				  pickupUpdationLogger.info(conList.getConNumber()+" exists:- "+cons.contains(conList.getConNumber()));
 				  if(!cons.contains(conList.getConNumber()))
 				  {
 					 totalCons++;
 				cd=new ConDetail();
 				conData=new ConDetailsData();
 				cd.setActualWeight(conList.getActualWeight());
 				totalWeight+=Double.parseDouble(conList.getActualWeight());
 				cd.setApplyDc(conList.getApplyDC());
 				cd.setApplyNfForm(conList.getApplyNfForm());
 				cd.setApplyVtv(conList.getApplyVTV());
 				cd.setConNumber(conList.getConNumber());
 				cd.setConsignmentType(conList.getConsignmentType());
 				cd.setCrmScheduleId(conList.getCrmScheduleId());
 				cd.setCustomerCode(conList.getCustomerCode());
 				cd.setCustomerName(conList.getCustomerName());
 				customerName=conList.getCustomerName();
 				cd.setDeclaredValue(conList.getDeclaredValue());
 				cd.setDestinationPincode(conList.getDestinationPinCode());
 				 cd.setErpUpdated(0);
 				cd.setGatePassTime(conList.getGatePassTime());
 			    cd.setEwayBillNumber(conList.geteWayBillNumber());
 				cd.setLatValue(conList.getLatValue());
 				cd.setLongValue(conList.getLongValue());
 				cd.setNoOfPackage(conList.getNoOfPackage());
 				totalPieces+=Integer.parseInt(conList.getNoOfPackage());
 				cd.setOrderNo(conList.getOrderNo());
 				cd.setOriginPincode(conList.getOriginPinCode());
 				cd.setPackageType(conList.getPackageType());
 				cd.setPanNo(conList.getPanNo());
 				cd.setPaymentBasis(conList.getPaymentBasis());
 				cd.setPickupScheduleId(conList.getPickupScheduleId());
 				pickupScheduleId=Integer.toString(conList.getPickupScheduleId());
 				if(conList.getPickupDate()!=null)
 				cd.setPickupDate(format.parse(conList.getPickupDate()));
 				cd.setProduct(conList.getProduct());
 				cd.setReceiverName(conList.getReceiverName());
 				cd.setReceiverPhoneNo(conList.getReceiverPhoneNo());
 				cd.setRefNumber(conList.getRefNumber());
 				cd.setRiskType(conList.getRiskType());
 				
 				
 				cd.setSpecialInstruction(conList.getSpecialInstruction());
 				cd.setTinNo(conList.getTinNo());
 				cd.setTotalVolWeight(conList.getTotalVolWeight());
 				cd.setUserId(conList.getUserId());
 				cd.setUserName(conList.getUserName());
 				userId=conList.getUserName();
 				cd.setVolType(conList.getVolType());
 				cd.setVtcAmount(conList.getVtcAmount());
 				cd.setIsScanned(conList.isScanned()==true?1:0);
 				cd.setCreatedTimestamp(new Date());
 				mt.persist(cd);
 				
 				Piece p=null;
 				if(conList.getPieces()!=null&&conList.getPieces().size()>0)
 				for(Pieces pc:conList.getPieces()){
 				p=new Piece();
 				p.setLastPieceNo(pc.getLastPieceNumber());
 				p.setPcrKey(Integer.parseInt(pc.getPcrKey()));
 				p.setTotalUsed(pc.getTotalUsed());
 				p.setConDetail(cd);
 				p.setCreatedTimestamp(new Date());
 				mt.persist(p);
 			}
 				PieceEntry pieceEntry=null;
 				
 				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
 				for(PieceEntryModal pe:conList.getPieceEntry()){
 					pieceEntry=new PieceEntry();
 					pieceEntry.setConDetail(cd);
 					pieceEntry.setFromPieceNo(pe.getFromPieceNo());
 					pieceEntry.setPcrKey(Integer.parseInt(pe.getPcrKey()));
 					pieceEntry.setToPieceNo(pe.getToPieceNo());
 					pieceEntry.setTotalPieces(pe.getTotalPieces());
 					pieceEntry.setCreatedTimestamp(new Date());
	    			mt.persist(pieceEntry);
	    				
	    				}
 				PieceVolume pieceVolume=null;
 				
 				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
 				for(PieceVolumeModal pv:conList.getPieceVolume()){
 					pieceVolume=new PieceVolume();
 					pieceVolume.setConDetail(cd);
 					pieceVolume.setNoOfPieces(pv.getNoOfPieces());
 					pieceVolume.setTotalVolWeight(pv.getTotalVolWeight());
 					pieceVolume.setVolBreadth(pv.getVolBreadth());
 					pieceVolume.setVolHeight(pv.getVolHeight());
 					pieceVolume.setVolLength(pv.getVolLength());
 					pieceVolume.setCreatedTimestamp(new Date());
	    			mt.persist(pieceVolume);
	    				
	    				}
 				
 				
 				UpdatedEwayBillNumber updatedEway=null;
			
				if(conList.geteWayBillNumberList()!=null&&conList.geteWayBillNumberList().size()>0){
					for(String s:conList.geteWayBillNumberList()){
						
						updatedEway=new UpdatedEwayBillNumber();
						updatedEway.setConDetail(cd);
						updatedEway.setCreatedTimestamp(new Date());
						updatedEway.setEwayBillNumber(s);
						mt.persist(updatedEway);
					}
				}
 				
 				mt.commit();
 				//System.out.println("cd id "+cd.getId());
 				conMap.put(cd.getId(), cd.getConNumber());
 				cd1=mt.find(ConDetail.class, cd.getId());
 				cd1.setConEntryId(cd.getId());
 				mt.persist(cd1);mt.commit();
 			   dbFlag=1;
 				//System.out.println("list1 "+gson.toJson(list1));
 			  try{
					if(conList.getCustomerCode()!=null){
					CustomerMasterData cm=mt.find(CustomerMasterData.class, conList.getCustomerCode());
					customerAddress=cm.getCustomerAddress();
					}
				}catch(Exception e){
					e.printStackTrace();
					pickupUpdationLogger.error("Exception while getting customer master data",e);
				}
				latitude=Double.parseDouble(conList.getLatValue());
				longitude=Double.parseDouble(conList.getLongValue());
				updatedTime=conList.getPickupDate();
 				}else{
 					
 					conNumber+=" "+conList.getConNumber();
 					result = gson.toJson(new GeneralResponse(Constants.FALSE,"Con Already Exists for Con Numbers"+conNumber,null));
 					resultMessage="Con Already Exists for Con Numbers"+conNumber;
 				}
 				
 				
 		}//end of for
			     if(dbFlag==1){
	 					result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",null));
	 				}
                 }else if(imagesFlag!=null&&Integer.parseInt(imagesFlag)==1){
                	
			          for(ConDetailsModalV2 conList:updationList.getConDetails())
	    				{
	    				 // System.out.println(conList.getConNumber()+" exists:-"+queryList.contains(conList.getConNumber()));
	    				    pickupUpdationLogger.info(conList.getConNumber()+" exists:- "+cons.contains(conList.getConNumber()));
	    				    conData=new ConDetailsData();
	    				    conData.setActualWeight(conList.getActualWeight());
		    				conData.setApplyDC(conList.getApplyDC());
		    				conData.setApplyNFForm(conList.getApplyNfForm());
		    				conData.setApplyVTV(conList.getApplyVTV());
		    				conData.setConNumber(conList.getConNumber());
		    				conData.setConsignmentType(conList.getConsignmentType());
		    				conData.setCRMScheduleID(conList.getCrmScheduleId());
		    				conData.setCustomerCode(conList.getCustomerCode());
		    				conData.setDeclaredValue(conList.getDeclaredValue());
		    				conData.setDestinationPINCode(conList.getDestinationPinCode());
		    				conData.setGatePassTime(conList.getGatePassTime());
		    				conData.setLatvalue(conList.getLatValue());
		    				conData.setLongvalue(conList.getLongValue());
		    				conData.setNoofPackage(conList.getNoOfPackage());
		    				conData.setOrderNo(conList.getOrderNo());
		    				conData.setOriginPINCode(conList.getOriginPinCode());
		    				conData.setPackageType(conList.getPackageType());
		    				conData.setPANNo(conList.getPanNo());
		    				conData.setPaymentbasis(conList.getPaymentBasis());
		    				//System.out.println("conList.getPickupdate() "+conList.getPickupDate());
		    				if(conList.getPickupDate()!=null)
		    				conData.setPickupdate(conList.getPickupDate());
		    				conData.setProduct(conList.getProduct());
		    				conData.setReceiverName(conList.getReceiverName());
		    				conData.setReceiverPhoneNo(conList.getReceiverPhoneNo());
		    				conData.setRefNumber(conList.getRefNumber());
		    				conData.setRiskType(conList.getRiskType());
		    				/*cd.setShipmentImageUrl(conList.getShipmentImageURL());*/
		    				
		    				conData.setSpecialInstruction(conList.getSpecialInstruction());
		    				conData.setTINNo(conList.getTinNo());
		    				conData.setTotalVolWeight(conList.getTotalVolWeight());
		    				conData.setUserID(conList.getUserId());
		    				conData.setUserName(conList.getUserName());
		    				conData.setVolType(conList.getVolType());
		    				conData.setVTCAmount(conList.getVtcAmount());
		    				conData.setEwayBillNo(conList.geteWayBillNumber());
		    				PiecesData pieceData=null;
		    				List<PiecesData> piecesList=new ArrayList<PiecesData>();
		    				if(conList.getPieces()!=null&&conList.getPieces().size()>0)
		    				for(Pieces pc:conList.getPieces()){
		    				pieceData=new PiecesData();
		    				pieceData.setLastPieceNumber(pc.getLastPieceNumber());
		    				pieceData.setPCR_Key(pc.getPcrKey());
		    				pieceData.setTotalUsed(pc.getTotalUsed());
		    				piecesList.add(pieceData);
		    				}
		    				PieceEntryData pieceEntryData=null;
		    				List<PieceEntryData> pieceEntryList=new ArrayList<PieceEntryData>();
		    				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
		    				for(PieceEntryModal pe:conList.getPieceEntry()){
		    					pieceEntryData=new PieceEntryData();
		    					pieceEntryData.setFromPieceNo(pe.getFromPieceNo());
			    				pieceEntryData.setPCR_Key(pe.getPcrKey());
			    				pieceEntryData.setToPieceNo(pe.getToPieceNo());
			    				pieceEntryData.setTotalPieces(pe.getTotalPieces());
			    				pieceEntryList.add(pieceEntryData);
			    				}
		    				PieceVolumeData pieceVolumeData=null;
		    				List<PieceVolumeData> pieceVolumeList=new ArrayList<PieceVolumeData>();
		    				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
		    				for(PieceVolumeModal pv:conList.getPieceVolume()){
		    					pieceVolumeData=new PieceVolumeData();
		    					pieceVolumeData.setNoofPieces(pv.getNoOfPieces());
			    				pieceVolumeData.setTotalVolWeight(pv.getTotalVolWeight());
			    				pieceVolumeData.setVolbreadth(pv.getVolBreadth());
			    				pieceVolumeData.setVolHeight(pv.getVolHeight());
			    				pieceVolumeData.setVolLength(pv.getVolLength());
			    				pieceVolumeList.add(pieceVolumeData);
			    				}
		    				String piecesImagesURL="";
		    				PiecesImagesData piecesImagesData=null;
		    				List<PiecesImagesData> piecesImagesDataList=new ArrayList<PiecesImagesData>();
		    				
		    				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
		    					int k=0;
		    				for(PiecesImages pi:conList.getPiecesImages()){
		    					k++;
		    					piecesImagesData=new PiecesImagesData();
		    				   piecesImagesURL=FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(pi.getPiecesImagesURL(),conList.getConNumber(),"jpg",1,"pieceImage"+Integer.toString(k));
		    				   piecesImagesData.setPiecesImagesURL(piecesImagesURL);
		    				   piecesImagesDataList.add(piecesImagesData);
		    			   }
		    				}
		    				conData.setPieces(piecesList);
		    				conData.setPieceEntry(pieceEntryList);
		    				conData.setPieceVolume(pieceVolumeList);
		    				conData.setPiecesImages(piecesImagesDataList);
	    				  if(!cons.contains(conList.getConNumber()))
	    				  {
	    					  totalCons++;
	    				cd=new ConDetail();
	    				
	    				cd.setActualWeight(conList.getActualWeight());
	    				totalWeight+=Double.parseDouble(conList.getActualWeight());
	    				cd.setApplyDc(conList.getApplyDC());
	    				cd.setApplyNfForm(conList.getApplyNfForm());
	    				cd.setApplyVtv(conList.getApplyVTV());
	    				cd.setConNumber(conList.getConNumber());
	    				cd.setConsignmentType(conList.getConsignmentType());
	    				cd.setCrmScheduleId(conList.getCrmScheduleId());
	    				cd.setCustomerCode(conList.getCustomerCode());
	    				cd.setCustomerName(conList.getCustomerName());
	    				customerName=conList.getCustomerName();
	    				cd.setDeclaredValue(conList.getDeclaredValue());
	    				cd.setDestinationPincode(conList.getDestinationPinCode());
	    				 cd.setErpUpdated(0);
	    				cd.setGatePassTime(conList.getGatePassTime());
	    				cd.setEwayBillNumber(conList.geteWayBillNumber());
	    				/*cd.setImage1Url(conList.getImage1URL());
	    				cd.setImage2Url(conList.getImage2URL());*/
	    				//System.out.println("CommonTasks.toBase64 "+CommonTasks.toBase64(conList.getImage1URL()));
	    				if(conList.getImage1URL()!=null&&(!conList.getImage1URL().isEmpty())){
	    				cd.setImage1Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage1URL(),conList.getConNumber(),"jpg",1,"Image1"));
	    				}else{cd.setImage1Url("");}
	    				if(conList.getImage2URL()!=null&&(!conList.getImage2URL().isEmpty())){
	    				cd.setImage2Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage2URL(),conList.getConNumber(),"jpg",1,"Image2"));
	    				}else{cd.setImage2Url("");}
	    				cd.setLatValue(conList.getLatValue());
	    				cd.setLongValue(conList.getLongValue());
	    				cd.setNoOfPackage(conList.getNoOfPackage());
	    				cd.setOrderNo(conList.getOrderNo());
	    				cd.setOriginPincode(conList.getOriginPinCode());
	    				cd.setPackageType(conList.getPackageType());
	    				cd.setPanNo(conList.getPanNo());
	    				cd.setPaymentBasis(conList.getPaymentBasis());
	    				cd.setPickupScheduleId(conList.getPickupScheduleId());
	    				pickupScheduleId=Integer.toString(conList.getPickupScheduleId());
	    				//System.out.println("conList.getPickupdate() "+conList.getPickupDate());
	    				if(conList.getPickupDate()!=null)
	    				cd.setPickupDate(format.parse(conList.getPickupDate()));
	    				cd.setProduct(conList.getProduct());
	    				cd.setReceiverName(conList.getReceiverName());
	    				cd.setReceiverPhoneNo(conList.getReceiverPhoneNo());
	    				cd.setRefNumber(conList.getRefNumber());
	    				cd.setRiskType(conList.getRiskType());
	    				/*cd.setShipmentImageUrl(conList.getShipmentImageURL());*/
	    				if(conList.getShipmentImageURL()!=null&&(!conList.getShipmentImageURL().isEmpty())){
	    				cd.setShipmentImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getShipmentImageURL(),conList.getConNumber(),"jpg",1,"Shipment"));
	    				}else{cd.setShipmentImageUrl("");}
	    				cd.setSpecialInstruction(conList.getSpecialInstruction());
	    				cd.setTinNo(conList.getTinNo());
	    				cd.setTotalVolWeight(conList.getTotalVolWeight());
	    				cd.setUserId(conList.getUserId());
	    				cd.setUserName(conList.getUserName());
	    				userId=conList.getUserName();
	    				cd.setVolType(conList.getVolType());
	    				cd.setVtcAmount(conList.getVtcAmount());
	    				cd.setIsScanned(conList.isScanned()==true?1:0);
	    				cd.setCreatedTimestamp(new Date());
	    				mt.persist(cd);
	    				
	    				
	    				
	    				
	    				
	    				Piece p=null;
	    				
	    				if(conList.getPieces()!=null&&conList.getPieces().size()>0)
	    				for(Pieces pc:conList.getPieces()){
	    				p=new Piece();
	    				p.setLastPieceNo(pc.getLastPieceNumber());
	    				p.setPcrKey(Integer.parseInt(pc.getPcrKey()));
	    				p.setTotalUsed(pc.getTotalUsed());
	    				p.setConDetail(cd);
	    				p.setCreatedTimestamp(new Date());
	    				mt.persist(p);
	    				}
	    				PieceEntry pieceEntry=null;
	    				
	    				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
	    				for(PieceEntryModal pe:conList.getPieceEntry()){
	    					pieceEntry=new PieceEntry();
	    					
	    					pieceEntry.setConDetail(cd);
	    					pieceEntry.setFromPieceNo(pe.getFromPieceNo());
	    					pieceEntry.setPcrKey(Integer.parseInt(pe.getPcrKey()));
	    					pieceEntry.setToPieceNo(pe.getToPieceNo());
	    					pieceEntry.setTotalPieces(pe.getTotalPieces());
	    					pieceEntry.setCreatedTimestamp(new Date());
		    				mt.persist(pieceEntry);
		    				}
	    				PieceVolume pieceVolume=null;
	    				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
	    				for(PieceVolumeModal pv:conList.getPieceVolume()){
	    					pieceVolume=new PieceVolume();
	    					pieceVolume.setConDetail(cd);
	    					pieceVolume.setNoOfPieces(pv.getNoOfPieces());
	    					pieceVolume.setTotalVolWeight(pv.getTotalVolWeight());
	    					pieceVolume.setVolBreadth(pv.getVolBreadth());
	    					pieceVolume.setVolHeight(pv.getVolHeight());
	    					pieceVolume.setVolLength(pv.getVolLength());
	    					pieceVolume.setCreatedTimestamp(new Date());
		    				mt.persist(pieceVolume);
		    				
		    				}
	    				
	    				List<PiecesImages> piecesImagesList=new ArrayList<PiecesImages>();
	    				
	    				
	    				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
	    					int k=0;
	    				for(PiecesImages pi:conList.getPiecesImages()){
	    					k++;
	    				   PiecesImage pieceImage=new PiecesImage();
	    				   pieceImage.setConDetail(cd);
	    				   pieceImage.setCreatedTimestamp(new Date());
	    				   piecesImagesURL=FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(pi.getPiecesImagesURL(),conList.getConNumber(),"jpg",1,"pieceImage"+Integer.toString(k));
	    				   pieceImage.setPiecesImagesUrl(piecesImagesURL);
	    				   piecesImagesList.add(pi);
	    				   mt.persist(pieceImage);
	    				}
	    				}
	    				
	    				
	    				EwayBillNo ewayNumber=null;UpdatedEwayBillNumber updatedEway=null;
	    				List<EwayBillNo> ewayList=new ArrayList<EwayBillNo>();
	    				if(conList.geteWayBillNumberList()!=null&&conList.geteWayBillNumberList().size()>0){
	    					for(String s:conList.geteWayBillNumberList()){
	    						ewayNumber=new EwayBillNo();
	    						ewayNumber.setEwayBillNum(s);
	    						ewayList.add(ewayNumber);
	    						updatedEway=new UpdatedEwayBillNumber();
	    						updatedEway.setConDetail(cd);
	    						updatedEway.setCreatedTimestamp(new Date());
	    						updatedEway.setEwayBillNumber(s);
	    						mt.persist(updatedEway);
	    					}
	    				}
	    				mt.commit();
	    				//System.out.println("cd id "+cd.getId());
	    				conList.setConEntryId(cd.getId());
	    				conList.setImage1URL(cd.getImage1Url());
	    				conList.setImage2URL(cd.getImage2Url());
	    				conList.setShipmentImageURL(cd.getShipmentImageUrl());
	    				conList.setPiecesImages(piecesImagesList);
	    				conList.seteWayBillNumber(conList.geteWayBillNumber());
	    				conData.setEwayBillNoList(ewayList);
	    				
	    				cd1=mt.find(ConDetail.class, cd.getId());
	    				cd1.setConEntryId(cd.getId());
	    				mt.persist(cd1);mt.commit();
	    			     list1.add(conList);
	    			     dbFlag=1;
	    			     try{
		    					if(conList.getCustomerCode()!=null){
		    					CustomerMasterData cm=mt.find(CustomerMasterData.class, conList.getCustomerCode());
		    					customerAddress=cm.getCustomerAddress();
		    					}
		    				}catch(Exception e){
		    					e.printStackTrace();
		    					pickupUpdationLogger.error("Exception while getting customer master data",e);
		    				}
		    				latitude=Double.parseDouble(conList.getLatValue());
		    				longitude=Double.parseDouble(conList.getLongValue());
		    				updatedTime=conList.getPickupDate();
	    				//System.out.println("list1 "+gson.toJson(list1));
	    				}else{
	    					System.out.println("in else");
	    					cd=mt.find(ConDetail.class, conIdMap.get(conList.getConNumber()));
	    					if(cd.getImage1Url()==null){
	    					if(conList.getImage1URL()!=null&&(!conList.getImage1URL().isEmpty())){
	    	    				cd.setImage1Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage1URL(),conList.getConNumber(),"jpg",1,"Image1"));
	    	    				}else{cd.setImage1Url("");}	
	    					}
	    					if(cd.getImage2Url()==null){
	    					if(conList.getImage2URL()!=null&&(!conList.getImage2URL().isEmpty())){
	    	    				cd.setImage2Url(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getImage2URL(),conList.getConNumber(),"jpg",1,"Image2"));
	    	    				}else{cd.setImage2Url("");}
	    					}
	    					if(cd.getShipmentImageUrl()==null){
	    					if(conList.getShipmentImageURL()!=null&&(!conList.getShipmentImageURL().isEmpty())){
	    	    				cd.setShipmentImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(conList.getShipmentImageURL(),conList.getConNumber(),"jpg",1,"Shipment"));
	    	    				}else{cd.setShipmentImageUrl("");}
	    					}
	    					mt.persist(cd);
		    				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
		    					int k=0;
		    				for(PiecesImages pi:conList.getPiecesImages()){
		    				k++;
		    				   PiecesImage pieceImage=new PiecesImage();
		    				   pieceImage.setConDetail(cd);
		    				   pieceImage.setCreatedTimestamp(new Date());
		    				   piecesImagesURL=FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(pi.getPiecesImagesURL(),conList.getConNumber(),"jpg",1,"pieceImage"+Integer.toString(k));
		    				   pieceImage.setPiecesImagesUrl(piecesImagesURL);
		    				   mt.persist(pieceImage);
		    				 
		    			   }
		    				}
	    					
	    					mt.commit();
	    					
	    				}
	    				  conMap.put(cd.getId(), cd.getConNumber());
	    				    conData.setConEntryID(cd.getId());
		    				conData.setImage1URL(cd.getImage1Url());
		    				conData.setImage2URL(cd.getImage2Url());
		    				conData.setShipmentImageURL(cd.getShipmentImageUrl());
		    				
		    				conDetailslList.add(conData);
	    					
	    				
	    				
	    		}//end of for
			          pickupUpdationData.setConDetails(conDetailslList);
	    				//System.out.println("DataSent to spoton "+gson.toJson(pickupUpdationData));
	    				pickupUpdationLogger.info("Spoton URL Called"+urlString);
	    				pickupUpdationLogger.info("DataSent to spoton "+gson.toJson(pickupUpdationData));
	    				System.out.println("DataSent to spoton "+gson.toJson(pickupUpdationData));
	    				try{
		    				obj = new URL(urlString);
		    				con = (HttpURLConnection) obj.openConnection();
		    				con.setConnectTimeout(5000);
		    				con.setReadTimeout(10000);
		    				con.setRequestMethod("POST");
		    				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
		    				con.setRequestProperty("Accept", "application/json");
		    				con.setDoOutput(true);
		    				
		    				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
		    				  streamWriter.write(gson.toJson(pickupUpdationData));
		    				  streamWriter.flush();
		    				  
		    				  pickupUpdationLogger.info("Response Code : " +con.getResponseCode());
		    		            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		    		            	stringBuilder=new StringBuilder();
		    		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		    		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		    		                String response1 = null;
		    		                while ((response1 = bufferedReader.readLine()) != null) {
		    		                    stringBuilder.append(response1);
		    		                }
		    		                bufferedReader.close();
		    		               // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
		    		                pickupUpdationLogger.info("Response From Spoton : " +stringBuilder.toString());
		    		                responseCode= con.getResponseCode();
		    		                PickupUpdatinResponse responseList=gson.fromJson(stringBuilder.toString(),PickupUpdatinResponse.class);
		    	    				
		    	    				//System.out.println("responseList "+gson.toJson(responseList));
		    	    				pickupUpdationLogger.info("ResponseList "+gson.toJson(responseList));
		    	    				if(responseList!=null&&responseList.getPickUpUpdateStatus()!=null&&responseList.getPickUpUpdateStatus().size()>0){
		    	    					PickupUpdationStatus ps=null;
		    	    				List<String> conNumbers=new ArrayList<String>();
		    	    					for(PickupUpdateStatusModal pm:responseList.getPickUpUpdateStatus()){
		    	    						ps=new PickupUpdationStatus();
		    	    						ps.setConEntryId(pm.getConEntryId());
		    	    						  ConDetail c=mt.find(ConDetail.class, pm.getConEntryId());
		    	    						  c.setErpUpdated(1);
		    	    						ps.setTransactionResult(pm.getTransactionResult());
		    	    						ps.setCreatedTimestamp(new Date());
		    	    						if(pm.getTransactionResult().equalsIgnoreCase("Con Already Exists")){
		    	    							conNumbers.add(conMap.get(pm.getConEntryId()));
		    	    						}
		    	    						if(((pm.getTransactionResult().equalsIgnoreCase("Success")))){
		    	    							flag=1;
		    	    							result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",responseList));
		    	    							resultMessage="Success";
		    	    						}
		    	    						mt.persist(c);
		    	    						mt.persist(ps);
		    	    						mt.commit();
		    	    					}
		    	    					
		    	    					if(result.length()==0&&conNumbers!=null&&conNumbers.size()>0){
		    	    						String message="";
		    	    						for(String c:conNumbers){
		    	    							message=message+" "+c;
		    	    							//System.out.println("message "+message);
		    	    						}
		    	    						message="Con Already Exists for Con Numbers "+message;
		    	    						result = gson.toJson(new GeneralResponse(Constants.FALSE,message,null));
		    	    						resultMessage=message;
		    	    					}else if(flag==0){
		    	    						result = gson.toJson(new GeneralResponse(Constants.FALSE,"Pickup Updation Failed", null));
		    	    						resultMessage="Pickup Updation Failed";
		    	    					}
		    	    					else{
		    	    						result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",responseList));
		    	    						resultMessage="Success";
		    	    					}
		    	    					
		    	    				}else{
		    	    					result = gson.toJson(new GeneralResponse(Constants.FALSE,"Pickup Updation Failed", null));
		    	    					resultMessage="Pickup Updation Failed";
		    	    				}
		    		    			
		    		    		}else{
		    		            	
		    		            //System.out.println("Response failed");
		    		            pickupUpdationLogger.info("Response failed from spoton");
		    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		    		            resultMessage="Response Failed From ERP";
		    		            }
		    				
		    			  }catch (SocketTimeoutException e){		
		    					e.printStackTrace();
		    					
		    					pickupUpdationLogger.error("SocketTimeoutException From ERP StackTrace",e);
		    					
		    					if(dbFlag==0){
		    						result = gson.toJson(new GeneralResponse(Constants.FALSE,"Con Already Exists for Con Numbers"+conNumber,null));
		    						resultMessage="SocketTimeoutException but Sent response as-Con Already Exists for Con Numbers"+conNumber;
		    					}else if(dbFlag==1){
		    						result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",null));
		    					resultMessage="SocketTimeoutException but Sent response as-Success";
		    					}
		    				}catch(Exception e){
		    					
		    					e.printStackTrace();
		    					pickupUpdationLogger.error("EXCEPTION_IN_SERVER At erp code",e);
		    					result = gson.toJson(new GeneralResponse(Constants.FALSE,
		    							Constants.ERRORS_EXCEPTION_IN_SERVER,null));
		    					resultMessage="EXCEPTION_IN_SERVER "+e;
		    				}
                	 
                 }
                 if(dbFlag==1){
                     try{
     					Calendar todaysDate=Calendar.getInstance();
     					todaysDate.setTime(new Date());
     					int lastRouteVersion=CommonTasks.getLastRouteVersion(mt,userId,pickupUpdationLogger);
     					if(lastRouteVersion!=0){
    						String routeQuery="Select c from CustomerOptimalPath c where c.username=?1 and c.routeVersion=?6 "
    						 		+ "and c.pickupScheduleId=?5 "
    						 		+ "and FUNC('DAY', c.createdTimestamp)=?2 and FUNC('MONTH', c.createdTimestamp)=?3 and FUNC('YEAR', c.createdTimestamp)=?4 ";
    						TypedQuery<CustomerOptimalPath> tq3=mt.createQuery(CustomerOptimalPath.class, routeQuery).setParameter(1, userId);
    						tq3.setParameter(6, lastRouteVersion);
    						tq3.setParameter(5,pickupScheduleId);
    						tq3.setParameter(2, todaysDate.getTime().getDate());tq3.setParameter(3, todaysDate.getTime().getMonth()+1);tq3.setParameter(4, todaysDate.get(Calendar.YEAR));
    						List<CustomerOptimalPath> routeList=tq3.getResultList();
    					   //System.out.println("routeList size "+routeList.size());
    						if(routeList!=null&&routeList.size()>0){
    							pickupUpdationLogger.info("routeList.size "+routeList.size());
    					    	for(CustomerOptimalPath r:routeList){
    					    		r.setActiveFlag(0);
    					    		r.setUpdatedTimestamp(new Date());
    					    		mt.persist(r);
    					    		mt.commit();
    					    		
    					    	}
    					    }
     					}
     					}catch(Exception e){
     						 e.printStackTrace();
     						 pickupUpdationLogger.error("Exception while setting active flag=0 in optimal route ",e);
     					 }
     					
     					try{
     						UserLocation ul=new UserLocation();
     						ul.setPdcOrPickupScheduleId(pickupScheduleId);
     						ul.setNoOfCons(totalCons);
     						ul.setNoOfPieces(totalPieces);
     						ul.setCustomerName(customerName);
     						ul.setCustomerAddress(customerAddress);
     						ul.setTotalWeight(totalWeight);
     						ul.setLatitude(latitude);
     						ul.setLongitude(longitude);
     						ul.setUpdatedTime(updatedTime);
     						ul.setFlag(2);
     						ul.setTimestamp(new Date());
     						if(updatedTime!="")
     						ul.setDeviceTimestamp(format.parse(updatedTime));
     						String deviceInfoQuery="Select d from DeviceInfo d where d.userId=?1 order by d.deviceId desc";
     						TypedQuery<DeviceInfo> dq=mt.createQuery(DeviceInfo.class, deviceInfoQuery).setParameter(1, userId);
     						DeviceInfo deviceInfo=dq.setMaxResults(1).getSingleResult();
     						  if(deviceInfo!=null){
     							  ul.setDeviceInfo(deviceInfo);
     						  }
     						  String userDataMasterQuery="Select u from UserDataMaster u where u.appUserName=?1 order by u.id desc";
     						  TypedQuery<UserDataMaster> uq=mt.createQuery(UserDataMaster.class, userDataMasterQuery).setParameter(1, userId);
     						  UserDataMaster userDataMasterId=uq.setMaxResults(1).getSingleResult();
    	    						  if(userDataMasterId!=null){
    	    							  ul.setUserDataMaster(userDataMasterId);
    	    						  }
    	    						  mt.persist(ul);
    	    						  mt.commit();
     					}catch(Exception e){
     						 e.printStackTrace();
     						 pickupUpdationLogger.error("Exception while saving data in user location",e);
     					 }	
                     }	
                 
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
				resultMessage="Incomplete data sent to the server";
			}
			
			}else{
				CurrentUpdationStatus cs=new CurrentUpdationStatus();
				cs.setPickupScheduleId_conNumber(pickupScheduleId);
				cs.setType(2);
				cs.setApkVersion(apkVersion);
				cs.setDeviceImei(deviceIMEI);
				cs.setUsername(userId);
				cs.setTimestamp(new Date());
				if(status!=null&&Integer.parseInt(status)==0)
				{
					cs.setStatus("pending");
				}else if(status!=null&&Integer.parseInt(status)==1)
				{
					cs.setStatus("in progress");
				}
				mt.persist(cs);
				mt.commit();
				result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",null));
			}
	}else{
		result = gson.toJson(new GeneralResponse(Constants.FALSE,
				Constants.UPDATE_TO_LATEST_VERSION, null));
	}		
			
}catch (SocketTimeoutException e){		
	e.printStackTrace();
	pickupUpdationLogger.error("SocketTimeoutException from device StackTrace ",e);
	response.setStatus(503);
	response.sendError(503, "SocketTimeoutException");
	pickupUpdationLogger.info("SocketTimeoutException from device with response  code "+e);
	
	resultMessage="SocketTimeoutException from device with response  code ";
	
}catch(Exception e){
	
	e.printStackTrace();
	pickupUpdationLogger.error("EXCEPTION_IN_SERVER", e);
	result = gson.toJson(new GeneralResponse(Constants.FALSE,
			Constants.ERRORS_EXCEPTION_IN_SERVER,null));
	resultMessage="EXCEPTION_IN_SERVER "+e;
}finally{
	if(mt!=null){mt.close();}
	
	if(con!=null){con.disconnect();}
}

//System.out.println("PickupUpdationService result"+result);
boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Pickup Updation V2", conMap.values(),userId,pickupScheduleId,ipadd,resultMessage,null);
pickupUpdationLogger.info("auditlogStatus "+auditlogStatus);
pickupUpdationLogger.info("PickupUpdationServiceV2 result"+result);
pickupUpdationLogger.info("**************END*******************");

response.getWriter().write(result);
	
	}

}
