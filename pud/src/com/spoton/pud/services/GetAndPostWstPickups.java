package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModalV2;
import com.spoton.pud.data.ConDetailsModalV3;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupUpdateStatusModal;
import com.spoton.pud.data.PickupUpdatinResponse;
import com.spoton.pud.data.PickupUpdationModalV2;
import com.spoton.pud.data.PickupUpdationModalV3;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.PieceVolumeModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesImages;
import com.spoton.pud.data.WSTPiecesRefListModal;
import com.spoton.pud.data.WstPickupUpdationDataList;
import com.spoton.pud.data.WstPickupUpdationErpModal;
import com.spoton.pud.data.WstPickupUpdationModal;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.PickupUpdationStatus;
import com.spoton.pud.jpa.WstPickupsUpdatedData;

/**
 * Servlet implementation class GetAndPostWstPickups
 */
@WebServlet("/getAndPostWstPickups")
public class GetAndPostWstPickups extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetAndPostWstPickups");
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAndPostWstPickups() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    
   // *********************************Wst Pickups don't have order number and Pickup Schedule id.ConNumber is Unique***************
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String userName= request.getParameter("userName");
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion+" userName "+userName);
        Calendar todayDate=Calendar.getInstance();
		
		Calendar tomorrowDate=Calendar.getInstance();
		if(new Date().getHours()>6) {
		
		todayDate.setTime(new Date());
		todayDate.set(Calendar.HOUR_OF_DAY, 00);
		todayDate.set(Calendar.MINUTE, 00);
		todayDate.set(Calendar.SECOND, 00);
		todayDate.set(Calendar.MILLISECOND, 0);
		
		tomorrowDate.add(Calendar.DATE, +1);
		tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
		tomorrowDate.set(Calendar.MINUTE, 00);
		tomorrowDate.set(Calendar.SECOND, 00);
		tomorrowDate.set(Calendar.MILLISECOND, 0);
		
		//pullPudDataLogger.info("todayDate "+todayDate.getTime()+" tomorrowDate "+tomorrowDate.getTime());
		
		}else { // pickups are valid till next day 6 am .Before 6 am consider one day back pickups 
			todayDate.add(Calendar.DATE, -1);
			todayDate.set(Calendar.HOUR_OF_DAY, 00);
			todayDate.set(Calendar.MINUTE, 00);
			todayDate.set(Calendar.SECOND, 00);
			todayDate.set(Calendar.MILLISECOND, 0);
			
			tomorrowDate.setTime(new Date());
			tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
			tomorrowDate.set(Calendar.MINUTE, 00);
			tomorrowDate.set(Calendar.SECOND, 00);
			tomorrowDate.set(Calendar.MILLISECOND, 0);
		}
		try{
			if(CommonTasks.check(userName)){
				mt=new ManageTransaction();
				//To get Piece Entry data
				/*String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id from piece_entry pe,con_details c "
						+ "where pe.con_id=c.id and c.pickup_type=5 and c.user_name=?1 and date(c.pickup_date)=curdate()";*/ //removing joins
				String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id,pe.id from piece_entry pe "
						+ "where  pe.pickup_type=5 and pe.user_name=?1 and pe.pickup_date>=?2 and pe.pickup_date<=?3";
				Query pieceEntryQuery=mt.createNativeQuery(pieceEntry).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
				List<Object[]> pieceEntryList=pieceEntryQuery.getResultList();
				PieceEntryModal pieceEntryData=null;
				List<PieceEntryModal> pieceEntryDataList=null;
				Map<Integer,List<PieceEntryModal>> pieceEntryMap=new HashMap<Integer, List<PieceEntryModal>>();
				for(Object[] o:pieceEntryList){
					pieceEntryData=new PieceEntryModal();
					pieceEntryData.setPcrKey(Integer.toString((int) o[0]));
					pieceEntryData.setFromPieceNo((String) o[1]);
					pieceEntryData.setToPieceNo((String) o[2]);
					pieceEntryData.setTotalPieces((String) o[3]);
					pieceEntryData.setId((int) o[5]);
					if(pieceEntryMap.containsKey((int) o[4])){
						pieceEntryDataList=pieceEntryMap.get((int) o[4]);
						pieceEntryDataList.add(pieceEntryData);
					}else{
						pieceEntryDataList=new ArrayList<PieceEntryModal>();
						pieceEntryDataList.add(pieceEntryData);
						pieceEntryMap.put((int) o[4], pieceEntryDataList);
					}
					
					
				}
				
				//To get Pieces data
				/*String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id "
						+ "from pieces pe,con_details c where pe.con_id=c.id and c.pickup_type=5 and c.user_name=?1 and date(c.pickup_date)=curdate()";*/	//removing joins
				String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id,pe.id "
						+ "from pieces pe where pe.pickup_type=5 and pe.user_name=?1 and pe.pickup_date>=?2 and pe.pickup_date<=?3 ";
				Query piecesQuery=mt.createNativeQuery(pieces).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
				List<Object[]> piecesList=piecesQuery.getResultList();
				Pieces piecesData=null;
				List<Pieces> piecesDataList=null;
				Map<Integer,List<Pieces>> piecesDataMap=new HashMap<Integer, List<Pieces>>();
				for(Object[] o:piecesList){
					piecesData=new Pieces();
					piecesData.setPcrKey(Integer.toString((int) o[0]));
					piecesData.setLastPieceNumber((String) o[1]);
					piecesData.setTotalUsed((String) o[2]);
					piecesData.setId((int) o[4]);
					if(piecesDataMap.containsKey((int) o[3])){
						piecesDataList=piecesDataMap.get((int) o[3]);
						piecesDataList.add(piecesData);
						
					}else{
						piecesDataList=new ArrayList<Pieces>();
						piecesDataList.add(piecesData);
						piecesDataMap.put((int) o[3], piecesDataList);
						
					}
					
				}
				
				//To get Piece Volume data
				/*String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id,pe.id "
						+ "from piece_volume pe,con_details c where pe.con_id=c.id and c.pickup_type=5 and c.user_name=?1 and date(c.pickup_date)=curdate()";*/	//removing joins
				String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id,pe.id "
						+ "from piece_volume pe where  pe.pickup_type=5 and pe.user_name=?1 and pe.pickup_date>=?2 and pe.pickup_date<=?3 ";
				Query pieceVolumeQuery=mt.createNativeQuery(pieceVolume).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
				List<Object[]> pieceVolumeList=pieceVolumeQuery.getResultList();
				PieceVolumeModal pieceVolumeData=null;
				List<PieceVolumeModal> pieceVolumeDataList=null;
				Map<Integer,List<PieceVolumeModal>> pieceVolumeDataMap=new HashMap<Integer, List<PieceVolumeModal>>();
				for(Object[] o:pieceVolumeList){
					pieceVolumeData=new PieceVolumeModal();
					pieceVolumeData.setNoOfPieces((int) o[0]);
					pieceVolumeData.setTotalVolWeight((double) o[1]);
					pieceVolumeData.setVolBreadth((double) o[2]);
					pieceVolumeData.setVolHeight((double) o[3]);
					pieceVolumeData.setVolLength((double) o[4]);
					pieceVolumeData.setId((int) o[6]);
					if(pieceVolumeDataMap.containsKey((int) o[5])){
						pieceVolumeDataList=pieceVolumeDataMap.get((int) o[5]);
						pieceVolumeDataList.add(pieceVolumeData);
					}else{
						pieceVolumeDataList=new ArrayList<PieceVolumeModal>();
					    pieceVolumeDataList.add(pieceVolumeData);
						pieceVolumeDataMap.put((int) o[5], pieceVolumeDataList);
					}
					
				}
				
				//To get Piece Images data
				/*String pieceImage="select pe.pieces_images_url,pe.con_id from pieces_images pe,con_details c "
						+ "where pe.con_id=c.id and c.pickup_type=5 and c.user_name=?1 and date(c.pickup_date)=curdate()";*/ //removing joins
				String pieceImage="select pe.pieces_images_url,pe.con_id,pe.id from pieces_images pe "
						+ "where pe.pickup_type=5 and pe.user_name=?1 and pe.pickup_date>=?2 and pe.pickup_date<=?3";
				
				Query pieceImageQuery=mt.createNativeQuery(pieceImage).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
				List<Object[]> pieceImageList=pieceImageQuery.getResultList();
				PiecesImages pieceImageData=null;
				List<PiecesImages> pieceImagesDataList=null;
				Map<Integer,List<PiecesImages>> pieceImagesDataMap=new HashMap<Integer, List<PiecesImages>>();
				for(Object[] o:pieceImageList){
					pieceImageData=new PiecesImages();
					pieceImageData.setPiecesImagesURL((String) o[0]);
					pieceImageData.setId((int) o[2]);
					if(pieceImagesDataMap.containsKey((int) o[1])){
						pieceImagesDataList=pieceImagesDataMap.get((int) o[1]);
						pieceImagesDataList.add(pieceImageData);
					}else{
						pieceImagesDataList=new ArrayList<PiecesImages>();
						pieceImagesDataList.add(pieceImageData);
						pieceImagesDataMap.put((int) o[1], pieceImagesDataList);
					}
					
				}
				
				
				//To get wst pieces
				/*String wstPieces="select w.con_number,w.piece_number,w.piece_ref_number,w.piece_serial_number,w.con_id from wst_pieces_ref_list w,con_details c "
						+ "where w.con_id=c.id and c.pickup_type=5 and c.user_name=?1 and date(c.pickup_date)=curdate()";*/ //removing joins
				
				String wstPieces="select w.con_number,w.piece_number,w.piece_ref_number,w.piece_serial_number,w.con_id,w.id from wst_pieces_ref_list w "
						+ "where w.pickup_type=5 and w.user_name=?1 and w.pickup_date>=?2 and w.pickup_date<=?3";
				Query wstPiecesQuery=mt.createNativeQuery(wstPieces).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
				List<Object[]> wstPiecesList=wstPiecesQuery.getResultList();
				WSTPiecesRefListModal wstPiecesData=null;
				List<WSTPiecesRefListModal> wstPiecesDataList=null;
				Map<Integer,List<WSTPiecesRefListModal>> wstPiecesDataMap=new HashMap<Integer, List<WSTPiecesRefListModal>>();
				for(Object[] o:wstPiecesList){
					wstPiecesData=new WSTPiecesRefListModal();
					wstPiecesData.setConNumber((String) o[0]);
					wstPiecesData.setPieceNo((String) o[1]);
					wstPiecesData.setPieceRefNo((String)o[2]);
					wstPiecesData.setPieceSlNo((String) o[3]);
					wstPiecesData.setId((int) o[5]);
					if(wstPiecesDataMap.containsKey((int) o[4])) {
						wstPiecesDataList=wstPiecesDataMap.get((int)o[4]);
						wstPiecesDataList.add(wstPiecesData);
					}else {
						wstPiecesDataList=new ArrayList<WSTPiecesRefListModal>();
						wstPiecesDataList.add(wstPiecesData);
						wstPiecesDataMap.put((int) o[4], wstPiecesDataList);
						
					}
					
					
				}
				
				
				
				/*//To get eway bill numbers data
				 String addedEwayBillNumbers="select pe.eway_bill_number,pe.con_id from updated_eway_bill_numbers pe,con_details c "
							+ "where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1";
					Query addedEwayBillNumbersQuery=mt.createNativeQuery(addedEwayBillNumbers).setParameter(1, userName);
					List<Object[]> ewayNumbersList=addedEwayBillNumbersQuery.getResultList();
					List<String> ewaysList=null;
					Map<Integer,List<String>> ewayNumbersDataMap=new HashMap<Integer, List<String>>();
					for(Object[] o:ewayNumbersList){
						
						if(ewayNumbersDataMap.containsKey((int) o[1])){
							ewaysList=ewayNumbersDataMap.get((int) o[1]);
							ewaysList.add((String)o[0]);
						}else{
							ewaysList=new ArrayList<String>();
							ewaysList.add((String)o[0]);
							ewayNumbersDataMap.put((int) o[1], ewaysList);
						}
					}*/
				//Map<String,PickupUpdationModalV3> pickupsMap=new HashMap<String, PickupUpdationModalV3>();
					String conQuery="Select c.con_entry_id,c.con_number,c.ref_number,c.pickup_date,c.product,c.origin_pincode,c.destination_pincode,"
							+ "c.actual_weight,c.apply_dc,c.apply_nf_form,c.apply_vtv,c.consignment_type,c.crm_schedule_id,c.customer_code,"
							+ "c.declared_value,c.gate_pass_time,c.image1_url,c.image2_url,c.lat_value,"
							+ "c.long_value,c.no_of_package,c.order_no,c.package_type,c.pan_no,c.payment_basis,c.receiver_name,c.receiver_phone_no,"
							+ "c.risk_type,c.shipment_image_url,c.special_instruction,c.tin_no,c.total_vol_weight,c.user_id,c.user_name,c.vol_type,c.vtc_amount,c.id,c.customer_name,c.vehicle_number,c.product_type "
							+ "from con_details c where c.pickup_type=5 and c.user_name=?1 and c.pickup_date>=?2 and c.pickup_date<=?3";
					Query query1=mt.createNativeQuery(conQuery).setParameter(1, userName).setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
					List<Object[]> consList=query1.getResultList();
					if(consList!=null&&consList.size()>0){
						logger.info("consList size "+consList.size());
						List<ConDetailsModalV3> conDetailslList=new ArrayList<ConDetailsModalV3>();
						
						for(Object[] o:consList){
							ConDetailsModalV3 conData=new ConDetailsModalV3();
							  conData.setConEntryId((int) o[36]);
							  conData.setConNumber( o[1]!=null?(String) o[1]:"");
							  conData.setRefNumber(o[2]!=null?(String) o[2]:"");
							  if(o[3]!=null)
				    		    conData.setPickupDate(format.format((Date) o[3]));
								conData.setProduct(o[4]!=null?(String) o[4]:"");
								conData.setOriginPinCode(o[5]!=null?(String) o[5]:"");
								conData.setDestinationPinCode(o[6]!=null?(String) o[6]:"");
							    conData.setActualWeight(o[7]!=null?(String) o[7]:"");
			    				conData.setApplyDC(o[8]!=null?(String) o[8]:"");
			    				conData.setApplyNfForm((String) o[9]);
			    				conData.setApplyVTV((String) o[10]);
			    			    conData.setConsignmentType((String) o[11]);
			    				conData.setCrmScheduleId((String) o[12]);
			    				conData.setCustomerCode((String) o[13]);
			    				conData.setDeclaredValue((String) o[14]);
			    				conData.setGatePassTime((String) o[15]);
			    				conData.setImage1URL((String) o[16]);
			    				conData.setImage2URL((String) o[17]);
			    				conData.setLatValue((String) o[18]);
			    				conData.setLongValue((String) o[19]);
			    				conData.setNoOfPackage((String) o[20]);
			    				conData.setOrderNo((String) o[21]);
			    			    conData.setPackageType((String) o[22]);
			    				conData.setPanNo((String) o[23]);
			    				conData.setPaymentBasis((String) o[24]);
			    				conData.setReceiverName((String) o[25]);
			    				conData.setReceiverPhoneNo((String) o[26]);
			    				conData.setRiskType((String) o[27]);
			    				conData.setShipmentImageURL((String) o[28]);
			    				conData.setSpecialInstruction((String) o[29]);
			    				conData.setTinNo((String) o[30]);
			    				conData.setTotalVolWeight((String) o[31]);
			    				conData.setUserId((String) o[32]);
			    				conData.setUserName((String) o[33]);
			    				conData.setVolType((String) o[34]);
			    				conData.setVtcAmount((String) o[35]);
			    				conData.setCustomerName((String) o[37]);
			    				conData.setVehicleNumber((String) o[38]);
			    				conData.setProductType((String) o[39]);
			    				if(pieceEntryMap.get(conData.getConEntryId())!=null)
			    				conData.setPieceEntry(pieceEntryMap.get(conData.getConEntryId()));
			    				
			    				if(piecesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPieces(piecesDataMap.get(conData.getConEntryId()));
			    				
			    				if(pieceVolumeDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPieceVolume(pieceVolumeDataMap.get(conData.getConEntryId()));
			    				
			    				if(pieceImagesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPiecesImages(pieceImagesDataMap.get(conData.getConEntryId()));
			    				
			    				if(wstPiecesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setWstPiecesRefList(wstPiecesDataMap.get(conData.getConEntryId()));
			    				conDetailslList.add(conData);
			    				/*if(ewayNumbersDataMap.get(conData.getConEntryId())!=null)
			    					conData.seteWayBillNumberList(ewayNumbersDataMap.get(conData.getConEntryId()));*/
			    				/*if(pickupsMap.containsKey(conData.getCrmScheduleId())){
			    					PickupUpdationModalV3 pickupData=pickupsMap.get(conData.getCrmScheduleId());
			    					conDetailslList=pickupData.getConDetails();
			    					conDetailslList.add(conData);
			    				}else{
			    					PickupUpdationModalV3 pickupData=new PickupUpdationModalV3();
			    					conDetailslList=new ArrayList<ConDetailsModalV3>();
			    					conDetailslList.add(conData);
			    					pickupData.setConDetails(conDetailslList);
				    				pickupsMap.put(conData.getCrmScheduleId(),pickupData);
			    				}*/
			    				
						}// end of main for
					
						 result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, conDetailslList));
					
					}else{
						result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
					}
					
			}else{
			 result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null)
				mt.close();
		}
		//System.out.println("result "+result);
		logger.info("result "+result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("downloadWstPickup");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion);
		try{
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			WstPickupUpdationModal updationList=gson.fromJson(datasent,WstPickupUpdationModal.class);
			 if(updationList!=null&&updationList.getWstPickupUpdationData()!=null&&updationList.getWstPickupUpdationData().size()>0)
				{
				 mt=new ManageTransaction();
				 ConDetail cd=mt.find(ConDetail.class, updationList.getConEntryId());
				 cd.setPickupType(6);
				 mt.persist(cd);
				 mt.commit();
				 List<WstPickupUpdationErpModal> erpDataList=new ArrayList<WstPickupUpdationErpModal>();
				 for(WstPickupUpdationDataList w:updationList.getWstPickupUpdationData()) {
					 WstPickupUpdationErpModal erpData=new WstPickupUpdationErpModal();
					 erpData.setConNumber(w.getConNumber());
					 erpData.setPieceNo(w.getPieceNo());
					 erpData.setIsManual(w.getIsManual());
					 erpData.setIsPrinted(w.getIsPrinted());
					 if(w.getPrintedDateTime()!=null)
					 erpData.setPrintedDateTime(format.parse(w.getPrintedDateTime()));
					 erpData.setUserID(w.getUserId());
					 erpDataList.add(erpData);
					 WstPickupsUpdatedData updatedData=new WstPickupsUpdatedData();
					 updatedData.setConDetail(cd);
					 updatedData.setConNumber(w.getConNumber());
					 updatedData.setPieceNumber(w.getPieceNo());
					 updatedData.setCreatedTimestamp(new Date());
					 updatedData.setIsManual(w.getIsManual());
					 updatedData.setIsPrinted(w.getIsPrinted());
					 if(w.getPrintedDateTime()!=null)
					 updatedData.setPrintedDateTime(format.parse(w.getPrintedDateTime()));
					 updatedData.setUserId(w.getUserId());
					 mt.persist(updatedData);
					 mt.commit();
				 }
				 logger.info("DataSent to spoton "+gson.toJson(erpDataList));
					try{
						obj = new URL(urlString);
						con = (HttpURLConnection) obj.openConnection();
						con.setConnectTimeout(5000);
						con.setReadTimeout(60000);
						con.setRequestMethod("POST");
						con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
						con.setRequestProperty("Accept", "application/json");
						con.setDoOutput(true);
						
						OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
						  streamWriter.write(gson.toJson(erpDataList));
						  streamWriter.flush();
						  streamWriter.close();
						  logger.info("Response Code : " +con.getResponseCode());
				            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
				            	stringBuilder=new StringBuilder();
				                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
				                BufferedReader bufferedReader = new BufferedReader(streamReader);
				                String response1 = null;
				                while ((response1 = bufferedReader.readLine()) != null) {
				                    stringBuilder.append(response1);
				                }
				                bufferedReader.close();
				                streamReader.close();
				               // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
				                logger.info("Response From Spoton : " +stringBuilder.toString());
				              
				                PickupUpdatinResponse responseList=gson.fromJson(stringBuilder.toString(),PickupUpdatinResponse.class);
				                if(responseList!=null&&responseList.getPickUpUpdateStatus()!=null&&responseList.getPickUpUpdateStatus().size()>0){
			    					PickupUpdationStatus ps=null;
			    					for(PickupUpdateStatusModal pm:responseList.getPickUpUpdateStatus()){
			    						ps=new PickupUpdationStatus();
			    						ConDetail c=mt.find(ConDetail.class, pm.getConEntryId());
			  						    c.setErpUpdated(1);
			    						ps.setConEntryId(pm.getConEntryId());
			    						ps.setTransactionResult(pm.getTransactionResult());
			    						ps.setCreatedTimestamp(new Date());	
			    						mt.persist(c);mt.persist(ps);
			    						mt.commit();
			    					}
			    					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,responseList));
				                }else{
		    		            logger.info("No Response from spoton");
		    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Response from spoton", null));
		    		          }
			    				
				               
				            }else{
		    		            logger.info("Response failed from spoton");
		    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		    		          }
				            }catch (SocketTimeoutException e){		
		    					e.printStackTrace();
		    					logger.error("SocketTimeoutException  From ERP",e);
		    					
		    					result = gson.toJson(new GeneralResponse(Constants.FALSE,"SocketTimeoutException From ERP", null));
				            }catch(Exception e){
				    			e.printStackTrace();
				    			logger.error("Exception e ",e);
				    			result = gson.toJson(new GeneralResponse(Constants.FALSE,
				    					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				    		}
				}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null)
				mt.close();
			if(con!=null) {
				con.disconnect();
			}
		}
		logger.info("result "+result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	}

}
