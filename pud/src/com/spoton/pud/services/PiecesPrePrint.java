package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConPieceData;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesPrePrintErpModal;
import com.spoton.pud.data.PiecesPrePrintModal;
import com.spoton.pud.jpa.PieceEntry;

/**
 * Servlet implementation class PiecesPrePrint
 */
@WebServlet("/piecesPrePrint")
public class PiecesPrePrint extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PiecesPrePrint");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PiecesPrePrint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("piecesPrePrintUrl");
		StringBuilder stringBuilder = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion);
		try{
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			PiecesPrePrintModal inputData=gson.fromJson(datasent, PiecesPrePrintModal.class);
			if(inputData!=null) {
				PiecesPrePrintErpModal erpData=new PiecesPrePrintErpModal();
				erpData.setConNo(inputData.getConNumber());
				erpData.setDestination(inputData.getDestination());
				erpData.setDestinationPincode(inputData.getDestinationPincode());
				erpData.setOrigin(inputData.getOrigin());
				erpData.setOriginPincode(inputData.getOriginPincode());
				erpData.setUserID(inputData.getUserId());
				if(inputData.getPieces()!=null&&inputData.getPieces().size()>0) {
					List<PiecesData> piecesList=new ArrayList<PiecesData>();
					for(Pieces p:inputData.getPieces()) {
						PiecesData pd=new PiecesData();
						pd.setLastPieceNumber(p.getLastPieceNumber());
						pd.setPCR_Key(p.getPcrKey());
						pd.setTotalUsed(p.getTotalUsed());
						piecesList.add(pd);
					}
					erpData.setPieces(piecesList);
				}
				if(inputData.getPieceEntry()!=null&&inputData.getPieceEntry().size()>0) {
					List<PieceEntryData> pieceEntryDataList=new ArrayList<PieceEntryData>();
					for(PieceEntryModal p:inputData.getPieceEntry()) {
						PieceEntryData pm=new PieceEntryData();
						pm.setFromPieceNo(p.getFromPieceNo());
						pm.setPCR_Key(p.getPcrKey());
						pm.setToPieceNo(p.getToPieceNo());
						pm.setTotalPieces(p.getTotalPieces());
						pieceEntryDataList.add(pm);
					}
					erpData.setPieceEntry(pieceEntryDataList);
				}
				
                 logger.info("DataSent to spoton "+gson.toJson(erpData));
				
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				con.setConnectTimeout(5000);
    			  con.setReadTimeout(60000);
				  OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(erpData));
				  streamWriter.flush();
				  streamWriter.close();
				
				  logger.info("Response Code : " + con.getResponseCode());
				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		                logger.info("Response From Spoton"+stringBuilder.toString());
		                (stringBuilder.toString()).replace("", "");
		                if(stringBuilder.toString().substring(1,8).equalsIgnoreCase("Success")) {
		                result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
		                }else {
		                	  result = gson.toJson(new GeneralResponse(Constants.FALSE,stringBuilder.toString(),null));
		                }
				  }else{
			           
			            logger.info("Response From Spoton Failed");
			            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
			           
			            }
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null)
				mt.close();
			if(con!=null) {
				con.disconnect();
			}
		}
		logger.info("result "+result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
