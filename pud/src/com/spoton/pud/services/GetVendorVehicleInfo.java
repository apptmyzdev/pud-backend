package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.VendorInfoModal;
import com.spoton.pud.jpa.VendorVehicleInfo;

/**
 * Servlet implementation class VendorVehicleInfo
 */
@WebServlet("/getVendorVehicleInfo")
public class GetVendorVehicleInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetVendorVehicleInfo");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetVendorVehicleInfo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		ManageTransaction mt = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
        String vendorId = request.getParameter("vendorId");
        String location =request.getParameter("location");
		String imei = request.getHeader("imei");
		String apkVersion = request.getHeader("apkVersion");
		logger.info("imei:-" + imei + "apkVersion:-" + apkVersion);
		logger.info("vendorId:-" + vendorId+" location:-" + location);
		HttpURLConnection con=null; URL obj =null;
		try {
			if (CommonTasks.check(vendorId, location)) {
				
				mt=new ManageTransaction();
				String urlString=FilesUtil.getProperty("vendorVehicleInfo");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("VendorCode",vendorId);
				jsonObj.put("Branch",location);
				logger.info("DataSent to ERP"+jsonObj.toJSONString());
				//System.out.println("jsonObj.toJSONString() "+jsonObj.toJSONString());
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				  OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(jsonObj.toJSONString());
				  streamWriter.flush();
				  streamWriter.close();
				  StringBuilder stringBuilder = null;
				  // set the connection timeout to 5 seconds and the read timeout to 10 seconds
				
					 logger.info("Response Code : "+ con.getResponseCode());
		            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		              // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
		                logger.info("Response from ERP : "+stringBuilder.toString());
		               
		                Type vehicalInfoType = new TypeToken<ArrayList<VendorInfoModal>>(){}.getType();
		    			List<VendorInfoModal> vehicalInfoList=gson.fromJson(stringBuilder.toString(), vehicalInfoType);
		    			if(vehicalInfoList!=null&&vehicalInfoList.size()>0){
		    				List<VendorVehicleInfo> list=new ArrayList<VendorVehicleInfo>();
		    				for(VendorInfoModal v:vehicalInfoList){
		    				VendorVehicleInfo vi=new VendorVehicleInfo();
		    				vi.setBacctName(v.getBacctName());
		    				vi.setBaCode(v.getBACode());
		    				vi.setBranch(location);
		    				vi.setVehicleId(v.getVehicleId());
		    				vi.setVehicleNo(v.getVehicleNo());
		    				vi.setVehicleType(v.getVehicleType());
		    				vi.setVendorCode(vendorId);
		    				mt.persist(vi);
		    				mt.commit();
		    				list.add(vi);
		    				}
		    				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,list));
		    			}else{
		    				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
		    			}
		               
		    		}else{
		            	logger.info("Response failed from server");
		            	//System.out.println("Response failed");
		            	 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
		            	
		            }
			} else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_INCOMPLETE_DATA, null));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			logger.info("EXCEPTION_IN_SERVER " + e);
		} finally {
			if (mt != null)
				mt.close();
			if(con!=null) {
				con.disconnect();
			}
		}
		//System.out.println(" VendorVehicleInfo Result " + result);
		logger.info(" VendorVehicleInfo Result " + result);
		logger.info("********END**********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
