package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.data.GeneralResponse;

import com.spoton.pud.data.ManifestListErpModal;
import com.spoton.pud.data.ManifestListModal;

/**
 * Servlet implementation class GetManifestList
 */
@WebServlet("/getManifestList")
public class GetManifestList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetManifestList");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetManifestList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		try {
			String imei = request.getHeader("imei");
			String apkVersion= request.getHeader("apkVersion");
			String userName=request.getParameter("userName");
			String fromDate=request.getParameter("fromDate");
			String toDate=request.getParameter("toDate");
			logger.info("userName "+userName+" fromDate "+fromDate+" toDate "+toDate);
			logger.info(" imei "+imei+" apkVersion "+apkVersion);
			if(CommonTasks.check(userName,fromDate,toDate)) {
				
				String urlString=FilesUtil.getProperty("getManifestList");
				StringBuilder stringBuilder = null;
				JSONObject erpDataToPost = new JSONObject();
				erpDataToPost.put("UserID",userName);
				erpDataToPost.put("FromDate",fromDate);
				erpDataToPost.put("ToDate",toDate);
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				
				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(erpDataToPost.toJSONString());
				  streamWriter.flush();
				  streamWriter.close();
				  logger.info("Datasent to spoton "+erpDataToPost.toJSONString());
				  logger.info("Response Code : " +con.getResponseCode());
				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		                logger.info("Response from server : "+stringBuilder.toString());
		                Type data = new TypeToken<ArrayList<ManifestListErpModal>>(){}.getType();
		    			List<ManifestListErpModal> erpDataList =  gson.fromJson(stringBuilder.toString(),data);
		    			if(erpDataList!=null&&erpDataList.size()>0) {
		    				List<ManifestListModal> dataList=new ArrayList<ManifestListModal>();
		    				for(ManifestListErpModal d:erpDataList) {
		    					ManifestListModal md=new ManifestListModal();
		    					md.setCollectedAmount(d.getCollectedAmount());
		    					md.setCollectionType(d.getCollectionType());
		    					md.setConNo(d.getConNo());
		    					md.setCustomerName(d.getCustomerName());
		    					md.setManifestDate(d.getManifestDate());
		    					md.setManifestNo(d.getManifestNo());
		    					dataList.add(md);
		    				}
		    				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, dataList));
		    			}else {
		    				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
		    			}
				  }else {
					    logger.info("Response failed from Spoton");
					    result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response failed from Spoton with Response code as "+con.getResponseCode(), null));
				 }
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(con!=null) {
				con.disconnect();
			}
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
