package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.MobileDataDownloadResult;
import com.spoton.pud.data.MobileDataDownloadResultList;
import com.spoton.pud.data.MobileDataDownloaded;
import com.spoton.pud.data.MobileDataDownloadedList;
import com.spoton.pud.data.PulledPickupsModal;
import com.spoton.pud.jpa.PickupRegistration;
import com.spoton.pud.jpa.PudData;
import com.spoton.pud.jpa.PulledDataAcknowledgement;

/**
 * Servlet implementation class PulledPickupsAcknowledgement
 */
@WebServlet("/pulledPickupsAcknowledgement")
public class PulledPickupsAcknowledgement extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pulledPickupsLogger = Logger.getLogger("PulledPickupsAcknowledgement");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PulledPickupsAcknowledgement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pulledPickupsLogger.info("********START*********");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		String datasent=request.getReader().readLine();
		pulledPickupsLogger.info("Datasent from Mobile "+datasent);
		String result = "";PulledPickupsModal pp=null;int flag=0;
		  HttpURLConnection con = null;URL obj = null;StringBuilder stringBuilder = null;
		  String urlString = FilesUtil.getProperty("downloadedApplicationData");
		 
		try{
			if(datasent!=null&&datasent!=""){
				pp=gson.fromJson(datasent, PulledPickupsModal.class);
				//pulledPickupsLogger.info("parsed json of datasent  "+gson.toJson(pp));
			}
		if(pp!=null&&CommonTasks.check(pp.getUserId())&&pp.getPickupScheduleIds()!=null&&pp.getPickupScheduleIds().size()>0){
			mt=new ManageTransaction();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar currentDate=Calendar.getInstance();
			currentDate.setTime(new Date());
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 0);
			
			 Calendar yesterday=Calendar.getInstance();
				yesterday.add(Calendar.DATE, -1);
				yesterday.set(Calendar.HOUR_OF_DAY, 00);
				yesterday.set(Calendar.MINUTE, 00);
				yesterday.set(Calendar.SECOND, 00);
				yesterday.set(Calendar.MILLISECOND, 0);
			String query="select id,pickup_schedule_id from pud.pud_data where user_id=?1 and created_timestamp between ?3 and ?4 and pulled_time is NULL";
			Query nq=mt.createNativeQuery(query).setParameter(1, pp.getUserId());
			nq.setParameter(3, yesterday.getTime());nq.setParameter(4,currentDate.getTime());
			@SuppressWarnings("unchecked")
			List<Object[]> list=nq.getResultList();
			List<MobileDataDownloaded> mobDataDownload=null;
			MobileDataDownloadedList downloadedData=new MobileDataDownloadedList();
		
			if(list!=null&&list.size()>0){
				mobDataDownload=new ArrayList<MobileDataDownloaded>();
				for(Object[] o:list){
					for(Integer s:pp.getPickupScheduleIds()){
						if(s.equals((int) o[1])){
							PudData pd=mt.find(PudData.class, (int) o[0]);
							pd.setPulledTime(new Date());
							mt.persist(pd);
							mt.commit();
							/*PulledDataAcknowledgement pu=new PulledDataAcknowledgement();
							pu.setCreatedTimestamp(new Date());
							pu.setDownloadDate(pd.getPulledTime());
							pu.setDownloadType(2);
							pu.setTransactionId((int) o[0]);
							pu.setIs_registered_pickup(0);
							mt.persist(pu);
							mt.commit();*/
							flag=1;
							MobileDataDownloaded md=new MobileDataDownloaded();
						    md.setTransactionId(Integer.toString((int) o[0]));
							md.setDownloadType("2");
							md.setDownloadDate(format.format(pd.getPulledTime()));
							mobDataDownload.add(md);
						 }
					}
						}
					}        if(mobDataDownload!=null&&mobDataDownload.size()>0){
						
					          try{
							downloadedData.setMobDataDownload(mobDataDownload);
							pulledPickupsLogger.info("urlString   "+urlString);
							pulledPickupsLogger.info("DataSent to Spoton "+gson.toJson(downloadedData));
							obj = new URL(urlString);
				    		con = (HttpURLConnection) obj.openConnection();
				    		con.setRequestMethod("POST");
				    		con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				    		con.setDoOutput(true);
				    		con.setConnectTimeout(5000);
				    		con.setReadTimeout(60000);
				    		OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				    		  streamWriter.write(gson.toJson(downloadedData));
				    		  streamWriter.flush();
				    		  streamWriter.close();
				    		  pulledPickupsLogger.info("Response Code "+con.getResponseCode());
				    		  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
					              	stringBuilder=new StringBuilder();
					                  InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
					                  BufferedReader bufferedReader = new BufferedReader(streamReader);
					                  String response1 = null;
					                  while ((response1 = bufferedReader.readLine()) != null) {
					                      stringBuilder.append(response1 + "\n");
					                  }
					                  bufferedReader.close();
					                  streamReader.close();
					                  //Response received from spoton and saving in db
					                 // System.out.println("Data Received From Spoton "+stringBuilder.toString());
					                  pulledPickupsLogger.info("Data Received From Spoton "+stringBuilder.toString());
					                  MobileDataDownloadResultList downloadResultList=gson.fromJson(stringBuilder.toString(), MobileDataDownloadResultList.class);
					                  //pulledPickupsLogger.info("downloadResultList "+gson.toJson(downloadResultList));
					                  if(downloadResultList!=null&&downloadResultList.getMobDataDownloadResult()!=null&&downloadResultList.getMobDataDownloadResult().size()>0){
					                	
					                	  for(MobileDataDownloadResult r:downloadResultList.getMobDataDownloadResult()){
					                		  PulledDataAcknowledgement p=new PulledDataAcknowledgement();
					                			PudData pd=mt.find(PudData.class,r.getTransactionId());
					                		  p.setCreatedTimestamp(new Date());
					                		  p.setDownloadDate(pd.getPulledTime());
					                		  p.setDownloadType(2);
					                		  p.setTransactionId(pd.getId());
					                		  p.setResultRemarks(r.getResultRemarks());
					                		  p.setResultTransactionId(r.getTransactionId());
					                		  p.setResultFlag(r.isResultFlag()?1:0);
					                		 
					                		  mt.persist(p);
					      					  mt.commit();
					      					
					                			 
					                	  }
						}
					else{
						pulledPickupsLogger.info("Null Response From ERP");
	                  }
					}else{
		              	
						pulledPickupsLogger.info("Response failed from ERP");
		           }
					}catch (SocketTimeoutException e){		
									e.printStackTrace();
									pulledPickupsLogger.info("SocketTimeoutException "+e);
						 }
						
					}else{
						pulledPickupsLogger.info("No New data Found to be Sent to Spoton");
					}
					
			String query2="select id,pickup_schedule_id from pud.pickup_registration where mobile_user_id=?1 and pulled_time is NULL";
			Query nq2=mt.createNativeQuery(query2).setParameter(1, pp.getUserId());
			@SuppressWarnings("unchecked")
			List<Object[]> list2=nq2.getResultList();
			
			if(list2!=null&&list2.size()>0){
				
				for(Object[] o:list2){
					for(Integer s:pp.getPickupScheduleIds()){
						if(s.equals((int) o[1])){
							PickupRegistration pr=mt.find(PickupRegistration.class,(int) o[0]);
							pr.setPulledTime(new Date());
							mt.persist(pr);
							mt.commit();
							/*PulledDataAcknowledgement pu2=new PulledDataAcknowledgement();
							pu2.setCreatedTimestamp(new Date());
							pu2.setDownloadDate(new Date());
							pu2.setDownloadType(2);
							pu2.setTransactionId((int) o[0]);
							pu2.setIs_registered_pickup(1);
							mt.persist(pu2);
							mt.commit();*/
							flag=1;
							
						}
					}
					}
				}
			 if(list!=null&&list.size()==0&&list2!=null&&list2.size()==0){
					result = gson.toJson(new GeneralResponse(Constants.FALSE,"No pickups found", null));
			} else if(flag!=0){
			   result = gson.toJson(new GeneralResponse(Constants.TRUE,
						Constants.REQUEST_COMPLETED, null));
			}else{
				 result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Data is Already Downloaded in Application", null));
			}
			
			
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		}catch(Exception e){
		e.printStackTrace();
		pulledPickupsLogger.error(" EXCEPTION IN SERVER",e);
		result = gson.toJson(new GeneralResponse(
				Errors.status, 
				Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
				null));
	}finally{
		if(mt!=null)
		mt.close();
		if(con!=null)
			con.disconnect();
	}
		//System.out.println("PulledPickupsAcknowledgement result"+result);
		pulledPickupsLogger.info("PulledPickupsAcknowledgement result"+result);
		pulledPickupsLogger.info("********END*********");
		response.getWriter().write(result);
	}

}
