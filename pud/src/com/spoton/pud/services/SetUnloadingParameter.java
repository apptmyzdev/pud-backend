package com.spoton.pud.services;

import java.io.IOException;
import java.util.List;


import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.AutoPickupUnloading;

/**
 * Servlet implementation class SetUnloadingParameter
 */
@WebServlet("/setUnloadingParameter")
public class SetUnloadingParameter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SetUnloadingParameter");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SetUnloadingParameter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		try {
		String sheetNumber=request.getParameter("sheetNumber");
		String scCode = request.getParameter("scCode");
		logger.info("sheetNumber "+sheetNumber+" scCode "+scCode);
		if(CommonTasks.check(sheetNumber,scCode)) {
			mt=new ManageTransaction();
			//String query="Select transaction_id,is_unloading_started from auto_pickup_unloading where pickup_unloading_sheet_number=?1 and user_id like ?2";
			String query="Select transaction_id,is_unloading_started from auto_pickup_unloading where  vehicle_number=(select vehicle_number from auto_pickup_unloading where pickup_unloading_sheet_number=?1 and user_id like ?2 ) and user_id like ?2";
			Query q=mt.createNativeQuery(query).setParameter(1, sheetNumber).setParameter(2, scCode+"%");
			List< Object[]> data= q.getResultList();
			if(data!=null&&data.size()>0) {
				for(Object[] o:data) {
			  AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, (int) o[0]);
			  au.setIsUnloadingStarted(1);
			  mt.persist(au);
				}
				 mt.commit();
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"Sheet Not Found", null));
			}
			 
			/*String query="update auto_pickup_unloading set is_unloading_started=1 where pickup_unloading_sheet_number=?1 and  user_id like ?2 and transaction_id!=0;";
		    Query q=mt.createNativeQuery(query).setParameter(1, sheetNumber).setParameter(2, scCode+"%");
		   mt.begin();
		    q.executeUpdate();
		    mt.commit();*/
		    result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,null));
		    
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null)
				mt.close();
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
