package com.spoton.pud.services;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.connmgr.ManageTransaction;

/**
 * Servlet implementation class AgentLogoutService
 */
@WebServlet("/agentLogoutService")
public class AgentLogoutService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger agentLogoutLogger = Logger.getLogger("AgentLogoutService");
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AgentLogoutService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		agentLogoutLogger.info("*********START*********");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		String result = "";
		String sessionid = request.getHeader("sessionId");
		String devid = request.getParameter("deviceId");
		String deviceIMEI = request.getHeader("imei");
		String devicemac=request.getParameter("devicemac");
		String apkVersion= request.getHeader("apkVersion");
		String ipadd = request.getRemoteAddr();String resultMessage="";
		agentLogoutLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd+" devicemac "+devicemac);
		agentLogoutLogger.info("sessionid :"+ sessionid+" devid "+devid);
		int deviceId = 0;
		//System.out.println("devid"+devid);
		if (devid != null && devid.trim().length() > 0){
			deviceId = Integer.parseInt(devid);
			//agentLogoutLogger.info("deviceId :"+ deviceId);
		}
		String userId = request.getParameter("userName");
		agentLogoutLogger.info("Logging out :"+ userId);
		HttpSession session = request.getSession(true);
		ManageTransaction manageTransaction = null;
		String query = "";
		String query1 = "";
		if (session != null) {
			try {
				manageTransaction = new ManageTransaction();
				//query = "update user_session_data set session_id = ?1 , updated_timestamp = ?2, active_flag = 0 where session_id = ?3 ";
				if(deviceIMEI!=null) {
				  query = "update user_session_data set updated_timestamp = ?2, active_flag = 0 where user_name = ?3 and imei=?1 and active_flag=1";
				}else {
					 query = "update user_session_data set updated_timestamp = ?2, active_flag = 0 where user_name = ?3 and device_mac=?1 and active_flag=1";
				}
				  Query nativeQuery = manageTransaction.createNativeQuery(query);
				//nativeQuery.setParameter(1, session.getId());
				nativeQuery.setParameter(1, deviceIMEI!=null?deviceIMEI:devicemac);
				nativeQuery.setParameter(2, new Date());
				nativeQuery.setParameter(3,userId);
				manageTransaction.begin();
				nativeQuery.executeUpdate();
				if (deviceId > 0){
					query1 = "update device_info set active_flag = 0 , updated_timestamp = ?1 where device_id = ?2 and user_id = ?3 and active_flag=1";
					Query nativeQuery1 = manageTransaction.createNativeQuery(query1);
					nativeQuery1.setParameter(1, new Date());
					nativeQuery1.setParameter(2, deviceId);
					nativeQuery1.setParameter(3, userId);
					nativeQuery1.executeUpdate();
				}
				manageTransaction.commit();
				session.invalidate();
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,null));
				resultMessage=Constants.SUCCESSFUL;
				//agentLogoutLogger.info("response sent as :"+result);
			}
			catch (Exception e)
			{
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
				agentLogoutLogger.error("exception in server :",e);
			} finally
			{
				manageTransaction.close();
			}
		} else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INVAILD_SESSION, null));
			resultMessage=Constants.ERROR_INVAILD_SESSION;
		}
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Logout", null,userId,null,ipadd,resultMessage,null);
		agentLogoutLogger.info("auditlogStatus "+auditlogStatus);
		response.getWriter().write(result);
		agentLogoutLogger.info("response sent :"+result);
		agentLogoutLogger.info("*************END*****************");
	}

}
