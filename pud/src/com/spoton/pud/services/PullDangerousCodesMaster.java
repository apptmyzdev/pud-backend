package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DangerousCodesMasterModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.DangerousCodeMaster;

/**
 * Servlet implementation class PullDangerousCodesMaster
 */
@WebServlet("/pullDangerousCodesMaster")
public class PullDangerousCodesMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PullDangerousCodesMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullDangerousCodesMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		try {
			mt = new ManageTransaction();
			String dangerousCodesMaster="select d from DangerousCodeMaster d";
			TypedQuery<DangerousCodeMaster> dangerousCodesMasterQuery=mt.createQuery(DangerousCodeMaster.class, dangerousCodesMaster);
			List<DangerousCodeMaster> dangerousCodesList=dangerousCodesMasterQuery.getResultList();
			if(dangerousCodesList!=null&&dangerousCodesList.size()>0) {
				List<DangerousCodesMasterModal> dangerousCodesDataList=new ArrayList<DangerousCodesMasterModal>();
				for(DangerousCodeMaster d:dangerousCodesList) {
					DangerousCodesMasterModal m=new DangerousCodesMasterModal();
					m.setDangeorusCode(d.getDangerousCode());
					m.setDangeorusDesc(d.getDangerousDesc());
					m.setDangeorusId(d.getDangerousId());
					dangerousCodesDataList.add(m);
				}
				result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED,dangerousCodesDataList));
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_NO_DATA_AVAILABLE, null));
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if (mt != null) {
				mt.close();
			}
		}
		
		logger.info("result " + result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
