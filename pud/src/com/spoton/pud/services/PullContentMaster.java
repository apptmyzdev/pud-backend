package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.AirContentMasterModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.AirContentMaster;

/**
 * Servlet implementation class PullContentMaster
 */
@WebServlet("/pullContentMaster")
public class PullContentMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PullContentMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PullContentMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		try {
			mt = new ManageTransaction();
			String airContentQry="Select a from AirContentMaster a";
			TypedQuery<AirContentMaster> tq1=mt.createQuery(AirContentMaster.class, airContentQry);
			List<AirContentMaster> airContentList=tq1.getResultList();
			if(airContentList!=null&&airContentList.size()>0) {
				List<AirContentMasterModal> airContentDataList=new ArrayList<AirContentMasterModal>();
				for(AirContentMaster ac:airContentList) {
					AirContentMasterModal airContentData=new AirContentMasterModal();
					airContentData.setDangerousDescription(ac.getDangerousDescription());
					airContentData.setDangerousId(ac.getDangerousId());
					airContentDataList.add(airContentData);
				}
					result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED,airContentDataList));
				}else {
					result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_NO_DATA_AVAILABLE, null));
				}
				
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if (mt != null) {
				mt.close();
			}
		}
		
		logger.info("result " + result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
