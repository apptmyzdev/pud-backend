package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;

import com.spoton.pud.data.Covid19SurveyDataModal;
import com.spoton.pud.data.Covid19SurveyErpModal;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class Covid19Survey
 */
@WebServlet("/covid19Survey")
public class Covid19Survey extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("Covid19Survey");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Covid19Survey() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType(Constants.CONTENT_TYPE_JSON);

		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con = null;
		URL obj = null;
		try {
			String id = request.getParameter("id");
			String mode = request.getParameter("mode");
			String deviceImei = request.getHeader("imei");
			String apkVersion = request.getHeader("apkVersion");
			logger.info("id " + id + " mode " + mode + " deviceImei " + deviceImei + " apkVersion " + apkVersion);
			SimpleDateFormat df = new SimpleDateFormat("M/dd/yyyy h:mm:ss a");
			String curdate = df.format(new Date());
			List<Covid19SurveyDataModal> dataList = new ArrayList<Covid19SurveyDataModal>();
			if (CommonTasks.check(id, mode)) {

				try {
//					JSONObject jsonObj = new JSONObject();
//
//					jsonObj.put("ID", id);
//					jsonObj.put("MODE", mode);
//					String urlString = FilesUtil.getProperty("covid19Survey");
//					logger.info("url " + urlString);
//					obj = new URL(urlString);
//					con = (HttpURLConnection) obj.openConnection();
//					con.setRequestMethod("POST");
//					con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
//					con.setRequestProperty("Accept", "application/json");
//					con.setDoOutput(true);
//
//					OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
//					streamWriter.write(jsonObj.toJSONString());
//					streamWriter.flush();
//					streamWriter.close();
//					StringBuilder stringBuilder = null;
//					// set the connection timeout to 5 seconds and the read timeout to 10 seconds
//					con.setConnectTimeout(5000);
//					con.setReadTimeout(60000);
//					logger.info("Datasent to spoton " + jsonObj.toJSONString());
//					logger.info("Response Code : " + con.getResponseCode());
//					if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
//						stringBuilder = new StringBuilder();
//						InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
//						BufferedReader bufferedReader = new BufferedReader(streamReader);
//						String response1 = null;
//						while ((response1 = bufferedReader.readLine()) != null) {
//							stringBuilder.append(response1 + "\n");
//						}
//						bufferedReader.close();
//						streamReader.close();
//						logger.info("Response from server : " + stringBuilder.toString());
//						Type data = new TypeToken<ArrayList<Covid19SurveyErpModal>>() {
//						}.getType();
//						List<Covid19SurveyErpModal> erpDataList = gson.fromJson(stringBuilder.toString(), data);
//
//						if (erpDataList != null && erpDataList.size() > 0) {
////			    				List<Covid19SurveyDataModal> dataList=new ArrayList<Covid19SurveyDataModal>();
//							for (Covid19SurveyErpModal d : erpDataList) {
//								Covid19SurveyDataModal cd = new Covid19SurveyDataModal();
//								cd.setAge(d.getAge());
//								cd.setDesignation(d.getDesignation());
//								cd.setGender(d.getGender());
//								cd.setId(d.getID());
//								cd.setName(d.getName());
//								/*
//								 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
//								 * getSurveyDate());
//								 */
//								cd.setSurveyDate(d.getSurveyDate());
//								cd.setTemperature(d.getTemperature());
//								dataList.add(cd);
//							}
//							result = gson
//									.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
//						} else {
//							Covid19SurveyDataModal fmodel = new Covid19SurveyDataModal();
//							fmodel.setAge("30");
//							fmodel.setDesignation("PUD Owner");
//							fmodel.setGender("Male");
//							fmodel.setId(id);
//							fmodel.setName(id);
//							/*
//							 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
//							 * getSurveyDate());
//							 */
//							fmodel.setSurveyDate(curdate);
//							fmodel.setTemperature("96^F- 98.6^F - Normal");
//							dataList.add(fmodel);
//							result = gson
//									.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
//						}
//
//					} else {
//
//						logger.info("Response failed from server");
//						// System.out.println("Response failed");
////			            	 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
//						Covid19SurveyDataModal fmodel = new Covid19SurveyDataModal();
//						fmodel.setAge("30");
//						fmodel.setDesignation("PUD Owner");
//						fmodel.setGender("Male");
//						fmodel.setId(id);
//						fmodel.setName(id);
//						/*
//						 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
//						 * getSurveyDate());
//						 */
//						fmodel.setSurveyDate(curdate);
//						fmodel.setTemperature("96^F- 98.6^F - Normal");
//						dataList.add(fmodel);
//						result = gson
//								.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
//					}
					
					Covid19SurveyDataModal fmodel = new Covid19SurveyDataModal();
					fmodel.setAge("30");
					fmodel.setDesignation("PUD Owner");
					fmodel.setGender("Male");
					fmodel.setId(id);
					fmodel.setName(id);
					/*
					 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
					 * getSurveyDate());
					 */
					fmodel.setSurveyDate(curdate);
					fmodel.setTemperature("96^F- 98.6^F - Normal");
					dataList.add(fmodel);
					result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
				} catch (Exception e) {
					logger.error("Exception ", e);
					e.printStackTrace();
					Covid19SurveyDataModal fmodel = new Covid19SurveyDataModal();
					fmodel.setAge("30");
					fmodel.setDesignation("PUD Owner");
					fmodel.setGender("Male");
					fmodel.setId(id);
					fmodel.setName(id);
					/*
					 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
					 * getSurveyDate());
					 */
					fmodel.setSurveyDate(curdate);
					fmodel.setTemperature("96^F- 98.6^F - Normal");
					dataList.add(fmodel);
					result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
				}

			}

			else {
				Covid19SurveyDataModal fmodel = new Covid19SurveyDataModal();
				fmodel.setAge("30");
				fmodel.setDesignation("PUD Owner");
				fmodel.setGender("Male");
				fmodel.setId(id);
				fmodel.setName(id);
				/*
				 * cd.setSurveyDate(mode.equalsIgnoreCase("PUD")?"4/19/2020 06:00:00 PM":d.
				 * getSurveyDate());
				 */
				fmodel.setSurveyDate(curdate);
				fmodel.setTemperature("96^F- 98.6^F - Normal");
				dataList.add(fmodel);
				result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, dataList));
//				result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_INCOMPLETE_DATA, null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		} finally {
			if (con != null)
				con.disconnect();

		}
		response.getWriter().write(result);
		logger.info("Result " + result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
