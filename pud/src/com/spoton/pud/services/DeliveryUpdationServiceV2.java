package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModal;
import com.spoton.pud.data.DeliveryUpdationData;
import com.spoton.pud.data.DeliveryUpdationDataList;
import com.spoton.pud.data.DeliveryUpdationModal;
import com.spoton.pud.data.DeliveryUpdationModalList;
import com.spoton.pud.data.DeliveryUpdationResponse;
import com.spoton.pud.data.DeliveryUpdationResponseList;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.CurrentUpdationStatus;
import com.spoton.pud.jpa.DeliveryUpdation;
import com.spoton.pud.jpa.DeliveryUpdationStatus;

/**
 * Servlet implementation class DeliveryUpdationServiceV2
 */
@WebServlet("/deliveryUpdationServiceV2")
public class DeliveryUpdationServiceV2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger deliveryUpdationLogger = Logger.getLogger("DeliveryUpdationServiceV2");
	Logger logger = Logger.getLogger("PullingDeliveriesDataToPortal");
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeliveryUpdationServiceV2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("********GET METHOD START**********");
		//content type must be set to text/event-stream
		response.setContentType("text/event-stream");	
		String userId=request.getParameter("userId");
		String pdcNumber=request.getParameter("pdcNumber");
		String conNumber=request.getParameter("conNumber");
		ManageTransaction mt=new ManageTransaction();
		Gson gson = new GsonBuilder().serializeNulls().create();
		logger.info("userId "+userId+" pdcNumber "+pdcNumber+" conNumber "+conNumber);
		//encoding must be set to UTF-8
		response.setCharacterEncoding("UTF-8");
		String query="Select c.status  from  current_updation_status c where c.pickup_schedule_idorcon_number=?1 and c.username=?2 and c.type=1 ORDER BY id LIMIT 1";
		    
		Query q=mt.createNativeQuery(query);
		q.setParameter(1, conNumber);q.setParameter(2, userId);
		try{
		Object o=q.getSingleResult();
		
		if(o!=null){
			logger.info(" current status "+o);
			logger.info("Response :- Delivery is " +o);
			response.getWriter().write("data: "+ "Delivery is " +o+"\n\n");
		
		}
		}catch(Exception e){
			String query2=" select d from DeliveryUpdation d where d.awbNo=?1 and d.fieldEmployeeName=?2 and d.pdcNumber=?3";
			TypedQuery<DeliveryUpdation> tq=mt.createQuery(DeliveryUpdation.class, query2);
			tq.setParameter(1, conNumber);tq.setParameter(2, userId);tq.setParameter(3, pdcNumber);
			List<DeliveryUpdation> list=tq.getResultList();
			logger.info("list "+list);
			if(list!=null&&list.size()>0){
				logger.info("list size "+list.size());
				List<DeliveryUpdationModal> deliveriesList=new ArrayList<DeliveryUpdationModal>();
				for(DeliveryUpdation du:list){
					DeliveryUpdationModal d=new DeliveryUpdationModal();
					 d.setAwbNo(du.getAwbNo());
					 d.setDeliveredTo(du.getDeliveredTo());
					 d.setErpUpdated(du.getErpUpdated());
					 d.setFieldEmployeeName(du.getFieldEmployeeName());
				     d.setLatValue(du.getLatVlue());
					 d.setLongValue(du.getLongValue());
					 d.setReason(du.getReason());
					 d.setReceiverMobileNo(du.getReceiverMobileNo());
					 d.setRelationship(du.getRelationship());
					 d.setRemarks(du.getRemarks());
					 d.setStatus(du.getStatus());
					 d.setConsigneeName(du.getConsigneeName());
					d.setPaymentType(du.getPaymentType());
					 d.setTransactionDate(format.format(du.getTransactionDate()));
					 d.setPdcNumber(du.getPdcNumber());
					 deliveriesList.add(d);
				}
				
				DeliveryUpdationModalList data=new DeliveryUpdationModalList();
				data.setDeliveryEntity(deliveriesList);
				logger.info("Response :- "+ gson.toJson(data));
				response.getWriter().write("data: "+gson.toJson(data)+"\n\n");
				
			}else{
				logger.info("Response :- Not Yet Found");
				response.getWriter().write("data: "+"Not Yet Found"+"\n\n");
				
			}
			
		}
       
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		deliveryUpdationLogger.info("********START**********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("deliveryUpdationUrl");
		String datasent="";ManageTransaction mt=null;
		DeliveryUpdationModalList updationList=null;
		StringBuilder stringBuilder = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String pdcNumber=""; String resultMessage="";String userName="";
		String imagesFlag=request.getParameter("imagesFlag");
		String ipadd = request.getRemoteAddr();
		String status=request.getParameter("status");
		String conNumber=request.getParameter("conNumber");
		String userId=request.getParameter("userId");
		deliveryUpdationLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		deliveryUpdationLogger.info(" imagesFlag "+imagesFlag+"status:- "+status+" conNumber:- "+conNumber+" userId:- "+userId);
		 Map<String,Integer> conIdMap=new HashMap<String, Integer>();
		
		try{
			mt=new ManageTransaction();
			/*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){*/
				if(status!=null&&Integer.parseInt(status)==2){
			datasent=request.getReader().readLine();
			deliveryUpdationLogger.info("DataSent from Mobile "+datasent);
			updationList=gson.fromJson(datasent, DeliveryUpdationModalList.class);
			//System.out.println("gson.tojson(du) "+gson.toJson(updationList));
			//deliveryUpdationLogger.info("updationList "+gson.toJson(updationList));
			int  dbFlag=0;
			if(updationList!=null&&updationList.getDeliveryEntity()!=null&&updationList.getDeliveryEntity().size()>0)
			{
				
				DeliveryUpdationDataList updationDataList=new DeliveryUpdationDataList();
				List<DeliveryUpdationData> updationData=new ArrayList<DeliveryUpdationData>();
				String query="select d.awbNo,d.pdcNumber,d.transactionId from  DeliveryUpdation d where d.fieldEmployeeName=?1";
			    TypedQuery<Object[]> tq=mt.createQuery(Object[].class, query);
			    tq.setParameter(1, updationList.getDeliveryEntity().get(0).getFieldEmployeeName());
			    List<Object[]> queryList=tq.getResultList();
			    List<String> cons=new ArrayList<String>();
			   Map<String,Integer> map=new HashMap<String, Integer>();
			    
			    for(Object [] o:queryList){
			    	map.put((String)o[0]+(String) o[1], (int) o[2]);
		        	
		        	  
		          }
			    
			    if(imagesFlag!=null&&Integer.parseInt(imagesFlag)==0){
			    	 for(DeliveryUpdationModal du:updationList.getDeliveryEntity()){
			    		 deliveryUpdationLogger.info(du.getAwbNo()+" exists:- "+cons.contains(du.getAwbNo()));
			    		 if(!map.containsKey((du.getAwbNo()+du.getPdcNumber()))){
						 DeliveryUpdation d=new DeliveryUpdation();
						 d.setAwbNo(du.getAwbNo());
						 d.setDeliveredTo(du.getDeliveredTo());
						 d.setErpUpdated(0);
						 d.setFieldEmployeeName(du.getFieldEmployeeName());
						 userName=du.getFieldEmployeeName();
						 d.setLatVlue(du.getLatValue());
						 d.setLongValue(du.getLongValue());
						 d.setReason(du.getReason());
						 d.setReceiverMobileNo(du.getReceiverMobileNo());
						 d.setRelationship(du.getRelationship());
						 d.setRemarks(du.getRemarks());
						 d.setStatus(du.getStatus());
						 d.setConsigneeName(du.getConsigneeName());
						 d.setCreatedTime(new Date());
						 d.setTransactionDate(format.parse(du.getTransactionDate()));
						 d.setPdcNumber(du.getPdcNumber());
						 d.setPaymentType(du.getPaymentType());
						 pdcNumber=du.getPdcNumber();
						 mt.persist(d);
						 mt.commit();
						 conIdMap.put(d.getAwbNo(),d.getTransactionId());
						 dbFlag=1;
			    		 }else{
			    			 DeliveryUpdation d=mt.find(DeliveryUpdation.class, conIdMap.get(du.getAwbNo()));
			    			 if(d.getPdcNumber().equals(du.getPdcNumber())){
			    			result = gson.toJson(new GeneralResponse(Constants.FALSE,"Data already Exists",null));
			    			 }
			    		 }
						 
					 }
			    	 if(dbFlag==1){
			    		 result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
			    	 }
			    }else if(imagesFlag!=null&&Integer.parseInt(imagesFlag)==1){
			    	DeliveryUpdation d=null;
			    	 for(DeliveryUpdationModal du:updationList.getDeliveryEntity()){
			    		 deliveryUpdationLogger.info(du.getAwbNo()+du.getPdcNumber()+" exists:- "+map.containsKey(du.getAwbNo()));
			    		 if(!map.containsKey((du.getAwbNo()+du.getPdcNumber()))){
						 d=new DeliveryUpdation();
						 d.setAwbNo(du.getAwbNo());
						 d.setDeliveredTo(du.getDeliveredTo());
						 d.setErpUpdated(0);
						 d.setFieldEmployeeName(du.getFieldEmployeeName());
						 userName=du.getFieldEmployeeName();
						 d.setLatVlue(du.getLatValue());
						 d.setLongValue(du.getLongValue());
						 d.setReason(du.getReason());
						 d.setReceiverMobileNo(du.getReceiverMobileNo());
						 d.setRelationship(du.getRelationship());
						 d.setRemarks(du.getRemarks());
						 d.setStatus(du.getStatus());
						 d.setConsigneeName(du.getConsigneeName());
						 d.setCreatedTime(new Date());
						 d.setTransactionDate(format.parse(du.getTransactionDate()));
						 d.setPdcNumber(du.getPdcNumber());
						 d.setPaymentType(du.getPaymentType());
						 pdcNumber=du.getPdcNumber();
						 if(du.getImageURL()!=null&&(!du.getImageURL().isEmpty()))
    						 d.setImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getImageURL(),du.getAwbNo(),"jpg",2,"POD"));
						 if(du.getSignatureURL()!=null&&(!du.getSignatureURL().isEmpty()))
							 d.setSignatureUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getSignatureURL(),du.getAwbNo(),"jpg",2,"Signature"));
						 mt.persist(d);
						 mt.commit();
						 conIdMap.put(d.getAwbNo(),d.getTransactionId());
						 dbFlag=1;
			    		 }else{
			    			 d=mt.find(DeliveryUpdation.class, map.get(du.getAwbNo()+du.getPdcNumber()));
			    			 
			    				 if(d.getImageUrl()==null){
			    					 if(du.getImageURL()!=null&&(!du.getImageURL().isEmpty()))
			    						 d.setImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getImageURL(),du.getAwbNo(),"jpg",2,"POD"));
			    				 }
			    				 if(d.getSignatureUrl()==null){
			    				 if(du.getSignatureURL()!=null&&(!du.getSignatureURL().isEmpty()))
									 d.setSignatureUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getSignatureURL(),du.getAwbNo(),"jpg",2,"Signature"));
			    				 }
			    				 mt.persist(d);
								 mt.commit();
			    				 
			    		 }
			    		 DeliveryUpdationData data=new DeliveryUpdationData();
						 data.setAWBNo(du.getAwbNo());
						 data.setDeliveredTo(du.getDeliveredTo());
						 data.setFieldEmployeeName(du.getFieldEmployeeName());
						 data.setLatvalue(du.getLatValue());
						 data.setLongvalue(du.getLongValue());
						 data.setReason(du.getReason());
						 data.setReceiverMobileNo(du.getReceiverMobileNo());
						 data.setRelationship(du.getRelationship());
						 data.setRemarks(du.getRemarks());
						 data.setStatus(du.getStatus());
						 data.setTransactionDate(du.getTransactionDate());
						 data.setTransactionID(d.getTransactionId());
						 data.setPaymentType(du.getPaymentType()!=null?du.getPaymentType():"");
						 if(d.getImageUrl()!=null)
							 data.setImageURL(d.getImageUrl());
						 else{data.setImageURL("");}
						 if(d.getSignatureUrl()!=null)
							 data.setSignatureURL(d.getSignatureUrl());
						 else{data.setSignatureURL("");}
						 updationData.add(data);
						 
			    }//end of for
			    	 updationDataList.setDeliveryEntity(updationData);
					 //System.out.println("Datasent to spoton "+gson.toJson(updationDataList));
					 deliveryUpdationLogger.info("Spoton URL Called"+urlString);
					 deliveryUpdationLogger.info("Datasent to spoton "+gson.toJson(updationDataList));
					 try{
						obj = new URL(urlString);
	    				con = (HttpURLConnection) obj.openConnection();
	    				con.setConnectTimeout(5000);
	    				con.setReadTimeout(10000);
	    				con.setRequestMethod("POST");
	    				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
	    				con.setRequestProperty("Accept", "application/json");
	    				con.setDoOutput(true);
	    				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
	    				  streamWriter.write(gson.toJson(updationDataList));
	    				  streamWriter.flush();
	    				 deliveryUpdationLogger.info("Response Code : " +con.getResponseCode());
	    			      if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
	  		            	stringBuilder=new StringBuilder();
	  		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	  		                BufferedReader bufferedReader = new BufferedReader(streamReader);
	  		                String response1 = null;
	  		                while ((response1 = bufferedReader.readLine()) != null) {
	  		                    stringBuilder.append(response1 + "\n");
	  		                }
	  		                bufferedReader.close();
	  		                //System.out.println("Data Received From Spoton "+stringBuilder.toString());
	  		                deliveryUpdationLogger.info("Data Received From Spoton "+stringBuilder.toString());
	  		    			
	  		    		}else{
	  		            	
	  		            //System.out.println("Response failed");
	  		          deliveryUpdationLogger.info("Response failed from Spoton");
	  		        result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response failed", null));
	  		      resultMessage="Response failed from ERP";
	  		    		}
	  		            }catch(SocketTimeoutException e){
	  		          	e.printStackTrace();
	  					deliveryUpdationLogger.info("SocketTimeoutException from ERP but Sent response as- Request Completed "+e);
	  					resultMessage="SocketTimeoutException from ERP but Sent response as- Request Completed";
	  					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
	  		            }catch(Exception e){
	  		  			e.printStackTrace();
	  					 deliveryUpdationLogger.info(" EXCEPTION IN SERVER At erp code"+e);
	  					result = gson.toJson(new GeneralResponse(Constants.FALSE,
	  							Constants.ERRORS_EXCEPTION_IN_SERVER,null));
	  					resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
	  				}
	  		            	
	  		            
	  		            
	    			      if(result.length()==0){
	    			      DeliveryUpdationResponseList responseList=gson.fromJson(stringBuilder.toString(),DeliveryUpdationResponseList.class);
	    			      //System.out.println("responseList "+gson.toJson(responseList));
	    			      deliveryUpdationLogger.info("ResponseList:- "+gson.toJson(responseList));
		    				if(responseList!=null&&responseList.getDeliveryUpdateStatus()!=null&&responseList.getDeliveryUpdateStatus().size()>0){
		    					DeliveryUpdationStatus ds=null;
		    					for(DeliveryUpdationResponse r:responseList.getDeliveryUpdateStatus()){
		    						ds=new DeliveryUpdationStatus();
		    						ds.setTransactionId(Integer.parseInt(r.getTransactionId()));
		    						DeliveryUpdation dd=mt.find(DeliveryUpdation.class, Integer.parseInt(r.getTransactionId()));
		    						dd.setErpUpdated(1);
		    						ds.setTransactionRemarks(r.getTransactionMessage());
		    						ds.setTransactionResult(r.getTransactionResult());
		    						ds.setCreatedTimestamp(new Date());
		    						mt.persist(d);
		    						mt.persist(ds);
		    						mt.commit();
		    						}
		    					
		    					for(DeliveryUpdationResponse r:responseList.getDeliveryUpdateStatus()){
		    						if(r.getTransactionResult()=="true"){
		    							result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,responseList));
		    							resultMessage=Constants.REQUEST_COMPLETED;
		    						}else{
		    							result = gson.toJson(new GeneralResponse(Constants.FALSE,r.getTransactionMessage(), null));
		    							resultMessage=r.getTransactionMessage();
		    							break;
		    							
		    						}
		    					
		    				}
		    					
		    				}else{
		    					result = gson.toJson(new GeneralResponse(Constants.FALSE,"Delivery Updation Failed", null));
		    					resultMessage="Delivery Updation Failed";
		    				}
			    }
			    }
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
				resultMessage=Constants.ERROR_INCOMPLETE_DATA;
			}
				}else{
					CurrentUpdationStatus cs=new CurrentUpdationStatus();
					cs.setPickupScheduleId_conNumber(conNumber);
					cs.setType(1);
					cs.setApkVersion(apkVersion);
					cs.setDeviceImei(deviceIMEI);
					cs.setUsername(userId);
					cs.setTimestamp(new Date());
					if(status!=null&&Integer.parseInt(status)==1)
					{
						cs.setStatus("Not Yet Updated");
					}
					mt.persist(cs);
					mt.commit();
					result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",null));
				}
			/*}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.UPDATE_TO_LATEST_VERSION, null));
			}*/
		}catch (SocketTimeoutException e){		
				e.printStackTrace();
				response.setStatus(503);
				response.sendError(503, "SocketTimeoutException");
				deliveryUpdationLogger.info("SocketTimeoutException from device with response  code ");
				resultMessage="SocketTimeoutException from device with response  code ";
				
		}catch(Exception e){
			e.printStackTrace();
			 deliveryUpdationLogger.error(" EXCEPTION IN SERVER",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			resultMessage=Constants.ERRORS_EXCEPTION_IN_SERVER+" "+e;
		}finally{
			if(mt!=null){mt.close();}
			
			if(con!=null){con.disconnect();}
		}
		//System.out.println("DeliveryUpdationService result"+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Delivery Updation", conIdMap.keySet(),userName,pdcNumber,ipadd,resultMessage,null);
		deliveryUpdationLogger.info("auditlogStatus "+auditlogStatus);
		 deliveryUpdationLogger.info("Delivery Updation Service result"+result);
		 deliveryUpdationLogger.info("END");
		response.getWriter().write(result);
	}

}
