package com.spoton.pud.services;

import java.io.IOException;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GcmPushResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.DeliveryUpdation;
import com.spoton.pud.mobileoperations.SendMessage;

/**
 * Servlet implementation class SendPushAndGetDeviceData
 */
@WebServlet("/sendPushAndGetDeviceData")
public class SendPushAndGetDeviceData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SendPushAndGetDeviceData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendPushAndGetDeviceData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START***********");
		String conNumber=request.getParameter("conNumber");
		String username=request.getParameter("username");
		String status=request.getParameter("status");
		String pickupScheduleId=request.getParameter("pickupScheduleId");
		String pdcNumber=request.getParameter("pdcNumber");
		String result="";
		Gson gson = new GsonBuilder().serializeNulls().create();
		logger.info("conNumber "+conNumber+" username "+username+" status "+status+" pickupScheduleId "+pickupScheduleId+" pdcNumber "+pdcNumber);
		try{
			ManageTransaction mt=new ManageTransaction();
			GcmPushResponse pushResponse = new GcmPushResponse();
			if(status!=null&&Integer.parseInt(status)==1){
				String deliveryQuery=" select d from DeliveryUpdation d where d.awbNo=?1 and d.fieldEmployeeName=?2 and d.pdcNumber=?3";
				TypedQuery<DeliveryUpdation> tq=mt.createQuery(DeliveryUpdation.class, deliveryQuery);
				tq.setParameter(1, conNumber);tq.setParameter(2, username);tq.setParameter(3, pdcNumber);
				List<DeliveryUpdation> deliveriesList=tq.getResultList();
				logger.info("deliveriesList "+deliveriesList);
				if(deliveriesList!=null&&deliveriesList.size()>0){
					result = gson.toJson(new GeneralResponse(Constants.FALSE,"This Con Number is already updated",null));
				}else{
				pushResponse.setId(4);
			pushResponse.setMessage(conNumber);
				}
			}else if(status!=null&&Integer.parseInt(status)==2){
				String pickupQuery="select c from ConDetail c where c.userName=?1 and c.pickupScheduleId=?2";
				TypedQuery<ConDetail> tq=mt.createQuery(ConDetail.class, pickupQuery);
				tq.setParameter(1, username);tq.setParameter(2, Integer.parseInt(pickupScheduleId));
				List<ConDetail> pickupList=tq.getResultList();
				logger.info("pickupList "+pickupList);
				if(pickupList!=null&&pickupList.size()>0){
					result = gson.toJson(new GeneralResponse(Constants.FALSE,"Pickup is already Closed",null));
				}else{
				pushResponse.setId(5);
				pushResponse.setMessage(pickupScheduleId);
				}
			}
			if(result.length()==0){
				String query="Select d.pushToken from device_info d where d.user_id=?1";
				Query nq=mt.createNativeQuery(query).setParameter(1, username);
				Object o=nq.getSingleResult();
				if(o!=null){
				logger.info("push Token "+(String)o);
				
			String message = gson.toJson(pushResponse);
			System.out.println("GCM message : "+message);
			logger.info("GCM message : "+message);
			boolean pushReturn =  SendMessage.ANDROID_sendMessage((String)o, message);
			logger.info("pushReturn "+pushReturn);
			if(pushReturn){
				result = gson.toJson(new GeneralResponse(Constants.TRUE,"Push Sent Successfully",null));
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"Failed To Send Push",null));
			}
			}
			}
		}catch(NoResultException e1){
			result = gson.toJson(new GeneralResponse(Constants.FALSE,"Device is not Registered",null));
		
		}catch(Exception e){
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			logger.info("EXCEPTION_IN_SERVER "+e);
		}
		logger.info(" result "+result);
		logger.info("**************END***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
