package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupExecutionErpResponseModal;
import com.spoton.pud.data.PickupUpdateStatusModal;
import com.spoton.pud.data.PickupUpdatinResponse;
import com.spoton.pud.data.SubmitPickupExecutionErpModal;
import com.spoton.pud.data.SubmitPickupExecutionErpModalList;
import com.spoton.pud.data.SubmitPickupExecutionModal;
import com.spoton.pud.jpa.PickupExecutionDetail;

/**
 * Servlet implementation class SubmitPickupExecution
 */
@WebServlet("/submitPickupExecution")
public class SubmitPickupExecution extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SubmitPickupExecution");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmitPickupExecution() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost( request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String deviceImei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("submitPickupExecution");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceImei+" apkVersion "+apkVersion);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ManageTransaction mt=null;
		try{
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			Type type = new TypeToken<ArrayList<SubmitPickupExecutionModal>>(){}.getType();
			List<SubmitPickupExecutionModal> inputDataList = gson.fromJson(datasent,type);
			if (inputDataList != null&inputDataList.size()>0) {
				List<SubmitPickupExecutionErpModal> erpDataList=new ArrayList<SubmitPickupExecutionErpModal>();
				SubmitPickupExecutionErpModalList erpData=new SubmitPickupExecutionErpModalList();
				mt=new ManageTransaction();
				Map<String,Integer> idMap=new HashMap<String,Integer>();
				for(SubmitPickupExecutionModal sp:inputDataList) {
					PickupExecutionDetail entity=new PickupExecutionDetail();
					entity.setConNumber(sp.getConNumber());
					
					entity.setIsConScanned(sp.getIsConScanned());
					entity.setIsPieceScanned(sp.getIsPieceScanned());
					entity.setPieceNumber(sp.getPieceNumber());
					entity.setScannedDateTime(sp.getScannedDateTime()!=null?format.parse(sp.getScannedDateTime()):null);
					entity.setUserId(sp.getUserId());
					entity.setCreatedTimestamp(new Date());
					mt.persist(entity);
					mt.commit();
					idMap.put(sp.getPieceNumber(),entity.getId());
					SubmitPickupExecutionErpModal ed=new SubmitPickupExecutionErpModal();
					ed.setConNumber(sp.getConNumber());
					ed.setIsConScanned(sp.getIsConScanned());
					ed.setIsPieceScanned(sp.getIsPieceScanned());
					ed.setPieceNo(sp.getPieceNumber());
					ed.setScannedDateTime(sp.getScannedDateTime());
					ed.setUserID(sp.getUserId());
					erpDataList.add(ed);
				}
				erpData.setAPIPiecesUpdate_List(erpDataList);
				
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);

				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				streamWriter.write(gson.toJson(erpData));
				streamWriter.flush();
				streamWriter.close();
				logger.info("Datasent to spoton " + gson.toJson(erpData));
				logger.info("Response Code : " + con.getResponseCode());
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stringBuilder = new StringBuilder();
					InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
					BufferedReader bufferedReader = new BufferedReader(streamReader);
					String response1 = null;
					while ((response1 = bufferedReader.readLine()) != null) {
						stringBuilder.append(response1);
					}
					bufferedReader.close();
					streamReader.close();
					logger.info("Response From Spoton : " + stringBuilder.toString());
					PickupExecutionErpResponseModal erpResponse=gson.fromJson(stringBuilder.toString(),PickupExecutionErpResponseModal.class);
    				if(erpResponse!=null&&erpResponse.getTransationResult()){
    					//for(PickupUpdateStatusModal pm:responseList.getPickUpUpdateStatus()){
    					for(String pieceNumber:idMap.keySet()) {
    					PickupExecutionDetail pe=mt.find(PickupExecutionDetail.class, idMap.get(pieceNumber));
    					if(pe!=null) {
    						pe.setErpUpdated(1);
    						pe.setErpResult(erpResponse.getTransationMessage());
    						mt.persist(pe);
    					}
    					}
    					//}
    					mt.commit();
    					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
    				}
					
				} else {
					logger.info("Response failed from Spoton");
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Response failed from Spoton with Response code as " + con.getResponseCode(), null));
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		logger.info("result " + result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	}

}
