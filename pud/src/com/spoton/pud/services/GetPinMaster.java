package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.Pincode;

import com.spoton.pud.jpa.PinCodeMaster;

/**
 * Servlet implementation class GetPinMaster
 */
@WebServlet("/getpinmaster")
public class GetPinMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetPinMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPinMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		logger.info("*****************Start***************");
////		PincodeMasterDevResponse res = new PincodeMasterDevResponse();
////		res.setStatus(true);
//		String apkVersion = "";
//		apkVersion = request.getHeader("apkVersion");
//		logger.info("ver::"+apkVersion);
//		int version = 0;
//		try{
//			version = Integer.parseInt(request.getParameter("version"));
//		}catch (Exception e){
//			version = 0;
//		}
//		logger.info("Version "+version);
//		String result = "";
//		StringBuilder toCsv=new StringBuilder();
//		
//		ManageTransaction mt = new ManageTransaction();
////		List<Pincode> pins = new ArrayList<Pincode>();
//		try{
//		String qry = "Select pcm from PinCodeMaster pcm where pcm.version > ?1";
//		TypedQuery<PinCodeMaster> pcmQuery = mt.createQuery(PinCodeMaster.class, qry);
//		pcmQuery.setParameter(1, version);
//		List<PinCodeMaster> pinCodes = pcmQuery.getResultList();
//		logger.info("Pincodes Size " + pinCodes.size());
//		for (PinCodeMaster pincode:pinCodes){
//			Pincode pin = new Pincode();
//			pin.setActive(pincode.getActive() == 1?true:false);
////			pin.setActiveStatus(pincode.getActiveStatus());
//			pin.setBranchCode(pincode.getId().getBranchCode());
//			pin.setLocationName(pincode.getLocationName());
//			pin.setPinCode(pincode.getId().getPinCode());
//			pin.setServicable(pincode.getServicable());
//			pin.setServiceType(pincode.getServiceType());
//			pin.setVersion(pincode.getVersion());
//			if(pincode.getIsAirServicable()!=null)
//			pin.setIsAirServicable(pincode.getIsAirServicable().equalsIgnoreCase("N")?0:1);
//			if(pincode.getIsAirServicableDel()!=null)
//			pin.setIsAirServicableDel(pincode.getIsAirServicableDel().equalsIgnoreCase("N")?0:1);
//			pin.setAreaId(pincode.getAreaId());
//			pin.setPincodeType(pincode.getId().getPincodeType());
//			//**23-07-2020 - akshay
//			pin.setAccountCode(pincode.getId().getAccountCode());
//			pin.setFlag(pincode.getFlag());
//			pin.setPickupAllowed(pincode.getPickupAllowed());
//			pin.setDeliveryAllowed(pincode.getDeliveryAllowed());
//			//**
//			toCsv.append(pin.toString() + "\n");
////			pins.add(pin);
////			System.out.println("Added " + pin.getPinCode());
//		}
//	/*	logger.info("Output Data ");
//		logger.info(toCsv.toString());*/
//		}catch(Exception e){
////			res.setStatus(false);
////			res.setMessage(e.getLocalizedMessage());
////			result = new Gson().toJson(res);
//			logger.error("Exception ", e);
//			response.getWriter().write("exception : " + e.getLocalizedMessage());
//			e.printStackTrace();
//		}
//		finally{
//			mt.close();
//		}
//		
////		res.setStatus(true);
////		res.setData(pins);
////		result = new Gson().toJson(res);
//		logger.info("*****************End***************");
//		response.getWriter().write(toCsv.toString());
//	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*****************Start***************");
//		PincodeMasterDevResponse res = new PincodeMasterDevResponse();
//		res.setStatus(true);
		//response.setContentType("text/plain");
		String apkVersion = "";
		apkVersion = request.getHeader("apkVersion");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		logger.info("ver::"+apkVersion);
		int version = 0;
		int pagelimit = 10000;
		int pageno = 0;
		int offset = 0;
		
		
		
		if(CommonTasks.check(request.getParameter("page")))
			pageno = Integer.parseInt(request.getParameter("page"));
		else
			pageno = 1;
		
		offset = (pageno - 1) * 10000;
		
		
		try{
			version = Integer.parseInt(request.getParameter("version"));
		}catch (Exception e){
			version = 0;
		}
		logger.info("Version "+version);
		String result = "";
		StringBuilder toCsv=new StringBuilder();
		
		ManageTransaction mt = new ManageTransaction();
//		List<Pincode> pins = new ArrayList<Pincode>();
		try{
		String qry = "Select pcm from PinCodeMaster pcm where pcm.version > ?1 order by pcm.rowNumber asc";
		TypedQuery<PinCodeMaster> pcmQuery = mt.createQuery(PinCodeMaster.class, qry);
		pcmQuery.setParameter(1, version);
		pcmQuery.setFirstResult(offset).setMaxResults(pagelimit);
		List<PinCodeMaster> pinCodes = pcmQuery.getResultList();
		logger.info("Pincodes Size " + pinCodes.size());
		for (PinCodeMaster pincode:pinCodes){
			Pincode pin = new Pincode();
			pin.setActive(pincode.getActive() == 1?true:false);
//			pin.setActiveStatus(pincode.getActiveStatus());
			pin.setBranchCode(pincode.getId().getBranchCode());
			pin.setLocationName(pincode.getLocationName());
			pin.setPinCode(pincode.getId().getPinCode());
			pin.setServicable(pincode.getServicable());
			pin.setServiceType(pincode.getServiceType());
			pin.setVersion(pincode.getVersion());
			if(pincode.getIsAirServicable()!=null)
			pin.setIsAirServicable(pincode.getIsAirServicable().equalsIgnoreCase("N")?0:1);
			if(pincode.getIsAirServicableDel()!=null)
			pin.setIsAirServicableDel(pincode.getIsAirServicableDel().equalsIgnoreCase("N")?0:1);
			pin.setAreaId(pincode.getAreaId());
			pin.setPincodeType(pincode.getId().getPincodeType());
			//**23-07-2020 - akshay
			pin.setAccountCode(pincode.getId().getAccountCode());
			pin.setFlag(pincode.getFlag());
			pin.setPickupAllowed(pincode.getPickupAllowed());
			pin.setDeliveryAllowed(pincode.getDeliveryAllowed());
			pin.setRegion(pincode.getRegion());
			pin.setDepot(pincode.getDepot());
			pin.setArea(pincode.getArea());
			if(pincode.getUpdatedTime()!=null) {
				pin.setTimestamp(format.format(pincode.getUpdatedTime()));
			}else if(pincode.getCreatedTime()!=null) {
				pin.setTimestamp(format.format(pincode.getCreatedTime()));
			}
			//**
			toCsv.append(pin.toString() + "\n");
//			pins.add(pin);
//			System.out.println("Added " + pin.getPinCode());
		}
	/*	logger.info("Output Data ");
		logger.info(toCsv.toString());*/
		}catch(Exception e){
//			res.setStatus(false);
//			res.setMessage(e.getLocalizedMessage());
//			result = new Gson().toJson(res);
			logger.error("Exception ", e);
			response.getWriter().write("exception : " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		finally{
			mt.close();
		}
		
//		res.setStatus(true);
//		res.setData(pins);
//		result = new Gson().toJson(res);
		logger.info("*****************End***************");
		response.getWriter().write(toCsv.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
