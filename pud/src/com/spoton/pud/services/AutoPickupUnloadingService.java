package com.spoton.pud.services;


import java.io.IOException;

import java.lang.reflect.Type;
/*import java.net.HttpURLConnection;
import java.net.URL;*/
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;

import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.AutoPickupUnloadingInputModal;
import com.spoton.pud.data.AutoPickupUnloadingResponseModal;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.AutoPickupUnloading;
import com.spoton.pud.jpa.AutoPickupUnloadingCon;
import com.spoton.pud.jpa.ConDetail;

/**
 * Servlet implementation class AutoPickupUnloading
 */
@WebServlet("/autoPickupUnloadingService")
public class AutoPickupUnloadingService extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("AutoPickupUnloadingService");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AutoPickupUnloadingService() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String deviceImei = request.getHeader("imei");
		String apkVersion = request.getHeader("apkVersion");
		/*HttpURLConnection con = null;
		URL obj = null;
		String urlString = FilesUtil.getProperty("autoPickupUnloading");
		StringBuilder stringBuilder = null;*/
		logger.info("deviceIMEI " + deviceImei + " apkVersion " + apkVersion);
		ManageTransaction mt = null;
	/*Calendar currentDateMrng=Calendar.getInstance();
		currentDateMrng.setTime(new Date());
		currentDateMrng.set(Calendar.HOUR_OF_DAY, 0);
		currentDateMrng.set(Calendar.MINUTE, 0);
		currentDateMrng.set(Calendar.SECOND, 0);
		currentDateMrng.set(Calendar.MILLISECOND, 0);*/
		
		 Calendar yesterday=Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			yesterday.set(Calendar.HOUR_OF_DAY, 00);
			yesterday.set(Calendar.MINUTE, 00);
			yesterday.set(Calendar.SECOND, 00);
			yesterday.set(Calendar.MILLISECOND, 0);
		
		Calendar currentDateEvng=Calendar.getInstance();
		currentDateEvng.setTime(new Date());
		currentDateEvng.set(Calendar.HOUR_OF_DAY, 23);
		currentDateEvng.set(Calendar.MINUTE, 59);
		currentDateEvng.set(Calendar.SECOND, 59);
		currentDateEvng.set(Calendar.MILLISECOND, 0);
		
		try {
			String datasent = request.getReader().readLine();
			logger.info("Datasent from Mobile " + datasent);
			List<String> allConsList=new ArrayList<String>();
			String username=null;//String vehicleNumber=null;int unloadingEntityId=0;
			Map<String,Integer> conMap=new HashMap<String,Integer>();
			Map<String,Integer> vehicleMap=new HashMap<String,Integer>();
			mt = new ManageTransaction();
			if(datasent!=null) {
			Type type = new TypeToken<ArrayList<AutoPickupUnloadingInputModal>>(){}.getType();
			List<AutoPickupUnloadingInputModal> inputDataList = gson.fromJson(datasent,type);
			
			//Map<Integer,String> countMap=new HashMap<Integer,String>();
			//if (inputDataList != null&inputDataList.size()>0) {
				//List<JSONObject> erpDataList=new ArrayList<JSONObject>();
				
				for(AutoPickupUnloadingInputModal inputData:inputDataList) {
				AutoPickupUnloading entity = new AutoPickupUnloading();
				entity.setCreatedTimestamp(new Date());
				entity.setUserId(inputData.getUserId());
				entity.setVehicleNumber(inputData.getVehicleNo());
				entity.setTotalConsCount(inputData.getConNumbers()!=null?inputData.getConNumbers().size():0);
				username=inputData.getUserId();
				mt.persist(entity);
				mt.commit();
				vehicleMap.put(inputData.getVehicleNo(), entity.getTransactionId());
				//unloadingEntityId=entity.getTransactionId();
				if(inputData.getConNumbers()!=null&&inputData.getConNumbers().size()>0) {
					for(String s:inputData.getConNumbers()) {
						allConsList.add(s);
					AutoPickupUnloadingCon cons=new AutoPickupUnloadingCon();
					cons.setAutoPickupUnloading(entity);
					cons.setConNumber(s);
					cons.setCreatedTimestamp(new Date());
					cons.setUserId(inputData.getUserId());
					cons.setVehicleNumber(inputData.getVehicleNo());
					mt.persist(cons);
					mt.commit();
					conMap.put(cons.getConNumber(), cons.getId());
					}
				}
				
				}
			}
			//	if(allConsList!=null&&allConsList.size()>0) {
					
					/*String cq="select c.conNumber,s.statusFlag,c.actualWeight,c.noOfPackage,c.conEntryId,c.vehicleNumber FROM ConDetail c,PickupUpdationStatus s where c.isUnloadingSheetGenerated=0 and "
							+ "c.erpUpdated=1 and c.conEntryId=s.conDetail.conEntryId "
							+ "and c.userName=?1 and c.createdTimestamp>=?2 and c.createdTimestamp<=?3 and c.pickupType=0";*/
				try {
					username=request.getParameter("userName");
				if(username==null&&deviceImei!=null&&!deviceImei.isEmpty()) {
					String getUser="SELECT u.user_name FROM pud.user_session_data u where u.active_flag=1 and u.imei=?1 order by u.created_timestamp desc limit 1";
				    Query usertq=mt.createNativeQuery(getUser).setParameter(1, deviceImei);
				    username=(String) usertq.getSingleResult();
					
				}
				}catch(Exception e) {
					logger.info("Exception in getting username "+e);
				}
				logger.info("username "+username);
				if(username!=null) {
				String cq="select c.conNumber,c.statusFlag,c.actualWeight,c.noOfPackage,c.conEntryId,c.vehicleNumber FROM ConDetail c where c.isUnloadingSheetGenerated=0 and "
						+ "c.erpUpdated=1 "
						+ "and c.userName=?1 and c.createdTimestamp>=?2 and c.createdTimestamp<=?3 and c.pickupType=0";
					

				TypedQuery<Object[]> q=mt.createQuery(Object[].class, cq);
					
					q.setParameter(1, username).setParameter(2, yesterday.getTime()).setParameter(3, currentDateEvng.getTime());
				
					List<Object[]> list=q.getResultList();
				
					if(list!=null&&list.size()>0) {
						for(Object[] o:list) {
							if(vehicleMap.containsKey((String) o[5])) {
							if(conMap.containsKey((String) o[0])) {
								
								AutoPickupUnloadingCon uc=mt.find(AutoPickupUnloadingCon.class, conMap.get((String) o[0]));
								if(uc!=null) {
									uc.setIsConBooked(1);
									uc.setBookedConStatus((String) o[1]);
									uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
									uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
									uc.setConDetailsId((int) o[4]);
									AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
									if(au!=null) {
										au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
										au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
										mt.persist(au);
									}
								    mt.persist(uc);
								    mt.commit();
									
								}
							}else {
								if(o[1]!=null&&(((String) o[1]).equalsIgnoreCase("Y"))) {
								AutoPickupUnloadingCon uc=new AutoPickupUnloadingCon();
								uc.setAutoPickupUnloading(mt.find(AutoPickupUnloading.class, vehicleMap.get((String) o[5])));
								uc.setIsConBooked(1);
								uc.setBookedConStatus((String) o[1]);
								uc.setConNumber((String) o[0]);
								uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								uc.setConDetailsId((int) o[4]);
								uc.setCreatedTimestamp(new Date());
								uc.setUserId(username);
								uc.setVehicleNumber((String) o[5]);
								
								AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
								if(au!=null) {
									au.setTotalConsCount(au.getTotalConsCount()+1);
									au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
									au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
									mt.persist(au);
								}
							    mt.persist(uc);
							    mt.commit();
							}
							}
							}else {
								if(o[1]!=null&&(((String) o[1]).equalsIgnoreCase("Y"))) {
								AutoPickupUnloading entity = new AutoPickupUnloading();
								entity.setCreatedTimestamp(new Date());
								entity.setUserId(username);
								entity.setVehicleNumber((String) o[5]);
								entity.setTotalConsCount(1);
								entity.setTotalConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								entity.setTotalPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								mt.persist(entity);
								mt.commit();
								vehicleMap.put((String) o[5], entity.getTransactionId());
								AutoPickupUnloadingCon cons=new AutoPickupUnloadingCon();
								cons.setAutoPickupUnloading(entity);
								cons.setConNumber((String) o[0]);
								cons.setCreatedTimestamp(new Date());
								cons.setBookedConStatus((String) o[1]);
								cons.setConDetailsId((int) o[4]);
								cons.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								cons.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								cons.setIsConBooked(1);
								cons.setUserId(username);
								cons.setVehicleNumber((String) o[5]);
								mt.persist(cons);
								mt.commit();
								conMap.put(cons.getConNumber(), cons.getId());
								}
							}
							ConDetail cd=mt.find(ConDetail.class, (int) o[4]);
				    		if(cd!=null) {
				    		cd.setIsUnloadingSheetGenerated(2);//2 is set to avoid duplicate cons for auto unloading sheet generation
				    		mt.persist(cd);
				    		mt.commit();
				    		}
							
						}
						
					}
					
					//update status for cash booking cons
					/*String cq1="select c.conNumber,s.flag,c.actualWeight,c.noOfPackage,c.conEntryId,c.vehicleNumber FROM ConDetail c,CashBookingResponse s where  c.isUnloadingSheetGenerated=0 and "
							+ "c.erpUpdated=1 and c.conEntryId=s.conEntryId "
							+ "and c.userName=?1 and c.createdTimestamp>=?2 and c.createdTimestamp<=?3 and c.pickupType=3";
*/					
					String cq1="select c.conNumber,c.statusFlag,c.actualWeight,c.noOfPackage,c.conEntryId,c.vehicleNumber FROM ConDetail c where  c.isUnloadingSheetGenerated=0 and "
							+ "c.erpUpdated=1 "
							+ "and c.userName=?1 and c.createdTimestamp>=?2 and c.createdTimestamp<=?3 and c.pickupType=3";
					
					TypedQuery<Object[]> q1=mt.createQuery(Object[].class, cq1);
					
					q1.setParameter(1, username).setParameter(2, yesterday.getTime()).setParameter(3, currentDateEvng.getTime());
					
					List<Object[]> list1=q1.getResultList();
					if(list1!=null&&list1.size()>0) {
						for(Object[] o:list1) {
							if(vehicleMap.containsKey((String) o[5])) {
							if(conMap.containsKey((String) o[0])) {
								
								AutoPickupUnloadingCon uc=mt.find(AutoPickupUnloadingCon.class, conMap.get((String) o[0]));
								if(uc!=null) {
									uc.setIsConBooked(1);
									uc.setBookedConStatus((String) o[1]);
									uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
									uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
									uc.setConDetailsId((int) o[4]);
									uc.setUserId(username);
									uc.setVehicleNumber((String) o[5]);
									AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
									if(au!=null) {
										
										au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
										au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
										mt.persist(au);
									}
								    mt.persist(uc);
								    mt.commit();
									
								}
							}else {
								if(o[1]!=null&&(((String) o[1]).equalsIgnoreCase("Y"))) {
								AutoPickupUnloadingCon uc=new AutoPickupUnloadingCon();
								uc.setAutoPickupUnloading(mt.find(AutoPickupUnloading.class,  vehicleMap.get((String) o[5])));
								uc.setIsConBooked(1);
								uc.setBookedConStatus((String) o[1]);
								uc.setConNumber((String) o[0]);
								uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								uc.setConDetailsId((int) o[4]);
								uc.setCreatedTimestamp(new Date());
								uc.setUserId(username);
								uc.setVehicleNumber((String) o[5]);
								AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
								if(au!=null) {
									au.setTotalConsCount(au.getTotalConsCount()+1);
									au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
									au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
									mt.persist(au);
								}
							    mt.persist(uc);
							    mt.commit();
							}
							}
						}else {
							if(o[1]!=null&&(((String) o[1]).equalsIgnoreCase("Y"))) {
							AutoPickupUnloading entity = new AutoPickupUnloading();
							entity.setCreatedTimestamp(new Date());
							entity.setUserId(username);
							entity.setVehicleNumber((String) o[5]);
							entity.setTotalConsCount(1);
							entity.setTotalConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
							entity.setTotalPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
							mt.persist(entity);
							mt.commit();
							vehicleMap.put((String) o[5], entity.getTransactionId());
							AutoPickupUnloadingCon cons=new AutoPickupUnloadingCon();
							cons.setAutoPickupUnloading(entity);
							cons.setConNumber((String) o[0]);
							cons.setCreatedTimestamp(new Date());
							cons.setBookedConStatus((String) o[1]);
							cons.setConDetailsId((int) o[4]);
							cons.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
							cons.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
							cons.setIsConBooked(1);
							cons.setUserId(username);
							cons.setVehicleNumber((String) o[5]);
							mt.persist(cons);
							mt.commit();
							conMap.put(cons.getConNumber(), cons.getId());
							}
						}
							ConDetail cd=mt.find(ConDetail.class, (int) o[4]);
				    		if(cd!=null) {
				    		cd.setIsUnloadingSheetGenerated(2);//2 is set to avoid duplicate cons for auto unloading sheet generation
				    		mt.persist(cd);
				    		mt.commit();
				    		}

						}
							
						
					}
				}
					
					
			/*	String cq="select c.conNumber,s.statusFlag,c.actualWeight,c.noOfPackage FROM ConDetail c,PickupUpdationStatus s where c.conNumber in :allConsList and "
						+ "c.erpUpdated=1 and c.conEntryId=s.conDetail.conEntryId "
						+ "and c.userName=?1";
				
				TypedQuery<Object[]> q=mt.createQuery(Object[].class, cq);
				q.setParameter("allConsList", allConsList);
				q.setParameter(1, username);
				
				List<Object[]> list=q.getResultList();
				if(list!=null&&list.size()>0) {
					for(Object[] o:list) {
						if(map.containsKey((String) o[0])) {
							
							AutoPickupUnloadingCon uc=mt.find(AutoPickupUnloadingCon.class, map.get((String) o[0]));
							if(uc!=null) {
								uc.setIsConBooked(1);
								uc.setBookedConStatus((String) o[1]);
								uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
								if(au!=null) {
									au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
									au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
									mt.persist(au);
								}
							    mt.persist(uc);
								mt.commit();
							}
						}
					}
				}
				//update status for cash booking cons
				String cq1="select c.conNumber,s.flag,c.actualWeight,c.noOfPackage FROM ConDetail c,CashBookingResponse s where c.conNumber in :allConsList and "
						+ "c.erpUpdated=1 and c.conEntryId=s.conEntryId "
						+ "and c.userName=?1";
				
				TypedQuery<Object[]> q1=mt.createQuery(Object[].class, cq1);
				q1.setParameter("allConsList", allConsList);
				q1.setParameter(1, username);
				
				List<Object[]> list1=q1.getResultList();
				if(list1!=null&&list1.size()>0) {
					for(Object[] o:list1) {
						if(map.containsKey((String) o[0])) {
							
							AutoPickupUnloadingCon uc=mt.find(AutoPickupUnloadingCon.class, map.get((String) o[0]));
							if(uc!=null) {
								uc.setIsConBooked(1);
								uc.setBookedConStatus((String) o[1]);
								uc.setConWeight(o[2]!=null?Double.parseDouble((String) o[2]):0);
								uc.setPiecesCount(o[3]!=null?Integer.parseInt((String) o[3]):0);
								AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
								if(au!=null) {
									au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
									au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
									mt.persist(au);
								}
							    mt.persist(uc);
								mt.commit();
							}
						}
					}
				}*/
				
				//}
				
				List<AutoPickupUnloadingResponseModal> responseDataList=new ArrayList<AutoPickupUnloadingResponseModal>();
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, responseDataList));
				/*JSONObject jsonObj = new JSONObject();
				jsonObj.put("UserID", inputData.getUserId());
				jsonObj.put("VehicleNo", inputData.getVehicleNo());
				jsonObj.put("TransactionID", entity.getTransactionId());
				jsonObj.put("ConNo", inputData.getConNumbers());
				erpDataList.add(jsonObj);
				}
				JSONObject erpJson=new JSONObject();
				erpJson.put("AutoPickupUnloadingInput",erpDataList);
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(10000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);

				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				streamWriter.write(erpJson.toJSONString());
				streamWriter.flush();
				logger.info("Datasent to spoton " + erpJson.toJSONString());
				logger.info("Response Code : " + con.getResponseCode());
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stringBuilder = new StringBuilder();
					InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
					BufferedReader bufferedReader = new BufferedReader(streamReader);
					String response1 = null;
					while ((response1 = bufferedReader.readLine()) != null) {
						stringBuilder.append(response1);
					}
					bufferedReader.close();
					logger.info("Response From Spoton : " + stringBuilder.toString());
					Type data=new TypeToken<ArrayList<AutoPickupUnloadingErpResponseModal>>(){}.getType();
					List<AutoPickupUnloadingErpResponseModal> erpResponseList=gson.fromJson(stringBuilder.toString(),data);
					if(erpResponseList!=null&&erpResponseList.size()>0) {
						List<AutoPickupUnloadingResponseModal> responseDataList=new ArrayList<AutoPickupUnloadingResponseModal>();
						for(AutoPickupUnloadingErpResponseModal erpResponse:erpResponseList) {
						AutoPickupUnloading ap=mt.find(AutoPickupUnloading.class,erpResponse.getTransactionID());
						if(ap!=null) {
							ap.setTransactionResult(erpResponse.getTransationResult());
							ap.setTransactionMessage(erpResponse.getTransationMessage());
							ap.setPickupUnloadingSheetNumber(erpResponse.getPickupUnloadingSheetNo());
							mt.persist(ap);
							mt.commit();
						}
					
						
						AutoPickupUnloadingResponseModal responseData=new AutoPickupUnloadingResponseModal();
						responseData.setPickupUnloadingSheetNo(erpResponse.getPickupUnloadingSheetNo());
						responseData.setTransactionId(erpResponse.getTransactionID());
						responseData.setTransationMessage(erpResponse.getTransationMessage());
						responseData.setTransationResult(erpResponse.getTransationResult());
						responseDataList.add(responseData);
						}
						result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, responseDataList));
						
					}else {
						result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Response From Erp", null));
					}

				} else {
					logger.info("Response failed from Spoton");
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Response failed from Spoton with Response code as " + con.getResponseCode(), null));
				}*/
			//}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		logger.info("result " + result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
	}

}
