package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.common.utilities.GeocodeLocations;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.DeliveryConditionImagesErpModal;
import com.spoton.pud.data.DeliveryUpdationData;
import com.spoton.pud.data.DeliveryUpdationDataList;
import com.spoton.pud.data.DeliveryUpdationModal;
import com.spoton.pud.data.DeliveryUpdationModalList;
import com.spoton.pud.data.DeliveryUpdationResponse;
import com.spoton.pud.data.DeliveryUpdationResponseList;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.DeliveryUpdation;
import com.spoton.pud.jpa.DeliveryUpdationStatus;

/**
 * Servlet implementation class SubmitFtcDockets
 */
@WebServlet("/submitFtcDockets")
public class SubmitFtcDockets extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SubmitFtcDockets");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubmitFtcDockets() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String imei = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("submitFtcDockets");
		StringBuilder stringBuilder = null;
		logger.info("imei "+imei+" apkVersion "+apkVersion);
		ManageTransaction mt=null;
		try {
			String datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			DeliveryUpdationModalList updationList=gson.fromJson(datasent, DeliveryUpdationModalList.class);
			DeliveryUpdationDataList updationDataList=new DeliveryUpdationDataList();
			List<DeliveryUpdationData> updationData=new ArrayList<DeliveryUpdationData>();
			if(updationList!=null&&updationList.getDeliveryEntity()!=null&&updationList.getDeliveryEntity().size()>0)
			{
				 mt=new ManageTransaction();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for(DeliveryUpdationModal du:updationList.getDeliveryEntity()){
					 DeliveryUpdation d=new DeliveryUpdation();
					 if(du.getImageURL()!=null&&(!du.getImageURL().isEmpty()))
						 d.setImageUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getImageURL(),du.getAwbNo(),"jpg",2,"POD"));
					 d.setAwbNo(du.getAwbNo());
					 d.setDeliveredTo(du.getDeliveredTo());
					 d.setErpUpdated(0);
					 d.setFieldEmployeeName(du.getFieldEmployeeName());
					 d.setLatVlue(du.getLatValue());
					 d.setLongValue(du.getLongValue());
					 d.setReason(du.getReason());
					 d.setReceiverMobileNo(du.getReceiverMobileNo());
					 d.setRelationship(du.getRelationship());
					 d.setRemarks(du.getRemarks());
					 d.setStatus(du.getStatus());
					 d.setConsigneeName(du.getConsigneeName());
					 d.setCreatedTime(new Date());
					 d.setTransactionDate(du.getTransactionDate()!=null?format.parse(du.getTransactionDate()):null);
					 d.setPdcNumber(du.getPdcNumber());
					 d.setPaymentType(du.getPaymentType());
					 d.setIsFtc(du.getIsFtc());
					 d.setCollectionType(du.getCollectionType());
					 d.setCollectedAmount(du.getCollectedAmount()!=null?du.getCollectedAmount():0);
					 d.setBankName(du.getBankName());
					 d.setBankBranch(du.getBankBranch());
					 d.setChequeAmount(du.getChequeAmount()!=null?du.getChequeAmount():0);
					 d.setChequeNumber(du.getChequeNumber());
					 d.setIsValidPod(du.getIsValidPod());
					 d.setShipmentType(du.getShipmentType());
					 d.setDeliveryUpdatedAddress(du.getDlyAddress());
					 d.setDeliveryUpdatedPincode(du.getDlyPincode());
					 d.setChequeDate(du.getChequeDate()!=null?format.parse(du.getChequeDate()):null);
					 if(du.getSignatureURL()!=null&&(!du.getSignatureURL().isEmpty()))
						 d.setSignatureUrl(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getSignatureURL(),du.getAwbNo(),"jpg",2,"Signature"));
					 if(du.getChequeImg1URL()!=null&&(!du.getChequeImg1URL().isEmpty()))
						 d.setChequeImg1(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getChequeImg1URL(),du.getAwbNo(),"jpg",2,"ChequeImage1"));
					 if(du.getChequeImg2URL()!=null&&(!du.getChequeImg2URL().isEmpty()))
						 d.setChequeImg2(FilesUtil.getProperty("ImageURL")+CommonTasks.toImgFromBase64(du.getChequeImg2URL(),du.getAwbNo(),"jpg",2,"ChequeImage2"));
					d.setIsTdsDeducted(du.getIsTDSDeducted());
					d.setIsFtcPaymentDocket(1);
					 if(du.getLatValue()!=0&&du.getLongValue()!=0) {
						 try {
							 List<String> addressAndPincode=GeocodeLocations.getGeocodedAddress(du.getLatValue(),du.getLongValue(),logger);
							 if(addressAndPincode!=null) {
							 String address=addressAndPincode.get(0);
							 String pincode=addressAndPincode.get(1);
							 d.setIsReverseGeocodingSuccess(1);
							 d.setGeocodedAddress(address);
							 d.setGeocodedPincode(pincode);
							 }
						 }catch(Exception e) {
							 e.printStackTrace();
							 logger.error("Exception while geocoding location ",e);
							 
						 }
						 }
					
					
					mt.persist(d);
					 mt.commit();
					
					 DeliveryUpdationData data=new DeliveryUpdationData();
					 data.setAWBNo(du.getAwbNo());
					 data.setDeliveredTo(du.getDeliveredTo());
					 data.setFieldEmployeeName(du.getFieldEmployeeName());
					 data.setLatvalue(du.getLatValue());
					 data.setLongvalue(du.getLongValue());
					 data.setReason(du.getReason());
					 data.setReceiverMobileNo(du.getReceiverMobileNo());
					 data.setRelationship(du.getRelationship());
					 data.setRemarks(du.getRemarks());
					 data.setStatus(du.getStatus());
					 data.setTransactionDate(du.getTransactionDate());
					 data.setTransactionID(d.getTransactionId());
					 data.setPaymentType( du.getPaymentType()!=null?du.getPaymentType():"");
					 data.setShipmentType(du.getShipmentType());
					 if(d.getImageUrl()!=null)
						 data.setImageURL(d.getImageUrl());
					 else{data.setImageURL("");}
					 if(d.getSignatureUrl()!=null)
						 data.setSignatureURL(d.getSignatureUrl());
					 else{data.setSignatureURL("");}
					 data.setIsFTC(du.getIsFtc());
					 data.setCollectionType(du.getCollectionType());
					 data.setCollectedAmt(du.getCollectedAmount());
					 data.setBankName(du.getBankName());
					 data.setBankBr(du.getBankBranch());
					 data.setCheqNo(du.getChequeNumber());
					 data.setCheqAmt(du.getChequeAmount());
					 data.setCheqDate(d.getChequeDate());
					 data.setCheqImg1(d.getChequeImg1()!=null?d.getChequeImg1():"");
					 data.setCheqImg2(d.getChequeImg2()!=null?d.getChequeImg2():"");
					 data.setIsValidPOD(du.getIsValidPod()!=null?du.getIsValidPod():"");
					 data.setIsTDSDeducted(du.getIsTDSDeducted());
					 data.setTANNo(du.getTanNumber());
					 data.setCustomerName(du.getCustomerName());
					 data.setDelConditionImages(new ArrayList<DeliveryConditionImagesErpModal>());
					 updationData.add(data);
				}
				 updationDataList.setDeliveryEntity(updationData);
			      logger.info("Spoton URL Called"+urlString);
			      logger.info("Datasent to spoton "+gson.toJson(updationDataList));
			      obj = new URL(urlString);
  				con = (HttpURLConnection) obj.openConnection();
  				con.setRequestMethod("POST");
  				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
  				con.setRequestProperty("Accept", "application/json");
  				con.setDoOutput(true);
  				 con.setConnectTimeout(5000);
	    			  con.setReadTimeout(60000);
  				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
  				  streamWriter.write(gson.toJson(updationDataList));
  				  streamWriter.flush();
  				streamWriter.close();
  				  logger.info("Response Code : " +con.getResponseCode());
  			      if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		                logger.info("Data Received From Spoton "+stringBuilder.toString());
  		                
	  		              DeliveryUpdationResponseList responseList=gson.fromJson(stringBuilder.toString(),DeliveryUpdationResponseList.class);
	  		            if(responseList!=null&&responseList.getDeliveryUpdateStatus()!=null&&responseList.getDeliveryUpdateStatus().size()>0){
	    					DeliveryUpdationStatus ds=null;
	    					for(DeliveryUpdationResponse r:responseList.getDeliveryUpdateStatus()){
	    						ds=new DeliveryUpdationStatus();
	    						ds.setTransactionId(Integer.parseInt(r.getTransactionId()));
	    						DeliveryUpdation d=mt.find(DeliveryUpdation.class, Integer.parseInt(r.getTransactionId()));
	    						d.setErpUpdated(1);
	    						d.setTransactionRemarks(r.getTransactionMessage());
	    						d.setTransactionResult(r.getTransactionResult());
	    						ds.setTransactionRemarks(r.getTransactionMessage());
	    						ds.setTransactionResult(r.getTransactionResult());
	    						ds.setCreatedTimestamp(new Date());
	    						mt.persist(ds);mt.persist(d);
	    						
	    						}
	    					mt.commit();
	    					result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",responseList));
	    					
	    				}else{
	    					result = gson.toJson(new GeneralResponse(Constants.FALSE,"Delivery Updation Failed", null));
	    					
	    				}
  			      }else{
		            	logger.info("Response failed from Spoton");
		                result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response failed from Spoton", null));
		     }
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
				
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
			if(con!=null) {
				con.disconnect();
			}
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
