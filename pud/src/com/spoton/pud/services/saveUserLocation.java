package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.UserLocationHistory;
import com.spoton.pud.data.UserLocationModel;
import com.spoton.pud.jpa.DeviceInfo;
import com.spoton.pud.jpa.UserDataMaster;
import com.spoton.pud.jpa.UserLocation;

/*This API saves the location of the device(mapped to user(pud agent) in the DB.)*/

@WebServlet("/saveUserLocation")
public class saveUserLocation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public saveUserLocation() {
        super();
    }
   
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		     // Declare and define logger file. 
		    Logger logger = Logger.getLogger("SaveUserLocation");
	 	    logger.info("\n----------------------------START----------------------------") ;
	 	    
	 	    // set response ContentType and CharacterEncoding. Getting the constants.
	 		response.setCharacterEncoding(Constants.CHARACTER_ENCODING_UTF_8);
	 		response.setContentType(Constants.CONTENT_TYPE_JSON);
	 		
	 		// Define variables.
	 		String result = "";
	 		Gson gson = new GsonBuilder().serializeNulls().create();
	 		String dataReceived = request.getReader().readLine();
	 		logger.info("Data received is "+dataReceived);
	 		 
	 		 if (CommonTasks.check(dataReceived)) {
	 			UserLocationHistory userLocationHistory = null;
	 				try {
	 					userLocationHistory = gson.fromJson(dataReceived, UserLocationHistory.class);
	 				} catch (Exception e) {
	 					e.printStackTrace();
	 					result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_PARSING_REQUEST_DATA, null));
	 					logger.info( Constants.ERROR_PARSING_REQUEST_DATA);
	 					logger.error("Exception details"+" :",e);
	 				}

	 				if (userLocationHistory != null && userLocationHistory.getUserLocationModels()!=null && !userLocationHistory.getUserLocationModels().isEmpty() && userLocationHistory.getUserLocationModels().size()>0 && result.length() == 0) {
	 					ManageTransaction mt = new ManageTransaction();
	 					try {
	 						
	 						logger.info("Total no of location = "+userLocationHistory.getUserLocationModels().size());
	 						
	 						//Iterate through list and make sure all are saved in MT.
	 						mt.begin();
	 						for (UserLocationModel userLocationModel : userLocationHistory.getUserLocationModels()) {
	 							// Create UserLocation  persistent objects for all locations. 
 								UserLocation userLocation = new UserLocation();
 								userLocation.setLatitude(userLocationModel.getLatitude());
 								userLocation.setLongitude(userLocationModel.getLongitude());
 								userLocation.setAddress(userLocationModel.getAddress());
 								userLocation.setPincode(userLocationModel.getPincode());
 								userLocation.setAccuracy(userLocationModel.getAccuracy());
 								userLocation.setDistance(userLocationModel.getDistance());
 								userLocation.setDeviceTimestamp(new SimpleDateFormat(Constants.DATE_FORMAT).parse(userLocationModel.getDeviceTimestamp()));
 								userLocation.setTimestamp(new Date());
 								userLocation.setUserDataMaster((userLocationModel.getDeviceInfo()!=null && userLocationModel.getDeviceInfo().getId()!=0)?mt.find(UserDataMaster.class, userLocationModel.getUserDataMaster().getId()):null);
 								userLocation.setDeviceInfo((userLocationModel.getUserDataMaster()!=null && userLocationModel.getUserDataMaster().getId()!=0)?mt.find(DeviceInfo.class, userLocationModel.getDeviceInfo().getId()):null);
	 							mt.persist(userLocation);
							}
	 						//Commit all persistent objects at once.
	 						mt.commit();
							result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED,null));
	 					} catch (Exception e) {
	 						e.printStackTrace();
	 						result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
	 						logger.error("Exception in server"+" :",e);

	 					} finally {
	 						mt.close();
	 					}
	 				}
	 			} else {
	 				logger.info(Constants.ERROR_INCOMPLETE_DATA);
	 				result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_INCOMPLETE_DATA, null));
	 			}
	 			logger.info("response= "+result);
	 			logger.info("\n----------------------------END----------------------------") ;
	 			response.getWriter().write(result);
	 	}
	     
	 }