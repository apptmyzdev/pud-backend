package com.spoton.pud.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PrintConDetailsModel;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.PinCodeMaster;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@WebServlet({"/printConDetails"})
public class PrintConDetails
  extends HttpServlet
{
  private static final long serialVersionUID = 1L;
  Logger printConDetails = Logger.getLogger("PrintConDetails");
  public PrintConDetails() {}
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    doPost(request, response);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
	printConDetails.info("*********START*********");
    response.setContentType("application/json");
    String result = "";
    Gson gson = new GsonBuilder().serializeNulls().create();
    String userName = request.getParameter("userName");
    String conNum = request.getParameter("conNum");
    printConDetails.info("Request from user :"+userName);
    printConDetails.info("For Con number :"+conNum);
    String deviceIMEI = request.getHeader("imei");
	String apkVersion= request.getHeader("apkVersion");
	String resultMessage="";
	String ipadd = request.getRemoteAddr();
	printConDetails.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
	String inputParameters="conNumber "+conNum;
    ManageTransaction manageTransaction = null;
    DecimalFormat decf = new DecimalFormat("0.000");
    decf.setMaximumFractionDigits(3);
    Map<Integer,String> conMap=new HashMap<Integer, String>();
    conMap.put(1, conNum);
    if (CommonTasks.check(new String[] { userName, conNum })) {
      try
      {
        manageTransaction = new ManageTransaction();
        Map<Integer, String> pincodesMap = new HashMap<Integer, String>();
        
        String query = "select d.id.pinCode, d.id.branchCode from PinCodeMaster d";
        TypedQuery tquery = manageTransaction.createQuery(PinCodeMaster.class, query);
        List<Object[]> pinCodesList = tquery.getResultList();
        if ((pinCodesList != null) && (pinCodesList.size() > 0)) {
          for (Object[] obj : pinCodesList) {
            pincodesMap.put((Integer)obj[0], (String)obj[1]);
          }
          SimpleDateFormat outDate = new SimpleDateFormat("dd-MM-yyyy");
          String mQuery = "select d from ConDetail d where d.userName = ?1 and d.conNumber=?2 and d.createdTimestamp = (select max(d.createdTimestamp) from ConDetail d where d.userName = ?1 and d.conNumber = ?2)";
          TypedQuery<ConDetail> typedQuery = manageTransaction.createQuery(ConDetail.class, mQuery);
          typedQuery.setParameter(1, userName);
          typedQuery.setParameter(2, conNum);
          try
          {
            ConDetail obj = (ConDetail)typedQuery.getSingleResult();
            if (obj != null)
            {
              PrintConDetailsModel model = null;
              
              model = new PrintConDetailsModel();
              model.setConNum(obj.getConNumber() == null ? "-" : obj.getConNumber());
              model.setRefNo(obj.getRefNumber() == null ? "-" : obj.getRefNumber());
              model.setShipDate(obj.getPickupDate() == null ? "-" : outDate.format(obj.getPickupDate()));
              model.setDecValue(obj.getDeclaredValue() == null ? "-" : obj.getDeclaredValue());
              model.setOriginPincode(obj.getOriginPincode() == null ? "-" : obj.getOriginPincode());
              model.setDestPincode(obj.getDestinationPincode() == null ? "-" : obj.getDestinationPincode());
              model.setWeight(obj.getActualWeight() == null ? "-" : decf.format(Double.parseDouble(obj.getActualWeight())));
              model.setVolume(obj.getTotalVolWeight() == null ? "-" : decf.format(Double.parseDouble(obj.getTotalVolWeight())));
              model.setPcs(obj.getNoOfPackage() == null ? "-" : obj.getNoOfPackage());
              model.setSender(obj.getCustomerCode() == null ? "-" : obj.getCustomerCode());
              model.setCustomerName(obj.getCustomerName() == null ? "-" : obj.getCustomerName());
              if (pincodesMap.containsKey(Integer.valueOf(Integer.parseInt(obj.getOriginPincode()))))
              {
                model.setOriginName((String)pincodesMap.get(Integer.valueOf(Integer.parseInt(obj.getOriginPincode()))));
              }
              else {
                model.setOriginName("-");
              }
              if (pincodesMap.containsKey(Integer.valueOf(Integer.parseInt(obj.getDestinationPincode()))))
              {
                model.setDestName((String)pincodesMap.get(Integer.valueOf(Integer.parseInt(obj.getDestinationPincode()))));
              }
              else {
                model.setDestName("-");
              }
              
              result = gson.toJson(new GeneralResponse(true, "Success", model));
              printConDetails.info(result);
              resultMessage="Success";
            }
            else
            {
              result = gson.toJson(new GeneralResponse(false, "No Data Available", null));
              printConDetails.info(result);
              resultMessage="No Data Available";
            }
          }
          catch (Exception e) {
            e.printStackTrace();
            printConDetails.error("Exception",e);
            result = gson.toJson(new GeneralResponse(false, "Con doesn't exist", null));
            resultMessage="Con doesn't exist";
            printConDetails.info(result);
          }
          
        }
        else
        {
          result = gson.toJson(new GeneralResponse(false, "No Data Available", null));
          printConDetails.info(result);
          resultMessage="No Data Available";
        }
      }
      catch (Exception e) {
        e.printStackTrace();
        printConDetails.error("Exception",e);
        result = gson.toJson(new GeneralResponse(false, "Exception in server", null));
        
        printConDetails.info(result);
        resultMessage="Exception in server "+e;
      }finally{
    	  if(manageTransaction != null)
    		  manageTransaction.close();
      }
      
    } else {
      result = gson.toJson(new GeneralResponse(false, "Incomplete data sent to the server", null));
      printConDetails.info(result);
      resultMessage="Incomplete data sent to the server";
    }
    
    response.getWriter().write(result);
    boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Printing Con Details", conMap.values(),userName,null,ipadd,resultMessage,inputParameters);
    printConDetails.info("auditlogStatus "+auditlogStatus);
    printConDetails.info("Final response sent as : "+result);
    printConDetails.info("*************************END*********************************************");
  }
}