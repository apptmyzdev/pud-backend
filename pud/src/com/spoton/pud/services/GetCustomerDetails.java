package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;




import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Correct;
import com.spoton.common.utilities.Errors;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.CustomerDetailsData;
import com.spoton.pud.data.CustomerDetailsModal;
import com.spoton.pud.data.CustomerDetailsResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupRegistrationModal;
import com.spoton.pud.jpa.UserSessionData;


/**
 * Servlet implementation class GetCustomerDetails
 */
@WebServlet("/getCustomerDetails")
public class GetCustomerDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger customerDetailsLogger = Logger.getLogger("GetCustomerDetails");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCustomerDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		customerDetailsLogger.info("**************START*******************");
		String urlString = null;
		URL obj =null;
		
		BufferedReader in = null;
		StringBuffer response1 = new StringBuffer();
		Gson gson = new GsonBuilder().serializeNulls().create();
		String customerCode=request.getParameter("customerCode"); 
		String customerName=request.getParameter("customerName"); 
		String userId = request.getParameter("userId"); 
		HttpURLConnection con=null; String result="";
		//System.out.println("customerCode "+customerCode);
		CustomerDetailsResponse responseData=new CustomerDetailsResponse();
		List<CustomerDetailsModal> customerDetailsList=new ArrayList<CustomerDetailsModal>();
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String resultMessage="";
		String ipadd = request.getRemoteAddr();
		 String inputParameters="";
		customerDetailsLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
			try{
	      	 if((CommonTasks.check(customerCode)||CommonTasks.check(customerName))&&CommonTasks.check(userId)){
	      		boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
				if(checkapkVersion){
				customerDetailsLogger.info("userId "+userId +" CustomerCode From Mobile "+customerCode);
				//customerDetailsLogger.info("CustomerCode From Mobile"+customerCode);
				inputParameters="CustomerCode "+customerCode;
				 urlString=FilesUtil.getProperty("customerDetailsUrl")+userId+"/"+customerCode+"/C";
			  if(customerName!=null){
				 customerDetailsLogger.info("CustomerName From Mobile"+customerName);
				 inputParameters="CustomerName "+customerName;
				 urlString=FilesUtil.getProperty("customerDetailsUrl")+userId+"/"+customerName+"/A";
			 }
			 //System.out.println("urlString "+urlString);
			 
			 customerDetailsLogger.info("URL Called "+urlString);
			// add reuqest header
			 if(urlString!=null){
			 obj = new URL(urlString);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			 con.setReadTimeout(10000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			int responseCode = con.getResponseCode();
			customerDetailsLogger.info("ResponseCode"+responseCode);
			//System.out.println("Response Code : " +responseCode);
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	
			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			//System.out.println("response1 "+response1);
			String receivedData = null;
			receivedData = response1.toString();
			customerDetailsLogger.info("Received Data From Server"+receivedData);
			//System.out.println("dataReceived "+receivedData);
		
			Type customerDetailsListType = new TypeToken<ArrayList<CustomerDetailsData>>(){}.getType();
			List<CustomerDetailsData> DetailsList=gson.fromJson(receivedData, customerDetailsListType);
			
			
			for(CustomerDetailsData c:DetailsList){
				CustomerDetailsModal cd=new CustomerDetailsModal();
				cd.setCustomerBranch(c.getCustomerBranch());
				cd.setCustomerCity(c.getCustomerCity());
				cd.setCustomerCode(c.getCustomerCode());
				cd.setCustomerName(c.getCustomerName());
				cd.setCustomerType(c.getCustomerType());
				cd.setTransactionRemarks(c.getTransactionRemarks());
				cd.setTransactionResult(c.getTransactionResult());
				cd.setProductType(c.getProductType());
				customerDetailsList.add(cd);
			}
			
			if(customerDetailsList.size()>0){
				responseData.setCustomerDetailsList(customerDetailsList);
			result = gson.toJson(new GeneralResponse(
					Correct.status, 
					Correct.CORRECT_REQUEST_COMPLETED.message, 
					responseData));
			resultMessage=Correct.CORRECT_REQUEST_COMPLETED.message;
			
			
			}else{
            	result = gson.toJson(new GeneralResponse(
    					Errors.status, 
    					Errors.ERRORS_NO_DATA_AVAILABLE.ERRORS_AVAILABLE.message, 
    					responseData));
            	resultMessage=Errors.ERRORS_NO_DATA_AVAILABLE.ERRORS_AVAILABLE.message;
            }
			 } 
				}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							Constants.UPDATE_TO_LATEST_VERSION, null));
				}
		}else{
			result = gson.toJson(new GeneralResponse(
					Errors.status, 
					Errors.ERRORS_INCOMPLETE_DATA.ERRORS_INCOMPLETE_DATA_SENT.message, 
					responseData));
			resultMessage=Errors.ERRORS_INCOMPLETE_DATA.ERRORS_INCOMPLETE_DATA_SENT.message;
			
		}
			}catch (SocketTimeoutException e){		
				e.printStackTrace();
				customerDetailsLogger.info("SocketTimeoutException "+e);
				 result = gson.toJson(new GeneralResponse(
						 Constants.FALSE,
						 "ERP did not respond. Please try again", 
	    					null));
				 resultMessage="ERP did not respond. Please try again";
				
			
	        	  }catch(Exception e){
	      			e.printStackTrace();
	      			customerDetailsLogger.error("EXCEPTION_IN_SERVER ",e);
	      			result = gson.toJson(new GeneralResponse(
	      					Errors.status, 
	      					Errors.ERRORS_EXCEPTIONS.ERRORS_EXCEPTION_IN_SERVER.message, 
	      					responseData));
	      			resultMessage="EXCEPTION_IN_SERVER "+e;
	      		}finally{
	      			if(in!=null)
	      		    in.close();
	      			if(con!=null)
	      			con.disconnect();
	      		}
	      		
			response.getWriter().write(result);
			 boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Getting Customer Details", null,userId,null,ipadd,resultMessage,inputParameters);
			 customerDetailsLogger.info("auditlogStatus "+auditlogStatus);
			customerDetailsLogger.info("Result of GetCustomerDetails"+result);
			customerDetailsLogger.info("**************END*******************");
			//System.out.println("result"+result);
	          }

		
	}


