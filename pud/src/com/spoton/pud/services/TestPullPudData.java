package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PullPudDataModalV2;
import com.spoton.pud.jpa.PickupRegistration;
import com.spoton.pud.jpa.PudData;

/**
 * Servlet implementation class TestPullPudData
 */
@WebServlet("/testPullPudData")
public class TestPullPudData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pullPudDataLogger = Logger.getLogger("TestPullPudData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestPullPudData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @SuppressWarnings("deprecation")
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		pullPudDataLogger.info("**************START*******************");
		response.setContentType("application/json");
		Gson gson = new GsonBuilder().serializeNulls().create();
		
		String result = "";
	    String userId=request.getParameter("userId");
		ManageTransaction mt=null;
		pullPudDataLogger.info("userId "+userId);
		String deviceIMEI =request.getHeader("imei");
		String apkVersion=request.getHeader("apkVersion");
		String resultMessage="";
		String ipadd = request.getRemoteAddr();
		pullPudDataLogger.info("deviceIMEI:- "+deviceIMEI+" apkVersion:- "+apkVersion+" ipadd:- "+ipadd);
		
		try{
		if((CommonTasks.check(userId))){
			boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){
			SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mt=new ManageTransaction();
			/*Calendar c=Calendar.getInstance();
			//c.setTime(new Date());
			c.add(Calendar.DATE, +1);
			c.set(Calendar.HOUR_OF_DAY, 06);
			c.set(Calendar.MINUTE, 00);
			c.set(Calendar.SECOND, 00);
			c.set(Calendar.MILLISECOND, 0);
			//System.out.println("c time "+c.getTime());
			Calendar threeDaysBackDate=Calendar.getInstance();
			threeDaysBackDate.add(Calendar.DATE, -3);
			
			threeDaysBackDate.set(Calendar.HOUR_OF_DAY, 0);
			threeDaysBackDate.set(Calendar.MINUTE, 0);
			threeDaysBackDate.set(Calendar.SECOND, 0);
			threeDaysBackDate.set(Calendar.MILLISECOND, 0);
			
			Calendar twoDaysBackDate=Calendar.getInstance();
			twoDaysBackDate.add(Calendar.DATE, -2);
			
			twoDaysBackDate.set(Calendar.HOUR_OF_DAY, 0);
			twoDaysBackDate.set(Calendar.MINUTE, 0);
			twoDaysBackDate.set(Calendar.SECOND, 0);
			twoDaysBackDate.set(Calendar.MILLISECOND, 0);
			
			//System.out.println("twoDaysBackDate "+twoDaysBackDate);
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			Date todaysDate=new Date();
			
			//Date todaysDate=dateformat.parse("2018-01-09 03:00:00");
			//System.out.println("todaysDate "+todaysDate);
			Calendar currentDate=Calendar.getInstance();
			currentDate.setTime(new Date());
			currentDate.set(Calendar.HOUR_OF_DAY, 23);
			currentDate.set(Calendar.MINUTE, 59);
			currentDate.set(Calendar.SECOND, 59);
			currentDate.set(Calendar.MILLISECOND, 0);
			
			
			Calendar yesterday=Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			yesterday.set(Calendar.HOUR_OF_DAY, 00);
			yesterday.set(Calendar.MINUTE, 00);
			yesterday.set(Calendar.SECOND, 00);
			yesterday.set(Calendar.MILLISECOND, 0);
			//System.out.println("yesterday "+yesterday);
			pullPudDataLogger.info("Current time "+todaysDate);
			pullPudDataLogger.info("threeDaysBackDate "+threeDaysBackDate.getTime() +"twoDaysBackDate"+twoDaysBackDate.getTime());
			pullPudDataLogger.info("yesterday "+yesterday.getTime() +"tomorrow"+c.getTime());*/
			Map<Integer, PullPudDataModalV2> map = new HashMap<Integer, PullPudDataModalV2>();
			
			
			
			Calendar todayDate=Calendar.getInstance();
			
			Calendar tomorrowDate=Calendar.getInstance();
			if(new Date().getHours()>6) {
			
			todayDate.setTime(new Date());
			todayDate.set(Calendar.HOUR_OF_DAY, 06);
			todayDate.set(Calendar.MINUTE, 00);
			todayDate.set(Calendar.SECOND, 00);
			todayDate.set(Calendar.MILLISECOND, 0);
			
			tomorrowDate.add(Calendar.DATE, +1);
			tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
			tomorrowDate.set(Calendar.MINUTE, 00);
			tomorrowDate.set(Calendar.SECOND, 00);
			tomorrowDate.set(Calendar.MILLISECOND, 0);
			
			pullPudDataLogger.info("todayDate "+todayDate.getTime()+" tomorrowDate "+tomorrowDate.getTime());
			
			}else { // pickups are valid till next day 6 am .Before 6 am consider one day back pickups 
				todayDate.add(Calendar.DATE, -1);
				todayDate.set(Calendar.HOUR_OF_DAY, 06);
				todayDate.set(Calendar.MINUTE, 00);
				todayDate.set(Calendar.SECOND, 00);
				todayDate.set(Calendar.MILLISECOND, 0);
				
				tomorrowDate.setTime(new Date());
				tomorrowDate.set(Calendar.HOUR_OF_DAY, 06);
				tomorrowDate.set(Calendar.MINUTE, 00);
				tomorrowDate.set(Calendar.SECOND, 00);
				tomorrowDate.set(Calendar.MILLISECOND, 0);
			}
			//String query="Select d from PudData d where d.userId=?1 and d.pickupDate>=?2 and d.pickupDate<=?3 ";
			String query="Select d from PudData d where d.userId=?1 and d.validTill>?2 and d.updatedAttemptedFlag is NULL";
			TypedQuery<PudData> typedQuery=mt.createQuery(PudData.class, query);//oda is 2 days only but getting 3 days data as after 12am the diff of days will be 3
			typedQuery.setParameter(1, userId).setParameter(2, todayDate.getTime());
			//typedQuery.setParameter(2,threeDaysBackDate.getTime());typedQuery.setParameter(3,currentDate.getTime());
			List<PudData> list=typedQuery.getResultList();
			PullPudDataModalV2 pd=null;
		

			if(list!=null&&list.size()!=0){
				pullPudDataLogger.info("PudData List size fom db "+list.size());
				
				for(Object s:list){
					PudData p=(PudData)s;
				
					pd=new PullPudDataModalV2();
					pd.setId(p.getId());
					pd.setCft(p.getCft());
					pd.setContactMobile(p.getContactMobile());
					pd.setContactPerson(p.getContactPerson());
					pd.setCustomerCode(p.getCustomerCode());
					pd.setCustomerName(p.getCustomerName());
					pd.setCustomerRefNumber(p.getCustomerRefNumber());
					pd.setDeliveryAddress(p.getDeliveryAddress());
					pd.setDeliveryCity(p.getDeliveryCity());
					pd.setDeliveryPincode(p.getDeliveryPincode());
					pd.setMinCfgWeight(p.getMinCfgWeight());
					pd.setPickupAddress(p.getPickupAddress());
					pd.setPickupCity(p.getPickupCity());
					pd.setSenderMailId(p.getSenderMailId());
					if(p.getPickupDate()!=null)
					pd.setPickupDate(dateformat.format(p.getPickupDate()));
					pd.setPickupOrderNo(p.getPickupOrderNo());
					pd.setPickupPincode(p.getPickupPincode());
					pd.setPickupRegisterId(p.getPickupRegisterId());
					pd.setPickupScheduleId(p.getPickupScheduleId());
					pd.setPieces(p.getPieces());
					pd.setUserId(p.getUserId());
					pd.setWeight(p.getWeight());
					pd.setVehicleCapacity(p.getVehicleCapacity());
					pd.setVehicleNumber(p.getVehicleNumber());
					pd.setVehicleType(p.getVehicleType());
					pd.setProductType(p.getProductType());
					map.put(p.getPickupScheduleId(), pd);
					/*if(p.getIsOda()==1&&todaysDate.getHours()>6){//oda true and not midnight so allow only 2 days data
						Date startDate = twoDaysBackDate.getTime();Date endDate = p.getPickupDate();
						long diff = endDate.getTime() - startDate.getTime();
						long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
						
						if(diffIndays>=0&&diffIndays<=2){
					map.put(p.getPickupScheduleId(), pd);
					}
					
					}else if(p.getIsOda()==1&&todaysDate.getHours()<=6){//oda true but after 12 am we need to check for 3 days
						Date startDate = threeDaysBackDate.getTime();Date endDate = p.getPickupDate();
						long diff = endDate.getTime() - startDate.getTime();
						long diffIndays=TimeUnit.MILLISECONDS.toDays(diff);
						if(diffIndays>=0&&diffIndays<3){
						map.put(p.getPickupScheduleId(), pd);
					}
					}else if(p.getIsOda()==0 && todaysDate.getHours()>6&&((format2.format(todaysDate)).equals(format2.format(p.getPickupDate())))){ //oda zero but todays data //till 12 am this condition will work
						map.put(p.getPickupScheduleId(), pd);
					}else if(p.getIsOda()==0 && todaysDate.getHours()<=6&&((format2.format(yesterday.getTime())).equals(format2.format(p.getPickupDate())))){ //after 12am todaysDate changes //till 6am following will work
						map.put(p.getPickupScheduleId(), pd);
					}*/
					
					
					//pullPudDataLogger.info("PickupScheduleId from pudData "+p.getPickupScheduleId());
					
				}
			}
				
			//String query2="Select p from PickupRegistration p where p.mobileUserId=?1 and p.pickupDate>=?2 and p.pickupDate<=?3 and p.transactionResult=1 ";	
			String query2="Select p from PickupRegistration p where p.mobileUserId=?1 and p.validTill>?2 and p.validTill<=?3 and  p.transactionResult=1 and p.updatedAttemptedFlag is NULL ";
			TypedQuery<PickupRegistration> typedQuery2=mt.createQuery(PickupRegistration.class, query2).setParameter(1, userId);
			typedQuery2.setParameter(2,todayDate.getTime()).setParameter(3, tomorrowDate.getTime());
			//typedQuery2.setParameter(2,yesterday.getTime());typedQuery2.setParameter(3,currentDate.getTime());	
			List<PickupRegistration> list1=typedQuery2.getResultList();
			
			//System.out.println("list1 "+list1.size());
				if(list1!=null&&list1.size()>0){
					pullPudDataLogger.info("PickupRegistration list size "+list1.size());
					for(PickupRegistration pr:list1){
						pd=new PullPudDataModalV2();
						pd.setId(pr.getId());
						pd.setCft(pr.getCft());
						pd.setContactMobile(pr.getContactPersonNumber());
						pd.setContactPerson(pr.getContactPersonName());
						pd.setCustomerCode(pr.getCustomerCode());
						pd.setCustomerName(pr.getCustomerName());
						pd.setPickupAddress(pr.getPickupAddress());
						if(pr.getPickupDate()!=null)
						pd.setPickupDate(dateformat.format(pr.getPickupDate()));
						pd.setPickupOrderNo(pr.getPickupOrderNo());
						pd.setPickupPincode(pr.getPickupPincode());
						pd.setPickupScheduleId(pr.getPickupScheduleId());
						pd.setPieces(pr.getPieces());
						pd.setUserId(pr.getMobileUserId());
						pd.setWeight((float)pr.getWeight());
						pd.setSenderMailId(pr.getSenderMailId());
						pd.setProductType(pr.getProductType());
						map.put(pr.getPickupScheduleId(), pd);
						/*if(todaysDate.getHours()>6&&((format2.format(todaysDate)).equals(format2.format(pr.getPickupDate())))){//after 6 am only current pickups
							map.put(pr.getPickupScheduleId(), pd);
							
						}else if(todaysDate.getHours()<=6&&((format2.format(yesterday.getTime())).equals(format2.format(pr.getPickupDate())))){ //after 12 am this condition
							map.put(pr.getPickupScheduleId(), pd);
						}*/
						
					//	pullPudDataLogger.info("PickupScheduleId from PickupRegistration "+pr.getPickupScheduleId());
						
					}
				}
				
				/*String query3="select p.pickupScheduleId from ConDetail p where p.userName=?1 and p.pickupDate>=?2 and p.pickupDate<=?3 and p.pickupType!=3";
				TypedQuery<Integer> tq=mt.createQuery(Integer.class, query3);
				tq.setParameter(1, userId);
				
				tq.setParameter(2,threeDaysBackDate.getTime());tq.setParameter(3,currentDate.getTime());
				List<Integer> updatedPickups=tq.getResultList();
				if(updatedPickups!=null&&updatedPickups.size()!=0)
				for(Integer o:updatedPickups){
					pullPudDataLogger.info(" Normal PickupScheduleId from PickupUpdation "+o);
					if(map.containsKey(o)){
						map.remove(o);
					}
				}*/
				
				
				/*String query5="select p.pickupScheduleId from ConDetail p,CashBookingResponse r "
						+ "where p.userName=?1 and p.conEntryId=r.conEntryId and r.flag=?4 and p.pickupDate>=?2 and p.pickupDate<=?3 and p.pickupType=3";
				TypedQuery<Integer> tq4=mt.createQuery(Integer.class, query5);
				tq4.setParameter(1, userId);tq4.setParameter(4, "Y");
				
				tq4.setParameter(2,threeDaysBackDate.getTime());tq4.setParameter(3,currentDate.getTime());
				List<Integer> updatedCashPickups=tq4.getResultList();
				if(updatedCashPickups!=null&&updatedCashPickups.size()!=0)
				for(Integer o:updatedCashPickups){
					pullPudDataLogger.info("  Cash PickupScheduleId from PickupUpdation "+o);
					if(map.containsKey(o)){
						map.remove(o);
					}
				}
				*/
				
				

				/*String query4="Select r.pickupScheduleId from PickupReschedule r,PickupRescheduleUpdateStatus s where s.pickupScheduleId=r.pickupScheduleId "
						     + "and r.appMobileUserName=?1 and s.timestamp>=?2 and s.timestamp<=?3 and s.transactionResult=?5";
				TypedQuery<Integer> tquery=mt.createQuery(Integer.class, query4);
				tquery.setParameter(5, "true");tquery.setParameter(1, userId);
				
				tquery.setParameter(2,threeDaysBackDate.getTime());tquery.setParameter(3,currentDate.getTime());
				List<Integer> attempedPickups=tquery.getResultList();
				if(attempedPickups!=null&&attempedPickups.size()!=0)
				for(Integer o:attempedPickups){
					pullPudDataLogger.info("PickupScheduleId from PickupReschedule "+o);
					if(map.containsKey(o)){
						map.remove(o);
					}
				}*/
				if(map.values()!=null&&map.values().size()!=0){
				result = gson.toJson(new GeneralResponse(Constants.TRUE,
						Constants.REQUEST_COMPLETED,map.values()));
				resultMessage=Constants.REQUEST_COMPLETED;
				}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_NO_DATA_AVAILABLE,null));
				resultMessage=Constants.ERROR_NO_DATA_AVAILABLE;
				}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.UPDATE_TO_LATEST_VERSION, null));
				
			}
		}else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERROR_INCOMPLETE_DATA,null));
			resultMessage=Constants.ERROR_INCOMPLETE_DATA;
		}
		}catch(Exception e){
			e.printStackTrace();
			//System.out.println("e.getStackTrace()"+e.getStackTrace());
			pullPudDataLogger.info("EXCEPTION_IN_SERVER "+e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			resultMessage="EXCEPTION_IN_SERVER "+e;
		}finally{
			if(mt!=null)
			mt.close();
		}
		pullPudDataLogger.info("PullPudData v2 result "+result);
		boolean auditlogStatus = CommonTasks.saveAuditLog(deviceIMEI,apkVersion,"Pulled Pud Data", null,userId,null,ipadd,resultMessage,null);
		pullPudDataLogger.info("auditlogStatus "+auditlogStatus);
		//System.out.println("result "+result);
		response.getWriter().write(result);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
