package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Errors;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.UserDataModel;
import com.spoton.pud.jpa.UserDataMaster;

/**
 * Servlet implementation class UserMasterData
 */
@WebServlet("/userData")
public class GetUserDataMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetUserDataMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUserDataMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		logger.info("**********START***********");
		String urlString = FilesUtil.getProperty("userDataMaster");
		logger.info("url "+urlString);
		URL obj = new URL(urlString);
		Gson gson = new GsonBuilder().serializeNulls().create();

		BufferedReader inData = null;
		StringBuffer response1 = new StringBuffer();
		ManageTransaction manageTransaction = null;
		HttpURLConnection con = null;
		String result="";
		try{
			 con = (HttpURLConnection) obj.openConnection();
				// add reuqest header
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setConnectTimeout(5000);
				con.setReadTimeout(10000);
				int responseCode = con.getResponseCode();
				logger.info("responseCode "+responseCode);
				//System.out.println("responded with status code : "+responseCode);
				inData = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
		
				while ((inputLine = inData.readLine()) != null) {
					response1.append(inputLine);
				}
				
				String receivedData = null;
				receivedData = response1.toString();
				logger.info("Response from Erp "+receivedData);
				//System.out.println("dataReceived "+receivedData);
			if(receivedData != null)
			{
				manageTransaction = new ManageTransaction();
				UserDataMaster master = null;
				Type userMasterDataType = new TypeToken<ArrayList<UserDataModel>>(){}.getType();
				List <UserDataModel> dataMasterModelsList = gson.fromJson(receivedData,userMasterDataType);
				Map<String,String> checkMap = new HashMap<String,String>();
				String checkQuery = "select d.appUserName from UserDataMaster d";
				TypedQuery <String> typedQuery = manageTransaction.createQuery(String.class,checkQuery);
				List<String>exUsersList = typedQuery.getResultList();
				for(String exUser : exUsersList)
				{
					checkMap.put(exUser, exUser);
				}
				int count = 0;
				if(dataMasterModelsList != null && dataMasterModelsList.size() > 0)
				{
				
					
					manageTransaction.begin();
					for(UserDataModel dataobj : dataMasterModelsList)
					{
						if(checkMap.containsKey(dataobj.getAppUserName()) == false)
						{
						master = new UserDataMaster();
						master.setAppUserName(dataobj.getAppUserName());
						master.setAppPwd(dataobj.getAppPwd());
						master.setCurrentlyUsed(dataobj.getCurrentlyUsed());
						master.setIs9digitPartNo(dataobj.getIs9digitPartNo() == true ? 1 : 0);
						master.setIsActive(dataobj.getIsActive() == true ? 1 : 0);
						master.setIsConEntry(dataobj.getIsConEntry() == true ? 1 : 0);
						master.setIsDelivery(dataobj.getIsDelivery() == true ? 1 : 0);
						master.setIsPickupAppActive(dataobj.getIsPickupAppActive() == true ? 1 : 0);
						master.setIsPickupRegistration(dataobj.getIsPickupRegistration() == true ? 1 : 0);
						master.setIsPiecesEntry(dataobj.getIsPiecesEntry() == true ? 1 : 0);
						master.setIsOfflinePrinting(dataobj.getIsOfflinePrinting() == true ? 1 : 0);
						master.setLocation(dataobj.getLocation());
						master.setLoginStatus(dataobj.getLoginStatus() == true ? 1 : 0);
						master.setPrinterSlNo(dataobj.getPrinterSlNo());
						master.setUserId(dataobj.getUserId() != 0 ? dataobj.getUserId() : 0);
						master.setVerisonId(dataobj.getVerisonId());
						manageTransaction.persist(master);
						count++;
						}
					}
					manageTransaction.commit();
					//System.out.println(count);
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,null));
				}
				else{
					result = gson.toJson(new GeneralResponse(Errors.status,Errors.ERRORS_NO_DATA_AVAILABLE.ERRORS_AVAILABLE.message,null));
				}
			
			}
			else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
			}
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,null));
		}
		finally{
			if(inData != null)
			inData.close();
			if(manageTransaction != null)
				manageTransaction.close();
			if(con != null)
				con.disconnect();
		}
		logger.info("GetUserDataMaster Result "+result);
		logger.info("**************END**********");
		 response.getWriter().write(result);
	}

}
