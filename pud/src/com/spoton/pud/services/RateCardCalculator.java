package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.RateCardCalculatorERPModal;
import com.spoton.pud.data.RateCardCalculatorInputModal;
import com.spoton.pud.data.RateCardCalculatorResponseModal;

/**
 * Servlet implementation class RateCardCalculator
 */
@WebServlet("/rateCardCalculator")
public class RateCardCalculator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("RateCardCalculator");  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RateCardCalculator() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*****************Start***************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("rateCardCalculator");
		String datasent="";StringBuilder stringBuilder = null;
		try{
			datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			if(CommonTasks.check(datasent)){
				RateCardCalculatorInputModal input=gson.fromJson(datasent,RateCardCalculatorInputModal.class);
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("OriginCode", input.getOriginCode());
				jsonObj.put("DestnCode",input.getDestinationCode());
				jsonObj.put("CustCode", input.getCustomerCode());
				jsonObj.put("Paybas", input.getPaymentBasis());
				jsonObj.put("ActWt", input.getActualWeight());
				jsonObj.put("Volume", input.getVolume());
				jsonObj.put("DeclValue", input.getDeclaredValue());
				jsonObj.put("DeclValue", input.getDeclaredValue());
				jsonObj.put("ApplyVTC",input.getApplyVtc());
				jsonObj.put("RiskType",input.getRiskType());
				jsonObj.put("DACC",input.getDacc());
				jsonObj.put("NForm",input.getnForm());
				jsonObj.put("FTC",input.getFtc());
				jsonObj.put("DC",input.getDc());
				jsonObj.put("UserID",input.getUserName());
				jsonObj.put("ProductType",input.getProductType());
				logger.info("Datasent to spoton "+jsonObj.toJSONString());
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				
				  OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(jsonObj.toJSONString());
				  streamWriter.flush();
				  streamWriter.close();
				  con.setConnectTimeout(5000);
					con.setReadTimeout(60000);
					logger.info("Response Code : "+con.getResponseCode());
					  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
			            	stringBuilder=new StringBuilder();
			                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
			                BufferedReader bufferedReader = new BufferedReader(streamReader);
			                String response1 = null;
			                while ((response1 = bufferedReader.readLine()) != null) {
			                    stringBuilder.append(response1 + "\n");
			                }
			                bufferedReader.close();
			                streamReader.close();
			                logger.info("Response from server : "+stringBuilder.toString());
			               
			                RateCardCalculatorERPModal re=gson.fromJson(stringBuilder.toString(),RateCardCalculatorERPModal.class);
			                if(re!=null){
			                	RateCardCalculatorResponseModal data=new RateCardCalculatorResponseModal();
			                	data.setCollection(re.getCollection());
			                	data.setDacc(re.getDACC());
			                	data.setDcac(re.getDCAC());
			                	data.setDfs(re.getDFS());
			                	data.setDkt(re.getDKT());
			                	data.setErroCode(re.getErroCode());
			                	data.setFov(re.getFOV());
			                	data.setFrt(re.getFRT());
			                	data.setFtc(re.getFTC());
			                	data.setGrossAmount(re.getGrossAmount());
			                	data.setHandlingCharge(re.getHandlingCharge());
			                	data.setMinCharge(re.getMinCharge());
			                	data.setNfForm(re.getNF_Form());
			                	data.setOda(re.getODA());
			                	data.setOtherRecovery(re.getOtherRecovery());
			                	data.setPda(re.getPDA());
			                	data.setPhDelv(re.getPH_Delv());
			                	data.setPhPickup(re.getPH_Pickup());
			                	data.setServiceTax(re.getServiceTax());
			                	data.setServiceTaxPercent(re.getServiceTaxPercent());
			                	data.setStateCharge(re.getStateCharge());
			                	data.setTotalAmount(re.getTotalAmount());
			                	data.setVtc(re.getVTC());
			                	data.setChargedWeight(re.getChargedWeight());
			                	data.setRateModifyEnabled(re.getRateModifyEnabled());
			                	data.setEnvironmentalSurcharges(re.getEnvironmental_Surcharges());
			                	data.setActualAmount(re.getActualAmount());
			                	data.setCovidSurcharge(re.getCovidSurcharge());
			                	result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,data));
			                	
			                }else{
			                	result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.ERROR_NO_DATA_AVAILABLE,null));
			                }
					  }
			      else{
            	
            	logger.info("Response failed from server");
            	//System.out.println("Response failed");
            	 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
			      }
            
				
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			}
		}catch(Exception e){
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			
				if(con!=null)
					con.disconnect();
			}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

}
