package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.GoogleDirectionsResponse;
import com.spoton.pud.data.VehicleEtaModal;
import com.spoton.pud.data.VehicleEtaSheetDetailsModal;
import com.spoton.pud.data.VehicleEtaSheetsModal;
import com.spoton.pud.jpa.VehicleEtaLocationsData;

/**
 * Servlet implementation class GetVehicleEtaData
 */
@WebServlet("/getVehicleEtaData")
public class GetVehicleEtaData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetVehicleEtaData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetVehicleEtaData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START**************");
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		
		String result = "";
		ManageTransaction mt = null;
		Gson gson = new GsonBuilder().serializeNulls().create();
		String scCode=request.getParameter("scCode");
		logger.info(" scCode "+scCode);
		String loggedInEmpCode = request.getParameter("loggedInEmpCode");
		String loggedInEmpBranch = request.getParameter("loggedInEmpBranch");
		String inputParameters="scCode:- "+scCode;
		String outputMessage="";
		try {
			mt=new ManageTransaction();
			if(CommonTasks.check(scCode)){
				
				Calendar currentDateMorning=Calendar.getInstance();
				currentDateMorning.setTime(new Date());
				currentDateMorning.set(Calendar.HOUR_OF_DAY, 00);
				currentDateMorning.set(Calendar.MINUTE, 00);
				currentDateMorning.set(Calendar.SECOND, 00);
				currentDateMorning.set(Calendar.MILLISECOND, 0);
				
				Calendar currentDate=Calendar.getInstance();
				currentDate.setTime(new Date());
				currentDate.set(Calendar.HOUR_OF_DAY, 23);
				currentDate.set(Calendar.MINUTE, 59);
				currentDate.set(Calendar.SECOND, 59);
				currentDate.set(Calendar.MILLISECOND, 0);
				
				 Calendar yesterday=Calendar.getInstance();
					yesterday.add(Calendar.DATE, -1);
					yesterday.set(Calendar.HOUR_OF_DAY, 00);
					yesterday.set(Calendar.MINUTE, 00);
					yesterday.set(Calendar.SECOND, 00);
					yesterday.set(Calendar.MILLISECOND, 0);
					
					String branchQuery="Select latitude,longitude from all_branches where branch_code=?1";
					Query nq=mt.createNativeQuery(branchQuery).setParameter(1, scCode);
					Object[] originLocation=(Object[]) nq.getSingleResult();
					logger.info("Service Centre Location--- Latitude "+(double)originLocation[0]+ " Longitude "+(double)originLocation[1]);
				
				String query="select distinct(vehicle_number) FROM pud.auto_pickup_unloading where is_unloading_started=0 "
						+ "and transaction_result=?1 and created_timestamp>?2 and  created_timestamp<?3 and user_id like ?4";
			Query q=mt.createNativeQuery(query).setParameter(1, "true");
			q.setParameter(2, currentDateMorning.getTime()).setParameter(3, currentDate.getTime());
			q.setParameter(4, scCode+"%");
			List<String> vehiclesList=q.getResultList();
			Map<String,VehicleEtaModal> map=new HashMap<String,VehicleEtaModal>();
			if(vehiclesList!=null&&vehiclesList.size()>0) {
			String q2="Select c.vehicleNumber,c.latValue,c.longValue from ConDetail c where c.vehicleNumber in :1  and c.erpUpdated=1 "
					+ "and c.createdTimestamp >?2 and  c.createdTimestamp<?3 and c.userName like ?4 order by c.createdTimestamp desc";
			TypedQuery<Object[]> tq=mt.createQuery(Object[].class, q2);
			tq.setParameter(1,vehiclesList).setParameter(4, scCode+"%");
			tq.setParameter(2, currentDateMorning.getTime()).setParameter(3, currentDate.getTime());
			List<Object[]> list=tq.getResultList();
			
			for(Object[] o:list) {
				if(!map.containsKey((String) o[0])) {
					VehicleEtaModal vm=new VehicleEtaModal();
					
					vm.setVehicleNumber((String) o[0]);
					vm.setUnloadingSheetDetails(new ArrayList<VehicleEtaSheetsModal>());
					if(o[1]!=null&&o[2]!=null&&originLocation[0]!=null&&originLocation[1]!=null&&(double)originLocation[0]!=0&&(double)originLocation[1]!=0) {
					String eta=calculateEta((double)originLocation[0],(double)originLocation[1],Double.parseDouble((String)o[1]),Double.parseDouble((String)o[2]),logger,gson,mt);
					logger.info("eta "+eta);
					vm.setEstimatedArrivalTime(eta);
						
					}
					map.put((String) o[0], vm);
				}
			}
			// String sheetsQuery="SELECT pickup_unloading_sheet_number,total_cons_count,total_pieces_count,total_con_weight FROM pud.auto_pickup_unloading a "
				   		//+ "where a.transaction_result='true' and a.vehicle_number in :1 and a.user_id like ?2 and a.is_unloading_started=0  and a.created_timestamp>?3 and  a.created_timestamp<?4";
			
			 String sheetsQuery="SELECT  a.pickupUnloadingSheetNumber,a.totalConsCount,a.totalPiecesCount,a.totalConWeight,a.vehicleNumber FROM AutoPickupUnloading a "
				   		+ "where a.transactionResult='true' and a.vehicleNumber in :1 and a.userId like ?2 and a.isUnloadingStarted=0  and a.createdTimestamp>?3 and  a.createdTimestamp<?4";
				TypedQuery<Object[]> tq1=mt.createQuery(Object[].class, sheetsQuery);
				tq1.setParameter(1,vehiclesList).setParameter(2, scCode+"%");
				tq1.setParameter(3, currentDateMorning.getTime()).setParameter(4, currentDate.getTime());
				List<Object[]> sheetList=tq1.getResultList();
			if(sheetList!=null) {
			   for(Object[] o:sheetList) {
				   if(map.containsKey((String) o[4])) {
						VehicleEtaSheetsModal vm=new VehicleEtaSheetsModal();
						vm.setPickupUnloadingSheetNumber((String) o[0]);
						VehicleEtaSheetDetailsModal sheetDetails=new VehicleEtaSheetDetailsModal();
						sheetDetails.setConCount((int) o[1]);
						sheetDetails.setConWeight((double) o[3]);
						sheetDetails.setPiecesCount((int) o[2]);
						vm.setSheetDetails(sheetDetails);
						VehicleEtaModal vehicleData=map.get((String) o[4]);
						vehicleData.getUnloadingSheetDetails().add(vm);
						vehicleData.setTotalConCount(vehicleData.getTotalConCount()+sheetDetails.getConCount());
						vehicleData.setTotalPiecesCount(vehicleData.getTotalPiecesCount()+sheetDetails.getPiecesCount());
						vehicleData.setTotalConWeight(vehicleData.getTotalConWeight()+sheetDetails.getConWeight());
				   }
				}
			}
			}
			if(map.values().size()>0) {
				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,map.values()));
				outputMessage=Constants.REQUEST_COMPLETED;
			}else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
				outputMessage=Constants.ERROR_NO_DATA_AVAILABLE;
			}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA,null));
				outputMessage=Constants.ERROR_INCOMPLETE_DATA;
				}
			
		
		}catch (Exception e) {
				e.printStackTrace();
				logger.error("Exception",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				outputMessage=Constants.ERRORS_EXCEPTION_IN_SERVER;
				
			} finally {
				if (mt != null)
					mt.close();
			}
		//System.out.println("result "+result);
		boolean auditLogStatus=CommonTasks.saveRtmsAuditLog(inputParameters, loggedInEmpBranch, loggedInEmpCode, outputMessage, "Vehicle-ETA Report");
		logger.info("auditLogStatus "+auditLogStatus);
			logger.info("result "+result);
			logger.info("**************END**************");
			response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private String calculateEta(double originLat,double originLong,double destinationLat,double destinationLong,Logger logger ,Gson gson,ManageTransaction mt) {
		String time=null;String duration=null;
		try {
		String query="Select v.duration from vehicle_eta_locations_data v where v.origin_latitude like ?1 and v.origin_longitude like ?2 and "
				+ "v.destination_latitude like ?3 and v.destination_longitude like ?4";
			Query q=mt.createNativeQuery(query).setParameter(1,roundToFourDecimals(originLat)).setParameter(2, roundToFourDecimals(originLong)).setParameter(3, roundToFourDecimals(destinationLat)).setParameter(4, roundToFourDecimals(destinationLong));
			 duration=(String) q.getSingleResult();
		}catch(NoResultException e) {
			logger.info("Not found in cached data ");
			try {
			String URL = "https://maps.googleapis.com/maps/api/directions/json";
		 String origin=String.valueOf(originLat)+","+String.valueOf(originLong);
		 String destination=String.valueOf(destinationLat)+","+String.valueOf(destinationLong);
		 String key="AIzaSyDCqU7Sw_Ks1bjGSSoRMT1FrLjHum3PRLY";//Constants.GOOGLE_API_KEY;
		 URL url = new URL(URL + "?origin="
				    + URLEncoder.encode(origin, "UTF-8") +"&destination=" + URLEncoder.encode(destination, "UTF-8")+"&key="+key);
		logger.info("originLat "+originLat+" originLong "+originLong+ " destinationLat "+destinationLat +" destinationLong "+destinationLong);
		 logger.info("Google URL  "+url);
		 URLConnection conn = url.openConnection();
		  String response1 = null;StringBuilder stringBuilder = new StringBuilder();
		  InputStreamReader streamReader = new InputStreamReader(conn.getInputStream());
       BufferedReader bufferedReader = new BufferedReader(streamReader);
		  while ((response1 = bufferedReader.readLine()) != null) {
           stringBuilder.append(response1);
       }
		 
			GoogleDirectionsResponse res=gson.fromJson(stringBuilder.toString(),GoogleDirectionsResponse.class);
			logger.info("Google api response "+gson.toJson(res));
			 duration=res.getRoutes()[0].getLegs()[0].getDuration().getText();
			if(duration!=null) {
				VehicleEtaLocationsData vd=new VehicleEtaLocationsData();
				vd.setCreatedTimestamp(new Date());
				vd.setDestinationLatitude(roundToFourDecimals(destinationLat));
				vd.setDestinationLongitude(roundToFourDecimals(destinationLong));
				vd.setDuration(duration);
				vd.setOriginLatitude(roundToFourDecimals(originLat));
				vd.setOriginLongitude(roundToFourDecimals(originLong));
				mt.persist(vd);
				mt.commit();
			}
			
			}catch(Exception e1) {
				e1.printStackTrace();
				logger.error("Exception in getting google api response ", e);
			}
		
	}catch(Exception e) {
		e.printStackTrace();
		logger.error("Exception in duration", e);
	}
		logger.info("duration "+duration);
		if(duration!=null) {
		Calendar cd=Calendar.getInstance();
		if(duration.contains("hour")&&duration.contains("mins")) {
			cd.add(Calendar.HOUR_OF_DAY,Integer.parseInt(duration.split(" ")[0].trim()));
			cd.add(Calendar.MINUTE,Integer.parseInt(duration.split(" ")[2].trim()));
		}
		else if(duration.contains("mins")) {
			cd.add(Calendar.MINUTE,Integer.parseInt(duration.split(" ")[0].trim()));
		}
		SimpleDateFormat format=new SimpleDateFormat("HH:mm");
		
		time=format.format(cd.getTime());
		
		}
		logger.info("time "+time);
		return time;
		
	}
	
	private static double roundToFourDecimals(double val){
		return Math.round(val*10000)/10000.0;
	}

}
