package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PayzappLinkRequestErpInputModal;
import com.spoton.pud.data.PayzappLinkRequestErpResponseModal;
import com.spoton.pud.data.PayzappLinkRequestInputModal;
import com.spoton.pud.data.PayzappLinkRequestResponseModal;

/**
 * Servlet implementation class PayzappLinkRequest
 */
@WebServlet("/payzappLinkRequest")
public class PayzappLinkRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PayzappLinkRequest");  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PayzappLinkRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("*****************Start***************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("payzappLinkRequest");
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String datasent="";StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion);
		
		try{
			/*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){*/
			datasent=request.getReader().readLine();
			logger.info("Datasent from Mobile "+datasent);
			if(CommonTasks.check(datasent)){
				
				PayzappLinkRequestInputModal pm=gson.fromJson(datasent, PayzappLinkRequestInputModal.class);
				PayzappLinkRequestErpInputModal  inputData=new PayzappLinkRequestErpInputModal();
				inputData.setConNumber(pm.getConNumber());
				inputData.setCustEmailId(pm.getCustEmailId());
				inputData.setCustMobileNo(pm.getCustMobileNo());
				inputData.setCustName(pm.getCustName());
				inputData.setFlag(pm.getFlag());
				inputData.setInvoiceNumber(pm.getInvoiceNumber());
				logger.info("Datasent to spoton "+gson.toJson(inputData));
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				
				  OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(inputData));
				  streamWriter.flush();
				  con.setConnectTimeout(5000);
					con.setReadTimeout(60000);
					logger.info("Response Code : "+con.getResponseCode());
					 if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
			            	stringBuilder=new StringBuilder();
			                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
			                BufferedReader bufferedReader = new BufferedReader(streamReader);
			                String response1 = null;
			                while ((response1 = bufferedReader.readLine()) != null) {
			                    stringBuilder.append(response1 + "\n");
			                }
			                bufferedReader.close();
			                logger.info("Response from server : "+stringBuilder.toString());
			              //  System.out.println("Response from server : "+stringBuilder.toString());
			                PayzappLinkRequestErpResponseModal erpResponse=gson.fromJson(stringBuilder.toString(),PayzappLinkRequestErpResponseModal.class);
			                if(erpResponse!=null){
			                PayzappLinkRequestResponseModal data=new PayzappLinkRequestResponseModal();
			                data.setErrorMsg(erpResponse.getErrorMsg());
			                data.setErrorStatus(erpResponse.getErrorStatus());
			                data.setResponseRemarks(erpResponse.getResponseRemarks());
			                if(erpResponse.getErrorStatus().equalsIgnoreCase("Success")){
				                result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,data));
				                }else{
				                	 result = gson.toJson(new GeneralResponse(Constants.FALSE,erpResponse.getResponseRemarks(),data));
				                }
			                }
					 } 
					 else{
			            	logger.info("Response failed from Erp");
			            	result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
						      }
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
			}
		/*}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.UPDATE_TO_LATEST_VERSION, null));
		}		*/
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}
		logger.info("Result "+result);
		logger.info("*****************END***************");
		response.getWriter().write(result);
	
		}

	}


