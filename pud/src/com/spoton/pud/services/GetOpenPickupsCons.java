package com.spoton.pud.services;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsModalV3;
import com.spoton.pud.data.EwayBillDetails;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupUpdationModalV3;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.PieceVolumeData;
import com.spoton.pud.data.PieceVolumeModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesImages;
import com.spoton.pud.data.PiecesImagesData;
import com.spoton.pud.jpa.AddedCon;

/**
 * Servlet implementation class GetOpenPickupsCons
 */
@WebServlet("/getOpenPickupsCons")
public class GetOpenPickupsCons extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetOpenPickupsCons");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetOpenPickupsCons() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("*******START********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String pickupScheduleId = request.getParameter("pickupScheduleId");
		String userName = request.getParameter("userName");
		logger.info("pickupScheduleId "+pickupScheduleId+"userName "+userName);
		ManageTransaction mt = null;
		try {
			if ((CommonTasks.check(pickupScheduleId,userName))) {
				mt = new ManageTransaction();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				//To get Piece Entry data
				String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id from added_piece_entry pe,added_cons a "
						+ "where pe.con_id=a.con_entry_id and a.user_name=?1 and a.pickup_schedule_id=?2";	
				Query pieceEntryQuery=mt.createNativeQuery(pieceEntry).setParameter(1, userName).setParameter(2, Integer.parseInt(pickupScheduleId));
				List<Object[]> pieceEntryList=pieceEntryQuery.getResultList();
				PieceEntryModal pieceEntryData=null;
				List<PieceEntryModal> pieceEntryDataList=null;
				Map<Integer,List<PieceEntryModal>> pieceEntryMap=new HashMap<Integer, List<PieceEntryModal>>();
				for(Object[] o:pieceEntryList){
					pieceEntryData=new PieceEntryModal();
					pieceEntryData.setPcrKey(Integer.toString((int) o[0]));
					pieceEntryData.setFromPieceNo((String) o[1]);
					pieceEntryData.setToPieceNo((String) o[2]);
					pieceEntryData.setTotalPieces((String) o[3]);
					if(pieceEntryMap.containsKey((int) o[4])){
						pieceEntryDataList=pieceEntryMap.get((int) o[4]);
						pieceEntryDataList.add(pieceEntryData);
					}else{
						pieceEntryDataList=new ArrayList<PieceEntryModal>();
						pieceEntryDataList.add(pieceEntryData);
						pieceEntryMap.put((int) o[4], pieceEntryDataList);
					}
					
					
				}
				
				//To get Pieces data
				String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id from added_pieces pe,added_cons a "
						+ "where pe.con_id=a.con_entry_id and a.user_name=?1 and a.pickup_schedule_id=?2";
				Query piecesQuery=mt.createNativeQuery(pieces).setParameter(1, userName).setParameter(2, Integer.parseInt(pickupScheduleId));
				List<Object[]> piecesList=piecesQuery.getResultList();
				Pieces piecesData=null;
				List<Pieces> piecesDataList=null;
				Map<Integer,List<Pieces>> piecesDataMap=new HashMap<Integer, List<Pieces>>();
				for(Object[] o:piecesList){
					piecesData=new Pieces();
					piecesData.setPcrKey(Integer.toString((int) o[0]));
					piecesData.setLastPieceNumber((String) o[1]);
					piecesData.setTotalUsed((String) o[2]);
					if(piecesDataMap.containsKey((int) o[3])){
						piecesDataList=piecesDataMap.get((int) o[3]);
						piecesDataList.add(piecesData);
						
					}else{
						piecesDataList=new ArrayList<Pieces>();
						piecesDataList.add(piecesData);
						piecesDataMap.put((int) o[3], piecesDataList);
						
					}
					
				}
				
				//To get Piece Volume data
				String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id from added_piece_volume pe,added_cons a "
						+ "where pe.con_id=a.con_entry_id and a.user_name=?1 and a.pickup_schedule_id=?2";
				Query pieceVolumeQuery=mt.createNativeQuery(pieceVolume).setParameter(1, userName).setParameter(2, Integer.parseInt(pickupScheduleId));
				List<Object[]> pieceVolumeList=pieceVolumeQuery.getResultList();
				PieceVolumeModal pieceVolumeData=null;
				List<PieceVolumeModal> pieceVolumeDataList=null;
				Map<Integer,List<PieceVolumeModal>> pieceVolumeDataMap=new HashMap<Integer, List<PieceVolumeModal>>();
				for(Object[] o:pieceVolumeList){
					pieceVolumeData=new PieceVolumeModal();
					pieceVolumeData.setNoOfPieces((int) o[0]);
					pieceVolumeData.setTotalVolWeight((double) o[1]);
					pieceVolumeData.setVolBreadth((double) o[2]);
					pieceVolumeData.setVolHeight((double) o[3]);
					pieceVolumeData.setVolLength((double) o[4]);
					if(pieceVolumeDataMap.containsKey((int) o[5])){
						pieceVolumeDataList=pieceVolumeDataMap.get((int) o[5]);
						pieceVolumeDataList.add(pieceVolumeData);
					}else{
						pieceVolumeDataList=new ArrayList<PieceVolumeModal>();
					    pieceVolumeDataList.add(pieceVolumeData);
						pieceVolumeDataMap.put((int) o[5], pieceVolumeDataList);
					}
					
				}
				
				//To get Piece Images data
				String pieceImage="select pe.pieces_images_url,pe.con_id from added_pieces_images pe, added_cons a "
						+ "where pe.con_id=a.con_entry_id and a.user_name=?1 and a.pickup_schedule_id=?2";
				Query pieceImageQuery=mt.createNativeQuery(pieceImage).setParameter(1, userName).setParameter(2, Integer.parseInt(pickupScheduleId));
				List<Object[]> pieceImageList=pieceImageQuery.getResultList();
				PiecesImages pieceImageData=null;
				List<PiecesImages> pieceImagesDataList=null;
				Map<Integer,List<PiecesImages>> pieceImagesDataMap=new HashMap<Integer, List<PiecesImages>>();
				for(Object[] o:pieceImageList){
					pieceImageData=new PiecesImages();
					pieceImageData.setPiecesImagesURL((String) o[0]);
					if(pieceImagesDataMap.containsKey((int) o[1])){
						pieceImagesDataList=pieceImagesDataMap.get((int) o[1]);
						pieceImagesDataList.add(pieceImageData);
					}else{
						pieceImagesDataList=new ArrayList<PiecesImages>();
						pieceImagesDataList.add(pieceImageData);
						pieceImagesDataMap.put((int) o[1], pieceImagesDataList);
					}
					
				}
				
				//To get added eway bill numbers
			    String addedEwayBillNumbers="select pe.eway_bill_number,pe.con_id,pe.invoice_number,pe.invoice_amount,pe.is_exempted from added_eway_bill_numbers pe,added_cons a "
						+ "where pe.con_id=a.con_entry_id and a.user_name=?1 and a.pickup_schedule_id=?2";
				Query addedEwayBillNumbersQuery=mt.createNativeQuery(addedEwayBillNumbers).setParameter(1, userName).setParameter(2,Integer.parseInt(pickupScheduleId));
				List<Object[]> ewayNumbersList=addedEwayBillNumbersQuery.getResultList();
				//List<String> ewaysList=null;
				List<EwayBillDetails> ewaysList=null;
				//Map<Integer,List<String>> ewayNumbersDataMap=new HashMap<Integer, List<String>>();
				Map<Integer,List<EwayBillDetails>> ewayNumbersDataMap=new HashMap<Integer, List<EwayBillDetails>>();
				for(Object[] o:ewayNumbersList){
					EwayBillDetails e=new EwayBillDetails();
					e.setEwayBillNum((String)o[0]);
					e.setInvoiceAmount((double) o[3]);
					e.setInvoiceNo((String)o[2]);
					e.setIsExempted((String)o[4]);
					if(ewayNumbersDataMap.containsKey((int) o[1])){
						ewaysList=ewayNumbersDataMap.get((int) o[1]);
						ewaysList.add(e);
					}else{
						ewaysList=new ArrayList<EwayBillDetails>();
						ewaysList.add(e);
						ewayNumbersDataMap.put((int) o[1], ewaysList);
					}
				}
				
				
				
				String query="select a from AddedCon a where a.userName=?1 and a.pickupScheduleId=?2";
				TypedQuery<AddedCon> tq=mt.createQuery(AddedCon.class, query);
				tq.setParameter(1, userName);tq.setParameter(2, Integer.parseInt(pickupScheduleId));
				List<AddedCon> list=tq.getResultList();
				if(list!=null&&list.size()>0){
					List<ConDetailsModalV3> conList=new ArrayList<ConDetailsModalV3>();
					PickupUpdationModalV3 pd=new PickupUpdationModalV3();
					for(AddedCon a:list){
						ConDetailsModalV3 cd=new ConDetailsModalV3();
						cd.setConEntryId(a.getConEntryId());
						cd.setActualWeight(a.getActualWeight());
						cd.setApplyDC(a.getApplyDc());
	    				cd.setApplyNfForm(a.getApplyNfForm());
	    				cd.setApplyVTV(a.getApplyVtv());
	    				cd.setConNumber(a.getConNumber());
	    				cd.setConsignmentType(a.getConsignmentType());
	    				cd.setCrmScheduleId(a.getCrmScheduleId());
	    				cd.setCustomerCode(a.getCustomerCode());
	    				cd.setCustomerName(a.getCustomerName());
	    				cd.setDeclaredValue(a.getDeclaredValue());
	    				cd.setDestinationPinCode(a.getDestinationPincode());
	    			    cd.setGatePassTime(a.getGatePassTime());
	    			    cd.setImage1URL(a.getImage1Url());
	    				cd.setImage2URL(a.getImage2Url());
	    				cd.setLatValue(a.getLatValue());
	    				cd.setLongValue(a.getLongValue());
	    				cd.setNoOfPackage(a.getNoOfPackage());
	    				cd.setOrderNo(a.getOrderNo());
	    				cd.setOriginPinCode(a.getOriginPincode());
	    				cd.setPackageType(a.getPackageType());
	    				cd.setPanNo(a.getPanNo());
	    				cd.setPaymentBasis(a.getPaymentBasis());
	    				cd.setPickupScheduleId(a.getPickupScheduleId());
	    				if(a.getPickupDate()!=null)
	    				cd.setPickupDate(format.format(a.getPickupDate()));
	    				cd.setProduct(a.getProduct());
	    				cd.setReceiverName(a.getReceiverName());
	    				cd.setReceiverPhoneNo(a.getReceiverPhoneNo());
	    				cd.setRefNumber(a.getRefNumber());
	    				cd.setRiskType(a.getRiskType());
	    				cd.setShipmentImageURL(a.getShipmentImageUrl());
	    				cd.setSpecialInstruction(a.getSpecialInstruction());
	    				cd.setTinNo(a.getTinNo());
	    				cd.setTotalVolWeight(a.getTotalVolWeight());
	    				cd.setUserId(a.getUserId());
	    				cd.setUserName(a.getUserName());
	    				cd.setVolType(a.getVolType());
	    				cd.setVtcAmount(a.getVtcAmount());
	    				if(ewayNumbersDataMap.get(a.getConEntryId())!=null){
	        				cd.seteWayBillNumberList(ewayNumbersDataMap.get(a.getConEntryId()));
	        				}else{cd.seteWayBillNumberList(new ArrayList<EwayBillDetails>());}
	    				if(pieceEntryMap.get(a.getConEntryId())!=null)
		    				cd.setPieceEntry(pieceEntryMap.get(a.getConEntryId()));
		    				else{cd.setPieceEntry(new ArrayList<PieceEntryModal>());}
		    				if(piecesDataMap.get(a.getConEntryId())!=null)
		    					cd.setPieces(piecesDataMap.get(a.getConEntryId()));
		    				else{cd.setPieces(new ArrayList<Pieces>());}
		    				if(pieceVolumeDataMap.get(a.getConEntryId())!=null)
		    					cd.setPieceVolume(pieceVolumeDataMap.get(a.getConEntryId()));
		    				else{cd.setPieceVolume(new ArrayList<PieceVolumeModal>());}
		    				if(pieceImagesDataMap.get(a.getConEntryId())!=null)
		    					cd.setPiecesImages(pieceImagesDataMap.get(a.getConEntryId()));
		    				else{cd.setPiecesImages(new ArrayList<PiecesImages>());}
	    				conList.add(cd);
	    				
					}
					
					pd.setConDetails(conList);
					logger.info("pickup updation data "+gson.toJson(pd));
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,pd));
				}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"No Cons Found", null));
				}
			} else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_INCOMPLETE_DATA, null));

			}
		} catch (Exception e) {
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			logger.info("EXCEPTION_IN_SERVER "+e);
		} finally {
			if (mt != null)
				mt.close();
		}
		//System.out.println("result "+result);
		logger.info("GetOpenPickupsCons result "+result);
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
