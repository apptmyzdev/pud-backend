package com.spoton.pud.services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class SaveAuditLogsFromDevice
 */
@WebServlet("/saveAuditLogsFromDevice")
public class SaveAuditLogsFromDevice extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SaveAuditLogsFromDevice");
	 // upload settings
    private static final int MEMORY_THRESHOLD   = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveAuditLogsFromDevice() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
	 logger.info("**************START***********");
	 String userId=request.getParameter("userId");
	 logger.info("userId "+userId);
	 Gson gson = new GsonBuilder().serializeNulls().create();
	 String result="";
	  try {
		// checks if the request actually contains upload file
        if (!ServletFileUpload.isMultipartContent(request)) {
            // if not, we stop here
           
            System.out.println("Error: Form must has enctype=multipart/form-data.");
            logger.info("File is not MultipartContent");
            result = gson.toJson(new GeneralResponse(Constants.FALSE,"File is not MultipartContent",null));
        }
 
        // configures upload settings
        DiskFileItemFactory factory = new DiskFileItemFactory();
        
        // sets memory threshold - beyond which files are stored in disk
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        // sets temporary location to store files
        //factory.setRepository(new File(FilesUtil.getProperty("DeviceAuditLogsFilePath")));
 
        ServletFileUpload upload = new ServletFileUpload(factory);
         
        // sets maximum size of upload file
        upload.setFileSizeMax(MAX_FILE_SIZE);
         
        // sets maximum size of request (include file + form data)
        upload.setSizeMax(MAX_REQUEST_SIZE);
 
        // constructs the directory path to store upload file
        // this path is relative to application's directory
        String uploadPath =FilesUtil.getProperty("DeviceAuditLogsFilePath");/* getServletContext().getRealPath("")
                + File.separator + UPLOAD_DIRECTORY;*/
         System.out.println("uploadPath "+uploadPath);
         logger.info("uploadPath "+uploadPath);
        // creates the directory if it does not exist
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
 
      	            // parses the request's content to extract file data
            @SuppressWarnings("unchecked")
            List<FileItem> formItems = upload.parseRequest(request);
 
            if (formItems != null && formItems.size() > 0) {
                // iterates over form's fields
                for (FileItem item : formItems) {
                    // processes only fields that are not form fields
                    if (!item.isFormField()) {
                        String fileName = new File(item.getName()).getName().concat("_"+userId);
                        String filePath = uploadPath + "/" + fileName;
                        File storeFile = new File(filePath);
                        
                       System.out.println("fileName "+fileName+" filePath "+filePath);
                       logger.info(" filePath "+filePath);
                       logger.info(" fileName "+fileName);
                        // saves the file on disk
                        item.write(storeFile);
                        logger.info("File Saved successfully!");
                        result = gson.toJson(new GeneralResponse(Constants.TRUE,"File Saved successfully at "+filePath,null));
                    }
                }
            }
        } catch (Exception ex) {
            /*request.setAttribute("message",
                    "There was an error: " + ex.getMessage());*/
            logger.info("There was an error: " + ex.getMessage());
            result = gson.toJson(new GeneralResponse(Constants.FALSE,ex.getMessage(),null));
        }
        
        logger.info(" result "+result);
		logger.info("**************END***********");
		response.getWriter().write(result);
        
    }
}
