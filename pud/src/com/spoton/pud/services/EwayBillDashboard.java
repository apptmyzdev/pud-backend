package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.EwayBillDashboardErpModal;
import com.spoton.pud.data.EwayBillDashboardModal;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class EwayBillDashboard
 */
@WebServlet("/ewayBillDashboard")
public class EwayBillDashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("EwayBillDashboard");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EwayBillDashboard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		String userName=request.getParameter("userName");
		String pickupDate=request.getParameter("pickupDate");
		logger.info("userName "+userName+" pickupDate "+pickupDate);
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("ewayBillDashboard");
		StringBuilder stringBuilder = null;
		
		try{
			if(CommonTasks.check(userName,pickupDate)){
			urlString=urlString+userName+"/"+pickupDate;
			
			obj = new URL(urlString);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			 con.setReadTimeout(60000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			logger.info("url called "+urlString);
			 logger.info("ResponseCode "+con.getResponseCode());
			   if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
				   stringBuilder=new StringBuilder();
	                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	                String response1 = null;
	                while ((response1 = bufferedReader.readLine()) != null) {
	                    stringBuilder.append(response1 + "\n");
	                }
	                bufferedReader.close();
	                streamReader.close();
	                logger.info("Response From Spoton : " +stringBuilder.toString());
	                Type responseListType = new TypeToken<ArrayList<EwayBillDashboardErpModal>>(){}.getType();
	                List<EwayBillDashboardErpModal> responseList=gson.fromJson(stringBuilder.toString(),responseListType);
	                if(responseList!=null&&responseList.size()>0){
	                	List<EwayBillDashboardModal> list=new ArrayList<EwayBillDashboardModal>();
	                	for(EwayBillDashboardErpModal e:responseList){
	                		EwayBillDashboardModal ed=new EwayBillDashboardModal();
	                		ed.setConId(e.getCON_ID());
	                		ed.setErrorRemarks(e.getErrorRemarks());
	                		ed.setEwayBillNo(e.getEwayBillNo());
	                		ed.seteWayBillStatus(e.geteWayBillStatus());
	                		ed.setPickupDate(e.getPICKUPDATE());
	                		ed.setPickupOrderNo(e.getPickupOrderNo());
	                		ed.setUserId(e.getUSER_ID());
	                		ed.setValidTill(e.getValidTill());
	                		ed.setVehicleNo(e.getVehicleNo());
	                		list.add(ed);
	                	}
	                	result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, list));
	                }else {
	                	 result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
	                }
			   }else{
	            	
	            //System.out.println("Response failed");
				   logger.info("Response failed from spoton");
	            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
	            
	            }
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_INCOMPLETE_DATA, null));
			}
			}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null){mt.close();}
			if(con!=null) {
				con.disconnect();
			}
		}
		
		
		logger.info("result "+result);
		logger.info("**********END***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
