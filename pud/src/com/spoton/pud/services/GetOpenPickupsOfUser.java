package com.spoton.pud.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.GetOpenPickupsModal;

/**
 * Servlet implementation class GetOpenPickupsOfUser
 */
@WebServlet("/getOpenPickupsOfUser")
public class GetOpenPickupsOfUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetOpenPickupsOfUser");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetOpenPickupsOfUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*******START********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		String pickupDate = request.getParameter("pickupDate");
		String userName = request.getParameter("userName");
		logger.info("pickupDate "+pickupDate+" userName "+userName);
		ManageTransaction mt = null;
		try {
			if ((CommonTasks.check(pickupDate,userName))) {
				mt = new ManageTransaction();
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//String apkQuery = "select max(apk_version) from user_session_data where user_name = ?1";
				String apkQuery ="select apk_version from user_session_data where  user_name = ?1  and created_timestamp = (select max(created_timestamp) from pud.user_session_data where user_name =?1 )";
				
				String query="select distinct(c.pickup_schedule_id),c.pickup_date,c.order_no,c.user_name from pud.added_cons c "
						+ "where c.con_number not in (select d.con_number d from pud.con_details d where d.user_name=?1 and date(d.pickup_date)=?2) and "
						+ "c.user_name=?1 and date(c.pickup_date)=?2";
			   Query q=mt.createNativeQuery(query).setParameter(1, userName).setParameter(2, pickupDate);
			   Query apkNativeQuery = mt.createNativeQuery(apkQuery);
			   apkNativeQuery.setParameter(1, userName);
			   
			   List<Object[]> list=q.getResultList();
			   try{
			   String currentAPK = (String)apkNativeQuery.getSingleResult();
			   
			   logger.info("currentAPK "+currentAPK);
			   if(list!=null&&list.size()>0){
				   logger.info("list size"+list.size());
				   List<GetOpenPickupsModal> data=new ArrayList<GetOpenPickupsModal>();
				   for(Object[] o:list){
					   GetOpenPickupsModal gp=new GetOpenPickupsModal();
					   gp.setPickupScheduleId((int) o[0]);
					   gp.setPickupOrderNumber((String) o[2]);
					   gp.setPickupDate(format.format((Date) o[1]));
					   gp.setUserName((String) o[3]);
					   gp.setCurrentApkVersion(currentAPK);
					   data.add(gp);
				   }
				   result = gson.toJson(new GeneralResponse(Constants.TRUE,
						   Constants.REQUEST_COMPLETED, data));
			   }else{
				   result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"All Pickups Are Closed", null));
			   }
			   }catch(Exception e) {
					e.printStackTrace();
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Invalid User", null));
			   }
			} else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.ERROR_INCOMPLETE_DATA, null));

			}
		} catch (Exception e) {
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			logger.info("EXCEPTION_IN_SERVER "+e);
		} finally {
			if (mt != null)
				mt.close();
		}
	//System.out.println("GetOpenPickupsOfUser result"+result);
		logger.info("GetOpenPickupsOfUser result"+result);
		logger.info("*******END********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
