package com.spoton.pud.data;

public class PayzappMultipleInputErpModal {

	private String PickupScheduleID;
	private String CustName; 
	private String CustMobileNo;  
	private String CustEmailId ;
	private String PUDUserID;
	private String InvoiceNumbers;
	public String getPickupScheduleID() {
		return PickupScheduleID;
	}
	public void setPickupScheduleID(String pickupScheduleID) {
		PickupScheduleID = pickupScheduleID;
	}
	public String getCustName() {
		return CustName;
	}
	public void setCustName(String custName) {
		CustName = custName;
	}
	public String getCustMobileNo() {
		return CustMobileNo;
	}
	public void setCustMobileNo(String custMobileNo) {
		CustMobileNo = custMobileNo;
	}
	public String getCustEmailId() {
		return CustEmailId;
	}
	public void setCustEmailId(String custEmailId) {
		CustEmailId = custEmailId;
	}
	public String getPUDUserID() {
		return PUDUserID;
	}
	public void setPUDUserID(String pUDUserID) {
		PUDUserID = pUDUserID;
	}
	public String getInvoiceNumbers() {
		return InvoiceNumbers;
	}
	public void setInvoiceNumbers(String invoiceNumbers) {
		InvoiceNumbers = invoiceNumbers;
	}
	
	
	
}
