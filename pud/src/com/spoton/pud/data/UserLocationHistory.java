package com.spoton.pud.data;

import java.util.List;


public class UserLocationHistory {

	private List<UserLocationModel> userLocationModels;

	public List<UserLocationModel> getUserLocationModels() {
		return userLocationModels;
	}

	public void setUserLocationModels(List<UserLocationModel> userLocationModels) {
		this.userLocationModels = userLocationModels;
	}

	@Override
	public String toString() {
		return "UserLocationHistory [userLocationModels=" + userLocationModels + "]";
	}
	
	
	
}
