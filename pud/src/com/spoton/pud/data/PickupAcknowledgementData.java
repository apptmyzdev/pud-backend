package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupAcknowledgementData {

	
	@SerializedName("PickupscheduleID")
	private int pickupScheduleId;
	
	@SerializedName("RevaPickUpID")
	private int revaPickupId;
	
	@SerializedName("TransactionResult")
	private String transactionResult;
	
	@SerializedName("TransactionRemarks")
	private String transactionRemarks;

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public int getRevaPickupId() {
		return revaPickupId;
	}

	public void setRevaPickupId(int revaPickupId) {
		this.revaPickupId = revaPickupId;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getTransactionRemarks() {
		return transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}
	
	
	
}
