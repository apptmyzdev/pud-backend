package com.spoton.pud.data;

public class ReassignPickupErpInputModal {
	
	private Integer PickUpScheduleID;
	private String RefNo;
	private String UserID;
	private String ToUserID;
	private String RescheduleDateTime;
	private String Remarks;
	private double GPSValue_LAT;
	private double GPSValue_LON;
	public Integer getPickUpScheduleID() {
		return PickUpScheduleID;
	}
	public void setPickUpScheduleID(Integer pickUpScheduleID) {
		PickUpScheduleID = pickUpScheduleID;
	}
	public String getRefNo() {
		return RefNo;
	}
	public void setRefNo(String refNo) {
		RefNo = refNo;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getToUserID() {
		return ToUserID;
	}
	public void setToUserID(String toUserID) {
		ToUserID = toUserID;
	}
	public String getRescheduleDateTime() {
		return RescheduleDateTime;
	}
	public void setRescheduleDateTime(String rescheduleDateTime) {
		RescheduleDateTime = rescheduleDateTime;
	}
	public String getRemarks() {
		return Remarks;
	}
	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
	public double getGPSValue_LAT() {
		return GPSValue_LAT;
	}
	public void setGPSValue_LAT(double gPSValue_LAT) {
		GPSValue_LAT = gPSValue_LAT;
	}
	public double getGPSValue_LON() {
		return GPSValue_LON;
	}
	public void setGPSValue_LON(double gPSValue_LON) {
		GPSValue_LON = gPSValue_LON;
	}
	
	
	

}
