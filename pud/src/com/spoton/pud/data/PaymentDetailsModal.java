package com.spoton.pud.data;

public class PaymentDetailsModal {
	
	private String InvoiceId;
	private String PayAmount;
	private String Status;
	private String ResponseMessage;
	public String getInvoiceId() {
		return InvoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		InvoiceId = invoiceId;
	}
	public String getPayAmount() {
		return PayAmount;
	}
	public void setPayAmount(String payAmount) {
		PayAmount = payAmount;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getResponseMessage() {
		return ResponseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		ResponseMessage = responseMessage;
	}
	
	
	

}
