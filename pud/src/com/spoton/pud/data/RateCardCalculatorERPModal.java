package com.spoton.pud.data;

public class RateCardCalculatorERPModal {
	
	private double FRT;
	private double DFS;
	private double StateCharge;
	private double DKT;
	private double FOV;
	private double MinCharge;
	private double PDA;
	private double ODA;
	private double HandlingCharge;
	private double OtherRecovery;
	private double FTC;
	private double VTC;
	private double PH_Pickup;
	private double PH_Delv;
	private double DACC;
	private double NF_Form;
	private double DCAC;
	private double GrossAmount;
	private double ServiceTax;
	private double ServiceTaxPercent;
	private double TotalAmount;
	private double ChargedWeight; 
	private double Collection;
	private String ErroCode;
	private String RateModifyEnabled;
	private String Environmental_Surcharges;
	private double ActualAmount;
	
	/*02-09-2020*/
	private float CovidSurcharge;
	
	public double getFRT() {
		return FRT;
	}
	public void setFRT(double fRT) {
		FRT = fRT;
	}
	public double getDFS() {
		return DFS;
	}
	public void setDFS(double dFS) {
		DFS = dFS;
	}
	public double getStateCharge() {
		return StateCharge;
	}
	public void setStateCharge(double stateCharge) {
		StateCharge = stateCharge;
	}
	public double getDKT() {
		return DKT;
	}
	public void setDKT(double dKT) {
		DKT = dKT;
	}
	public double getFOV() {
		return FOV;
	}
	public void setFOV(double fOV) {
		FOV = fOV;
	}
	public double getMinCharge() {
		return MinCharge;
	}
	public void setMinCharge(double minCharge) {
		MinCharge = minCharge;
	}
	public double getPDA() {
		return PDA;
	}
	public void setPDA(double pDA) {
		PDA = pDA;
	}
	public double getODA() {
		return ODA;
	}
	public void setODA(double oDA) {
		ODA = oDA;
	}
	public double getHandlingCharge() {
		return HandlingCharge;
	}
	public void setHandlingCharge(double handlingCharge) {
		HandlingCharge = handlingCharge;
	}
	public double getOtherRecovery() {
		return OtherRecovery;
	}
	public void setOtherRecovery(double otherRecovery) {
		OtherRecovery = otherRecovery;
	}
	public double getFTC() {
		return FTC;
	}
	public void setFTC(double fTC) {
		FTC = fTC;
	}
	public double getVTC() {
		return VTC;
	}
	public void setVTC(double vTC) {
		VTC = vTC;
	}
	public double getPH_Pickup() {
		return PH_Pickup;
	}
	public void setPH_Pickup(double pH_Pickup) {
		PH_Pickup = pH_Pickup;
	}
	public double getPH_Delv() {
		return PH_Delv;
	}
	public void setPH_Delv(double pH_Delv) {
		PH_Delv = pH_Delv;
	}
	public double getDACC() {
		return DACC;
	}
	public void setDACC(double dACC) {
		DACC = dACC;
	}
	public double getNF_Form() {
		return NF_Form;
	}
	public void setNF_Form(double nF_Form) {
		NF_Form = nF_Form;
	}
	public double getDCAC() {
		return DCAC;
	}
	public void setDCAC(double dCAC) {
		DCAC = dCAC;
	}
	public double getGrossAmount() {
		return GrossAmount;
	}
	public void setGrossAmount(double grossAmount) {
		GrossAmount = grossAmount;
	}
	public double getServiceTax() {
		return ServiceTax;
	}
	public void setServiceTax(double serviceTax) {
		ServiceTax = serviceTax;
	}
	public double getTotalAmount() {
		return TotalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		TotalAmount = totalAmount;
	}
	public double getCollection() {
		return Collection;
	}
	public void setCollection(double collection) {
		Collection = collection;
	}
	public String getErroCode() {
		return ErroCode;
	}
	public void setErroCode(String erroCode) {
		ErroCode = erroCode;
	}
	public double getChargedWeight() {
		return ChargedWeight;
	}
	public void setChargedWeight(double chargedWeight) {
		ChargedWeight = chargedWeight;
	}
	public String getRateModifyEnabled() {
		return RateModifyEnabled;
	}
	public void setRateModifyEnabled(String rateModifyEnabled) {
		RateModifyEnabled = rateModifyEnabled;
	}
	public double getServiceTaxPercent() {
		return ServiceTaxPercent;
	}
	public void setServiceTaxPercent(double serviceTaxPercent) {
		ServiceTaxPercent = serviceTaxPercent;
	}
	public String getEnvironmental_Surcharges() {
		return Environmental_Surcharges;
	}
	public void setEnvironmental_Surcharges(String environmental_Surcharges) {
		Environmental_Surcharges = environmental_Surcharges;
	}
	public double getActualAmount() {
		return ActualAmount;
	}
	public void setActualAmount(double actualAmount) {
		ActualAmount = actualAmount;
	}
	
	/** added on Sep 2, 2020 **/
	public float getCovidSurcharge() {
		return CovidSurcharge;
	}
	/** added on Sep 2, 2020 **/
	public void setCovidSurcharge(float covidSurcharge) {
		CovidSurcharge = covidSurcharge;
	}
	
}
