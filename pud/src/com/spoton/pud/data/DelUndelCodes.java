package com.spoton.pud.data;

public class DelUndelCodes {
	int	del_undel_code;
	int	code;
	String short_code;
	String description;
	public int getDel_undel_code() {
		return del_undel_code;
	}
	public void setDel_undel_code(int del_undel_code) {
		this.del_undel_code = del_undel_code;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getShort_code() {
		return short_code;
	}
	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
