package com.spoton.pud.data;

import java.util.List;

import com.spoton.pud.jpa.PickupCancellationMaster;

public class ResponseData {

	private String sessionId;
	private String branch;
	private List<PaymentModeModal> paymentModeList;
	private List<PickupCancelModel> pickupCancelMasterList;
	private List<Relationships> relationshipList;
	private List<DelUndelCodes> delUndelCodes;
	private List<PickupCancelReasonModel> cancelReasonsList;
	
	
	public List<PickupCancelReasonModel> getCancelReasonsList() {
		return cancelReasonsList;
	}
	public void setCancelReasonsList(List<PickupCancelReasonModel> cancelReasonsList) {
		this.cancelReasonsList = cancelReasonsList;
	}
	public List<DelUndelCodes> getDelUndelCodes() {
		return delUndelCodes;
	}
	public void setDelUndelCodes(List<DelUndelCodes> delUndelCodes) {
		this.delUndelCodes = delUndelCodes;
	}
	public List<Relationships> getRelationshipList() {
		return relationshipList;
	}
	public void setRelationshipList(List<Relationships> relationshipList) {
		this.relationshipList = relationshipList;
	}
	public List<PickupCancelModel> getPickupCancelMasterList() {
		return pickupCancelMasterList;
	}
	public void setPickupCancelMasterList(
			List<PickupCancelModel> pickupCancelMasterList) {
		this.pickupCancelMasterList = pickupCancelMasterList;
	}
	public List<PaymentModeModal> getPaymentModeList() {
		return paymentModeList;
	}
	public void setPaymentModeList(List<PaymentModeModal> paymentModeList) {
		this.paymentModeList = paymentModeList;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	/*public int geteWayFlag() {
		return eWayFlag;
	}
	public void seteWayFlag(int eWayFlag) {
		this.eWayFlag = eWayFlag;
	}
	*/
	

}
