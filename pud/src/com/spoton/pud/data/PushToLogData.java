package com.spoton.pud.data;

public class PushToLogData {

	private String deviceId;
	private int command;
	private String status;
	private String timeStamp;
	private String message;
	private int pushMessageId;

	public PushToLogData() {
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public int getCommand() {
		return command;
	}

	public void setCommand(int command) {
		this.command = command;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public PushToLogData(String deviceId, int command, String status,
			String timeStamp, String message) {
		super();
		this.deviceId = deviceId;
		this.command = command;
		this.status = status;
		this.timeStamp = timeStamp;
		this.message = message;
	}

	public int getPushMessageId() {
		return pushMessageId;
	}
	
	public void setPushMessageId(int pushMessageId) {
		this.pushMessageId = pushMessageId;
	}


	@Override
	public String toString() {
		return "PushToLogData [deviceId=" + deviceId + ", command=" + command + ", status=" + status + ", timeStamp="
				+ timeStamp + ", message=" + message + ", pushMessageId=" + pushMessageId + "]";
	}

}