package com.spoton.pud.data;

public class PiecesImages {

	private String  piecesImagesURL;

	public String getPiecesImagesURL() {
		return piecesImagesURL;
	}
	private int id;

	public void setPiecesImagesURL(String piecesImagesURL) {
		this.piecesImagesURL = piecesImagesURL;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
