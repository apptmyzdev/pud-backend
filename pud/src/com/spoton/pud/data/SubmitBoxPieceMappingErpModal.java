package com.spoton.pud.data;

public class SubmitBoxPieceMappingErpModal {
	
	private String BoxNo;
	private String PieceNo;
	private String IsBoxMapping;//Y or N
	private String ConNumber;
	private String ScannedDateTime;
	private String UserID;
	public String getBoxNo() {
		return BoxNo;
	}
	public void setBoxNo(String boxNo) {
		BoxNo = boxNo;
	}
	public String getPieceNo() {
		return PieceNo;
	}
	public void setPieceNo(String pieceNo) {
		PieceNo = pieceNo;
	}
	public String getIsBoxMapping() {
		return IsBoxMapping;
	}
	public void setIsBoxMapping(String isBoxMapping) {
		IsBoxMapping = isBoxMapping;
	}
	public String getConNumber() {
		return ConNumber;
	}
	public void setConNumber(String conNumber) {
		ConNumber = conNumber;
	}
	public String getScannedDateTime() {
		return ScannedDateTime;
	}
	public void setScannedDateTime(String scannedDateTime) {
		ScannedDateTime = scannedDateTime;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}

	
}
