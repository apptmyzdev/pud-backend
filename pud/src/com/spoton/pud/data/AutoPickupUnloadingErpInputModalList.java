package com.spoton.pud.data;

import java.util.Collection;
import java.util.List;

public class AutoPickupUnloadingErpInputModalList {
	
	private Collection<AutoPickupUnloadingErpInputModal> AutoPickupUnloadingInput;

	public Collection<AutoPickupUnloadingErpInputModal> getAutoPickupUnloadingInput() {
		return AutoPickupUnloadingInput;
	}

	public void setAutoPickupUnloadingInput(Collection<AutoPickupUnloadingErpInputModal> collection) {
		AutoPickupUnloadingInput = collection;
	}
	
	

}
