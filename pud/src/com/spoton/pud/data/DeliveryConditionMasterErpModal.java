package com.spoton.pud.data;

public class DeliveryConditionMasterErpModal {
	
	private String CondID;
	private String CondName;
	private String sessionId;
	public String getCondID() {
		return CondID;
	}
	public void setCondID(String condID) {
		CondID = condID;
	}
	public String getCondName() {
		return CondName;
	}
	public void setCondName(String condName) {
		CondName = condName;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
	

}
