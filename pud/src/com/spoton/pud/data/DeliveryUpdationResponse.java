package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class DeliveryUpdationResponse {

	@SerializedName("TransationResult")
	private String transactionResult;
	
	@SerializedName("TransactionID")
	private String transactionId;
	
	@SerializedName("TransationMessage")
	private String transactionMessage;

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionMessage() {
		return transactionMessage;
	}

	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}
	
	
	
}
