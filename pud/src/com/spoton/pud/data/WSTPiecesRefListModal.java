package com.spoton.pud.data;

public class WSTPiecesRefListModal {
	
	private String conNumber;
	private String pieceNo;
	private String pieceRefNo;
	private String pieceSlNo;
	private int id;
	public String getConNumber() {
		return conNumber;
	}
	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}
	public String getPieceNo() {
		return pieceNo;
	}
	public void setPieceNo(String pieceNo) {
		this.pieceNo = pieceNo;
	}
	public String getPieceRefNo() {
		return pieceRefNo;
	}
	public void setPieceRefNo(String pieceRefNo) {
		this.pieceRefNo = pieceRefNo;
	}
	public String getPieceSlNo() {
		return pieceSlNo;
	}
	public void setPieceSlNo(String pieceSlNo) {
		this.pieceSlNo = pieceSlNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
