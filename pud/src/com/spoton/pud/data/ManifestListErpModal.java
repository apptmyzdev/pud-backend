package com.spoton.pud.data;

public class ManifestListErpModal {
	
	private String ManifestNo;
	private String ManifestDate;
	private String ConNo;
	private String CollectedAmount;
	private String CustomerName;
	private String CollectionType;
	public String getManifestNo() {
		return ManifestNo;
	}
	public void setManifestNo(String manifestNo) {
		ManifestNo = manifestNo;
	}
	public String getManifestDate() {
		return ManifestDate;
	}
	public void setManifestDate(String manifestDate) {
		ManifestDate = manifestDate;
	}
	public String getConNo() {
		return ConNo;
	}
	public void setConNo(String conNo) {
		ConNo = conNo;
	}
	public String getCollectedAmount() {
		return CollectedAmount;
	}
	public void setCollectedAmount(String collectedAmount) {
		CollectedAmount = collectedAmount;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCollectionType() {
		return CollectionType;
	}
	public void setCollectionType(String collectionType) {
		CollectionType = collectionType;
	}
	
	

}
