package com.spoton.pud.data;

import java.util.List;

public class DeliveriesModel {
	boolean result;
	String errMsg;
	List<Deliveries> deliveries;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public List<Deliveries> getDeliveries() {
		return deliveries;
	}
	public void setDeliveries(List<Deliveries> deliveries) {
		this.deliveries = deliveries;
	}
	

}
