package com.spoton.pud.data;

import java.util.Date;

public class PrintConDetailsModel
{
	private String refNo;
	private String shipDate;
	private String decValue;
	private String sender;
	private String customerName;
	private String originPincode;
	private String destPincode;
	private String conNum;
	private String originName;
	private String destName;
	private String volume;
	private String weight;
	private String pcs;
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getShipDate() {
		return shipDate;
	}
	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}
	public String getDecValue() {
		return decValue;
	}
	public void setDecValue(String decValue) {
		this.decValue = decValue;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getOriginPincode() {
		return originPincode;
	}
	public void setOriginPincode(String originPincode) {
		this.originPincode = originPincode;
	}
	public String getDestPincode() {
		return destPincode;
	}
	public void setDestPincode(String destPincode) {
		this.destPincode = destPincode;
	}
	public String getConNum() {
		return conNum;
	}
	public void setConNum(String conNum) {
		this.conNum = conNum;
	}
	public String getOriginName() {
		return originName;
	}
	public void setOriginName(String originName) {
		this.originName = originName;
	}
	public String getDestName() {
		return destName;
	}
	public void setDestName(String destName) {
		this.destName = destName;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getPcs() {
		return pcs;
	}
	public void setPcs(String pcs) {
		this.pcs = pcs;
	}
	
}
