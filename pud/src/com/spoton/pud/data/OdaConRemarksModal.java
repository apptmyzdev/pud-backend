/*
 * Decompiled with CFR 0_123.
 */
package com.spoton.pud.data;

public class OdaConRemarksModal {
    private String conNo;
    private String pdc;
    private String userId;
    private String remarks;
    private String remarksDate;
    private float latitude;
    private float longitude;

    public String getConNo() {
        return this.conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarksDate() {
        return this.remarksDate;
    }

    public void setRemarksDate(String remarksDate) {
        this.remarksDate = remarksDate;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getPdc() {
        return this.pdc;
    }

    public void setPdc(String pdc) {
        this.pdc = pdc;
    }
}
