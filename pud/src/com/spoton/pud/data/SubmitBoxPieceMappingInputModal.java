package com.spoton.pud.data;



public class SubmitBoxPieceMappingInputModal {
	
	private String boxNumber;
	private String conNumber;
	private String isBoxPieceMapping;//Y or N
	private String pieceNumber;
	private String scannedDatetime;
	private String userId;
	public String getBoxNumber() {
		return boxNumber;
	}
	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}
	public String getConNumber() {
		return conNumber;
	}
	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}
	public String getIsBoxPieceMapping() {
		return isBoxPieceMapping;
	}
	public void setIsBoxPieceMapping(String isBoxPieceMapping) {
		this.isBoxPieceMapping = isBoxPieceMapping;
	}
	public String getPieceNumber() {
		return pieceNumber;
	}
	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}
	public String getScannedDatetime() {
		return scannedDatetime;
	}
	public void setScannedDatetime(String scannedDatetime) {
		this.scannedDatetime = scannedDatetime;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
	

}
