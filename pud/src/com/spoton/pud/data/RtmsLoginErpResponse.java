package com.spoton.pud.data;

import java.util.List;

public class RtmsLoginErpResponse {

	private List<BranchDetails> BranchDetails;
	private boolean isActive;
	private String ErrorMsg;
	public List<BranchDetails> getBranchDetails() {
		return BranchDetails;
	}
	public void setBranchDetails(List<BranchDetails> branchDetails) {
		BranchDetails = branchDetails;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public String getErrorMsg() {
		return ErrorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		ErrorMsg = errorMsg;
	}
	
	
	
}
