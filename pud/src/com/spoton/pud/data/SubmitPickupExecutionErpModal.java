package com.spoton.pud.data;

public class SubmitPickupExecutionErpModal {
	
	private String ConNumber;
	private String PieceNo;
	private String IsConScanned;
	private String ScannedDateTime;
	private String IsPieceScanned;
	private String UserID;
	public String getConNumber() {
		return ConNumber;
	}
	public void setConNumber(String conNumber) {
		ConNumber = conNumber;
	}
	public String getPieceNo() {
		return PieceNo;
	}
	public void setPieceNo(String pieceNo) {
		PieceNo = pieceNo;
	}
	public String getIsConScanned() {
		return IsConScanned;
	}
	public void setIsConScanned(String isConScanned) {
		IsConScanned = isConScanned;
	}
	public String getScannedDateTime() {
		return ScannedDateTime;
	}
	public void setScannedDateTime(String scannedDateTime) {
		ScannedDateTime = scannedDateTime;
	}
	public String getIsPieceScanned() {
		return IsPieceScanned;
	}
	public void setIsPieceScanned(String isPieceScanned) {
		IsPieceScanned = isPieceScanned;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	
	
	
	

}
