package com.spoton.pud.data;

public class CustomerMasterErpModal {

	private String customerCode ;
	private String customerAddress ;
	private String customerBranch  ;
	private String customerCity ; 
	private String customerName ;
	private String customerType;  
	private String customerMobileNumber ; 
	private String customerEmailId;  
	private double customerLocationLatitude;  
	private double customerLocationLongitude;
	private String Type;
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public String getCustomerBranch() {
		return customerBranch;
	}
	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}
	public String getCustomerCity() {
		return customerCity;
	}
	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}
	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}
	public String getCustomerEmailId() {
		return customerEmailId;
	}
	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}
	public double getCustomerLocationLatitude() {
		return customerLocationLatitude;
	}
	public void setCustomerLocationLatitude(double customerLocationLatitude) {
		this.customerLocationLatitude = customerLocationLatitude;
	}
	public double getCustomerLocationLongitude() {
		return customerLocationLongitude;
	}
	public void setCustomerLocationLongitude(double customerLocationLongitude) {
		this.customerLocationLongitude = customerLocationLongitude;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	
	
	 

	     
}
