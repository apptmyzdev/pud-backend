package com.spoton.pud.data;

import java.util.List;

public class DelUndelCodesModel {
	boolean result;
	String errMsg;
	List<DelUndelCodes> codes;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public List<DelUndelCodes> getCodes() {
		return codes;
	}
	public void setCodes(List<DelUndelCodes> codes) {
		this.codes = codes;
	}
	
	
}
