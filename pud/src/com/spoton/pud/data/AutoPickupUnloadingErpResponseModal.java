package com.spoton.pud.data;

public class AutoPickupUnloadingErpResponseModal {
	
	private String TransationResult;
	private String TransationMessage;
	private String PickupUnloadingSheetNo;
	private int TransactionID;
	public String getTransationResult() {
		return TransationResult;
	}
	public void setTransationResult(String transationResult) {
		TransationResult = transationResult;
	}
	public String getTransationMessage() {
		return TransationMessage;
	}
	public void setTransationMessage(String transationMessage) {
		TransationMessage = transationMessage;
	}
	public String getPickupUnloadingSheetNo() {
		return PickupUnloadingSheetNo;
	}
	public void setPickupUnloadingSheetNo(String pickupUnloadingSheetNo) {
		PickupUnloadingSheetNo = pickupUnloadingSheetNo;
	}
	public int getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(int transactionID) {
		TransactionID = transactionID;
	}
	
	

}
