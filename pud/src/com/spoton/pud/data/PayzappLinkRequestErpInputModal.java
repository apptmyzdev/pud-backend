package com.spoton.pud.data;

public class PayzappLinkRequestErpInputModal {

	private String ConNumber  ;
	private String CustName; 
	private String CustMobileNo;  
	private String CustEmailId ;
	private String InvoiceNumber;  
	private String Flag;
	public String getConNumber() {
		return ConNumber;
	}
	public void setConNumber(String conNumber) {
		ConNumber = conNumber;
	}
	public String getCustName() {
		return CustName;
	}
	public void setCustName(String custName) {
		CustName = custName;
	}
	public String getCustMobileNo() {
		return CustMobileNo;
	}
	public void setCustMobileNo(String custMobileNo) {
		CustMobileNo = custMobileNo;
	}
	public String getCustEmailId() {
		return CustEmailId;
	}
	public void setCustEmailId(String custEmailId) {
		CustEmailId = custEmailId;
	}
	public String getInvoiceNumber() {
		return InvoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		InvoiceNumber = invoiceNumber;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	} 
	
	
	
}
