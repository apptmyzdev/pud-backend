package com.spoton.pud.data;

import java.util.List;

public class VehicleEtaModal {
	
	
	private String vehicleNumber;
	private String estimatedArrivalTime;
	private int totalConCount;
	private int totalPiecesCount;
	private double totalConWeight;
	List<VehicleEtaSheetsModal> unloadingSheetDetails;
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getEstimatedArrivalTime() {
		return estimatedArrivalTime;
	}
	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		this.estimatedArrivalTime = estimatedArrivalTime;
	}
	public List<VehicleEtaSheetsModal> getUnloadingSheetDetails() {
		return unloadingSheetDetails;
	}
	public void setUnloadingSheetDetails(List<VehicleEtaSheetsModal> unloadingSheetDetails) {
		this.unloadingSheetDetails = unloadingSheetDetails;
	}
	public int getTotalConCount() {
		return totalConCount;
	}
	public void setTotalConCount(int totalConCount) {
		this.totalConCount = totalConCount;
	}
	public int getTotalPiecesCount() {
		return totalPiecesCount;
	}
	public void setTotalPiecesCount(int totalPiecesCount) {
		this.totalPiecesCount = totalPiecesCount;
	}
	public double getTotalConWeight() {
		return totalConWeight;
	}
	public void setTotalConWeight(double totalConWeight) {
		this.totalConWeight = totalConWeight;
	}
	
	

}
