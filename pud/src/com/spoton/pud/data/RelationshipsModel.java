package com.spoton.pud.data;

import java.util.List;

public class RelationshipsModel {
	boolean result;
	String errMsg;
	List<Relationships> codes;
	public boolean isResult() {
		return result;
	}
	public void setResult(boolean result) {
		this.result = result;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public List<Relationships> getCodes() {
		return codes;
	}
	public void setCodes(List<Relationships> codes) {
		this.codes = codes;
	}
	
}
