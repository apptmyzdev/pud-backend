package com.spoton.pud.data;

import com.google.gson.Gson;

import java.util.List;



public class PiecesPrePrintModal {
    private String conNumber;
    private String origin;
    private String destination;
   
    private int isLabelsPrePrinted;
    private List<Pieces> pieces;
    private List<PieceEntryModal> pieceEntry;
    private String isUpdated;
    private String originPincode;
    private String destinationPincode;
    private String userId;

    public PiecesPrePrintModal() {
        super();
    }

   

    

    public String getConNumber() {
        return conNumber;
    }

    public void setConNumber(String conNumber) {
        this.conNumber = conNumber;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    

   
    public int getLabelsPrePrinted() {
        return isLabelsPrePrinted;
    }

    public void setLabelsPrePrinted(int labelsPrePrinted) {
        isLabelsPrePrinted = labelsPrePrinted;
    }

    public List<Pieces> getPieces() {
        return pieces;
    }

    public void setPieces(List<Pieces> pieces) {
        this.pieces = pieces;
    }

    public List<PieceEntryModal> getPieceEntry() {
        return pieceEntry;
    }

    public void setPieceEntry(List<PieceEntryModal> pieceEntry) {
        this.pieceEntry = pieceEntry;
    }

    public String getIsUpdated() {
        return isUpdated;
    }

    public void setIsUpdated(String isUpdated) {
        this.isUpdated = isUpdated;
    }

    public int getIsLabelsPrePrinted() {
        return isLabelsPrePrinted;
    }

    public void setIsLabelsPrePrinted(int isLabelsPrePrinted) {
        this.isLabelsPrePrinted = isLabelsPrePrinted;
    }

    public String getOriginPincode() {
        return originPincode;
    }

    public void setOriginPincode(String originPincode) {
        this.originPincode = originPincode;
    }

    public String getDestinationPincode() {
        return destinationPincode;
    }

    public void setDestinationPincode(String destinationPincode) {
        this.destinationPincode = destinationPincode;
    }
    
    

    public String getUserId() {
		return userId;
	}





	public void setUserId(String userId) {
		this.userId = userId;
	}





	@Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
