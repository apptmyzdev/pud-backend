package com.spoton.pud.data;



public class Pieces {

	
	private String pcrKey; 
	
	
	private String totalUsed; 
	
	
	private String lastPieceNumber;
	
	private int id;

	public String getPcrKey() {
		return pcrKey;
	}

	public void setPcrKey(String pcrKey) {
		this.pcrKey = pcrKey;
	}

	public String getTotalUsed() {
		return totalUsed;
	}

	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}

	public String getLastPieceNumber() {
		return lastPieceNumber;
	}

	public void setLastPieceNumber(String lastPieceNumber) {
		this.lastPieceNumber = lastPieceNumber;
	} 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
