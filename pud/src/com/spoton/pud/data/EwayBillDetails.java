package com.spoton.pud.data;



public class EwayBillDetails {

	private String ewayBillNum;
	private  String invoiceNo; 
	private  double invoiceAmount; 
	private String isExempted;
	private int id;
	public String getEwayBillNum() {
		return ewayBillNum;
	}
	public void setEwayBillNum(String ewayBillNum) {
		this.ewayBillNum = ewayBillNum;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getIsExempted() {
		return isExempted;
	}
	public void setIsExempted(String isExempted) {
		this.isExempted = isExempted;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
}
