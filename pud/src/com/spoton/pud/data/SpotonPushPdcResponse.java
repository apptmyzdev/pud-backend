package com.spoton.pud.data;

import java.util.List;

public class SpotonPushPdcResponse {
	List<SpotonAWBDataResponse> pushPdcResponse;

	public List<SpotonAWBDataResponse> getPushPdcResponse() {
		return pushPdcResponse;
	}

	public void setPushPdcResponse(List<SpotonAWBDataResponse> pushPdcResponse) {
		this.pushPdcResponse = pushPdcResponse;
	}
	
	
}
