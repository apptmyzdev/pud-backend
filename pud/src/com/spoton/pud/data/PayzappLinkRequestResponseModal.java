package com.spoton.pud.data;

public class PayzappLinkRequestResponseModal {

	private String errorStatus;  
	private String errorMsg ; 
	private String responseRemarks;
	public String getErrorStatus() {
		return errorStatus;
	}
	public void setErrorStatus(String errorStatus) {
		this.errorStatus = errorStatus;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getResponseRemarks() {
		return responseRemarks;
	}
	public void setResponseRemarks(String responseRemarks) {
		this.responseRemarks = responseRemarks;
	}
	
	
	
	
	
}
