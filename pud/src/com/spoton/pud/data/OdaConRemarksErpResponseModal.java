/*
 * Decompiled with CFR 0_123.
 */
package com.spoton.pud.data;

public class OdaConRemarksErpResponseModal {
    private boolean TransationResult;
    private String TransationMessage;
    private int TransactionID;

    public boolean getTransationResult() {
        return this.TransationResult;
    }

    public void setTransationResult(boolean transationResult) {
        this.TransationResult = transationResult;
    }

    public String getTransationMessage() {
        return this.TransationMessage;
    }

    public void setTransationMessage(String transationMessage) {
        this.TransationMessage = transationMessage;
    }

    public int getTransactionID() {
        return this.TransactionID;
    }

    public void setTransactionID(int transactionID) {
        this.TransactionID = transactionID;
    }
}
