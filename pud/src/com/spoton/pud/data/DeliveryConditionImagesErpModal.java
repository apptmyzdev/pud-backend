package com.spoton.pud.data;

public class DeliveryConditionImagesErpModal {
	
	private String PieceNo;
	private String ImageUrl;
	public String getPieceNo() {
		return PieceNo;
	}
	public void setPieceNo(String pieceNo) {
		PieceNo = pieceNo;
	}
	public String getImageUrl() {
		return ImageUrl;
	}
	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}
	
	

}
