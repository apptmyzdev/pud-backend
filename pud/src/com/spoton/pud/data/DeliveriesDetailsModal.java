package com.spoton.pud.data;

import java.math.BigDecimal;

public class DeliveriesDetailsModal {
	
	private long conNumber;
	private BigDecimal conWt;
	private String consigneeName;
	private int pcsCount;
	private double latValue;
	private double lonValue;
	private String pdc;
	private String status;
	private String consigneeAddress;
	public String getConsigneeAddress() {
		return consigneeAddress;
	}
	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}
	public long getConNumber() {
		return conNumber;
	}
	public void setConNumber(long conNumber) {
		this.conNumber = conNumber;
	}
	public BigDecimal getConWt() {
		return conWt;
	}
	public void setConWt(BigDecimal conWt) {
		this.conWt = conWt;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	public int getPcsCount() {
		return pcsCount;
	}
	public void setPcsCount(int pcsCount) {
		this.pcsCount = pcsCount;
	}
	public double getLatValue() {
		return latValue;
	}
	public void setLatValue(double latValue) {
		this.latValue = latValue;
	}
	public double getLonValue() {
		return lonValue;
	}
	public void setLonValue(double lonValue) {
		this.lonValue = lonValue;
	}
	public String getPdc() {
		return pdc;
	}
	public void setPdc(String pdc) {
		this.pdc = pdc;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
