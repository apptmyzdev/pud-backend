package com.spoton.pud.data;

public class VehiclePickupsAndDeliveries {
	
	private long totalCount;
	private double totalWeight;
	private double currentUsage;
	private double projectedFree;
	public long getTotalPickupsCount() {
		return totalCount;
	}
	
	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public double getCurrentUsage() {
		return currentUsage;
	}
	public void setCurrentUsage(double currentUsage) {
		this.currentUsage = currentUsage;
	}
	public double getProjectedFree() {
		return projectedFree;
	}
	public void setProjectedFree(double projectedFree) {
		this.projectedFree = projectedFree;
	}
	
	

}
