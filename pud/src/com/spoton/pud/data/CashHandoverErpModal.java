package com.spoton.pud.data;

import java.util.Date;

public class CashHandoverErpModal {
	
	private String Dockno;
	private String CollectionType;
	private String ShipmentType;
	private Float CollectedAmt;
	private String CreatedDate;
	public String getDockno() {
		return Dockno;
	}
	public void setDockno(String dockno) {
		Dockno = dockno;
	}
	public String getCollectionType() {
		return CollectionType;
	}
	public void setCollectionType(String collectionType) {
		CollectionType = collectionType;
	}
	public String getShipmentType() {
		return ShipmentType;
	}
	public void setShipmentType(String shipmentType) {
		ShipmentType = shipmentType;
	}
	public Float getCollectedAmt() {
		return CollectedAmt;
	}
	public void setCollectedAmt(Float collectedAmt) {
		CollectedAmt = collectedAmt;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	
	

}
