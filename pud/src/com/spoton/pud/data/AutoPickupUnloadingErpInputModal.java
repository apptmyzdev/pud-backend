package com.spoton.pud.data;

import java.util.List;

public class AutoPickupUnloadingErpInputModal {
	
	private String UserID;
	private String VehicleNo;
	private int TransactionID;
	private List<String> ConNo;
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	public String getVehicleNo() {
		return VehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}
	public int getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(int transactionID) {
		TransactionID = transactionID;
	}
	public List<String> getConNo() {
		return ConNo;
	}
	public void setConNo(List<String> conNo) {
		ConNo = conNo;
	}
	
	
	

}
