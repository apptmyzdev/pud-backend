package com.spoton.pud.data;

import java.util.List;

public class PostCashHandoverDetailsErpModal {
	
	private List<String> Dockno;
	private String EmpCode;
	private String UserID;
	public List<String> getDockno() {
		return Dockno;
	}
	public void setDockno(List<String> dockno) {
		Dockno = dockno;
	}
	public String getEmpCode() {
		return EmpCode;
	}
	public void setEmpCode(String empCode) {
		EmpCode = empCode;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}
	
	

}
