package com.spoton.pud.data;

import java.util.List;

public class PiecesPrePrintErpModal {
	
	 private String ConNo;
	    private String Origin;
	    private String Destination;
	   
	   
	    private List<PiecesData> Pieces;
	    private List<PieceEntryData> PieceEntry;
	   
	    private String OriginPincode;
	    private String DestinationPincode;
	    private String UserID;
		public String getConNo() {
			return ConNo;
		}
		public void setConNo(String conNo) {
			ConNo = conNo;
		}
		public String getOrigin() {
			return Origin;
		}
		public void setOrigin(String origin) {
			Origin = origin;
		}
		public String getDestination() {
			return Destination;
		}
		public void setDestination(String destination) {
			Destination = destination;
		}
		
		
		public List<PiecesData> getPieces() {
			return Pieces;
		}
		public void setPieces(List<PiecesData> pieces) {
			Pieces = pieces;
		}
		public List<PieceEntryData> getPieceEntry() {
			return PieceEntry;
		}
		public void setPieceEntry(List<PieceEntryData> pieceEntry) {
			PieceEntry = pieceEntry;
		}
		public String getOriginPincode() {
			return OriginPincode;
		}
		public void setOriginPincode(String originPincode) {
			OriginPincode = originPincode;
		}
		public String getDestinationPincode() {
			return DestinationPincode;
		}
		public void setDestinationPincode(String destinationPincode) {
			DestinationPincode = destinationPincode;
		}
		public String getUserID() {
			return UserID;
		}
		public void setUserID(String userID) {
			UserID = userID;
		}
	    
	    
	    

}
