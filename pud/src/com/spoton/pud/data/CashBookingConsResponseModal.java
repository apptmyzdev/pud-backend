package com.spoton.pud.data;

public class CashBookingConsResponseModal {

	private String conEntryID;
	private String flag;
	private String statusDetail;
	private String invno;
	private String invDate;
	private String spotonGst;
	private String customerGst;
	private String conno;
	private String actwt;
	private String chrgwt;
	private String totalPkgs;
	private String taxAmount;
	private String orgCode;
	private String destCode;
	private String chargeAmount;
	private String cGst;
	private String sGst;
	private String uGst;
	private String iGst;
	private String netAmt;
	private String spotonAddrees;
	private String customerEmail;  
	private String customerPhone;  
	private String customerGstin;  
	private String spotonGSTStateName;  
	private String shipperGSTStateName ;  
	
	public String getConEntryID() {
		return conEntryID;
	}
	public void setConEntryID(String conEntryID) {
		this.conEntryID = conEntryID;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getStatusDetail() {
		return statusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		this.statusDetail = statusDetail;
	}
	public String getInvno() {
		return invno;
	}
	public void setInvno(String invno) {
		this.invno = invno;
	}
	public String getInvDate() {
		return invDate;
	}
	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}
	public String getSpotonGst() {
		return spotonGst;
	}
	public void setSpotonGst(String spotonGst) {
		this.spotonGst = spotonGst;
	}
	public String getCustomerGst() {
		return customerGst;
	}
	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}
	public String getConno() {
		return conno;
	}
	public void setConno(String conno) {
		this.conno = conno;
	}
	public String getActwt() {
		return actwt;
	}
	public void setActwt(String actwt) {
		this.actwt = actwt;
	}
	public String getChrgwt() {
		return chrgwt;
	}
	public void setChrgwt(String chrgwt) {
		this.chrgwt = chrgwt;
	}
	public String getTotalPkgs() {
		return totalPkgs;
	}
	public void setTotalPkgs(String totalPkgs) {
		this.totalPkgs = totalPkgs;
	}
	public String getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getDestCode() {
		return destCode;
	}
	public void setDestCode(String destCode) {
		this.destCode = destCode;
	}
	public String getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	public String getcGst() {
		return cGst;
	}
	public void setcGst(String cGst) {
		this.cGst = cGst;
	}
	public String getsGst() {
		return sGst;
	}
	public void setsGst(String sGst) {
		this.sGst = sGst;
	}
	public String getuGst() {
		return uGst;
	}
	public void setuGst(String uGst) {
		this.uGst = uGst;
	}
	public String getiGst() {
		return iGst;
	}
	public void setiGst(String iGst) {
		this.iGst = iGst;
	}
	public String getNetAmt() {
		return netAmt;
	}
	public void setNetAmt(String netAmt) {
		this.netAmt = netAmt;
	}
	public String getSpotonAddrees() {
		return spotonAddrees;
	}
	public void setSpotonAddrees(String spotonAddrees) {
		this.spotonAddrees = spotonAddrees;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerGstin() {
		return customerGstin;
	}
	public void setCustomerGstin(String customerGstin) {
		this.customerGstin = customerGstin;
	}
	public String getSpotonGSTStateName() {
		return spotonGSTStateName;
	}
	public void setSpotonGSTStateName(String spotonGSTStateName) {
		this.spotonGSTStateName = spotonGSTStateName;
	}
	public String getShipperGSTStateName() {
		return shipperGSTStateName;
	}
	public void setShipperGSTStateName(String shipperGSTStateName) {
		this.shipperGSTStateName = shipperGSTStateName;
	}
	
	
	
}
