package com.spoton.pud.data;

public class GetPiecesModal {

	private int id;
	private int pkey;
	private String complete;
	private String endNo;
	private String lastPieceNo;
	private String seriesType;
	private String startNo;
	private String totalIsssued;
	private String totalUsed;
	private String userId;
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPkey() {
		return pkey;
	}
	public void setPkey(int pkey) {
		this.pkey = pkey;
	}
	public String getComplete() {
		return complete;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getEndNo() {
		return endNo;
	}
	public void setEndNo(String endNo) {
		this.endNo = endNo;
	}
	public String getLastPieceNo() {
		return lastPieceNo;
	}
	public void setLastPieceNo(String lastPieceNo) {
		this.lastPieceNo = lastPieceNo;
	}
	public String getSeriesType() {
		return seriesType;
	}
	public void setSeriesType(String seriesType) {
		this.seriesType = seriesType;
	}
	public String getStartNo() {
		return startNo;
	}
	public void setStartNo(String startNo) {
		this.startNo = startNo;
	}
	public String getTotalIsssued() {
		return totalIsssued;
	}
	public void setTotalIsssued(String totalIsssued) {
		this.totalIsssued = totalIsssued;
	}
	public String getTotalUsed() {
		return totalUsed;
	}
	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
}
