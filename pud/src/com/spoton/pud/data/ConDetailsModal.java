package com.spoton.pud.data;

import java.util.List;





public class ConDetailsModal {
	
	
	private int conEntryId;
	
	
	private String conNumber;  

	
	private String refNumber;  
	
	
	private String pickupDate;  
	
	
	private String product;  
	
	
	private String originPinCode;  
	
	
	
	private String destinationPinCode;  
	
	
	private String paymentBasis;  
	
	
	private String customerCode;  
	
	
	private String noOfPackage;  
	
	
	private String actualWeight;  
	
	
	private String applyVTV;  
	
	
	private String vtcAmount;  
	
	
	private String applyDC;  
	
	
	private String applyNfForm;  
	
	
	private String riskType;  
	
	
	private String declaredValue;  
	
	
	private String specialInstruction;  
	
	
	private String latValue;  
	
	
	private String longValue;  
	
	
	private String userId;  
	

	private String orderNo;  
	
	
	private String crmScheduleId;  
	
	
	private String userName;  
	
	
	private String receiverName;  
	
	
	private String receiverPhoneNo;  
	
	
	private String packageType;  
	
	
	private String consignmentType;  
	
	
	private String totalVolWeight;  
	
	
	private String volType;  
	
	
	private String tinNo;  
	
	
	private String panNo;  
	
	
	private String image1URL;  
	
	
	private String image2URL;  
	
	
	private String shipmentImageURL;  
	
	private int erpUpdated;
	private String gatePassTime;  
	
	private boolean scanned;  
	
	private String customerName;
	
	private  List<Pieces> pieces;  
	
	
	private  List<PieceEntryModal> pieceEntry;  
	
	
	private  List<PieceVolumeModal> pieceVolume;

	private  List<PiecesImages> piecesImages;
	
	private int pickupScheduleId;

	public int getConEntryId() {
		return conEntryId;
	}


	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}


	public String getConNumber() {
		return conNumber;
	}


	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}


	public String getRefNumber() {
		return refNumber;
	}


	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}


	public String getPickupDate() {
		return pickupDate;
	}


	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}


	public String getProduct() {
		return product;
	}


	public void setProduct(String product) {
		this.product = product;
	}


	public String getOriginPinCode() {
		return originPinCode;
	}


	public void setOriginPinCode(String originPinCode) {
		this.originPinCode = originPinCode;
	}


	public String getDestinationPinCode() {
		return destinationPinCode;
	}


	public void setDestinationPinCode(String destinationPinCode) {
		this.destinationPinCode = destinationPinCode;
	}


	public String getPaymentBasis() {
		return paymentBasis;
	}


	public void setPaymentBasis(String paymentBasis) {
		this.paymentBasis = paymentBasis;
	}


	public String getCustomerCode() {
		return customerCode;
	}


	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}


	public String getNoOfPackage() {
		return noOfPackage;
	}


	public void setNoOfPackage(String noOfPackage) {
		this.noOfPackage = noOfPackage;
	}


	public String getActualWeight() {
		return actualWeight;
	}


	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}


	public String getApplyVTV() {
		return applyVTV;
	}


	public void setApplyVTV(String applyVTV) {
		this.applyVTV = applyVTV;
	}


	public String getVtcAmount() {
		return vtcAmount;
	}


	public void setVtcAmount(String vtcAmount) {
		this.vtcAmount = vtcAmount;
	}


	public String getApplyDC() {
		return applyDC;
	}


	public void setApplyDC(String applyDC) {
		this.applyDC = applyDC;
	}


	public String getApplyNfForm() {
		return applyNfForm;
	}


	public void setApplyNfForm(String applyNfForm) {
		this.applyNfForm = applyNfForm;
	}


	public String getRiskType() {
		return riskType;
	}


	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}


	public String getDeclaredValue() {
		return declaredValue;
	}


	public void setDeclaredValue(String declaredValue) {
		this.declaredValue = declaredValue;
	}


	public String getSpecialInstruction() {
		return specialInstruction;
	}


	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}


	public String getLatValue() {
		return latValue;
	}


	public void setLatValue(String latValue) {
		this.latValue = latValue;
	}


	public String getLongValue() {
		return longValue;
	}


	public void setLongValue(String longValue) {
		this.longValue = longValue;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getOrderNo() {
		return orderNo;
	}


	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}


	public String getCrmScheduleId() {
		return crmScheduleId;
	}


	public void setCrmScheduleId(String crmScheduleId) {
		this.crmScheduleId = crmScheduleId;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getReceiverName() {
		return receiverName;
	}


	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}


	public String getReceiverPhoneNo() {
		return receiverPhoneNo;
	}


	public void setReceiverPhoneNo(String receiverPhoneNo) {
		this.receiverPhoneNo = receiverPhoneNo;
	}


	public String getPackageType() {
		return packageType;
	}


	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}


	public String getConsignmentType() {
		return consignmentType;
	}


	public void setConsignmentType(String consignmentType) {
		this.consignmentType = consignmentType;
	}


	public String getTotalVolWeight() {
		return totalVolWeight;
	}


	public void setTotalVolWeight(String totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}


	public String getVolType() {
		return volType;
	}


	public void setVolType(String volType) {
		this.volType = volType;
	}


	public String getTinNo() {
		return tinNo;
	}


	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}


	public String getPanNo() {
		return panNo;
	}


	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}


	public String getImage1URL() {
		return image1URL;
	}


	public void setImage1URL(String image1url) {
		image1URL = image1url;
	}


	public String getImage2URL() {
		return image2URL;
	}


	public void setImage2URL(String image2url) {
		image2URL = image2url;
	}


	public String getShipmentImageURL() {
		return shipmentImageURL;
	}


	public void setShipmentImageURL(String shipmentImageURL) {
		this.shipmentImageURL = shipmentImageURL;
	}


	public String getGatePassTime() {
		return gatePassTime;
	}


	public void setGatePassTime(String gatePassTime) {
		this.gatePassTime = gatePassTime;
	}


	public List<Pieces> getPieces() {
		return pieces;
	}


	public void setPieces(List<Pieces> pieces) {
		this.pieces = pieces;
	}


	public List<PieceEntryModal> getPieceEntry() {
		return pieceEntry;
	}


	public void setPieceEntry(List<PieceEntryModal> pieceEntry) {
		this.pieceEntry = pieceEntry;
	}


	public List<PieceVolumeModal> getPieceVolume() {
		return pieceVolume;
	}


	public void setPieceVolume(List<PieceVolumeModal> pieceVolume) {
		this.pieceVolume = pieceVolume;
	}


	public List<PiecesImages> getPiecesImages() {
		return piecesImages;
	}


	public void setPiecesImages(List<PiecesImages> piecesImages) {
		this.piecesImages = piecesImages;
	}


	public boolean isScanned() {
		return scanned;
	}


	public void setScanned(boolean isScanned) {
		this.scanned = isScanned;
	}


	public int getPickupScheduleId() {
		return pickupScheduleId;
	}


	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public int getErpUpdated() {
		return erpUpdated;
	}


	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	
	
	
}
	
	
	
	

