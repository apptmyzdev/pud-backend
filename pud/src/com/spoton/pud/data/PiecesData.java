package com.spoton.pud.data;

public class PiecesData {

private String PCR_Key; 
	
private String TotalUsed; 
	
private String LastPieceNumber;

private int id;

public String getPCR_Key() {
	return PCR_Key;
}

public void setPCR_Key(String pCR_Key) {
	PCR_Key = pCR_Key;
}

public String getTotalUsed() {
	return TotalUsed;
}

public void setTotalUsed(String totalUsed) {
	TotalUsed = totalUsed;
}

public String getLastPieceNumber() {
	return LastPieceNumber;
}

public void setLastPieceNumber(String lastPieceNumber) {
	LastPieceNumber = lastPieceNumber;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}



	
}
