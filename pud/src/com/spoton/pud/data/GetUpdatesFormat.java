
package com.spoton.pud.data;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Reva
 *
 */
public class GetUpdatesFormat implements Type {
	private boolean staus;
	private String msg;
	private int version;
	private String url;
	private String rtmsBaseUrl;
	private String notes;
	private List<UrlData> urlData;
	private boolean covid;

	public GetUpdatesFormat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean isStaus() {
		return staus;
	}

	public void setStaus(boolean staus) {
		this.staus = staus;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public List<UrlData> getUrlData() {
		return urlData;
	}

	public void setUrlData(List<UrlData> urlData) {
		this.urlData = urlData;
	}
	
	

	public boolean isCovid() {
		return covid;
	}

	public void setCovid(boolean covid) {
		this.covid = covid;
	}

	public GetUpdatesFormat(boolean staus, String msg, int version, String url,
			String notes) {
		super();
		this.staus = staus;
		this.msg = msg;
		this.version = version;
		this.url = url;
		this.notes = notes;
	}
	
	

	public String getRtmsBaseUrl() {
		return rtmsBaseUrl;
	}

	public void setRtmsBaseUrl(String rtmsBaseUrl) {
		this.rtmsBaseUrl = rtmsBaseUrl;
	}

	@Override
	public String toString() {
		return "GetUpdatesFormat [staus=" + staus + ", msg=" + msg
				+ ", version=" + version + ", url=" + url + ", notes=" + notes
				+ "]";
	}

}
