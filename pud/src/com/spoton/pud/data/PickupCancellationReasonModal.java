package com.spoton.pud.data;



import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class PickupCancellationReasonModal {
	
	@SerializedName("ISACTIVE") 
	private boolean activeStatus;
	
	@SerializedName("FailureCategory") 
	private String failureCategory;
	
	@SerializedName("PickupStatusId") 
	private int pickupStatusId;
	
	@SerializedName("ReasonCode") 
	private String reasonCode;
	
	@SerializedName("ReasonDesc") 
	private String reasonDesc;
	
	@SerializedName("UPDTBY") 
	private String updatedBy;
	
	@SerializedName("UPDTON") 
	private Date updatedOn;

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getFailureCategory() {
		return failureCategory;
	}

	public void setFailureCategory(String failureCategory) {
		this.failureCategory = failureCategory;
	}

	public int getPickupStatusId() {
		return pickupStatusId;
	}

	public void setPickupStatusId(int pickupStatusId) {
		this.pickupStatusId = pickupStatusId;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
	
}
