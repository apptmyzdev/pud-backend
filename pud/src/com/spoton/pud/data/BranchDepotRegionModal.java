package com.spoton.pud.data;

public class BranchDepotRegionModal {
	private String Branch_Code;
	private String Branch_Name;
	private String Depot_Code;
	private String Depot_Name;
	private String Region_Code;
	private String Region_Name;
	private String Result;
	private String ErrMsg;
	public String getBranch_Code() {
		return Branch_Code;
	}
	public void setBranch_Code(String branch_Code) {
		Branch_Code = branch_Code;
	}
	public String getBranch_Name() {
		return Branch_Name;
	}
	public void setBranch_Name(String branch_Name) {
		Branch_Name = branch_Name;
	}
	public String getDepot_Code() {
		return Depot_Code;
	}
	public void setDepot_Code(String depot_Code) {
		Depot_Code = depot_Code;
	}
	public String getDepot_Name() {
		return Depot_Name;
	}
	public void setDepot_Name(String depot_Name) {
		Depot_Name = depot_Name;
	}
	public String getRegion_Code() {
		return Region_Code;
	}
	public void setRegion_Code(String region_Code) {
		Region_Code = region_Code;
	}
	public String getRegion_Name() {
		return Region_Name;
	}
	public void setRegion_Name(String region_Name) {
		Region_Name = region_Name;
	}
	public String getResult() {
		return Result;
	}
	public void setResult(String result) {
		Result = result;
	}
	public String getErrMsg() {
		return ErrMsg;
	}
	public void setErrMsg(String errMsg) {
		ErrMsg = errMsg;
	}

	
	

}
