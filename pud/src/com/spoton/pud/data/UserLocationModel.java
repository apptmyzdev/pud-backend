package com.spoton.pud.data;

public class UserLocationModel{

	private int id;
	private float accuracy;
	private String address;
	private String deviceTimestamp;
	private double distance;
	private double latitude;
	private double longitude;
	private String pincode;
	private String timestamp;
	private UserDataModel userDataMaster;
	private DeviceInfoModal deviceInfo;

	public UserLocationModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAccuracy() {
		return this.accuracy;
	}

	public void setAccuracy(float accuracy) {
		this.accuracy = accuracy;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getDistance() {
		return this.distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getDeviceTimestamp() {
		return deviceTimestamp;
	}

	public void setDeviceTimestamp(String deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public UserDataModel getUserDataMaster() {
		return userDataMaster;
	}

	public void setUserDataMaster(UserDataModel userDataMaster) {
		this.userDataMaster = userDataMaster;
	}

	public DeviceInfoModal getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoModal deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	@Override
	public String toString() {
		return "UserLocationModel [id=" + id + ", accuracy=" + accuracy + ", address=" + address + ", deviceTimestamp="
				+ deviceTimestamp + ", distance=" + distance + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", pincode=" + pincode + ", timestamp=" + timestamp + ", userDataMaster=" + userDataMaster
				+ ", deviceInfo=" + deviceInfo + "]";
	}

	


}