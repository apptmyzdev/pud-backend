package com.spoton.pud.data;

import java.util.List;

public class SubmitBoxPieceMappingErpModalList {
	
	private List<SubmitBoxPieceMappingErpModal> APIPiecesUpdate_Input;

	public List<SubmitBoxPieceMappingErpModal> getAPIPiecesUpdate_Input() {
		return APIPiecesUpdate_Input;
	}

	public void setAPIPiecesUpdate_Input(List<SubmitBoxPieceMappingErpModal> aPIPiecesUpdate_Input) {
		APIPiecesUpdate_Input = aPIPiecesUpdate_Input;
	}
	
	

}
