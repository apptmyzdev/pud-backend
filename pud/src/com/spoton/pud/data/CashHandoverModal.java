package com.spoton.pud.data;



public class CashHandoverModal {
	
	private String docketNumber;
	private String collectionType;
	private String shipmentType;
	private Float collectedAmount;
	private String createdDate;
	public String getDocketNumber() {
		return docketNumber;
	}
	public void setDocketNumber(String docketNumber) {
		this.docketNumber = docketNumber;
	}
	public String getCollectionType() {
		return collectionType;
	}
	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}
	public String getShipmentType() {
		return shipmentType;
	}
	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}
	public Float getCollectedAmount() {
		return collectedAmount;
	}
	public void setCollectedAmount(Float collectedAmount) {
		this.collectedAmount = collectedAmount;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	
	

}
