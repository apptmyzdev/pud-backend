package com.spoton.pud.data;

public class EwayBillNumberInfo {

	private String userGstin;
	private String invoiceNo;
	private String invoiceDate;
	private String delPincode;
	private String invoiceAmount;
	private String rejectStatus;
	private String validUpto;
	private String senderName;  
	private String senderAddress;  
	private String senderCity;  
	private String receiverName; 
	private String receiverAddress; 
	private String receiverCity;  
	private String originPincode;  
	public String getUserGstin() {
		return userGstin;
	}
	public void setUserGstin(String userGstin) {
		this.userGstin = userGstin;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getDelPincode() {
		return delPincode;
	}
	public void setDelPincode(String delPincode) {
		this.delPincode = delPincode;
	}
	public String getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public String getRejectStatus() {
		return rejectStatus;
	}
	public void setRejectStatus(String rejectStatus) {
		this.rejectStatus = rejectStatus;
	}
	public String getValidUpto() {
		return validUpto;
	}
	public void setValidUpto(String validUpto) {
		this.validUpto = validUpto;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public String getSenderAddress() {
		return senderAddress;
	}
	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	public String getReceiverAddress() {
		return receiverAddress;
	}
	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}
	public String getReceiverCity() {
		return receiverCity;
	}
	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}
	public String getOriginPincode() {
		return originPincode;
	}
	public void setOriginPincode(String originPincode) {
		this.originPincode = originPincode;
	}
	
	
	
}
