
package com.spoton.pud.data;



public class RtmsVehicleInfoModal {

	private int vehicleId;
	private double totalCapacity;
	private double availableCapacity;
	private double currentLocationLatitude;
	private double currentLocationLongitude;
	private VendorMasterModel vendorDetails;
	private UserMasterModel userDetails;
	private VehiclePickupsAndDeliveries vehiclePickups;
	private VehiclePickupsAndDeliveries vehicleDeliveries;
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	public double getTotalCapacity() {
		return totalCapacity;
	}
	public void setTotalCapacity(double totalCapacity) {
		this.totalCapacity = totalCapacity;
	}
	public double getCurrentLocationLatitude() {
		return currentLocationLatitude;
	}
	public void setCurrentLocationLatitude(double currentLocationLatitude) {
		this.currentLocationLatitude = currentLocationLatitude;
	}
	public double getCurrentLocationLongitude() {
		return currentLocationLongitude;
	}
	public void setCurrentLocationLongitude(double currentLocationLongitude) {
		this.currentLocationLongitude = currentLocationLongitude;
	}
	public VendorMasterModel getVendorDetails() {
		return vendorDetails;
	}
	public void setVendorDetails(VendorMasterModel vendorDetails) {
		this.vendorDetails = vendorDetails;
	}
	
	
	
	public UserMasterModel getUserDetails() {
		return userDetails;
	}
	public void setUserDetails(UserMasterModel userDetails) {
		this.userDetails = userDetails;
	}
	
	
	
	public VehiclePickupsAndDeliveries getVehiclePickups() {
		return vehiclePickups;
	}
	public void setVehiclePickups(VehiclePickupsAndDeliveries vehiclePickups) {
		this.vehiclePickups = vehiclePickups;
	}
	public VehiclePickupsAndDeliveries getVehicleDeliveries() {
		return vehicleDeliveries;
	}
	public void setVehicleDeliveries(VehiclePickupsAndDeliveries vehicleDeliveries) {
		this.vehicleDeliveries = vehicleDeliveries;
	}
	public double getAvailableCapacity() {
		return availableCapacity;
	}
	public void setAvailableCapacity(double availableCapacity) {
		this.availableCapacity = availableCapacity;
	}
	
	
}


