package com.spoton.pud.data;

import java.util.List;

public class AutoPickupUnloadingInputModal {
	
	private String userId;
	private String vehicleNo;
	private int transactionId;
	private List<String> conNumbers;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public List<String> getConNumbers() {
		return conNumbers;
	}
	public void setConNumbers(List<String> conNumbers) {
		this.conNumbers = conNumbers;
	}
	
	

}
