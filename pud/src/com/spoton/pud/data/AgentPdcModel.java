package com.spoton.pud.data;

public class AgentPdcModel {
	String	agentId;
	String	pdc;
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getPdc() {
		return pdc;
	}
	public void setPdc(String pdc) {
		this.pdc = pdc;
	}
	
	
}
