/*
 * Decompiled with CFR 0_123.
 */
package com.spoton.pud.data;

public class OdaConRemarksErpModal {
    private String ConNo;
    private String UserID;
    private String Remarks;
    private String RemarksDate;
    private float Latitude;
    private float Longitude;
    private int TransactionID;

    public String getConNo() {
        return this.ConNo;
    }

    public void setConNo(String conNo) {
        this.ConNo = conNo;
    }

    public String getUserID() {
        return this.UserID;
    }

    public void setUserID(String userID) {
        this.UserID = userID;
    }

    public String getRemarks() {
        return this.Remarks;
    }

    public void setRemarks(String remarks) {
        this.Remarks = remarks;
    }

    public String getRemarksDate() {
        return this.RemarksDate;
    }

    public void setRemarksDate(String remarksDate) {
        this.RemarksDate = remarksDate;
    }

    public float getLatitude() {
        return this.Latitude;
    }

    public void setLatitude(float latitude) {
        this.Latitude = latitude;
    }

    public float getLongitude() {
        return this.Longitude;
    }

    public void setLongitude(float longitude) {
        this.Longitude = longitude;
    }

    public int getTransactionID() {
        return this.TransactionID;
    }

    public void setTransactionID(int transactionID) {
        this.TransactionID = transactionID;
    }
}
