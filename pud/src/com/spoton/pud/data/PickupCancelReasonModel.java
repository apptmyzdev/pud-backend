package com.spoton.pud.data;



import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class PickupCancelReasonModel {
	
	private int activeStatus;
	
	private String failureCategory;
	
	private int pickupStatusId;
	
	private String reasonCode;
	
	private String reasonDesc;
	
	private String updatedBy;
	
	private Date updatedOn;

	public int isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getFailureCategory() {
		return failureCategory;
	}

	public void setFailureCategory(String failureCategory) {
		this.failureCategory = failureCategory;
	}

	public int getPickupStatusId() {
		return pickupStatusId;
	}

	public void setPickupStatusId(int pickupStatusId) {
		this.pickupStatusId = pickupStatusId;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDesc() {
		return reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	
	
}
