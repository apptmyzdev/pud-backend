package com.spoton.pud.data;

public class VendorMasterModel {

	private int vendorId;
	private String appUsername;
	private String location;
	public int getVendorId() {
		return vendorId;
	}
	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}
	public String getAppUsername() {
		return appUsername;
	}
	public void setAppUsername(String appUsername) {
		this.appUsername = appUsername;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
}
