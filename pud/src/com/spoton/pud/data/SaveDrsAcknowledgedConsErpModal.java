package com.spoton.pud.data;

public class SaveDrsAcknowledgedConsErpModal {

	private String ConNo;
	private String PDCNo;
	private String IsOK;  //Y-yes N-No
	private String Remarks;
	private int TransactionId;
	public String getConNo() {
		return ConNo;
	}
	public void setConNo(String conNo) {
		ConNo = conNo;
	}
	public String getPDCNo() {
		return PDCNo;
	}
	public void setPDCNo(String pDCNo) {
		PDCNo = pDCNo;
	}
	public String getIsOK() {
		return IsOK;
	}
	public void setIsOK(String isOK) {
		IsOK = isOK;
	}
	public String getRemarks() {
		return Remarks;
	}
	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
	public int getTransactionId() {
		return TransactionId;
	}
	public void setTransactionId(int transactionId) {
		TransactionId = transactionId;
	}
	
	
}
