package com.spoton.pud.data;

import java.util.List;

public class CashBookingErpInputModal {

	private String CustomerName ;
	private String CustomerAddress; 
	private String CustomerCity; 
	private String CustomerEmail;
	private  String CustomerPhone;  
   private  String CustomerGSTIN;
   private  String  ReceiverName; 
   private  String  ReceiverAddress;
   private  String  ReceiverCity; 
   private  String  ReceiverEmail; 
   private  String  ReceiverPhone; 
   private  String  ReceiverGSTIN; 
   private  String  RateModifyEnabled;
   private String IsPrePrinted;
	private List<ConDetailsData> ConDetails;
	private  List<RateCardCalculatorERPModal> RateCardOutput; 

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getCustomerAddress() {
		return CustomerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		CustomerAddress = customerAddress;
	}

	public String getCustomerCity() {
		return CustomerCity;
	}

	public String getCustomerPhone() {
		return CustomerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		CustomerPhone = customerPhone;
	}

	public String getCustomerGSTIN() {
		return CustomerGSTIN;
	}

	public void setCustomerGSTIN(String customerGSTIN) {
		CustomerGSTIN = customerGSTIN;
	}

	public void setCustomerCity(String customerCity) {
		CustomerCity = customerCity;
	}

	public List<ConDetailsData> getConDetails() {
		return ConDetails;
	}

	public void setConDetails(List<ConDetailsData> conDetails) {
		ConDetails = conDetails;
	}

	public String getCustomerEmail() {
		return CustomerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}

	public String getReceiverName() {
		return ReceiverName;
	}

	public void setReceiverName(String receiverName) {
		ReceiverName = receiverName;
	}

	public String getReceiverAddress() {
		return ReceiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		ReceiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return ReceiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		ReceiverCity = receiverCity;
	}

	public String getReceiverEmail() {
		return ReceiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		ReceiverEmail = receiverEmail;
	}

	public String getReceiverPhone() {
		return ReceiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		ReceiverPhone = receiverPhone;
	}

	public String getReceiverGSTIN() {
		return ReceiverGSTIN;
	}

	public void setReceiverGSTIN(String receiverGSTIN) {
		ReceiverGSTIN = receiverGSTIN;
	}

	public String getRateModifyEnabled() {
		return RateModifyEnabled;
	}

	public void setRateModifyEnabled(String rateModifyEnabled) {
		RateModifyEnabled = rateModifyEnabled;
	}

	public List<RateCardCalculatorERPModal> getRateCardOutput() {
		return RateCardOutput;
	}

	public void setRateCardOutput(List<RateCardCalculatorERPModal> rateCardOutput) {
		RateCardOutput = rateCardOutput;
	}

	public String getIsPrePrinted() {
		return IsPrePrinted;
	}

	public void setIsPrePrinted(String isPrePrinted) {
		IsPrePrinted = isPrePrinted;
	}

	
	
	
	
	
}
