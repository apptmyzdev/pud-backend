package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {

	private int id;

	@SerializedName("AppPwd")
	private String appPwd;

	@SerializedName("AppUserName")
	private String appUserName;

	@SerializedName("CurrentlyUsed ")
	private String currentlyUsed;

	@SerializedName("is9DigitPartNo")
	private boolean is9digitPartNo;

	@SerializedName("Isactive")
	private boolean isActive;

	@SerializedName("IsConEntry")
	private boolean isConEntry;

	@SerializedName("IsDelivery")
	private boolean isDelivery;

	@SerializedName("isOffLinePrinting")
	private boolean isOfflinePrinting;

	@SerializedName("isPickupAppActive")
	private boolean isPickupAppActive;

	@SerializedName("IsPickUpRegistration")
	private boolean isPickupRegistration;

	@SerializedName("IsPiecesEntry")
	private boolean isPiecesEntry;

	@SerializedName("Location")
	private String location;

	@SerializedName("LoginStatus")
	private boolean loginStatus;

	@SerializedName("PrinterSlNo ")
	private String printerSlNo;

	@SerializedName("TabID ")
	private String tabId;

	@SerializedName("UserID")
	private int userId;

	@SerializedName("VersionID ")
	private String verisonId;

	public UserDataModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppPwd() {
		return this.appPwd;
	}

	public void setAppPwd(String appPwd) {
		this.appPwd = appPwd;
	}

	public String getAppUserName() {
		return this.appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getCurrentlyUsed() {
		return this.currentlyUsed;
	}

	public void setCurrentlyUsed(String currentlyUsed) {
		this.currentlyUsed = currentlyUsed;
	}

	public boolean getIs9digitPartNo() {
		return this.is9digitPartNo;
	}

	public void setIs9digitPartNo(boolean is9digitPartNo) {
		this.is9digitPartNo = is9digitPartNo;
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean getIsConEntry() {
		return this.isConEntry;
	}

	public void setIsConEntry(boolean isConEntry) {
		this.isConEntry = isConEntry;
	}

	public boolean getIsDelivery() {
		return this.isDelivery;
	}

	public void setIsDelivery(boolean isDelivery) {
		this.isDelivery = isDelivery;
	}

	public boolean getIsOfflinePrinting() {
		return this.isOfflinePrinting;
	}

	public void setIsOfflinePrinting(boolean isOfflinePrinting) {
		this.isOfflinePrinting = isOfflinePrinting;
	}

	public boolean getIsPickupAppActive() {
		return this.isPickupAppActive;
	}

	public void setIsPickupAppActive(boolean isPickupAppActive) {
		this.isPickupAppActive = isPickupAppActive;
	}

	public boolean getIsPickupRegistration() {
		return this.isPickupRegistration;
	}

	public void setIsPickupRegistration(boolean isPickupRegistration) {
		this.isPickupRegistration = isPickupRegistration;
	}

	public boolean getIsPiecesEntry() {
		return this.isPiecesEntry;
	}

	public void setIsPiecesEntry(boolean isPiecesEntry) {
		this.isPiecesEntry = isPiecesEntry;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean getLoginStatus() {
		return this.loginStatus;
	}

	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getPrinterSlNo() {
		return this.printerSlNo;
	}

	public void setPrinterSlNo(String printerSlNo) {
		this.printerSlNo = printerSlNo;
	}

	public String getTabId() {
		return this.tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getVerisonId() {
		return this.verisonId;
	}

	public void setVerisonId(String verisonId) {
		this.verisonId = verisonId;
	}

}