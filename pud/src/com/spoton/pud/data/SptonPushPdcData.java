package com.spoton.pud.data;

import java.util.List;

public class SptonPushPdcData {
	List<SpotonAWBData>	pdcData;

	public List<SpotonAWBData> getPdcData() {
		return pdcData;
	}

	public void setPdcData(List<SpotonAWBData> pdcData) {
		this.pdcData = pdcData;
	}
	
	
}
