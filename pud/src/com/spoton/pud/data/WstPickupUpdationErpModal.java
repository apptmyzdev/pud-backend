package com.spoton.pud.data;

import java.util.Date;

public class WstPickupUpdationErpModal {
	
	private String ConNumber; 
	private String PieceNo; 
	private String IsPrinted; 
	private Date PrintedDateTime; 
	private String IsManual; 
	private String UserID;
	public String getConNumber() {
		return ConNumber;
	}
	public void setConNumber(String conNumber) {
		ConNumber = conNumber;
	}
	public String getPieceNo() {
		return PieceNo;
	}
	public void setPieceNo(String pieceNo) {
		PieceNo = pieceNo;
	}
	public String getIsPrinted() {
		return IsPrinted;
	}
	public void setIsPrinted(String isPrinted) {
		IsPrinted = isPrinted;
	}
	public Date getPrintedDateTime() {
		return PrintedDateTime;
	}
	public void setPrintedDateTime(Date printedDateTime) {
		PrintedDateTime = printedDateTime;
	}
	public String getIsManual() {
		return IsManual;
	}
	public void setIsManual(String isManual) {
		IsManual = isManual;
	}
	public String getUserID() {
		return UserID;
	}
	public void setUserID(String userID) {
		UserID = userID;
	}

	
	
}
