package com.spoton.pud.data;

public class CashBookingConsERPModal {

	private String ConEntryID;
	private String Flag;
	private String StatusDetail;
	private String Invno;
	private String InvDate;
	private String SpotonGst;
	private String CustomerGst;
	private String Conno;
	private String Actwt;
	private String Chrgwt;
	private String TotalPkgs;
	private String TaxAmount;
	private String OrgCode;
	private String DestCode;
	private String ChargeAmount;
	private String CGST;
	private String SGST;
	private String UGST;
	private String IGST;
	private String NetAmt;
	private String SpotonAddrees;
	private String CustomerEmail;  
	private String CustomerPhone;  
	private String CustomerGSTIN;  
	private String SpotonGSTStateName ; 
	private String ShipperGSTStateName  ; 
	
	
	
	
	public String getConEntryID() {
		return ConEntryID;
	}
	public void setConEntryID(String conEntryID) {
		ConEntryID = conEntryID;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	public String getStatusDetail() {
		return StatusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		StatusDetail = statusDetail;
	}
	public String getInvno() {
		return Invno;
	}
	public void setInvno(String invno) {
		Invno = invno;
	}
	public String getInvDate() {
		return InvDate;
	}
	public void setInvDate(String invDate) {
		InvDate = invDate;
	}
	public String getSpotonGst() {
		return SpotonGst;
	}
	public void setSpotonGst(String spotonGst) {
		SpotonGst = spotonGst;
	}
	public String getCustomerGst() {
		return CustomerGst;
	}
	public void setCustomerGst(String customerGst) {
		CustomerGst = customerGst;
	}
	public String getConno() {
		return Conno;
	}
	public void setConno(String conno) {
		Conno = conno;
	}
	public String getActwt() {
		return Actwt;
	}
	public void setActwt(String actwt) {
		Actwt = actwt;
	}
	public String getChrgwt() {
		return Chrgwt;
	}
	public void setChrgwt(String chrgwt) {
		Chrgwt = chrgwt;
	}
	public String getTotalPkgs() {
		return TotalPkgs;
	}
	public void setTotalPkgs(String totalPkgs) {
		TotalPkgs = totalPkgs;
	}
	public String getTaxAmount() {
		return TaxAmount;
	}
	public void setTaxAmount(String taxAmount) {
		TaxAmount = taxAmount;
	}
	public String getOrgCode() {
		return OrgCode;
	}
	public void setOrgCode(String orgCode) {
		OrgCode = orgCode;
	}
	public String getDestCode() {
		return DestCode;
	}
	public void setDestCode(String destCode) {
		DestCode = destCode;
	}
	public String getChargeAmount() {
		return ChargeAmount;
	}
	public void setChargeAmount(String chargeAmount) {
		ChargeAmount = chargeAmount;
	}
	public String getCGST() {
		return CGST;
	}
	public void setCGST(String cGST) {
		CGST = cGST;
	}
	public String getSGST() {
		return SGST;
	}
	public void setSGST(String sGST) {
		SGST = sGST;
	}
	public String getUGST() {
		return UGST;
	}
	public void setUGST(String uGST) {
		UGST = uGST;
	}
	public String getIGST() {
		return IGST;
	}
	public void setIGST(String iGST) {
		IGST = iGST;
	}
	public String getNetAmt() {
		return NetAmt;
	}
	public void setNetAmt(String netAmt) {
		NetAmt = netAmt;
	}
	public String getSpotonAddrees() {
		return SpotonAddrees;
	}
	public void setSpotonAddrees(String spotonAddrees) {
		SpotonAddrees = spotonAddrees;
	}
	public String getCustomerEmail() {
		return CustomerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		CustomerEmail = customerEmail;
	}
	public String getCustomerPhone() {
		return CustomerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		CustomerPhone = customerPhone;
	}
	public String getCustomerGSTIN() {
		return CustomerGSTIN;
	}
	public void setCustomerGSTIN(String customerGSTIN) {
		CustomerGSTIN = customerGSTIN;
	}
	public String getSpotonGSTStateName() {
		return SpotonGSTStateName;
	}
	public void setSpotonGSTStateName(String spotonGSTStateName) {
		SpotonGSTStateName = spotonGSTStateName;
	}
	public String getShipperGSTStateName() {
		return ShipperGSTStateName;
	}
	public void setShipperGSTStateName(String shipperGSTStateName) {
		ShipperGSTStateName = shipperGSTStateName;
	}

	
	
	
}
