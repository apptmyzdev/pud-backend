package com.spoton.pud.data;

public class RateCardCalculatorResponseModal {

	private double frt;
	private double dfs;
	private double stateCharge;
	private double dkt;
	private double fov;
	private double minCharge;
	private double pda;
	private double oda;
	private double handlingCharge;
	private double otherRecovery;
	private double ftc;
	private double vtc;
	private double phPickup;
	private double phDelv;
	private double dacc;
	private double nfForm;
	private double dcac;
	private double grossAmount;
	private double serviceTax;
	private double serviceTaxPercent;
	private double totalAmount;
	private double chargedWeight;
	private double collection;
	private String erroCode;
	private String rateModifyEnabled;
	private String environmentalSurcharges;
	private double actualAmount;
	
	/*02-09-2020*/
	private float covidSurcharge;
	
	public double getFrt() {
		return frt;
	}
	public void setFrt(double frt) {
		this.frt = frt;
	}
	public double getDfs() {
		return dfs;
	}
	public void setDfs(double dfs) {
		this.dfs = dfs;
	}
	public double getStateCharge() {
		return stateCharge;
	}
	public void setStateCharge(double stateCharge) {
		this.stateCharge = stateCharge;
	}
	public double getDkt() {
		return dkt;
	}
	public void setDkt(double dkt) {
		this.dkt = dkt;
	}
	public double getFov() {
		return fov;
	}
	public void setFov(double fov) {
		this.fov = fov;
	}
	public double getMinCharge() {
		return minCharge;
	}
	public void setMinCharge(double minCharge) {
		this.minCharge = minCharge;
	}
	public double getPda() {
		return pda;
	}
	public void setPda(double pda) {
		this.pda = pda;
	}
	public double getOda() {
		return oda;
	}
	public void setOda(double oda) {
		this.oda = oda;
	}
	public double getHandlingCharge() {
		return handlingCharge;
	}
	public void setHandlingCharge(double handlingCharge) {
		this.handlingCharge = handlingCharge;
	}
	public double getOtherRecovery() {
		return otherRecovery;
	}
	public void setOtherRecovery(double otherRecovery) {
		this.otherRecovery = otherRecovery;
	}
	public double getFtc() {
		return ftc;
	}
	public void setFtc(double ftc) {
		this.ftc = ftc;
	}
	public double getVtc() {
		return vtc;
	}
	public void setVtc(double vtc) {
		this.vtc = vtc;
	}
	public double getPhPickup() {
		return phPickup;
	}
	public void setPhPickup(double phPickup) {
		this.phPickup = phPickup;
	}
	public double getPhDelv() {
		return phDelv;
	}
	public void setPhDelv(double phDelv) {
		this.phDelv = phDelv;
	}
	public double getDacc() {
		return dacc;
	}
	public void setDacc(double dacc) {
		this.dacc = dacc;
	}
	public double getNfForm() {
		return nfForm;
	}
	public void setNfForm(double nfForm) {
		this.nfForm = nfForm;
	}
	public double getDcac() {
		return dcac;
	}
	public void setDcac(double dcac) {
		this.dcac = dcac;
	}
	public double getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(double grossAmount) {
		this.grossAmount = grossAmount;
	}
	public double getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getCollection() {
		return collection;
	}
	public void setCollection(double collection) {
		this.collection = collection;
	}
	public String getErroCode() {
		return erroCode;
	}
	public void setErroCode(String erroCode) {
		this.erroCode = erroCode;
	}
	public double getChargedWeight() {
		return chargedWeight;
	}
	public void setChargedWeight(double chargedWeight) {
		this.chargedWeight = chargedWeight;
	}
	public String getRateModifyEnabled() {
		return rateModifyEnabled;
	}
	public void setRateModifyEnabled(String rateModifyEnabled) {
		this.rateModifyEnabled = rateModifyEnabled;
	}
	public double getServiceTaxPercent() {
		return serviceTaxPercent;
	}
	public void setServiceTaxPercent(double serviceTaxPercent) {
		this.serviceTaxPercent = serviceTaxPercent;
	}
	public String getEnvironmentalSurcharges() {
		return environmentalSurcharges;
	}
	public void setEnvironmentalSurcharges(String environmentalSurcharges) {
		this.environmentalSurcharges = environmentalSurcharges;
	}
	public double getActualAmount() {
		return actualAmount;
	}
	public void setActualAmount(double actualAmount) {
		this.actualAmount = actualAmount;
	}
	
	/** added on Sep 2, 2020 **/
	public float getCovidSurcharge() {
		return covidSurcharge;
	}
	/** added on Sep 2, 2020 **/
	public void setCovidSurcharge(float covidSurcharge) {
		this.covidSurcharge = covidSurcharge;
	}
	
	
	
	
}
