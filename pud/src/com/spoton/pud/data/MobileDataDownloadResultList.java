package com.spoton.pud.data;

import java.util.List;

public class MobileDataDownloadResultList {
	
	private List<MobileDataDownloadResult> MobDataDownloadResult;

	public List<MobileDataDownloadResult> getMobDataDownloadResult() {
		return MobDataDownloadResult;
	}

	public void setMobDataDownloadResult(
			List<MobileDataDownloadResult> mobDataDownloadResult) {
		MobDataDownloadResult = mobDataDownloadResult;
	}
	
	

}
