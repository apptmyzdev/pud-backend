package com.spoton.pud.data;

public class AirportCodesMasterModal {
	
	private String airportId;
	private String airportCode;
	private String airportDescription;
	public String getAirportId() {
		return airportId;
	}
	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getAirportDescription() {
		return airportDescription;
	}
	public void setAirportDescription(String airportDescription) {
		this.airportDescription = airportDescription;
	}
	
	

}
