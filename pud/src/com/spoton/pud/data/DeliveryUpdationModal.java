package com.spoton.pud.data;


import java.util.List;

public class DeliveryUpdationModal {

	private String awbNo;  
	private int transactionId;  
	private String transactionDate;  
	private String deliveredTo;  
	private String relationship;  
	private String remarks;  
	private String receiverMobileNo;  
	private String signatureURL;
	private String reason;  
	private String status;  
	private double longValue;  
	private double latValue;  
	private String fieldEmployeeName;  
	private String imageURL;
	private String 	pdcNumber;
	private String 	paymentType;
	private String	consigneeName;
	private int erpUpdated;  
	private int consignmentId;
	private String isFtc;
	private String collectionType;
	private Float collectedAmount;
	private String bankName;
	private String bankBranch;
	private String chequeDate;
	private String chequeNumber;
	private Float chequeAmount;
	private String chequeImg1URL;
	private String chequeImg2URL;
	private String isValidPod; 
	private String isTDSDeducted;
	private int deliveryConditionId;
	private int deliverySubConditionId;
	private String tanNumber;
	private String customerName;
	private String shipmentType;
	private String dlyAddress;
    private String dlyPincode;
	private List<DeliveryConditionImages> deliveryConditionImages;
	
	public String getAwbNo() {
		return awbNo;
	}
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDeliveredTo() {
		return deliveredTo;
	}
	public void setDeliveredTo(String deliveredTo) {
		this.deliveredTo = deliveredTo;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getReceiverMobileNo() {
		return receiverMobileNo;
	}
	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}
	public String getSignatureURL() {
		return signatureURL;
	}
	public void setSignatureURL(String signatureURL) {
		this.signatureURL = signatureURL;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getLongValue() {
		return longValue;
	}
	public double setLongValue(double longValue) {
		return this.longValue = longValue;
	}
	public double getLatValue() {
		return latValue;
	}
	public double setLatValue(double latValue) {
		return this.latValue = latValue;
	}
	public String getFieldEmployeeName() {
		return fieldEmployeeName;
	}
	public void setFieldEmployeeName(String fieldEmployeeName) {
		this.fieldEmployeeName = fieldEmployeeName;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getPdcNumber() {
		return pdcNumber;
	}
	public void setPdcNumber(String pdcNumber) {
		this.pdcNumber = pdcNumber;
	}
	public String getConsigneeName() {
		return consigneeName;
	}
	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}
	public int getErpUpdated() {
		return erpUpdated;
	}
	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public int getConsignmentId() {
		return consignmentId;
	}
	public void setConsignmentId(int consignmentId) {
		this.consignmentId = consignmentId;
	}
	public String getIsFtc() {
		return isFtc;
	}
	public void setIsFtc(String isFtc) {
		this.isFtc = isFtc;
	}
	public String getCollectionType() {
		return collectionType;
	}
	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}
	public Float getCollectedAmount() {
		return collectedAmount;
	}
	public void setCollectedAmount(Float collectedAmount) {
		this.collectedAmount = collectedAmount;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankBranch() {
		return bankBranch;
	}
	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}
	public String getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public Float getChequeAmount() {
		return chequeAmount;
	}
	public void setChequeAmount(Float chequeAmount) {
		this.chequeAmount = chequeAmount;
	}
	public String getChequeImg1URL() {
		return chequeImg1URL;
	}
	public void setChequeImg1URL(String chequeImg1URL) {
		this.chequeImg1URL = chequeImg1URL;
	}
	public String getChequeImg2URL() {
		return chequeImg2URL;
	}
	public void setChequeImg2URL(String chequeImg2URL) {
		this.chequeImg2URL = chequeImg2URL;
	}
	public String getIsValidPod() {
		return isValidPod;
	}
	public void setIsValidPod(String isValidPod) {
		this.isValidPod = isValidPod;
	}
	public String getIsTDSDeducted() {
		return isTDSDeducted;
	}
	public void setIsTDSDeducted(String isTDSDeducted) {
		this.isTDSDeducted = isTDSDeducted;
	}
	
	public int getDeliveryConditionId() {
		return deliveryConditionId;
	}
	public void setDeliveryConditionId(int deliveryConditionId) {
		this.deliveryConditionId = deliveryConditionId;
	}

	
	public List<DeliveryConditionImages> getDeliveryConditionImages() {
		return deliveryConditionImages;
	}
	public void setDeliveryConditionImages(List<DeliveryConditionImages> deliveryConditionImages) {
		this.deliveryConditionImages = deliveryConditionImages;
	}
	public String getTanNumber() {
		return tanNumber;
	}
	public void setTanNumber(String tanNumber) {
		this.tanNumber = tanNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getShipmentType() {
		return shipmentType;
	}
	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}
	public String getDlyAddress() {
		return dlyAddress;
	}
	public void setDlyAddress(String dlyAddress) {
		this.dlyAddress = dlyAddress;
	}
	public String getDlyPincode() {
		return dlyPincode;
	}
	public void setDlyPincode(String dlyPincode) {
		this.dlyPincode = dlyPincode;
	}
	public int getDeliverySubConditionId() {
		return deliverySubConditionId;
	}
	public void setDeliverySubConditionId(int deliverySubConditionId) {
		this.deliverySubConditionId = deliverySubConditionId;
	} 
	
	
	
	
}
