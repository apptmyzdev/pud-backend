package com.spoton.pud.data;



public class PieceVolumeModal {

	private int id;
	private int noofPieces; 
	
	
	private double volLength;
	
	
	private double volBreadth;
	
	
	private double volHeight;
	
	
	private double totalVolWeight;
	
	private double actualWeightPerPiece;
	private int contentTypeId;

	

	public int getNoOfPieces() {
		return noofPieces;
	}


	public void setNoOfPieces(int noOfPieces) {
		this.noofPieces = noOfPieces;
	}


	public double getVolLength() {
		return volLength;
	}


	public void setVolLength(double volLength) {
		this.volLength = volLength;
	}


	public double getVolBreadth() {
		return volBreadth;
	}


	public void setVolBreadth(double volBreadth) {
		this.volBreadth = volBreadth;
	}


	public double getVolHeight() {
		return volHeight;
	}


	public void setVolHeight(double volHeight) {
		this.volHeight = volHeight;
	}


	public double getTotalVolWeight() {
		return totalVolWeight;
	}


	public void setTotalVolWeight(double totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public double getActualWeightPerPiece() {
		return actualWeightPerPiece;
	}


	public void setActualWeightPerPiece(double actualWeightPerPiece) {
		this.actualWeightPerPiece = actualWeightPerPiece;
	}


	public int getContentTypeId() {
		return contentTypeId;
	}


	public void setContentTypeId(int contentTypeId) {
		this.contentTypeId = contentTypeId;
	}

	
	
}
