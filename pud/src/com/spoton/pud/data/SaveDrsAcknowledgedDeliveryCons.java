package com.spoton.pud.data;

public class SaveDrsAcknowledgedDeliveryCons {
	
	private String conNumber;
	private String pdcNumber;
	private String isOk;  //Y-yes N-No
	private String remarks;
	private int transactionId; //deliveryConDetailsId
	public String getConNumber() {
		return conNumber;
	}
	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}
	public String getPdcNumber() {
		return pdcNumber;
	}
	public void setPdcNumber(String pdcNumber) {
		this.pdcNumber = pdcNumber;
	}
	public String getIsOk() {
		return isOk;
	}
	public void setIsOk(String isOk) {
		this.isOk = isOk;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
	

}
