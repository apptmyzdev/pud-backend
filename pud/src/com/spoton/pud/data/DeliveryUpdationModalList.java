package com.spoton.pud.data;

import java.util.List;

public class DeliveryUpdationModalList {

	private List<DeliveryUpdationModal> deliveryEntity;

	public List<DeliveryUpdationModal> getDeliveryEntity() {
		return deliveryEntity;
	}

	public void setDeliveryEntity(List<DeliveryUpdationModal> deliveryEntity) {
		this.deliveryEntity = deliveryEntity;
	}
	
	
	
}
