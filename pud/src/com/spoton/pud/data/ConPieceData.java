package com.spoton.pud.data;



public class ConPieceData {
    private int fromNo;
    private int toNo;
    private int noOfPcs;
    private String pcrKey; 

    public ConPieceData() {
        super();
    }

    public ConPieceData(int fromNo, int toNo, int noOfPcs) {
        this.fromNo = fromNo;
        this.toNo = toNo;
        this.noOfPcs = noOfPcs;
    }

    public int getFromNo() {
        return fromNo;
    }

    public void setFromNo(int fromNo) {
        this.fromNo = fromNo;
    }

    public int getToNo() {
        return toNo;
    }

    public void setToNo(int toNo) {
        this.toNo = toNo;
    }

    public int getNoOfPcs() {
        return noOfPcs;
    }

    public void setNoOfPcs(int noOfPcs) {
        this.noOfPcs = noOfPcs;
    }

    
    
    public String getPcrKey() {
		return pcrKey;
	}

	public void setPcrKey(String pcrKey) {
		this.pcrKey = pcrKey;
	}

	@Override
    public String toString() {
        return "ConPieceData{" +
                "fromNo=" + fromNo +
                ", toNo=" + toNo +
                ", noOfPcs=" + noOfPcs +
                '}';
    }
}
