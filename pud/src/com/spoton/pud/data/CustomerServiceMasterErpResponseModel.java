package com.spoton.pud.data;



public class CustomerServiceMasterErpResponseModel {
	
	private String AccountCode;
	private String Origin;
	private String Destination;
	private String sessionId;
	public String getAccountCode() {
		return AccountCode;
	}
	public void setAccountCode(String accountCode) {
		AccountCode = accountCode;
	}
	public String getOrigin() {
		return Origin;
	}
	public void setOrigin(String origin) {
		Origin = origin;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	/*public boolean changed(CustomerServiceMaster oldvalue) {
	
		if (oldvalue.get!=null&&this.getDepot()!=null&&(!this.getDepot().equalsIgnoreCase(oldPinCode.getDepot()))){
			return true;
		}
		
		
		return false;
		
	}*/
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	

}
