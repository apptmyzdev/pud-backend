package com.spoton.pud.data;

public class EwayBillDashboardErpModal {

	String CON_ID;  
	String EwayBillNo ;
	String PickupOrderNo ;
	String USER_ID  ;
	String PICKUPDATE ; 
	String VehicleNo;  
	String eWayBillStatus ; 
	String ErrorRemarks ; 
	String ValidTill ;
	public String getCON_ID() {
		return CON_ID;
	}
	public void setCON_ID(String cON_ID) {
		CON_ID = cON_ID;
	}
	public String getEwayBillNo() {
		return EwayBillNo;
	}
	public void setEwayBillNo(String ewayBillNo) {
		EwayBillNo = ewayBillNo;
	}
	public String getPickupOrderNo() {
		return PickupOrderNo;
	}
	public void setPickupOrderNo(String pickupOrderNo) {
		PickupOrderNo = pickupOrderNo;
	}
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getPICKUPDATE() {
		return PICKUPDATE;
	}
	public void setPICKUPDATE(String pICKUPDATE) {
		PICKUPDATE = pICKUPDATE;
	}
	public String getVehicleNo() {
		return VehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}
	public String geteWayBillStatus() {
		return eWayBillStatus;
	}
	public void seteWayBillStatus(String eWayBillStatus) {
		this.eWayBillStatus = eWayBillStatus;
	}
	public String getErrorRemarks() {
		return ErrorRemarks;
	}
	public void setErrorRemarks(String errorRemarks) {
		ErrorRemarks = errorRemarks;
	}
	public String getValidTill() {
		return ValidTill;
	}
	public void setValidTill(String validTill) {
		ValidTill = validTill;
	}
	
	
	
}
