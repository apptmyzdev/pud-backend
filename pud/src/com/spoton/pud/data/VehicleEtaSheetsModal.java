package com.spoton.pud.data;

public class VehicleEtaSheetsModal {
	
	private String pickupUnloadingSheetNumber;
	private VehicleEtaSheetDetailsModal sheetDetails;
	public String getPickupUnloadingSheetNumber() {
		return pickupUnloadingSheetNumber;
	}
	public void setPickupUnloadingSheetNumber(String pickupUnloadingSheetNumber) {
		this.pickupUnloadingSheetNumber = pickupUnloadingSheetNumber;
	}
	public VehicleEtaSheetDetailsModal getSheetDetails() {
		return sheetDetails;
	}
	public void setSheetDetails(VehicleEtaSheetDetailsModal sheetDetails) {
		this.sheetDetails = sheetDetails;
	}
	
	

}
