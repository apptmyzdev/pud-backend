package com.spoton.pud.data;

public class PayzappRequestLinkModal {
	
	private String customerCode;
	private String ptmsPtnm;
	private String ptmsEmail;
	private String mobileNumber;
	private String invoiceNumber;
	private double totalInvoiceAmount;
	private String remarks;
	private String pudAgentMobileNumber;
	private String flag;
	private String agentId;
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getPtmsPtnm() {
		return ptmsPtnm;
	}
	public void setPtmsPtnm(String ptmsPtnm) {
		this.ptmsPtnm = ptmsPtnm;
	}
	public String getPtmsEmail() {
		return ptmsEmail;
	}
	public void setPtmsEmail(String ptmsEmail) {
		this.ptmsEmail = ptmsEmail;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getPudAgentMobileNumber() {
		return pudAgentMobileNumber;
	}
	public void setPudAgentMobileNumber(String pudAgentMobileNumber) {
		this.pudAgentMobileNumber = pudAgentMobileNumber;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public double getTotalInvoiceAmount() {
		return totalInvoiceAmount;
	}
	public void setTotalInvoiceAmount(double totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	
	
	
	

}
