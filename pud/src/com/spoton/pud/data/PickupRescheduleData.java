package com.spoton.pud.data;

import java.util.Date;

public class PickupRescheduleData {
	
   private int PickUpScheduleID; 
	
	
	private String RefNo;

	
	private int StatusId;

	
	private String AppMobileuserName;

	
	private String RescheduleDateTime;

	
	private String Remarks;
	
	
	private String ConNumbers;
	
	
	private String UpdatedBy;
	
	
	private double GPSValue_LAT;
	
	
	private double GPSValue_LON;
	
	
	private String ReasonId;
	
	private String ContactPersonName;
	
	private String ContactPersonPhone;


	public int getPickUpScheduleID() {
		return PickUpScheduleID;
	}


	public void setPickUpScheduleID(int pickUpScheduleID) {
		PickUpScheduleID = pickUpScheduleID;
	}


	public String getRefNo() {
		return RefNo;
	}


	public void setRefNo(String refNo) {
		RefNo = refNo;
	}


	public int getStatusId() {
		return StatusId;
	}


	public void setStatusId(int statusId) {
		StatusId = statusId;
	}


	public String getAppMobileuserName() {
		return AppMobileuserName;
	}


	public void setAppMobileuserName(String appMobileuserName) {
		AppMobileuserName = appMobileuserName;
	}


	public String getRescheduleDateTime() {
		return RescheduleDateTime;
	}


	public void setRescheduleDateTime(String rescheduleDateTime) {
		RescheduleDateTime = rescheduleDateTime;
	}


	public String getRemarks() {
		return Remarks;
	}


	public void setRemarks(String remarks) {
		Remarks = remarks;
	}


	public String getConNumbers() {
		return ConNumbers;
	}


	public void setConNumbers(String conNumbers) {
		ConNumbers = conNumbers;
	}


	public String getUpdatedBy() {
		return UpdatedBy;
	}


	public void setUpdatedBy(String updatedBy) {
		UpdatedBy = updatedBy;
	}


	public double getGPSValue_LAT() {
		return GPSValue_LAT;
	}


	public void setGPSValue_LAT(double gPSValue_LAT) {
		GPSValue_LAT = gPSValue_LAT;
	}


	public double getGPSValue_LON() {
		return GPSValue_LON;
	}


	public void setGPSValue_LON(double gPSValue_LON) {
		GPSValue_LON = gPSValue_LON;
	}


	public String getReasonId() {
		return ReasonId;
	}


	public void setReasonId(String reasonId) {
		ReasonId = reasonId;
	}


	public String getContactPersonName() {
		return ContactPersonName;
	}


	public void setContactPersonName(String contactPersonName) {
		ContactPersonName = contactPersonName;
	}


	public String getContactPersonPhone() {
		return ContactPersonPhone;
	}


	public void setContactPersonPhone(String contactPersonPhone) {
		ContactPersonPhone = contactPersonPhone;
	}
	
	
	

}
