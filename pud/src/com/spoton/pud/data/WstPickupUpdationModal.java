package com.spoton.pud.data;

import java.util.List;

public class WstPickupUpdationModal {

	private int conEntryId;
	private List<WstPickupUpdationDataList> wstPickupUpdationData;
	public int getConEntryId() {
		return conEntryId;
	}
	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}
	public List<WstPickupUpdationDataList> getWstPickupUpdationData() {
		return wstPickupUpdationData;
	}
	public void setWstPickupUpdationData(List<WstPickupUpdationDataList> wstPickupUpdationData) {
		this.wstPickupUpdationData = wstPickupUpdationData;
	}
	
	
}
