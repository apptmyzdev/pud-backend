package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class Pincode {
	private int pinCode;
	private int isAirServicable;
	private int isAirServicableDel;
	private String servicable;
	private String branchCode;
	private String locationName;
	private boolean active;
	private String areaId;
	private String pincodeType;
//	private String activeStatus;
	private String serviceType;
	private String region;
	private String area;
	private String depot;
	private int version;
	private String timestamp;

	// ** added 23-07-2020 - akshay
	private String accountCode;

	private String flag;

	private int pickupAllowed;

	private int deliveryAllowed;
	// **

	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public String getServicable() {
		return servicable;
	}

	public void setServicable(String servicable) {
		this.servicable = servicable;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

//	public String getActiveStatus() {
//		return activeStatus;
//	}
//	public void setActiveStatus(String activeStatus) {
//		this.activeStatus = activeStatus;
//	}
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getIsAirServicable() {
		return isAirServicable;
	}

	public void setIsAirServicable(int isAirServicable) {
		this.isAirServicable = isAirServicable;
	}

	// @Override
//	public String toString() {
//		return "PincodeMasterDeviceResponse [pinCode=" + pinCode
//				+ ", servicable=" + servicable + ", branchCode=" + branchCode
//				+ ", locationName=" + locationName + ", active=" + active
//				+ ", activeStatus=" + activeStatus + ", serviceType="
//				+ serviceType + ", version=" + version + "]";
//	}
	/*
	 * @Override public String toString() { return "Pincode [pinCode=" + pinCode +
	 * ", servicable=" + servicable + ", branchCode=" + branchCode +
	 * ", locationName=" + locationName + ", active=" + active + ", serviceType=" +
	 * serviceType + ", version=" + version + ", isAirServicable=" + isAirServicable
	 * +"]"; }
	 */

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public int getIsAirServicableDel() {
		return isAirServicableDel;
	}

	public void setIsAirServicableDel(int isAirServicableDel) {
		this.isAirServicableDel = isAirServicableDel;
	}

	public String getPincodeType() {
		return pincodeType;
	}

	public void setPincodeType(String pincodeType) {
		this.pincodeType = pincodeType;
	}

	/** added on Jul 23, 2020 **/
	public String getAccountCode() {
		return accountCode;
	}

	/** added on Jul 23, 2020 **/
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/** added on Jul 23, 2020 **/
	public String getFlag() {
		return flag;
	}

	/** added on Jul 23, 2020 **/
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/** added on Jul 23, 2020 **/
	public int getPickupAllowed() {
		return pickupAllowed;
	}

	/** added on Jul 23, 2020 **/
	public void setPickupAllowed(int pickupAllowed) {
		this.pickupAllowed = pickupAllowed;
	}

	/** added on Jul 23, 2020 **/
	public int getDeliveryAllowed() {
		return deliveryAllowed;
	}

	/** added on Jul 23, 2020 **/
	public void setDeliveryAllowed(int deliveryAllowed) {
		this.deliveryAllowed = deliveryAllowed;
	}

	
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDepot() {
		return depot;
	}

	public void setDepot(String depot) {
		this.depot = depot;
	}

	
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String toString() {
		return pinCode + "," + servicable + "," + branchCode + "," + (active ? 1 : 0) + "," + serviceType + ","
				+ version + "," + isAirServicable + "," + isAirServicableDel + "," + areaId + "," + pincodeType + ","
				+ accountCode + "," + flag + "," + pickupAllowed + "," + deliveryAllowed+ "," +region +","+depot+ "," +area+","+timestamp;
	}

}
