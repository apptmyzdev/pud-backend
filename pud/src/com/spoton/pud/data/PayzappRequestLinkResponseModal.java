package com.spoton.pud.data;

public class PayzappRequestLinkResponseModal {
	
	private String ErrorStatus;
	private String ErrorMsg;
	private String CustomerCode;
	private String CustomerName;
	private String PTMSEMAIL;
	private String MOBILENO;
	private String InvoiceNumber;
	private double TotalInvoiceAmount;
	private String ResponseRemarks;
	public String getErrorStatus() {
		return ErrorStatus;
	}
	public void setErrorStatus(String errorStatus) {
		ErrorStatus = errorStatus;
	}
	public String getErrorMsg() {
		return ErrorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		ErrorMsg = errorMsg;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getPTMSEMAIL() {
		return PTMSEMAIL;
	}
	public void setPTMSEMAIL(String pTMSEMAIL) {
		PTMSEMAIL = pTMSEMAIL;
	}
	public String getMOBILENO() {
		return MOBILENO;
	}
	public void setMOBILENO(String mOBILENO) {
		MOBILENO = mOBILENO;
	}
	public String getInvoiceNumber() {
		return InvoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		InvoiceNumber = invoiceNumber;
	}
	public double getTotalInvoiceAmount() {
		return TotalInvoiceAmount;
	}
	public void setTotalInvoiceAmount(double totalInvoiceAmount) {
		TotalInvoiceAmount = totalInvoiceAmount;
	}
	public String getResponseRemarks() {
		return ResponseRemarks;
	}
	public void setResponseRemarks(String responseRemarks) {
		ResponseRemarks = responseRemarks;
	} 

	
	
}
