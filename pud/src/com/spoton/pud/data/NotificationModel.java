package com.spoton.pud.data;

public class NotificationModel {
	private String con;
	private String notificationPreference;  //both notify - PLTPUD, only on delivery - DELY
	//private String trackType;  //C - conNo , R - reference no
	private int sub;    //1- subscription on , 0 - subscription off
	public String getCon() {
		return con;
	}
	public void setCon(String con) {
		this.con = con;
	}
	public String getNotificationPreference() {
		return notificationPreference;
	}
	public void setNotificationPreference(String notificationPreference) {
		this.notificationPreference = notificationPreference;
	}
//	public String getTrackType() {
//		return trackType;
//	}
//	public void setTrackType(String trackType) {
//		this.trackType = trackType;
//	}
	public int getSub() {
		return sub;
	}
	public void setSub(int sub) {
		this.sub = sub;
	}
	@Override
	public String toString() {
		return "NotificationModel [con=" + con + ", notificationPreference=" + notificationPreference + ", sub=" + sub + "]";
	}
	
}
