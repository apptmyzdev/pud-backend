package com.spoton.pud.data;

public class ReassignPickupInputModal {
	
	private Integer pickupScheduleId;
	private String refNo;
	private String userId;
	private String toUserId;
	private String reassignDateTime;
	private String remarks;
	private String pickupType;//A- Assigned R-Registered
	private double gpsValueLat;
	private double gpsValueLon;
	public Integer getPickupScheduleId() {
		return pickupScheduleId;
	}
	public void setPickupScheduleId(Integer pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getToUserId() {
		return toUserId;
	}
	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}
	
	
	public String getReassignDateTime() {
		return reassignDateTime;
	}
	public void setReassignDateTime(String reassignDateTime) {
		this.reassignDateTime = reassignDateTime;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public double getGpsValueLat() {
		return gpsValueLat;
	}
	public void setGpsValueLat(double gpsValueLat) {
		this.gpsValueLat = gpsValueLat;
	}
	public double getGpsValueLon() {
		return gpsValueLon;
	}
	public void setGpsValueLon(double gpsValueLon) {
		this.gpsValueLon = gpsValueLon;
	}
	public String getPickupType() {
		return pickupType;
	}
	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}
	
	

}
