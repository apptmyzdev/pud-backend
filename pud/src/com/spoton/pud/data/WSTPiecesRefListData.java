package com.spoton.pud.data;

public class WSTPiecesRefListData {
	
	private String ConNumber;
	private String PieceNo;
	private String PieceRefNo;
	private String PieceSlNo;
	public String getConNumber() {
		return ConNumber;
	}
	public void setConNumber(String conNumber) {
		ConNumber = conNumber;
	}
	public String getPieceNo() {
		return PieceNo;
	}
	public void setPieceNo(String pieceNo) {
		PieceNo = pieceNo;
	}
	public String getPieceRefNo() {
		return PieceRefNo;
	}
	public void setPieceRefNo(String pieceRefNo) {
		PieceRefNo = pieceRefNo;
	}
	public String getPieceSlNo() {
		return PieceSlNo;
	}
	public void setPieceSlNo(String pieceSlNo) {
		PieceSlNo = pieceSlNo;
	}
	
	
	

}
