package com.spoton.pud.data;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.spoton.pud.data.PickupUpdateStatusModal;

public class PickupUpdatinResponse {

	@SerializedName("PickUpUpdateStatus")
	List<PickupUpdateStatusModal> pickUpUpdateStatus;

	public List<PickupUpdateStatusModal> getPickUpUpdateStatus() {
		return pickUpUpdateStatus;
	}

	public void setPickUpUpdateStatus(
			List<PickupUpdateStatusModal> pickUpUpdateStatus) {
		this.pickUpUpdateStatus = pickUpUpdateStatus;
	}
	
	
}
