package com.spoton.pud.data;

public class EwayBillNumberInfoErpModal {

	private String UserGSTIN;
	private String InvoiceNo;
	private String InvoiceDate;
	private String DelPincode;
	private String InvoiceAmount;
	private String RejectStatus;
	private String ValidUpto;
	private String SenderName;  
	private String SenderAddress;  
	private String SenderCity;  
	private String ReceiverName; 
	private String ReceiverAddress; 
	private String ReceiverCity;  
	private String OriginPincode;  
	public String getUserGSTIN() {
		return UserGSTIN;
	}
	public void setUserGSTIN(String userGSTIN) {
		UserGSTIN = userGSTIN;
	}
	public String getInvoiceNo() {
		return InvoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}
	public String getInvoiceDate() {
		return InvoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		InvoiceDate = invoiceDate;
	}
	public String getDelPincode() {
		return DelPincode;
	}
	public void setDelPincode(String delPincode) {
		DelPincode = delPincode;
	}
	public String getInvoiceAmount() {
		return InvoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		InvoiceAmount = invoiceAmount;
	}
	public String getRejectStatus() {
		return RejectStatus;
	}
	public void setRejectStatus(String rejectStatus) {
		RejectStatus = rejectStatus;
	}
	public String getValidUpto() {
		return ValidUpto;
	}
	public void setValidUpto(String validUpto) {
		ValidUpto = validUpto;
	}
	public String getSenderName() {
		return SenderName;
	}
	public void setSenderName(String senderName) {
		SenderName = senderName;
	}
	public String getSenderAddress() {
		return SenderAddress;
	}
	public void setSenderAddress(String senderAddress) {
		SenderAddress = senderAddress;
	}
	public String getSenderCity() {
		return SenderCity;
	}
	public void setSenderCity(String senderCity) {
		SenderCity = senderCity;
	}
	public String getReceiverName() {
		return ReceiverName;
	}
	public void setReceiverName(String receiverName) {
		ReceiverName = receiverName;
	}
	public String getReceiverAddress() {
		return ReceiverAddress;
	}
	public void setReceiverAddress(String receiverAddress) {
		ReceiverAddress = receiverAddress;
	}
	public String getReceiverCity() {
		return ReceiverCity;
	}
	public void setReceiverCity(String receiverCity) {
		ReceiverCity = receiverCity;
	}
	public String getOriginPincode() {
		return OriginPincode;
	}
	public void setOriginPincode(String originPincode) {
		OriginPincode = originPincode;
	}
	
	
	
	
}
