package com.spoton.pud.data;

import java.util.List;

public class PincodeMasterDevResponse {
	private boolean status;
	private String message;
	private List<Pincode> data;
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<Pincode> getData() {
		return data;
	}
	public void setData(List<Pincode> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "PincodeMasterDevResponse [status=" + status + ", message="
				+ message + ", data=" + data + "]";
	}
	
	
}
