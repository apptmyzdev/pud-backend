package com.spoton.pud.data;

public class PieceVolumeData {
	
     private int NoofPieces; 
	
	
	private double VolLength;
	
	
	private double Volbreadth;
	
	
	private double VolHeight;
	
	
	private double TotalVolWeight;

	private double ACTWTPERPKG;
	
	private int id;
	
	private String ContentTypeId;  

	public int getNoofPieces() {
		return NoofPieces;
	}


	public void setNoofPieces(int noofPieces) {
		NoofPieces = noofPieces;
	}


	public double getVolLength() {
		return VolLength;
	}


	public void setVolLength(double volLength) {
		VolLength = volLength;
	}


	public double getVolbreadth() {
		return Volbreadth;
	}


	public void setVolbreadth(double volbreadth) {
		Volbreadth = volbreadth;
	}


	public double getVolHeight() {
		return VolHeight;
	}


	public void setVolHeight(double volHeight) {
		VolHeight = volHeight;
	}


	public double getTotalVolWeight() {
		return TotalVolWeight;
	}


	public void setTotalVolWeight(double totalVolWeight) {
		TotalVolWeight = totalVolWeight;
	}


	public double getACTWTPERPKG() {
		return ACTWTPERPKG;
	}


	public void setACTWTPERPKG(double aCTWTPERPKG) {
		ACTWTPERPKG = aCTWTPERPKG;
	}


	public String getContentTypeId() {
		return ContentTypeId;
	}


	public void setContentTypeId(String contentTypeId) {
		ContentTypeId = contentTypeId;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	

}
