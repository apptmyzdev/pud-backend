package com.spoton.pud.data;

public class PaymentStausErpResonseModal {

	private String CustomerCode;
	private String CustomerName;
	private String InvoiceNo;
	private String PGTransactionId;
	private String Amount;
	private String Success;
	private String CreatedOn;
	private String ConNo;
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getInvoiceNo() {
		return InvoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}
	public String getPGTransactionId() {
		return PGTransactionId;
	}
	public void setPGTransactionId(String pGTransactionId) {
		PGTransactionId = pGTransactionId;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getSuccess() {
		return Success;
	}
	public void setSuccess(String success) {
		Success = success;
	}
	public String getCreatedOn() {
		return CreatedOn;
	}
	public void setCreatedOn(String createdOn) {
		CreatedOn = createdOn;
	}
	public String getConNo() {
		return ConNo;
	}
	public void setConNo(String conNo) {
		ConNo = conNo;
	}
	
	
	
}
