package com.spoton.pud.data;

import java.util.Properties;    

import javax.mail.*;    
import javax.mail.internet.*;    

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
public class Mailer{  
    public static boolean send(final String from,final String password,String to,String sub,String msg,Logger logger){  
    	Gson gson = new GsonBuilder().serializeNulls().create();
    	logger.info("******Sending mail******");
    	 logger.info("from address "+from+" password "+password+" to address "+to+" subject "+sub);
    	 logger.info("Mail message  "+msg);
          //Get properties object    
          Properties props = new Properties();   
          props.put("mail.smtp.host", "smtp.gmail.com");    
          props.put("mail.smtp.socketFactory.port", "465");    
          props.put("mail.smtp.socketFactory.class",    
                    "javax.net.ssl.SSLSocketFactory");    
          props.put("mail.smtp.auth", "true");    
          props.put("mail.smtp.port", "465");    
          //get Session   
          Session session = Session.getDefaultInstance(props,    
           new javax.mail.Authenticator() {    
           protected PasswordAuthentication getPasswordAuthentication() {    
           return new PasswordAuthentication(from,password);  
           }    
          });    
          //compose message    
          try {    
           MimeMessage message = new MimeMessage(session);    
           message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));//To address
           InternetAddress[] addressTo = new InternetAddress[6]; 
		    addressTo[0] = new InternetAddress("varsha.raparthi@apptmyz.com");
		    addressTo[1] = new InternetAddress("twinkle.kamdar@apptmyz.com");
		    addressTo[2] = new InternetAddress("anuraag@apptmyz.com");
		    addressTo[3] = new InternetAddress("manjunath.swamy@spoton.co.in");
		    addressTo[4] = new InternetAddress("alok.b@spoton.co.in");
		    addressTo[5] = new InternetAddress("akshay.gundawar@apptmyz.com");
		    message.setRecipients(Message.RecipientType.CC, addressTo); //CC addresses
		    logger.info("Recipients To "+new InternetAddress(to));
		    logger.info("Recipients CC "+gson.toJson(addressTo));
		   message.setSubject(sub);    
           message.setText(msg);    
           //send message  
           Transport.send(message);    
           //System.out.println("message sent successfully");  
           logger.info("******Mail sent successfully******");
           return true;
          } catch (MessagingException e) {
        	  e.printStackTrace();
        	  logger.error("Excepton in sending mail ",e);
        	  return false;
        	  
          }    
             
    }  
}  