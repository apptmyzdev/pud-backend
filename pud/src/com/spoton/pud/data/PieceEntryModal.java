package com.spoton.pud.data;



public class PieceEntryModal {

	
	private String pcrKey; 
	
	
	private String fromPieceNo; 
	
	
	private String toPieceNo; 
	
	
	private String totalPieces;
	
	private int id;

	public String getPcrKey() {
		return pcrKey;
	}

	public void setPcrKey(String pcrKey) {
		this.pcrKey = pcrKey;
	}

	public String getFromPieceNo() {
		return fromPieceNo;
	}

	public void setFromPieceNo(String fromPieceNo) {
		this.fromPieceNo = fromPieceNo;
	}

	public String getToPieceNo() {
		return toPieceNo;
	}

	public void setToPieceNo(String toPieceNo) {
		this.toPieceNo = toPieceNo;
	}

	public String getTotalPieces() {
		return totalPieces;
	}

	public void setTotalPieces(String totalPieces) {
		this.totalPieces = totalPieces;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
}
