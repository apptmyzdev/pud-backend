package com.spoton.pud.data;

public class GoogleDirectionsResponse {

	private String status;
	private Routes[] routes;
	private GeocodedWayPoints[] geocoded_waypoints;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Routes[] getRoutes() {
		return routes;
	}
	public void setRoutes(Routes[] routes) {
		this.routes = routes;
	}
	public GeocodedWayPoints[] getGeocoded_waypoints() {
		return geocoded_waypoints;
	}
	public void setGeocoded_waypoints(GeocodedWayPoints[] geocoded_waypoints) {
		this.geocoded_waypoints = geocoded_waypoints;
	}
	
	
}
