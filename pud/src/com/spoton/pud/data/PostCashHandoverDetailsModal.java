package com.spoton.pud.data;

import java.util.List;

public class PostCashHandoverDetailsModal {
	
	private List<String> docketNumbersList;
	private String empCode;
	private String userId;
	public List<String> getDocketNumbersList() {
		return docketNumbersList;
	}
	public void setDocketNumbersList(List<String> docketNumbersList) {
		this.docketNumbersList = docketNumbersList;
	}
	public String getEmpCode() {
		return empCode;
	}
	public void setEmpCode(String empCode) {
		this.empCode = empCode;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
