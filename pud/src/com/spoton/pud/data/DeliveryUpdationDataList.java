package com.spoton.pud.data;

import java.util.List;

public class DeliveryUpdationDataList {

	private List<DeliveryUpdationData> DeliveryEntity;

	public List<DeliveryUpdationData> getDeliveryEntity() {
		return DeliveryEntity;
	}

	public void setDeliveryEntity(List<DeliveryUpdationData> deliveryEntity) {
		DeliveryEntity = deliveryEntity;
	}
	
	
}
