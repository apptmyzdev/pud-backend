package com.spoton.pud.data;

import java.util.List;

public class PickupUpdationModalV3 {

	private List<ConDetailsModalV3> conDetails;

	public List<ConDetailsModalV3> getConDetails() {
		return conDetails;
	}

	public void setConDetails(List<ConDetailsModalV3> conDetails) {
		this.conDetails = conDetails;
	}
	
	
}
