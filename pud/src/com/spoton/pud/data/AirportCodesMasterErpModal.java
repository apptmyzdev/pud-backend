package com.spoton.pud.data;

public class AirportCodesMasterErpModal {
	
	private String Airportid;
	private String AirportCode;
	private String AirportDescription;
	public String getAirportid() {
		return Airportid;
	}
	public void setAirportid(String airportid) {
		Airportid = airportid;
	}
	public String getAirportCode() {
		return AirportCode;
	}
	public void setAirportCode(String airportCode) {
		AirportCode = airportCode;
	}
	public String getAirportDescription() {
		return AirportDescription;
	}
	public void setAirportDescription(String airportDescription) {
		AirportDescription = airportDescription;
	}
	

	
}
