package com.spoton.pud.data;

public class SubmitPickupExecutionModal {
	
	private String conNumber;
	private String pieceNumber;
	private String isConScanned;
	private String scannedDateTime;
	private String isPieceScanned;
	private String userId;
	public String getConNumber() {
		return conNumber;
	}
	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}
	public String getPieceNumber() {
		return pieceNumber;
	}
	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}
	public String getIsConScanned() {
		return isConScanned;
	}
	public void setIsConScanned(String isConScanned) {
		this.isConScanned = isConScanned;
	}
	public String getScannedDateTime() {
		return scannedDateTime;
	}
	public void setScannedDateTime(String scannedDateTime) {
		this.scannedDateTime = scannedDateTime;
	}
	public String getIsPieceScanned() {
		return isPieceScanned;
	}
	public void setIsPieceScanned(String isPieceScanned) {
		this.isPieceScanned = isPieceScanned;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	

}
