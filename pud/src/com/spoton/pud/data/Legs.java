package com.spoton.pud.data;

public class Legs {
	
	private Duration duration;
	private Duration distance;

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public Duration getDistance() {
		return distance;
	}

	public void setDistance(Duration distance) {
		this.distance = distance;
	}
	
	
	
	

}
