package com.spoton.pud.data;

public class DangerousCodesMasterModal {

	private String dangeorusId;
	private String dangeorusCode;
	private String dangeorusDesc;
	public String getDangeorusId() {
		return dangeorusId;
	}
	public void setDangeorusId(String dangeorusId) {
		this.dangeorusId = dangeorusId;
	}
	public String getDangeorusCode() {
		return dangeorusCode;
	}
	public void setDangeorusCode(String dangeorusCode) {
		this.dangeorusCode = dangeorusCode;
	}
	public String getDangeorusDesc() {
		return dangeorusDesc;
	}
	public void setDangeorusDesc(String dangeorusDesc) {
		this.dangeorusDesc = dangeorusDesc;
	}
	
	
}
