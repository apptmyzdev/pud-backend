package com.spoton.pud.data;

public class DeliveryConditionImages {
	
	private String pieceNumber;
	private String deliveryConditionImage;
	public String getPieceNumber() {
		return pieceNumber;
	}
	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}
	public String getDeliveryConditionImage() {
		return deliveryConditionImage;
	}
	public void setDeliveryConditionImage(String deliveryConditionImage) {
		this.deliveryConditionImage = deliveryConditionImage;
	}
	
	

}
