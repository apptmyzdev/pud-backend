package com.spoton.pud.data;

import java.util.Date;

public class DocketSeriesResponseModel {

	private int id;
	
	private String docketUserKey;

	private String branchCode;

	private int docketKey;

	private String fromSeries;

	private String lastSrNumber;

	private String toSeries;

	private String totalLeaf;

	private int userId;

	private String userName;

	private String vendorCode;
	
	private String productType;
	
	private Date createdTimestamp;

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public DocketSeriesResponseModel() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public int getDocketKey() {
		return this.docketKey;
	}

	public void setDocketKey(int docketKey) {
		this.docketKey = docketKey;
	}

	public String getFromSeries() {
		return this.fromSeries;
	}

	public void setFromSeries(String fromSeries) {
		this.fromSeries = fromSeries;
	}

	public String getLastSrNumber() {
		return this.lastSrNumber;
	}

	public void setLastSrNumber(String lastSrNumber) {
		this.lastSrNumber = lastSrNumber;
	}

	public String getToSeries() {
		return this.toSeries;
	}

	public void setToSeries(String toSeries) {
		this.toSeries = toSeries;
	}

	public String getTotalLeaf() {
		return this.totalLeaf;
	}

	public void setTotalLeaf(String totalLeaf) {
		this.totalLeaf = totalLeaf;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getDocketUserKey() {
		return docketUserKey;
	}

	public void setDocketUserKey(String docketUserKey) {
		this.docketUserKey = docketUserKey;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
}