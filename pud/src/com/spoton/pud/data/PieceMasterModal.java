package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PieceMasterModal {
	
	
	@SerializedName("PKey")
	private int pkey;
	
	@SerializedName("Complete")
	private String complete;
	
	@SerializedName("End_no")
	private String endNo;
	
	@SerializedName("LastPiece_No")
	private String lastPieceNo;
	
	@SerializedName("Series_Type")
	private String seriesType;
	
	@SerializedName("Start_No")
	private String startNo;
	
	@SerializedName("Total_issued")
	private String totalIsssued;
	
	@SerializedName("Total_Used")
	private String totalUsed;
	
	private String userId;
	
	public int getPkey() {
		return pkey;
	}
	public void setPkey(int pkey) {
		this.pkey = pkey;
	}
	public String getComplete() {
		return complete;
	}
	public void setComplete(String complete) {
		this.complete = complete;
	}
	public String getEndNo() {
		return endNo;
	}
	public void setEndNo(String endNo) {
		this.endNo = endNo;
	}
	public String getLastPieceNo() {
		return lastPieceNo;
	}
	public void setLastPieceNo(String lastPieceNo) {
		this.lastPieceNo = lastPieceNo;
	}
	public String getSeriesType() {
		return seriesType;
	}
	public void setSeriesType(String seriesType) {
		this.seriesType = seriesType;
	}
	public String getStartNo() {
		return startNo;
	}
	public void setStartNo(String startNo) {
		this.startNo = startNo;
	}
	public String getTotalIsssued() {
		return totalIsssued;
	}
	public void setTotalIsssued(String totalIsssued) {
		this.totalIsssued = totalIsssued;
	}
	public String getTotalUsed() {
		return totalUsed;
	}
	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

    
	
	
}
