package com.spoton.pud.data;

import java.util.List;

public class PulledPickupsModal {

	private String userId;
	private List<Integer> pickupScheduleIds;

      
		
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<Integer> getPickupScheduleIds() {
		return pickupScheduleIds;
	}
	public void setPickupScheduleIds(List<Integer> pickupScheduleIds) {
		this.pickupScheduleIds = pickupScheduleIds;
	}
	
	
}
