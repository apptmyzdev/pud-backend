package com.spoton.pud.data;

public class PostCashHandoverDetailsErpResponseModal {
	
	private boolean Result;
	private String Message;
	
	
	public boolean isResult() {
		return Result;
	}
	public void setResult(boolean result) {
		Result = result;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	
	
	

}
