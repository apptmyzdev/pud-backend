package com.spoton.pud.data;

public class UnloadingSheetsDataModal {
	
	private String vehicleNumber;
	private String sheetNumber;
	private int NumberOfCons;
	private int NumberOfPieces;
	private double totalWeight;
	private String generatedTime;
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getSheetNumber() {
		return sheetNumber;
	}
	public void setSheetNumber(String sheetNumber) {
		this.sheetNumber = sheetNumber;
	}
	
	
	
	public int getNumberOfCons() {
		return NumberOfCons;
	}
	public void setNumberOfCons(int numberOfCons) {
		NumberOfCons = numberOfCons;
	}
	public int getNumberOfPieces() {
		return NumberOfPieces;
	}
	public void setNumberOfPieces(int numberOfPieces) {
		NumberOfPieces = numberOfPieces;
	}
	public double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}
	public String getGeneratedTime() {
		return generatedTime;
	}
	public void setGeneratedTime(String generatedTime) {
		this.generatedTime = generatedTime;
	}
	
	

}
