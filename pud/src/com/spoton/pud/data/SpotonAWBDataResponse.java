package com.spoton.pud.data;

public class SpotonAWBDataResponse {
	boolean	isInserted;
	String	awbNumber;
	String	consignmentID;
	String 	errorDescription;
	public boolean isInserted() {
		return isInserted;
	}
	public void setInserted(boolean isInserted) {
		this.isInserted = isInserted;
	}
	public String getAwbNumber() {
		return awbNumber;
	}
	public void setAwbNumber(String awbNumber) {
		this.awbNumber = awbNumber;
	}
	public String getConsignmentID() {
		return consignmentID;
	}
	public void setConsignmentID(String consignmentID) {
		this.consignmentID = consignmentID;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
}
