package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupCancellationMasterModal {

	@SerializedName("StatusID")
	private int statusId;
	
	@SerializedName("Status") 
	private String status;
	
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}


