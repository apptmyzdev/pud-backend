package com.spoton.pud.data;

public class MobileDataDownloaded {

	private String TransactionId;
	
	private String DownloadType;
	
	private String DownloadDate;

	public String getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(String transactionId) {
		TransactionId = transactionId;
	}

	public String getDownloadType() {
		return DownloadType;
	}

	public void setDownloadType(String downloadType) {
		DownloadType = downloadType;
	}

	public String getDownloadDate() {
		return DownloadDate;
	}

	public void setDownloadDate(String downloadDate) {
		DownloadDate = downloadDate;
	}
	
	
}
