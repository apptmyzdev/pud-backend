package com.spoton.pud.data;

public class VendorInfoModal {

	private String VehicleType;
	private int VehicleId;
	private String VehicleNo;
	private String BacctName;
	private String BACode;
	public String getVehicleType() {
		return VehicleType;
	}
	public void setVehicleType(String vehicleType) {
		VehicleType = vehicleType;
	}
	public int getVehicleId() {
		return VehicleId;
	}
	public void setVehicleId(int vehicleId) {
		VehicleId = vehicleId;
	}
	public String getVehicleNo() {
		return VehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		VehicleNo = vehicleNo;
	}
	public String getBacctName() {
		return BacctName;
	}
	public void setBacctName(String bacctName) {
		BacctName = bacctName;
	}
	public String getBACode() {
		return BACode;
	}
	public void setBACode(String bACode) {
		BACode = bACode;
	}
 
	
	
	
}
