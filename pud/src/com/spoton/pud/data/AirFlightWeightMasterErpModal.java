package com.spoton.pud.data;

public class AirFlightWeightMasterErpModal {
	
	private String Vendorid;
	private String VendorCode;
	private String FlightID;
	private String FlightDesc;
	private String WtAllowed;
	public String getVendorid() {
		return Vendorid;
	}
	public void setVendorid(String vendorid) {
		Vendorid = vendorid;
	}
	public String getVendorCode() {
		return VendorCode;
	}
	public void setVendorCode(String vendorCode) {
		VendorCode = vendorCode;
	}
	public String getFlightID() {
		return FlightID;
	}
	public void setFlightID(String flightID) {
		FlightID = flightID;
	}
	public String getFlightDesc() {
		return FlightDesc;
	}
	public void setFlightDesc(String flightDesc) {
		FlightDesc = flightDesc;
	}
	public String getWtAllowed() {
		return WtAllowed;
	}
	public void setWtAllowed(String wtAllowed) {
		WtAllowed = wtAllowed;
	}
	
	
	

}
