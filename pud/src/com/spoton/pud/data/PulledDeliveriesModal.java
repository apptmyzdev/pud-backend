package com.spoton.pud.data;

import java.util.List;

public class PulledDeliveriesModal {

	private String agentId;
	private List<String> pdcNumbers;
	
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public List<String> getPdcNumbers() {
		return pdcNumbers;
	}
	public void setPdcNumbers(List<String> pdcNumbers) {
		this.pdcNumbers = pdcNumbers;
	}
	
	
	
}
