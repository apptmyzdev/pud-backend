package com.spoton.pud.data;

public class PudDashboardModal {

	private long pickupsSucceeded;
	private long pickupsAttempted;
	private long pickupsPending;
	private long pickupsReassigned;
	
	private long deliveriesSucceeded;
	private long deliveriesAttempted;
	private long deliveriesPending;
	
	
	public long getPickupsSucceeded() {
		return pickupsSucceeded;
	}
	public void setPickupsSucceeded(long pickupsSucceeded) {
		this.pickupsSucceeded = pickupsSucceeded;
	}
	public long getPickupsAttempted() {
		return pickupsAttempted;
	}
	public void setPickupsAttempted(long pickupsAttempted) {
		this.pickupsAttempted = pickupsAttempted;
	}
	public long getPickupsPending() {
		return pickupsPending;
	}
	public void setPickupsPending(long pickupsPending) {
		this.pickupsPending = pickupsPending;
	}
	public long getDeliveriesSucceeded() {
		return deliveriesSucceeded;
	}
	public void setDeliveriesSucceeded(long deliveriesSucceeded) {
		this.deliveriesSucceeded = deliveriesSucceeded;
	}
	public long getDeliveriesAttempted() {
		return deliveriesAttempted;
	}
	public void setDeliveriesAttempted(long deliveriesAttempted) {
		this.deliveriesAttempted = deliveriesAttempted;
	}
	public long getDeliveriesPending() {
		return deliveriesPending;
	}
	public void setDeliveriesPending(long deliveriesPending) {
		this.deliveriesPending = deliveriesPending;
	}
	public long getPickupsReassigned() {
		return pickupsReassigned;
	}
	public void setPickupsReassigned(long pickupsReassigned) {
		this.pickupsReassigned = pickupsReassigned;
	}
	
	

}
