package com.spoton.pud.data;

public class PullPudDataModal {

	private int id;
	private float cft;
	private String contactMobile;
	private String contactPerson;
	private String customerCode;
	private String customerName;
	private String customerRefNumber;
	private String deliveryAddress;
	private String deliveryCity;
	private int deliveryPincode;
	private float minCfgWeight;
	private String pickupAddress;
	private String pickupCity;
	private String pickupDate;
	private String pickupOrderNo;
	private int pickupPincode;
	private int pickupRegisterId;
	private int pickupScheduleId;
	private int pieces;
	private float weight;
	private String userId;
	private String productType;
	/*private String vehicleType;
	private String vehicleNumber;
	private String vehicleCapacity;*/
	
	public float getCft() {
		return cft;
	}
	public void setCft(float cft) {
		this.cft = cft;
	}
	public String getContactMobile() {
		return contactMobile;
	}
	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}
	public String getContactPerson() {
		return contactPerson;
	}
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerRefNumber() {
		return customerRefNumber;
	}
	public void setCustomerRefNumber(String customerRefNumber) {
		this.customerRefNumber = customerRefNumber;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getDeliveryCity() {
		return deliveryCity;
	}
	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}
	public int getDeliveryPincode() {
		return deliveryPincode;
	}
	public void setDeliveryPincode(int deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}
	public float getMinCfgWeight() {
		return minCfgWeight;
	}
	public void setMinCfgWeight(float minCfgWeight) {
		this.minCfgWeight = minCfgWeight;
	}
	public String getPickupAddress() {
		return pickupAddress;
	}
	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}
	public String getPickupCity() {
		return pickupCity;
	}
	public void setPickupCity(String pickupCity) {
		this.pickupCity = pickupCity;
	}
	public String getPickupDate() {
		return pickupDate;
	}
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	public String getPickupOrderNo() {
		return pickupOrderNo;
	}
	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}
	public int getPickupPincode() {
		return pickupPincode;
	}
	public void setPickupPincode(int pickupPincode) {
		this.pickupPincode = pickupPincode;
	}
	public int getPickupRegisterId() {
		return pickupRegisterId;
	}
	public void setPickupRegisterId(int pickupRegisterId) {
		this.pickupRegisterId = pickupRegisterId;
	}
	public int getPickupScheduleId() {
		return pickupScheduleId;
	}
	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}
	public int getPieces() {
		return pieces;
	}
	public void setPieces(int pieces) {
		this.pieces = pieces;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/*public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleCapacity() {
		return vehicleCapacity;
	}
	public void setVehicleCapacity(String vehicleCapacity) {
		this.vehicleCapacity = vehicleCapacity;
	}*/
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	
	
}
