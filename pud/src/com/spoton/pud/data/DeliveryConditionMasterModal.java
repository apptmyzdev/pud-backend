package com.spoton.pud.data;

public class DeliveryConditionMasterModal {

	
	private int conditionId;
	private String conditionName;
	
	public int getConditionId() {
		return this.conditionId;
	}

	public void setConditionId(int conditionId) {
		this.conditionId = conditionId;
	}

	public String getConditionName() {
		return this.conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

}
