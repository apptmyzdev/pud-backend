package com.spoton.pud.data;

public class PieceEntryData {
	
    private String PCR_Key; 
	
	
	private String FromPieceNo; 
	
	
	private String ToPieceNo; 
	
	
	private String TotalPieces;
	
	private int id;


	public String getPCR_Key() {
		return PCR_Key;
	}


	public void setPCR_Key(String pCR_Key) {
		PCR_Key = pCR_Key;
	}


	public String getFromPieceNo() {
		return FromPieceNo;
	}


	public void setFromPieceNo(String fromPieceNo) {
		FromPieceNo = fromPieceNo;
	}


	public String getToPieceNo() {
		return ToPieceNo;
	}


	public void setToPieceNo(String toPieceNo) {
		ToPieceNo = toPieceNo;
	}


	public String getTotalPieces() {
		return TotalPieces;
	}


	public void setTotalPieces(String totalPieces) {
		TotalPieces = totalPieces;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	

}
