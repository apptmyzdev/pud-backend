package com.spoton.pud.data;

import java.util.List;

public class ReassignPickupErpInputModalList {
	
	private List<ReassignPickupErpInputModal> PickUpReassign;

	public List<ReassignPickupErpInputModal> getPickUpReassign() {
		return PickUpReassign;
	}

	public void setPickUpReassign(List<ReassignPickupErpInputModal> pickUpReassign) {
		PickUpReassign = pickUpReassign;
	}
	
	

}
