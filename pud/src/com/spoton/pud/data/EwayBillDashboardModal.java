package com.spoton.pud.data;

public class EwayBillDashboardModal {
	
	String ConId;  
	String ewayBillNo ;
	String pickupOrderNo ;
	String userId  ;
	String pickupDate ; 
	String vehicleNo;  
	String eWayBillStatus ; 
	String errorRemarks ; 
	String validTill ;
	public String getConId() {
		return ConId;
	}
	public void setConId(String conId) {
		ConId = conId;
	}
	public String getEwayBillNo() {
		return ewayBillNo;
	}
	public void setEwayBillNo(String ewayBillNo) {
		this.ewayBillNo = ewayBillNo;
	}
	public String getPickupOrderNo() {
		return pickupOrderNo;
	}
	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPickupDate() {
		return pickupDate;
	}
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String geteWayBillStatus() {
		return eWayBillStatus;
	}
	public void seteWayBillStatus(String eWayBillStatus) {
		this.eWayBillStatus = eWayBillStatus;
	}
	public String getErrorRemarks() {
		return errorRemarks;
	}
	public void setErrorRemarks(String errorRemarks) {
		this.errorRemarks = errorRemarks;
	}
	public String getValidTill() {
		return validTill;
	}
	public void setValidTill(String validTill) {
		this.validTill = validTill;
	}
	
	
	

}
