package com.spoton.pud.data;

import java.util.List;

public class MobileDataDownloadedList {
	
	private List<MobileDataDownloaded> MobDataDownload;

	public List<MobileDataDownloaded> getMobDataDownload() {
		return MobDataDownload;
	}

	public void setMobDataDownload(List<MobileDataDownloaded> mobDataDownload) {
		MobDataDownload = mobDataDownload;
	}
	
	

}
