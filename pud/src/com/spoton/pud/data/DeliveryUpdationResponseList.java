package com.spoton.pud.data;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DeliveryUpdationResponseList {

	@SerializedName("DeliveryUpdateStatus")
	List<DeliveryUpdationResponse> deliveryUpdateStatus;

	public List<DeliveryUpdationResponse> getDeliveryUpdateStatus() {
		return deliveryUpdateStatus;
	}

	public void setDeliveryUpdateStatus(
			List<DeliveryUpdationResponse> deliveryUpdateStatus) {
		this.deliveryUpdateStatus = deliveryUpdateStatus;
	}
	
	
	
}
