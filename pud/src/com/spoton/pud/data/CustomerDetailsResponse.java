package com.spoton.pud.data;

import java.util.List;

public class CustomerDetailsResponse {

	private String sessionId;
	private List<CustomerDetailsModal> customerDetailsList;
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public List<CustomerDetailsModal> getCustomerDetailsList() {
		return customerDetailsList;
	}
	public void setCustomerDetailsList(
			List<CustomerDetailsModal> customerDetailsList) {
		this.customerDetailsList = customerDetailsList;
	}



}
