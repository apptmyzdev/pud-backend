package com.spoton.pud.data;

public class PayzappCheckStatusResponse {
	private String InvoiceNo;
	private double PayAmount;
	private String Status;
	private String ResponseMessage;
	private String ConNo;
	private String RaisedLocation;
	public String getInvoiceNo() {
		return InvoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		InvoiceNo = invoiceNo;
	}
	public double getPayAmount() {
		return PayAmount;
	}
	public void setPayAmount(double payAmount) {
		PayAmount = payAmount;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getResponseMessage() {
		return ResponseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		ResponseMessage = responseMessage;
	}
	public String getConNo() {
		return ConNo;
	}
	public void setConNo(String conNo) {
		ConNo = conNo;
	}
	public String getRaisedLocation() {
		return RaisedLocation;
	}
	public void setRaisedLocation(String raisedLocation) {
		RaisedLocation = raisedLocation;
	}

	

}
