package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupCancelModel {

	private int statusId;
	
	private String status;
	
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}


