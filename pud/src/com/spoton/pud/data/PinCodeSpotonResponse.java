package com.spoton.pud.data;
import com.google.gson.annotations.SerializedName;
import com.spoton.pud.jpa.PinCodeMaster;

public class PinCodeSpotonResponse {

	@SerializedName("PinCode")
	private int pinCode;
	
	@SerializedName("IsServiceable")
	private String servicable;
	
	@SerializedName("Branchcode")
	private String branchCode;
	
	@SerializedName("IsAirServiceable")
	private String isAirServiceable;
	
	@SerializedName("IsAirServiceableDel")
	private String isAirServiceableDel;
	
	@SerializedName("LocationName")
	private String locationName;
	
	@SerializedName("IsActive")
	private boolean active;
	
	@SerializedName("IsActiveStatus")
	private String activeStatus;
	
	@SerializedName("Service_type")
	private String serviceType;
	
	@SerializedName("AreaID")
	private String areaId;
	
	@SerializedName("PType")
	private String pType;

	//** added 23-07-2020 - akshay
	@SerializedName("AccountCode")
	private String accountCode;
	
	@SerializedName("Flag")
	private String flag;
	
	@SerializedName("IsPickupAllowed")
	private boolean pickupAllowed;
	
	@SerializedName("IsDeliveryAllowed")
	private boolean deliveryAllowed;
	//**

	@SerializedName("Region")
	private String region;
	
	@SerializedName("Depot")
	private String depot;
	
	@SerializedName("Area")
	private String area;
	
	private String sessionId;
	
	public int getPinCode() {
		return pinCode;
	}

	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}

	public String getServicable() {
		return servicable;
	}

	public void setServicable(String servicable) {
		this.servicable = servicable;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	

	

	public String getIsAirServiceable() {
		return isAirServiceable;
	}

	public void setIsAirServiceable(String isAirServiceable) {
		this.isAirServiceable = isAirServiceable;
	}

	public String getIsAirServiceableDel() {
		return isAirServiceableDel;
	}

	public void setIsAirServiceableDel(String isAirServiceableDel) {
		this.isAirServiceableDel = isAirServiceableDel;
	}
	
	

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	
	

	public String getpType() {
		return pType;
	}

	public void setpType(String pType) {
		this.pType = pType;
	}
	
	

	/** added on Jul 23, 2020 **/
	public String getAccountCode() {
		return accountCode;
	}

	/** added on Jul 23, 2020 **/
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/** added on Jul 23, 2020 **/
	public String getFlag() {
		return flag;
	}

	/** added on Jul 23, 2020 **/
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/** added on Jul 23, 2020 **/
	public boolean getPickupAllowed() {
		return pickupAllowed;
	}

	/** added on Jul 23, 2020 **/
	public void setPickupAllowed(boolean pickupAllowed) {
		this.pickupAllowed = pickupAllowed;
	}

	/** added on Jul 23, 2020 **/
	public boolean getDeliveryAllowed() {
		return deliveryAllowed;
	}

	/** added on Jul 23, 2020 **/
	public void setDeliveryAllowed(boolean deliveryAllowed) {
		this.deliveryAllowed = deliveryAllowed;
	}

	
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDepot() {
		return depot;
	}

	public void setDepot(String depot) {
		this.depot = depot;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public boolean changed(PinCodeMaster oldPinCode) {
		// TODO Auto-generated method stub
	
		if (this.isActive()){
			if (oldPinCode.getActive() == 0)
				return true;
		}else {
			if (oldPinCode.getActive() == 1)
				return true;
		}
		
		
		if (oldPinCode.getActiveStatus()!=null&&this.getActiveStatus()!=null&&(!this.getActiveStatus().equalsIgnoreCase(oldPinCode.getActiveStatus()))){
			return true;
		}
		/*if (oldPinCode.getId().getBranchCode()!=null&&this.getBranchCode()!=null&&(!this.getBranchCode().equalsIgnoreCase(oldPinCode.getId().getBranchCode()))){
			return true;
		}*/
		if (oldPinCode.getLocationName()!=null&&this.getLocationName()!=null&&(!this.getLocationName().equalsIgnoreCase(oldPinCode.getLocationName()))){
			return true;
		}
		if (oldPinCode.getServicable()!=null&&this.getServicable()!=null&&(!this.getServicable().equalsIgnoreCase(oldPinCode.getServicable()))){
			return true;
		}
		if (oldPinCode.getIsAirServicable()==null||(oldPinCode.getIsAirServicable()!=null&&this.getIsAirServiceable()!=null&&(!this.getIsAirServiceable().equalsIgnoreCase(oldPinCode.getIsAirServicable())))){
			return true;
		}
		
		if (oldPinCode.getIsAirServicableDel()==null||(oldPinCode.getIsAirServicableDel()!=null&&this.getIsAirServiceableDel()!=null&&(!this.getIsAirServiceableDel().equalsIgnoreCase(oldPinCode.getIsAirServicableDel())))){
			return true;
		}
		
		if (oldPinCode.getServiceType()!=null&&this.getServiceType()!=null&&(!this.getServiceType().equalsIgnoreCase(oldPinCode.getServiceType()))){
			return true;
		}
		if (oldPinCode.getAreaId()==null||(oldPinCode.getAreaId()!=null&&this.getAreaId()!=null&&(!this.getAreaId().equalsIgnoreCase(oldPinCode.getAreaId())))){
			return true;
		}
		
		if (oldPinCode.getFlag()==null||(oldPinCode.getFlag()!=null&&this.getFlag()!=null&&(!this.getFlag().equalsIgnoreCase(oldPinCode.getFlag())))){
			return true;
		}
		
		if ((oldPinCode.getPickupAllowed() == 0 && this.getPickupAllowed()) || (oldPinCode.getPickupAllowed() == 1 && !this.getPickupAllowed())){
			return true;
		}
		
		if ((oldPinCode.getDeliveryAllowed() == 0 && this.getDeliveryAllowed()) || (oldPinCode.getDeliveryAllowed() == 1 && !this.getDeliveryAllowed())){
			return true;
		}
		
		if (oldPinCode.getRegion()!=null&&this.getRegion()!=null&&(!this.getRegion().equalsIgnoreCase(oldPinCode.getRegion()))){
			return true;
		}
		
		if (oldPinCode.getDepot()!=null&&this.getDepot()!=null&&(!this.getDepot().equalsIgnoreCase(oldPinCode.getDepot()))){
			return true;
		}
		
		if (oldPinCode.getArea()!=null&&this.getArea()!=null&&(!this.getArea().equalsIgnoreCase(oldPinCode.getArea()))){
			return true;
		}
		
		
		return false;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	
}
