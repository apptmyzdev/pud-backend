package com.spoton.pud.data;

public class MobileDataDownloadResult {

	private boolean ResultFlag;
	
	private int TransactionId;
	
	private String ResultRemarks;

	public boolean isResultFlag() {
		return ResultFlag;
	}

	public void setResultFlag(boolean resultFlag) {
		ResultFlag = resultFlag;
	}

	public int getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(int transactionId) {
		TransactionId = transactionId;
	}

	public String getResultRemarks() {
		return ResultRemarks;
	}

	public void setResultRemarks(String resultRemarks) {
		ResultRemarks = resultRemarks;
	}
	
	
}
