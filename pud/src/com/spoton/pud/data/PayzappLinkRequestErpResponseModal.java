package com.spoton.pud.data;

public class PayzappLinkRequestErpResponseModal {

	private String ErrorStatus;  
	private String ErrorMsg ; 
	private String ResponseRemarks;
	public String getErrorStatus() {
		return ErrorStatus;
	}
	public void setErrorStatus(String errorStatus) {
		ErrorStatus = errorStatus;
	}
	public String getErrorMsg() {
		return ErrorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		ErrorMsg = errorMsg;
	}
	public String getResponseRemarks() {
		return ResponseRemarks;
	}
	public void setResponseRemarks(String responseRemarks) {
		ResponseRemarks = responseRemarks;
	}   
	
	
	
}
