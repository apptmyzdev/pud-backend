package com.spoton.pud.data;



public class PickupExecutionErpResponseModal {
	
	
	private boolean TransationResult;
	
	
	private String TransationMessage;
	
	
	private int ConEntryID;


	public boolean getTransationResult() {
		return TransationResult;
	}


	public void setTransationResult(boolean transationResult) {
		TransationResult = transationResult;
	}


	public String getTransationMessage() {
		return TransationMessage;
	}


	public void setTransationMessage(String transationMessage) {
		TransationMessage = transationMessage;
	}


	public int getConEntryID() {
		return ConEntryID;
	}


	public void setConEntryID(int conEntryID) {
		ConEntryID = conEntryID;
	}


	
}
