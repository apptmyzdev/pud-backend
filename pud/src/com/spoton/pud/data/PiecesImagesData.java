package com.spoton.pud.data;

public class PiecesImagesData {
	
	private String  PiecesImagesURL;
	
	private int id;

	public String getPiecesImagesURL() {
		return PiecesImagesURL;
	}

	public void setPiecesImagesURL(String piecesImagesURL) {
		PiecesImagesURL = piecesImagesURL;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
