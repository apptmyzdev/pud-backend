package com.spoton.pud.data;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class PickupRescheduleResponseList {

	@SerializedName("PickUpResheduleUpdateStatus")
	List<PickupRescheduleResponse> pickUpResheduleUpdateStatus;

	public List<PickupRescheduleResponse> getPickUpResheduleUpdateStatus() {
		return pickUpResheduleUpdateStatus;
	}

	public void setPickUpResheduleUpdateStatus(
			List<PickupRescheduleResponse> pickUpResheduleUpdateStatus) {
		this.pickUpResheduleUpdateStatus = pickUpResheduleUpdateStatus;
	}

	
	

	
	
	
	
}
