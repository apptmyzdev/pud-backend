package com.spoton.pud.data;

public class AutoPickupUnloadingResponseModal {
	
	private String transationResult;
	private String transationMessage;
	private String pickupUnloadingSheetNo;
	private int transactionId;
	public String getTransationResult() {
		return transationResult;
	}
	public void setTransationResult(String transationResult) {
		this.transationResult = transationResult;
	}
	public String getTransationMessage() {
		return transationMessage;
	}
	public void setTransationMessage(String transationMessage) {
		this.transationMessage = transationMessage;
	}
	public String getPickupUnloadingSheetNo() {
		return pickupUnloadingSheetNo;
	}
	public void setPickupUnloadingSheetNo(String pickupUnloadingSheetNo) {
		this.pickupUnloadingSheetNo = pickupUnloadingSheetNo;
	}
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	
	

}
