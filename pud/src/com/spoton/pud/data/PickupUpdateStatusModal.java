package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupUpdateStatusModal {
	
	
	@SerializedName("TransationResult")
	private String transactionResult;
	

	@SerializedName("Flag")
	private String flag;
	
	
	@SerializedName("ConEntryID")
	private int conEntryId;

	public int getConEntryId() {
		return conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}


	

}
