package com.spoton.pud.data;

import java.util.List;

public class SaveDrsAcknowledgedConsErpModalList {
	
	private List<SaveDrsAcknowledgedConsErpModal> DRSAcknowledgeInput;

	public List<SaveDrsAcknowledgedConsErpModal> getDRSAcknowledgeInput() {
		return DRSAcknowledgeInput;
	}

	public void setDRSAcknowledgeInput(List<SaveDrsAcknowledgedConsErpModal> dRSAcknowledgeInput) {
		DRSAcknowledgeInput = dRSAcknowledgeInput;
	}
	
	

}
