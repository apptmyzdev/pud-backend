package com.spoton.pud.data;

import java.util.Collection;
import java.util.List;

public class PushNotificationModel {
	private Collection<String> registration_ids;
	private FCMNotificationContentModel notification;
	private Object data;

	public PushNotificationModel() {
		super();
	}



	public Collection<String> getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(Collection<String> registration_ids) {
		this.registration_ids = registration_ids;
	}

	public FCMNotificationContentModel getNotification() {
		return notification;
	}

	public void setNotification(FCMNotificationContentModel notification) {
		this.notification = notification;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}

