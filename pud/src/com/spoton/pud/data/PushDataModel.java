package com.spoton.pud.data;

public class PushDataModel {

	private int id;
	private int masterType;//1-Pincode Master//2-bank master// 3-content master// 4-Customer Service Master// 5-Delivery Condition Master// 6-Delivery Sub condition master
	private boolean updateMandatory;
    
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMasterType() {
		return masterType;
	}

	public void setMasterType(int masterType) {
		this.masterType = masterType;
	}

	public boolean isUpdateMandatory() {
		return updateMandatory;
	}

	public void setUpdateMandatory(boolean updateMandatory) {
		this.updateMandatory = updateMandatory;
	}

	

	
	
}
