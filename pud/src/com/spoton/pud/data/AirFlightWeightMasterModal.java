package com.spoton.pud.data;

public class AirFlightWeightMasterModal {
	
	private String vendorId;
	private String vendorCode;
	private String flightId;
	private String flightDesc;
	private String wtAllowed;
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getFlightId() {
		return flightId;
	}
	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}
	public String getFlightDesc() {
		return flightDesc;
	}
	public void setFlightDesc(String flightDesc) {
		this.flightDesc = flightDesc;
	}
	public String getWtAllowed() {
		return wtAllowed;
	}
	public void setWtAllowed(String wtAllowed) {
		this.wtAllowed = wtAllowed;
	}
	
	

}
