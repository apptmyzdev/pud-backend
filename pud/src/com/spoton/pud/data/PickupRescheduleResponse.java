package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupRescheduleResponse {

	
	@SerializedName("TransationResult")
	private String transactionResult;
	
	@SerializedName("PickUpScheduleID")
	private int pickUpScheduleID;
	
	
	

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public int getPickUpScheduleID() {
		return pickUpScheduleID;
	}

	public void setPickUpScheduleID(int pickUpScheduleID) {
		this.pickUpScheduleID = pickUpScheduleID;
	}

	
	
	
}
