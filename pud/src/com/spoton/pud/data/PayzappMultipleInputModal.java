package com.spoton.pud.data;

public class PayzappMultipleInputModal {

	private String pickupScheduleId;
	private String custName; 
	private String custMobileNo;  
	private String custEmailId ;
	private String pudUserId;
	private String invoiceNumbers;
	public String getPickupScheduleId() {
		return pickupScheduleId;
	}
	public void setPickupScheduleId(String pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustMobileNo() {
		return custMobileNo;
	}
	public void setCustMobileNo(String custMobileNo) {
		this.custMobileNo = custMobileNo;
	}
	public String getCustEmailId() {
		return custEmailId;
	}
	public void setCustEmailId(String custEmailId) {
		this.custEmailId = custEmailId;
	}
	
	public String getPudUserId() {
		return pudUserId;
	}
	public void setPudUserId(String pudUserId) {
		this.pudUserId = pudUserId;
	}
	public String getInvoiceNumbers() {
		return invoiceNumbers;
	}
	public void setInvoiceNumbers(String invoiceNumbers) {
		this.invoiceNumbers = invoiceNumbers;
	}
	
	
	
}
