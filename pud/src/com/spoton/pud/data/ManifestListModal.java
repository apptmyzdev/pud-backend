package com.spoton.pud.data;

public class ManifestListModal {
	
	private String manifestNo;
	private String manifestDate;
	private String conNo;
	private String collectedAmount;
	private String customerName;
	private String collectionType;
	public String getManifestNo() {
		return manifestNo;
	}
	public void setManifestNo(String manifestNo) {
		this.manifestNo = manifestNo;
	}
	public String getManifestDate() {
		return manifestDate;
	}
	public void setManifestDate(String manifestDate) {
		this.manifestDate = manifestDate;
	}
	public String getConNo() {
		return conNo;
	}
	public void setConNo(String conNo) {
		this.conNo = conNo;
	}
	public String getCollectedAmount() {
		return collectedAmount;
	}
	public void setCollectedAmount(String collectedAmount) {
		this.collectedAmount = collectedAmount;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCollectionType() {
		return collectionType;
	}
	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}
	
	
	
	
	

}
