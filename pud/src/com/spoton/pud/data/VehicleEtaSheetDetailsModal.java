package com.spoton.pud.data;

public class VehicleEtaSheetDetailsModal {
	
	private int conCount;
	private int piecesCount;
	private double conWeight;
	public int getConCount() {
		return conCount;
	}
	public void setConCount(int conCount) {
		this.conCount = conCount;
	}
	public int getPiecesCount() {
		return piecesCount;
	}
	public void setPiecesCount(int piecesCount) {
		this.piecesCount = piecesCount;
	}
	public double getConWeight() {
		return conWeight;
	}
	public void setConWeight(double conWeight) {
		this.conWeight = conWeight;
	}
	
	

}
