package com.spoton.pud.data;

public class AirContentMasterModal {
	
	private int dangerousId;
	private String dangerousDescription;
	public int getDangerousId() {
		return dangerousId;
	}
	public void setDangerousId(int dangerousId) {
		this.dangerousId = dangerousId;
	}
	public String getDangerousDescription() {
		return dangerousDescription;
	}
	public void setDangerousDescription(String dangerousDescription) {
		this.dangerousDescription = dangerousDescription;
	}
	
	

}
