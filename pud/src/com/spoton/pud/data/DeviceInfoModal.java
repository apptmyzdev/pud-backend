package com.spoton.pud.data;

import java.util.Date;

public class DeviceInfoModal {

	private int id;
	private int activeFlag;
	private String appVersion;
	private Date createdTimestamp;
	private String deviceAvailableMemory;
	private String deviceBattery;
	private String deviceBrand;
	private String deviceImei;
	private String deviceMac;
	private String deviceMobileNumber;
	private String deviceModel;
	private String deviceName;
	private String deviceOsVersion;
	private String deviceServiceProvider;
	private String deviceTotalMemory;
	private boolean gpsStatus;
	private String pushToken;
	private String simCard;
    private String uniqueKey;
    private String updatedIpAddress;
    private Date updatedTimestamp;
    private String userId;
    
    
    
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public String getDeviceAvailableMemory() {
		return deviceAvailableMemory;
	}
	public void setDeviceAvailableMemory(String deviceAvailableMemory) {
		this.deviceAvailableMemory = deviceAvailableMemory;
	}
	public String getDeviceBattery() {
		return deviceBattery;
	}
	public void setDeviceBattery(String deviceBattery) {
		this.deviceBattery = deviceBattery;
	}
	public String getDeviceBrand() {
		return deviceBrand;
	}
	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}
	public String getDeviceImei() {
		return deviceImei;
	}
	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}
	public String getDeviceMac() {
		return deviceMac;
	}
	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}
	public String getDeviceMobileNumber() {
		return deviceMobileNumber;
	}
	public void setDeviceMobileNumber(String deviceMobileNumber) {
		this.deviceMobileNumber = deviceMobileNumber;
	}
	public String getDeviceModel() {
		return deviceModel;
	}
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getDeviceOsVersion() {
		return deviceOsVersion;
	}
	public void setDeviceOsVersion(String deviceOsVersion) {
		this.deviceOsVersion = deviceOsVersion;
	}
	public String getDeviceServiceProvider() {
		return deviceServiceProvider;
	}
	public void setDeviceServiceProvider(String deviceServiceProvider) {
		this.deviceServiceProvider = deviceServiceProvider;
	}
	public String getDeviceTotalMemory() {
		return deviceTotalMemory;
	}
	public void setDeviceTotalMemory(String deviceTotalMemory) {
		this.deviceTotalMemory = deviceTotalMemory;
	}
	public boolean getGpsStatus() {
		return gpsStatus;
	}
	public void setGpsStatus(boolean gpsStatus) {
		this.gpsStatus = gpsStatus;
	}
	public String getPushToken() {
		return pushToken;
	}
	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}
	public String getSimCard() {
		return simCard;
	}
	public void setSimCard(String simCard) {
		this.simCard = simCard;
	}
	public String getUniqueKey() {
		return uniqueKey;
	}
	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}
	public String getUpdatedIpAddress() {
		return updatedIpAddress;
	}
	public void setUpdatedIpAddress(String updatedIpAddress) {
		this.updatedIpAddress = updatedIpAddress;
	}
	public Date getUpdatedTimestamp() {
		return updatedTimestamp;
	}
	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	
}
