package com.spoton.pud.data;

import com.google.gson.annotations.SerializedName;

public class PickupRegistrationData {
	@SerializedName("CFT")
	private float cft;

	@SerializedName("ContactPersonName")
	private String contactPersonName;

	@SerializedName("ContactPersonNumber")
	private String contactPersonNumber;

	@SerializedName("CustomerCode")
	private String customerCode;

	@SerializedName("CustomerName")
	private String customerName;

	@SerializedName("GPS_LAT")
	private double gpsLat;

	@SerializedName("GPS_LON")
	private double gpsLon;

	@SerializedName("MobileUserID")
	private String mobileUserId;

	@SerializedName("SenderMailId")
	private String senderMailId;

	@SerializedName("PickupAddress")
	private String pickupAddress;

	@SerializedName("PickupDate")
	private String pickupDate;

	@SerializedName("PickUpOrderNO")
	private String pickupOrderNo;

	@SerializedName("PickupPincode")
	private int pickupPincode;

	@SerializedName("PickupScheduleID")
	private int pickupScheduleId;

	@SerializedName("Pieces")
	private int pieces;

	@SerializedName("TransactionRemarks")
	private String transactionRemarks;

	@SerializedName("TransactionResult")
	private String transactionResult;

	@SerializedName("ProductType")
	private String productType;

	@SerializedName("Weight")
	private double weight;

	/* 18-09-2020 - ak */
	@SerializedName("IsConNoteEnabled")
	private boolean conNoteEnabled;

	/* 18-09-2020 - ak */
	@SerializedName("IsOneOrderOneCon")
	private boolean oneOrderOneCon;

	public float getCft() {
		return cft;
	}

	public void setCft(float cft) {
		this.cft = cft;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonNumber() {
		return contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getGpsLat() {
		return gpsLat;
	}

	public void setGpsLat(double gpsLat) {
		this.gpsLat = gpsLat;
	}

	public double getGpsLon() {
		return gpsLon;
	}

	public void setGpsLon(double gpsLon) {
		this.gpsLon = gpsLon;
	}

	public String getMobileUserId() {
		return mobileUserId;
	}

	public void setMobileUserId(String mobileUserId) {
		this.mobileUserId = mobileUserId;
	}

	public String getPickupAddress() {
		return pickupAddress;
	}

	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public String getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupOrderNo() {
		return pickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}

	public int getPickupPincode() {
		return pickupPincode;
	}

	public void setPickupPincode(int pickupPincode) {
		this.pickupPincode = pickupPincode;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public int getPieces() {
		return pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	public String getTransactionRemarks() {
		return transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getSenderMailId() {
		return senderMailId;
	}

	public void setSenderMailId(String senderMailId) {
		this.senderMailId = senderMailId;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	/** added on Sep 18, 2020 **/
	public boolean isConNoteEnabled() {
		return conNoteEnabled;
	}

	/** added on Sep 18, 2020 **/
	public void setConNoteEnabled(boolean conNoteEnabled) {
		this.conNoteEnabled = conNoteEnabled;
	}

	/** added on Sep 18, 2020 **/
	public boolean isOneOrderOneCon() {
		return oneOrderOneCon;
	}

	/** added on Sep 18, 2020 **/
	public void setOneOrderOneCon(boolean oneOrderOneCon) {
		this.oneOrderOneCon = oneOrderOneCon;
	}

}
