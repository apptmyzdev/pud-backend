package com.spoton.pud.data;

import java.util.List;

public class CashBookingInputModal {

	private String customerName;
	private String customerAddress;
	private String customerCity;
	private String customerEmail;
	private String customerPhone;
	private String customerGSTIN;
	private String receiverName;
	private String receiverAddress;
	private String receiverCity;
	private String receiverEmail;
	private String receiverPhone;
	private String receiverGSTIN;
	private String rateModifyEnabled;
	private List<ConDetailsModalV3> conDetails;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public List<ConDetailsModalV3> getConDetails() {
		return conDetails;
	}

	public void setConDetails(List<ConDetailsModalV3> conDetails) {
		this.conDetails = conDetails;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerGSTIN() {
		return customerGSTIN;
	}

	public void setCustomerGSTIN(String customerGSTIN) {
		this.customerGSTIN = customerGSTIN;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getReceiverPhone() {
		return receiverPhone;
	}

	public void setReceiverPhone(String receiverPhone) {
		this.receiverPhone = receiverPhone;
	}

	public String getReceiverGSTIN() {
		return receiverGSTIN;
	}

	public void setReceiverGSTIN(String receiverGSTIN) {
		this.receiverGSTIN = receiverGSTIN;
	}

	public String getRateModifyEnabled() {
		return rateModifyEnabled;
	}

	public void setRateModifyEnabled(String rateModifyEnabled) {
		this.rateModifyEnabled = rateModifyEnabled;
	}

}
