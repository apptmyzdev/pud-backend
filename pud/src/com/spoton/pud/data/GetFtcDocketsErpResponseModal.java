package com.spoton.pud.data;

public class GetFtcDocketsErpResponseModal {
	
	private String ConNo;
	private String InvoiceAmount;
	private String CustomerName;
	private String ShipmentType;
	public String getConNo() {
		return ConNo;
	}
	public void setConNo(String conNo) {
		ConNo = conNo;
	}
	public String getInvoiceAmount() {
		return InvoiceAmount;
	}
	public void setInvoiceAmount(String invoiceAmount) {
		InvoiceAmount = invoiceAmount;
	}
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getShipmentType() {
		return ShipmentType;
	}
	public void setShipmentType(String shipmentType) {
		ShipmentType = shipmentType;
	}
	
	

}
