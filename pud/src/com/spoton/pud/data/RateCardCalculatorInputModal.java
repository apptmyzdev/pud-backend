package com.spoton.pud.data;

public class RateCardCalculatorInputModal {

	private String originCode;
	private String destinationCode;
	private String customerCode;
	private String paymentBasis;
	private String actualWeight;
	private String volume;
	private String declaredValue;
	private String applyVtc;
	private String riskType;
	private String dacc;
	private String nForm;
	private String ftc;
	private String dc;
	private String userName;
	private String productType;
	public String getOriginCode() {
		return originCode;
	}
	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}
	public String getDestinationCode() {
		return destinationCode;
	}
	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getPaymentBasis() {
		return paymentBasis;
	}
	public void setPaymentBasis(String paymentBasis) {
		this.paymentBasis = paymentBasis;
	}
	public String getActualWeight() {
		return actualWeight;
	}
	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getDeclaredValue() {
		return declaredValue;
	}
	public void setDeclaredValue(String declaredValue) {
		this.declaredValue = declaredValue;
	}
	public String getApplyVtc() {
		return applyVtc;
	}
	public void setApplyVtc(String applyVtc) {
		this.applyVtc = applyVtc;
	}
	public String getRiskType() {
		return riskType;
	}
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	public String getDacc() {
		return dacc;
	}
	public void setDacc(String dacc) {
		this.dacc = dacc;
	}
	public String getnForm() {
		return nForm;
	}
	public void setnForm(String nForm) {
		this.nForm = nForm;
	}
	public String getFtc() {
		return ftc;
	}
	public void setFtc(String ftc) {
		this.ftc = ftc;
	}
	public String getDc() {
		return dc;
	}
	public void setDc(String dc) {
		this.dc = dc;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	
	
}
