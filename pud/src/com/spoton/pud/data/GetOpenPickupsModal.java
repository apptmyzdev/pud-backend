package com.spoton.pud.data;

public class GetOpenPickupsModal {

	private int pickupScheduleId;
	private String pickupOrderNumber;
	private String pickupDate;
	private String userName;
	private String currentApkVersion;
	
	
	public int getPickupScheduleId() {
		return pickupScheduleId;
	}
	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}
	public String getPickupOrderNumber() {
		return pickupOrderNumber;
	}
	public void setPickupOrderNumber(String pickupOrderNumber) {
		this.pickupOrderNumber = pickupOrderNumber;
	}
	public String getPickupDate() {
		return pickupDate;
	}
	public void setPickupDate(String pickupDate) {
		this.pickupDate = pickupDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCurrentApkVersion() {
		return currentApkVersion;
	}
	public void setCurrentApkVersion(String currentApkVersion) {
		this.currentApkVersion = currentApkVersion;
	}
	
	
}
