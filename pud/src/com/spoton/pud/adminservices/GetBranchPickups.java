package com.spoton.pud.adminservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.PickupsDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class GetBranchPickups
 */
@WebServlet("/getBranchPickups")
public class GetBranchPickups extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBranchPickups() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction = null;
		String reqDate = request.getParameter("reqDate");
		String branchCode = request.getParameter("branchCode");
		String userName = "";
		String fdatestring = null;
		String tdatestring = null;
		Date reqFromDate = null;
		Date reqToDate = null;
		
		if (request.getParameter("userName") != null) {
			userName = request.getParameter("userName");
		}

		if (CommonTasks.check(reqDate,branchCode)) {
			fdatestring = reqDate.concat(" 00:00:00");
			tdatestring = reqDate.concat(" 23:59:59");
			try {

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				reqFromDate = format.parse(fdatestring);
				reqToDate = format.parse(tdatestring);
				manageTransaction = new ManageTransaction();
				String pudQuery = "";
				String regQuery = "";
				String successQuery = "";
				String attemptQuery = "";
				String consQuery = "";
				String piecesQuery = "";
				List<PickupsDataModel> responseData = new ArrayList<PickupsDataModel>();
				if(CommonTasks.check(branchCode, userName))
				{
					/*pudQuery = "select A.pickup_schedule_id, A.customer_code, A.customer_name, A.pickup_address, A.user_id, B.location "
							+"from pud_data A, user_data_master B where A.user_id = B.app_user_name and A.pickup_date >= ?1 "
							+"and A.pickup_date <= ?2 and A.user_id = ?3 and B.location = ?4";*/ //removing joins
					
					pudQuery = "select A.pickup_schedule_id, A.customer_code, A.customer_name, A.pickup_address, A.user_id, SUBSTRING(A.user_id  from 1 for 4) "
							+"from pud_data A where A.pickup_date >= ?1 "
							+"and A.pickup_date <= ?2 and A.user_id = ?3 and SUBSTRING(A.user_id  from 1 for 4) = ?4";
					
					
					/*regQuery = "select A.pickup_schedule_id, A.Customer_code, A.customer_name, A.pickup_address, A.mobile_user_id, B.location"
							+" from pickup_registration A, user_data_master B where A.mobile_user_id = B.app_user_name" 
							+" and A.pickup_date >= ?1 and A.pickup_date <= ?2 and A.mobile_user_id = ?3 and B.location = ?4";*/ //removing joins
					
					regQuery = "select A.pickup_schedule_id, A.Customer_code, A.customer_name, A.pickup_address, A.mobile_user_id, SUBSTRING(A.mobile_user_id  from 1 for 4)"
							+" from pickup_registration A where " 
							+" A.pickup_date >= ?1 and A.pickup_date <= ?2 and A.mobile_user_id = ?3 and SUBSTRING(A.mobile_user_id  from 1 for 4) = ?4";
					
					
					/*successQuery = "select B.pickup_schedule_id from con_details B , pickup_updation_status C, user_data_master D "
							+"where C.con_entry_id = B.con_entry_id and B.user_name = D.app_user_name and B.pickup_date  >= ?1 and B.pickup_date <= ?2 "
							+"and B.user_name = ?3 and D.location = ?4 and C.transaction_result = 'Success'";*/
					
					successQuery = "select B.pickup_schedule_id from con_details B "
							+"where B.pickup_date  >= ?1 and B.pickup_date <= ?2 "
							+"and B.user_name = ?3 and SUBSTRING(B.user_name  from 1 for 4) = ?4 and B.transaction_result = 'Success'";
					
					
					/*attemptQuery = "select B.pickup_schedule_id from pickup_reschedule B, pickup_reschedule_update_status C, user_data_master D"
							+" where B.pickup_schedule_id = C.pickup_schedule_id and B.app_mobile_user_name = D.app_user_name and B.created_timestamp >= ?1"
							+" and B.created_timestamp <= ?2 and B.app_mobile_user_name = ?3 and D.location = ?4 and C.transaction_result = 'true'";*/ //removing joins
					
					attemptQuery = "select B.pickup_schedule_id from pickup_reschedule B "
							+" where B.created_timestamp >= ?1"
							+" and B.created_timestamp <= ?2 and B.app_mobile_user_name = ?3 and SUBSTRING(B.app_mobile_user_name from 1 for 4) = ?4 and B.transaction_result = 'true'";
					
					/*consQuery = "select pickup_schedule_id,count(distinct(con_number)) from con_details A, user_data_master B "
							+ "where A.pickup_date >= ?1 and A.pickup_date <= ?2 and A.user_name = ?3 and B.location = ?4 " //removing joins
							+ "group by A.pickup_schedule_id";*/
					
					consQuery = "select pickup_schedule_id,count(distinct(con_number)) from con_details A "
							+ "where A.pickup_date >= ?1 and A.pickup_date <= ?2 and A.user_name = ?3 and SUBSTRING(A.user_name from 1 for 4) = ?4 " 
							+ "group by A.pickup_schedule_id";
					
					
					/*piecesQuery = "select B.pickup_schedule_id, sum(C.total_pieces), D.location "
							+"from con_details B, piece_entry C, user_data_master D "
							+"where B.con_entry_id = C.con_id and B.user_name = D.app_user_name "
							+"and B.pickup_date >= ?1 and B.pickup_date <= ?2 and B.user_name = ?3 and D.location = ?4 group by B.pickup_schedule_id";*/ //removing joins
					
					piecesQuery = "select C.pickup_schedule_id, sum(C.total_pieces), SUBSTRING(C.user_name from 1 for 4) "
							+"from  piece_entry C "
							+"where  C.pickup_date >= ?1 and C.pickup_date <= ?2 and C.user_name = ?3 and SUBSTRING(C.user_name from 1 for 4) = ?4 group by C.pickup_schedule_id";
				}
				
				else
					
					{
						/*pudQuery = "select A.pickup_schedule_id, A.customer_code, A.customer_name, A.pickup_address, A.user_id, B.location "
									+"from pud_data A, user_data_master B where A.user_id = B.app_user_name and A.pickup_date >= ?1 "
									+"and A.pickup_date <= ?2 and B.location = ?3";*/ //removing joins
						
						pudQuery = "select A.pickup_schedule_id, A.customer_code, A.customer_name, A.pickup_address, A.user_id, SUBSTRING(A.user_id  from 1 for 4) "
								+"from pud_data A where A.pickup_date >= ?1 "
								+"and A.pickup_date <= ?2 and SUBSTRING(A.user_id  from 1 for 4) = ?3";
						
						/*regQuery = "select A.pickup_schedule_id, A.Customer_code, A.customer_name, A.pickup_address, A.mobile_user_id, B.location"
									+" from pickup_registration A, user_data_master B where A.mobile_user_id = B.app_user_name" 
									+" and A.pickup_date >= ?1 and A.pickup_date <= ?2 and B.location = ?3";*/ //removing joins
						
						regQuery = "select A.pickup_schedule_id, A.Customer_code, A.customer_name, A.pickup_address, A.mobile_user_id,SUBSTRING(A.mobile_user_id from 1 for 4)"
								+" from pickup_registration A where " 
								+" A.pickup_date >= ?1 and A.pickup_date <= ?2 and SUBSTRING(A.mobile_user_id  from 1 for 4) = ?3";
						
						/*successQuery = "select B.pickup_schedule_id from con_details B , pickup_updation_status C, user_data_master D "
								+"where C.con_entry_id = B.con_entry_id and B.user_name = D.app_user_name and B.pickup_date  >= ?1 and B.pickup_date <= ?2 "
								+"and D.location = ?3 and C.transaction_result = 'Success'"; *///removing joins
						
						successQuery = "select B.pickup_schedule_id from con_details B "
								+"where  B.pickup_date  >= ?1 and B.pickup_date <= ?2 "
								+"and SUBSTRING(B.user_name  from 1 for 4) = ?3 and B.transaction_result = 'Success'"; 
						
						/*attemptQuery = "select B.pickup_schedule_id from pickup_reschedule B, pickup_reschedule_update_status C, user_data_master D"
								+" where B.pickup_schedule_id = C.pickup_schedule_id and B.app_mobile_user_name = D.app_user_name and B.created_timestamp >= ?1"
								+" and B.created_timestamp <= ?2 and D.location = ?3 and C.transaction_result = 'true'";*/ //removing joins
						
						attemptQuery = "select B.pickup_schedule_id from pickup_reschedule B "
								+" where B.created_timestamp >= ?1"
								+" and B.created_timestamp <= ?2 and SUBSTRING(B.app_mobile_user_name from 1 for 4) = ?3 and B.transaction_result = 'true'";
						
						/*consQuery = "select pickup_schedule_id,count(distinct(con_number)) from con_details A, user_data_master B "
								+ "where A.pickup_date >= ?1 and A.pickup_date <= ?2 and B.location = ?3 "
								+ "group by A.pickup_schedule_id";*/ //removing joins
						consQuery = "select pickup_schedule_id,count(distinct(con_number)) from con_details A "
								+ "where A.pickup_date >= ?1 and A.pickup_date <= ?2 and SUBSTRING(A.user_name from 1 for 4) = ?3 "
								+ "group by A.pickup_schedule_id";
						
						/*piecesQuery = "select B.pickup_schedule_id, sum(C.total_pieces), D.location "
								+"from con_details B, piece_entry C, user_data_master D "
								+"where B.con_entry_id = C.con_id and B.user_name = D.app_user_name "
								+"and B.pickup_date >= ?1 and B.pickup_date <= ?2 and D.location = ?3 group by B.pickup_schedule_id";*/ //removing joins
						
						piecesQuery = "select C.pickup_schedule_id, sum(C.total_pieces), SUBSTRING(C.user_name from 1 for 4) "
								+"from  piece_entry C "
								+"where  C.pickup_date >= ?1 and C.pickup_date <= ?2 and  SUBSTRING(C.user_name from 1 for 4) = ?3 group by C.pickup_schedule_id";
					}
				
				Query nativePudQuery = manageTransaction.createNativeQuery(pudQuery);
				Query nativeRegQuery = manageTransaction.createNativeQuery(regQuery);
				Query nativeSQuery = manageTransaction.createNativeQuery(successQuery);
				Query nativeAQuery = manageTransaction.createNativeQuery(attemptQuery);
				Query nativeConQuery = manageTransaction.createNativeQuery(consQuery);
				Query nativePiece = manageTransaction.createNativeQuery(piecesQuery);
				
				if(CommonTasks.check(branchCode, userName))
				{
					nativePudQuery.setParameter(1, reqFromDate);
					nativePudQuery.setParameter(2, reqToDate);
					nativePudQuery.setParameter(3, userName);
					nativePudQuery.setParameter(4, branchCode);
					
					nativeRegQuery.setParameter(1, reqFromDate);
					nativeRegQuery.setParameter(2, reqToDate);
					nativeRegQuery.setParameter(3, userName);
					nativeRegQuery.setParameter(4, branchCode);
					
					nativeSQuery.setParameter(1, reqFromDate);
					nativeSQuery.setParameter(2, reqToDate);
					nativeSQuery.setParameter(3, userName);
					nativeSQuery.setParameter(4, branchCode);
					
					nativeAQuery.setParameter(1, reqFromDate);
					nativeAQuery.setParameter(2, reqToDate);
					nativeAQuery.setParameter(3, userName);
					nativeAQuery.setParameter(4, branchCode);
					
					nativeConQuery.setParameter(1, reqFromDate);
					nativeConQuery.setParameter(2, reqToDate);
					nativeConQuery.setParameter(3, userName);
					nativeConQuery.setParameter(4, branchCode);
					
					nativePiece.setParameter(1, reqFromDate);
					nativePiece.setParameter(2, reqToDate);
					nativePiece.setParameter(3, userName);
					nativePiece.setParameter(4, branchCode);
					
				}
			
				else
				{
					nativePudQuery.setParameter(1, reqFromDate);
					nativePudQuery.setParameter(2, reqToDate);
					nativePudQuery.setParameter(3, branchCode);
					
					nativeRegQuery.setParameter(1, reqFromDate);
					nativeRegQuery.setParameter(2, reqToDate);
					nativeRegQuery.setParameter(3, branchCode);
					
					nativeSQuery.setParameter(1, reqFromDate);
					nativeSQuery.setParameter(2, reqToDate);
					nativeSQuery.setParameter(3, branchCode);
					
					nativeAQuery.setParameter(1, reqFromDate);
					nativeAQuery.setParameter(2, reqToDate);
					nativeAQuery.setParameter(3, branchCode);
					
					nativeConQuery.setParameter(1, reqFromDate);
					nativeConQuery.setParameter(2, reqToDate);
					nativeConQuery.setParameter(3, branchCode);
					
					nativePiece.setParameter(1, reqFromDate);
					nativePiece.setParameter(2, reqToDate);
					nativePiece.setParameter(3, branchCode);
					
				}

				List<Object []> pudPickups = nativePudQuery.getResultList();
				List<Object []> regPickups = nativeRegQuery.getResultList();
				
				
				
				if(pudPickups != null && pudPickups.size() > 0 || regPickups != null && regPickups.size() > 0)
				{
					List<Object []> s_pickupsList = nativeSQuery.getResultList();
					List<Object []> a_pickupsList = nativeAQuery.getResultList();
					List<Object []> consCountList = nativeConQuery.getResultList();
					List<Object []> pieceCountList = nativePiece.getResultList();
 					Map <Integer, Long> consCountMap = new HashMap<Integer, Long>();
 					Map <Integer, Double> pieceCountMap = new HashMap<Integer, Double>();
 					
 					for(Object [] pieceObj : pieceCountList)
 					{
 						pieceCountMap.put((Integer) pieceObj[0], (double) pieceObj[1]);
 					}
 					
					for(Object [] conObj : consCountList)
					{
						consCountMap.put((Integer) conObj[0], (long) conObj[1]);
					}
					
					
					Map <Integer, String> pickupsMap = new HashMap<Integer, String>();
					
					for(Object [] obj : pudPickups)
					{
						pickupsMap.put((Integer) obj[0], "pickup pending");
					}
					for(Object [] obj : regPickups)
					{
						pickupsMap.put((Integer) obj[0], "Pickup pending");
					}
					for(Object obj : s_pickupsList)
					{
					       pickupsMap.put((Integer) obj, "Pickup Success");
					}
					
					for(Object aobj : a_pickupsList)
					{
						if(pickupsMap.get((Integer) aobj) != "Pickup Success")
						{
							pickupsMap.put((Integer) aobj, "Pickup Attempted");
						}
						
					}
					
					PickupsDataModel model = null;
						for(Object [] obj : pudPickups)
						{
							model = new PickupsDataModel();
							model.setPickUpScheduleId((Integer) obj[0]);
							model.setCustomerCode((String) obj[1]);
							model.setCustomerName((String) obj[2]);
							model.setPickupAddress((String) obj[3]);
							model.setUserId((String) obj[4]);
							model.setStatus(pickupsMap.get((Integer) obj[0]));
							model.setConCount(consCountMap.containsKey((Integer) obj[0]) == false ? 0 : consCountMap.get((Integer) obj[0]));
							model.setPieceCount(pieceCountMap.containsKey((Integer) obj[0]) == false ? 0 : pieceCountMap.get((Integer) obj[0]));
							responseData.add(model);
						} 
						
						for(Object [] obj : regPickups)
						{
							model = new PickupsDataModel();
							model.setPickUpScheduleId((Integer) obj[0]);
							model.setCustomerCode((String) obj[1]);
							model.setCustomerName((String) obj[2]);
							model.setPickupAddress((String) obj[3]);
							model.setUserId((String) obj[4]);
							model.setStatus(pickupsMap.get((Integer) obj[0]));
							model.setConCount(consCountMap.containsKey((Integer) obj[0]) == false ? 0 : consCountMap.get((Integer) obj[0]));
							model.setPieceCount(pieceCountMap.containsKey((Integer) obj[0]) == false ? 0 : pieceCountMap.get((Integer) obj[0]));
							responseData.add(model);
						} 
						
						result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,responseData));
				}
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
				}
				
			}

			catch (Exception e) {
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}
			
			finally{
				manageTransaction.close();
			}
		} 
		else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		response.getWriter().write(result);
	
	}

}
