package com.spoton.pud.adminservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.DeliveriesDataModel;
import com.spoton.pud.adminmodels.PickupsDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class GetDeliveriesDataReport
 */
@WebServlet("/getDeliveriesDataReport")
public class GetDeliveriesDataReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDeliveriesDataReport() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction = null;
		String reqDate = request.getParameter("reqDate");
		String userName = "";
		String fdatestring = null;
		String tdatestring = null;
		Date reqFromDate = null;
		Date reqToDate = null;
		Map<String, DeliveriesDataModel> responseList = new HashMap<String, DeliveriesDataModel>();
		
		
		if (request.getParameter("agent") != null) {
			userName = request.getParameter("agent");
		}

		if (CommonTasks.check(reqDate)) {
			fdatestring = reqDate.concat(" 00:00:00");
			tdatestring = reqDate.concat(" 23:59:59");
			try {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				reqFromDate = format.parse(fdatestring);
				reqToDate = format.parse(tdatestring);
				
				manageTransaction = new ManageTransaction();
				String delQuery = "";
				String statQuery = "";
				if(CommonTasks.check(userName))
				{
					/*delQuery = "select B.pdc, A.consignee_name, A.created_date, A.consignee_address, A.consignee_city, A.consignee_pincode,"
					+" A.consignor_city, A.con, B.agent_id, A.pcs_count from agent_pdc B, delivery_con_details A where B.pdc = A.pdc"
					+" AND B.created_timestamp >= ?1 and B.created_timestamp <= ?2 and B.agent_id = ?3 group by B.agent_id, B.pdc, A.con";*/ //removing joins
	
		         delQuery = "select A.pdc, A.consignee_name, A.created_date, A.consignee_address, A.consignee_city, A.consignee_pincode,"
				+" A.consignor_city, A.con, A.agent_id, A.pcs_count from delivery_con_details A where "
				+"A.created_date >= ?1 and A.created_date <= ?2 and A.agent_id = ?3 group by A.agent_id, A.pdc, A.con";
	        }
				else{
					/*delQuery = "select B.pdc, A.consignee_name, A.created_date, A.consignee_address, A.consignee_city, A.consignee_pincode,"
					+" A.consignor_city, A.con, B.agent_id, A.pcs_count from agent_pdc B, delivery_con_details A where B.pdc = A.pdc"
					+" AND B.created_timestamp >= ?1 and B.created_timestamp <= ?2 group by B.agent_id, B.pdc, A.con";*/ //removing joins
		        delQuery = "select A.pdc, A.consignee_name, A.created_date, A.consignee_address, A.consignee_city, A.consignee_pincode,"
				+" A.consignor_city, A.con, A.agent_id, A.pcs_count from  delivery_con_details A where "
				+" A.created_date >= ?1 and A.created_date <= ?2 group by A.agent_id, A.pdc, A.con";
				}
				
				Query nativeQuery = manageTransaction.createNativeQuery(delQuery);
				
				if(CommonTasks.check(userName))
				{
					nativeQuery.setParameter(1, reqFromDate);
					nativeQuery.setParameter(2, reqToDate);
					nativeQuery.setParameter(3, userName);
				}
				else{
					nativeQuery.setParameter(1, reqFromDate);
					nativeQuery.setParameter(2, reqToDate);
				}
				statQuery = "select pdc_number, status, reason, awb_no from delivery_updation";
				Query nQuery = manageTransaction.createNativeQuery(statQuery);
				List<Object []> statList = nQuery.getResultList();
				Map<String,String> updatedMap = new HashMap<String, String>();
				for(Object [] o : statList)
				{
					updatedMap.put((String) o[0]+"-"+(String) o[3], (String) o[1]+"-"+(String) o[2]);
				}
				List<Object []> pdcList = nativeQuery.getResultList();
				DeliveriesDataModel dataModel = null;
				if(pdcList != null && pdcList.size() > 0)
				{
					 for(Object [] obj : pdcList)
					 {
						 dataModel = new DeliveriesDataModel();
						 dataModel.setPdc((String) obj[0]);
						 dataModel.setConNo(obj[7] != null ? (Integer) obj[7] : 0);
						 dataModel.setConsigneeName((String) obj[1]);
						 dataModel.setCreatedTime((Date) obj[2]);
						 dataModel.setAssignedTo((String) obj[8]);
						 dataModel.setConsigneeCode((String) obj[5]);
						 dataModel.setConsigneeAddress((String) obj[3]);
						 dataModel.setConsignorAddress((String) obj[6]);
//						 dataModel.setPieceCount((Integer) obj[9]);
						 if(updatedMap.containsKey((String) obj[0]+"-"+(Integer)obj[7]))
								 {
							 		dataModel.setStatus(updatedMap.get((String) obj[0]+"-"+(Integer)obj[7]));
								 }
						 else{
							 dataModel.setStatus("Pending");
						 }
						 responseList.put((String) obj[0]+"-"+(Integer)obj[7], dataModel);
					 } 
					 
					 result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,responseList.values()));
				}
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}
			finally{
				manageTransaction.close();
			}
		}
		else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		
		response.getWriter().write(result);
	}

}
