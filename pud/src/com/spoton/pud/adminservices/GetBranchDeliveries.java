package com.spoton.pud.adminservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.DeliveriesDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class GetBranchDeliveries
 */
@WebServlet("/getBranchDeliveries")
public class GetBranchDeliveries extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBranchDeliveries() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction = null;
		String reqDate = request.getParameter("reqDate");
		String branchCode = request.getParameter("branchCode");
		String userName = null;
		String fdatestring = null;
		String tdatestring = null;
		Date reqFromDate = null;
		Date reqToDate = null;
		DeliveriesDataModel dataModel = null;
		List<DeliveriesDataModel> responseList = new ArrayList<DeliveriesDataModel>();
		
		
		if (request.getParameter("agent") != null) {
			userName = request.getParameter("agent");
		}

		if (CommonTasks.check(reqDate,branchCode)) {
			fdatestring = reqDate.concat(" 00:00:00");
			tdatestring = reqDate.concat(" 23:59:59");
			try {
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				reqFromDate = format.parse(fdatestring);
				reqToDate = format.parse(tdatestring);
				
				String totalConsQuery = "";
				String saQuery = "";
				manageTransaction = new ManageTransaction();
				
				
				if(CommonTasks.check(userName, branchCode))
				{
					/*totalConsQuery = "select distinct(concat(B.con, B.pdc)),B.con,B.pdc, B.created_date, B.consignee_name, "
					+"B.consignee_address, B.consignee_city, B.consignee_pincode, B.consignor_city, A.agent_id "
					+ "from delivery_con_details B, agent_pdc A, user_data_master C "
					+ "where A.pdc = B.pdc and A.agent_id = C.app_user_name and A.created_timestamp >= ?1 and A.created_timestamp <= ?2 "
					+"and B.created_date >= ?3 and B.created_date <= ?4 and C.location = ?5 and A.agent_id = ?6";*/ //removing joins
			
			totalConsQuery = "select distinct(concat(B.con, B.pdc)),B.con,B.pdc, B.created_date, B.consignee_name, "
					+"B.consignee_address, B.consignee_city, B.consignee_pincode, B.consignor_city, B.agent_id "
					+ "from delivery_con_details B "
					+ "where B.created_date >= ?3 and B.created_date <= ?4 and SUBSTRING(B.agent_id  from 1 for 4)=?5 and B.agent_id = ?6";
			
			
			/*saQuery = "select A.con, B.status "
					+"from delivery_con_details A, delivery_updation B, delivery_updation_status C, user_data_master D "
					+"where B.field_employee_name = D.app_user_name and  A.con = B.awb_no and B.transaction_id = C.transaction_id "
					+"and C.transaction_result = 'true' and B.transaction_date >= ?1 and B.transaction_date <= ?2 and B.field_employee_name = ?3 and D.location = ?4";*/ //removing joins
			saQuery = "select B.awb_no, B.status "
					+"from  delivery_updation B "
					+"where  B.transaction_result = 'true' and B.transaction_date >= ?1 and B.transaction_date <= ?2 and B.field_employee_name = ?3 "
					+ "and SUBSTRING(B.field_employee_name  from 1 for 4) = ?4";
		}
		else{
		/*	totalConsQuery = "select distinct(concat(B.con, B.pdc)),B.con,B.pdc, B.created_date, B.consignee_name, "
					+"B.consignee_address, B.consignee_city, B.consignee_pincode, B.consignor_city, A.agent_id "
					+ "from delivery_con_details B, agent_pdc A, user_data_master C "
					+ "where A.pdc = B.pdc and A.agent_id = C.app_user_name and A.created_timestamp >= ?1 and A.created_timestamp <= ?2 "
					+"and B.created_date >= ?3 and B.created_date <= ?4 and C.location = ?5";*/ //removing joins
			
			totalConsQuery = "select distinct(concat(B.con, B.pdc)),B.con,B.pdc, B.created_date, B.consignee_name, "
					+"B.consignee_address, B.consignee_city, B.consignee_pincode, B.consignor_city, B.agent_id "
					+ "from delivery_con_details B "
					+ "where  B.created_date >= ?3 and B.created_date <= ?4 and SUBSTRING(B.agent_id  from 1 for 4)=?5";

		
		/*saQuery = "select A.con, B.status "
				+"from delivery_con_details A, delivery_updation B, delivery_updation_status C, user_data_master D "
				+"where B.field_employee_name = D.app_user_name and  A.con = B.awb_no and B.transaction_id = C.transaction_id "
				+"and C.transaction_result = 'true' and B.transaction_date >= ?1 and B.transaction_date <= ?2 and D.location = ?3";*/ //removing joins
			
			saQuery = "select B.awb_no, B.status "
					+"from  delivery_updation B "
					+"where B.transaction_result = 'true' and B.transaction_date >= ?1 and B.transaction_date <= ?2 "
					+ "and SUBSTRING(B.field_employee_name  from 1 for 4) = ?3";
		}
				Query nativeTotQuery = manageTransaction.createNativeQuery(totalConsQuery);
				Query nativeSAQuery = manageTransaction.createNativeQuery(saQuery);
				
				if(CommonTasks.check(userName, branchCode))
				{
					nativeTotQuery.setParameter(1, reqFromDate);
					nativeTotQuery.setParameter(2, reqToDate);
					nativeTotQuery.setParameter(3, reqFromDate);
					nativeTotQuery.setParameter(4, reqToDate);
					nativeTotQuery.setParameter(5, branchCode);
					nativeTotQuery.setParameter(6, userName);
					
					nativeSAQuery.setParameter(1, reqFromDate);
					nativeSAQuery.setParameter(2, reqToDate);
					nativeSAQuery.setParameter(3, userName);
					nativeSAQuery.setParameter(4, branchCode);
				}
				else{
					nativeTotQuery.setParameter(1, reqFromDate);
					nativeTotQuery.setParameter(2, reqToDate);
					nativeTotQuery.setParameter(3, reqFromDate);
					nativeTotQuery.setParameter(4, reqToDate);
					nativeTotQuery.setParameter(5, branchCode);
				
					nativeSAQuery.setParameter(1, reqFromDate);
					nativeSAQuery.setParameter(2, reqToDate);
					nativeSAQuery.setParameter(3, branchCode);
				}
				
				List<Object[]> totalConsList = nativeTotQuery.getResultList();
				List<Object[]> saConsList = nativeSAQuery.getResultList();
				
				if(totalConsList != null && totalConsList.size() > 0)
				{
					Map<String, String> saConsMap = new HashMap<String, String>();
					for(Object [] obj : saConsList)
					{
						saConsMap.put((String) obj[0], (String) obj[1]);
					}
					
					for(Object [] obj : totalConsList)
					{
						dataModel = new DeliveriesDataModel();
						dataModel.setConNo((Integer) obj[1]);
						dataModel.setPdc((String) obj[2]);
						dataModel.setCreatedTime((Date) obj[3]);
						dataModel.setConsigneeName((String) obj[4]);
						dataModel.setConsigneeAddress((String) obj[5]+","+(String) obj[6]);
						dataModel.setConsigneeCode((String) obj[7]);
						dataModel.setConsignorAddress((String) obj[8]);
						dataModel.setAssignedTo((String) obj[9]);
						
						if(saConsMap.containsKey((Integer) obj[1]) == false)
						{
							dataModel.setStatus("Delivery Pending");
						}
						else{
							dataModel.setStatus(saConsMap.get((Integer) obj[1]));
						}
						
						responseList.add(dataModel);
					}
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,responseList));
				}
				
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}
			finally{
				manageTransaction.close();
			}
		}
		else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		
		response.getWriter().write(result);
	
	}

}
