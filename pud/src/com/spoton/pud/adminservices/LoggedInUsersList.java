package com.spoton.pud.adminservices;

import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.CurrentLoginsModel;
import com.spoton.pud.adminmodels.HourlyPickupsBarModel;
import com.spoton.pud.adminmodels.OnLoadDataModel;
import com.spoton.pud.adminmodels.UAuditsDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class LoggedInUsersList
 */
@WebServlet("/loggedInUsers")
public class LoggedInUsersList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoggedInUsersList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String result = "";
		ManageTransaction manageTransaction = null;
		try {
			manageTransaction = new ManageTransaction();
			Map<String, CurrentLoginsModel> loginsMap = new HashMap<String, CurrentLoginsModel>();
			/*	String query = "select A.user_name, A.created_timestamp, A.imei, B.location, A.apk_version from user_session_data A, user_data_master B "
			+ "where A.user_name = B.app_user_name and active_flag = 1";*/ //removing joins
	
	        String query = "select A.user_name, A.created_timestamp, A.imei, SUBSTRING(A.user_name  from 1 for 4), A.apk_version from user_session_data A "
			+ "where A.active_flag = 1";
			Query nativeQuery = manageTransaction.createNativeQuery(query);
			List<Object[]> userLoginList = nativeQuery.getResultList();
			if (userLoginList != null && userLoginList.size() > 0) {
				CurrentLoginsModel user = null;
				for (Object[] obj : userLoginList) {
					if (loginsMap.containsKey((String) obj[2]) == false) {
						user = new CurrentLoginsModel();
						user.setUserId((String) obj[0]);
						user.setLoginTime((Date) obj[1]);
						user.setImei((String) obj[2]);
						user.setLocation((String) obj[3]);
						user.setApkVersion((String) obj[4]);
						loginsMap.put((String) obj[0], user);
					}
				}
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date cur_date = new Date();
			String fDate = dateFormat.format(cur_date);
			String fromDate = fDate.concat(" 00:00:00");
			String toDate = fDate.concat(" 23:59:59");
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date reqFdate = format.parse(fromDate);
			Date reqTdate = format.parse(toDate);

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, -5);
			String pastTime = format.format(cal.getTime());
			Calendar calendar = Calendar.getInstance();
			String curTime = format.format(calendar.getTime());
			Date currentTime = format.parse(curTime);
			Date pTime = format.parse(pastTime);

			/*	String hourlyPQuery = "select count(distinct(B.pickup_schedule_id)), hour(B.pickup_date) from con_details B , pickup_updation_status C "
			+"where C.con_entry_id = B.con_entry_id and B.pickup_date >= ?1 and B.pickup_date <= ?2 "
			+"and C.transaction_result = 'Success' group by hour(B.pickup_date)"; */ //removing joins
	
	         String hourlyPQuery = "select count(distinct(B.pickup_schedule_id)), hour(B.pickup_date) from con_details B  "
			+"where  B.pickup_date >= ?1 and B.pickup_date <= ?2 "
			+"and B.transaction_result = 'Success' group by hour(B.pickup_date)"; 

			Query mpQuery = manageTransaction.createNativeQuery(hourlyPQuery);
			mpQuery.setParameter(1, reqFdate);
			mpQuery.setParameter(2, reqTdate);
			List<Object []> successPickupsData = mpQuery.getResultList();
			Map<Long, Long> successPickupsMap = null;
			if(successPickupsData != null && !successPickupsData.isEmpty()){
				successPickupsMap = new HashMap<>();
				for(Object [] o : successPickupsData){
					
					if(!successPickupsMap.containsKey((long) o[0])){
						successPickupsMap.put((long) o[0], ((long) o[1]));
					}
				}
			}
			
			Map<Long, Long> hourlySuccessPickups = new TreeMap<>();
			List<HourlyPickupsBarModel> hourlyPickupsGraphData = null;
			HourlyPickupsBarModel hourlyPickupsGraphObj = null;
			for(int h = 0; h < 24; h++){
				hourlySuccessPickups.put((long)h, (long) 0);
			}
			if(successPickupsMap != null && !successPickupsMap.isEmpty()){
				for(Long i : successPickupsMap.values()){
					if(hourlySuccessPickups.containsKey(i)){
						hourlySuccessPickups.put(i, hourlySuccessPickups.get(i) + 1);
					}
				}
				
				hourlyPickupsGraphData = new ArrayList<HourlyPickupsBarModel>();
				for(Entry<Long, Long> e : hourlySuccessPickups.entrySet()){
					hourlyPickupsGraphObj = new HourlyPickupsBarModel();
					hourlyPickupsGraphObj.setHour(e.getKey());
					hourlyPickupsGraphObj.setCount(e.getValue());
					hourlyPickupsGraphData.add(hourlyPickupsGraphObj);
				}
			}
			/* hourly pickups end */
			
			/* hourly deliveries begin */
			String hourlyDels = "select d.awb_no, d.pdc_number, d.transaction_date from delivery_updation d where d.status = 'Delivered' "
					+ "and d.transaction_date >= ?1 and d.transaction_date <= ?2";
			Query mdelQuery = manageTransaction.createNativeQuery(hourlyDels);
			mdelQuery.setParameter(1, reqFdate);
			mdelQuery.setParameter(2, reqTdate);
			List<Object[]> successDeliveriesData = mdelQuery.getResultList();
			Map<String, Integer> successDeliveriesMap = null;
			if(successDeliveriesData != null && !successDeliveriesData.isEmpty()){
				successDeliveriesMap = new HashMap<>();
				for(Object [] o : successDeliveriesData){
					if(!successDeliveriesMap.containsKey((String) o[0]+""+(String) o[1])){
						successDeliveriesMap.put((String) o[0]+""+(String) o[1], (Integer)((Date) o[2]).getHours());
					}
				}
			}
			
			Map<Integer, Long> hourlySuccessDeliveries = new TreeMap<>();
			List<HourlyPickupsBarModel> hourlyDeliveriesGraphData = null;
			HourlyPickupsBarModel hourlyDeliveriesGraphObj = null;
			
			for(Integer h = 0; h < 24; h++){
				hourlySuccessDeliveries.put(h, (long) 0);
			}
			if(successDeliveriesMap != null && !successDeliveriesMap.isEmpty()){
				for(Integer i : successDeliveriesMap.values()){
					if(hourlySuccessDeliveries.containsKey(i)){
						hourlySuccessDeliveries.put(i, hourlySuccessDeliveries.get(i) + 1);
					}
				}
				
				hourlyDeliveriesGraphData = new ArrayList<HourlyPickupsBarModel>();
				for(Entry<Integer, Long> e : hourlySuccessDeliveries.entrySet()){
					hourlyDeliveriesGraphObj = new HourlyPickupsBarModel();
					hourlyDeliveriesGraphObj.setHour(e.getKey());
					hourlyDeliveriesGraphObj.setCount(e.getValue());
					hourlyDeliveriesGraphData.add(hourlyDeliveriesGraphObj);
				}
			}
			
			long regCount = 0;
			long pudCount = 0;
			long totCount = 0;
			long successCount = 0;
			long attemptedCount = 0;
			long pendingCount = 0;
			long pickupsSum = 0;
			long delSuccessCount = 0;
			long totdelsCount = 0;
			long delsAttempted = 0;
			long delsPending = 0;
			try {
				// hourly deliveries end
				String totreg = "select count(distinct(pickup_schedule_id)) from pickup_registration where pickup_date >= ?1 and pickup_date <= ?2";
				Query totRegQuery = manageTransaction.createNativeQuery(totreg);
				totRegQuery.setParameter(1, reqFdate);
				totRegQuery.setParameter(2, reqTdate);
				regCount = (long) totRegQuery.getSingleResult();

				String totpud = "select count(distinct(pickup_schedule_id)) from pud_data where pickup_date >= ?1 and pickup_date <= ?2";
				Query totPUDQuery = manageTransaction.createNativeQuery(totpud);
				totPUDQuery.setParameter(1, reqFdate);
				totPUDQuery.setParameter(2, reqTdate);
				pudCount = (long) totPUDQuery.getSingleResult();

				/*String pickupsSuccess = "select count(distinct(B.pickup_schedule_id)) from con_details B , pickup_updation_status C "
				+ "where C.con_entry_id = B.con_entry_id and B.pickup_date  >= ?1 and B.pickup_date <= ?2"
				+ " and C.transaction_result = 'Success'";*///removing joins
		
		        String pickupsSuccess = "select count(distinct(B.pickup_schedule_id)) from con_details B  "
				+ "where  B.pickup_date  >= ?1 and B.pickup_date <= ?2"
				+ " and B.transaction_result = 'Success'";
				Query successQuery = manageTransaction.createNativeQuery(pickupsSuccess);
				successQuery.setParameter(1, reqFdate);
				successQuery.setParameter(2, reqTdate);
				successCount = (long) successQuery.getSingleResult();

				/*String pkAtQuery = "select count(distinct(B.pickup_schedule_id)) from pickup_reschedule B, "
				+ "pickup_reschedule_update_status C where B.pickup_schedule_id = C.pickup_schedule_id "
				+ "and B.created_timestamp >= ?1 and B.created_timestamp <= ?2 and C.transaction_result = 'true'";*///removing joins
		
		         String pkAtQuery = "select count(distinct(B.pickup_schedule_id)) from pickup_reschedule B "
				+ " where  B.created_timestamp >= ?1 and B.created_timestamp <= ?2 and B.transaction_result = 'true'";
				Query pkAtNative = manageTransaction.createNativeQuery(pkAtQuery);
				pkAtNative.setParameter(1, reqFdate);
				pkAtNative.setParameter(2, reqTdate);
				attemptedCount = (long) pkAtNative.getSingleResult();
				
				/*String totalDels = "select C.con, C.pdc from agent_pdc A, delivery_con_details C "
						+ "where A.pdc = C.pdc and A.created_timestamp > curDate() and C.created_date > curdate()";*/ //removing joins
				
				String totalDels = "select C.con, C.pdc from  delivery_con_details C "
						+ "where  C.created_date > curdate()";
				Query totDelsQuery = manageTransaction.createNativeQuery(totalDels);
				List<Object []> totalDelsList = totDelsQuery.getResultList();
				Map<String, String> totalDelsMap = null;
				if(totalDelsList != null && !totalDelsList.isEmpty()){
					totalDelsMap = new HashMap<>();
					String t_key = "";
					for(Object [] s : totalDelsList){
						t_key = (Integer) s[0]+""+(String) s[1];
						if(!totalDelsMap.containsKey(t_key)){
							totalDelsMap.put(t_key, t_key);
						}
					}
				}
				if(totalDelsMap!=null)
				totdelsCount = totalDelsMap.size();
				
				String delSCount = "select d.awb_no, d.pdc_number, d.status from delivery_updation d where d.status = 'Delivered' "
						+ "and d.transaction_date >= ?1 and d.transaction_date <= ?2";
				Query delsQuery = manageTransaction.createNativeQuery(delSCount);
				delsQuery.setParameter(1, reqFdate);
				delsQuery.setParameter(2, reqTdate);
				List<Object []> successDeliveriesCountList = delsQuery.getResultList();
				Map<String, String> successDeliveriesCountMap = null;
				
				if(successDeliveriesCountList != null && !successDeliveriesCountList.isEmpty()){
					successDeliveriesCountMap = new HashMap<>();
					String d_key = "";
					for (Object[] a : successDeliveriesCountList) {
						d_key = (String) a[0] + "" + (String) a[1];
						if (!successDeliveriesCountMap.containsKey(d_key)) {
							successDeliveriesCountMap.put(d_key, d_key);
						}
					}
				}
				if(successDeliveriesCountMap!=null)
				delSuccessCount = successDeliveriesCountMap.size();
				
				String delACount = "select d.awb_no, d.pdc_number, d.status from delivery_updation d where d.status = 'Delivery Attempted' "
						+ "and d.transaction_date >= ?1 and d.transaction_date <= ?2";
				Query delaQuery = manageTransaction.createNativeQuery(delACount);
				delaQuery.setParameter(1, reqFdate);
				delaQuery.setParameter(2, reqTdate);
				List<Object []> attemptedDeliveriesCountList = delaQuery.getResultList();
				Map<String, String> attemptedDeliveriesCountMap = null;
				if(attemptedDeliveriesCountList != null && !attemptedDeliveriesCountList.isEmpty()){
					attemptedDeliveriesCountMap = new HashMap<>();
					String a_key = "";
					for(Object [] a : attemptedDeliveriesCountList){
						a_key = (String) a[0] + "" + (String) a[1];
						if(!attemptedDeliveriesCountMap.containsKey(a_key)){
							attemptedDeliveriesCountMap.put(a_key, a_key);
						}
					}
				}
				if(attemptedDeliveriesCountMap!=null)
				delsAttempted = attemptedDeliveriesCountMap.size();

			} catch (Exception e) {
				e.printStackTrace();

			}

			totCount = regCount + pudCount;
			if (totCount != 0 && totCount > 0) {
				pendingCount = totCount - (successCount + attemptedCount);
			} else {
				pendingCount = 0;
			}
			if (totdelsCount != 0 && totdelsCount > 0) {
				delsPending = totdelsCount - (delSuccessCount + delsAttempted);
			} else {
				delsPending = 0;
			}

			String audits = "select user_id, service_type, result_message, timestamp from audit_log where timestamp >= ?1 and timestamp <= ?2";
			Query auditQuery = manageTransaction.createNativeQuery(audits);
			auditQuery.setParameter(1, pTime);
			auditQuery.setParameter(2, currentTime);
			List<Object[]> aList = auditQuery.getResultList();
			List<UAuditsDataModel> auditsList = null;
			UAuditsDataModel auditsModel = null;
			auditsList = new ArrayList<UAuditsDataModel>();
			if (aList != null && aList.size() > 0) {
				
				for (Object[] auObj : aList) {
					auditsModel = new UAuditsDataModel();
					auditsModel.setUserId((String) auObj[0]);
					auditsModel.setServiceType((String) auObj[1]);
					auditsModel.setServiceResponse((String) auObj[2]);
					auditsModel.setTimeStamp((Date) auObj[3]);
					auditsList.add(auditsModel);
				}
			}

			List<CurrentLoginsModel> response_list = new ArrayList<CurrentLoginsModel>();
			for (CurrentLoginsModel obj : loginsMap.values()) {
				response_list.add(obj);
			}
			OnLoadDataModel onLoadDataModel = new OnLoadDataModel();
			onLoadDataModel.setTodaysPickups(totCount);
			onLoadDataModel.setAttemptedPickups(attemptedCount);
			onLoadDataModel.setSuccessPickups(successCount);
			onLoadDataModel.setPendingPickups(pendingCount);
			onLoadDataModel.setTotRegPickups(regCount+pudCount);
			onLoadDataModel.setTodaysDeliveries(totdelsCount);
			onLoadDataModel.setDelsSuccess(delSuccessCount);
			onLoadDataModel.setDelsAttempted(delsAttempted);
			onLoadDataModel.setDelsPending(delsPending);
			onLoadDataModel.setLogList(response_list);
			onLoadDataModel.setMonthlySPickupsList(hourlyPickupsGraphData);
			onLoadDataModel.setHourlyDeliveries(hourlyDeliveriesGraphData);
			onLoadDataModel.setAuditsList(auditsList);

			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.SUCCESSFUL, onLoadDataModel));
		} catch (Exception e) {
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		} finally {
			if (manageTransaction != null) {
				manageTransaction.close();
			}
		}

		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
