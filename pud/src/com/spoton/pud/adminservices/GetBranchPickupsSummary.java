package com.spoton.pud.adminservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.AbstractDocument.BranchElement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.BranchPickupsSummaryModel;
import com.spoton.pud.adminmodels.PickupsDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class GetBranchPickupsSummary
 */
@WebServlet("/getBranchPickupsSummary")
public class GetBranchPickupsSummary extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetBranchPickupsSummary() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction = null;
		String reqDate = request.getParameter("reqDate");
		String branchCode = request.getParameter("branchCode");
		String userName = "";
		String fdatestring = null;
		String tdatestring = null;
		Date reqFromDate = null;
		Date reqToDate = null;
		
		if (request.getParameter("userName") != null) {
			userName = request.getParameter("userName");
		}

		if (CommonTasks.check(reqDate)) {
			fdatestring = reqDate.concat(" 00:00:00");
			tdatestring = reqDate.concat(" 23:59:59");
			try {

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				reqFromDate = format.parse(fdatestring);
				reqToDate = format.parse(tdatestring);
				manageTransaction = new ManageTransaction();
				BranchPickupsSummaryModel summaryModel = null;
				Map<String, BranchPickupsSummaryModel> summaryMap = new HashMap<String, BranchPickupsSummaryModel>();
				String locQuery = "select distinct(location) from user_data_master";
				Query locationsQuery = manageTransaction.createNativeQuery(locQuery);
				List<Object> branchesList = locationsQuery.getResultList();
				if(branchesList != null && branchesList.size() > 0)
				{
				for(Object obj : branchesList)
				{
					summaryModel = new BranchPickupsSummaryModel();
					summaryModel.setBranch((String) obj);
					summaryMap.put((String) obj, summaryModel);
				}
				
				/*String totReg = "select count(distinct(pickup_schedule_id)), B.location from pickup_registration A, user_data_master B "
				+"where A.mobile_user_id = B.app_user_name and A.pickup_date >= ?1 and A.pickup_date <= ?2 "
				+"and B.location = ?3";*///removing joins
		
		String totReg = "select count(distinct(pickup_schedule_id)), SUBSTRING(A.mobile_user_id  from 1 for 4) from pickup_registration A "
				+"where  A.pickup_date >= ?1 and A.pickup_date <= ?2 "
				+"group by SUBSTRING(A.mobile_user_id  from 1 for 4)";
		
		/*String totPud = "select count(distinct(pickup_schedule_id)), B.location from pud_data A, user_data_master B "
				+"where A.user_id = B.app_user_name and A.pickup_date >= ?1 and A.pickup_date <= ?2 "
				+"and B.location = ?3";*///removing joins
		
		String totPud = "select count(distinct(pickup_schedule_id)), SUBSTRING(A.user_id  from 1 for 4) from pud_data A "
				+"where A.pickup_date >= ?1 and A.pickup_date <= ?2 "
				+"group by SUBSTRING(A.user_id  from 1 for 4)";
		
		/*String pkSuccess = "select count(distinct(B.pickup_schedule_id)), D.location from con_details B , pickup_updation_status C, user_data_master D"
				+" where C.con_entry_id = B.con_entry_id and B.user_name = D.app_user_name and D.location = ?1 and B.pickup_date  >= ?2 and B.pickup_date <= ?3"
				+" and C.transaction_result = 'Success'";*///removing joins
		String pkSuccess = "select count(distinct(B.pickup_schedule_id)), SUBSTRING(B.user_name  from 1 for 4) from con_details B "
				+" where  B.pickup_date  >= ?2 and B.pickup_date <= ?3"
				+" and B.transaction_result = 'Success' group by SUBSTRING(B.user_name  from 1 for 4)";
		
		/*String pkAttempt = "select count(distinct(B.pickup_schedule_id)), D.location from pickup_reschedule B, "
				+"pickup_reschedule_update_status C, user_data_master D where B.pickup_schedule_id = C.pickup_schedule_id "
				+"and B.app_mobile_user_name = D.app_user_name and D.location = ?1 and B.created_timestamp >= ?2 and B.created_timestamp <= ?3 "
				+"and C.transaction_result = 'true'";*///removing joins
		
		String pkAttempt = "select count(distinct(B.pickup_schedule_id)), SUBSTRING(B.app_mobile_user_name from 1 for 4) from pickup_reschedule B "
				+" where   B.created_timestamp >= ?2 and B.created_timestamp <= ?3 "
				+"and B.transaction_result = 'true'  group by SUBSTRING(B.app_mobile_user_name from 1 for 4)";
				
				Query totRegQuery = manageTransaction.createNativeQuery(totReg);
				totRegQuery.setParameter(1, reqFromDate);
				totRegQuery.setParameter(2, reqToDate);
				//totRegQuery.setParameter(3, branchCode);

				Query totPudQuery = manageTransaction.createNativeQuery(totPud);
				totPudQuery.setParameter(1, reqFromDate);
				totPudQuery.setParameter(2, reqToDate);
				//totPudQuery.setParameter(3, branchCode);
				
				Query successPKQuery = manageTransaction.createNativeQuery(pkSuccess);
				//successPKQuery.setParameter(1, branchCode);
				successPKQuery.setParameter(2, reqFromDate);
				successPKQuery.setParameter(3, reqToDate);
				
				Query attemptsQuery = manageTransaction.createNativeQuery(pkAttempt);
				//attemptsQuery.setParameter(1, branchCode);
				attemptsQuery.setParameter(2, reqFromDate);
				attemptsQuery.setParameter(3, reqToDate);
				
				List<Object []> locRegList = totRegQuery.getResultList();
				List<Object []> locPudList = totPudQuery.getResultList();
				List<Object []> locSuccessList = successPKQuery.getResultList();
				List<Object []> locAttemptsList = attemptsQuery.getResultList();
				
				if(locRegList != null && locRegList.size() > 0)
				{
					for(Object [] obj : locRegList)
					{
						if(summaryMap.get((String) obj[1])!=null)
						summaryMap.get((String) obj[1]).setTotalPickups((long) obj[0]);
					}
				}
				
				if(locPudList != null && locPudList.size() > 0)
				{
					for(Object [] obj : locPudList)
					{
						if(summaryMap.get((String) obj[1])!=null)
						summaryMap.get((String) obj[1]).setTotalPickups(summaryMap.get((String) obj[1]).getTotalPickups() + (long) obj[0]);
					}
				}
				
				if(locSuccessList != null && locSuccessList.size() > 0)
				{
					for(Object [] obj : locSuccessList)
					{
						if(summaryMap.get((String) obj[1])!=null)
						summaryMap.get((String) obj[1]).setPickupSuccessCount((long) obj[0]);
					} 
				}
				
				if(locAttemptsList != null && locAttemptsList.size() > 0)
				{
					for(Object [] obj : locAttemptsList)
					{
						if(summaryMap.get((String) obj[1])!=null)
						summaryMap.get((String) obj[1]).setAttemptedPickups((long) obj[0]);
					}
				}
				
				for(BranchPickupsSummaryModel model : summaryMap.values())
				{
					if(model.getTotalPickups() > 0)
					{
						if(summaryMap.get(model.getBranch())!=null)
						summaryMap.get(model.getBranch()).setPendingCount(model.getTotalPickups() - (model.getPickupSuccessCount() + model.getAttemptedPickups()));
						
					}
				}
				
				for(BranchPickupsSummaryModel model : summaryMap.values())
				{
					if(model.getTotalPickups() > 0)
					{
//						System.out.println(model.getBranch()+" -s"+model.getPickupSuccessCount()+" A-"+model.getAttemptedPickups());
						if(summaryMap.get(model.getBranch())!=null)
						summaryMap.get(model.getBranch()).setProductivity(((model.getPickupSuccessCount() + model.getAttemptedPickups())/model.getTotalPickups()*100));
//						System.out.println("tot "+ summaryMap.get(model.getBranch()).getTotalPickups() +"att "+summaryMap.get(model.getBranch()).getProductivity());
					}
				}
				
				result = gson.toJson(new GeneralResponse(Constants.TRUE, Constants.SUCCESSFUL,summaryMap.values()));
				
				}
				
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERROR_NO_DATA_AVAILABLE,null));
				}
				
			}

			catch (Exception e) {
				e.printStackTrace();
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			}
			
			finally{
				manageTransaction.close();
			}
		} 
		else {
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
		response.getWriter().write(result);
	
	}

}
