package com.spoton.pud.adminservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.adminmodels.CurrentLoginsModel;
import com.spoton.pud.adminmodels.HourlyPickupsBarModel;
import com.spoton.pud.adminmodels.OnLoadDataModel;
import com.spoton.pud.adminmodels.UAuditsDataModel;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class LoggedInUsersList
 */
@WebServlet("/dashboardData")
public class DashboardData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DashboardData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		Gson gson = new GsonBuilder().serializeNulls().create();
		String result = "";
		ManageTransaction manageTransaction = null;
		try{
			manageTransaction = new ManageTransaction();
			Map<String,CurrentLoginsModel> loginsMap = new HashMap<String,CurrentLoginsModel>();
			//removing joins //String query = "select A.user_name, A.created_timestamp, A.imei, B.location, A.apk_version from user_session_data A, user_data_master B where A.user_name = B.app_user_name and active_flag = 1";
			String query = "select A.user_name, A.created_timestamp, A.imei, A.apk_version from user_session_data A where active_flag = 1";
			Query nativeQuery = manageTransaction.createNativeQuery(query);
			List<Object[]> userLoginList = nativeQuery.getResultList();
			if(userLoginList != null && userLoginList.size() > 0)
			{
				CurrentLoginsModel user = null;
				for(Object [] obj : userLoginList)
				{
					if(loginsMap.containsKey((String) obj[2]) == false)
							{
								user = new CurrentLoginsModel();
								user.setUserId((String) obj[0]);
								user.setLoginTime((Date) obj[1]);
								user.setImei((String) obj[2]);
								user.setLocation((String) obj[3]);
								user.setApkVersion((String) obj[4]);
								loginsMap.put((String) obj[0], user);
							}
				}
			}
			
			long regCount = 0;
			long pudCount = 0;
			long totCount = 0;
			long successCount = 0;
			long attemptedCount = 0;
			long pendingCount = 0;
			long pickupsSum = 0;
			long delSuccessCount = 0;
			long totdelsCount = 0;
			long delsAttempted = 0;
			long delsPending = 0;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date cur_date = new Date();
			String fDate = dateFormat.format(cur_date);
			String fromDate = fDate.concat(" 00:00:00");
			String toDate = fDate.concat(" 23:59:59");
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date reqFdate = format.parse(fromDate);
			Date reqTdate = format.parse(toDate);
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MINUTE, -5);
			String pastTime = format.format(cal.getTime());
			Calendar calendar = Calendar.getInstance();
			String curTime = format.format(calendar.getTime());
			Date currentTime = format.parse(curTime);
			Date pTime = format.parse(pastTime);
			/*String hourlyPQuery = "select count(distinct(B.pickup_schedule_id)), extract(hour from B.pickup_date) from con_details B , pickup_updation_status C "
			+"where C.con_entry_id = B.con_entry_id and B.pickup_date >= ?1 and B.pickup_date <= ?2 "
			+"and C.transaction_result = 'Success' group by extract(hour from B.pickup_date)"; */ //removing joins
	
	   String hourlyPQuery = "select count(distinct(B.pickup_schedule_id)), extract(hour from B.pickup_date) from con_details B "
			+"where B.pickup_date >= ?1 and B.pickup_date <= ?2 "
			+"and B.transaction_result = 'Success' group by extract(hour from B.pickup_date)"; 
	
	/*String hourlyDels =  "select count(distinct(concat(d.awb_no,d.pdc_number))), extract(hour from d.transaction_date) from delivery_updation d,delivery_updation_status s where d.status = 'Delivered' "
		        + "and d.transaction_id=s.transaction_id and d.transaction_date >= curdate() and s.transaction_result = 'true' and d.transaction_date <= ?1 "
		        + "group by extract(hour from d.transaction_date)";*/ //removing joins
	
	    String hourlyDels =  "select count(distinct(concat(d.awb_no,d.pdc_number))), extract(hour from d.transaction_date) from delivery_updation d where d.status = 'Delivered' "
		        + "and d.transaction_date >= curdate() and d.transaction_result = 'true' and d.transaction_date <= ?1 "
		        + "group by extract(hour from d.transaction_date)";
			Query mpQuery = manageTransaction.createNativeQuery(hourlyPQuery);
			mpQuery.setParameter(1, reqFdate);
			mpQuery.setParameter(2, reqTdate);
			
			Query mdelQuery = manageTransaction.createNativeQuery(hourlyDels);
			mdelQuery.setParameter(1, reqTdate);
			
			List<Object []> hourlyList = mpQuery.getResultList();
			List<Object []> hourlyDelsList = mdelQuery.getResultList();
			List<HourlyPickupsBarModel> hList = new ArrayList<HourlyPickupsBarModel>();
			List<HourlyPickupsBarModel> hdels = new ArrayList<HourlyPickupsBarModel>();
			List<UAuditsDataModel> auditsList = new ArrayList<UAuditsDataModel>();
			UAuditsDataModel auditsModel = null;
			HourlyPickupsBarModel barModel = null;
			/*hourly pickups begin*/
			if(hourlyList != null && hourlyList.size() > 0)
			{
				
				for(Object [] obj : hourlyList)
				{
					barModel = new HourlyPickupsBarModel();
					barModel.setCount((long) obj[0]);
					barModel.setHour((long) obj[1]);
					hList.add(barModel);
				}
			}
			
			Map <Long, Long> hMap = new HashMap<Long, Long>();
			long count = 0;
			for(long i=0;i<24;i++)
			{
				hMap.put(i, count);
			}
			
			if(hList != null && hList.size() > 0)
			{
				for(HourlyPickupsBarModel obj : hList)
				{
					hMap.put(obj.getHour(), obj.getCount());
				}
			}
			List<Long> hrlist = new ArrayList<Long>();
			if(hMap.values().size() > 0)
			{
				for(Long o : hMap.values())
				{
					hrlist.add(o);
				}
			}
			/*hourly pickups end*/
			/*hourly deliveries begin*/
			if(hourlyDelsList != null && hourlyDelsList.size() > 0)
			{
				
				for(Object [] obj : hourlyDelsList)
				{
					barModel = new HourlyPickupsBarModel();
					barModel.setCount((long) obj[0]);
					barModel.setHour((int) obj[1]);
					hdels.add(barModel);
				}
			}
			Map <Long, Long> hdelMap = new HashMap<Long, Long>();
			long delCount = 0;
			for(long i=0;i<24;i++)
			{
				hdelMap.put(i, count);
			}
			
			if(hdels != null && hdels.size() > 0)
			{
				for(HourlyPickupsBarModel obj : hdels)
				{
					hdelMap.put(obj.getHour(), obj.getCount());
				}
			}
			
			List<Long> hrdelList = new ArrayList<Long>();
			if(hdelMap.values().size() > 0)
			{
				for(Long o : hdelMap.values())
				{
					hrdelList.add(o);
				}
			}
			
//			hourly deliveries end
			String totreg = "select count(distinct(pickup_schedule_id)) from pickup_registration where Date(pickup_date) = curdate()";
			String totpud = "select count(distinct(pickup_schedule_id)) from pud_data where Date(pickup_date) = curdate()";
			String regQuery = "select count(distinct(pickup_schedule_id)) from pickup_registration where pickup_date >= ?1 and pickup_date <= ?2";
			String pudQuery = "select count(distinct(pickup_schedule_id)) from pud_data where pickup_date >= ?1 and pickup_date <= ?2";
			/*String pickupsSuccess = "select count(distinct(B.pickup_schedule_id)) from con_details B , pickup_updation_status C "
					+ "where C.con_entry_id = B.con_entry_id and B.pickup_date  >= ?1 and B.pickup_date <= ?2"
					+ " and C.transaction_result = 'Success'";*/ //removing joins
			
			String pickupsSuccess = "select count(distinct(B.pickup_schedule_id)) from con_details B "
					+ "where B.pickup_date  >= ?1 and B.pickup_date <= ?2"
					+ " and B.transaction_result = 'Success'";
			
			/*String pkAtQuery = "select count(distinct(B.pickup_schedule_id)) from pickup_reschedule B, "
					+ "pickup_reschedule_update_status C where B.pickup_schedule_id = C.pickup_schedule_id "
					+ "and B.created_timestamp >= ?1 and B.created_timestamp <= ?2 and C.transaction_result = 'true'";*/ //removing joins
			
			String pkAtQuery = "select count(distinct(B.pickup_schedule_id)) from pickup_reschedule B "
					+ "where B.created_timestamp >= ?1 and B.created_timestamp <= ?2 and B.transaction_result = 'true'";
			
			/*String delSCount = "select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d,delivery_updation_status s where d.status = 'Delivered' "
	 		        + "and d.transaction_id=s.transaction_id and d.transaction_date >= curdate() and s.transaction_result = 'true'";*/ //removing joins
			
			String delSCount = "select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.status = 'Delivered' "
	 		        + "and d.transaction_date >= curdate() and d.transaction_result = 'true'";
			
			/*String delsACount = "select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d,delivery_updation_status s where d.status = 'Delivery Attempted' "
	 		        + "and d.transaction_id=s.transaction_id and d.transaction_date >= curdate() and s.transaction_result = 'true'";*/ //removing joins
			
			String delsACount = "select count(distinct(concat(d.awb_no,d.pdc_number))) from delivery_updation d where d.status = 'Delivery Attempted' "
	 		        + "and d.transaction_date >= curdate() and d.transaction_result = 'true'";
			
			/*String totalDels = "select count(distinct(concat(C.con,C.pdc))) from agent_pdc A, delivery_con_details C "
					+ "where A.pdc = C.pdc and A.created_timestamp >= curDate() and C.created_date >= curdate()"; */ //removing joins
			
			String totalDels = "select count(distinct(concat(C.con,C.pdc))) from delivery_con_details C where C.created_date >= curdate()";
			
			String audits = "select user_id, service_type, result_message, timestamp from audit_log where timestamp >= ?1 and timestamp <= ?2";
			
			Query totRegQuery = manageTransaction.createNativeQuery(totreg);
			Query totPUDQuery = manageTransaction.createNativeQuery(totpud);
			Query auditQuery = manageTransaction.createNativeQuery(audits);
			auditQuery.setParameter(1, pTime);
			auditQuery.setParameter(2, currentTime);
			
			Query regNative = manageTransaction.createNativeQuery(regQuery);
			regNative.setParameter(1, reqFdate);
			regNative.setParameter(2, reqTdate);
			
			Query pudNative = manageTransaction.createNativeQuery(pudQuery);
			pudNative.setParameter(1, reqFdate);
			pudNative.setParameter(2, reqTdate);
			
			Query successQuery = manageTransaction.createNativeQuery(pickupsSuccess);
			successQuery.setParameter(1, reqFdate);
			successQuery.setParameter(2, reqTdate);
			
			Query pkAtNative = manageTransaction.createNativeQuery(pkAtQuery);
			pkAtNative.setParameter(1, reqFdate);
			pkAtNative.setParameter(2, reqTdate);
			
			Query delsQuery = manageTransaction.createNativeQuery(delSCount);
			Query totDelsQuery = manageTransaction.createNativeQuery(totalDels);
			Query delsAQuery = manageTransaction.createNativeQuery(delsACount);
			
			try{
				pickupsSum = (long)totRegQuery.getSingleResult() + (long)totPUDQuery.getSingleResult();
				regCount = (long)regNative.getSingleResult();
				pudCount = (long)pudNative.getSingleResult();
				successCount = (long)successQuery.getSingleResult();
				attemptedCount = (long)pkAtNative.getSingleResult();
				delSuccessCount = (long)delsQuery.getSingleResult();
				totdelsCount = (long)totDelsQuery.getSingleResult();
				delsAttempted = (long)delsAQuery.getSingleResult();
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
			}
			
			totCount = regCount+pudCount;
			if(totCount != 0 && totCount > 0)
			{
			pendingCount = totCount-successCount-attemptedCount;
			}
			else{
				pendingCount = 0;
			}
			if(totdelsCount != 0 && totdelsCount > 0)
			{
			delsPending = totdelsCount-delSuccessCount-delsAttempted;
			}
			else{
				delsPending = 0;
			}
			
			List<Object []> aList = auditQuery.getResultList();
			if(aList != null && aList.size() > 0)
			{
				for(Object [] auObj : aList)
				{
					auditsModel = new UAuditsDataModel();
					auditsModel.setUserId((String) auObj[0]);
					auditsModel.setServiceType((String) auObj[1]);
					auditsModel.setServiceResponse((String) auObj[2]);
					auditsModel.setTimeStamp((Date) auObj[3]);
					auditsList.add(auditsModel);
				}
			}
			
			List<CurrentLoginsModel> response_list = new ArrayList<CurrentLoginsModel>();
			for(CurrentLoginsModel obj : loginsMap.values())
			{
				response_list.add(obj);
			}
			OnLoadDataModel onLoadDataModel = new OnLoadDataModel();
			onLoadDataModel.setTodaysPickups(totCount);
			onLoadDataModel.setAttemptedPickups(attemptedCount);
			onLoadDataModel.setSuccessPickups(successCount);
			onLoadDataModel.setPendingPickups(pendingCount);
			onLoadDataModel.setTotRegPickups(pickupsSum);
			onLoadDataModel.setTodaysDeliveries(totdelsCount);
			onLoadDataModel.setDelsSuccess(delSuccessCount);
			onLoadDataModel.setDelsAttempted(delsAttempted);
			onLoadDataModel.setDelsPending(delsPending);
			onLoadDataModel.setLogList(response_list);
			onLoadDataModel.setMonthlySPickupsList((List<HourlyPickupsBarModel>)hrlist);
            onLoadDataModel.setHourlyDeliveries((List<HourlyPickupsBarModel>)hrdelList);
			onLoadDataModel.setAuditsList(auditsList);
			
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.SUCCESSFUL,onLoadDataModel));
		}
		catch(Exception e)
		{
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(manageTransaction!=null){
				manageTransaction.close();
			}
		}
		
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
