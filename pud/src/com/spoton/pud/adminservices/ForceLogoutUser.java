package com.spoton.pud.adminservices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GcmPushResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.mobileoperations.SendMessage;

/**
 * Servlet implementation class ForceLogoutUser
 */
@WebServlet("/forceLogoutUser")
public class ForceLogoutUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("LogoutUser");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForceLogoutUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction manageTransaction = null;
		String userName = request.getParameter("userName");
		String query = "";
		String query1 = "";
		String query2 = "";
		boolean pushReturn = false;
		
		if(CommonTasks.check(userName))
		{
			logger.info("User Id :"+userName);
			try{
				manageTransaction = new ManageTransaction();
				
				String checkQuery = "select * from user_data_master where app_user_name = ?1";
				Query nQuery = manageTransaction.createNativeQuery(checkQuery);
				nQuery.setParameter(1, userName);
				List<Object []> user = nQuery.getResultList();
				if(!user.isEmpty())
				{
					logger.info("User exists in master data");
					String checkLog = "select * from user_session_data where user_name = ?1 and active_flag = 1";
					Query checkLogQuery = manageTransaction.createNativeQuery(checkLog);
					checkLogQuery.setParameter(1, userName);
					List<Object []> logObj = checkLogQuery.getResultList();
					if(!logObj.isEmpty()){
						logger.info("user is active in session data");
						query2 = "select * from device_info where active_flag = 1 and user_id = ?1";
						Query nativeQuery2 = manageTransaction.createNativeQuery(query2);
						nativeQuery2.setParameter(1, userName);
						List<Object []> regDevice = nativeQuery2.getResultList();
						if(!regDevice.isEmpty())
						{
							logger.info("user record found in device info : device-"+regDevice.get(0)[8]);
							String pushTokenQuery = "select pushToken from device_info where user_id=?1 and active_flag=1 order by device_id desc limit 1";
							Query typedQuery1 = manageTransaction.createNativeQuery(pushTokenQuery);
							typedQuery1.setParameter(1, userName);
							String pushToken = (String) typedQuery1.getSingleResult();
							if(CommonTasks.check(pushToken))
							{
								logger.info("push token found : "+pushToken);
								String message = "Logout User";
								List<GcmPushResponse> pushList = new ArrayList<GcmPushResponse>();
								GcmPushResponse pushResponse = new GcmPushResponse();
								pushResponse.setId(7);
								pushResponse.setMessage(message);
								pushList.add(pushResponse);

								String pushMessage = gson.toJson(pushList);
								pushReturn = SendMessage.ANDROID_sendMessage(pushToken, pushMessage);
								if(pushReturn)
								{
									logger.info("push sent to device");
//									query = "update user_session_data set active_flag = 0 , updated_timestamp = ?1 where user_name = ?2 and active_flag=1";
//									Query nativeQuery = manageTransaction.createNativeQuery(query);
//									nativeQuery.setParameter(1, new Date());
//									nativeQuery.setParameter(2, userName);
//									manageTransaction.begin();
//									nativeQuery.executeUpdate();
//									manageTransaction.commit();
//									
//									query1 = "update device_info set active_flag = 0 , updated_timestamp = ?1 where user_id = ?2 and active_flag = 1";
//									Query nativeQuery1 = manageTransaction.createNativeQuery(query1);
//									nativeQuery1.setParameter(1, new Date());
//									nativeQuery1.setParameter(2, userName);
//									manageTransaction.begin();
//									nativeQuery1.executeUpdate();
//									manageTransaction.commit();
									
									result = gson.toJson(new GeneralResponse(Constants.TRUE, "User Logged out(Push notification sent).", null));
									
								}
								else{
									result =  gson.toJson(new GeneralResponse(Constants.FALSE, "Failed to send push notification. User still active", null));
								}
							}
							else{
								result = gson.toJson(new GeneralResponse(Constants.FALSE, "Push token not found", null));
							}
						}
						else{
							
							result = gson.toJson(new GeneralResponse(Constants.FALSE, "User is not active on any device", null));
						}
						
					}
					else
					{
						result = gson.toJson(new GeneralResponse(Constants.FALSE, "User not logged in any device", null));
					}
				}
				else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE, "User doesn't exist", null));
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				logger.error("Exception e : "+e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			}
			finally {
				if(manageTransaction != null)
				manageTransaction.close();
			}
		}
		else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA,null));
		}
		logger.info(result);
		response.getWriter().write(result);
	}

}
