package com.spoton.pud.connmgr;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ManageConnection {
	
	protected EntityManagerFactory emf = null;
	private static String persitenceName="pud";
	
	ManageConnection(){
		emf =  Persistence.createEntityManagerFactory(persitenceName);
	}
	
	public EntityManagerFactory getEntityManagerFactory(){
		return emf;
	}
}


