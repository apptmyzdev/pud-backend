package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the pulled_data_acknowledgement database table.
 * 
 */
@Entity
@Table(name="pulled_data_acknowledgement")
@NamedQuery(name="PulledDataAcknowledgement.findAll", query="SELECT p FROM PulledDataAcknowledgement p")
public class PulledDataAcknowledgement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="download_date")
	private Date downloadDate;

	@Column(name="download_type")
	private int downloadType;

	@Column(name="result_flag")
	private int resultFlag;

	@Column(name="result_remarks")
	private String resultRemarks;

	@Column(name="result_transaction_id")
	private int resultTransactionId;

	@Column(name="transaction_id")
	private int transactionId;

	public PulledDataAcknowledgement() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDownloadDate() {
		return this.downloadDate;
	}

	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}

	public int getDownloadType() {
		return this.downloadType;
	}

	public void setDownloadType(int downloadType) {
		this.downloadType = downloadType;
	}

	public int getResultFlag() {
		return this.resultFlag;
	}

	public void setResultFlag(int resultFlag) {
		this.resultFlag = resultFlag;
	}

	public String getResultRemarks() {
		return this.resultRemarks;
	}

	public void setResultRemarks(String resultRemarks) {
		this.resultRemarks = resultRemarks;
	}

	public int getResultTransactionId() {
		return this.resultTransactionId;
	}

	public void setResultTransactionId(int resultTransactionId) {
		this.resultTransactionId = resultTransactionId;
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
}