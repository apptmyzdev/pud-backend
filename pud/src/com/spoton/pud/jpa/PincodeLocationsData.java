package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pincode_locations_data database table.
 * 
 */
@Entity
@Table(name="pincode_locations_data")
@NamedQuery(name="PincodeLocationsData.findAll", query="SELECT p FROM PincodeLocationsData p")
public class PincodeLocationsData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String area;

	private String district;

	private double latitude;

	private double longitude;

	private int pincode;

	private String state;

	@Column(name="state_id")
	private int stateId;

	public PincodeLocationsData() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getPincode() {
		return this.pincode;
	}

	public void setPincode(int pincode) {
		this.pincode = pincode;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getStateId() {
		return this.stateId;
	}

	public void setStateId(int stateId) {
		this.stateId = stateId;
	}

}