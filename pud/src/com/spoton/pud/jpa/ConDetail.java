package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the con_details database table.
 * 
 */
@Entity
@Table(name="con_details")
@NamedQuery(name="ConDetail.findAll", query="SELECT c FROM ConDetail c")
public class ConDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="actual_weight")
	private String actualWeight;

	@Column(name="apply_dc")
	private String applyDc;
	
	@Column(name="product_type")
	private String productType;

	//bi-directional many-to-one association to UpdatedEwayBillNumber
		@OneToMany(mappedBy="conDetail")
		private List<UpdatedEwayBillNumber> updatedEwayBillNumbers;
	
		@Column(name="eway_bill_number")
		  private String ewayBillNumber;
		
	@Column(name="vehicle_number")
	private String vehicleNumber;
	
	@Column(name="apply_nf_form")
	private String applyNfForm;

	@Column(name="apply_vtv")
	private String applyVtv;

	@Column(name="con_entry_id")
	private int conEntryId;

	@Column(name="con_number")
	private String conNumber;

	@Column(name="consignment_type")
	private String consignmentType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="crm_schedule_id")
	private String crmScheduleId;

	@Column(name="customer_code")
	private String customerCode;

	@Column(name="customer_name")
	private String customerName;
	
	@Column(name="customer_address")
	private String customerAddress;
	
	@Column(name="customer_city")
	private String customerCity;
	
	@Column(name="customer_phone")
	private String customerPhone;
	
	@Column(name="customer_email")
	private String customerEmail;
	
	@Column(name="customer_gstin")
	private String customerGstin;

	@Column(name="declared_value")
	private String declaredValue;

	@Column(name="destination_pincode")
	private String destinationPincode;

	@Column(name="erp_updated")
	private int erpUpdated;

	@Column(name="gate_pass_time")
	private String gatePassTime;

	@Column(name="pickup_type")
	private int pickupType;
	
	
	@Column(name="image1_url")
	private String image1Url;

	@Column(name="image2_url")
	private String image2Url;

	@Column(name="is_scanned")
	private int isScanned;

	@Column(name="lat_value")
	private String latValue;

	@Column(name="long_value")
	private String longValue;

	@Column(name="no_of_package")
	private String noOfPackage;

	@Column(name="order_no")
	private String orderNo;

	@Column(name="origin_pincode")
	private String originPincode;

	@Column(name="package_type")
	private String packageType;

	@Column(name="pan_no")
	private String panNo;

	@Column(name="payment_basis")
	private String paymentBasis;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;
	
	

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	private String product;

	@Column(name="receiver_name")
	private String receiverName;
	
	@Column(name="receiver_address")
	private String receiverAddress;
	
	@Column(name="receiver_city")
	private String receiverCity;

	@Column(name="receiver_phone_no")
	private String receiverPhoneNo;

	@Column(name="ref_number")
	private String refNumber;

	@Column(name="risk_type")
	private String riskType;

	@Column(name="shipment_image_url")
	private String shipmentImageUrl;

	@Column(name="special_instruction")
	private String specialInstruction;

	@Column(name="tin_no")
	private String tinNo;

	@Column(name="total_vol_weight")
	private String totalVolWeight;

	@Column(name="user_id")
	private String userId;

	@Column(name="user_name")
	private String userName;

	@Column(name="vol_type")
	private String volType;

	@Column(name="vtc_amount")
	private String vtcAmount;

	
	@Column(name="receiver_gstin")
	private String receiverGstin;
	
	@Column(name="receiver_email")
	private String receiverEmail;
	
	@Column(name="sender_email")
	private String senderEmail;
	
	@Column(name="rate_modify_enabled")
	private String rateModifyEnabled;
	
	@Column(name="is_box_piece_mapping")
	private String isBoxPieceMapping;
	
	@Column(name="pickup_updated_address")
	private String pickupUpdatedAddress;

	@Column(name="pickup_updated_pincode")
	private String pickupUpdatedPincode;
	
	@Column(name="is_pre_printed") //added on 18102019-ak
	private String isPrePrinted;
	
	@Column(name="is_unloading_sheet_generated") 
	private int isUnloadingSheetGenerated;
	
	@Column(name="transaction_result")
	private String transactionResult;
	
	@Column(name="status_flag")
	private String statusFlag;
	
	/*@Column(name="is_reverse_geocoding_success")
	private int isReverseGeocodingSuccess;

	@Column(name="geocoded_address")
	private String geocodedAddress;
	
	@Column(name="geocoded_pincode")
	private String geocodedPincode;
*/
	
	//bi-directional many-to-one association to PickupUpdationStatus
	@OneToMany(mappedBy="conDetail")
	private List<PickupUpdationStatus> pickupUpdationStatuses;

	//bi-directional many-to-one association to PieceEntry
	@OneToMany(mappedBy="conDetail")
	private List<PieceEntry> pieceEntries;

	//bi-directional many-to-one association to PieceVolume
	@OneToMany(mappedBy="conDetail")
	private List<PieceVolume> pieceVolumes;

	//bi-directional many-to-one association to Piece
	@OneToMany(mappedBy="conDetail")
	private List<Piece> pieces;

	//bi-directional many-to-one association to PiecesImage
	@OneToMany(mappedBy="conDetail")
	private List<PiecesImage> piecesImages;
	
	//bi-directional many-to-one association to PickupUpdationStatus
		@OneToMany(mappedBy="conDetail")
		private List<WstPiecesRefList> wstPiecesRefList;
		
		//bi-directional many-to-one association to PickupUpdationStatus
				@OneToMany(mappedBy="conDetail")
				private List<WstPickupsUpdatedData> wstPickupsUpdatedData;
				
				//bi-directional many-to-one association to PickupUpdationStatus
				@OneToMany(mappedBy="conDetail")
				private List<ModifiedRateCardValue> modifiedRateCardValue;

	public ConDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActualWeight() {
		return this.actualWeight;
	}

	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}

	public String getApplyDc() {
		return this.applyDc;
	}

	public void setApplyDc(String applyDc) {
		this.applyDc = applyDc;
	}

	public String getApplyNfForm() {
		return this.applyNfForm;
	}

	public void setApplyNfForm(String applyNfForm) {
		this.applyNfForm = applyNfForm;
	}

	public String getApplyVtv() {
		return this.applyVtv;
	}

	public void setApplyVtv(String applyVtv) {
		this.applyVtv = applyVtv;
	}

	public int getConEntryId() {
		return this.conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public String getConsignmentType() {
		return this.consignmentType;
	}

	public void setConsignmentType(String consignmentType) {
		this.consignmentType = consignmentType;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCrmScheduleId() {
		return this.crmScheduleId;
	}

	public void setCrmScheduleId(String crmScheduleId) {
		this.crmScheduleId = crmScheduleId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDeclaredValue() {
		return this.declaredValue;
	}

	public void setDeclaredValue(String declaredValue) {
		this.declaredValue = declaredValue;
	}

	public String getDestinationPincode() {
		return this.destinationPincode;
	}

	public void setDestinationPincode(String destinationPincode) {
		this.destinationPincode = destinationPincode;
	}

	public int getErpUpdated() {
		return this.erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public String getGatePassTime() {
		return this.gatePassTime;
	}

	public void setGatePassTime(String gatePassTime) {
		this.gatePassTime = gatePassTime;
	}

	public String getImage1Url() {
		return this.image1Url;
	}

	public void setImage1Url(String image1Url) {
		this.image1Url = image1Url;
	}

	public String getImage2Url() {
		return this.image2Url;
	}

	public void setImage2Url(String image2Url) {
		this.image2Url = image2Url;
	}

	public int getIsScanned() {
		return this.isScanned;
	}

	public void setIsScanned(int isScanned) {
		this.isScanned = isScanned;
	}

	public String getLatValue() {
		return this.latValue;
	}

	public void setLatValue(String latValue) {
		this.latValue = latValue;
	}

	public String getLongValue() {
		return this.longValue;
	}

	public void setLongValue(String longValue) {
		this.longValue = longValue;
	}

	public String getNoOfPackage() {
		return this.noOfPackage;
	}

	public void setNoOfPackage(String noOfPackage) {
		this.noOfPackage = noOfPackage;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOriginPincode() {
		return this.originPincode;
	}

	public void setOriginPincode(String originPincode) {
		this.originPincode = originPincode;
	}

	public String getPackageType() {
		return this.packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	public String getPanNo() {
		return this.panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getPaymentBasis() {
		return this.paymentBasis;
	}

	public void setPaymentBasis(String paymentBasis) {
		this.paymentBasis = paymentBasis;
	}

	public Date getPickupDate() {
		return this.pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverPhoneNo() {
		return this.receiverPhoneNo;
	}

	public void setReceiverPhoneNo(String receiverPhoneNo) {
		this.receiverPhoneNo = receiverPhoneNo;
	}

	public String getRefNumber() {
		return this.refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getRiskType() {
		return this.riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getShipmentImageUrl() {
		return this.shipmentImageUrl;
	}

	public void setShipmentImageUrl(String shipmentImageUrl) {
		this.shipmentImageUrl = shipmentImageUrl;
	}

	public String getSpecialInstruction() {
		return this.specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getTinNo() {
		return this.tinNo;
	}

	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getTotalVolWeight() {
		return this.totalVolWeight;
	}

	public void setTotalVolWeight(String totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	
	public String getRateModifyEnabled() {
		return rateModifyEnabled;
	}

	public void setRateModifyEnabled(String rateModifyEnabled) {
		this.rateModifyEnabled = rateModifyEnabled;
	}

	public String getVolType() {
		return this.volType;
	}

	public void setVolType(String volType) {
		this.volType = volType;
	}

	public String getVtcAmount() {
		return this.vtcAmount;
	}

	public void setVtcAmount(String vtcAmount) {
		this.vtcAmount = vtcAmount;
	}

	public List<PickupUpdationStatus> getPickupUpdationStatuses() {
		return this.pickupUpdationStatuses;
	}

	public void setPickupUpdationStatuses(List<PickupUpdationStatus> pickupUpdationStatuses) {
		this.pickupUpdationStatuses = pickupUpdationStatuses;
	}

	public PickupUpdationStatus addPickupUpdationStatus(PickupUpdationStatus pickupUpdationStatus) {
		getPickupUpdationStatuses().add(pickupUpdationStatus);
		pickupUpdationStatus.setConDetail(this);

		return pickupUpdationStatus;
	}

	public PickupUpdationStatus removePickupUpdationStatus(PickupUpdationStatus pickupUpdationStatus) {
		getPickupUpdationStatuses().remove(pickupUpdationStatus);
		pickupUpdationStatus.setConDetail(null);

		return pickupUpdationStatus;
	}

	public List<PieceEntry> getPieceEntries() {
		return this.pieceEntries;
	}

	public void setPieceEntries(List<PieceEntry> pieceEntries) {
		this.pieceEntries = pieceEntries;
	}

	public PieceEntry addPieceEntry(PieceEntry pieceEntry) {
		getPieceEntries().add(pieceEntry);
		pieceEntry.setConDetail(this);

		return pieceEntry;
	}

	public PieceEntry removePieceEntry(PieceEntry pieceEntry) {
		getPieceEntries().remove(pieceEntry);
		pieceEntry.setConDetail(null);

		return pieceEntry;
	}

	public List<PieceVolume> getPieceVolumes() {
		return this.pieceVolumes;
	}

	public void setPieceVolumes(List<PieceVolume> pieceVolumes) {
		this.pieceVolumes = pieceVolumes;
	}

	public PieceVolume addPieceVolume(PieceVolume pieceVolume) {
		getPieceVolumes().add(pieceVolume);
		pieceVolume.setConDetail(this);

		return pieceVolume;
	}

	public PieceVolume removePieceVolume(PieceVolume pieceVolume) {
		getPieceVolumes().remove(pieceVolume);
		pieceVolume.setConDetail(null);

		return pieceVolume;
	}

	public List<Piece> getPieces() {
		return this.pieces;
	}

	public void setPieces(List<Piece> pieces) {
		this.pieces = pieces;
	}

	public Piece addPiece(Piece piece) {
		getPieces().add(piece);
		piece.setConDetail(this);

		return piece;
	}

	public Piece removePiece(Piece piece) {
		getPieces().remove(piece);
		piece.setConDetail(null);

		return piece;
	}

	public List<PiecesImage> getPiecesImages() {
		return this.piecesImages;
	}

	public void setPiecesImages(List<PiecesImage> piecesImages) {
		this.piecesImages = piecesImages;
	}

	public PiecesImage addPiecesImage(PiecesImage piecesImage) {
		getPiecesImages().add(piecesImage);
		piecesImage.setConDetail(this);

		return piecesImage;
	}

	public PiecesImage removePiecesImage(PiecesImage piecesImage) {
		getPiecesImages().remove(piecesImage);
		piecesImage.setConDetail(null);

		return piecesImage;
	}

	public List<UpdatedEwayBillNumber> getUpdatedEwayBillNumbers() {
		return this.updatedEwayBillNumbers;
	}

	public void setUpdatedEwayBillNumbers(List<UpdatedEwayBillNumber> updatedEwayBillNumbers) {
		this.updatedEwayBillNumbers = updatedEwayBillNumbers;
	}

	public UpdatedEwayBillNumber addUpdatedEwayBillNumber(UpdatedEwayBillNumber updatedEwayBillNumber) {
		getUpdatedEwayBillNumbers().add(updatedEwayBillNumber);
		updatedEwayBillNumber.setConDetail(this);

		return updatedEwayBillNumber;
	}

	public UpdatedEwayBillNumber removeUpdatedEwayBillNumber(UpdatedEwayBillNumber updatedEwayBillNumber) {
		getUpdatedEwayBillNumbers().remove(updatedEwayBillNumber);
		updatedEwayBillNumber.setConDetail(null);

		return updatedEwayBillNumber;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getEwayBillNumber() {
		return ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public String getReceiverGstin() {
		return receiverGstin;
	}

	public void setReceiverGstin(String receiverGstin) {
		this.receiverGstin = receiverGstin;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public List<WstPiecesRefList> getWstPiecesRefList() {
		return wstPiecesRefList;
	}

	public void setWstPiecesRefList(List<WstPiecesRefList> wstPiecesRefList) {
		this.wstPiecesRefList = wstPiecesRefList;
	}

	public List<WstPickupsUpdatedData> getWstPickupsUpdatedData() {
		return wstPickupsUpdatedData;
	}

	public void setWstPickupsUpdatedData(List<WstPickupsUpdatedData> wstPickupsUpdatedData) {
		this.wstPickupsUpdatedData = wstPickupsUpdatedData;
	}

	public List<ModifiedRateCardValue> getModifiedRateCardValue() {
		return modifiedRateCardValue;
	}

	public void setModifiedRateCardValue(List<ModifiedRateCardValue> modifiedRateCardValue) {
		this.modifiedRateCardValue = modifiedRateCardValue;
	}

	public String getCustomerAddress() {
		return customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerGstin() {
		return customerGstin;
	}

	public void setCustomerGstin(String customerGstin) {
		this.customerGstin = customerGstin;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getIsBoxPieceMapping() {
		return isBoxPieceMapping;
	}

	public void setIsBoxPieceMapping(String isBoxPieceMapping) {
		this.isBoxPieceMapping = isBoxPieceMapping;
	}

	public String getPickupUpdatedAddress() {
		return pickupUpdatedAddress;
	}

	public void setPickupUpdatedAddress(String pickupUpdatedAddress) {
		this.pickupUpdatedAddress = pickupUpdatedAddress;
	}

	public String getPickupUpdatedPincode() {
		return pickupUpdatedPincode;
	}

	public void setPickupUpdatedPincode(String pickupUpdatedPincode) {
		this.pickupUpdatedPincode = pickupUpdatedPincode;
	}

	public String getIsPrePrinted() {
		return isPrePrinted;
	}

	public void setIsPrePrinted(String isPrePrinted) {
		this.isPrePrinted = isPrePrinted;
	}

	public int getIsUnloadingSheetGenerated() {
		return isUnloadingSheetGenerated;
	}

	public void setIsUnloadingSheetGenerated(int isUnloadingSheetGenerated) {
		this.isUnloadingSheetGenerated = isUnloadingSheetGenerated;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getStatusFlag() {
		return statusFlag;
	}

	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}

	

	
	
	
	/*public int getIsReverseGeocodingSuccess() {
		return isReverseGeocodingSuccess;
	}

public void setIsReverseGeocodingSuccess(int isReverseGeocodingSuccess) {
		this.isReverseGeocodingSuccess = isReverseGeocodingSuccess;
	}

	public String getGeocodedAddress() {
		return geocodedAddress;
	}

	public void setGeocodedAddress(String geocodedAddress) {
		this.geocodedAddress = geocodedAddress;
	}

	public String getGeocodedPincode() {
		return geocodedPincode;
	}

	public void setGeocodedPincode(String geocodedPincode) {
		this.geocodedPincode = geocodedPincode;
	}*/

	
	
	
}