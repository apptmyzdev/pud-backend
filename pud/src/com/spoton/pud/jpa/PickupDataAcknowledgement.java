package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the pickup_data_acknowledgement database table.
 * 
 */
@Entity
@Table(name="pickup_data_acknowledgement")
@NamedQuery(name="PickupDataAcknowledgement.findAll", query="SELECT p FROM PickupDataAcknowledgement p")
public class PickupDataAcknowledgement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	@Column(name="transaction_remarks")
	private String transactionRemarks;

	@Column(name="transaction_result")
	private String transactionResult;

	//bi-directional many-to-one association to PudData
	@ManyToOne
	@JoinColumn(name="reva_pickup_id")
	private PudData pudData;

	@Column(name="pickup_type")
	private int pickupType;
	
	@Column(name="con_entry_id")
	private int conEntryId;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;
	
	public PickupDataAcknowledgement() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	
	
	
	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public int getConEntryId() {
		return conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getTransactionRemarks() {
		return this.transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public PudData getPudData() {
		return this.pudData;
	}

	public void setPudData(PudData pudData) {
		this.pudData = pudData;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	
}