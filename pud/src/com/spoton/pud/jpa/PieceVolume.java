package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the piece_volume database table.
 * 
 */
@Entity
@Table(name="piece_volume")
@NamedQuery(name="PieceVolume.findAll", query="SELECT p FROM PieceVolume p")
public class PieceVolume implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	
	@Column(name="no_of_pieces")
	private int noOfPieces;

	@Column(name="total_vol_weight")
	private double totalVolWeight;

	@Column(name="vol_breadth")
	private double volBreadth;

	@Column(name="vol_height")
	private double volHeight;

	@Column(name="vol_length")
	private double volLength;
	
	@Column(name="actual_weight_per_piece")
	private double actualWeightPerPiece;
	
	@Column(name="content_type_id")
	private int contentTypeId;

	@Column(name="con_number")
	private String conNumber;
	
	@Column(name="erp_updated")
	private int erpUpdated;
	
	@Column(name="pickup_type")
	private int pickupType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;

	@Column(name="customer_code")
	private String customerCode;
	
	
	//bi-directional many-to-one association to ConDetail
	@ManyToOne
	@JoinColumn(name="con_id")
	private ConDetail conDetail;

	public PieceVolume() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNoOfPieces() {
		return this.noOfPieces;
	}

	public void setNoOfPieces(int noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public double getTotalVolWeight() {
		return this.totalVolWeight;
	}

	public void setTotalVolWeight(double totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}

	public double getVolBreadth() {
		return this.volBreadth;
	}

	public void setVolBreadth(double volBreadth) {
		this.volBreadth = volBreadth;
	}

	public double getVolHeight() {
		return this.volHeight;
	}

	public void setVolHeight(double volHeight) {
		this.volHeight = volHeight;
	}

	public double getVolLength() {
		return this.volLength;
	}

	public void setVolLength(double volLength) {
		this.volLength = volLength;
	}

	public ConDetail getConDetail() {
		return this.conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public double getActualWeightPerPiece() {
		return actualWeightPerPiece;
	}

	public void setActualWeightPerPiece(double actualWeightPerPiece) {
		this.actualWeightPerPiece = actualWeightPerPiece;
	}

	public int getContentTypeId() {
		return contentTypeId;
	}

	public void setContentTypeId(int contentTypeId) {
		this.contentTypeId = contentTypeId;
	}
	
	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public int getErpUpdated() {
		return erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
}