package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the delivery_condition_master database table.
 * 
 */
@Entity
@Table(name="delivery_condition_master")
@NamedQuery(name="DeliveryConditionMaster.findAll", query="SELECT d FROM DeliveryConditionMaster d")
public class DeliveryConditionMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	/*@GeneratedValue(strategy=GenerationType.IDENTITY)*/
	@Column(name="condition_id")
	private int conditionId;

	@Column(name="condition_name")
	private String conditionName;

	//bi-directional many-to-one association to DeliveryUpdation
	@OneToMany(mappedBy="deliveryConditionMaster")
	private List<DeliveryUpdation> deliveryUpdations;

	public DeliveryConditionMaster() {
	}

	public int getConditionId() {
		return this.conditionId;
	}

	public void setConditionId(int conditionId) {
		this.conditionId = conditionId;
	}

	public String getConditionName() {
		return this.conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public List<DeliveryUpdation> getDeliveryUpdations() {
		return this.deliveryUpdations;
	}

	public void setDeliveryUpdations(List<DeliveryUpdation> deliveryUpdations) {
		this.deliveryUpdations = deliveryUpdations;
	}

	public DeliveryUpdation addDeliveryUpdation(DeliveryUpdation deliveryUpdation) {
		getDeliveryUpdations().add(deliveryUpdation);
		deliveryUpdation.setDeliveryConditionMaster(this);

		return deliveryUpdation;
	}

	public DeliveryUpdation removeDeliveryUpdation(DeliveryUpdation deliveryUpdation) {
		getDeliveryUpdations().remove(deliveryUpdation);
		deliveryUpdation.setDeliveryConditionMaster(null);

		return deliveryUpdation;
	}

}