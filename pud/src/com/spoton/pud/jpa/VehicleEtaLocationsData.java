package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the vehicle_eta_cached_data database table.
 * 
 */
@Entity
@Table(name="vehicle_eta_locations_data")
@NamedQuery(name="VehicleEtaLocationsData.findAll", query="SELECT v FROM VehicleEtaLocationsData v")
public class VehicleEtaLocationsData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="destination_latitude")
	private double destinationLatitude;

	@Column(name="destination_longitude")
	private double destinationLongitude;

	private String duration;

	@Column(name="origin_latitude")
	private double originLatitude;

	@Column(name="origin_longitude")
	private double originLongitude;

	public VehicleEtaLocationsData() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public double getDestinationLatitude() {
		return this.destinationLatitude;
	}

	public void setDestinationLatitude(double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public double getDestinationLongitude() {
		return this.destinationLongitude;
	}

	public void setDestinationLongitude(double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public String getDuration() {
		return this.duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public double getOriginLatitude() {
		return this.originLatitude;
	}

	public void setOriginLatitude(double originLatitude) {
		this.originLatitude = originLatitude;
	}

	public double getOriginLongitude() {
		return this.originLongitude;
	}

	public void setOriginLongitude(double originLongitude) {
		this.originLongitude = originLongitude;
	}

}