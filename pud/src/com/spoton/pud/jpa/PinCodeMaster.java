package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pin_code_master database table.
 * 
 */
@Entity
@Table(name="pin_code_master")
@NamedQuery(name="PinCodeMaster.findAll", query="SELECT p FROM PinCodeMaster p")
public class PinCodeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PinCodeMasterPK id;

	private int active;

	@Column(name="active_status")
	private String activeStatus;

	@Column(name="area_id")
	private String areaId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_time")
	private Date createdTime;

	@Column(name="delivery_allowed")
	private int deliveryAllowed;

	private String flag;

	@Column(name="is_air_servicable")
	private String isAirServicable;

	@Column(name="is_air_servicable_del")
	private String isAirServicableDel;

	@Column(name="location_name")
	private String locationName;

	@Column(name="pickup_allowed")
	private int pickupAllowed;

	private String servicable;

	@Column(name="service_type")
	private String serviceType;
	
	@Column(name="region")
	private String region;
	
	@Column(name="depot")
	private String depot;
	
	@Column(name="area")
	private String area;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_time")
	private Date updatedTime;

	private int version;

	@Column(name="row_num")
	private Integer rowNumber;
	
	
	public PinCodeMaster() {
	}

	public PinCodeMasterPK getId() {
		return this.id;
	}

	public void setId(PinCodeMasterPK id) {
		this.id = id;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getActiveStatus() {
		return this.activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getAreaId() {
		return this.areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public int getDeliveryAllowed() {
		return this.deliveryAllowed;
	}

	public void setDeliveryAllowed(int deliveryAllowed) {
		this.deliveryAllowed = deliveryAllowed;
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getIsAirServicable() {
		return this.isAirServicable;
	}

	public void setIsAirServicable(String isAirServicable) {
		this.isAirServicable = isAirServicable;
	}

	public String getIsAirServicableDel() {
		return this.isAirServicableDel;
	}

	public void setIsAirServicableDel(String isAirServicableDel) {
		this.isAirServicableDel = isAirServicableDel;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getPickupAllowed() {
		return this.pickupAllowed;
	}

	public void setPickupAllowed(int pickupAllowed) {
		this.pickupAllowed = pickupAllowed;
	}

	public String getServicable() {
		return this.servicable;
	}

	public void setServicable(String servicable) {
		this.servicable = servicable;
	}

	public String getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Date getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getDepot() {
		return depot;
	}

	public void setDepot(String depot) {
		this.depot = depot;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	
	
}