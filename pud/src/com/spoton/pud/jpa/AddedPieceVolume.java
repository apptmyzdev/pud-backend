package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the added_piece_volume database table.
 * 
 */
@Entity
@Table(name="added_piece_volume")
@NamedQuery(name="AddedPieceVolume.findAll", query="SELECT a FROM AddedPieceVolume a")
public class AddedPieceVolume implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="no_of_pieces")
	private int noOfPieces;

	@Column(name="total_vol_weight")
	private double totalVolWeight;

	@Column(name="vol_breadth")
	private double volBreadth;

	@Column(name="vol_height")
	private double volHeight;

	@Column(name="vol_length")
	private double volLength;
	
	@Column(name="actual_weight_per_piece")
	private double actualWeightPerPiece;
	
	@Column(name="content_type_id")
	private int contentTypeId;

	@Column(name="con_number")
	private String conNumber;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to AddedCon
	@ManyToOne
	@JoinColumn(name="con_id")
	private AddedCon addedCon;
	
	

	public AddedPieceVolume() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public int getNoOfPieces() {
		return this.noOfPieces;
	}

	public void setNoOfPieces(int noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public double getTotalVolWeight() {
		return this.totalVolWeight;
	}

	public void setTotalVolWeight(double totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}

	public double getVolBreadth() {
		return this.volBreadth;
	}

	public void setVolBreadth(double volBreadth) {
		this.volBreadth = volBreadth;
	}

	public double getVolHeight() {
		return this.volHeight;
	}

	public void setVolHeight(double volHeight) {
		this.volHeight = volHeight;
	}

	public double getVolLength() {
		return this.volLength;
	}

	public void setVolLength(double volLength) {
		this.volLength = volLength;
	}

	public AddedCon getAddedCon() {
		return this.addedCon;
	}

	public void setAddedCon(AddedCon addedCon) {
		this.addedCon = addedCon;
	}

	public double getActualWeightPerPiece() {
		return actualWeightPerPiece;
	}

	public void setActualWeightPerPiece(double actualWeightPerPiece) {
		this.actualWeightPerPiece = actualWeightPerPiece;
	}

	public int getContentTypeId() {
		return contentTypeId;
	}

	public void setContentTypeId(int contentTypeId) {
		this.contentTypeId = contentTypeId;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}