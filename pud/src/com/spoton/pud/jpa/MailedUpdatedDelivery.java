package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mailed_updated_deliveries database table.
 * 
 */
@Entity
@Table(name="mailed_updated_deliveries")
@NamedQuery(name="MailedUpdatedDelivery.findAll", query="SELECT m FROM MailedUpdatedDelivery m")
public class MailedUpdatedDelivery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="transaction_id")
	private int transactionId;

	public MailedUpdatedDelivery() {
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

}