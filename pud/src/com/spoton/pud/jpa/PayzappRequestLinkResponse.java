package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the payzapp_request_link_response database table.
 * 
 */
@Entity
@Table(name="payzapp_request_link_response")
@NamedQuery(name="PayzappRequestLinkResponse.findAll", query="SELECT p FROM PayzappRequestLinkResponse p")
public class PayzappRequestLinkResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="customer_code")
	private String customerCode;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="error_msg")
	private String errorMsg;

	@Column(name="error_status")
	private String errorStatus;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="mobile_number")
	private String mobileNumber;

	@Column(name="payzapp_request_link_input_id")
	private int payzappRequestLinkInputId;

	@Column(name="ptms_email")
	private String ptmsEmail;

	@Column(name="response_remarks")
	private String responseRemarks;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	@Column(name="total_invoice_amount")
	private double totalInvoiceAmount;

	public PayzappRequestLinkResponse() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getErrorMsg() {
		return this.errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorStatus() {
		return this.errorStatus;
	}

	public void setErrorStatus(String errorStatus) {
		this.errorStatus = errorStatus;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public int getPayzappRequestLinkInputId() {
		return this.payzappRequestLinkInputId;
	}

	public void setPayzappRequestLinkInputId(int payzappRequestLinkInputId) {
		this.payzappRequestLinkInputId = payzappRequestLinkInputId;
	}

	public String getPtmsEmail() {
		return this.ptmsEmail;
	}

	public void setPtmsEmail(String ptmsEmail) {
		this.ptmsEmail = ptmsEmail;
	}

	public String getResponseRemarks() {
		return this.responseRemarks;
	}

	public void setResponseRemarks(String responseRemarks) {
		this.responseRemarks = responseRemarks;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getTotalInvoiceAmount() {
		return this.totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(double totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}

}