package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the device_apk_versions database table.
 * 
 */
@Entity
@Table(name="device_apk_versions")
@NamedQuery(name="DeviceApkVersion.findAll", query="SELECT d FROM DeviceApkVersion d")
public class DeviceApkVersion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int version;

	private String notes;

	@Column(name="updated_time")
	private Timestamp updatedTime;

	private String url;

	public DeviceApkVersion() {
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}