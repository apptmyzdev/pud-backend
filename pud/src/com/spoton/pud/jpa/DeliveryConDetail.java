package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the delivery_con_details database table.
 * 
 */
@Entity
@Table(name="delivery_con_details")
@NamedQuery(name="DeliveryConDetail.findAll", query="SELECT d FROM DeliveryConDetail d")
public class DeliveryConDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="actual_weight")
	private double actualWeight;

	private double cgst;

	@Column(name="charge_amount")
	private double chargeAmount;

	@Column(name="charge_weight")
	private double chargeWeight;

	private long con;

	@Column(name="con_wt")
	private BigDecimal conWt;

	@Column(name="consignee_address")
	private String consigneeAddress;

	@Column(name="consignee_city")
	private String consigneeCity;

	@Column(name="consignee_company")
	private String consigneeCompany;

	@Column(name="consignee_emailid")
	private String consigneeEmailid;

	@Column(name="consignee_mobile")
	private String consigneeMobile;

	@Column(name="consignee_name")
	private String consigneeName;

	@Column(name="consignee_pincode")
	private String consigneePincode;

	@Column(name="consignor_city")
	private String consignorCity;

	@Column(name="consignor_email")
	private String consignorEmail;

	@Column(name="consignor_mobile")
	private String consignorMobile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Column(name="customer_gst")
	private String customerGst;

	@Column(name="destination_code")
	private String destinationCode;

	private double igst;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="invoice_date")
	private Date invoiceDate;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="is_already_paid")
	private int isAlreadyPaid;

	@Column(name="is_drs_acknowledged")
	private int isDrsAcknowledged;

	@Column(name="is_ftc")
	private int isFtc;

	@Column(name="is_oda")
	private int isOda;

	private double latitude;

	private double longitude;

	@Column(name="net_amount")
	private double netAmount;

	@Column(name="oda_days_validity")
	private int odaDaysValidity;

	@Column(name="origin_code")
	private String originCode;

	@Column(name="payment_basis")
	private int paymentBasis;

	@Column(name="pcs_count")
	private int pcsCount;

	private String pdc;

	@Column(name="receiver_customer_code")
	private String receiverCustomerCode;

	private String ref1;

	private String ref2;

	private double sgst;

	@Column(name="spoton_gst_state_name")
	private String spotonGSTStateName;
	
	@Column(name="shipper_gst_state_name")
	private String shipperGSTStateName;

	@Column(name="spoton_address")
	private String spotonAddress;

	@Column(name="spoton_gst")
	private String spotonGst;

	

	@Column(name="tax_amount")
	private double taxAmount;

	private double ugst;

	@Column(name="updated_flag")
	private int updatedFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="valid_till")
	private Date validTill;

	@Column(name="vehicle_capacity")
	private String vehicleCapacity;

	@Column(name="vehicle_number")
	private String vehicleNumber;

	@Column(name="vehicle_type")
	private String vehicleType;
	
	@Column(name="agent_id")
	private String agentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pulled_time")
	private Date pulledTime;

	//bi-directional one-to-one association to DrsAcknowledgedDeliveryCon
	@OneToOne(mappedBy="deliveryConDetail")
	private DrsAcknowledgedDeliveryCon drsAcknowledgedDeliveryCon;

	public DeliveryConDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getActualWeight() {
		return this.actualWeight;
	}

	public void setActualWeight(double actualWeight) {
		this.actualWeight = actualWeight;
	}

	public double getCgst() {
		return this.cgst;
	}

	public void setCgst(double cgst) {
		this.cgst = cgst;
	}

	public double getChargeAmount() {
		return this.chargeAmount;
	}

	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public double getChargeWeight() {
		return this.chargeWeight;
	}

	public void setChargeWeight(double chargeWeight) {
		this.chargeWeight = chargeWeight;
	}

	public long getCon() {
		return this.con;
	}

	public void setCon(long con) {
		this.con = con;
	}

	public BigDecimal getConWt() {
		return this.conWt;
	}

	public void setConWt(BigDecimal conWt) {
		this.conWt = conWt;
	}

	public String getConsigneeAddress() {
		return this.consigneeAddress;
	}

	public void setConsigneeAddress(String consigneeAddress) {
		this.consigneeAddress = consigneeAddress;
	}

	public String getConsigneeCity() {
		return this.consigneeCity;
	}

	public void setConsigneeCity(String consigneeCity) {
		this.consigneeCity = consigneeCity;
	}

	public String getConsigneeCompany() {
		return this.consigneeCompany;
	}

	public void setConsigneeCompany(String consigneeCompany) {
		this.consigneeCompany = consigneeCompany;
	}

	public String getConsigneeEmailid() {
		return this.consigneeEmailid;
	}

	public void setConsigneeEmailid(String consigneeEmailid) {
		this.consigneeEmailid = consigneeEmailid;
	}

	public String getConsigneeMobile() {
		return this.consigneeMobile;
	}

	public void setConsigneeMobile(String consigneeMobile) {
		this.consigneeMobile = consigneeMobile;
	}

	public String getConsigneeName() {
		return this.consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public String getConsigneePincode() {
		return this.consigneePincode;
	}

	public void setConsigneePincode(String consigneePincode) {
		this.consigneePincode = consigneePincode;
	}

	public String getConsignorCity() {
		return this.consignorCity;
	}

	public void setConsignorCity(String consignorCity) {
		this.consignorCity = consignorCity;
	}

	public String getConsignorEmail() {
		return this.consignorEmail;
	}

	public void setConsignorEmail(String consignorEmail) {
		this.consignorEmail = consignorEmail;
	}

	public String getConsignorMobile() {
		return this.consignorMobile;
	}

	public void setConsignorMobile(String consignorMobile) {
		this.consignorMobile = consignorMobile;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustomerGst() {
		return this.customerGst;
	}

	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}

	public String getDestinationCode() {
		return this.destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	public double getIgst() {
		return this.igst;
	}

	public void setIgst(double igst) {
		this.igst = igst;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public int getIsAlreadyPaid() {
		return this.isAlreadyPaid;
	}

	public void setIsAlreadyPaid(int isAlreadyPaid) {
		this.isAlreadyPaid = isAlreadyPaid;
	}

	public int getIsDrsAcknowledged() {
		return this.isDrsAcknowledged;
	}

	public void setIsDrsAcknowledged(int isDrsAcknowledged) {
		this.isDrsAcknowledged = isDrsAcknowledged;
	}

	public int getIsFtc() {
		return this.isFtc;
	}

	public void setIsFtc(int isFtc) {
		this.isFtc = isFtc;
	}

	public int getIsOda() {
		return this.isOda;
	}

	public void setIsOda(int isOda) {
		this.isOda = isOda;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(double netAmount) {
		this.netAmount = netAmount;
	}

	public int getOdaDaysValidity() {
		return this.odaDaysValidity;
	}

	public void setOdaDaysValidity(int odaDaysValidity) {
		this.odaDaysValidity = odaDaysValidity;
	}

	public String getOriginCode() {
		return this.originCode;
	}

	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}

	public int getPaymentBasis() {
		return this.paymentBasis;
	}

	public void setPaymentBasis(int paymentBasis) {
		this.paymentBasis = paymentBasis;
	}

	public int getPcsCount() {
		return this.pcsCount;
	}

	public void setPcsCount(int pcsCount) {
		this.pcsCount = pcsCount;
	}

	public String getPdc() {
		return this.pdc;
	}

	public void setPdc(String pdc) {
		this.pdc = pdc;
	}

	public String getReceiverCustomerCode() {
		return this.receiverCustomerCode;
	}

	public void setReceiverCustomerCode(String receiverCustomerCode) {
		this.receiverCustomerCode = receiverCustomerCode;
	}

	public String getRef1() {
		return this.ref1;
	}

	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}

	public String getRef2() {
		return this.ref2;
	}

	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}

	public double getSgst() {
		return this.sgst;
	}

	public void setSgst(double sgst) {
		this.sgst = sgst;
	}

	

	public String getSpotonAddress() {
		return this.spotonAddress;
	}

	public void setSpotonAddress(String spotonAddress) {
		this.spotonAddress = spotonAddress;
	}

	public String getSpotonGst() {
		return this.spotonGst;
	}

	public void setSpotonGst(String spotonGst) {
		this.spotonGst = spotonGst;
	}

	
	
	public String getSpotonGSTStateName() {
		return spotonGSTStateName;
	}

	public void setSpotonGSTStateName(String spotonGSTStateName) {
		this.spotonGSTStateName = spotonGSTStateName;
	}

	public String getShipperGSTStateName() {
		return shipperGSTStateName;
	}

	public void setShipperGSTStateName(String shipperGSTStateName) {
		this.shipperGSTStateName = shipperGSTStateName;
	}

	public double getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getUgst() {
		return this.ugst;
	}

	public void setUgst(double ugst) {
		this.ugst = ugst;
	}

	public int getUpdatedFlag() {
		return this.updatedFlag;
	}

	public void setUpdatedFlag(int updatedFlag) {
		this.updatedFlag = updatedFlag;
	}

	public Date getValidTill() {
		return this.validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public String getVehicleCapacity() {
		return this.vehicleCapacity;
	}

	public void setVehicleCapacity(String vehicleCapacity) {
		this.vehicleCapacity = vehicleCapacity;
	}

	public String getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public DrsAcknowledgedDeliveryCon getDrsAcknowledgedDeliveryCon() {
		return this.drsAcknowledgedDeliveryCon;
	}

	public void setDrsAcknowledgedDeliveryCon(DrsAcknowledgedDeliveryCon drsAcknowledgedDeliveryCon) {
		this.drsAcknowledgedDeliveryCon = drsAcknowledgedDeliveryCon;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Date getPulledTime() {
		return pulledTime;
	}

	public void setPulledTime(Date pulledTime) {
		this.pulledTime = pulledTime;
	}

	
}