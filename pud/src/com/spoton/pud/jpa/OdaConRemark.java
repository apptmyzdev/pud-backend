/*
 * Decompiled with CFR 0_123.
 * 
 * Could not load the following classes:
 *  javax.persistence.Column
 *  javax.persistence.Entity
 *  javax.persistence.GeneratedValue
 *  javax.persistence.GenerationType
 *  javax.persistence.Id
 *  javax.persistence.NamedQuery
 *  javax.persistence.Table
 *  javax.persistence.Temporal
 *  javax.persistence.TemporalType
 */
package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="oda_con_remarks")
@NamedQuery(name="OdaConRemark.findAll", query="SELECT o FROM OdaConRemark o")
public class OdaConRemark implements Serializable {
    private static final long serialVersionUID = 1;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="transaction_id")
    private int transactionId;
    @Column(name="con_number")
    private String conNumber;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="created_timestamp")
    private Date createdTimestamp;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="erp_response_timestamp")
    private Date erpResponseTimestamp;
    private float latitude;
    private float longitude;
    private String pdc;
    private String remarks;
    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="remarks_date")
    private Date remarksDate;
    @Column(name="transaction_message")
    private String transactionMessage;
    @Column(name="transaction_result")
    private int transactionResult;
    @Column(name="user_id")
    private String userId;

    public int getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(int transactionId) {
        this.transactionId = transactionId;
    }

    public String getConNumber() {
        return this.conNumber;
    }

    public void setConNumber(String conNumber) {
        this.conNumber = conNumber;
    }

    public Date getCreatedTimestamp() {
        return this.createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public Date getErpResponseTimestamp() {
        return this.erpResponseTimestamp;
    }

    public void setErpResponseTimestamp(Date erpResponseTimestamp) {
        this.erpResponseTimestamp = erpResponseTimestamp;
    }

    public float getLatitude() {
        return this.latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return this.longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getPdc() {
        return this.pdc;
    }

    public void setPdc(String pdc) {
        this.pdc = pdc;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getRemarksDate() {
        return this.remarksDate;
    }

    public void setRemarksDate(Date remarksDate) {
        this.remarksDate = remarksDate;
    }

    public String getTransactionMessage() {
        return this.transactionMessage;
    }

    public void setTransactionMessage(String transactionMessage) {
        this.transactionMessage = transactionMessage;
    }

    public int getTransactionResult() {
        return this.transactionResult;
    }

    public void setTransactionResult(int transactionResult) {
        this.transactionResult = transactionResult;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
