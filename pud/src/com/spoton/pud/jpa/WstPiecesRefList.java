package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the wst_pieces_ref_list database table.
 * 
 */
@Entity
@Table(name="wst_pieces_ref_list")
@NamedQuery(name="WstPiecesRefList.findAll", query="SELECT w FROM WstPiecesRefList w")
public class WstPiecesRefList implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to ConDetail
		@ManyToOne
		@JoinColumn(name="con_id")
		private ConDetail conDetail;

	@Column(name="con_number")
	private String conNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="piece_number")
	private String pieceNumber;

	@Column(name="piece_ref_number")
	private String pieceRefNumber;

	@Column(name="piece_serial_number")
	private String pieceSerialNumber;
	
	@Column(name="erp_updated")
	private int erpUpdated;
	
	@Column(name="pickup_type")
	private int pickupType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;

	public WstPiecesRefList() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public ConDetail getConDetail() {
		return conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getPieceNumber() {
		return this.pieceNumber;
	}

	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}

	public String getPieceRefNumber() {
		return this.pieceRefNumber;
	}

	public void setPieceRefNumber(String pieceRefNumber) {
		this.pieceRefNumber = pieceRefNumber;
	}

	public String getPieceSerialNumber() {
		return this.pieceSerialNumber;
	}

	public void setPieceSerialNumber(String pieceSerialNumber) {
		this.pieceSerialNumber = pieceSerialNumber;
	}

	public int getErpUpdated() {
		return erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	
}