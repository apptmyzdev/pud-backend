package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the device_info database table.
 * 
 */
@Entity
@Table(name="device_info")
@NamedQuery(name="DeviceInfo.findAll", query="SELECT d FROM DeviceInfo d")
public class DeviceInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="device_id")
	private int deviceId;

	@Column(name="active_flag")
	private int activeFlag;

	@Column(name="app_version")
	private String appVersion;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="device_available_memory")
	private String deviceAvailableMemory;

	@Column(name="device_battery")
	private String deviceBattery;

	@Column(name="device_brand")
	private String deviceBrand;

	@Column(name="device_imei")
	private String deviceImei;

	@Column(name="device_mac")
	private String deviceMac;

	@Column(name="device_mobile_number")
	private String deviceMobileNumber;

	@Column(name="device_model")
	private String deviceModel;

	@Column(name="device_name")
	private String deviceName;

	@Column(name="device_os_version")
	private String deviceOsVersion;

	@Column(name="device_service_provider")
	private String deviceServiceProvider;

	@Column(name="device_total_memory")
	private String deviceTotalMemory;

	@Column(name="gps_status")
	private int gpsStatus;

	private String pushToken;

	@Column(name="sim_card")
	private String simCard;

	private String uniqueKey;

	@Column(name="updated_ip_address")
	private String updatedIpAddress;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_timestamp")
	private Date updatedTimestamp;

	@Column(name="user_id")
	private String userId;

	//bi-directional many-to-one association to UserLocation
	@OneToMany(mappedBy="deviceInfo")
	private List<UserLocation> userLocations;
	
	/*//bi-directional many-to-one association to GeoFenceUpdate
	@OneToMany(mappedBy="deviceInfo")
	private List<GeoFenceUpdate> geoFenceUpdates;*/

/*	//bi-directional one-to-one association to GeofenceDeviceMap
	@OneToOne(mappedBy="deviceInfo")
	private GeofenceDeviceMap geofenceDeviceMap;*/

	public DeviceInfo() {
	}

	public int getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAppVersion() {
		return this.appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getDeviceAvailableMemory() {
		return this.deviceAvailableMemory;
	}

	public void setDeviceAvailableMemory(String deviceAvailableMemory) {
		this.deviceAvailableMemory = deviceAvailableMemory;
	}

	public String getDeviceBattery() {
		return this.deviceBattery;
	}

	public void setDeviceBattery(String deviceBattery) {
		this.deviceBattery = deviceBattery;
	}

	public String getDeviceBrand() {
		return this.deviceBrand;
	}

	public void setDeviceBrand(String deviceBrand) {
		this.deviceBrand = deviceBrand;
	}

	public String getDeviceImei() {
		return this.deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getDeviceMac() {
		return this.deviceMac;
	}

	public void setDeviceMac(String deviceMac) {
		this.deviceMac = deviceMac;
	}

	public String getDeviceMobileNumber() {
		return this.deviceMobileNumber;
	}

	public void setDeviceMobileNumber(String deviceMobileNumber) {
		this.deviceMobileNumber = deviceMobileNumber;
	}

	public String getDeviceModel() {
		return this.deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getDeviceName() {
		return this.deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceOsVersion() {
		return this.deviceOsVersion;
	}

	public void setDeviceOsVersion(String deviceOsVersion) {
		this.deviceOsVersion = deviceOsVersion;
	}

	public String getDeviceServiceProvider() {
		return this.deviceServiceProvider;
	}

	public void setDeviceServiceProvider(String deviceServiceProvider) {
		this.deviceServiceProvider = deviceServiceProvider;
	}

	public String getDeviceTotalMemory() {
		return this.deviceTotalMemory;
	}

	public void setDeviceTotalMemory(String deviceTotalMemory) {
		this.deviceTotalMemory = deviceTotalMemory;
	}

	public int getGpsStatus() {
		return this.gpsStatus;
	}

	public void setGpsStatus(int gpsStatus) {
		this.gpsStatus = gpsStatus;
	}

	public String getPushToken() {
		return this.pushToken;
	}

	public void setPushToken(String pushToken) {
		this.pushToken = pushToken;
	}

	public String getSimCard() {
		return this.simCard;
	}

	public void setSimCard(String simCard) {
		this.simCard = simCard;
	}

	public String getUniqueKey() {
		return this.uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public String getUpdatedIpAddress() {
		return this.updatedIpAddress;
	}

	public void setUpdatedIpAddress(String updatedIpAddress) {
		this.updatedIpAddress = updatedIpAddress;
	}

	public Date getUpdatedTimestamp() {
		return this.updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<UserLocation> getUserLocations() {
		return this.userLocations;
	}

	public void setUserLocations(List<UserLocation> userLocations) {
		this.userLocations = userLocations;
	}

	public UserLocation addUserLocation(UserLocation userLocation) {
		getUserLocations().add(userLocation);
		userLocation.setDeviceInfo(this);

		return userLocation;
	}

	public UserLocation removeUserLocation(UserLocation userLocation) {
		getUserLocations().remove(userLocation);
		userLocation.setDeviceInfo(null);

		return userLocation;
	}

/*	public List<GeoFenceUpdate> getGeoFenceUpdates() {
		return this.geoFenceUpdates;
	}

	public void setGeoFenceUpdates(List<GeoFenceUpdate> geoFenceUpdates) {
		this.geoFenceUpdates = geoFenceUpdates;
	}*/

	/*public GeoFenceUpdate addGeoFenceUpdate(GeoFenceUpdate geoFenceUpdate) {
		getGeoFenceUpdates().add(geoFenceUpdate);
		geoFenceUpdate.setDeviceInfo(this);

		return geoFenceUpdate;
	}

	public GeoFenceUpdate removeGeoFenceUpdate(GeoFenceUpdate geoFenceUpdate) {
		getGeoFenceUpdates().remove(geoFenceUpdate);
		geoFenceUpdate.setDeviceInfo(null);

		return geoFenceUpdate;
	}*/

	/*public GeofenceDeviceMap getGeofenceDeviceMap() {
		return this.geofenceDeviceMap;
	}

	public void setGeofenceDeviceMap(GeofenceDeviceMap geofenceDeviceMap) {
		this.geofenceDeviceMap = geofenceDeviceMap;
	}*/


}