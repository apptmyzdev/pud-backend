package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the box_piece_mapping_details database table.
 * 
 */
@Entity
@Table(name="box_piece_mapping_details")
@NamedQuery(name="BoxPieceMappingDetail.findAll", query="SELECT b FROM BoxPieceMappingDetail b")
public class BoxPieceMappingDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="box_number")
	private String boxNumber;

	@Column(name="con_number")
	private String conNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="erp_updated")
	private int erpUpdated;

	@Column(name="is_box_piece_mapping")
	private String isBoxPieceMapping;

	@Column(name="piece_number")
	private String pieceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="scanned_datetime")
	private Date scannedDatetime;

	@Column(name="transaction_messge")
	private String transactionMessge;

	@Column(name="transaction_result")
	private String transactionResult;

	@Column(name="user_id")
	private String userId;

	public BoxPieceMappingDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBoxNumber() {
		return this.boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public int getErpUpdated() {
		return this.erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public String getIsBoxPieceMapping() {
		return this.isBoxPieceMapping;
	}

	public void setIsBoxPieceMapping(String isBoxPieceMapping) {
		this.isBoxPieceMapping = isBoxPieceMapping;
	}

	public String getPieceNumber() {
		return this.pieceNumber;
	}

	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}

	public Date getScannedDatetime() {
		return this.scannedDatetime;
	}

	public void setScannedDatetime(Date scannedDatetime) {
		this.scannedDatetime = scannedDatetime;
	}

	public String getTransactionMessge() {
		return this.transactionMessge;
	}

	public void setTransactionMessge(String transactionMessge) {
		this.transactionMessge = transactionMessge;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}