package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the current_updation_status database table.
 * 
 */
@Entity
@Table(name="current_updation_status")
@NamedQuery(name="CurrentUpdationStatus.findAll", query="SELECT c FROM CurrentUpdationStatus c")
public class CurrentUpdationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="apk_version")
	private String apkVersion;

	@Column(name="device_imei")
	private String deviceImei;

	@Column(name="pickup_schedule_idorcon_number")
	private String pickupScheduleId_conNumber;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	private int type;

	private String username;

	public CurrentUpdationStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApkVersion() {
		return this.apkVersion;
	}

	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}

	public String getDeviceImei() {
		return this.deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getPickupScheduleId_conNumber() {
		return this.pickupScheduleId_conNumber;
	}

	public void setPickupScheduleId_conNumber(String pickupScheduleId_conNumber) {
		this.pickupScheduleId_conNumber = pickupScheduleId_conNumber;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}