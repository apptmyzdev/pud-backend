package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the wst_pickups_updated_data database table.
 * 
 */
@Entity
@Table(name="wst_pickups_updated_data")
@NamedQuery(name="WstPickupsUpdatedData.findAll", query="SELECT w FROM WstPickupsUpdatedData w")
public class WstPickupsUpdatedData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	

	@Column(name="con_number")
	private String conNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="is_manual")
	private String isManual;

	@Column(name="is_printed")
	private String isPrinted;

	@Column(name="piece_number")
	private String pieceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="printed_date_time")
	private Date printedDateTime;

	@Column(name="user_id")
	private String userId;
	
	//bi-directional many-to-one association to ConDetail
			@ManyToOne
			@JoinColumn(name="con_entry_id")
			private ConDetail conDetail;

	public WstPickupsUpdatedData() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	
	public ConDetail getConDetail() {
		return conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getIsManual() {
		return this.isManual;
	}

	public void setIsManual(String isManual) {
		this.isManual = isManual;
	}

	public String getIsPrinted() {
		return this.isPrinted;
	}

	public void setIsPrinted(String isPrinted) {
		this.isPrinted = isPrinted;
	}

	public String getPieceNumber() {
		return this.pieceNumber;
	}

	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}

	public Date getPrintedDateTime() {
		return this.printedDateTime;
	}

	public void setPrintedDateTime(Date printedDateTime) {
		this.printedDateTime = printedDateTime;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}