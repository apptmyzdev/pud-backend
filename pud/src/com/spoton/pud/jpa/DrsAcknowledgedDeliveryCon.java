package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the drs_acknowledged_delivery_cons database table.
 * 
 */
@Entity
@Table(name="drs_acknowledged_delivery_cons")
@NamedQuery(name="DrsAcknowledgedDeliveryCon.findAll", query="SELECT d FROM DrsAcknowledgedDeliveryCon d")
public class DrsAcknowledgedDeliveryCon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	
	@Column(name="transaction_id")
	private int transactionId;

	@Column(name="con_number")
	private String conNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="is_ok")
	private String isOk;

	@Column(name="pdc_number")
	private String pdcNumber;

	private String remarks;

	@Column(name="transaction_meaage")
	private String transactionMeaage;

	@Column(name="transaction_result")
	private String transactionResult;

	//bi-directional one-to-one association to DeliveryConDetail
	@OneToOne
	@JoinColumn(name="transaction_id",insertable=false, updatable=false)
	private DeliveryConDetail deliveryConDetail;

	public DrsAcknowledgedDeliveryCon() {
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getIsOk() {
		return this.isOk;
	}

	public void setIsOk(String isOk) {
		this.isOk = isOk;
	}

	public String getPdcNumber() {
		return this.pdcNumber;
	}

	public void setPdcNumber(String pdcNumber) {
		this.pdcNumber = pdcNumber;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTransactionMeaage() {
		return this.transactionMeaage;
	}

	public void setTransactionMeaage(String transactionMeaage) {
		this.transactionMeaage = transactionMeaage;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	/*public DeliveryConDetail getDeliveryConDetail() {
		return this.deliveryConDetail;
	}

	public void setDeliveryConDetail(DeliveryConDetail deliveryConDetail) {
		this.deliveryConDetail = deliveryConDetail;
	}
*/
}