package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pickup_reassigned database table.
 * 
 */
@Entity
@Table(name="pickup_reassigned")
@NamedQuery(name="PickupReassigned.findAll", query="SELECT p FROM PickupReassigned p")
public class PickupReassigned implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="gps_value_lat")
	private double gpsValueLat;

	@Column(name="gps_value_lon")
	private double gpsValueLon;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	@Column(name="pickup_type")
	private String pickupType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reassign_datetime")
	private Date reassignDatetime;

	@Column(name="ref_number")
	private String refNumber;

	private String remarks;

	@Column(name="to_user_id")
	private String toUserId;

	@Column(name="transaction_result")
	private String transactionResult;

	@Column(name="user_id")
	private String userId;

	public PickupReassigned() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public double getGpsValueLat() {
		return this.gpsValueLat;
	}

	public void setGpsValueLat(double gpsValueLat) {
		this.gpsValueLat = gpsValueLat;
	}

	public double getGpsValueLon() {
		return this.gpsValueLon;
	}

	public void setGpsValueLon(double gpsValueLon) {
		this.gpsValueLon = gpsValueLon;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getPickupType() {
		return this.pickupType;
	}

	public void setPickupType(String pickupType) {
		this.pickupType = pickupType;
	}

	public Date getReassignDatetime() {
		return this.reassignDatetime;
	}

	public void setReassignDatetime(Date reassignDatetime) {
		this.reassignDatetime = reassignDatetime;
	}

	public String getRefNumber() {
		return this.refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getToUserId() {
		return this.toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}