package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the delivery_updation database table.
 * 
 */
@Entity
@Table(name="delivery_updation")
@NamedQuery(name="DeliveryUpdation.findAll", query="SELECT d FROM DeliveryUpdation d")
public class DeliveryUpdation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="transaction_id")
	private int transactionId;

	@Column(name="awb_no")
	private String awbNo;

	@Column(name="bank_branch")
	private String bankBranch;
	
	@Column(name="shipment_type")
	private String shipmentType;

	@Column(name="bank_name")
	private String bankName;

	@Column(name="cheque_amount")
	private float chequeAmount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="cheque_date")
	private Date chequeDate;

	@Column(name="cheque_img1")
	private String chequeImg1;

	@Column(name="cheque_img2")
	private String chequeImg2;

	@Column(name="cheque_number")
	private String chequeNumber;

	@Column(name="collected_amount")
	private float collectedAmount;

	@Column(name="collection_type")
	private String collectionType;

	@Column(name="consignee_name")
	private String consigneeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_time")
	private Date createdTime;

	@Column(name="delivered_to")
	private String deliveredTo;

	@Column(name="erp_updated")
	private int erpUpdated;
	
	@Column(name="is_ftc_payment_docket")
	private int isFtcPaymentDocket;

	@Column(name="field_employee_name")
	private String fieldEmployeeName;

	@Column(name="image_url")
	private String imageUrl;
	
	@Column(name="delivery_updated_address")
	private String deliveryUpdatedAddress;

	@Column(name="delivery_updated_pincode")
	private String deliveryUpdatedPincode;


	@Column(name="is_ftc")
	private String isFtc;

	@Column(name="is_tds_deducted")
	private String isTdsDeducted;

	@Column(name="is_valid_pod")
	private String isValidPod;

	@Column(name="lat_vlue")
	private double latVlue;

	@Column(name="long_value")
	private double longValue;

	@Column(name="payment_type")
	private String paymentType;

	@Column(name="pdc_number")
	private String pdcNumber;

	private String reason;

	@Column(name="receiver_mobile_no")
	private String receiverMobileNo;
	
	@Column(name="tan_number")
	private String tanNumber;

	@Column(name="customer_name")
	private String customerName;

	private String relationship;

	private String remarks;

	@Column(name="signature_url")
	private String signatureUrl;
	
	
	@Column(name="is_reverse_geocoding_success")
	private int isReverseGeocodingSuccess;

	@Column(name="geocoded_address")
	private String geocodedAddress;
	
	@Column(name="geocoded_pincode")
	private String geocodedPincode;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="transaction_date")
	private Date transactionDate;
	
	@Column(name="transaction_remarks")
	private String transactionRemarks;

	@Column(name="transaction_result")
	private String transactionResult;

	//bi-directional many-to-one association to DeliveryConditionImage
	@OneToMany(mappedBy="deliveryUpdation")
	private List<DeliveryConditionImage> deliveryConditionImages;

	//bi-directional many-to-one association to DeliveryConditionMaster
	@ManyToOne
	@JoinColumn(name="delivery_condition_id")
	private DeliveryConditionMaster deliveryConditionMaster;

	@ManyToOne
	@JoinColumn(name="delivery_sub_condition_id")
	private DeliveryConditionSubMaster deliveryConditionSubMaster;

	
	public DeliveryUpdation() {
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getAwbNo() {
		return this.awbNo;
	}

	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	public String getBankBranch() {
		return this.bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public float getChequeAmount() {
		return this.chequeAmount;
	}

	public void setChequeAmount(float chequeAmount) {
		this.chequeAmount = chequeAmount;
	}

	public Date getChequeDate() {
		return this.chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getChequeImg1() {
		return this.chequeImg1;
	}

	public void setChequeImg1(String chequeImg1) {
		this.chequeImg1 = chequeImg1;
	}

	public String getChequeImg2() {
		return this.chequeImg2;
	}

	public void setChequeImg2(String chequeImg2) {
		this.chequeImg2 = chequeImg2;
	}

	public String getChequeNumber() {
		return this.chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public float getCollectedAmount() {
		return this.collectedAmount;
	}

	public void setCollectedAmount(float collectedAmount) {
		this.collectedAmount = collectedAmount;
	}

	public String getCollectionType() {
		return this.collectionType;
	}

	public void setCollectionType(String collectionType) {
		this.collectionType = collectionType;
	}

	public String getConsigneeName() {
		return this.consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getDeliveredTo() {
		return this.deliveredTo;
	}

	public void setDeliveredTo(String deliveredTo) {
		this.deliveredTo = deliveredTo;
	}

	public int getErpUpdated() {
		return this.erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public String getFieldEmployeeName() {
		return this.fieldEmployeeName;
	}

	public void setFieldEmployeeName(String fieldEmployeeName) {
		this.fieldEmployeeName = fieldEmployeeName;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getIsFtc() {
		return this.isFtc;
	}

	public void setIsFtc(String isFtc) {
		this.isFtc = isFtc;
	}

	public String getIsTdsDeducted() {
		return this.isTdsDeducted;
	}

	public void setIsTdsDeducted(String isTdsDeducted) {
		this.isTdsDeducted = isTdsDeducted;
	}

	public String getIsValidPod() {
		return this.isValidPod;
	}

	public void setIsValidPod(String isValidPod) {
		this.isValidPod = isValidPod;
	}

	public double getLatVlue() {
		return this.latVlue;
	}

	public void setLatVlue(double latVlue) {
		this.latVlue = latVlue;
	}

	public double getLongValue() {
		return this.longValue;
	}

	public void setLongValue(double longValue) {
		this.longValue = longValue;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getPdcNumber() {
		return this.pdcNumber;
	}

	public void setPdcNumber(String pdcNumber) {
		this.pdcNumber = pdcNumber;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getReceiverMobileNo() {
		return this.receiverMobileNo;
	}

	public void setReceiverMobileNo(String receiverMobileNo) {
		this.receiverMobileNo = receiverMobileNo;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSignatureUrl() {
		return this.signatureUrl;
	}

	public void setSignatureUrl(String signatureUrl) {
		this.signatureUrl = signatureUrl;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public List<DeliveryConditionImage> getDeliveryConditionImages() {
		return this.deliveryConditionImages;
	}

	public void setDeliveryConditionImages(List<DeliveryConditionImage> deliveryConditionImages) {
		this.deliveryConditionImages = deliveryConditionImages;
	}

	public DeliveryConditionImage addDeliveryConditionImage(DeliveryConditionImage deliveryConditionImage) {
		getDeliveryConditionImages().add(deliveryConditionImage);
		deliveryConditionImage.setDeliveryUpdation(this);

		return deliveryConditionImage;
	}

	public DeliveryConditionImage removeDeliveryConditionImage(DeliveryConditionImage deliveryConditionImage) {
		getDeliveryConditionImages().remove(deliveryConditionImage);
		deliveryConditionImage.setDeliveryUpdation(null);

		return deliveryConditionImage;
	}

	public DeliveryConditionMaster getDeliveryConditionMaster() {
		return this.deliveryConditionMaster;
	}

	public void setDeliveryConditionMaster(DeliveryConditionMaster deliveryConditionMaster) {
		this.deliveryConditionMaster = deliveryConditionMaster;
	}

	public String getTanNumber() {
		return tanNumber;
	}

	public void setTanNumber(String tanNumber) {
		this.tanNumber = tanNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getIsFtcPaymentDocket() {
		return isFtcPaymentDocket;
	}

	public void setIsFtcPaymentDocket(int isFtcPaymentDocket) {
		this.isFtcPaymentDocket = isFtcPaymentDocket;
	}

	public int getIsReverseGeocodingSuccess() {
		return isReverseGeocodingSuccess;
	}

	public void setIsReverseGeocodingSuccess(int isReverseGeocodingSuccess) {
		this.isReverseGeocodingSuccess = isReverseGeocodingSuccess;
	}

	public String getGeocodedAddress() {
		return geocodedAddress;
	}

	public void setGeocodedAddress(String geocodedAddress) {
		this.geocodedAddress = geocodedAddress;
	}

	public String getGeocodedPincode() {
		return geocodedPincode;
	}

	public void setGeocodedPincode(String geocodedPincode) {
		this.geocodedPincode = geocodedPincode;
	}

	public String getShipmentType() {
		return shipmentType;
	}

	public void setShipmentType(String shipmentType) {
		this.shipmentType = shipmentType;
	}

	public String getDeliveryUpdatedAddress() {
		return deliveryUpdatedAddress;
	}

	public void setDeliveryUpdatedAddress(String deliveryUpdatedAddress) {
		this.deliveryUpdatedAddress = deliveryUpdatedAddress;
	}

	public String getDeliveryUpdatedPincode() {
		return deliveryUpdatedPincode;
	}

	public void setDeliveryUpdatedPincode(String deliveryUpdatedPincode) {
		this.deliveryUpdatedPincode = deliveryUpdatedPincode;
	}

	public String getTransactionRemarks() {
		return transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public DeliveryConditionSubMaster getDeliveryConditionSubMaster() {
		return deliveryConditionSubMaster;
	}

	public void setDeliveryConditionSubMaster(DeliveryConditionSubMaster deliveryConditionSubMaster) {
		this.deliveryConditionSubMaster = deliveryConditionSubMaster;
	}

	
	
}