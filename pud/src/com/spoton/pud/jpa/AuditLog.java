package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the audit_log database table.
 * 
 */
@Entity
@Table(name="audit_log")
@NamedQuery(name="AuditLog.findAll", query="SELECT a FROM AuditLog a")
public class AuditLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="apk_version")
	private String apkVersion;

	@Column(name="`con_number/awb_number`")
	private String conNumber_awbNumber;

	@Column(name="device_imei")
	private String deviceImei;

	@Column(name="input_parameters")
	private String inputParameters;

	private String ipadd;

	@Column(name="`pickup_schedule_id/pdc_number`")
	private String pickupScheduleId_pdcNumber;

	@Column(name="result_message")
	private String resultMessage;
	
	@Column(name="service_type")
	private String serviceType;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	@Column(name="user_id")
	private String userId;

	public AuditLog() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApkVersion() {
		return this.apkVersion;
	}

	public void setApkVersion(String apkVersion) {
		this.apkVersion = apkVersion;
	}

	public String getConNumber_awbNumber() {
		return this.conNumber_awbNumber;
	}

	public void setConNumber_awbNumber(String conNumber_awbNumber) {
		this.conNumber_awbNumber = conNumber_awbNumber;
	}

	public String getDeviceImei() {
		return this.deviceImei;
	}

	public void setDeviceImei(String deviceImei) {
		this.deviceImei = deviceImei;
	}

	public String getIpadd() {
		return this.ipadd;
	}

	public void setIpadd(String ipadd) {
		this.ipadd = ipadd;
	}

	public String getPickupScheduleId_pdcNumber() {
		return this.pickupScheduleId_pdcNumber;
	}

	public void setPickupScheduleId_pdcNumber(String pickupScheduleId_pdcNumber) {
		this.pickupScheduleId_pdcNumber = pickupScheduleId_pdcNumber;
	}

	public String getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getResultMessage() {
		return this.resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	
	public String getInputParameters() {
		return this.inputParameters;
	}

	public void setInputParameters(String inputParameters) {
		this.inputParameters = inputParameters;
	}

}