package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the vendor_master database table.
 * 
 */
@Entity
@Table(name="vendor_master")
@NamedQuery(name="VendorMaster.findAll", query="SELECT v FROM VendorMaster v")
public class VendorMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="app_password")
	private String appPassword;

	@Column(name="app_username")
	private String appUsername;

	private String location;

	@Column(name="vendor_id")
	private int vendorId;

	@Column(name="vendor_name")
	private String vendorName;

	public VendorMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppPassword() {
		return this.appPassword;
	}

	public void setAppPassword(String appPassword) {
		this.appPassword = appPassword;
	}

	public String getAppUsername() {
		return this.appUsername;
	}

	public void setAppUsername(String appUsername) {
		this.appUsername = appUsername;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getVendorId() {
		return this.vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

}