package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the delivery_condition_images database table.
 * 
 */
@Entity
@Table(name="delivery_condition_images")
@NamedQuery(name="DeliveryConditionImage.findAll", query="SELECT d FROM DeliveryConditionImage d")
public class DeliveryConditionImage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="image_url")
	private String imageUrl;

	@Column(name="piece_number")
	private String pieceNumber;

	//bi-directional many-to-one association to DeliveryUpdation
	@ManyToOne
	@JoinColumn(name="delivery_updation_id")
	private DeliveryUpdation deliveryUpdation;

	@Column(name="erp_updated")
	private int erpUpdated;
	
	public DeliveryConditionImage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getPieceNumber() {
		return this.pieceNumber;
	}

	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}

	public DeliveryUpdation getDeliveryUpdation() {
		return this.deliveryUpdation;
	}

	public void setDeliveryUpdation(DeliveryUpdation deliveryUpdation) {
		this.deliveryUpdation = deliveryUpdation;
	}

	public int getErpUpdated() {
		return erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	
}