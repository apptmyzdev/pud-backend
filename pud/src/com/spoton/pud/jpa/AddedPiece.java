package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the added_pieces database table.
 * 
 */
@Entity
@Table(name="added_pieces")
@NamedQuery(name="AddedPiece.findAll", query="SELECT a FROM AddedPiece a")
public class AddedPiece implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="last_piece_no")
	private String lastPieceNo;

	@Column(name="pcr_key")
	private int pcrKey;

	@Column(name="total_used")
	private String totalUsed;
	
	@Column(name="con_number")
	private String conNumber;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to AddedCon
	@ManyToOne
	@JoinColumn(name="con_id")
	private AddedCon addedCon;

	public AddedPiece() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getLastPieceNo() {
		return this.lastPieceNo;
	}

	public void setLastPieceNo(String lastPieceNo) {
		this.lastPieceNo = lastPieceNo;
	}

	public int getPcrKey() {
		return this.pcrKey;
	}

	public void setPcrKey(int pcrKey) {
		this.pcrKey = pcrKey;
	}

	public String getTotalUsed() {
		return this.totalUsed;
	}

	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}

	public AddedCon getAddedCon() {
		return this.addedCon;
	}

	public void setAddedCon(AddedCon addedCon) {
		this.addedCon = addedCon;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}