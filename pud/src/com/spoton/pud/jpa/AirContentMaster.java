package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the air_content_master database table.
 * 
 */
@Entity
@Table(name="air_content_master")
@NamedQuery(name="AirContentMaster.findAll", query="SELECT a FROM AirContentMaster a")
public class AirContentMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	
	@Column(name="dangerous_id")
	private int dangerousId;

	@Column(name="dangerous_description")
	private String dangerousDescription;

	public AirContentMaster() {
	}

	public int getDangerousId() {
		return this.dangerousId;
	}

	public void setDangerousId(int dangerousId) {
		this.dangerousId = dangerousId;
	}

	public String getDangerousDescription() {
		return this.dangerousDescription;
	}

	public void setDangerousDescription(String dangerousDescription) {
		this.dangerousDescription = dangerousDescription;
	}

}