package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the customer_service_master database table.
 * 
 */
@Entity
@Table(name="customer_service_master")
@NamedQuery(name="CustomerServiceMaster.findAll", query="SELECT c FROM CustomerServiceMaster c")
public class CustomerServiceMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CustomerServiceMasterPK id;

	@Column(name="active_flag")
	private int activeFlag;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_time")
	private Date createdTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_time")
	private Date updatedTime;

	private int version;

	public CustomerServiceMaster() {
	}

	public CustomerServiceMasterPK getId() {
		return this.id;
	}

	public void setId(CustomerServiceMasterPK id) {
		this.id = id;
	}

	public int getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public Date getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}