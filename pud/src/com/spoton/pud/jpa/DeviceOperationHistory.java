package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the device_operation_history database table.
 * 
 */
@Entity
@Table(name="device_operation_history")
public class DeviceOperationHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="device_operation_history_id", unique=true, nullable=false)
	private int deviceOperationHistoryId;

	@Column(name="device_id", nullable=false)
	private int deviceId;

	@Column(name="device_operation_history_type_id", nullable=false)
	private int deviceOperationHistoryTypeId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="device_timestamp")
	private Date deviceTimestamp;

	@Column(length=512)
	private String messge;

	@Column(nullable=false, length=256)
	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	public DeviceOperationHistory() {
	}

	public int getDeviceOperationHistoryId() {
		return this.deviceOperationHistoryId;
	}

	public void setDeviceOperationHistoryId(int deviceOperationHistoryId) {
		this.deviceOperationHistoryId = deviceOperationHistoryId;
	}

	public int getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getDeviceOperationHistoryTypeId() {
		return this.deviceOperationHistoryTypeId;
	}

	public void setDeviceOperationHistoryTypeId(int deviceOperationHistoryTypeId) {
		this.deviceOperationHistoryTypeId = deviceOperationHistoryTypeId;
	}

	public Date getDeviceTimestamp() {
		return this.deviceTimestamp;
	}

	public void setDeviceTimestamp(Date deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public String getMessge() {
		return this.messge;
	}

	public void setMessge(String messge) {
		this.messge = messge;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}