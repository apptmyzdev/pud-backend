package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the added_cons database table.
 * 
 */
@Entity
@Table(name="added_cons")
@NamedQuery(name="AddedCon.findAll", query="SELECT a FROM AddedCon a")
public class AddedCon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="actual_weight")
	private String actualWeight;

	@Column(name="apply_dc")
	private String applyDc;

	@Column(name="apply_nf_form")
	private String applyNfForm;

	@Column(name="apply_vtv")
	private String applyVtv;

	@Column(name="con_entry_id")
	private int conEntryId;

	@Column(name="con_number")
	private String conNumber;

	@Column(name="eway_bill_number")
	  private String ewayBillNumber;
	
	@Column(name="consignment_type")
	private String consignmentType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="crm_schedule_id")
	private String crmScheduleId;

	@Column(name="customer_code")
	private String customerCode;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="declared_value")
	private String declaredValue;

	@Column(name="destination_pincode")
	private String destinationPincode;

	@Column(name="gate_pass_time")
	private String gatePassTime;

	@Column(name="image1_url")
	private String image1Url;

	@Column(name="image2_url")
	private String image2Url;

	@Column(name="is_scanned")
	private int isScanned;

	@Column(name="lat_value")
	private String latValue;

	@Column(name="long_value")
	private String longValue;

	@Column(name="no_of_package")
	private String noOfPackage;

	@Column(name="order_no")
	private String orderNo;

	@Column(name="origin_pincode")
	private String originPincode;

	@Column(name="package_type")
	private String packageType;

	@Column(name="pan_no")
	private String panNo;

	@Column(name="payment_basis")
	private String paymentBasis;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	private String product;

	@Column(name="receiver_name")
	private String receiverName;

	@Column(name="receiver_phone_no")
	private String receiverPhoneNo;

	@Column(name="ref_number")
	private String refNumber;

	@Column(name="risk_type")
	private String riskType;

	@Column(name="shipment_image_url")
	private String shipmentImageUrl;

	@Column(name="special_instruction")
	private String specialInstruction;

	@Column(name="tin_no")
	private String tinNo;

	@Column(name="total_vol_weight")
	private String totalVolWeight;

	@Column(name="user_id")
	private String userId;

	@Column(name="user_name")
	private String userName;

	@Column(name="vehicle_number")
	private String vehicleNumber;

	@Column(name="vol_type")
	private String volType;

	@Column(name="vtc_amount")
	private String vtcAmount;

	@Column(name="receiver_gstin")
	private String receiverGstin;
	
	@Column(name="receiver_email")
	private String receiverEmail;
	
	@Column(name="sender_email")
	private String senderEmail;
	
	@Column(name="receiver_address")
	private String receiverAddress;
	
	@Column(name="receiver_city")
	private String receiverCity;

	
	//bi-directional many-to-one association to AddedEwayBillNumber
	@OneToMany(mappedBy="addedCon")
	private List<AddedEwayBillNumber> addedEwayBillNumbers;
	
	@Column(name="pickup_updated_address")
	private String pickupUpdatedAddress;

	@Column(name="pickup_updated_pincode")
	private String pickupUpdatedPincode;
	
	@Column(name="is_pre_printed") //added on 18102019-ak
	private String isPrePrinted;

	public AddedCon() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActualWeight() {
		return this.actualWeight;
	}

	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}

	public String getApplyDc() {
		return this.applyDc;
	}

	public void setApplyDc(String applyDc) {
		this.applyDc = applyDc;
	}

	public String getApplyNfForm() {
		return this.applyNfForm;
	}

	public void setApplyNfForm(String applyNfForm) {
		this.applyNfForm = applyNfForm;
	}

	public String getApplyVtv() {
		return this.applyVtv;
	}

	public void setApplyVtv(String applyVtv) {
		this.applyVtv = applyVtv;
	}

	public int getConEntryId() {
		return this.conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public String getConsignmentType() {
		return this.consignmentType;
	}

	public void setConsignmentType(String consignmentType) {
		this.consignmentType = consignmentType;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCrmScheduleId() {
		return this.crmScheduleId;
	}

	public void setCrmScheduleId(String crmScheduleId) {
		this.crmScheduleId = crmScheduleId;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDeclaredValue() {
		return this.declaredValue;
	}

	public void setDeclaredValue(String declaredValue) {
		this.declaredValue = declaredValue;
	}

	public String getDestinationPincode() {
		return this.destinationPincode;
	}

	public void setDestinationPincode(String destinationPincode) {
		this.destinationPincode = destinationPincode;
	}

	public String getGatePassTime() {
		return this.gatePassTime;
	}

	public void setGatePassTime(String gatePassTime) {
		this.gatePassTime = gatePassTime;
	}

	public String getImage1Url() {
		return this.image1Url;
	}

	public void setImage1Url(String image1Url) {
		this.image1Url = image1Url;
	}

	public String getImage2Url() {
		return this.image2Url;
	}

	public void setImage2Url(String image2Url) {
		this.image2Url = image2Url;
	}

	public int getIsScanned() {
		return this.isScanned;
	}

	public void setIsScanned(int isScanned) {
		this.isScanned = isScanned;
	}

	public String getLatValue() {
		return this.latValue;
	}

	public void setLatValue(String latValue) {
		this.latValue = latValue;
	}

	public String getLongValue() {
		return this.longValue;
	}

	public void setLongValue(String longValue) {
		this.longValue = longValue;
	}

	public String getNoOfPackage() {
		return this.noOfPackage;
	}

	public void setNoOfPackage(String noOfPackage) {
		this.noOfPackage = noOfPackage;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOriginPincode() {
		return this.originPincode;
	}

	public void setOriginPincode(String originPincode) {
		this.originPincode = originPincode;
	}

	public String getPackageType() {
		return this.packageType;
	}

	public void setPackageType(String packageType) {
		this.packageType = packageType;
	}

	public String getPanNo() {
		return this.panNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public String getPaymentBasis() {
		return this.paymentBasis;
	}

	public void setPaymentBasis(String paymentBasis) {
		this.paymentBasis = paymentBasis;
	}

	public Date getPickupDate() {
		return this.pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getReceiverName() {
		return this.receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReceiverPhoneNo() {
		return this.receiverPhoneNo;
	}

	public void setReceiverPhoneNo(String receiverPhoneNo) {
		this.receiverPhoneNo = receiverPhoneNo;
	}

	public String getRefNumber() {
		return this.refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getRiskType() {
		return this.riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getShipmentImageUrl() {
		return this.shipmentImageUrl;
	}

	public void setShipmentImageUrl(String shipmentImageUrl) {
		this.shipmentImageUrl = shipmentImageUrl;
	}

	public String getSpecialInstruction() {
		return this.specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getTinNo() {
		return this.tinNo;
	}

	public void setTinNo(String tinNo) {
		this.tinNo = tinNo;
	}

	public String getTotalVolWeight() {
		return this.totalVolWeight;
	}

	public void setTotalVolWeight(String totalVolWeight) {
		this.totalVolWeight = totalVolWeight;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVolType() {
		return this.volType;
	}

	public void setVolType(String volType) {
		this.volType = volType;
	}

	public String getVtcAmount() {
		return this.vtcAmount;
	}

	public void setVtcAmount(String vtcAmount) {
		this.vtcAmount = vtcAmount;
	}

	public List<AddedEwayBillNumber> getAddedEwayBillNumbers() {
		return this.addedEwayBillNumbers;
	}

	public void setAddedEwayBillNumbers(List<AddedEwayBillNumber> addedEwayBillNumbers) {
		this.addedEwayBillNumbers = addedEwayBillNumbers;
	}
	
	
	
	

	public String getReceiverGstin() {
		return receiverGstin;
	}

	public void setReceiverGstin(String receiverGstin) {
		this.receiverGstin = receiverGstin;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public AddedEwayBillNumber addAddedEwayBillNumber(AddedEwayBillNumber addedEwayBillNumber) {
		getAddedEwayBillNumbers().add(addedEwayBillNumber);
		addedEwayBillNumber.setAddedCon(this);

		return addedEwayBillNumber;
	}

	public AddedEwayBillNumber removeAddedEwayBillNumber(AddedEwayBillNumber addedEwayBillNumber) {
		getAddedEwayBillNumbers().remove(addedEwayBillNumber);
		addedEwayBillNumber.setAddedCon(null);

		return addedEwayBillNumber;
	}

	public String getEwayBillNumber() {
		return ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}

	public String getPickupUpdatedAddress() {
		return pickupUpdatedAddress;
	}

	public void setPickupUpdatedAddress(String pickupUpdatedAddress) {
		this.pickupUpdatedAddress = pickupUpdatedAddress;
	}

	public String getPickupUpdatedPincode() {
		return pickupUpdatedPincode;
	}

	public void setPickupUpdatedPincode(String pickupUpdatedPincode) {
		this.pickupUpdatedPincode = pickupUpdatedPincode;
	}

	public String getIsPrePrinted() {
		return isPrePrinted;
	}

	public void setIsPrePrinted(String isPrePrinted) {
		this.isPrePrinted = isPrePrinted;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	
}