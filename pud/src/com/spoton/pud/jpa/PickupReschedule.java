package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the pickup_reschedule database table.
 * 
 */
@Entity
@Table(name="pickup_reschedule")
@NamedQuery(name="PickupReschedule.findAll", query="SELECT p FROM PickupReschedule p")
public class PickupReschedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="app_mobile_user_name")
	private String appMobileUserName;

	@Column(name="con_number")
	private String conNumber;

	@Column(name="gps_lat")
	private double gpsLat;

	@Column(name="gps_lon")
	private double gpsLon;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	@Column(name="reason_id")
	private String reasonId;

	@Column(name="ref_no")
	private String refNo;

	private String remarks;

	@Column(name="customer_code")
	private String customerCode;

	@Column(name="customer_name")
	private String customerName;
	
	@Column(name="product_type")
	private String productType;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reschedule_datetime")
	private Date rescheduleDatetime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;
	
	@Column(name="status_id")
	private int statusId;

	@Column(name="updated_by")
	private String updatedBy;
	
	@Column(name="transaction_result")
	private String transactionResult;
	
	@Column(name="contact_person_name")
	private String contactPersonName;
	
	@Column(name="contact_person_phone")
	private String contactPersonPhone;

	public PickupReschedule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppMobileUserName() {
		return this.appMobileUserName;
	}

	public void setAppMobileUserName(String appMobileUserName) {
		this.appMobileUserName = appMobileUserName;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public double getGpsLat() {
		return this.gpsLat;
	}

	public void setGpsLat(double gpsLat) {
		this.gpsLat = gpsLat;
	}

	public double getGpsLon() {
		return this.gpsLon;
	}

	public void setGpsLon(double gpsLon) {
		this.gpsLon = gpsLon;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getReasonId() {
		return this.reasonId;
	}

	
	
	public void setReasonId(String reasonId) {
		this.reasonId = reasonId;
	}

	
	
	
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getRefNo() {
		return this.refNo;
	}

	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getRescheduleDatetime() {
		return this.rescheduleDatetime;
	}

	public void setRescheduleDatetime(Date rescheduleDatetime) {
		this.rescheduleDatetime = rescheduleDatetime;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTransactionResult() {
		return transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getContactPersonName() {
		return contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonPhone() {
		return contactPersonPhone;
	}

	public void setContactPersonPhone(String contactPersonPhone) {
		this.contactPersonPhone = contactPersonPhone;
	}

	
	
}