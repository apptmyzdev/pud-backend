package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the air_flight_weight_master database table.
 * 
 */
@Entity
@Table(name="air_flight_weight_master")
@NamedQuery(name="AirFlightWeightMaster.findAll", query="SELECT a FROM AirFlightWeightMaster a")
public class AirFlightWeightMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="flight_desc")
	private String flightDesc;

	@Column(name="flight_id")
	private String flightId;

	@Column(name="vendor_code")
	private String vendorCode;

	@Column(name="vendor_id")
	private String vendorId;

	@Column(name="weight_allowed")
	private String weightAllowed;

	public AirFlightWeightMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getFlightDesc() {
		return this.flightDesc;
	}

	public void setFlightDesc(String flightDesc) {
		this.flightDesc = flightDesc;
	}

	public String getFlightId() {
		return this.flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorId() {
		return this.vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getWeightAllowed() {
		return this.weightAllowed;
	}

	public void setWeightAllowed(String weightAllowed) {
		this.weightAllowed = weightAllowed;
	}

}