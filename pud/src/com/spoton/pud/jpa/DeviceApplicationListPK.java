package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the device_application_list database table.
 * 
 */
@Embeddable
public class DeviceApplicationListPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="device_id")
	private int deviceId;

	@Column(name="application_package_name")
	private String applicationPackageName;

	public DeviceApplicationListPK() {
	}
	public int getDeviceId() {
		return this.deviceId;
	}
	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}
	public String getApplicationPackageName() {
		return this.applicationPackageName;
	}
	public void setApplicationPackageName(String applicationPackageName) {
		this.applicationPackageName = applicationPackageName;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof DeviceApplicationListPK)) {
			return false;
		}
		DeviceApplicationListPK castOther = (DeviceApplicationListPK)other;
		return 
			(this.deviceId == castOther.deviceId)
			&& this.applicationPackageName.equals(castOther.applicationPackageName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.deviceId;
		hash = hash * prime + this.applicationPackageName.hashCode();
		
		return hash;
	}
}