package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_version database table.
 * 
 */
@Entity
@Table(name="user_version")
@NamedQuery(name="UserVersion.findAll", query="SELECT u FROM UserVersion u")
public class UserVersion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String url;

	private String user;

	private int version;

	public UserVersion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}