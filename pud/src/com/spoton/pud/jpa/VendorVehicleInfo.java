package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the vendor_vehicle_info database table.
 * 
 */
@Entity
@Table(name="vendor_vehicle_info")
@NamedQuery(name="VendorVehicleInfo.findAll", query="SELECT v FROM VendorVehicleInfo v")
public class VendorVehicleInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="ba_code")
	private String baCode;

	@Column(name="bacct_name")
	private String bacctName;

	private String branch;

	@Column(name="vehicle_id")
	private int vehicleId;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private String vehicleType;

	@Column(name="vendor_code")
	private String vendorCode;

	public VendorVehicleInfo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBaCode() {
		return this.baCode;
	}

	public void setBaCode(String baCode) {
		this.baCode = baCode;
	}

	public String getBacctName() {
		return this.bacctName;
	}

	public void setBacctName(String bacctName) {
		this.bacctName = bacctName;
	}

	public String getBranch() {
		return this.branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public int getVehicleId() {
		return this.vehicleId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

}