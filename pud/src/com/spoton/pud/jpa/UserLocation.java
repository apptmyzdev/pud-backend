package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the user_location database table.
 * 
 */
@Entity
@Table(name="user_location")
@NamedQuery(name="UserLocation.findAll", query="SELECT u FROM UserLocation u")
public class UserLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private float accuracy;

	private String address;

	@Column(name="con_number")
	private String conNumber;

	@Column(name="customer_address")
	private String customerAddress;

	@Column(name="customer_name")
	private String customerName;

	//bi-directional many-to-one association to DeviceInfo
		@ManyToOne
		@JoinColumn(name="deviceId")
		private DeviceInfo deviceInfo;

		//bi-directional many-to-one association to UserDataMaster
		@ManyToOne
		@JoinColumn(name="userId")
		private UserDataMaster userDataMaster;

	@Temporal(TemporalType.TIMESTAMP)
	private Date deviceTimestamp;

	private double distance;

	@Temporal(TemporalType.TIMESTAMP)
	private Date endTime;

	private int flag;

	private double latitude;

	private double longitude;

	@Column(name="no_of_cons")
	private int noOfCons;

	@Column(name="no_of_pieces")
	private int noOfPieces;

	@Column(name="pdc_or_pickup_schedule_id")
	private String pdcOrPickupScheduleId;

	private String pincode;

	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	@Column(name="total_weight")
	private double totalWeight;

	@Column(name="updated_time")
	private String updatedTime;

	

	public UserLocation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getAccuracy() {
		return this.accuracy;
	}

	public void setAccuracy(float accuracy) {
		this.accuracy = accuracy;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public String getCustomerAddress() {
		return this.customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	

	public Date getDeviceTimestamp() {
		return this.deviceTimestamp;
	}

	public void setDeviceTimestamp(Date deviceTimestamp) {
		this.deviceTimestamp = deviceTimestamp;
	}

	public double getDistance() {
		return this.distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getFlag() {
		return this.flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getNoOfCons() {
		return this.noOfCons;
	}

	public void setNoOfCons(int noOfCons) {
		this.noOfCons = noOfCons;
	}

	public int getNoOfPieces() {
		return this.noOfPieces;
	}

	public void setNoOfPieces(int noOfPieces) {
		this.noOfPieces = noOfPieces;
	}

	public String getPdcOrPickupScheduleId() {
		return this.pdcOrPickupScheduleId;
	}

	public void setPdcOrPickupScheduleId(String pdcOrPickupScheduleId) {
		this.pdcOrPickupScheduleId = pdcOrPickupScheduleId;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Date getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public double getTotalWeight() {
		return this.totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public UserDataMaster getUserDataMaster() {
		return userDataMaster;
	}

	public void setUserDataMaster(UserDataMaster userDataMaster) {
		this.userDataMaster = userDataMaster;
	}

	
}