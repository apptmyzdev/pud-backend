package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the customer_service_master database table.
 * 
 */
@Embeddable
public class CustomerServiceMasterPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="account_code")
	private String accountCode;

	private String origin;

	private String destination;

	public CustomerServiceMasterPK() {
	}
	public String getAccountCode() {
		return this.accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getOrigin() {
		return this.origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return this.destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CustomerServiceMasterPK)) {
			return false;
		}
		CustomerServiceMasterPK castOther = (CustomerServiceMasterPK)other;
		return 
			this.accountCode.equals(castOther.accountCode)
			&& this.origin.equals(castOther.origin)
			&& this.destination.equals(castOther.destination);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.accountCode.hashCode();
		hash = hash * prime + this.origin.hashCode();
		hash = hash * prime + this.destination.hashCode();
		
		return hash;
	}
}