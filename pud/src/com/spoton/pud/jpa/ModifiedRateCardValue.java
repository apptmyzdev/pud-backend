package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the modified_rate_card_values database table.
 * 
 */
@Entity
@Table(name = "modified_rate_card_values")
@NamedQuery(name = "ModifiedRateCardValue.findAll", query = "SELECT m FROM ModifiedRateCardValue m")
public class ModifiedRateCardValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "charged_weight")
	private double chargedWeight;

	private double collection;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp")
	private Date createdTimestamp;

	private double dacc;

	private double dcac;

	private double dfs;

	private double dkt;

	@Column(name = "error_code")
	private String errorCode;

	private double fov;

	private double frt;

	private double ftc;

	@Column(name = "gross_amount")
	private double grossAmount;

	@Column(name = "handling_charge")
	private double handlingCharge;

	@Column(name = "min_charge")
	private double minCharge;

	@Column(name = "nf_form")
	private double nfForm;

	private double oda;

	@Column(name = "other_recovery")
	private double otherRecovery;

	private double pda;

	@Column(name = "ph_delivery")
	private double phDelivery;

	@Column(name = "ph_pickup")
	private double phPickup;

	@Column(name = "service_tax")
	private double serviceTax;

	@Column(name = "state_charge")
	private double stateCharge;

	@Column(name = "total_amount")
	private double totalAmount;

	private double vtc;
	
	// bi-directional many-to-one association to ConDetail
	@ManyToOne
	@JoinColumn(name = "con_id")
	private ConDetail conDetail;

	/*03-09-2020 - ak*/
	@Column(name = "covid_surcharge")
	private double covidSurcharge;
	/**/
	
	public ModifiedRateCardValue() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getChargedWeight() {
		return this.chargedWeight;
	}

	public void setChargedWeight(double chargedWeight) {
		this.chargedWeight = chargedWeight;
	}

	public double getCollection() {
		return this.collection;
	}

	public void setCollection(double collection) {
		this.collection = collection;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public double getDacc() {
		return this.dacc;
	}

	public void setDacc(double dacc) {
		this.dacc = dacc;
	}

	public double getDcac() {
		return this.dcac;
	}

	public void setDcac(double dcac) {
		this.dcac = dcac;
	}

	public double getDfs() {
		return this.dfs;
	}

	public void setDfs(double dfs) {
		this.dfs = dfs;
	}

	public double getDkt() {
		return this.dkt;
	}

	public void setDkt(double dkt) {
		this.dkt = dkt;
	}

	public String getErrorCode() {
		return this.errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public double getFov() {
		return this.fov;
	}

	public void setFov(double fov) {
		this.fov = fov;
	}

	public double getFrt() {
		return this.frt;
	}

	public void setFrt(double frt) {
		this.frt = frt;
	}

	public double getFtc() {
		return this.ftc;
	}

	public void setFtc(double ftc) {
		this.ftc = ftc;
	}

	public double getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public double getHandlingCharge() {
		return this.handlingCharge;
	}

	public void setHandlingCharge(double handlingCharge) {
		this.handlingCharge = handlingCharge;
	}

	public double getMinCharge() {
		return this.minCharge;
	}

	public void setMinCharge(double minCharge) {
		this.minCharge = minCharge;
	}

	public double getNfForm() {
		return this.nfForm;
	}

	public void setNfForm(double nfForm) {
		this.nfForm = nfForm;
	}

	public double getOda() {
		return this.oda;
	}

	public void setOda(double oda) {
		this.oda = oda;
	}

	public double getOtherRecovery() {
		return this.otherRecovery;
	}

	public void setOtherRecovery(double otherRecovery) {
		this.otherRecovery = otherRecovery;
	}

	public double getPda() {
		return this.pda;
	}

	public void setPda(double pda) {
		this.pda = pda;
	}

	public double getPhDelivery() {
		return this.phDelivery;
	}

	public void setPhDelivery(double phDelivery) {
		this.phDelivery = phDelivery;
	}

	public double getPhPickup() {
		return this.phPickup;
	}

	public void setPhPickup(double phPickup) {
		this.phPickup = phPickup;
	}

	public double getServiceTax() {
		return this.serviceTax;
	}

	public void setServiceTax(double serviceTax) {
		this.serviceTax = serviceTax;
	}

	public double getStateCharge() {
		return this.stateCharge;
	}

	public void setStateCharge(double stateCharge) {
		this.stateCharge = stateCharge;
	}

	public double getTotalAmount() {
		return this.totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getVtc() {
		return this.vtc;
	}

	public void setVtc(double vtc) {
		this.vtc = vtc;
	}

	public ConDetail getConDetail() {
		return conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	/** added on Sep 3, 2020 **/
	public double getCovidSurcharge() {
		return covidSurcharge;
	}

	/** added on Sep 3, 2020 **/
	public void setCovidSurcharge(double covidSurcharge) {
		this.covidSurcharge = covidSurcharge;
	}

	
}