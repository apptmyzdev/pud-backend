package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the delivery_condition_sub_master database table.
 * 
 */
@Entity
@Table(name="delivery_condition_sub_master")
@NamedQuery(name="DeliveryConditionSubMaster.findAll", query="SELECT d FROM DeliveryConditionSubMaster d")
public class DeliveryConditionSubMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="condition_id")
	private int conditionId;

	@Column(name="condition_name")
	private String conditionName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	public DeliveryConditionSubMaster() {
	}

	

	public int getConditionId() {
		return this.conditionId;
	}

	public void setConditionId(int conditionId) {
		this.conditionId = conditionId;
	}

	public String getConditionName() {
		return this.conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

}