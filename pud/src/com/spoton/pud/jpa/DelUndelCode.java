package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the del_undel_codes database table.
 * 
 */
@Entity
@Table(name="del_undel_codes")
@NamedQuery(name="DelUndelCode.findAll", query="SELECT d FROM DelUndelCode d")
public class DelUndelCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int code;

	@Column(name="del_undel_code")
	private int delUndelCode;

	private String description;

	@Column(name="short_code")
	private String shortCode;

	@Column(name="sort_order")
	private int sortOrder;

	public DelUndelCode() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCode() {
		return this.code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getDelUndelCode() {
		return this.delUndelCode;
	}

	public void setDelUndelCode(int delUndelCode) {
		this.delUndelCode = delUndelCode;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	public int getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

}