package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the delivery_updation_status database table.
 * 
 */
@Entity
@Table(name="delivery_updation_status")
@NamedQuery(name="DeliveryUpdationStatus.findAll", query="SELECT d FROM DeliveryUpdationStatus d")
public class DeliveryUpdationStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="transaction_id")
	private int transactionId;

	@Column(name="transaction_remarks")
	private String transactionRemarks;

	@Column(name="transaction_result")
	private String transactionResult;

	
	@PrimaryKeyJoinColumn(name="transaction_id")
	private DeliveryUpdation deliveryUpdation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;
	
	public DeliveryUpdationStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionRemarks() {
		return this.transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public DeliveryUpdation getDeliveryUpdation() {
		return this.deliveryUpdation;
	}

	public void setDeliveryUpdation(DeliveryUpdation deliveryUpdation) {
		this.deliveryUpdation = deliveryUpdation;
	}
	
	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
}