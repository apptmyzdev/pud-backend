package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pickup_cancellation_master database table.
 * 
 */
@Entity
@Table(name="pickup_cancellation_master")
@NamedQuery(name="PickupCancellationMaster.findAll", query="SELECT p FROM PickupCancellationMaster p")
public class PickupCancellationMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String status;

	@Column(name="status_id")
	private int statusId;

	public PickupCancellationMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStatusId() {
		return this.statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

}