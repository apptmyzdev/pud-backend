package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mailed_pickup_cons database table.
 * 
 */
@Entity
@Table(name="mailed_pickup_cons")
@NamedQuery(name="MailedPickupCon.findAll", query="SELECT m FROM MailedPickupCon m")
public class MailedPickupCon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="con_entry_id")
	private int conEntryId;

	public MailedPickupCon() {
	}

	public int getConEntryId() {
		return this.conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

}