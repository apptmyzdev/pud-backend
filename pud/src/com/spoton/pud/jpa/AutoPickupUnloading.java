package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the auto_pickup_unloading database table.
 * 
 */
@Entity
@Table(name="auto_pickup_unloading")
@NamedQuery(name="AutoPickupUnloading.findAll", query="SELECT a FROM AutoPickupUnloading a")
public class AutoPickupUnloading implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="transaction_id")
	private int transactionId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="is_unloading_started")
	private int isUnloadingStarted;

	@Column(name="pickup_unloading_sheet_number")
	private String pickupUnloadingSheetNumber;

	@Column(name="total_con_weight")
	private double totalConWeight;

	@Column(name="total_cons_count")
	private int totalConsCount;

	@Column(name="total_pieces_count")
	private int totalPiecesCount;

	@Column(name="transaction_message")
	private String transactionMessage;

	@Column(name="transaction_result")
	private String transactionResult;

	@Column(name="user_id")
	private String userId;

	@Column(name="vehicle_number")
	private String vehicleNumber;

	//bi-directional many-to-one association to AutoPickupUnloadingCon
	@OneToMany(mappedBy="autoPickupUnloading")
	private List<AutoPickupUnloadingCon> autoPickupUnloadingCons;

	public AutoPickupUnloading() {
	}

	public int getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public int getIsUnloadingStarted() {
		return this.isUnloadingStarted;
	}

	public void setIsUnloadingStarted(int isUnloadingStarted) {
		this.isUnloadingStarted = isUnloadingStarted;
	}

	public String getPickupUnloadingSheetNumber() {
		return this.pickupUnloadingSheetNumber;
	}

	public void setPickupUnloadingSheetNumber(String pickupUnloadingSheetNumber) {
		this.pickupUnloadingSheetNumber = pickupUnloadingSheetNumber;
	}

	public double getTotalConWeight() {
		return this.totalConWeight;
	}

	public void setTotalConWeight(double totalConWeight) {
		this.totalConWeight = totalConWeight;
	}

	public int getTotalConsCount() {
		return this.totalConsCount;
	}

	public void setTotalConsCount(int totalConsCount) {
		this.totalConsCount = totalConsCount;
	}

	public int getTotalPiecesCount() {
		return this.totalPiecesCount;
	}

	public void setTotalPiecesCount(int totalPiecesCount) {
		this.totalPiecesCount = totalPiecesCount;
	}

	public String getTransactionMessage() {
		return this.transactionMessage;
	}

	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public List<AutoPickupUnloadingCon> getAutoPickupUnloadingCons() {
		return this.autoPickupUnloadingCons;
	}

	public void setAutoPickupUnloadingCons(List<AutoPickupUnloadingCon> autoPickupUnloadingCons) {
		this.autoPickupUnloadingCons = autoPickupUnloadingCons;
	}

	public AutoPickupUnloadingCon addAutoPickupUnloadingCon(AutoPickupUnloadingCon autoPickupUnloadingCon) {
		getAutoPickupUnloadingCons().add(autoPickupUnloadingCon);
		autoPickupUnloadingCon.setAutoPickupUnloading(this);

		return autoPickupUnloadingCon;
	}

	public AutoPickupUnloadingCon removeAutoPickupUnloadingCon(AutoPickupUnloadingCon autoPickupUnloadingCon) {
		getAutoPickupUnloadingCons().remove(autoPickupUnloadingCon);
		autoPickupUnloadingCon.setAutoPickupUnloading(null);

		return autoPickupUnloadingCon;
	}

}