package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

/**
 * The persistent class for the pickup_registration database table.
 * 
 */
@Entity
@Table(name = "pickup_registration")
@NamedQuery(name = "PickupRegistration.findAll", query = "SELECT p FROM PickupRegistration p")
public class PickupRegistration implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private float cft;

	@Column(name = "contact_person_name")
	private String contactPersonName;

	@Column(name = "contact_person_number")
	private String contactPersonNumber;

	@Column(name = "Customer_code")
	private String customerCode;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "gps_lat")
	private double gpsLat;

	@Column(name = "gps_lon")
	private double gpsLon;

	@Column(name = "mobile_user_id")
	private String mobileUserId;

	@Column(name = "pickup_address")
	private String pickupAddress;

	@Column(name = "pickup_latitude")
	private double pickupLatitude;

	@Column(name = "pickup_longitude")
	private double pickupLongitude;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pickup_date")
	private Date pickupDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pulled_time")
	private Date pulledTime;

	@Column(name = "pickup_order_no")
	private String pickupOrderNo;

	@Column(name = "pickup_pincode")
	private int pickupPincode;

	@Column(name = "pickup_schedule_id")
	private int pickupScheduleId;

	private int pieces;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp")
	private Date createdTimestamp;

	@Column(name = "transaction_remarks")
	private String transactionRemarks;

	@Column(name = "transaction_result")
	private String transactionResult;

	@Column(name = "sender_mail_id")
	private String senderMailId;

	@Column(name = "product_type")
	private String productType;

	private double weight;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_till")
	private Date validTill;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_attempted_time")
	private Date updatedAttemptedTime;

	@Column(name = "updated_attempted_flag")
	private String updatedAttemptedFlag;

	@Column(name = "is_reassigned")
	private int isReassigned;

	@Column(name = "user_before_reassign")
	private String userBeforeReassign;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reassign_datetime")
	private Date reassignDateTime;

	/* 18-09-2020 -ak */
	@Column(name = "is_con_note_enabled")
	private int conNoteEnabled;

	@Column(name = "is_one_order_one_con")
	private int oneOrderOnecon;
	/**/

	public PickupRegistration() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getCft() {
		return this.cft;
	}

	public void setCft(float cft) {
		this.cft = cft;
	}

	public String getSenderMailId() {
		return senderMailId;
	}

	public void setSenderMailId(String senderMailId) {
		this.senderMailId = senderMailId;
	}

	public String getContactPersonName() {
		return this.contactPersonName;
	}

	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}

	public String getContactPersonNumber() {
		return this.contactPersonNumber;
	}

	public void setContactPersonNumber(String contactPersonNumber) {
		this.contactPersonNumber = contactPersonNumber;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getGpsLat() {
		return this.gpsLat;
	}

	public void setGpsLat(double gpsLat) {
		this.gpsLat = gpsLat;
	}

	public double getGpsLon() {
		return this.gpsLon;
	}

	public void setGpsLon(double gpsLon) {
		this.gpsLon = gpsLon;
	}

	public String getMobileUserId() {
		return this.mobileUserId;
	}

	public void setMobileUserId(String mobileUserId) {
		this.mobileUserId = mobileUserId;
	}

	public String getPickupAddress() {
		return this.pickupAddress;
	}

	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public Date getPickupDate() {
		return this.pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupOrderNo() {
		return this.pickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}

	public int getPickupPincode() {
		return this.pickupPincode;
	}

	public void setPickupPincode(int pickupPincode) {
		this.pickupPincode = pickupPincode;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public int getPieces() {
		return this.pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	public String getTransactionRemarks() {
		return this.transactionRemarks;
	}

	public void setTransactionRemarks(String transactionRemarks) {
		this.transactionRemarks = transactionRemarks;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public double getWeight() {
		return this.weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Date getPulledTime() {
		return this.pulledTime;
	}

	public void setPulledTime(Date pulledTime) {
		this.pulledTime = pulledTime;
	}

	public double getPickupLatitude() {
		return pickupLatitude;
	}

	public void setPickupLatitude(double pickupLatitude) {
		this.pickupLatitude = pickupLatitude;
	}

	public double getPickupLongitude() {
		return pickupLongitude;
	}

	public void setPickupLongitude(double pickupLongitude) {
		this.pickupLongitude = pickupLongitude;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public Date getUpdatedAttemptedTime() {
		return updatedAttemptedTime;
	}

	public void setUpdatedAttemptedTime(Date updatedAttemptedTime) {
		this.updatedAttemptedTime = updatedAttemptedTime;
	}

	public String getUpdatedAttemptedFlag() {
		return updatedAttemptedFlag;
	}

	public void setUpdatedAttemptedFlag(String updatedAttemptedFlag) {
		this.updatedAttemptedFlag = updatedAttemptedFlag;
	}

	public int getIsReassigned() {
		return isReassigned;
	}

	public void setIsReassigned(int isReassigned) {
		this.isReassigned = isReassigned;
	}

	public String getUserBeforeReassign() {
		return userBeforeReassign;
	}

	public void setUserBeforeReassign(String userBeforeReassign) {
		this.userBeforeReassign = userBeforeReassign;
	}

	public Date getReassignDateTime() {
		return reassignDateTime;
	}

	public void setReassignDateTime(Date reassignDateTime) {
		this.reassignDateTime = reassignDateTime;
	}

	/** added on Sep 18, 2020 **/
	public int getConNoteEnabled() {
		return conNoteEnabled;
	}

	/** added on Sep 18, 2020 **/
	public void setConNoteEnabled(int conNoteEnabled) {
		this.conNoteEnabled = conNoteEnabled;
	}

	/** added on Sep 18, 2020 **/
	public int getOneOrderOnecon() {
		return oneOrderOnecon;
	}

	/** added on Sep 18, 2020 **/
	public void setOneOrderOnecon(int oneOrderOnecon) {
		this.oneOrderOnecon = oneOrderOnecon;
	}

}