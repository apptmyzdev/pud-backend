package com.spoton.pud.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the pud_data database table.
 * 
 */
@Entity
@Table(name = "pud_data")
@NamedQuery(name = "PudData.findAll", query = "SELECT p FROM PudData p")
public class PudData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private float cft;

	private String branch;

	@Column(name = "contact_mobile")
	private String contactMobile;

	@Column(name = "contact_person")
	private String contactPerson;

	@Column(name = "customer_code")
	private String customerCode;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_ref_number")
	private String customerRefNumber;

	@Column(name = "delivery_address")
	private String deliveryAddress;

	@Column(name = "delivery_city")
	private String deliveryCity;

	@Column(name = "delivery_pincode")
	private int deliveryPincode;

	@Column(name = "min_cfg_weight")
	private float minCfgWeight;

	@Column(name = "pickup_address")
	private String pickupAddress;

	@Column(name = "pickup_latitude")
	private double pickupLatitude;

	@Column(name = "pickup_longitude")
	private double pickupLongitude;

	@Column(name = "pickup_city")
	private String pickupCity;

	@Column(name = "sender_mail_id")
	private String senderMailId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pickup_date")
	private Date pickupDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp")
	private Date createdTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pulled_time")
	private Date pulledTime;

	@Column(name = "pickup_order_no")
	private String pickupOrderNo;

	@Column(name = "pickup_pincode")
	private int pickupPincode;

	@Column(name = "pickup_register_id")
	private int pickupRegisterId;

	@Column(name = "pickup_schedule_id")
	private int pickupScheduleId;

	private int pieces;

	@Column(name = "user_id")
	private String userId;

	private float weight;

	@Column(name = "is_oda")
	private int isOda;

	@Column(name = "is_recurring")
	private int isRecurring;

	
	@Column(name = "vehicle_type")
	private String vehicleType;

	@Column(name = "vehicle_number")
	private String vehicleNumber;

	@Column(name = "is_reassigned")
	private int isReassigned;

	@Column(name = "user_before_reassign")
	private String userBeforeReassign;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "reassign_datetime")
	private Date reassignDateTime;

	@Column(name = "vehicle_capacity")
	private String vehicleCapacity;

	@Column(name = "product_type")
	private String productType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "valid_till")
	private Date validTill;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_attempted_time")
	private Date updatedAttemptedTime;

	@Column(name = "updated_attempted_flag")
	private String updatedAttemptedFlag;

	/* 02-09-2020 -ak */
	@Column(name = "is_con_note_enabled")
	private int conNoteEnabled;

	@Column(name = "is_one_order_one_con")
	private int oneOrderOnecon;
	/**/

	// bi-directional many-to-one association to PickupDataAcknowledgement
	@OneToMany(mappedBy = "pudData")
	private List<PickupDataAcknowledgement> pickupDataAcknowledgements;

	public PudData() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getCft() {
		return this.cft;
	}

	public void setCft(float cft) {
		this.cft = cft;
	}

	public String getContactMobile() {
		return this.contactMobile;
	}

	public void setContactMobile(String contactMobile) {
		this.contactMobile = contactMobile;
	}

	public Date getPulledTime() {
		return this.pulledTime;
	}

	public void setPulledTime(Date pulledTime) {
		this.pulledTime = pulledTime;
	}

	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerRefNumber() {
		return this.customerRefNumber;
	}

	public void setCustomerRefNumber(String customerRefNumber) {
		this.customerRefNumber = customerRefNumber;
	}

	public String getDeliveryAddress() {
		return this.deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getDeliveryCity() {
		return this.deliveryCity;
	}

	public void setDeliveryCity(String deliveryCity) {
		this.deliveryCity = deliveryCity;
	}

	public int getDeliveryPincode() {
		return this.deliveryPincode;
	}

	public void setDeliveryPincode(int deliveryPincode) {
		this.deliveryPincode = deliveryPincode;
	}

	public float getMinCfgWeight() {
		return this.minCfgWeight;
	}

	public void setMinCfgWeight(float minCfgWeight) {
		this.minCfgWeight = minCfgWeight;
	}

	public String getPickupAddress() {
		return this.pickupAddress;
	}

	public void setPickupAddress(String pickupAddress) {
		this.pickupAddress = pickupAddress;
	}

	public String getPickupCity() {
		return this.pickupCity;
	}

	public void setPickupCity(String pickupCity) {
		this.pickupCity = pickupCity;
	}

	public Date getPickupDate() {
		return this.pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public String getPickupOrderNo() {
		return this.pickupOrderNo;
	}

	public void setPickupOrderNo(String pickupOrderNo) {
		this.pickupOrderNo = pickupOrderNo;
	}

	public int getPickupPincode() {
		return this.pickupPincode;
	}

	public void setPickupPincode(int pickupPincode) {
		this.pickupPincode = pickupPincode;
	}

	public int getPickupRegisterId() {
		return this.pickupRegisterId;
	}

	public void setPickupRegisterId(int pickupRegisterId) {
		this.pickupRegisterId = pickupRegisterId;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public int getPieces() {
		return this.pieces;
	}

	public void setPieces(int pieces) {
		this.pieces = pieces;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public float getWeight() {
		return this.weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getSenderMailId() {
		return senderMailId;
	}

	public void setSenderMailId(String senderMailId) {
		this.senderMailId = senderMailId;
	}

	public List<PickupDataAcknowledgement> getPickupDataAcknowledgements() {
		return this.pickupDataAcknowledgements;
	}

	public void setPickupDataAcknowledgements(List<PickupDataAcknowledgement> pickupDataAcknowledgements) {
		this.pickupDataAcknowledgements = pickupDataAcknowledgements;
	}

	public PickupDataAcknowledgement addPickupDataAcknowledgement(PickupDataAcknowledgement pickupDataAcknowledgement) {
		getPickupDataAcknowledgements().add(pickupDataAcknowledgement);
		pickupDataAcknowledgement.setPudData(this);

		return pickupDataAcknowledgement;
	}

	public PickupDataAcknowledgement removePickupDataAcknowledgement(
			PickupDataAcknowledgement pickupDataAcknowledgement) {
		getPickupDataAcknowledgements().remove(pickupDataAcknowledgement);
		pickupDataAcknowledgement.setPudData(null);

		return pickupDataAcknowledgement;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public int getIsOda() {
		return isOda;
	}

	public void setIsOda(int isOda) {
		this.isOda = isOda;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public String getVehicleCapacity() {
		return vehicleCapacity;
	}

	public void setVehicleCapacity(String vehicleCapacity) {
		this.vehicleCapacity = vehicleCapacity;
	}

	public double getPickupLatitude() {
		return pickupLatitude;
	}

	public void setPickupLatitude(double pickupLatitude) {
		this.pickupLatitude = pickupLatitude;
	}

	public double getPickupLongitude() {
		return pickupLongitude;
	}

	public void setPickupLongitude(double pickupLongitude) {
		this.pickupLongitude = pickupLongitude;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public Date getUpdatedAttemptedTime() {
		return updatedAttemptedTime;
	}

	public void setUpdatedAttemptedTime(Date updatedAttemptedTime) {
		this.updatedAttemptedTime = updatedAttemptedTime;
	}

	public String getUpdatedAttemptedFlag() {
		return updatedAttemptedFlag;
	}

	public void setUpdatedAttemptedFlag(String updatedAttemptedFlag) {
		this.updatedAttemptedFlag = updatedAttemptedFlag;
	}

	public int getIsReassigned() {
		return isReassigned;
	}

	public void setIsReassigned(int isReassigned) {
		this.isReassigned = isReassigned;
	}

	public String getUserBeforeReassign() {
		return userBeforeReassign;
	}

	public void setUserBeforeReassign(String userBeforeReassign) {
		this.userBeforeReassign = userBeforeReassign;
	}

	public Date getReassignDateTime() {
		return reassignDateTime;
	}

	public void setReassignDateTime(Date reassignDateTime) {
		this.reassignDateTime = reassignDateTime;
	}

	/** added on Sep 2, 2020 **/
	public int getConNoteEnabled() {
		return conNoteEnabled;
	}

	/** added on Sep 2, 2020 **/
	public void setConNoteEnabled(int conNoteEnabled) {
		this.conNoteEnabled = conNoteEnabled;
	}

	/** added on Sep 2, 2020 **/
	public int getOneOrderOnecon() {
		return oneOrderOnecon;
	}

	/** added on Sep 2, 2020 **/
	public void setOneOrderOnecon(int oneOrderOnecon) {
		this.oneOrderOnecon = oneOrderOnecon;
	}

	public int getIsRecurring() {
		return isRecurring;
	}

	public void setIsRecurring(int isRecurring) {
		this.isRecurring = isRecurring;
	}

	
	
}