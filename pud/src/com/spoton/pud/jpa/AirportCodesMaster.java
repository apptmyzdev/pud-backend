package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the airport_codes_master database table.
 * 
 */
@Entity
@Table(name="airport_codes_master")
@NamedQuery(name="AirportCodesMaster.findAll", query="SELECT a FROM AirportCodesMaster a")
public class AirportCodesMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="airport_code")
	private String airportCode;

	@Column(name="airport_desc")
	private String airportDesc;

	@Column(name="airport_id")
	private String airportId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	public AirportCodesMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAirportCode() {
		return this.airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportDesc() {
		return this.airportDesc;
	}

	public void setAirportDesc(String airportDesc) {
		this.airportDesc = airportDesc;
	}

	public String getAirportId() {
		return this.airportId;
	}

	public void setAirportId(String airportId) {
		this.airportId = airportId;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

}