package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the device_application_list database table.
 * 
 */
@Entity
@Table(name="device_application_list")
@NamedQuery(name="DeviceApplicationList.findAll", query="SELECT d FROM DeviceApplicationList d")
public class DeviceApplicationList implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private DeviceApplicationListPK id;

	@Column(name="application_name")
	private String applicationName;

	@Column(name="enable_disable")
	private int enableDisable;

	private Timestamp ts;

	private String version;

	public DeviceApplicationList() {
	}

	public DeviceApplicationListPK getId() {
		return this.id;
	}

	public void setId(DeviceApplicationListPK id) {
		this.id = id;
	}

	public String getApplicationName() {
		return this.applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public int getEnableDisable() {
		return this.enableDisable;
	}

	public void setEnableDisable(int enableDisable) {
		this.enableDisable = enableDisable;
	}

	public Timestamp getTs() {
		return this.ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}