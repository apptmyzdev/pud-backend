package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the push_message_device_map database table.
 * 
 */
@Entity
@Table(name="push_message_device_map")
@NamedQuery(name="PushMessageDeviceMap.findAll", query="SELECT p FROM PushMessageDeviceMap p")
public class PushMessageDeviceMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int active;

	private int attempts;

	private int commandExecutedStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date commandExecutedTimestamp;

	private int deviceId;

	private int messageReceivedStatus;

	@Temporal(TemporalType.TIMESTAMP)
	private Date messageReceivedTimestamp;

	private int pushMessageId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedTimestamp;

	public PushMessageDeviceMap() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActive() {
		return this.active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public int getAttempts() {
		return this.attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public int getCommandExecutedStatus() {
		return this.commandExecutedStatus;
	}

	public void setCommandExecutedStatus(int commandExecutedStatus) {
		this.commandExecutedStatus = commandExecutedStatus;
	}

	public Date getCommandExecutedTimestamp() {
		return this.commandExecutedTimestamp;
	}

	public void setCommandExecutedTimestamp(Date commandExecutedTimestamp) {
		this.commandExecutedTimestamp = commandExecutedTimestamp;
	}

	public int getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getMessageReceivedStatus() {
		return this.messageReceivedStatus;
	}

	public void setMessageReceivedStatus(int messageReceivedStatus) {
		this.messageReceivedStatus = messageReceivedStatus;
	}

	public Date getMessageReceivedTimestamp() {
		return this.messageReceivedTimestamp;
	}

	public void setMessageReceivedTimestamp(Date messageReceivedTimestamp) {
		this.messageReceivedTimestamp = messageReceivedTimestamp;
	}

	public int getPushMessageId() {
		return this.pushMessageId;
	}

	public void setPushMessageId(int pushMessageId) {
		this.pushMessageId = pushMessageId;
	}

	public Date getUpdatedTimestamp() {
		return this.updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

}