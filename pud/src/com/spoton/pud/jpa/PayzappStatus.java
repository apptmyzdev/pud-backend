package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the payzapp_status database table.
 * 
 */
@Entity
@Table(name="payzapp_status")
@NamedQuery(name="PayzappStatus.findAll", query="SELECT p FROM PayzappStatus p")
public class PayzappStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="agent_id")
	private String agentId;

	@Column(name="con_number")
	private String conNumber;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="pay_amount")
	private double payAmount;

	@Column(name="raised_location")
	private String raisedLocation;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="request_timestamp")
	private Date requestTimestamp;

	@Column(name="response_message")
	private String responseMessage;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="response_timestamp")
	private Date responseTimestamp;

	private String status;

	public PayzappStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgentId() {
		return this.agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public double getPayAmount() {
		return this.payAmount;
	}

	public void setPayAmount(double payAmount) {
		this.payAmount = payAmount;
	}

	public String getRaisedLocation() {
		return this.raisedLocation;
	}

	public void setRaisedLocation(String raisedLocation) {
		this.raisedLocation = raisedLocation;
	}

	public Date getRequestTimestamp() {
		return this.requestTimestamp;
	}

	public void setRequestTimestamp(Date requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}

	public String getResponseMessage() {
		return this.responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Date getResponseTimestamp() {
		return this.responseTimestamp;
	}

	public void setResponseTimestamp(Date responseTimestamp) {
		this.responseTimestamp = responseTimestamp;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}