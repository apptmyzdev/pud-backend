package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the cash_booking_response database table.
 * 
 */
@Entity
@Table(name="cash_booking_response")
@NamedQuery(name="CashBookingResponse.findAll", query="SELECT c FROM CashBookingResponse c")
public class CashBookingResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="actual_weight")
	private String actualWeight;

	private String cgst;

	@Column(name="charge_amount")
	private String chargeAmount;

	@Column(name="charge_weight")
	private String chargeWeight;

	@Column(name="con_entry_id")
	private int conEntryId;

	@Column(name="con_no")
	private String conNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="customer_gst")
	private String customerGst;

	@Column(name="destination_code")
	private String destinationCode;

	private String flag;

	private String igst;

	@Column(name="invoice_date")
	private String invoiceDate;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="net_amount")
	private String netAmount;

	@Column(name="origin_code")
	private String originCode;

	private String sgst;

	@Column(name="spoton_address")
	private String spotonAddress;

	@Column(name="spoton_gst")
	private String spotonGst;

	@Column(name="status_details")
	private String statusDetails;

	@Column(name="tax_amount")
	private String taxAmount;

	@Column(name="total_packages")
	private String totalPackages;
	
	@Column(name="customer_email")
	private String customerEmail;
	
	@Column(name="customer_phone")
	private String customerPhone;
	
	@Column(name="customer_gstin")
	private String customerGstin;

	@Column(name="spoton_gst_state_name")
	private String spotonGstStateName;
	
	@Column(name="shipper_gst_state_name")
	private String shipperGstStateName;

	private String ugst;

	public CashBookingResponse() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActualWeight() {
		return this.actualWeight;
	}

	public void setActualWeight(String actualWeight) {
		this.actualWeight = actualWeight;
	}

	public String getCgst() {
		return this.cgst;
	}

	public void setCgst(String cgst) {
		this.cgst = cgst;
	}

	public String getChargeAmount() {
		return this.chargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getChargeWeight() {
		return this.chargeWeight;
	}

	public void setChargeWeight(String chargeWeight) {
		this.chargeWeight = chargeWeight;
	}

	public int getConEntryId() {
		return this.conEntryId;
	}

	public void setConEntryId(int conEntryId) {
		this.conEntryId = conEntryId;
	}

	public String getConNo() {
		return this.conNo;
	}

	public void setConNo(String conNo) {
		this.conNo = conNo;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCustomerGst() {
		return this.customerGst;
	}

	public void setCustomerGst(String customerGst) {
		this.customerGst = customerGst;
	}

	public String getDestinationCode() {
		return this.destinationCode;
	}

	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getIgst() {
		return this.igst;
	}

	public void setIgst(String igst) {
		this.igst = igst;
	}

	public String getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getNetAmount() {
		return this.netAmount;
	}

	public void setNetAmount(String netAmount) {
		this.netAmount = netAmount;
	}

	public String getOriginCode() {
		return this.originCode;
	}

	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}

	public String getSgst() {
		return this.sgst;
	}

	public void setSgst(String sgst) {
		this.sgst = sgst;
	}

	public String getSpotonAddress() {
		return this.spotonAddress;
	}

	public void setSpotonAddress(String spotonAddress) {
		this.spotonAddress = spotonAddress;
	}

	public String getSpotonGst() {
		return this.spotonGst;
	}

	public void setSpotonGst(String spotonGst) {
		this.spotonGst = spotonGst;
	}

	public String getStatusDetails() {
		return this.statusDetails;
	}

	public void setStatusDetails(String statusDetails) {
		this.statusDetails = statusDetails;
	}

	public String getTaxAmount() {
		return this.taxAmount;
	}

	public void setTaxAmount(String taxAmount) {
		this.taxAmount = taxAmount;
	}

	public String getTotalPackages() {
		return this.totalPackages;
	}

	public void setTotalPackages(String totalPackages) {
		this.totalPackages = totalPackages;
	}

	public String getUgst() {
		return this.ugst;
	}

	public void setUgst(String ugst) {
		this.ugst = ugst;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getCustomerGstin() {
		return customerGstin;
	}

	public void setCustomerGstin(String customerGstin) {
		this.customerGstin = customerGstin;
	}

	public String getSpotonGstStateName() {
		return spotonGstStateName;
	}

	public void setSpotonGstStateName(String spotonGstStateName) {
		this.spotonGstStateName = spotonGstStateName;
	}

	public String getShipperGstStateName() {
		return shipperGstStateName;
	}

	public void setShipperGstStateName(String shipperGstStateName) {
		this.shipperGstStateName = shipperGstStateName;
	}

	
	
}