package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the pieces database table.
 * 
 */
@Entity
@Table(name="pieces")
@NamedQuery(name="Piece.findAll", query="SELECT p FROM Piece p")
public class Piece implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="last_piece_no")
	private String lastPieceNo;

	@Column(name="pcr_key")
	private int pcrKey;

	@Column(name="total_used")
	private String totalUsed;

	//bi-directional many-to-one association to ConDetail
	@ManyToOne
	@JoinColumn(name="con_id")
	private ConDetail conDetail;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="con_number")
	private String conNumber;
	
	@Column(name="erp_updated")
	private int erpUpdated;
	
	@Column(name="pickup_type")
	private int pickupType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="customer_code")
	private String customerCode;
	
	public Piece() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastPieceNo() {
		return this.lastPieceNo;
	}

	public void setLastPieceNo(String lastPieceNo) {
		this.lastPieceNo = lastPieceNo;
	}

	public int getPcrKey() {
		return this.pcrKey;
	}

	public void setPcrKey(int pcrKey) {
		this.pcrKey = pcrKey;
	}

	public String getTotalUsed() {
		return this.totalUsed;
	}

	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}

	public ConDetail getConDetail() {
		return this.conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public int getErpUpdated() {
		return erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	
	
}