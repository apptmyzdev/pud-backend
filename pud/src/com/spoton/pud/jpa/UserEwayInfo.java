package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_eway_info database table.
 * 
 */
@Entity
@Table(name="user_eway_info")
@NamedQuery(name="UserEwayInfo.findAll", query="SELECT u FROM UserEwayInfo u")
public class UserEwayInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="eway_flag")
	private int ewayFlag;

	@Column(name="sc_code")
	private String scCode;

	@Column(name="user_name")
	private String userName;

	@Column(name="universal_amount")
	private double universalAmount;
	
	@Column(name="active_flag")
	private int activeFlag;

	
	private double amount;
	
	public UserEwayInfo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEwayFlag() {
		return this.ewayFlag;
	}

	public void setEwayFlag(int ewayFlag) {
		this.ewayFlag = ewayFlag;
	}

	public String getScCode() {
		return this.scCode;
	}

	public void setScCode(String scCode) {
		this.scCode = scCode;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getUniversalAmount() {
		return universalAmount;
	}

	public void setUniversalAmount(double universalAmount) {
		this.universalAmount = universalAmount;
	}

	public int getActiveFlag() {
		return activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

	
	
}