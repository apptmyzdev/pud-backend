package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user_data_master database table.
 * 
 */
@Entity
@Table(name="user_data_master")
@NamedQuery(name="UserDataMaster.findAll", query="SELECT u FROM UserDataMaster u")
public class UserDataMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="app_pwd")
	private String appPwd;

	@Column(name="app_user_name")
	private String appUserName;

	@Column(name="currently_used")
	private String currentlyUsed;

	@Column(name="is_9digit_part_no")
	private int is9digitPartNo;

	@Column(name="is_active")
	private int isActive;

	@Column(name="is_con_entry")
	private int isConEntry;

	@Column(name="is_delivery")
	private int isDelivery;

	@Column(name="is_offline_printing")
	private int isOfflinePrinting;

	@Column(name="is_pickup_app_active")
	private int isPickupAppActive;

	@Column(name="is_pickup_registration")
	private int isPickupRegistration;

	@Column(name="is_pieces_entry")
	private int isPiecesEntry;

	private String location;

	@Column(name="login_status")
	private int loginStatus;

	@Column(name="printer_sl_no")
	private String printerSlNo;

	@Column(name="tab_id")
	private String tabId;

	@Column(name="user_id")
	private int userId;

	@Column(name="verison_id")
	private String verisonId;

	//bi-directional many-to-one association to UserLocation
	@OneToMany(mappedBy="userDataMaster")
	private List<UserLocation> userLocations;

	public UserDataMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAppPwd() {
		return this.appPwd;
	}

	public void setAppPwd(String appPwd) {
		this.appPwd = appPwd;
	}

	public String getAppUserName() {
		return this.appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getCurrentlyUsed() {
		return this.currentlyUsed;
	}

	public void setCurrentlyUsed(String currentlyUsed) {
		this.currentlyUsed = currentlyUsed;
	}

	public int getIs9digitPartNo() {
		return this.is9digitPartNo;
	}

	public void setIs9digitPartNo(int is9digitPartNo) {
		this.is9digitPartNo = is9digitPartNo;
	}

	public int getIsActive() {
		return this.isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public int getIsConEntry() {
		return this.isConEntry;
	}

	public void setIsConEntry(int isConEntry) {
		this.isConEntry = isConEntry;
	}

	public int getIsDelivery() {
		return this.isDelivery;
	}

	public void setIsDelivery(int isDelivery) {
		this.isDelivery = isDelivery;
	}

	public int getIsOfflinePrinting() {
		return this.isOfflinePrinting;
	}

	public void setIsOfflinePrinting(int isOfflinePrinting) {
		this.isOfflinePrinting = isOfflinePrinting;
	}

	public int getIsPickupAppActive() {
		return this.isPickupAppActive;
	}

	public void setIsPickupAppActive(int isPickupAppActive) {
		this.isPickupAppActive = isPickupAppActive;
	}

	public int getIsPickupRegistration() {
		return this.isPickupRegistration;
	}

	public void setIsPickupRegistration(int isPickupRegistration) {
		this.isPickupRegistration = isPickupRegistration;
	}

	public int getIsPiecesEntry() {
		return this.isPiecesEntry;
	}

	public void setIsPiecesEntry(int isPiecesEntry) {
		this.isPiecesEntry = isPiecesEntry;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getLoginStatus() {
		return this.loginStatus;
	}

	public void setLoginStatus(int loginStatus) {
		this.loginStatus = loginStatus;
	}

	public String getPrinterSlNo() {
		return this.printerSlNo;
	}

	public void setPrinterSlNo(String printerSlNo) {
		this.printerSlNo = printerSlNo;
	}

	public String getTabId() {
		return this.tabId;
	}

	public void setTabId(String tabId) {
		this.tabId = tabId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getVerisonId() {
		return this.verisonId;
	}

	public void setVerisonId(String verisonId) {
		this.verisonId = verisonId;
	}

	public List<UserLocation> getUserLocations() {
		return this.userLocations;
	}

	public void setUserLocations(List<UserLocation> userLocations) {
		this.userLocations = userLocations;
	}

	public UserLocation addUserLocation(UserLocation userLocation) {
		getUserLocations().add(userLocation);
		userLocation.setUserDataMaster(this);

		return userLocation;
	}

	public UserLocation removeUserLocation(UserLocation userLocation) {
		getUserLocations().remove(userLocation);
		userLocation.setUserDataMaster(null);

		return userLocation;
	}

}