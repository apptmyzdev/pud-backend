package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the payzapp_request_link_input database table.
 * 
 */
@Entity
@Table(name="payzapp_request_link_input")
@NamedQuery(name="PayzappRequestLinkInput.findAll", query="SELECT p FROM PayzappRequestLinkInput p")
public class PayzappRequestLinkInput implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="customer_code")
	private String customerCode;

	private String flag;

	@Column(name="invoice_number")
	private String invoiceNumber;

	@Column(name="mobile_no")
	private String mobileNo;

	@Column(name="ptms_email")
	private String ptmsEmail;

	@Column(name="ptms_ptnm")
	private String ptmsPtnm;

	@Column(name="pud_agent_mobile_number")
	private String pudAgentMobileNumber;

	private String remarks;

	@Column(name="total_invoice_amount")
	private double totalInvoiceAmount;

	@Column(name="agent_id")
	private String agentId;
	
	
	public PayzappRequestLinkInput() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPtmsEmail() {
		return this.ptmsEmail;
	}

	public void setPtmsEmail(String ptmsEmail) {
		this.ptmsEmail = ptmsEmail;
	}

	public String getPtmsPtnm() {
		return this.ptmsPtnm;
	}

	public void setPtmsPtnm(String ptmsPtnm) {
		this.ptmsPtnm = ptmsPtnm;
	}

	public String getPudAgentMobileNumber() {
		return this.pudAgentMobileNumber;
	}

	public void setPudAgentMobileNumber(String pudAgentMobileNumber) {
		this.pudAgentMobileNumber = pudAgentMobileNumber;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public double getTotalInvoiceAmount() {
		return this.totalInvoiceAmount;
	}

	public void setTotalInvoiceAmount(double totalInvoiceAmount) {
		this.totalInvoiceAmount = totalInvoiceAmount;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	
	
}