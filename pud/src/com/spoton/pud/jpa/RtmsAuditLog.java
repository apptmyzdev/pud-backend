package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the rtms_audit_logs database table.
 * 
 */
@Entity
@Table(name="rtms_audit_logs")
@NamedQuery(name="RtmsAuditLog.findAll", query="SELECT r FROM RtmsAuditLog r")
public class RtmsAuditLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="input_parameters")
	private String inputParameters;

	@Column(name="logged_in_emp_branch")
	private String loggedInEmpBranch;

	@Column(name="logged_in_emp_code")
	private String loggedInEmpCode;

	@Column(name="output_message")
	private String outputMessage;

	@Column(name="service_type")
	private String serviceType;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	public RtmsAuditLog() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInputParameters() {
		return this.inputParameters;
	}

	public void setInputParameters(String inputParameters) {
		this.inputParameters = inputParameters;
	}

	public String getLoggedInEmpBranch() {
		return this.loggedInEmpBranch;
	}

	public void setLoggedInEmpBranch(String loggedInEmpBranch) {
		this.loggedInEmpBranch = loggedInEmpBranch;
	}

	public String getLoggedInEmpCode() {
		return this.loggedInEmpCode;
	}

	public void setLoggedInEmpCode(String loggedInEmpCode) {
		this.loggedInEmpCode = loggedInEmpCode;
	}

	public String getOutputMessage() {
		return this.outputMessage;
	}

	public void setOutputMessage(String outputMessage) {
		this.outputMessage = outputMessage;
	}

	public String getServiceType() {
		return this.serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

}