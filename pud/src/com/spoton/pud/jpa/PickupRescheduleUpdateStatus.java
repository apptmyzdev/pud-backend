package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the pickup_reschedule_update_status database table.
 * 
 */
@Entity
@Table(name="pickup_reschedule_update_status")
@NamedQuery(name="PickupRescheduleUpdateStatus.findAll", query="SELECT p FROM PickupRescheduleUpdateStatus p")
public class PickupRescheduleUpdateStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;

	@Column(name="transaction_result")
	private String transactionResult;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;
	
	public PickupRescheduleUpdateStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getTransactionResult() {
		return this.transactionResult;
	}

	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
}