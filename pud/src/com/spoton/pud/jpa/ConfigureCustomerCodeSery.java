package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the configure_customer_code_series database table.
 * 
 */
@Entity
@Table(name="configure_customer_code_series")
@NamedQuery(name="ConfigureCustomerCodeSery.findAll", query="SELECT c FROM ConfigureCustomerCodeSery c")
public class ConfigureCustomerCodeSery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="active_flag")
	private int activeFlag;

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="customer_code_starting_series_number")
	private String customerCodeStartingSeriesNumber;

	public ConfigureCustomerCodeSery() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCustomerCodeStartingSeriesNumber() {
		return this.customerCodeStartingSeriesNumber;
	}

	public void setCustomerCodeStartingSeriesNumber(String customerCodeStartingSeriesNumber) {
		this.customerCodeStartingSeriesNumber = customerCodeStartingSeriesNumber;
	}

}