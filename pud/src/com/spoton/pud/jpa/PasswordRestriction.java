package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the password_restriction database table.
 * 
 */
@Entity
@Table(name="password_restriction")
public class PasswordRestriction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="complex_password")
	private int complexPassword;

	@Column(name="device_id", nullable=false)
	private int deviceId;

	@Column(name="maximum_attempts")
	private int maximumAttempts;

	@Column(name="medium_password")
	private int mediumPassword;

	@Column(name="minimum_length")
	private int minimumLength;

	@Column(name="no_restriction")
	private int noRestriction;

	@Column(name="password_expiration")
	private int passwordExpiration;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	@Column(name="weak_password")
	private int weakPassword;

	public PasswordRestriction() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getComplexPassword() {
		return this.complexPassword;
	}

	public void setComplexPassword(int complexPassword) {
		this.complexPassword = complexPassword;
	}

	public int getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getMaximumAttempts() {
		return this.maximumAttempts;
	}

	public void setMaximumAttempts(int maximumAttempts) {
		this.maximumAttempts = maximumAttempts;
	}

	public int getMediumPassword() {
		return this.mediumPassword;
	}

	public void setMediumPassword(int mediumPassword) {
		this.mediumPassword = mediumPassword;
	}

	public int getMinimumLength() {
		return this.minimumLength;
	}

	public void setMinimumLength(int minimumLength) {
		this.minimumLength = minimumLength;
	}

	public int getNoRestriction() {
		return this.noRestriction;
	}

	public void setNoRestriction(int noRestriction) {
		this.noRestriction = noRestriction;
	}

	public int getPasswordExpiration() {
		return this.passwordExpiration;
	}

	public void setPasswordExpiration(int passwordExpiration) {
		this.passwordExpiration = passwordExpiration;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getWeakPassword() {
		return this.weakPassword;
	}

	public void setWeakPassword(int weakPassword) {
		this.weakPassword = weakPassword;
	}

}