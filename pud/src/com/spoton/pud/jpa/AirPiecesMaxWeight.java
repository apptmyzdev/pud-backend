package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the air_pieces_max_weight database table.
 * 
 */
@Entity
@Table(name="air_pieces_max_weight")
@NamedQuery(name="AirPiecesMaxWeight.findAll", query="SELECT a FROM AirPiecesMaxWeight a")
public class AirPiecesMaxWeight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="air_piece_max_weight")
	private double airPieceMaxWeight;

	@Column(name="created_timestamp")
	private Timestamp createdTimestamp;

	@Column(name="is_active")
	private int isActive;

	public AirPiecesMaxWeight() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAirPieceMaxWeight() {
		return this.airPieceMaxWeight;
	}

	public void setAirPieceMaxWeight(double airPieceMaxWeight) {
		this.airPieceMaxWeight = airPieceMaxWeight;
	}

	public Timestamp getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public int getIsActive() {
		return this.isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

}