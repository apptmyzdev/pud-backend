package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the added_pieces_images database table.
 * 
 */
@Entity
@Table(name="added_pieces_images")
@NamedQuery(name="AddedPiecesImage.findAll", query="SELECT a FROM AddedPiecesImage a")
public class AddedPiecesImage implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="pieces_images_url")
	private String piecesImagesUrl;

	
	@Column(name="con_number")
	private String conNumber;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;
	//bi-directional many-to-one association to AddedCon
	@ManyToOne
	@JoinColumn(name="con_id")
	private AddedCon addedCon;

	public AddedPiecesImage() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getPiecesImagesUrl() {
		return this.piecesImagesUrl;
	}

	public void setPiecesImagesUrl(String piecesImagesUrl) {
		this.piecesImagesUrl = piecesImagesUrl;
	}

	public AddedCon getAddedCon() {
		return this.addedCon;
	}

	public void setAddedCon(AddedCon addedCon) {
		this.addedCon = addedCon;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}