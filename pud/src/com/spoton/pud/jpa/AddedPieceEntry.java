package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the added_piece_entry database table.
 * 
 */
@Entity
@Table(name="added_piece_entry")
@NamedQuery(name="AddedPieceEntry.findAll", query="SELECT a FROM AddedPieceEntry a")
public class AddedPieceEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="from_piece_no")
	private String fromPieceNo;

	@Column(name="pcr_key")
	private int pcrKey;

	@Column(name="to_piece_no")
	private String toPieceNo;

	@Column(name="total_pieces")
	private String totalPieces;
	
	@Column(name="con_number")
	private String conNumber;
	
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;

	//bi-directional many-to-one association to AddedCon
	@ManyToOne
	@JoinColumn(name="con_id")
	private AddedCon addedCon;

	public AddedPieceEntry() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getFromPieceNo() {
		return this.fromPieceNo;
	}

	public void setFromPieceNo(String fromPieceNo) {
		this.fromPieceNo = fromPieceNo;
	}

	public int getPcrKey() {
		return this.pcrKey;
	}

	public void setPcrKey(int pcrKey) {
		this.pcrKey = pcrKey;
	}

	public String getToPieceNo() {
		return this.toPieceNo;
	}

	public void setToPieceNo(String toPieceNo) {
		this.toPieceNo = toPieceNo;
	}

	public String getTotalPieces() {
		return this.totalPieces;
	}

	public void setTotalPieces(String totalPieces) {
		this.totalPieces = totalPieces;
	}

	public AddedCon getAddedCon() {
		return this.addedCon;
	}

	public void setAddedCon(AddedCon addedCon) {
		this.addedCon = addedCon;
	}
	
	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}