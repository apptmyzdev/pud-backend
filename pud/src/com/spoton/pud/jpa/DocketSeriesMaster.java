package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the docket_series_master database table.
 * 
 */
@Entity
@Table(name="docket_series_master")
@NamedQuery(name="DocketSeriesMaster.findAll", query="SELECT d FROM DocketSeriesMaster d")
public class DocketSeriesMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="branch_code")
	private String branchCode;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="dkt_user_key")
	private String dktUserKey;

	@Column(name="docket_key")
	private int docketKey;

	@Column(name="from_series")
	private String fromSeries;

	@Column(name="last_sr_number")
	private String lastSrNumber;

	@Column(name="to_series")
	private String toSeries;

	@Column(name="total_leaf")
	private String totalLeaf;

	@Column(name="user_id")
	private int userId;

	@Column(name="user_name")
	private String userName;

	@Column(name="vendor_code")
	private String vendorCode;
	
	@Column(name="product_type")
	private String productType;

	public DocketSeriesMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getDktUserKey() {
		return this.dktUserKey;
	}

	public void setDktUserKey(String dktUserKey) {
		this.dktUserKey = dktUserKey;
	}

	public int getDocketKey() {
		return this.docketKey;
	}

	public void setDocketKey(int docketKey) {
		this.docketKey = docketKey;
	}

	public String getFromSeries() {
		return this.fromSeries;
	}

	public void setFromSeries(String fromSeries) {
		this.fromSeries = fromSeries;
	}

	public String getLastSrNumber() {
		return this.lastSrNumber;
	}

	public void setLastSrNumber(String lastSrNumber) {
		this.lastSrNumber = lastSrNumber;
	}

	public String getToSeries() {
		return this.toSeries;
	}

	public void setToSeries(String toSeries) {
		this.toSeries = toSeries;
	}

	public String getTotalLeaf() {
		return this.totalLeaf;
	}

	public void setTotalLeaf(String totalLeaf) {
		this.totalLeaf = totalLeaf;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	
}