package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the dangerous_code_master database table.
 * 
 */
@Entity
@Table(name="dangerous_code_master")
@NamedQuery(name="DangerousCodeMaster.findAll", query="SELECT d FROM DangerousCodeMaster d")
public class DangerousCodeMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="dangerous_code")
	private String dangerousCode;

	@Column(name="dangerous_desc")
	private String dangerousDesc;

	@Column(name="dangerous_id")
	private String dangerousId;

	public DangerousCodeMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getDangerousCode() {
		return this.dangerousCode;
	}

	public void setDangerousCode(String dangerousCode) {
		this.dangerousCode = dangerousCode;
	}

	public String getDangerousDesc() {
		return this.dangerousDesc;
	}

	public void setDangerousDesc(String dangerousDesc) {
		this.dangerousDesc = dangerousDesc;
	}

	public String getDangerousId() {
		return this.dangerousId;
	}

	public void setDangerousId(String dangerousId) {
		this.dangerousId = dangerousId;
	}

}