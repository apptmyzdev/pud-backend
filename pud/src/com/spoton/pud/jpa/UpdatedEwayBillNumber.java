package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the updated_eway_bill_numbers database table.
 * 
 */
@Entity
@Table(name="updated_eway_bill_numbers")
@NamedQuery(name="UpdatedEwayBillNumber.findAll", query="SELECT u FROM UpdatedEwayBillNumber u")
public class UpdatedEwayBillNumber implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="eway_bill_number")
	private String ewayBillNumber;
	
	@Column(name="invoice_number")
	private String invoiceNumber;;
	
	@Column(name="invoice_amount")
	private double invoiceAmount;
	
	@Column(name="is_exempted")
	private String isExempted;
	

	//bi-directional many-to-one association to ConDetail
	@ManyToOne
	@JoinColumn(name="con_id")
	private ConDetail conDetail;

	@Column(name="con_number")
	private String conNumber;
	
	@Column(name="erp_updated")
	private int erpUpdated;
	
	@Column(name="pickup_type")
	private int pickupType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pickup_date")
	private Date pickupDate;

	@Column(name="pickup_schedule_id")
	private int pickupScheduleId;
	
	@Column(name="user_name")
	private String userName;
	
	@Column(name="customer_code")
	private String customerCode;
	
	
	
	public UpdatedEwayBillNumber() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getEwayBillNumber() {
		return this.ewayBillNumber;
	}

	public void setEwayBillNumber(String ewayBillNumber) {
		this.ewayBillNumber = ewayBillNumber;
	}

	public ConDetail getConDetail() {
		return this.conDetail;
	}

	public void setConDetail(ConDetail conDetail) {
		this.conDetail = conDetail;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public double getInvoiceAmount() {
		return invoiceAmount;
	}

	public void setInvoiceAmount(double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	public String getIsExempted() {
		return isExempted;
	}

	public void setIsExempted(String isExempted) {
		this.isExempted = isExempted;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public int getErpUpdated() {
		return erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public int getPickupType() {
		return pickupType;
	}

	public void setPickupType(int pickupType) {
		this.pickupType = pickupType;
	}

	public Date getPickupDate() {
		return pickupDate;
	}

	public void setPickupDate(Date pickupDate) {
		this.pickupDate = pickupDate;
	}

	public int getPickupScheduleId() {
		return pickupScheduleId;
	}

	public void setPickupScheduleId(int pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	
}