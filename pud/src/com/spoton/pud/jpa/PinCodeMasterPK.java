package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the pin_code_master database table.
 * 
 */
@Embeddable
public class PinCodeMasterPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="pin_code")
	private int pinCode;

	@Column(name="branch_code")
	private String branchCode;

	@Column(name="pincode_type")
	private String pincodeType;
	
	@Column(name = "account_code")
	private String accountCode;


	public PinCodeMasterPK() {
	}
	public int getPinCode() {
		return this.pinCode;
	}
	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}
	public String getBranchCode() {
		return this.branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getPincodeType() {
		return this.pincodeType;
	}
	public void setPincodeType(String pincodeType) {
		this.pincodeType = pincodeType;
	}
	
	/** added on Jul 23, 2020 **/
	public String getAccountCode() {
		return accountCode;
	}

	/** added on Jul 23, 2020 **/
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PinCodeMasterPK)) {
			return false;
		}
		PinCodeMasterPK castOther = (PinCodeMasterPK)other;
		return (this.pinCode == castOther.pinCode) && this.branchCode.equals(castOther.branchCode) && this.pincodeType.equals(castOther.pincodeType) && this.accountCode.equals(castOther.accountCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.pinCode;
		hash = hash * prime + this.branchCode.hashCode();
		hash = hash * prime + this.pincodeType.hashCode();
		hash = hash * prime + this.accountCode.hashCode();
		
		return hash;
	}
}