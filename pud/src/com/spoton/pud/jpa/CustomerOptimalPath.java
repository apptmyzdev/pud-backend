package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the customer_optimal_path database table.
 * 
 */
@Entity
@Table(name="customer_optimal_path")
@NamedQuery(name="CustomerOptimalPath.findAll", query="SELECT c FROM CustomerOptimalPath c")
public class CustomerOptimalPath implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="active_flag")
	private int activeFlag;

	private String address;

	private String city;

	@Column(name="con_number")
	private long conNumber;

	@Column(name="consignee_company")
	private String consigneeCompany;

	@Column(name="consignee_name")
	private String consigneeName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="lat_value")
	private double latValue;

	@Column(name="lng_value")
	private double lngValue;

	@Column(name="mobile_number")
	private String mobileNumber;

	@Column(name="order_number")
	private String orderNumber;

	@Column(name="pickup_schedule_id")
	private String pickupScheduleId;

	private String pincode;

	@Column(name="route_version")
	private int routeVersion;

	private String type;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_timestamp")
	private Date updatedTimestamp;

	private String username;

	@Column(name="vehicle_number")
	private String vehicleNumber;

	public CustomerOptimalPath() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(int activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(long conNumber) {
		this.conNumber = conNumber;
	}

	public String getConsigneeCompany() {
		return this.consigneeCompany;
	}

	public void setConsigneeCompany(String consigneeCompany) {
		this.consigneeCompany = consigneeCompany;
	}

	public String getConsigneeName() {
		return this.consigneeName;
	}

	public void setConsigneeName(String consigneeName) {
		this.consigneeName = consigneeName;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getLatValue() {
		return this.latValue;
	}

	public void setLatValue(double latValue) {
		this.latValue = latValue;
	}

	public double getLngValue() {
		return this.lngValue;
	}

	public void setLngValue(double lngValue) {
		this.lngValue = lngValue;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getOrderNumber() {
		return this.orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getPickupScheduleId() {
		return this.pickupScheduleId;
	}

	public void setPickupScheduleId(String pickupScheduleId) {
		this.pickupScheduleId = pickupScheduleId;
	}

	public String getPincode() {
		return this.pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public int getRouteVersion() {
		return this.routeVersion;
	}

	public void setRouteVersion(int routeVersion) {
		this.routeVersion = routeVersion;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getUpdatedTimestamp() {
		return this.updatedTimestamp;
	}

	public void setUpdatedTimestamp(Date updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getVehicleNumber() {
		return this.vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

}