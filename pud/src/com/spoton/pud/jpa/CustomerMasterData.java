package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the customer_master_data database table.
 * 
 */
@Entity
@Table(name="customer_master_data")
@NamedQuery(name="CustomerMasterData.findAll", query="SELECT c FROM CustomerMasterData c")
public class CustomerMasterData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="customer_code")
	private String customerCode;

	@Column(name="customer_address")
	private String customerAddress;

	@Column(name="customer_branch")
	private String customerBranch;

	@Column(name="customer_city")
	private String customerCity;

	private String customer_emailId;

	@Column(name="customer_mobile_number")
	private String customerMobileNumber;

	@Column(name="customer_name")
	private String customerName;

	@Column(name="customer_type")
	private String customerType;
	
	@Column(name="product_type")
	private String productType;

	private double latitude;

	private double longitude;

	@Temporal(TemporalType.TIMESTAMP)
	private Date timestamp;

	private String type;

	public CustomerMasterData() {
	}

	public String getCustomerCode() {
		return this.customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerAddress() {
		return this.customerAddress;
	}

	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}

	public String getCustomerBranch() {
		return this.customerBranch;
	}

	public void setCustomerBranch(String customerBranch) {
		this.customerBranch = customerBranch;
	}

	public String getCustomerCity() {
		return this.customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomer_emailId() {
		return this.customer_emailId;
	}

	public void setCustomer_emailId(String customer_emailId) {
		this.customer_emailId = customer_emailId;
	}

	public String getCustomerMobileNumber() {
		return this.customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	
}