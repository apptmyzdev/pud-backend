package com.spoton.pud.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the piece_master database table.
 * 
 */
@Entity
@Table(name="piece_master")
@NamedQuery(name="PieceMaster.findAll", query="SELECT p FROM PieceMaster p")
public class PieceMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String complete;

	@Column(name="end_no")
	private String endNo;

	@Column(name="last_piece_no")
	private String lastPieceNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;
	
	private int pkey;

	@Column(name="series_type")
	private String seriesType;

	@Column(name="start_no")
	private String startNo;

	@Column(name="total_isssued")
	private String totalIsssued;

	@Column(name="total_used")
	private String totalUsed;

	@Column(name="user_id")
	private String userId;

	public PieceMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComplete() {
		return this.complete;
	}

	public void setComplete(String complete) {
		this.complete = complete;
	}

	public String getEndNo() {
		return this.endNo;
	}

	public void setEndNo(String endNo) {
		this.endNo = endNo;
	}

	public String getLastPieceNo() {
		return this.lastPieceNo;
	}

	public void setLastPieceNo(String lastPieceNo) {
		this.lastPieceNo = lastPieceNo;
	}

	public int getPkey() {
		return this.pkey;
	}

	public void setPkey(int pkey) {
		this.pkey = pkey;
	}

	public String getSeriesType() {
		return this.seriesType;
	}

	public void setSeriesType(String seriesType) {
		this.seriesType = seriesType;
	}

	public String getStartNo() {
		return this.startNo;
	}

	public void setStartNo(String startNo) {
		this.startNo = startNo;
	}

	public String getTotalIsssued() {
		return this.totalIsssued;
	}

	public void setTotalIsssued(String totalIsssued) {
		this.totalIsssued = totalIsssued;
	}

	public String getTotalUsed() {
		return this.totalUsed;
	}

	public void setTotalUsed(String totalUsed) {
		this.totalUsed = totalUsed;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

}