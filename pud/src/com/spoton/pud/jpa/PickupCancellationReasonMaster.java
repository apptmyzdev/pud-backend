package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pickup_cancellation_reason_master database table.
 * 
 */
@Entity
@Table(name="pickup_cancellation_reason_master")
@NamedQuery(name="PickupCancellationReasonMaster.findAll", query="SELECT p FROM PickupCancellationReasonMaster p")
public class PickupCancellationReasonMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="active_status")
	private int activeStatus;

	@Column(name="failure_category")
	private String failureCategory;

	@Column(name="pickup_status_id")
	private int pickupStatusId;

	@Column(name="reason_code")
	private String reasonCode;

	@Column(name="reason_desc")
	private String reasonDesc;

	@Column(name="updated_by")
	private String updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="updated_on")
	private Date updatedOn;

	public PickupCancellationReasonMaster() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getActiveStatus() {
		return this.activeStatus;
	}

	public void setActiveStatus(int activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getFailureCategory() {
		return this.failureCategory;
	}

	public void setFailureCategory(String failureCategory) {
		this.failureCategory = failureCategory;
	}

	public int getPickupStatusId() {
		return this.pickupStatusId;
	}

	public void setPickupStatusId(int pickupStatusId) {
		this.pickupStatusId = pickupStatusId;
	}

	public String getReasonCode() {
		return this.reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReasonDesc() {
		return this.reasonDesc;
	}

	public void setReasonDesc(String reasonDesc) {
		this.reasonDesc = reasonDesc;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}