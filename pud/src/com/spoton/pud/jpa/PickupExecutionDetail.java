package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the pickup_execution_details database table.
 * 
 */
@Entity
@Table(name="pickup_execution_details")
@NamedQuery(name="PickupExecutionDetail.findAll", query="SELECT p FROM PickupExecutionDetail p")
public class PickupExecutionDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="con_number")
	private String conNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	@Column(name="erp_result")
	private String erpResult;

	@Column(name="erp_updated")
	private int erpUpdated;

	@Column(name="is_con_scanned")
	private String isConScanned;

	@Column(name="is_piece_scanned")
	private String isPieceScanned;

	@Column(name="piece_number")
	private String pieceNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="scanned_date_time")
	private Date scannedDateTime;

	@Column(name="user_id")
	private String userId;

	public PickupExecutionDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getConNumber() {
		return this.conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getErpResult() {
		return this.erpResult;
	}

	public void setErpResult(String erpResult) {
		this.erpResult = erpResult;
	}

	public int getErpUpdated() {
		return this.erpUpdated;
	}

	public void setErpUpdated(int erpUpdated) {
		this.erpUpdated = erpUpdated;
	}

	public String getIsConScanned() {
		return this.isConScanned;
	}

	public void setIsConScanned(String isConScanned) {
		this.isConScanned = isConScanned;
	}

	public String getIsPieceScanned() {
		return this.isPieceScanned;
	}

	public void setIsPieceScanned(String isPieceScanned) {
		this.isPieceScanned = isPieceScanned;
	}

	public String getPieceNumber() {
		return this.pieceNumber;
	}

	public void setPieceNumber(String pieceNumber) {
		this.pieceNumber = pieceNumber;
	}

	public Date getScannedDateTime() {
		return this.scannedDateTime;
	}

	public void setScannedDateTime(Date scannedDateTime) {
		this.scannedDateTime = scannedDateTime;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}