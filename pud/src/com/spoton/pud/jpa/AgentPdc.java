package com.spoton.pud.jpa;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the agent_pdc database table.
 * 
 */
@Entity
@Table(name="agent_pdc")
@NamedQuery(name="AgentPdc.findAll", query="SELECT a FROM AgentPdc a")
public class AgentPdc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="agent_id")
	private String agentId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_timestamp")
	private Date createdTimestamp;

	private String pdc;
	
	private String branch;

	@Column(name="pdc_type")
	private int pdcType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pulled_time")
	private Date pulledTime;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pushed_time")
	private Date pushedTime;

	private String sccode;

	public AgentPdc() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgentId() {
		return this.agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Date getCreatedTimestamp() {
		return this.createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public String getPdc() {
		return this.pdc;
	}

	public void setPdc(String pdc) {
		this.pdc = pdc;
	}

	public int getPdcType() {
		return this.pdcType;
	}

	public void setPdcType(int pdcType) {
		this.pdcType = pdcType;
	}

	public Date getPulledTime() {
		return this.pulledTime;
	}

	public void setPulledTime(Date pulledTime) {
		this.pulledTime = pulledTime;
	}

	public Date getPushedTime() {
		return this.pushedTime;
	}

	public void setPushedTime(Date pushedTime) {
		this.pushedTime = pushedTime;
	}

	public String getSccode() {
		return this.sccode;
	}

	public void setSccode(String sccode) {
		this.sccode = sccode;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	

}