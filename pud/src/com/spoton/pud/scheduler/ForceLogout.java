package com.spoton.pud.scheduler;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.Correct;
import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.GcmPushResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.mobileoperations.SendMessage;

/**
 * Servlet implementation class CheckLoggedInUsers
 */
@WebServlet("/forceLogout")
public class ForceLogout extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("ForceLogout");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ForceLogout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("*******START*****");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt=null;
		try{
			mt=new ManageTransaction();
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date todaysDate = format.parse(format.format(new Date()));
			logger.info("todaysDate "+todaysDate);
			
			String query="select user_name,date(created_timestamp) from user_session_data where active_flag=1";
			Query nqry = mt.createNativeQuery(query);
			List<Object[]> list=nqry.getResultList();
			List<String> userIdList=new ArrayList<String>();
			if(list!=null&&list.size()>0){
				logger.info("list size "+list.size());
				for(Object[] o:list){
					//System.out.println("equals "+todaysDate.equals((Date) o[1])+" username "+(String) o[0]+" date "+(Date) o[1]);
                    if(!(todaysDate.equals((Date) o[1]))&&!userIdList.contains((String) o[0])){
                    	userIdList.add(((String) o[0]));
                    }

				}
				//System.out.println("userIdList "+gson.toJson(userIdList));
				logger.info("userIdList "+gson.toJson(userIdList));
				if(userIdList!=null&&userIdList.size()>0){
				//Get list of pushtokens and send push message 	 
		          Map <String,String> userTokenMap = SendMessage.getPushTokens();
		          List <String> pushListTokens = new ArrayList <String>();
		          if(userTokenMap != null && userTokenMap.size() != 0)
					{
		        	 for(String user : userIdList)
						{
							if(userTokenMap.containsKey(user))
							{
								
								pushListTokens.add(userTokenMap.get(user));
							}
						}
		        	
					  if(pushListTokens!=null&&pushListTokens.size()>0){
		        	 String txtMessage = "Logging Out the User";
		        	/* List<GcmPushResponse> pushList=new ArrayList<GcmPushResponse>();
						GcmPushResponse pushResponse = new GcmPushResponse();
						pushResponse.setId(7);
						pushResponse.setMessage(txtMessage);
						pushList.add(pushResponse);
						String message = gson.toJson(pushList);
						System.out.println("GCM message : "+message);
						logger.info("GCM message : "+message);
						 boolean pushReturn =  SendMessage.ANDROID_sendMessage(pushListTokens, message);
						 logger.info("pushReturn : "+pushReturn);*/
		        	 PushDataModel pd=new PushDataModel();
 	     			pd.setId(7);
 	     	     
 	     	      boolean pushReturn = CommonTasks.sendPushNotification("Logout User", "Logging Out the User", pushListTokens , logger, gson, pd);
 	     	    logger.info("pushReturn : "+pushReturn);       
		        	 if(pushReturn == true)
					        {
					        	 result = gson.toJson(new GeneralResponse(
											Correct.status, 
											Correct.CORRECT_REQUEST_COMPLETED.message, 
											null));
									
					        }
					        else{
					        	result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.GCM_FAILURE,null));
					        }
					}else{
			        	result = gson.toJson(new GeneralResponse(Constants.FALSE,"No push tokens found to send push notification",null));
			        }
		        
		         }
				}else{
					result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Users Found ",null));
				}
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE,null));
			}
		}catch(Exception e1){
			e1.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			logger.info("EXCEPTION_IN_SERVER : "+e1);
		
		}finally{
			if(mt!=null){mt.close();}
		}
		
	//System.out.println("ForceLogout RESULT "+result);
	logger.info("ForceLogout RESULT : "+result);
	logger.info("*******END********");
	response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
