package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsData;

import com.spoton.pud.data.EwayBillNo;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.Mailer;
import com.spoton.pud.data.PickupUpdateStatusModal;
import com.spoton.pud.data.PickupUpdatinResponse;
import com.spoton.pud.data.PickupUpdationData;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceVolumeData;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesImagesData;
import com.spoton.pud.jpa.AutoPickupUnloading;
import com.spoton.pud.jpa.AutoPickupUnloadingCon;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.MailedPickupCon;
import com.spoton.pud.jpa.PickupUpdationStatus;
import com.spoton.pud.jpa.Piece;
import com.spoton.pud.jpa.PieceEntry;
import com.spoton.pud.jpa.PieceVolume;
import com.spoton.pud.jpa.PiecesImage;
import com.spoton.pud.jpa.UpdatedEwayBillNumber;

/**
 * Servlet implementation class SendFailedPickupsToERP
 */
@WebServlet("/sendFailedPickupsToERP")
public class SendFailedPickupsToERP extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger pickupUpdationLogger = Logger.getLogger("SendFailedPickupsToERP");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendFailedPickupsToERP() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		pickupUpdationLogger.info("**************START*******************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("pickupUpdationUrl");
		ManageTransaction mt=new ManageTransaction();;
		StringBuilder stringBuilder = null;
		ConDetailsData conData=null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		PickupUpdationData pickupUpdationData=new PickupUpdationData();
		Map<Integer,String> conMap=new HashMap<Integer, String>();int flag=0;
		try{
			
			//To get Piece Entry data
				
			//String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id,pe.id from piece_entry pe,con_details c where pe.con_id=c.id and c.erp_updated=0 and c.pickup_type=0"; //removing joins
			String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id,pe.id from piece_entry pe where pe.erp_updated=0 and pe.pickup_type=0";
			Query pieceEntryQuery=mt.createNativeQuery(pieceEntry);
			List<Object[]> pieceEntryList=pieceEntryQuery.getResultList();
			PieceEntryData pieceEntryData=null;
			List<PieceEntryData> pieceEntryDataList=null;
			Map<Integer,List<PieceEntryData>> pieceEntryMap=new HashMap<Integer, List<PieceEntryData>>();
			for(Object[] o:pieceEntryList){
				pieceEntryData=new PieceEntryData();
				pieceEntryData.setPCR_Key(Integer.toString((int) o[0]));
				pieceEntryData.setFromPieceNo((String) o[1]);
				pieceEntryData.setToPieceNo((String) o[2]);
				pieceEntryData.setTotalPieces((String) o[3]);
				pieceEntryData.setId((int) o[5]);
				if(pieceEntryMap.containsKey((int) o[4])){
					pieceEntryDataList=pieceEntryMap.get((int) o[4]);
					pieceEntryDataList.add(pieceEntryData);
				}else{
					pieceEntryDataList=new ArrayList<PieceEntryData>();
					pieceEntryDataList.add(pieceEntryData);
					pieceEntryMap.put((int) o[4], pieceEntryDataList);
				}
				
				
			}
			//System.out.println("pieceEntryMap val"+gson.toJson(pieceEntryMap.values()));
			
			//To get Pieces data
			//String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id,pe.id from pieces pe,con_details c where pe.con_id=c.id and c.erp_updated=0 and c.pickup_type=0";	//removing joins
			String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id,pe.id from pieces pe where pe.erp_updated=0 and pe.pickup_type=0";
			Query piecesQuery=mt.createNativeQuery(pieces);
			List<Object[]> piecesList=piecesQuery.getResultList();
			PiecesData piecesData=null;
			List<PiecesData> piecesDataList=null;
			Map<Integer,List<PiecesData>> piecesDataMap=new HashMap<Integer, List<PiecesData>>();
			for(Object[] o:piecesList){
				piecesData=new PiecesData();
				piecesData.setPCR_Key(Integer.toString((int) o[0]));
				piecesData.setLastPieceNumber((String) o[1]);
				piecesData.setTotalUsed((String) o[2]);
				piecesData.setId((int) o[4]);
				if(piecesDataMap.containsKey((int) o[3])){
					piecesDataList=piecesDataMap.get((int) o[3]);
					piecesDataList.add(piecesData);
					
				}else{
					piecesDataList=new ArrayList<PiecesData>();
					piecesDataList.add(piecesData);
					piecesDataMap.put((int) o[3], piecesDataList);
					
				}
				
			}
			//System.out.println("piecesDataMap val"+gson.toJson(piecesDataMap.values()));
			//To get Piece Volume data
			//String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id,actual_weight_per_piece,content_type_id,pe.id from piece_volume pe,con_details c where pe.con_id=c.id and c.erp_updated=0 and c.pickup_type=0";	//removing joins
			String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id,pe.actual_weight_per_piece,pe.content_type_id,pe.id from piece_volume pe where  pe.erp_updated=0 and pe.pickup_type=0";
			Query pieceVolumeQuery=mt.createNativeQuery(pieceVolume);
			List<Object[]> pieceVolumeList=pieceVolumeQuery.getResultList();
			PieceVolumeData pieceVolumeData=null;
			List<PieceVolumeData> pieceVolumeDataList=null;
			Map<Integer,List<PieceVolumeData>> pieceVolumeDataMap=new HashMap<Integer, List<PieceVolumeData>>();
			for(Object[] o:pieceVolumeList){
				pieceVolumeData=new PieceVolumeData();
				pieceVolumeData.setNoofPieces((int) o[0]);
				pieceVolumeData.setTotalVolWeight((double) o[1]);
				pieceVolumeData.setVolbreadth((double) o[2]);
				pieceVolumeData.setVolHeight((double) o[3]);
				pieceVolumeData.setVolLength((double) o[4]);
				pieceVolumeData.setACTWTPERPKG((double) o[6]);
				pieceVolumeData.setContentTypeId(o[7]!=null?String.valueOf((int) o[7]):"");
				pieceVolumeData.setId((int) o[8]);
				if(pieceVolumeDataMap.containsKey((int) o[5])){
					pieceVolumeDataList=pieceVolumeDataMap.get((int) o[5]);
					pieceVolumeDataList.add(pieceVolumeData);
				}else{
					pieceVolumeDataList=new ArrayList<PieceVolumeData>();
				    pieceVolumeDataList.add(pieceVolumeData);
					pieceVolumeDataMap.put((int) o[5], pieceVolumeDataList);
				}
				
			}
			
			//To get Piece Images data
			//String pieceImage="select pe.pieces_images_url,pe.con_id,pe.id from pieces_images pe,con_details c where pe.con_id=c.id and c.erp_updated=0 and c.pickup_type=0"; //removing joins
			String pieceImage="select pe.pieces_images_url,pe.con_id,pe.id from pieces_images pe where  pe.erp_updated=0 and pe.pickup_type=0";
			Query pieceImageQuery=mt.createNativeQuery(pieceImage);
			List<Object[]> pieceImageList=pieceImageQuery.getResultList();
			PiecesImagesData pieceImageData=null;
			List<PiecesImagesData> pieceImagesDataList=null;
			Map<Integer,List<PiecesImagesData>> pieceImagesDataMap=new HashMap<Integer, List<PiecesImagesData>>();
			for(Object[] o:pieceImageList){
				pieceImageData=new PiecesImagesData();
				pieceImageData.setPiecesImagesURL((String) o[0]);
				pieceImageData.setId((int) o[2]);
				
				if(pieceImagesDataMap.containsKey((int) o[1])){
					pieceImagesDataList=pieceImagesDataMap.get((int) o[1]);
					pieceImagesDataList.add(pieceImageData);
				}else{
					pieceImagesDataList=new ArrayList<PiecesImagesData>();
					pieceImagesDataList.add(pieceImageData);
					pieceImagesDataMap.put((int) o[1], pieceImagesDataList);
				}
				
			}
			
			//To get eway bill numbers data
			 /*String addedEwayBillNumbers="select pe.eway_bill_number,pe.con_id,pe.invoice_number,pe.invoice_amount,pe.is_exempted,pe.id from updated_eway_bill_numbers pe,con_details c "
						+ "where pe.con_id=c.id and c.erp_updated=0 and c.pickup_type=0";*/ //removing joins
			 String addedEwayBillNumbers="select pe.eway_bill_number,pe.con_id,pe.invoice_number,pe.invoice_amount,pe.is_exempted,pe.id from updated_eway_bill_numbers pe "
						+ "where  pe.erp_updated=0 and pe.pickup_type=0";
				Query addedEwayBillNumbersQuery=mt.createNativeQuery(addedEwayBillNumbers);
				List<Object[]> ewayNumbersList=addedEwayBillNumbersQuery.getResultList();
				List<EwayBillNo> ewaysList=null;
				Map<Integer,List<EwayBillNo>> ewayNumbersDataMap=new HashMap<Integer, List<EwayBillNo>>();
				for(Object[] o:ewayNumbersList){
					EwayBillNo eway=new EwayBillNo();
					eway.setEwayBillNum((String)o[0]);
					eway.setInvoiceAmount(o[3]!=null?(double) o[3]:0);
					eway.setInvoiceNo(o[2]!=null?(String)o[2]:"");
					eway.setIsExempted(o[4]!=null?(String)o[4]:"");
					eway.setId((int) o[5]);
					if(ewayNumbersDataMap.containsKey((int) o[1])){
						ewaysList=ewayNumbersDataMap.get((int) o[1]);
						ewaysList.add(eway);
					}else{
						ewaysList=new ArrayList<EwayBillNo>();
						ewaysList.add(eway);
						ewayNumbersDataMap.put((int) o[1], ewaysList);
					}
				}
				Map<String,Integer> unloadingConMap=new HashMap<String,Integer>();
			try {
				Calendar yesterday=Calendar.getInstance();
				yesterday.add(Calendar.DATE, -1);
				yesterday.set(Calendar.HOUR_OF_DAY, 00);
				yesterday.set(Calendar.MINUTE, 00);
				yesterday.set(Calendar.SECOND, 00);
				yesterday.set(Calendar.MILLISECOND, 0);
			
			Calendar currentDateEvng=Calendar.getInstance();
			currentDateEvng.setTime(new Date());
			currentDateEvng.set(Calendar.HOUR_OF_DAY, 23);
			currentDateEvng.set(Calendar.MINUTE, 59);
			currentDateEvng.set(Calendar.SECOND, 59);
			currentDateEvng.set(Calendar.MILLISECOND, 0);
				
				/*String autoUnloadingQuery="SELECT c.con_number,c.id FROM pud.auto_pickup_unloading a ,pud.auto_pickup_unloading_cons c "
						+ "where a.transaction_id=c.auto_pickup_unloading_id and a.transaction_result is null "
						+ "and c.is_con_booked=0 and c.created_timestamp>?1 and c.created_timestamp<?2";*/ //removing joins
			String autoUnloadingQuery="SELECT c.con_number,c.id FROM pud.auto_pickup_unloading_cons c "
					+ "where c.transaction_result is null "
					+ "and c.is_con_booked=0 and c.created_timestamp>?1 and c.created_timestamp<?2";
				Query auq=mt.createNativeQuery(autoUnloadingQuery).setParameter(1, yesterday.getTime()).setParameter(2,currentDateEvng.getTime());
			List<Object[]> unloadingConList=auq.getResultList();
			
			if(unloadingConList!=null&&unloadingConList.size()>0) {
				for(Object[] o:unloadingConList) {
					unloadingConMap.put((String) o[0], (int) o[1]);
				}
			}
			}catch(Exception e) {
				e.printStackTrace();
				pickupUpdationLogger.error("Execption in auto unloading ",e);
			}	
			String conQuery="Select c.con_entry_id,c.con_number,c.ref_number,c.pickup_date,c.product,c.origin_pincode,c.destination_pincode,"
					+ "c.actual_weight,c.apply_dc,c.apply_nf_form,c.apply_vtv,c.consignment_type,c.crm_schedule_id,c.customer_code,"
					+ "c.declared_value,c.gate_pass_time,c.image1_url,c.image2_url,c.lat_value,"
					+ "c.long_value,c.no_of_package,c.order_no,c.package_type,c.pan_no,c.payment_basis,c.receiver_name,c.receiver_phone_no,"
					+ "c.risk_type,c.shipment_image_url,c.special_instruction,c.tin_no,c.total_vol_weight,c.user_id,c.user_name,c.vol_type,c.vtc_amount, "
					+ "c.id,c.vehicle_number,c.receiver_gstin,c.receiver_email,c.sender_email,c.is_pre_printed,c.receiver_address,c.receiver_city "
					+ "from con_details c where c.erp_updated=0 and c.pickup_type=0";
			
			Query query1=mt.createNativeQuery(conQuery);
			List<Object[]> consList=query1.getResultList();
			if(consList!=null&&consList.size()>0){
				List<ConDetailsData> conDetailslList=new ArrayList<ConDetailsData>();
				pickupUpdationLogger.info("consList size "+consList.size());
				
				int count=0;int size=consList.size();
				  for(Object[] o:consList){
					  if(piecesDataMap.get((int) o[36])!=null&&pieceEntryMap.get((int) o[36])!=null){
					  conData=new ConDetailsData();
					  conData.setConEntryID((int) o[36]);
					  conData.setConNumber( o[1]!=null?(String) o[1]:"");
					  conData.setRefNumber(o[2]!=null?(String) o[2]:"");
					  if(o[3]!=null)
		    		    conData.setPickupdate(format.format((Date) o[3]));
						conData.setProduct(o[4]!=null?(String) o[4]:"");
						conData.setOriginPINCode(o[5]!=null?(String) o[5]:"");
						conData.setDestinationPINCode(o[6]!=null?(String) o[6]:"");
					    conData.setActualWeight(o[7]!=null?(String) o[7]:"");
	    				conData.setApplyDC(o[8]!=null?(String) o[8]:"");
	    				conData.setApplyNFForm((String) o[9]);
	    				conData.setApplyVTV((String) o[10]);
	    			    conData.setConsignmentType((String) o[11]);
	    				conData.setCRMScheduleID((String) o[12]);
	    				conData.setCustomerCode((String) o[13]);
	    				conData.setDeclaredValue((String) o[14]);
	    				conData.setGatePassTime((String) o[15]);
	    				conData.setImage1URL((String) o[16]);
	    				conData.setImage2URL((String) o[17]);
	    				conData.setLatvalue((String) o[18]);
	    				conData.setLongvalue((String) o[19]);
	    				conData.setNoofPackage((String) o[20]);
	    				conData.setOrderNo((String) o[21]);
	    			    conData.setPackageType((String) o[22]);
	    				conData.setPANNo((String) o[23]);
	    				conData.setPaymentbasis(o[24]!=null?(String) o[24]:"2");
	    				conData.setReceiverName((String) o[25]);
	    				conData.setReceiverPhoneNo((String) o[26]);
	    				conData.setRiskType((String) o[27]);
	    				conData.setShipmentImageURL((String) o[28]);
	    				conData.setSpecialInstruction((String) o[29]);
	    				conData.setTINNo((String) o[30]);
	    				conData.setTotalVolWeight((String) o[31]);
	    				conData.setUserID((String) o[32]);
	    				conData.setUserName((String) o[33]);
	    				conData.setVolType((String) o[34]);
	    				conData.setVTCAmount((String) o[35]);
	    				conData.setVehicleNo((String) o[37]);
	    				conData.setEwayBillNo("");
	    				conData.setReceiverGSTIN(o[38]!=null?(String) o[38]:"");
	    				conData.setReceiverEmail(o[39]!=null?(String) o[39]:"");
	    				conData.setSenderEmail(o[40]!=null?(String) o[40]:"");
	    				conData.setIsPreprinted(o[41] != null ? (String) o[41] : "");
	    				conData.setReceiverAddress(o[42] != null ? (String) o[42] : "");
	    				conData.setReceiverCity(o[43] != null ? (String) o[43] : "");
	    				if(pieceEntryMap.get(conData.getConEntryID())!=null)
	    				conData.setPieceEntry(pieceEntryMap.get(conData.getConEntryID()));
	    				else{conData.setPieceEntry(new ArrayList<PieceEntryData>());}
	    				if(piecesDataMap.get(conData.getConEntryID())!=null)
	    					conData.setPieces(piecesDataMap.get(conData.getConEntryID()));
	    				else{conData.setPieces(new ArrayList<PiecesData>());}
	    				if(pieceVolumeDataMap.get(conData.getConEntryID())!=null)
	    					conData.setPieceVolume(pieceVolumeDataMap.get(conData.getConEntryID()));
	    				else{conData.setPieceVolume(new ArrayList<PieceVolumeData>());}
	    				if(pieceImagesDataMap.get(conData.getConEntryID())!=null)
	    					conData.setPiecesImages(pieceImagesDataMap.get(conData.getConEntryID()));
	    				else{conData.setPiecesImages(new ArrayList<PiecesImagesData>());}
	    				
	    				if(ewayNumbersDataMap.get(conData.getConEntryID())!=null)
	    					conData.setEwayBillNoList(ewayNumbersDataMap.get(conData.getConEntryID()));
	    				else{conData.setEwayBillNoList(new ArrayList<EwayBillNo>());}
	    				
	    				conDetailslList.add(conData);
	    				conMap.put(conData.getConEntryID(), conData.getConNumber());
	    				count++;
						
	    				}
					  size--;
				  
				  if(count==10||size==0){
					  try{
						  if(conDetailslList!=null&&conDetailslList.size()>0){
					pickupUpdationData.setConDetails(conDetailslList);
					//System.out.println("DataSent to spoton "+gson.toJson(pickupUpdationData));
					pickupUpdationLogger.info("Spoton URL Called "+urlString);
					pickupUpdationLogger.info("DataSent to spoton "+gson.toJson(pickupUpdationData));
    				obj = new URL(urlString);
    				con = (HttpURLConnection) obj.openConnection();
    				//System.out.println("current time"+System.currentTimeMillis());
    				con.setConnectTimeout(15000);
    				con.setReadTimeout(60000);
    				//System.out.println("current time 2"+System.currentTimeMillis());
    				//System.out.println("timeout "+con.getConnectTimeout());
    				con.setRequestMethod("POST");
    				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
    				con.setRequestProperty("Accept", "application/json");
    				con.setDoOutput(true);
    				
    				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
    				  streamWriter.write(gson.toJson(pickupUpdationData));
    				  streamWriter.flush();
    				  streamWriter.close();
    				  pickupUpdationLogger.info("Response Code : " +con.getResponseCode());
    		            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
    		            	stringBuilder=new StringBuilder();
    		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
    		                BufferedReader bufferedReader = new BufferedReader(streamReader);
    		                String response1 = null;
    		                while ((response1 = bufferedReader.readLine()) != null) {
    		                    stringBuilder.append(response1 + "\n");
    		                }
    		                bufferedReader.close();
    		                streamReader.close();
    		                //System.out.println("stringBuilder.toString()"+stringBuilder.toString());
    		                pickupUpdationLogger.info("Response From Spoton : " +stringBuilder.toString());
    		               
    		    			//System.out.println("Response Code : " +responseCode);
    		    			
    		    		}else{
    		            	
    		            //System.out.println("Response failed");
    		           pickupUpdationLogger.info("Response failed from spoton");
    		           CommonTasks.sendEmailForFailedPickups(pickupUpdationData,mt,result,gson,pickupUpdationLogger);
    		            //result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
    		            
    		            }
    		            if(result.length()==0){
    	    				PickupUpdatinResponse responseList=gson.fromJson(stringBuilder.toString(),PickupUpdatinResponse.class);
    	    				List<String> allConList=new ArrayList<String>();
    	    				//System.out.println("responseList "+gson.toJson(responseList));
    	    				//pickupUpdationLogger.info("responseList "+gson.toJson(responseList));
    	    				if(responseList!=null&&responseList.getPickUpUpdateStatus()!=null&&responseList.getPickUpUpdateStatus().size()>0){
    	    					PickupUpdationStatus ps=null;List<String> conNumbers=new ArrayList<String>();
    	    				for(PickupUpdateStatusModal pm:responseList.getPickUpUpdateStatus()){
    	    						ps=new PickupUpdationStatus();
    	    						ps.setConEntryId(pm.getConEntryId());
    	    						ConDetail c=mt.find(ConDetail.class, pm.getConEntryId());
    	    						if(c!=null) {
    	  						    c.setErpUpdated(1);
    	  						    c.setTransactionResult(pm.getTransactionResult());
    	  						    c.setStatusFlag(pm.getFlag());
    	  						  if(unloadingConMap.containsKey(c.getConNumber())) {
    	  							  AutoPickupUnloadingCon uc=mt.find(AutoPickupUnloadingCon.class,unloadingConMap.get(c.getConNumber()));
    	  							  uc.setIsConBooked(1);
    	  							  uc.setBookedConStatus(pm.getFlag());
    	  							 uc.setConWeight(c.getActualWeight()!=null?Double.parseDouble(c.getActualWeight()):0);
   								     uc.setPiecesCount(c.getNoOfPackage()!=null?Integer.parseInt(c.getNoOfPackage()):0);
   								     uc.setConDetailsId(c.getConEntryId());
   								     c.setIsUnloadingSheetGenerated(2);//2 is set to avoid duplicate cons for auto unloading sheet generation
   								     AutoPickupUnloading au=mt.find(AutoPickupUnloading.class, uc.getAutoPickupUnloading().getTransactionId());
   									if(au!=null) {
   										au.setTotalConWeight(au.getTotalConWeight()+uc.getConWeight());
   										au.setTotalPiecesCount(au.getTotalPiecesCount()+uc.getPiecesCount());
   										mt.persist(au);
   									}
    	  							  mt.persist(uc);
    	  						  }
    	    						}
    	    						ps.setTransactionResult(pm.getTransactionResult());
    	    						ps.setCreatedTimestamp(new Date());
    	    						ps.setStatusFlag(pm.getFlag());
    	    						if(pm.getTransactionResult().equalsIgnoreCase("Con Already Exists")){
    	    							conNumbers.add(conMap.get(pm.getConEntryId()));
    	    						}
    	    						if(((pm.getTransactionResult().equalsIgnoreCase("Success")))){
    	    							flag=1;
    	    							result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",responseList));
    	    							
    	    						}
    	    						mt.persist(ps);
    	    						
    	    						
    	    						
    	    							if(pieceEntryMap.get(pm.getConEntryId())!=null) {
	    							List<PieceEntryData> pieceEntryDatas=pieceEntryMap.get(pm.getConEntryId());
	    							for(PieceEntryData pd:pieceEntryDatas) {
	    								PieceEntry pe=mt.find(PieceEntry.class, pd.getId());
	    								if(pe!=null) {
	    									pe.setErpUpdated(1);
	    									mt.persist(pe);
	    								}
	    							}
	    						}
	    						
	    						if(pieceVolumeDataMap.get(pm.getConEntryId())!=null) {
	    							List<PieceVolumeData> pieceVolumeDatas=pieceVolumeDataMap.get(pm.getConEntryId());
	    							for(PieceVolumeData pd:pieceVolumeDatas) {
	    								PieceVolume pe=mt.find(PieceVolume.class, pd.getId());
	    								if(pe!=null) {
	    									pe.setErpUpdated(1);
	    									mt.persist(pe);
	    								}
	    							}
	    						}
	    						if(piecesDataMap.get(pm.getConEntryId())!=null) {
	    							List<PiecesData> pieceDatas=piecesDataMap.get(pm.getConEntryId());
	    							for(PiecesData pd:pieceDatas) {
	    								Piece pe=mt.find(Piece.class, pd.getId());
	    								if(pe!=null) {
	    									pe.setErpUpdated(1);
	    									mt.persist(pe);
	    								}
	    							}
	    						}
    	    					
	    						if(pieceImagesDataMap.get(pm.getConEntryId())!=null) {
	    							List<PiecesImagesData> pieceImageDatas=pieceImagesDataMap.get(pm.getConEntryId());
	    							for(PiecesImagesData pd:pieceImageDatas) {
	    								PiecesImage pe=mt.find(PiecesImage.class, pd.getId());
	    								if(pe!=null) {
	    									pe.setErpUpdated(1);
	    									mt.persist(pe);
	    								}
	    							}
	    						}
	    						
	    						if(ewayNumbersDataMap.get(pm.getConEntryId())!=null) {
	    							List<EwayBillNo> ewayDatas =ewayNumbersDataMap.get(pm.getConEntryId());
	    							for(EwayBillNo pd:ewayDatas) {
	    								UpdatedEwayBillNumber pe=mt.find(UpdatedEwayBillNumber.class, pd.getId());
	    								if(pe!=null) {
	    									pe.setErpUpdated(1);
	    									mt.persist(pe);
	    								}
	    							}
	    						}
    	    						
    	    					}//end of for 
    	    				mt.commit();
    	    				
    	    				
    	    				
    	    				
    	    				if(result.length()==0&&conNumbers!=null&&conNumbers.size()>0){
    							String message="";
    							for(String c:conNumbers){
    								message=message+" "+c;
    								//System.out.println("message "+message);
    							}
    							message="Con Already Exists for Con Numbers "+message;
    							result = gson.toJson(new GeneralResponse(Constants.FALSE,message,null));
    							
    						}else if(flag==0){
    							result = gson.toJson(new GeneralResponse(Constants.FALSE,"Pickup Updation Failed", null));
    							
    						}
    						else{
    							result = gson.toJson(new GeneralResponse(Constants.TRUE,"Success",responseList));
    							
    						}
    							
    	    					
    	    				}else{
    	    					 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Null Response From ERP", null));
    	    				}
    	    				
    	    				}
					  }
				  }catch (SocketTimeoutException e){		
						e.printStackTrace();
						
						pickupUpdationLogger.info("SocketTimeoutException "+e);
						CommonTasks.sendEmailForFailedPickups(pickupUpdationData,mt,result,gson,pickupUpdationLogger);
						
						/*PickupUpdationData newPickupUpdationData=null;
						try{
					String q="select con_entry_id FROM pud.mailed_pickup_cons";
						Query qu=mt.createNativeQuery(q);
						@SuppressWarnings("unchecked")
						List<Object> mailedConsList=qu.getResultList();
						if(mailedConsList!=null&&mailedConsList.size()>0){
						List<ConDetailsData> newConData=new ArrayList<ConDetailsData>();
						for(int i=0;i<pickupUpdationData.getConDetails().size();i++){
							if(!mailedConsList.contains((pickupUpdationData.getConDetails().get(i).getConEntryID()))){
								newConData.add(pickupUpdationData.getConDetails().get(i));
							}
							
						}
						if(newConData!=null&&newConData.size()>0){
							newPickupUpdationData=new PickupUpdationData();
							newPickupUpdationData.setConDetails(newConData);
						}
						}else{
							newPickupUpdationData=pickupUpdationData;
						}
						
						if(newPickupUpdationData!=null){
							String mailMessage="Hi Everyone,\n\n ERP failed to sent response to the following Pickup Updation data.Please check and recycle the ERP server if needed to receive this data again.\n\n "+gson.toJson(newPickupUpdationData)+"\n\n Thanks&Regards,\n Apptmyz.";
							boolean mailResult= Mailer.send(FilesUtil.getProperty("mailusername"),FilesUtil.getProperty("mailpassword"),"suman.kumar@spoton.co.in","ERP Response Failed For Pickup Updation",mailMessage,pickupUpdationLogger);
						if(mailResult){
					
							for(int i=0;i<newPickupUpdationData.getConDetails().size();i++){
								MailedPickupCon mp=new MailedPickupCon();
								mp.setConEntryId(newPickupUpdationData.getConDetails().get(i).getConEntryID());
								mt.persist(mp);
								
							}
							mt.commit();
							result = gson.toJson(new GeneralResponse(Constants.TRUE,"ERP timed out..Mail Sent Successfully",null));
						}else{
							result = gson.toJson(new GeneralResponse(Constants.FALSE,
									"ERP timed out..Failed To send Mail",null));
						}
						}else{
							result = gson.toJson(new GeneralResponse(Constants.FALSE,
									"ERP timed out..No New Data found To send Mail",null));
						}
						
						}catch(Exception e1){
							e1.printStackTrace();
							result = gson.toJson(new GeneralResponse(Constants.FALSE,
									"ERP timed out..Exception in sending mail",null));
							pickupUpdationLogger.info("ERP timed out..Exception in sending mail "+e1);
							}*/
					}catch(Exception e1){
						e1.printStackTrace();
						result = gson.toJson(new GeneralResponse(Constants.FALSE,
								Constants.ERRORS_EXCEPTION_IN_SERVER,null));
						 pickupUpdationLogger.error(" Exception while sending  records to erp",e1);
					
					}
    		            
    		            
    		            count=0;
    		            conDetailslList=new ArrayList<ConDetailsData>();
    	  			    // pickupUpdationLogger.info("Result of  records sent to erp "+result);
    	  			   result = "";
    	  			   
				  }//end of count=10
			}//end of for
				  result = gson.toJson(new GeneralResponse(Constants.TRUE,"Fetched All Failed Pickups and sent to erp", null));
    			  }else{
    				  result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Cons Found", null));
    			  }
    				
    				
    	
			
			
		
		}catch(Exception e1){
			e1.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
			pickupUpdationLogger.error("EXCEPTION_IN_SERVER ",e1);
		
		}finally{
			if(mt!=null){mt.close();}
			
			if(con!=null){con.disconnect();}
		}
		//System.out.println("sendFailedPickupsToERP result"+result);
		pickupUpdationLogger.info("sendFailedPickupsToERP result"+result);
		pickupUpdationLogger.info("**************END*******************");
		response.getWriter().write(result);
	}
	
	
	
	/*private void sendEmail(PickupUpdationData pickupUpdationData,ManageTransaction mt,String result,Gson gson) {
		PickupUpdationData newPickupUpdationData=null;
		try{
	String q="select con_entry_id FROM pud.mailed_pickup_cons";
		Query qu=mt.createNativeQuery(q);
		@SuppressWarnings("unchecked")
		List<Object> mailedConsList=qu.getResultList();
		if(mailedConsList!=null&&mailedConsList.size()>0){
		List<ConDetailsData> newConData=new ArrayList<ConDetailsData>();
		for(int i=0;i<pickupUpdationData.getConDetails().size();i++){
			if(!mailedConsList.contains((pickupUpdationData.getConDetails().get(i).getConEntryID()))){
				newConData.add(pickupUpdationData.getConDetails().get(i));
			}
			
		}
		if(newConData!=null&&newConData.size()>0){
			newPickupUpdationData=new PickupUpdationData();
			newPickupUpdationData.setConDetails(newConData);
		}
		}else{
			newPickupUpdationData=pickupUpdationData;
		}
		
		if(newPickupUpdationData!=null){
			String mailMessage="Hi Everyone,\n\n ERP failed to sent response to the following Pickup Updation data.Please check and recycle the ERP server if needed to receive this data again.\n\n "+gson.toJson(newPickupUpdationData)+"\n\n Thanks&Regards,\n Apptmyz.";
			boolean mailResult= Mailer.send(FilesUtil.getProperty("mailusername"),FilesUtil.getProperty("mailpassword"),"suman.kumar@spoton.co.in","ERP Response Failed For Pickup Updation",mailMessage,pickupUpdationLogger);
		if(mailResult){
	
			for(int i=0;i<newPickupUpdationData.getConDetails().size();i++){
				MailedPickupCon mp=new MailedPickupCon();
				mp.setConEntryId(newPickupUpdationData.getConDetails().get(i).getConEntryID());
				mt.persist(mp);
				
			}
			mt.commit();
			result = gson.toJson(new GeneralResponse(Constants.TRUE,"ERP timed out..Mail Sent Successfully",null));
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..Failed To send Mail",null));
		}
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..No New Data found To send Mail",null));
		}
		
		}catch(Exception e1){
			e1.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..Exception in sending mail",null));
			pickupUpdationLogger.info("ERP timed out..Exception in sending mail "+e1);
			}
	
		
	}*/
}

