package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.CustomerServiceMasterErpResponseModel;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PinCodeSpotonResponse;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.jpa.CustomerServiceMaster;
import com.spoton.pud.jpa.CustomerServiceMasterPK;
import com.spoton.pud.jpa.PinCodeMaster;
import com.spoton.pud.jpa.PinCodeMasterPK;

/**
 * Servlet implementation class SaveCustomerServiceMaster
 */
@WebServlet("/saveCustomerServiceMaster")
public class SaveCustomerServiceMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SaveCustomerServiceMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveCustomerServiceMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START**************");
		String urlString = FilesUtil.getProperty("customerServiceMaster");
		logger.info("Erp Url "+urlString);
		URL obj = new URL(urlString);

		Gson gson = new GsonBuilder().serializeNulls().create();
        String result="";
		BufferedReader in = null;
		StringBuffer response1 = new StringBuffer();
		ManageTransaction mt = new ManageTransaction();
		int inactive = 0;
		int changedpins = 0;
		int newpins = 0;
		
		try{
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			con.setConnectTimeout(5000);
			con.setReadTimeout(60000);
			int responseCode = con.getResponseCode();
			logger.info("Response Code : " + responseCode);
	
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	      //System.out.println("response1 "+response1);
			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			in.close();
			Type type = new TypeToken<ArrayList<CustomerServiceMasterErpResponseModel>>(){}.getType();
			List<CustomerServiceMasterErpResponseModel> erpResponse =  gson.fromJson(response1.toString(),	type);
			logger.info("response size :: "+erpResponse.size());
			Map<String, CustomerServiceMasterErpResponseModel> newDataMap = new HashMap<String, CustomerServiceMasterErpResponseModel>();
			for (CustomerServiceMasterErpResponseModel c:erpResponse){
				newDataMap.put(c.getAccountCode().trim()+"-"+c.getOrigin().trim()+"-"+c.getDestination().trim(),c);
			}
			logger.info("Size of data after filtering duplicates " + newDataMap.size());
			int version = getCurrentVersion(mt) + 1;
			logger.info("New Version " + version);
		
			Map<String, CustomerServiceMaster> oldDataMap = getOldData(mt);
			logger.info("old data size: " + oldDataMap.size());
			try{
               String okey = "";
				for(CustomerServiceMaster cs: oldDataMap.values()){
					okey =cs.getId().getAccountCode()+"-"+cs.getId().getOrigin()+"-"+cs.getId().getDestination().trim();
					if(!newDataMap.containsKey(okey)) {
						inactive += 1;
						cs.setActiveFlag(0);
						cs.setVersion(version);
						cs.setUpdatedTime(new Date());
						mt.persist(cs);
					}
				}
//				mt.commit();
				logger.info("Inactive ::"+inactive);
			for(CustomerServiceMasterErpResponseModel cs: newDataMap.values()){
				String key = cs.getAccountCode().trim()+"-"+cs.getOrigin().trim()+"-"+cs.getDestination().trim();
				CustomerServiceMasterPK primaryKey=new CustomerServiceMasterPK();
				primaryKey.setAccountCode(cs.getAccountCode().trim());
				primaryKey.setOrigin(cs.getOrigin().trim());
				primaryKey.setDestination(cs.getDestination().trim());
				primaryKey.setAccountCode(cs.getAccountCode());
					if (!oldDataMap.containsKey(key)){
						newpins += 1;
						CustomerServiceMaster csm = new CustomerServiceMaster();
						csm.setActiveFlag(1);
						csm.setCreatedTime(new Date());
						csm.setId(primaryKey);
						csm.setVersion(version);
						csm.setCreatedTime(new Date());
						mt.persist(csm);
					}
					/*else{
					
						CustomerServiceMaster oldPinCode = oldpinMap.get(key);
						if (pin.changed(oldPinCode)){
							changedpins += 1;
							//oldPinCode.setId(primaryKey);
							oldPinCode.setActive(pin.isActive()?1:0);
							oldPinCode.setActiveStatus(pin.getActiveStatus());
							oldPinCode.setLocationName(pin.getLocationName());
							oldPinCode.setServicable(pin.getServicable());
							oldPinCode.setServiceType(pin.getServiceType());
							oldPinCode.setIsAirServicable(pin.getIsAirServiceable());
							oldPinCode.setIsAirServicableDel(pin.getIsAirServiceableDel());
							oldPinCode.setUpdatedTime(new Date());
							oldPinCode.setVersion(version);
							oldPinCode.setAreaId(pin.getAreaId());
							oldPinCode.setFlag(pin.getFlag());
							oldPinCode.setPickupAllowed(pin.getPickupAllowed() ? 1 : 0);
							oldPinCode.setDeliveryAllowed(pin.getDeliveryAllowed() ? 1 : 0);
							oldPinCode.setRegion(pin.getRegion());
							oldPinCode.setDepot(pin.getDepot());
							oldPinCode.setArea(pin.getArea());
							mt.persist(oldPinCode);
						}
					}*/
			}
			logger.info("Commiting");
			mt.commit();
			logger.info("Committed");
			
			logger.info("new :: "+newpins+" changed::"+changedpins);
			List <String> pushListTokens = CommonTasks.getpushTokens(mt);
			 Integer isUpdateMandatory=0;
			if(pushListTokens!=null&&pushListTokens.size()>0) {
				 PushDataModel pd=new PushDataModel();
				 pd.setId(20);
	     	      pd.setMasterType(4);
	     	      String updateQuery="SELECT is_update_mandatory FROM masters_update where master_type_id=4";
	     	     Query q=mt.createNativeQuery(updateQuery);
	     	     try {
	     	      isUpdateMandatory=(Integer) q.getSingleResult();
	     	     }catch(NoResultException nr) {
	     	    	 nr.printStackTrace();
	     	     }
	     	     pd.setUpdateMandatory(isUpdateMandatory==1?true:false);
	     	      boolean pushReturn = CommonTasks.sendPushNotification("CustomerServiceMaster_Update", "CustomerServiceMaster_Update", pushListTokens , logger, gson, pd);
	     	     logger.info("pushReturn : "+pushReturn);
			        if(pushReturn == true)
			        {
			        	logger.info(" pushSent Successfully ");
			        	
			        }
			        else{
			        	logger.info(Constants.GCM_FAILURE);
			        	
			        }
				}
			result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
			
				
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Exception in loop ",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				
			}
		}catch(Exception e){
			logger.error("Exception in main: " , e);
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
		
			mt.close();
		}
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	private Map<String, CustomerServiceMaster> getOldData(ManageTransaction mt) {
		Map<String, CustomerServiceMaster> dataMap = new HashMap<String, CustomerServiceMaster>();
		String qry = "Select pcm from CustomerServiceMaster pcm ";
		TypedQuery<CustomerServiceMaster> pcmQuery = mt.createQuery(CustomerServiceMaster.class, qry);
		try{
			List<CustomerServiceMaster> oldData = pcmQuery.getResultList();
			for (CustomerServiceMaster cs:oldData){
				String key=cs.getId().getAccountCode()+"-"+cs.getId().getOrigin()+"-"+cs.getId().getDestination();
				dataMap.put(key,cs);
			}
		}catch(Exception e){
			logger.error("Exception in getting old data " , e);
		}
//		pudPincodeMaster.info("getting old pins done ");
		return dataMap;
	}

	private int getCurrentVersion(ManageTransaction mt) {
		Integer version = 0;
		String qry = "select max(version) from customer_service_master";
//		pudPincodeMaster.info("Checkig version");
		Query query = mt.createNativeQuery(qry);
		try{
			version = (Integer) query.getSingleResult();
		}catch(Exception e){
			logger.error("Exception in pin version retrieval " , e);
		}
//		pudPincodeMaster.info("Checkig version done: " + version);

		return version == null?0:version;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
