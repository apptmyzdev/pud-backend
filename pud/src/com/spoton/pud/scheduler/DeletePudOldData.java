package com.spoton.pud.scheduler;

import java.io.IOException;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;

/**
 * Servlet implementation class DeletePudOldData
 */
@WebServlet("/deletePudOldData")
public class DeletePudOldData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("DeletePudOldData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeletePudOldData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**************START*******************");
		ManageTransaction mt = new ManageTransaction();
		String result="";
		Gson gson = new GsonBuilder().serializeNulls().create();
		try {
			int cnt = 0;int cumcnt = 0;
			//-----------------------------------audit_log-----------------------------------------------
			try {
			String auditLogQuery="delete FROM pud.audit_log where id!=0 and timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(auditLogQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of audit_log data  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of audit_log data  deleted  :- " + cumcnt);
				logger.error("Exception while deleting audit_log data", e);
				
			}
			
			//-----------------------------------pickup_data_acknowledgement(AssignedPickupsStatusTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String assignedPickupsStatusQuery="delete FROM pud.pickup_data_acknowledgement where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(assignedPickupsStatusQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pickup_data_acknowledgement(AssignedPickupsStatusTable)  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pickup_data_acknowledgement(AssignedPickupsStatusTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pickup_data_acknowledgement(AssignedPickupsStatusTable)", e);
				
			}
			
			//-----------------------------------pud_data(AssignedPickupsTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String assignedPickupsQuery="delete FROM pud.pud_data where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(assignedPickupsQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pud_data(AssignedPickupsTable)  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pud_data(AssignedPickupsTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pud_data(AssignedPickupsTable)", e);
				
			}
			//-----------------------------------pickup_registration(RegisteredPickupsTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String registeredPickupsQuery="delete FROM pud.pickup_registration where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(registeredPickupsQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pickup_registration(RegisteredPickupsTable)  deleted  :- " + cumcnt);
			
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pickup_registration(RegisteredPickupsTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pickup_registration(RegisteredPickupsTable) ", e);
				
			}
			//-----------------------------------pickup_reschedule_update_status(AttemptedPickupsStatusTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String attemptedPickupsStatusQuery="delete FROM pud.pickup_reschedule_update_status where id!=0 and timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(attemptedPickupsStatusQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pickup_reschedule_update_status(AttemptedPickupsStatusTable)  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pickup_reschedule_update_status(AttemptedPickupsStatusTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pickup_reschedule_update_status(AttemptedPickupsStatusTable) ", e);
				
			}
			
			//-----------------------------------pickup_reschedule(AttemptedPickupsTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String attemptedPickupsQuery="delete FROM pud.pickup_reschedule where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(attemptedPickupsQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pickup_reschedule(AttemptedPickupsTable)  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pickup_reschedule(AttemptedPickupsTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pickup_reschedule(AttemptedPickupsTable) ", e);
				
			}
			
			//-----------------------------------pickup_reassigned(ReassignedPickupsTable)-----------------------------------------------
			try {
			cnt = 0;cumcnt = 0;
			String reassignedPickupsQuery="delete FROM pud.pickup_reassigned where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
			Query q = mt.createNativeQuery(reassignedPickupsQuery);
			do {
	            mt.begin();//needs to begin new transaction every time
				cnt = q.executeUpdate();
				cumcnt += cnt;
				mt.commit();
				
				
			} while (cnt == 100000);
			logger.info("Total records of pickup_reassigned(ReassignedPickupsTable)  deleted  :- " + cumcnt);
			}catch(Exception e) {
				e.printStackTrace();
				logger.info("Total records of pickup_reassigned(ReassignedPickupsTable)  deleted  :- " + cumcnt);
				logger.error("Exception while deleting pickup_reassigned(ReassignedPickupsTable) ", e);
				
			}
			
			//-----------------------------------vendor_con_wise_details(PudPartnerTable)-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String vendorConDetailsQuery="delete FROM pud.vendor_con_wise_details where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(vendorConDetailsQuery);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of vendor_con_wise_details(PudPartnerTable)  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of vendor_con_wise_details(PudPartnerTable)  deleted  :- " + cumcnt);
					logger.error("Exception while deleting vendor_con_wise_details(PudPartnerTable) ", e);
					
				}
			
			
			//-----------------------------------vendor_pickup_delivery_details(PudPartnerTable)-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String vendorPickupDetailsQuery="delete FROM pud.vendor_pickup_delivery_details where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(vendorPickupDetailsQuery);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of vendor_pickup_delivery_details(PudPartnerTable)  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of vendor_pickup_delivery_details(PudPartnerTable)  deleted  :- " + cumcnt);
					logger.error("Exception while deleting vendor_pickup_delivery_details(PudPartnerTable) ", e);
					
				}
			
			
			//-----------------------------------pulled_data_acknowledgement(P/D Pulleddata acknowledgement Table)-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.pulled_data_acknowledgement where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of pulled_data_acknowledgement  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of pulled_data_acknowledgement  deleted  :- " + cumcnt);
					logger.error("Exception while deleting pulled_data_acknowledgement ", e);
					
				}
			
			//-----------------------------------agent_pdc(Deliveries agent-pdc map table)-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.agent_pdc where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of agent_pdc  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of agent_pdc  deleted  :- " + cumcnt);
					logger.error("Exception while deleting agent_pdc ", e);
					
				}
			
			//-----------------------------------drs_acknowledged_delivery_cons-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.drs_acknowledged_delivery_cons where transaction_id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of drs_acknowledged_delivery_cons  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of drs_acknowledged_delivery_cons  deleted  :- " + cumcnt);
					logger.error("Exception while deleting drs_acknowledged_delivery_cons ", e);
					
				}
			
			//-----------------------------------delivery_con_details-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.delivery_con_details where id!=0 and created_date< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of delivery_con_details  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of delivery_con_details  deleted  :- " + cumcnt);
					logger.error("Exception while deleting delivery_con_details ", e);
					
				}
			
			//-----------------------------------delivery_condition_images-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.delivery_condition_images where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of delivery_condition_images  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of delivery_condition_images  deleted  :- " + cumcnt);
					logger.error("Exception while deleting delivery_condition_images ", e);
					
				}
			
			
			//-----------------------------------delivery_updation_status-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.delivery_updation_status where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of delivery_updation_status  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of delivery_updation_status  deleted  :- " + cumcnt);
					logger.error("Exception while deleting delivery_updation_status ", e);
					
				}
			
			//-----------------------------------delivery_updation-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.delivery_updation where transaction_id!=0 and created_time< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of delivery_updation  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of delivery_updation  deleted  :- " + cumcnt);
					logger.error("Exception while deleting delivery_updation ", e);
					
				}
			
			//-----------------------------------oda_con_remarks-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.oda_con_remarks where transaction_id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of oda_con_remarks  deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of oda_con_remarks  deleted  :- " + cumcnt);
					logger.error("Exception while deleting oda_con_remarks ", e);
					
				}
			
			
			//-----------------------------------added_pieces-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_pieces where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_pieces deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_pieces deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_pieces ", e);
					
				}
			
			//-----------------------------------added_piece_entry-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_piece_entry where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_piece_entry deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_piece_entry deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_piece_entry ", e);
					
				}
			
			//-----------------------------------added_piece_volume-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_piece_volume where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_piece_volume deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_piece_volume deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_piece_volume ", e);
					
				}
			
			
			//-----------------------------------added_pieces_images-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_pieces_images where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_pieces_images deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_pieces_images deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_pieces_images ", e);
					
				}
			

			//-----------------------------------added_eway_bill_numbers-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_eway_bill_numbers where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_eway_bill_numbers deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_eway_bill_numbers deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_eway_bill_numbers ", e);
					
				}
			
			//-----------------------------------added_cons-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.added_cons where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of added_cons deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of added_cons deleted  :- " + cumcnt);
					logger.error("Exception while deleting added_cons ", e);
					
				}
			
			
			//-----------------------------------updated_eway_bill_numbers-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.updated_eway_bill_numbers where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of updated_eway_bill_numbers deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of updated_eway_bill_numbers deleted  :- " + cumcnt);
					logger.error("Exception while deleting updated_eway_bill_numbers ", e);
					
				}
			
			//-----------------------------------pieces-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.pieces where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of pieces deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of pieces deleted  :- " + cumcnt);
					logger.error("Exception while deleting pieces ", e);
					
				}
			
			//-----------------------------------piece_entry-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.piece_entry where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of piece_entry deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of piece_entry deleted  :- " + cumcnt);
					logger.error("Exception while deleting piece_entry ", e);
					
				}
			
			//-----------------------------------piece_volume-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.piece_volume where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of piece_volume deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of piece_volume deleted  :- " + cumcnt);
					logger.error("Exception while deleting piece_volume ", e);
					
				}
			
			//-----------------------------------pieces_images-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.pieces_images where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of pieces_images deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of pieces_images deleted  :- " + cumcnt);
					logger.error("Exception while deleting pieces_images ", e);
					
				}
			
			//-----------------------------------wst_pieces_ref_list-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.wst_pieces_ref_list where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of wst_pieces_ref_list deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of wst_pieces_ref_list deleted  :- " + cumcnt);
					logger.error("Exception while deleting wst_pieces_ref_list ", e);
					
				}
			
			
			//-----------------------------------wst_pickups_updated_data-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.wst_pickups_updated_data where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of wst_pickups_updated_data deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of wst_pickups_updated_data deleted  :- " + cumcnt);
					logger.error("Exception while deleting wst_pickups_updated_data ", e);
					
				}
			
			//-----------------------------------modified_rate_card_values-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.modified_rate_card_values where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of modified_rate_card_values deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of modified_rate_card_values deleted  :- " + cumcnt);
					logger.error("Exception while deleting modified_rate_card_values ", e);
					
				}
			
			
			//-----------------------------------pickup_updation_status-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.pickup_updation_status where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of pickup_updation_status deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of pickup_updation_status deleted  :- " + cumcnt);
					logger.error("Exception while deleting pickup_updation_status ", e);
					
				}
			
			//-----------------------------------cash_booking_response-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.cash_booking_response where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of cash_booking_response deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of cash_booking_response deleted  :- " + cumcnt);
					logger.error("Exception while deleting cash_booking_response ", e);
					
				}
			
			
			//-----------------------------------box_piece_mapping_details-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.box_piece_mapping_details where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of box_piece_mapping_details deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of box_piece_mapping_details deleted  :- " + cumcnt);
					logger.error("Exception while deleting box_piece_mapping_details ", e);
					
				}
			

			//-----------------------------------pickup_execution_details-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.pickup_execution_details where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of pickup_execution_details deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of pickup_execution_details deleted  :- " + cumcnt);
					logger.error("Exception while deleting pickup_execution_details ", e);
					
				}
			
			
			
			//-----------------------------------auto_pickup_unloading_cons-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.auto_pickup_unloading_cons where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of auto_pickup_unloading_cons deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of auto_pickup_unloading_cons deleted  :- " + cumcnt);
					logger.error("Exception while deleting auto_pickup_unloading_cons ", e);
					
				}
			
			//-----------------------------------auto_pickup_unloading-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.auto_pickup_unloading where transaction_id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of auto_pickup_unloading deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of auto_pickup_unloading deleted  :- " + cumcnt);
					logger.error("Exception while deleting auto_pickup_unloading ", e);
					
				}
			
			//-----------------------------------con_details-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.con_details where id!=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of con_details deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of con_details deleted  :- " + cumcnt);
					logger.error("Exception while deleting con_details ", e);
					
				}
			
			
			//-----------------------------------user_location_old-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.user_location_old where id!=0 and  timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of user_location_old deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of user_location_old deleted  :- " + cumcnt);
					logger.error("Exception while deleting user_location_old ", e);
					
				}
			
			
		
			//-----------------------------------user_session_data-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.user_session_data where id!=0 and active_flag=0 and created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of user_session_data deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of user_session_data deleted  :- " + cumcnt);
					logger.error("Exception while deleting user_session_data ", e);
					
				}
			
			//-----------------------------------rtms_audit_logs-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.rtms_audit_logs where id!=0 and  timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of rtms_audit_logs deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of rtms_audit_logs deleted  :- " + cumcnt);
					logger.error("Exception while deleting rtms_audit_logs ", e);
					
				}
			
			//-----------------------------------rtms_login_details-----------------------------------------------
			try {
				cnt = 0;cumcnt = 0;
				String query="delete FROM pud.rtms_login_details where id!=0 and  created_timestamp< date_sub(now(), interval 3 month) limit 100000";
				Query q = mt.createNativeQuery(query);
				do {
		            mt.begin();//needs to begin new transaction every time
					cnt = q.executeUpdate();
					cumcnt += cnt;
					mt.commit();
					
					
				} while (cnt == 100000);
				logger.info("Total records of rtms_login_details deleted  :- " + cumcnt);
				}catch(Exception e) {
					e.printStackTrace();
					logger.info("Total records of rtms_login_details deleted  :- " + cumcnt);
					logger.error("Exception while deleting rtms_login_details ", e);
					
				}
			
			result = gson.toJson(new GeneralResponse(
					Constants.TRUE,
					Constants.REQUEST_COMPLETED, 
					null));
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER,null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
		}
		logger.info("Result "+result);
		logger.info("**************END*******************");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
