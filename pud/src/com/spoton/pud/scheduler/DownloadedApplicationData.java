package com.spoton.pud.scheduler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.MobileDataDownloadResult;
import com.spoton.pud.data.MobileDataDownloadResultList;
import com.spoton.pud.data.MobileDataDownloaded;
import com.spoton.pud.data.MobileDataDownloadedList;
import com.spoton.pud.jpa.PulledDataAcknowledgement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;




@WebServlet({"/downloadedApplicationData"})
public class DownloadedApplicationData
  extends HttpServlet
{
  private static final long serialVersionUID = 1L;
  Logger downloadedApplicationLogger = Logger.getLogger("DownloadedApplicationData");
  




  public DownloadedApplicationData() {}
  




  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    doPost(request, response);
  }
  



  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    downloadedApplicationLogger.info("*******START*******");
    response.setContentType("application/json");
    String result = "";ManageTransaction mt = null;
    Gson gson = new GsonBuilder().serializeNulls().create();
    HttpURLConnection con = null;URL obj = null;StringBuilder stringBuilder = null;
    String urlString = FilesUtil.getProperty("downloadedApplicationData");
    downloadedApplicationLogger.info("urlString " + urlString);
    try {
      mt = new ManageTransaction();
      List<Integer> ids = new ArrayList<Integer>();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      List<MobileDataDownloaded> mobDataDownload = new ArrayList<MobileDataDownloaded>();
      String pickupQuery = "select transaction_id from pulled_data_acknowledgement where download_type=2";
      Query pq = mt.createNativeQuery(pickupQuery);
      List<Integer> pickupsDownloadedlist = pq.getResultList();
     // System.out.println("pickupsDownloadedlist " + gson.toJson(pickupsDownloadedlist));
      //downloadedApplicationLogger.info("pickupsDownloadedlist " + gson.toJson(pickupsDownloadedlist));
      String deliveryQuery = "select transaction_id from pulled_data_acknowledgement where download_type=1";
      Query dq = mt.createNativeQuery(deliveryQuery);
      List<Integer> deliveriesDownloadedlist = dq.getResultList();
      //downloadedApplicationLogger.info("deliveriesDownloadedlist " + gson.toJson(deliveriesDownloadedlist));
      
      String query = "select id,pulled_time from pud_data where pulled_time!=?1";
      Query nq = mt.createNativeQuery(query).setParameter(1, "NULL");
      List<Object[]> list = nq.getResultList();
      if ((list != null) && (list.size() > 0)) {
       // downloadedApplicationLogger.info("pud data list " + gson.toJson(list));
        for (Object[] o : list) {
          if ((pickupsDownloadedlist != null) && (!pickupsDownloadedlist.contains(Integer.valueOf(((Integer)o[0]).intValue())))) {
            MobileDataDownloaded md = new MobileDataDownloaded();
            PulledDataAcknowledgement pd = new PulledDataAcknowledgement();
            md.setTransactionId(Integer.toString(((Integer)o[0]).intValue()));
            md.setDownloadType("2");
            md.setDownloadDate(format.format((Date)o[1]));
            mobDataDownload.add(md);
            pd.setCreatedTimestamp(new Date());
            pd.setDownloadDate((Date)o[1]);
            pd.setDownloadType(2);
            pd.setTransactionId(((Integer)o[0]).intValue());
            mt.persist(pd);
            mt.commit();
            ids.add(Integer.valueOf(pd.getId()));
          }
        }
      }
      


      String query2 = "select c.id,a.pulled_time from agent_pdc a,delivery_con_details c where a.pulled_time!=?1 and a.pdc=c.pdc";
      Query nq2 = mt.createNativeQuery(query2).setParameter(1, "NULL");
      List<Object[]> list2 = nq2.getResultList();
      

      if ((list2 != null) && (list2.size() > 0)) {
        //downloadedApplicationLogger.info("delivery conId list " + gson.toJson(list2));
        for (Object[] o : list2) {
          if ((deliveriesDownloadedlist != null) && (!deliveriesDownloadedlist.contains(Integer.valueOf(((Integer)o[0]).intValue())))) {
            MobileDataDownloaded md = new MobileDataDownloaded();
            PulledDataAcknowledgement pd = new PulledDataAcknowledgement();
            md.setTransactionId(Integer.toString(((Integer)o[0]).intValue()));
            md.setDownloadType("1");
            md.setDownloadDate(format.format((Date)o[1]));
            mobDataDownload.add(md);
            pd.setCreatedTimestamp(new Date());
            pd.setDownloadDate((Date)o[1]);
            pd.setDownloadType(1);
            pd.setTransactionId(((Integer)o[0]).intValue());
            mt.persist(pd);
            mt.commit();
            ids.add(Integer.valueOf(pd.getId()));
          }
        }
      }
      


     // downloadedApplicationLogger.info("ids List " + gson.toJson(ids));
     // downloadedApplicationLogger.info("MobDataDownload List " + gson.toJson(mobDataDownload));
      //System.out.println("MobDataDownload" + gson.toJson(mobDataDownload));
      if ((mobDataDownload != null) && (mobDataDownload.size() > 0)) {
        MobileDataDownloadedList downloadedData = new MobileDataDownloadedList();
        downloadedData.setMobDataDownload(mobDataDownload);
        downloadedApplicationLogger.info("DataSent to Spoton " + gson.toJson(downloadedData));
        System.out.println("DataSent to Spoton " + gson.toJson(downloadedData));
        obj = new URL(urlString);
        con = (HttpURLConnection)obj.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
        streamWriter.write(gson.toJson(downloadedData));
        streamWriter.flush();
        downloadedApplicationLogger.info("ResponseCode " + con.getResponseCode());
        if (con.getResponseCode() == 200) {
          stringBuilder = new StringBuilder();
          InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
          BufferedReader bufferedReader = new BufferedReader(streamReader);
          String response1 = null;
          while ((response1 = bufferedReader.readLine()) != null) {
            stringBuilder.append(response1 + "\n");
          }
          bufferedReader.close();
          //System.out.println("Data Received From Spoton " + stringBuilder.toString());
          downloadedApplicationLogger.info("Data Received From Spoton " + stringBuilder.toString());
          MobileDataDownloadResultList downloadResultList = (MobileDataDownloadResultList)gson.fromJson(stringBuilder.toString(), MobileDataDownloadResultList.class);
          downloadedApplicationLogger.info("downloadResultList " + gson.toJson(downloadResultList));
          if ((downloadResultList != null) && (downloadResultList.getMobDataDownloadResult() != null) && (downloadResultList.getMobDataDownloadResult().size() > 0)) {
            int i = 0;
            for (MobileDataDownloadResult r : downloadResultList.getMobDataDownloadResult()) {
              PulledDataAcknowledgement pd = (PulledDataAcknowledgement)mt.find(PulledDataAcknowledgement.class, ids.get(i));
              pd.setResultRemarks(r.getResultRemarks());
              pd.setResultTransactionId(r.getTransactionId());
              pd.setResultFlag(r.isResultFlag() ? 1 : 0);
              pd.setCreatedTimestamp(new Date());
              mt.persist(pd);
              mt.commit();
              i++;
            }
            
            result = gson.toJson(new GeneralResponse(true, "Request Completed", null));
          } else {
            result = gson.toJson(new GeneralResponse(true, "Null Response From ERP", null));
          }
        }
        else
        {
          System.out.println("Response failed");
          
          result = gson.toJson(new GeneralResponse(false, "Response failed from ERP", null));
        }
      }
      else {
        result = gson.toJson(new GeneralResponse(false, "No new Data Found", null));
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      downloadedApplicationLogger.info(" EXCEPTION IN SERVER" + e);
      result = gson.toJson(new GeneralResponse(false, 
        "Exception in server", null));
    }
    finally
    {
      if (mt != null)
        mt.close();
      if (con != null) {
        con.disconnect();
      }
    }
    System.out.println("DownloadedApplicationData result" + result);
    downloadedApplicationLogger.info("DownloadedApplicationData result" + result);
    downloadedApplicationLogger.info("************END************");
    response.getWriter().write(result);
  }
}