package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.DeliveryConditionMasterErpModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.jpa.DeliveryConditionMaster;

/**
 * Servlet implementation class SaveDeliveryConditionMaster
 */
@WebServlet("/saveDeliveryConditionMaster")
public class SaveDeliveryConditionMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SaveDeliveryConditionMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveDeliveryConditionMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*****************Start***************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("deliveryConditionMaster");
		
		StringBuilder stringBuilder = null;
		ManageTransaction mt=null;
		try{
			obj=new URL(urlString); 
			 con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			con.setReadTimeout(60000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			 if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
	            	stringBuilder=new StringBuilder();
	                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	                String response1 = null;
	                while ((response1 = bufferedReader.readLine()) != null) {
	                    stringBuilder.append(response1 + "\n");
	                }
	                bufferedReader.close();
	                streamReader.close();
	                logger.info("Response from server : "+stringBuilder.toString());
	                Type data = new TypeToken<ArrayList<DeliveryConditionMasterErpModal>>(){}.getType();
	    			List<DeliveryConditionMasterErpModal> dataList =  gson.fromJson(stringBuilder.toString(),data);
	    			if(dataList!=null&&dataList.size()>0) {
	    				mt=new ManageTransaction();
	    				String query="select condition_id from delivery_condition_master";
	    				Query qry = mt.createNativeQuery(query);
	    				@SuppressWarnings("unchecked")
						List<Integer> list=qry.getResultList();
	    				
	    				for(DeliveryConditionMasterErpModal b:dataList) {
	    					if(!list.contains(Integer.parseInt(b.getCondID()))) {
	    					DeliveryConditionMaster md=new DeliveryConditionMaster();
	    					md.setConditionId(Integer.parseInt(b.getCondID()));
	    					md.setConditionName(b.getCondName());
	    					mt.persist(md);
	    					}
	    				}
	    				mt.commit();
	    				List <String> pushListTokens = CommonTasks.getpushTokens(mt);
	    				 Integer isUpdateMandatory=0;
	    				if(pushListTokens!=null&&pushListTokens.size()>0) {
	    					 PushDataModel pd=new PushDataModel();
	    		     			pd.setId(20);
	    		     	      pd.setMasterType(5);
	    		     	      String updateQuery="SELECT is_update_mandatory FROM masters_update where master_type_id=5";
	    		     	     Query q=mt.createNativeQuery(updateQuery);
	    		     	     try {
	    		     	      isUpdateMandatory=(Integer) q.getSingleResult();
	    		     	     }catch(NoResultException nr) {
	    		     	    	 nr.printStackTrace();
	    		     	     }
	    		     	     pd.setUpdateMandatory(isUpdateMandatory==1?true:false);
	    		     	      boolean pushReturn = CommonTasks.sendPushNotification("DeliveryConditionMaster_Update", "DeliveryConditionMaster_Update", pushListTokens , logger, gson, pd);
	    		     	     logger.info("pushReturn : "+pushReturn);
	    				        if(pushReturn == true)
	    				        {
	    				        	logger.info(" pushSent Successfully ");
	    				        	
	    				        }
	    				        else{
	    				        	logger.info(Constants.GCM_FAILURE);
	    				        	
	    				        }
	    					}
	    				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
	    			}
                
               
		 }else {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From Erp with response code as "+con.getResponseCode(),null));
			}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
			if(con!=null) {
				con.disconnect();
			}
		}
	
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
		}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
