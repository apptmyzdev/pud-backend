package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.AirFlightWeightMasterErpModal;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PieceMasterModal;
import com.spoton.pud.jpa.AirFlightWeightMaster;

/**
 * Servlet implementation class DownloadAirFlightWeightMaster
 */
@WebServlet("/downloadAirFlightWeightMaster")
public class DownloadAirFlightWeightMaster extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("DownloadAirFlightWeightMaster");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadAirFlightWeightMaster() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("*****************Start***************");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
     	String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("airFlightWeightMaster");
		StringBuilder stringBuilder = null;
		ManageTransaction mt=null;
		try{
			obj=new URL(urlString); 
			 con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			con.setReadTimeout(60000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			 if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
	            	stringBuilder=new StringBuilder();
	                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
	                BufferedReader bufferedReader = new BufferedReader(streamReader);
	                String response1 = null;
	                while ((response1 = bufferedReader.readLine()) != null) {
	                    stringBuilder.append(response1 + "\n");
	                }
	                bufferedReader.close();
	                streamReader.close();
	                logger.info("Response from server : "+stringBuilder.toString());
	               
	                Type data = new TypeToken<ArrayList<AirFlightWeightMasterErpModal>>(){}.getType();
	    			List<AirFlightWeightMasterErpModal> dataList =  gson.fromJson(stringBuilder.toString(),data);
	    			if(dataList!=null&&dataList.size()>0) {
	    				mt=new ManageTransaction();
	    				String query="truncate air_flight_weight_master";
	    				Query qry = mt.createNativeQuery(query);
	    				mt.begin();
	    				qry.executeUpdate();
	    				logger.info("Data Truncated");
	    				for(AirFlightWeightMasterErpModal m:dataList) {
	    				AirFlightWeightMaster entity=new AirFlightWeightMaster();
	    				entity.setCreatedTimestamp(new Date());
	    				entity.setFlightDesc(m.getFlightDesc());
	    				entity.setFlightId(m.getFlightID());
	    				entity.setVendorCode(m.getVendorCode());
	    				entity.setVendorId(m.getVendorid());
	    				entity.setWeightAllowed(m.getWtAllowed());
	    				mt.persist(entity);
	    				}
	    				mt.commit();
	    				result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
	    			}
			 }
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
			if(con!=null) {
				con.disconnect();
			}
		}
	
		response.getWriter().write(result);
		logger.info("Result "+result);
		logger.info("*****************END***************");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
