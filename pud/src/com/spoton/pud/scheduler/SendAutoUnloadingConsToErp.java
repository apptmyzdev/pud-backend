package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.AutoPickupUnloadingErpInputModal;
import com.spoton.pud.data.AutoPickupUnloadingErpInputModalList;
import com.spoton.pud.data.AutoPickupUnloadingErpResponseModal;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.jpa.AutoPickupUnloading;
import com.spoton.pud.jpa.AutoPickupUnloadingCon;
import com.spoton.pud.jpa.ConDetail;


/**
 * Servlet implementation class SendAutoUnloadingConsToErp
 */
@WebServlet("/sendAutoUnloadingConsToErp")
public class SendAutoUnloadingConsToErp extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("SendAutoUnloadingConsToErp");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendAutoUnloadingConsToErp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con = null;
		URL obj = null;
		String urlString = FilesUtil.getProperty("autoPickupUnloading");
		StringBuilder stringBuilder = null;
		ManageTransaction mt = null;
		try {
			mt=new ManageTransaction();
			/*String query="SELECT a.transaction_id,c.con_number,a.user_id,a.vehicle_number FROM pud.auto_pickup_unloading a ,pud.auto_pickup_unloading_cons c,pud.con_details cd,pud.pickup_updation_status s "
					+ "where a.transaction_id=c.auto_pickup_unloading_id and  s.con_entry_id=cd.con_entry_id and s.transaction_result=?1 and a.transaction_result is null "
					+ "and c.con_number collate utf8_general_ci = cd.con_number collate utf8_general_ci and cd.erp_updated=1 and a.pickup_unloading_sheet_number is null";*/
			Calendar yesterday=Calendar.getInstance();
			yesterday.add(Calendar.DATE, -1);
			
			Calendar twoDaysBack=Calendar.getInstance();
			twoDaysBack.add(Calendar.DATE, -2);
			twoDaysBack.set(Calendar.HOUR_OF_DAY, 00);
			twoDaysBack.set(Calendar.MINUTE, 00);
			twoDaysBack.set(Calendar.SECOND, 00);
			twoDaysBack.set(Calendar.MILLISECOND, 0);
		
			
			Calendar currentDateEvng=Calendar.getInstance();
			currentDateEvng.setTime(new Date());
			currentDateEvng.set(Calendar.HOUR_OF_DAY, 23);
			currentDateEvng.set(Calendar.MINUTE, 59);
			currentDateEvng.set(Calendar.SECOND, 59);
			currentDateEvng.set(Calendar.MILLISECOND, 0);
			
			/*String query="SELECT a.transaction_id,c.con_number,a.user_id,a.vehicle_number,c.is_con_booked,c.booked_con_status,a.transaction_result,a.created_timestamp,c.con_details_id FROM pud.auto_pickup_unloading a ,pud.auto_pickup_unloading_cons c "
					+ "where a.transaction_id=c.auto_pickup_unloading_id and a.created_timestamp>=?1 and a.created_timestamp<=?2";*/ //removing joins
					
			String query="SELECT c.auto_pickup_unloading_id,c.con_number,c.user_id,c.vehicle_number,c.is_con_booked,c.booked_con_status,c.transaction_result,c.created_timestamp,c.con_details_id,c.id FROM pud.auto_pickup_unloading_cons c "
					+ "where  c.created_timestamp>=?1 and c.created_timestamp<=?2 ";
			
			Query q=mt.createNativeQuery(query).setParameter(1, twoDaysBack.getTime()).setParameter(2, currentDateEvng.getTime());
			List<Object[]> list=q.getResultList();
			if(list!=null&&list.size()>0) {
				List<String> conList=null;AutoPickupUnloadingErpInputModal ip=null;
				Map<Integer,AutoPickupUnloadingErpInputModal> map=new HashMap<Integer,AutoPickupUnloadingErpInputModal>();
				List<Integer> removedIdsList=new ArrayList<Integer>();
				List<Integer> conDetailIdsList=new ArrayList<Integer>();
				Map<Integer,List<Integer>> unloadingConIdsMap=new HashMap<Integer,List<Integer>>();
				List<Integer> unloadingConIdsList=null;
				for(Object[] o:list) {
					
					if(o[6]==null||(((String)o[6]).equalsIgnoreCase("false")&&((Date)o[7]).after(yesterday.getTime()))) {
				/*if(!map.containsKey((int) o[0])&&(int) o[4]==1&&o[5]!=null&&((String) o[5]).equalsIgnoreCase("Y")&&(!removedIdsList.contains((int) o[0]))) {*/
				/*if(!map.containsKey((int) o[0])&&(int) o[4]==1&&(!removedIdsList.contains((int) o[0]))) {*/
						if(!map.containsKey((int) o[0])&&(int) o[4]==1) {
					if(o[5]!=null&&((String) o[5]).equalsIgnoreCase("Y")) {
					ip=new AutoPickupUnloadingErpInputModal();
					ip.setTransactionID((int) o[0]);
					ip.setUserID((String) o[2]);
					ip.setVehicleNo((String) o[3]);
					conList=new ArrayList<String>();
				    conList.add((String) o[1]);
				    ip.setConNo(conList);
					map.put(ip.getTransactionID(), ip);
					//logger.info("Added Transaction Id "+(int) o[0]);
					conDetailIdsList.add((int) o[8]);
					if(!unloadingConIdsMap.containsKey(ip.getTransactionID())) {
					unloadingConIdsList=new ArrayList<Integer>();
					}
					unloadingConIdsList.add((int) o[9]);
					unloadingConIdsMap.put(ip.getTransactionID(), unloadingConIdsList);
					
					
					}else if(o[5]!=null&&((String) o[5]).equalsIgnoreCase("N")) {
						if(!unloadingConIdsMap.containsKey((int) o[0])) {
						unloadingConIdsList=new ArrayList<Integer>();
						}
						unloadingConIdsList.add((int) o[9]);
						unloadingConIdsMap.put((int) o[0], unloadingConIdsList);
						
					}
					
				}else {
					if(map.containsKey((int) o[0])) {
					ip=map.get((int) o[0]);
					if((int) o[4]==1) {
					if(o[5]!=null&&((String) o[5]).equalsIgnoreCase("Y")) {
						if(!ip.getConNo().contains((String) o[1])) {//as from device we are getting duplicate cons
					ip.getConNo().add((String) o[1]);
					conDetailIdsList.add((int) o[8]);
					unloadingConIdsMap.get((int) o[0]).add((int) o[9]);
						}
					}else if(o[5]!=null&&((String) o[5]).equalsIgnoreCase("N")) {
						unloadingConIdsMap.get((int) o[0]).add((int) o[9]);
					}
					map.put((int) o[0], ip);
					}/*else {
						if(!ip.getConNo().contains((String) o[1])) {//as from device we are getting duplicate cons 
						map.remove((int) o[0]);
						removedIdsList.add((int) o[0]);
						//logger.info("Removed Transaction Id "+(int) o[0]);
						}
					}*/
				}
				}
				}
				}
				/*List<AutoPickupUnloadingErpInputModal> erpDataList=new ArrayList<AutoPickupUnloadingErpInputModal>();
				for(AutoPickupUnloadingErpInputModal erpData:map.values()) {
					erpDataList.add(erpData);
				}*/
				if(map!=null&&map.values().size()>0) {
				AutoPickupUnloadingErpInputModalList erpDataList=new  AutoPickupUnloadingErpInputModalList();
				erpDataList.setAutoPickupUnloadingInput(map.values());
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(60000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);

				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				streamWriter.write(gson.toJson(erpDataList));
				streamWriter.flush();
				streamWriter.close();
				logger.info("Datasent to spoton " + gson.toJson(erpDataList));
				logger.info("Response Code : " + con.getResponseCode());
				if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stringBuilder = new StringBuilder();
					InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
					BufferedReader bufferedReader = new BufferedReader(streamReader);
					String response1 = null;
					while ((response1 = bufferedReader.readLine()) != null) {
						stringBuilder.append(response1);
					}
					bufferedReader.close();
					streamReader.close();
					logger.info("Response From Spoton : " + stringBuilder.toString());
					Type data=new TypeToken<ArrayList<AutoPickupUnloadingErpResponseModal>>(){}.getType();
					List<AutoPickupUnloadingErpResponseModal> erpResponseList=gson.fromJson(stringBuilder.toString(),data);
				    if(erpResponseList!=null&&erpResponseList.size()>0) {
				    	for(AutoPickupUnloadingErpResponseModal erpResponse:erpResponseList) {
							AutoPickupUnloading ap=mt.find(AutoPickupUnloading.class,erpResponse.getTransactionID());
							if(ap!=null) {
								ap.setTransactionResult(erpResponse.getTransationResult());
								ap.setTransactionMessage(erpResponse.getTransationMessage());
								ap.setPickupUnloadingSheetNumber(erpResponse.getPickupUnloadingSheetNo());
								mt.persist(ap);
								List<Integer> autoUnloadingConIdsList=unloadingConIdsMap.get(erpResponse.getTransactionID());
								for(Integer id:autoUnloadingConIdsList) {
									AutoPickupUnloadingCon ac=mt.find(AutoPickupUnloadingCon.class, id);
									 	if(ac!=null) {
									   ac.setTransactionResult(erpResponse.getTransationResult());
										ac.setPickupUnloadingSheetNumber(erpResponse.getPickupUnloadingSheetNo());
										ConDetail cd=mt.find(ConDetail.class, ac.getConDetailsId());
										if(cd!=null) {
										cd.setIsUnloadingSheetGenerated(1);
							    		mt.persist(cd);
										}
									}
								}
							}
				    	}
				    	/*if(	conDetailIdsList!=null&&conDetailIdsList.size()>0) {
				    		for(Integer id:conDetailIdsList) {
				    			if(id!=0) {
				    		ConDetail cd=mt.find(ConDetail.class, id);
				    		if(cd!=null) {
				    		cd.setIsUnloadingSheetGenerated(1);
				    		mt.persist(cd);
				    		}
				    			}
				    		}
				    	}*/
				    	
				    	mt.commit();
				    	
				    }
				    result = gson.toJson(new GeneralResponse(Constants.TRUE,"Fetched All Acknowledged Cons and sent to erp", null));
				} else {
					logger.info("Response failed from Spoton");
					result = gson.toJson(new GeneralResponse(Constants.FALSE,
							"Response failed from Spoton with Response code as " + con.getResponseCode(), null));
				}
				}else{
					  result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Cons Found", null));
				  }
			}else{
				  result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Cons Found", null));
			  }
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception e ", e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally {
			if(mt!=null) {
				mt.close();
			}
			if(con!=null) {
				con.disconnect();
			}
		}
		logger.info("result " + result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
