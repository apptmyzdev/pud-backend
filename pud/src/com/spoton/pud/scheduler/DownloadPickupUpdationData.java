package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;




import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsData;

import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupAcknowledgementData;
import com.spoton.pud.data.PickupUpdationData;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceVolumeData;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesImagesData;
import com.spoton.pud.data.WSTPiecesRefListData;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.CustomerMasterData;
import com.spoton.pud.jpa.PickupDataAcknowledgement;
import com.spoton.pud.jpa.Piece;
import com.spoton.pud.jpa.PieceEntry;
import com.spoton.pud.jpa.PieceVolume;
import com.spoton.pud.jpa.PiecesImage;
import com.spoton.pud.jpa.WstPiecesRefList;


/**
 * Servlet implementation class DownloadPickupUpdationData
 */
@WebServlet("/downloadPickupUpdationData")
public class DownloadPickupUpdationData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("DownloadPickupUpdationData");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadPickupUpdationData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		HttpURLConnection con=null; URL obj =null;
		 HttpURLConnection con1=null; URL obj1 =null;
		String urlString=FilesUtil.getProperty("pickupUpdationDataFromErp");
		StringBuilder stringBuilder = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
			obj = new URL(urlString);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setConnectTimeout(5000);
			 con.setReadTimeout(60000);
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			logger.info("url called "+urlString);
			 logger.info("ResponseCode "+con.getResponseCode());
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
				stringBuilder=new StringBuilder();
                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(streamReader);
                String response1 = null;
                while ((response1 = bufferedReader.readLine()) != null) {
                    stringBuilder.append(response1);
                }
                bufferedReader.close();
                streamReader.close();
                logger.info("Response From Spoton : " +stringBuilder.toString());
                PickupUpdationData responseList=gson.fromJson(stringBuilder.toString(),PickupUpdationData.class);
                if(responseList!=null&&responseList.getConDetails()!=null&&responseList.getConDetails().size()>0){
                	mt=new ManageTransaction();
                	Map<Integer,Integer> pickupIdsMap=new HashMap<Integer, Integer>();
                	/*List<String> downloadedCons=new ArrayList<String>();
                	String query="Select c.conNumber,c.orderNo,c.userName,c.pickupScheduleId from ConDetail c where c.pickupType=1";
                	TypedQuery<Object[]> tq=mt.createQuery(Object[].class, query);
                	List<Object[]> list=tq.getResultList();
                	for(Object[] o:list){
                		String key=(String) o[0]+"-"+(String) o[1]+"-"+(String) o[2]+"-"+(int) o[3];
                		downloadedCons.add(key);
                	}*/
                	for(ConDetailsData conList:responseList.getConDetails()){
                		//if(!downloadedCons.contains(conList.getConNumber()+"-"+conList.getOrderNo()+"-"+conList.getUserName()+"-"+conList.getCRMScheduleID())){
                		ConDetail cd=new ConDetail();
                		cd.setActualWeight(conList.getActualWeight());
	    				if(conList.getActualWeight()!=null)
	    				cd.setApplyDc(conList.getApplyDC());
	    				cd.setApplyNfForm(conList.getApplyNFForm());
	    				cd.setApplyVtv(conList.getApplyVTV());
	    				cd.setConNumber(conList.getConNumber());
	    				cd.setConsignmentType(conList.getConsignmentType());
	    				cd.setCrmScheduleId(conList.getCRMScheduleID());
	    				cd.setCustomerCode(conList.getCustomerCode());
	    				cd.setDeclaredValue(conList.getDeclaredValue());
	    				cd.setDestinationPincode(conList.getDestinationPINCode());
	    				cd.setErpUpdated(1);
	    				cd.setGatePassTime(conList.getGatePassTime());
	    				cd.setImage1Url(conList.getImage1URL());
	    				cd.setLatValue(conList.getLatvalue());
	    				cd.setLongValue(conList.getLongvalue());
	    				cd.setNoOfPackage(conList.getNoofPackage());
	    				cd.setProductType(conList.getProductType());
	    				cd.setOrderNo(conList.getOrderNo());
	    				cd.setOriginPincode(conList.getOriginPINCode());
	    				cd.setPackageType(conList.getPackageType());
	    				cd.setPanNo(conList.getPANNo());
	    				cd.setPaymentBasis(conList.getPaymentbasis());
	    				if(conList.getPickupdate()!=null)
	    				cd.setPickupDate(format.parse(conList.getPickupdate()));
	    				cd.setProduct(conList.getProduct());
	    				cd.setReceiverName(conList.getReceiverName());
	    				cd.setReceiverPhoneNo(conList.getReceiverPhoneNo());
	    				cd.setRefNumber(conList.getRefNumber());
	    				cd.setRiskType(conList.getRiskType());
	    				cd.setShipmentImageUrl(conList.getShipmentImageURL());
	    				cd.setSpecialInstruction(conList.getSpecialInstruction());
	    				cd.setTinNo(conList.getTINNo());
	    				cd.setTotalVolWeight(conList.getTotalVolWeight());
	    				cd.setUserId(conList.getUserID());
	    				cd.setUserName(conList.getUserName());
	    				cd.setPickupType(1);
	    				cd.setVolType(conList.getVolType());
	    				cd.setVtcAmount(conList.getVTCAmount());
	    				cd.setCreatedTimestamp(new Date());
	    				cd.setVehicleNumber(conList.getVehicleNo());
	    				cd.setEwayBillNumber(conList.getEwayBillNo());
	    				cd.setPickupScheduleId(Integer.parseInt(conList.getCRMScheduleID()));
	    				cd.setIsBoxPieceMapping(conList.getIsBoxPieceMapping());
	    				if(conList.getCustomerCode()!=null){
	    					CustomerMasterData cm=mt.find(CustomerMasterData.class, conList.getCustomerCode());
	    				if(cm!=null)
	    					cd.setCustomerName(cm.getCustomerName());
	    				}	    					
	    				mt.persist(cd);
	    				Piece p=null;
	    				if(conList.getPieces()!=null&&conList.getPieces().size()>0){
		    				for(PiecesData pc:conList.getPieces()){
		    				p=new Piece();
		    				p.setLastPieceNo(pc.getLastPieceNumber());
		    				p.setPcrKey(Integer.parseInt(pc.getPCR_Key()));
		    				p.setTotalUsed(pc.getTotalUsed());
		    				p.setConDetail(cd);
		    				p.setCreatedTimestamp(new Date());
		    				p.setConNumber(conList.getConNumber());
    	    				p.setErpUpdated(0);
    						 if(conList.getPickupdate()!=null)
    							 p.setPickupDate(format.parse(conList.getPickupdate()));
    						 p.setPickupScheduleId(conList.getCRMScheduleID()!=null?Integer.parseInt(conList.getCRMScheduleID()):0);
    						 p.setPickupType(cd.getPickupType());
    						 p.setUserName(conList.getUserName());
    						 p.setCustomerCode(conList.getCustomerCode());
		    				mt.persist(p);
		    				}
		    				}
	    				PieceEntry pieceEntry=null;
	    				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
		    				for(PieceEntryData pe:conList.getPieceEntry()){
		    					pieceEntry=new PieceEntry();
		    					pieceEntry.setConDetail(cd);
		    					pieceEntry.setFromPieceNo(pe.getFromPieceNo());
		    					pieceEntry.setPcrKey(Integer.parseInt(pe.getPCR_Key()));
		    					pieceEntry.setToPieceNo(pe.getToPieceNo());
		    					pieceEntry.setTotalPieces(pe.getTotalPieces());
		    					pieceEntry.setCreatedTimestamp(new Date());
		    					pieceEntry.setConNumber(conList.getConNumber());
		    					pieceEntry.setErpUpdated(0);
	    						 if(conList.getPickupdate()!=null)
	    							 pieceEntry.setPickupDate(format.parse(conList.getPickupdate()));
	    						 pieceEntry.setPickupScheduleId(conList.getCRMScheduleID()!=null?Integer.parseInt(conList.getCRMScheduleID()):0);
	    						 pieceEntry.setPickupType(cd.getPickupType());
	    						 pieceEntry.setUserName(conList.getUserName());
	    						 pieceEntry.setCustomerCode(conList.getCustomerCode());
			    				mt.persist(pieceEntry);
		    				}
	    				
	    				PieceVolume pieceVolume=null;
	    				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
		    				for(PieceVolumeData pv:conList.getPieceVolume()){
		    					pieceVolume=new PieceVolume();
		    					pieceVolume.setConDetail(cd);
		    					pieceVolume.setNoOfPieces(pv.getNoofPieces());
		    					pieceVolume.setTotalVolWeight(pv.getTotalVolWeight());
		    					pieceVolume.setVolBreadth(pv.getVolbreadth());
		    					pieceVolume.setVolHeight(pv.getVolHeight());
		    					pieceVolume.setVolLength(pv.getVolLength());
		    					pieceVolume.setCreatedTimestamp(new Date());
		    					pieceVolume.setConNumber(conList.getConNumber());
		    					pieceVolume.setErpUpdated(0);
	    						 if(conList.getPickupdate()!=null)
	    							 pieceVolume.setPickupDate(format.parse(conList.getPickupdate()));
	    						 pieceVolume.setPickupScheduleId(conList.getCRMScheduleID()!=null?Integer.parseInt(conList.getCRMScheduleID()):0);
	    						 pieceVolume.setPickupType(cd.getPickupType());
	    						 pieceVolume.setUserName(conList.getUserName());
	    						 pieceVolume.setCustomerCode(conList.getCustomerCode());
			    				mt.persist(pieceVolume);	
		    				}
	    				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
	    					for(PiecesImagesData pi:conList.getPiecesImages()){
	    						PiecesImage pieceImage=new PiecesImage();
	    						  pieceImage.setConDetail(cd);
	   	    				      pieceImage.setCreatedTimestamp(new Date());
	   	    				   pieceImage.setPiecesImagesUrl(pi.getPiecesImagesURL());
	   	    				pieceImage.setConNumber(conList.getConNumber());
	   	    				pieceImage.setErpUpdated(0);
    						 if(conList.getPickupdate()!=null)
    							 pieceImage.setPickupDate(format.parse(conList.getPickupdate()));
    						 pieceImage.setPickupScheduleId(conList.getCRMScheduleID()!=null?Integer.parseInt(conList.getCRMScheduleID()):0);
    						 pieceImage.setPickupType(cd.getPickupType());
    						 pieceImage.setUserName(conList.getUserName());
    						 pieceImage.setCustomerCode(conList.getCustomerCode());
	   	    				mt.persist(pieceImage);
	    					}
	    				}
	    				
	    				WstPiecesRefList wstPieces=null;
	    				if(conList.getWSTPiecesRefList()!=null&&conList.getWSTPiecesRefList().size()>0) {
	    					for(WSTPiecesRefListData w:conList.getWSTPiecesRefList()) {
	    						wstPieces=new WstPiecesRefList();
	    						wstPieces.setConDetail(cd);
	    						wstPieces.setConNumber(w.getConNumber());
	    						wstPieces.setCreatedTimestamp(new Date());
	    						wstPieces.setPieceNumber(w.getPieceNo());
	    						wstPieces.setPieceRefNumber(w.getPieceRefNo());
	    						wstPieces.setPieceSerialNumber(w.getPieceSlNo());
	    						wstPieces.setErpUpdated(0);
	    						 if(conList.getPickupdate()!=null)
	    							 wstPieces.setPickupDate(format.parse(conList.getPickupdate()));
	    						 wstPieces.setPickupScheduleId(conList.getCRMScheduleID()!=null?Integer.parseInt(conList.getCRMScheduleID()):0);
	    						 wstPieces.setPickupType(cd.getPickupType());
	    						 wstPieces.setUserName(conList.getUserName());
	    						mt.persist(wstPieces);
	    					}
	    				}
	    				/*if(conList.getEwayBillNoList()!=null&&conList.getEwayBillNoList().size()>0){
	    					for(EwayBillNo e:conList.getEwayBillNoList()){
	    						UpdatedEwayBillNumber updatedEway=new UpdatedEwayBillNumber();
	    						updatedEway.setConDetail(cd);
	    						updatedEway.setCreatedTimestamp(new Date());
	    						updatedEway.setEwayBillNumber(e.getEwayBillNum());
	    						mt.persist(updatedEway);
	    					}
	    				}*/
	    				
	    			
	    				mt.commit();
	    				ConDetail cd1=mt.find(ConDetail.class, cd.getId());
	    				cd1.setConEntryId(cd.getId());
	    				mt.persist(cd1);mt.commit();
	    				pickupIdsMap.put(Integer.parseInt(conList.getCRMScheduleID()),cd.getId());
						
                		
                		
                		
                		
                		/*}else{
                			logger.info("Already in db -ConNumber - "+conList.getConNumber());
                		}*/
                	} //end of main for
                	
                	for(Map.Entry<Integer,Integer> entry:pickupIdsMap.entrySet()){
                	JSONObject jsonObj = new JSONObject();
		        	jsonObj.put("PickupscheduleID",entry.getKey());
		        	jsonObj.put("RevaPickUpID",entry.getValue());
		        	logger.info("Json object posted back to ERP "+jsonObj.toString());
		        	 obj1 = new URL(FilesUtil.getProperty("downloadPickupDataAcknowledgementUrl"));
						con1 = (HttpURLConnection) obj1.openConnection();
						con1.setRequestMethod("POST");
						con1.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
						con1.setRequestProperty("Accept", "application/json");
						con1.setDoOutput(true);
						con1.setConnectTimeout(5000);
						con1.setReadTimeout(60000);
					 OutputStreamWriter streamWriter = new OutputStreamWriter(con1.getOutputStream());
		        	streamWriter.write(jsonObj.toJSONString());
					streamWriter.flush();
					streamWriter.close();
		            if (con1.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader1 = new InputStreamReader(con1.getInputStream());
		                BufferedReader bufferedReader1 = new BufferedReader(streamReader1);
		                String response2 = null;
		                while ((response2 = bufferedReader1.readLine()) != null) {
		                    stringBuilder.append(response2 + "\n");
		                }
		                bufferedReader1.close();
		                streamReader1.close();
		                //System.out.println("stringBuilder.toString()"+stringBuilder.toString());
		                int responseCode2 = con1.getResponseCode();
		    			//System.out.println("Response Code 2 : " +responseCode2);
		    			 logger.info("Response Code 2 : "+responseCode2);
		    			 logger.info("Response from spoton for acknowledgement sent "+stringBuilder.toString());
		    			
		    				 Type pickupListType = new TypeToken<ArrayList<PickupAcknowledgementData>>(){}.getType();
		    				// logger.info("pickupListType "+pickupListType);
		    			     List<PickupAcknowledgementData> pickupList=gson.fromJson(stringBuilder.toString(), pickupListType);
		    			     //System.out.println("pickupList "+pickupList);
		    			    // logger.info("Posted data response list: "+gson.toJson(pickupList));
		    			     PickupDataAcknowledgement pd=null;
		    			   for(PickupAcknowledgementData ad:pickupList){
		    			    	 pd=new PickupDataAcknowledgement();
		    			    	 pd.setPickupScheduleId(ad.getPickupScheduleId());
		    			    	 pd.setTransactionRemarks(ad.getTransactionRemarks());
		    			    	 pd.setTransactionResult(ad.getTransactionResult());
		    			    	 pd.setCreatedTimestamp(new Date());
		    			         pd.setPickupType(1);
		    			         pd.setConEntryId(pickupIdsMap.get((ad.getPickupScheduleId())));
		    			         mt.persist(pd);
		    			    	
		    			     }
		    			   mt.commit();
		    			 
		    			
		    			 
		            }
                	}
                	
                	
                	
                	result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
                }else{
                	result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Response From ERP", null));
                }
			}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
			}
                	}catch(Exception e){
            			e.printStackTrace();
            			logger.error("Exception e ",e);
            			result = gson.toJson(new GeneralResponse(Constants.FALSE,
            					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
            		}finally{
            			if(mt!=null){mt.close();}
            			if(con!=null) {
            				con.disconnect();
            			}
            			if(con1!=null) {
            				con1.disconnect();
            			}
            		}
		logger.info("result "+result);
		logger.info("**********END***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
