package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;

import com.spoton.pud.data.DocketSeriesModel;
import com.spoton.pud.data.GcmPushResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.jpa.DocketSeriesMaster;
import com.spoton.pud.mobileoperations.SendMessage;

/**
 * Servlet implementation class GetDocketSeriesData
 */
@WebServlet("/getDocketSeriesData")
public class GetDocketSeriesData extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger getDocketSeriesDataLogger = Logger.getLogger("GetDocketSeriesData");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDocketSeriesData() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		getDocketSeriesDataLogger
				.info("*******************************START*********************************");
		String urlString = FilesUtil.getProperty("docketSeriesMaster");
		URL obj = new URL(urlString);
		Gson gson = new GsonBuilder().serializeNulls().create();
		BufferedReader inData = null;
		StringBuilder response1 = new StringBuilder();
		ManageTransaction manageTransaction = null;
		HttpURLConnection con = null;
		String result = "";
		
		try {
			getDocketSeriesDataLogger.info("urlString : "+ urlString);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			con.setConnectTimeout(5000);
			con.setReadTimeout(60000);// erp is taking more than 10secs so
										// changed to 60 secs
			getDocketSeriesDataLogger.info("Response Code : "
					+ con.getResponseCode());
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inData = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				while ((inputLine = inData.readLine()) != null) {
					response1.append(inputLine);
				}
				inData.close();
				String receivedData = null;
				receivedData = response1.toString();
				/*getDocketSeriesDataLogger.info("DataReceived from ERP\n"
						+ receivedData);*/
				getDocketSeriesDataLogger
						.info("/*/*/*/*/**/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*Data Received/*/*/*/*/*/*/*/*/*/*/**/*/*/*/*/*/*/*/*");
				Type docketSeriesType = new TypeToken<ArrayList<DocketSeriesModel>>() {
				}.getType();
				List<DocketSeriesModel> docketSeriesList = gson.fromJson(
						receivedData, docketSeriesType);
				getDocketSeriesDataLogger.info("Total records received :"
						+ docketSeriesList.size());
				Map<Integer, String> userNamesMap = new HashMap<Integer, String>();
				if (docketSeriesList != null && docketSeriesList.size() > 0) {
					manageTransaction = new ManageTransaction();
					String query = "Select D.docketKey,D.userName from DocketSeriesMaster D";
					TypedQuery<Object[]> typedQuery = manageTransaction.createQuery(
							Object[].class, query);
                    List<Object[]> dbDataList = typedQuery.getResultList();
                    List<String> checkList=new ArrayList<String>();
                    for(Object[] o:dbDataList){
                    	String s=Integer.toString((int) o[0])+(String) o[1];
                    	checkList.add(s);
                    }
					for(DocketSeriesModel object:docketSeriesList){
					//	getDocketSeriesDataLogger.info("Docket Key and Username Concat "+object.getDocketKey()+object.getUserName()+" exists "+checkList.contains(object.getDocketKey()+object.getUserName()));
						if(!(checkList.contains((object.getDocketKey()+object.getUserName())))){
							DocketSeriesMaster master = new DocketSeriesMaster();
							master.setDktUserKey(object.getDocketKey()+"-"+object.getUserName()+"-"+object.getToSeries());
							master.setBranchCode(object.getBranchCode());
							master.setDocketKey(object.getDocketKey());
							master.setFromSeries(object.getFromSeries());
							master.setLastSrNumber(object.getLastSrNumber());
							master.setToSeries(object.getToSeries());
							master.setTotalLeaf(object.getTotalLeaf());
							master.setUserId(object.getUserId());
							master.setUserName(object.getUserName());
							master.setVendorCode(object.getVendorCode());
							master.setCreatedTimestamp(new Date());
							master.setProductType(object.getProductType());
							userNamesMap.put(object.getUserId(),object.getUserName());
							manageTransaction.persist(master);
							manageTransaction.commit();
						}
					}
					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.SUCCESSFUL,null));
					try {	
					if(userNamesMap.values() != null && userNamesMap.size() > 0)
						{
						Map <String,String> userTokenMap = SendMessage.getPushTokens();
						List <String> pushListTokens = new ArrayList <String>();
						
						if(userTokenMap != null && userTokenMap.size() != 0)
						{
						for(String user : userNamesMap.values())
						{
							if(userTokenMap.containsKey(user))
							{
								pushListTokens.add(userTokenMap.get(user));
							}
						}
						//push method
					
						/*String txtMessage = "PULL_DOCKET_SERIES";
						  List<GcmPushResponse> pushList=new ArrayList<GcmPushResponse>();
						GcmPushResponse pushResponse = new GcmPushResponse();
						pushResponse.setId(1);
						pushResponse.setMessage(txtMessage);
						pushList.add(pushResponse);
						String message = gson.toJson(pushList);
						getDocketSeriesDataLogger.info("GCM message : "+message);
						//System.out.println("GCM message : "+message);
						 boolean pushReturn =  SendMessage.ANDROID_sendMessage(pushListTokens, message);*/
						if(pushListTokens!=null&&pushListTokens.size()>0) {
						 PushDataModel pd=new PushDataModel();
	    	     			pd.setId(1);
	    	     	      
	    	     	      boolean pushReturn = CommonTasks.sendPushNotification("PULL_DOCKET_SERIES", "PULL_DOCKET_SERIES", pushListTokens , getDocketSeriesDataLogger, gson, pd);
	    	     	     getDocketSeriesDataLogger.info("pushReturn : "+pushReturn);
					        if(pushReturn == true)
					        {
					        	getDocketSeriesDataLogger.info(" pushSent Successfully ");
					        	
					        }
					        else{
					        	getDocketSeriesDataLogger.info(Constants.GCM_FAILURE);
					        	
					        }
						}
						}
						else{
							getDocketSeriesDataLogger.info("No Push Tokens Found");
							
						}
						}
						else{
							
							getDocketSeriesDataLogger.info("No New Users Found");
						}
					}catch(Exception e) {
						e.printStackTrace();
						getDocketSeriesDataLogger.error("--------------Exception while sending push ------------- :"
								, e);
					}
				}
			else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Data Received From ERP",null));
				
			}
			} else {
				getDocketSeriesDataLogger
						.info("ERP failed to respond..Response Code : "
								+ con.getResponseCode());
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						"Response Failed From ERP", null));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
			getDocketSeriesDataLogger
					.error("--------------Exception in server------------- :"
							, e);
			
		} finally {
			if (manageTransaction != null) {
				manageTransaction.close();
			}
			if (con != null) {
				con.disconnect();
			}
		}
		getDocketSeriesDataLogger.info("result "+result);
		response.getWriter().write(result);
		getDocketSeriesDataLogger.info("\n\n**********************************************END***************************************");

	}

}
