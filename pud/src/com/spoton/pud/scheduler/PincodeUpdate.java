package com.spoton.pud.scheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PinCodeSpotonResponse;
import com.spoton.pud.data.Pincode;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.jpa.PinCodeMaster;
import com.spoton.pud.jpa.PinCodeMasterPK;

import java.lang.reflect.Type;

/**
 * Servlet implementation class PincodeUpdate
 */
@WebServlet("/pincodeupdate")
public class PincodeUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Logger pudPincodeMaster = Logger.getLogger("PudPincodeMaster");
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PincodeUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		pudPincodeMaster.info("**********START**************");
		String urlString = FilesUtil.getProperty("pincodeupdateurl");
		pudPincodeMaster.info("Erp Url "+urlString);
		URL obj = new URL(urlString);

		Gson gson = new GsonBuilder().serializeNulls().create();
        String result="";
		BufferedReader in = null;
		StringBuffer response1 = new StringBuffer();
		ManageTransaction mt = new ManageTransaction();
		int inactive = 0;
		int changedpins = 0;
		int newpins = 0;
		
		try{
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
			/*con.setConnectTimeout(5000);
			con.setReadTimeout(60000);*/
			int responseCode = con.getResponseCode();
			pudPincodeMaster.info("Response Code : " + responseCode);
	
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	      //System.out.println("response1 "+response1);
			while ((inputLine = in.readLine()) != null) {
				response1.append(inputLine);
			}
			in.close();
//			pudPincodeMaster.info("Length of Response From Server received : " + response1.toString().length());
			Type pinCodeListType = new TypeToken<ArrayList<PinCodeSpotonResponse>>(){}.getType();
			List<PinCodeSpotonResponse> pinCodes =  gson.fromJson(response1.toString(),	pinCodeListType);
			pudPincodeMaster.info("response size :: "+pinCodes.size());
			Map<String, PinCodeSpotonResponse> pins = new HashMap<String, PinCodeSpotonResponse>();
			for (PinCodeSpotonResponse pin:pinCodes){
				pins.put(pin.getPinCode() + "-" + pin.getBranchCode().trim() + "-" + pin.getpType().trim() + "-"
						+ pin.getAccountCode(), pin);
			}
			
			/*pudPincodeMaster.info("Size of parsed erp response data object " + pinCodes.size());*/
			pudPincodeMaster.info("Size of data after filtering duplicates " + pins.size());
			int version = getCurrentVersion(mt) + 1;
			pudPincodeMaster.info("New Version " + version);
			int rowNumber=getMaxRowNumber(mt);
			//old code
			//22092020
			Map<String, PinCodeMaster> oldpinMap = getOldPinCodes(mt);
			pudPincodeMaster.info("old pin codes size: " + oldpinMap.size());
			//**
			try{
				//22092020
//				String query="truncate pin_code_master";
//				Query qry = mt.createNativeQuery(query);
//				mt.begin();
//				qry.executeUpdate();
//				pudPincodeMaster.info("Data Truncated");
//				for(PinCodeSpotonResponse pin: pins.values()){
//					PinCodeMasterPK primaryKey=new PinCodeMasterPK();
//					primaryKey.setBranchCode(pin.getBranchCode());
//					primaryKey.setPinCode(pin.getPinCode());
//					primaryKey.setPincodeType(pin.getpType());
//					primaryKey.setAccountCode(pin.getAccountCode());
//					PinCodeMaster pinCode = new PinCodeMaster();
//					pinCode.setId(primaryKey);
//					pinCode.setActive(pin.isActive()?1:0);
//					pinCode.setActiveStatus(pin.getActiveStatus());
//					pinCode.setCreatedTime(new Date());
//					pinCode.setLocationName(pin.getLocationName());
//					pinCode.setServicable(pin.getServicable());
//					pinCode.setServiceType(pin.getServiceType());
//					pinCode.setUpdatedTime(new Date());
//					pinCode.setVersion(version);
//					pinCode.setIsAirServicable(pin.getIsAirServiceable());
//					pinCode.setIsAirServicableDel(pin.getIsAirServiceableDel());
//					pinCode.setAreaId(pin.getAreaId());
//					//** 23-07-2020 - akshay
//					pinCode.setFlag(pin.getFlag());
//					pinCode.setPickupAllowed(pin.getPickupAllowed() ? 1 : 0);
//					pinCode.setDeliveryAllowed(pin.getDeliveryAllowed() ? 1 : 0);
//					//**
//					mt.persist(pinCode);
//					
//				}
//				mt.commit();
				//**
				//old code
				String okey = "";
				for(PinCodeMaster p: oldpinMap.values()){
					okey = p.getId().getPinCode() + "-" + p.getId().getBranchCode().trim() + "-" + p.getId().getPincodeType().trim() + "-"
							+ p.getId().getAccountCode();
					if(!pins.containsKey(okey)) {
						inactive += 1;
						p.setActive(0);
						p.setVersion(version);
						p.setUpdatedTime(new Date());
						mt.persist(p);
					}
				}
//				mt.commit();
				pudPincodeMaster.info("Inactive ::"+inactive);
			for(PinCodeSpotonResponse pin: pins.values()){
				String key = pin.getPinCode() + "-" + pin.getBranchCode().trim() + "-" + pin.getpType().trim() + "-"
						+ pin.getAccountCode();
				PinCodeMasterPK primaryKey=new PinCodeMasterPK();
				primaryKey.setBranchCode(pin.getBranchCode());
				primaryKey.setPinCode(pin.getPinCode());
				primaryKey.setPincodeType(pin.getpType());
				primaryKey.setAccountCode(pin.getAccountCode());
					if (!oldpinMap.containsKey(key)){
						newpins += 1;
						
						PinCodeMaster pinCode = new PinCodeMaster();
						rowNumber+=1;
						pinCode.setId(primaryKey);
						pinCode.setActive(pin.isActive()?1:0);
						pinCode.setActiveStatus(pin.getActiveStatus());
						pinCode.setCreatedTime(new Date());
						pinCode.setLocationName(pin.getLocationName());
						pinCode.setServicable(pin.getServicable());
						pinCode.setServiceType(pin.getServiceType());
						pinCode.setUpdatedTime(new Date());
						pinCode.setVersion(version);
						pinCode.setIsAirServicable(pin.getIsAirServiceable());
						pinCode.setIsAirServicableDel(pin.getIsAirServiceableDel());
						pinCode.setAreaId(pin.getAreaId());
						pinCode.setFlag(pin.getFlag());
						pinCode.setPickupAllowed(pin.getPickupAllowed() ? 1 : 0);
						pinCode.setDeliveryAllowed(pin.getDeliveryAllowed() ? 1 : 0);
						pinCode.setRegion(pin.getRegion());
						pinCode.setDepot(pin.getDepot());
						pinCode.setArea(pin.getArea());
						pinCode.setRowNumber(rowNumber);
						mt.persist(pinCode);
					}else{
					
						PinCodeMaster oldPinCode = oldpinMap.get(key);
						if (pin.changed(oldPinCode)){
							changedpins += 1;
							//oldPinCode.setId(primaryKey);
							oldPinCode.setActive(pin.isActive()?1:0);
							oldPinCode.setActiveStatus(pin.getActiveStatus());
							oldPinCode.setLocationName(pin.getLocationName());
							oldPinCode.setServicable(pin.getServicable());
							oldPinCode.setServiceType(pin.getServiceType());
							oldPinCode.setIsAirServicable(pin.getIsAirServiceable());
							oldPinCode.setIsAirServicableDel(pin.getIsAirServiceableDel());
							oldPinCode.setUpdatedTime(new Date());
							oldPinCode.setVersion(version);
							oldPinCode.setAreaId(pin.getAreaId());
							oldPinCode.setFlag(pin.getFlag());
							oldPinCode.setPickupAllowed(pin.getPickupAllowed() ? 1 : 0);
							oldPinCode.setDeliveryAllowed(pin.getDeliveryAllowed() ? 1 : 0);
							oldPinCode.setRegion(pin.getRegion());
							oldPinCode.setDepot(pin.getDepot());
							oldPinCode.setArea(pin.getArea());
							mt.persist(oldPinCode);
						}
					}
			}
			pudPincodeMaster.info("Commiting");
			mt.commit();
			pudPincodeMaster.info("Committed");
			
			pudPincodeMaster.info("new :: "+newpins+" changed::"+changedpins);
			
			List <String> pushListTokens = CommonTasks.getpushTokens(mt);
			 Integer isUpdateMandatory=0;
			if(pushListTokens!=null&&pushListTokens.size()>0) {
				 PushDataModel pd=new PushDataModel();
				 pd.setId(20);
	     	      pd.setMasterType(1);
	     	      String updateQuery="SELECT is_update_mandatory FROM masters_update where master_type_id=1";
	     	     Query q=mt.createNativeQuery(updateQuery);
	     	     try {
	     	      isUpdateMandatory=(Integer) q.getSingleResult();
	     	     }catch(NoResultException nr) {
	     	    	 nr.printStackTrace();
	     	     }
	     	     pd.setUpdateMandatory(isUpdateMandatory==1?true:false);
	     	      boolean pushReturn = CommonTasks.sendPushNotification("Pincode_Update", "Pincode_Update", pushListTokens , pudPincodeMaster, gson, pd);
	     	     pudPincodeMaster.info("pushReturn : "+pushReturn);
			        if(pushReturn == true)
			        {
			        	pudPincodeMaster.info(" pushSent Successfully ");
			        	
			        }
			        else{
			        	pudPincodeMaster.info(Constants.GCM_FAILURE);
			        	
			        }
				}
			
			
			result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,null));
			}catch(Exception e){
				e.printStackTrace();
				pudPincodeMaster.error("Exception in loop ",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
				
			}
			
			
		}catch(Exception e){
			pudPincodeMaster.error("Exception in main: " , e);
			e.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			mt.close();
		}
		response.getWriter().write(result);
		pudPincodeMaster.info("Result "+result);
		pudPincodeMaster.info("*****************END***************");
	}
	
	private Map<String, PinCodeMaster> getOldPinCodes(ManageTransaction mt){
//		pudPincodeMaster.info("getting old pins ");
		Map<String, PinCodeMaster> pinMap = new HashMap<String, PinCodeMaster>();
		String qry = "Select pcm from PinCodeMaster pcm ";
		TypedQuery<PinCodeMaster> pcmQuery = mt.createQuery(PinCodeMaster.class, qry);
		try{
			List<PinCodeMaster> pinCodes = pcmQuery.getResultList();
			for (PinCodeMaster pincode:pinCodes){
				String key=pincode.getId().getPinCode()+"-"+pincode.getId().getBranchCode().toUpperCase().trim()+"-"+pincode.getId().getPincodeType().toUpperCase().trim()+"-"+pincode.getId().getAccountCode();
				pinMap.put(key,pincode);
			}
		}catch(Exception e){
			pudPincodeMaster.error("Exception in getting old pins " , e);
		}
//		pudPincodeMaster.info("getting old pins done ");
		return pinMap;
	}

	private int getCurrentVersion(ManageTransaction mt) {
		// TODO Auto-generated method stub
		Integer version = 0;
		String qry = "select max(version) from pin_code_master";
//		pudPincodeMaster.info("Checkig version");
		Query query = mt.createNativeQuery(qry);
		try{
			version = (Integer) query.getSingleResult();
		}catch(Exception e){
			pudPincodeMaster.error("Exception in pin version retrieval " , e);
		}
//		pudPincodeMaster.info("Checkig version done: " + version);

		return version == null?0:version;
	}
	
	private int getMaxRowNumber(ManageTransaction mt) {
		// TODO Auto-generated method stub
		Integer rowNumber=0;
		String qry = "select max(row_num) from pin_code_master";
//		pudPincodeMaster.info("Checkig version");
		Query query = mt.createNativeQuery(qry);
		try{
			rowNumber = (Integer) query.getSingleResult();
		}catch(Exception e){
			pudPincodeMaster.error("Exception in pin row number  retrieval " , e);
		}
//		pudPincodeMaster.info("Checkig version done: " + version);

		return rowNumber == null?0:rowNumber;
	}
	
}
