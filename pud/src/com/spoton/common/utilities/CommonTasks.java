package com.spoton.common.utilities;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsData;
import com.spoton.pud.data.FCMNotificationContentModel;
import com.spoton.pud.data.FCMPushResponse;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.GoogleResponse;
import com.spoton.pud.data.Mailer;
import com.spoton.pud.data.PickupUpdationData;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.data.PushNotificationModel;
import com.spoton.pud.jpa.AuditLog;
import com.spoton.pud.jpa.MailedPickupCon;
import com.spoton.pud.jpa.PushLoginUsersDataEntity;
import com.spoton.pud.jpa.RtmsAuditLog;
import com.spoton.pud.jpa.UserSessionData;

public class CommonTasks {

	public static boolean check(String... args) {
		boolean result = true;

		for (String arg : args) {
			if (arg == null || arg.trim().length() == 0) {
				result = false;
				break;
			}
		}
		return result;
	}
	@SuppressWarnings("deprecation")
	public static String toBase64(String pic) {

		String extension = FilesUtil.getProperty("ImageExtension");
		String path = FilesUtil.getProperty("ImagePath");

		// path = new File(".").getCanonicalPath() +"/" + path;

		//System.out.println("NEW PATH: " + path);
		File fileImg = new File(path);
		fileImg.mkdirs();
		//
		pic = URLDecoder.decode(pic);

		byte[] imgBytes = org.apache.commons.codec.binary.Base64
				.decodeBase64(pic);
		String fileName = (new SimpleDateFormat("yyyyMMddhhmmssSSSS").format(new Date())
				+ "." + extension).trim();

		fileImg = null;
		fileImg = new File(path + fileName);

		if (fileImg.exists()) {
			fileImg = null;
			fileImg = new File(path + fileName);
		}

		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(fileImg);
			fOut.write(imgBytes);
			fOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//System.out.println("Output file saved: " + fileImg.getAbsolutePath());
		return fileName;
	}
	
	public static boolean saveRtmsAuditLog(String inputParameters,String loggedInEmpBranch,String loggedInEmpCode,String outputMessage,String serviceType) {
		ManageTransaction mt=new ManageTransaction();
		try{
		RtmsAuditLog rl=new RtmsAuditLog();
		rl.setInputParameters(inputParameters);
		rl.setLoggedInEmpBranch(loggedInEmpBranch);
		rl.setLoggedInEmpCode(loggedInEmpCode);
		rl.setOutputMessage(outputMessage);
		rl.setServiceType(serviceType);
		rl.setTimestamp(new Date());
		mt.persist(rl);
		mt.commit();
		return true;
		}catch (Exception e){
		e.printStackTrace();
		}finally {
			mt.close();
		}
		return false;
		
	}
	
	

	
	@SuppressWarnings("deprecation")
	public static String toImgFromBase64(String pic,String conNumber,String extension,int serviceId,String imageType) throws IOException {
		Calendar c=Calendar.getInstance();
	    c.setTime(new Date());
	   /* Logger imageLogger = Logger.getLogger("ImagesLogger");
	    imageLogger.info("****START****");
	    imageLogger.info("conNumber:- "+conNumber+" extension:- "+extension+" serviceId:- "+serviceId+" imageType:- "+imageType);*/
		String path = FilesUtil.getProperty("ImagePath");
		//imageLogger.info("ImagePath "+path);
		//System.out.println("path "+path);
		String imgPath="";
		if(serviceId==2){
			imgPath="Deliveries/"+Integer.toString(c.get(Calendar.YEAR))+"/"+Integer.toString(c.getTime().getMonth()+1)+"/"+Integer.toString(c.getTime().getDate())+"/";
			path=path+imgPath;
		    
		}else if(serviceId==1){
			imgPath="Pickups/"+Integer.toString(c.get(Calendar.YEAR))+"/"+Integer.toString(c.getTime().getMonth()+1)+"/"+Integer.toString(c.getTime().getDate())+"/";
			path=path+imgPath;
		}
		//imageLogger.info("imgPath "+imgPath);
		
		//imageLogger.info("Final path "+path);
		
		//File fileImg = new File(path);
		/*imageLogger.info("fileImg "+fileImg);
		imageLogger.info("fileImg.exists() "+fileImg.exists());*/
		/*if(!fileImg.exists())
		fileImg.mkdirs();*/
		//System.out.println("fileImg "+fileImg);
		//
		

		byte[] imgBytes = org.apache.commons.codec.binary.Base64.decodeBase64(pic);
		//imageLogger.info("imgBytes "+imgBytes);
		//String fileName = (new SimpleDateFormat(conNumber+"_yyyyMMddhhmmssSSSS").format(new Date()) + "." + extension).trim();
		
		String fileName = (conNumber+"_"+imageType+"." + extension).trim();
		//System.out.println("fileName "+fileName);
		//imageLogger.info("fileName "+fileName);
		//fileImg = null;
		File fileImg = new File(path + fileName);
		
		/*imageLogger.info(" Final fileImg "+fileImg);
		imageLogger.info("Final fileImg.exists() "+fileImg.exists());*/
		
        
		FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(fileImg);
			fOut.write(imgBytes);
			//fOut.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//imageLogger.info(" Exception in image "+e);
		}finally {
			if(fOut!=null) {
			fOut.close();
			}
		}

		//System.out.println("Output file saved: " + fileImg.getAbsolutePath());
	   /* imageLogger.info("Output file saved: " + fileImg.getAbsolutePath());
	    imageLogger.info("****END****");*/
		return imgPath + fileName;
	}
	
	
	public static List<String> getpushTokens(ManageTransaction manageTransaction){
		try {
			String pushTokensQuery="select pushToken from device_info where active_flag=1";
			Query q=manageTransaction.createNativeQuery(pushTokensQuery);
			List<String> pushTokensList=q.getResultList();
			return pushTokensList;
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return null;
	}
	
	/*private final static ObjectMapper objectMapper = new ObjectMapper();

	public static String getJsonStringFromObject1(Object obj)
			throws ExceptionUtil {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonGenerationException e) {
			throw new ExceptionUtil(e.toString());
		} catch (JsonMappingException e) {
			throw new ExceptionUtil(e.toString());
		} catch (IOException e) {
			throw new ExceptionUtil(e.toString());
		}
	}

	public static Object parseResponse(String jsonStr, Class<?> classOfT)
			throws ExceptionUtil {
		try {
			return objectMapper.readValue(jsonStr, classOfT);
		} catch (JsonParseException e) {
			throw new ExceptionUtil("Error Parsing data",
					e.toString()+"Data may not be valid : " + classOfT != null ? classOfT
							.getSimpleName() : "class is null");
		} catch (Exception e) {
			throw new ExceptionUtil(e.toString());
		}
	}*/
	public static String getBase64(String imageName) {
		File file = new File(FilesUtil.getProperty("ImagePath") + imageName);
		String imageDataString = "";
		try{
			// Reading a Image file from file system
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            // Converting Image byte array into Base64 String
            imageDataString = Base64.encodeBase64String(imageData);
            imageInFile.close();
            System.out.println("Image Successfully Manipulated!");
        } catch (FileNotFoundException e) {
            System.out.println("Image not found" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
		return imageDataString;
	}
	
	
	
	
	
	public static boolean saveAuditLog(String deviceIMEI, String apkVersion,String serviceType, Collection<String> conNumberOrawbNumber, String userId, String pickupScheduleIdOrpdcNumber, String ipadd,String resultMessage,String inputParameters) {
		Logger auditLogger = Logger.getLogger("AuditLogs");
		ManageTransaction manageTransaction=new ManageTransaction();
		try{
			   
			auditLogger.info("*********START**********");
			String versionQuery="select id,apk_version from user_session_data where user_name=?1 and active_flag=1 and imei=?2";
			Query q=manageTransaction.createNativeQuery(versionQuery).setParameter(1, userId).setParameter(2, deviceIMEI);
			try{
			@SuppressWarnings("unchecked")
			List<Object[]> list= q.getResultList();
			if(list!=null&&list.size()>0){
				for(Object[] o:list) {
				auditLogger.info("User Current Version "+(String) o[1]);
				if(Float.parseFloat((String) o[1])<Float.parseFloat(apkVersion)){
					
					UserSessionData u=manageTransaction.find(UserSessionData.class, (int) o[0]);
					u.setApkVersion(apkVersion);
					manageTransaction.persist(u);
					manageTransaction.commit();
				}
				}
			}
			}catch(Exception e){
				e.printStackTrace();
				auditLogger.info("Exception in updating version "+e);
			}
			
			AuditLog audit = new AuditLog();
			
			audit.setDeviceImei(deviceIMEI);
			audit.setApkVersion(apkVersion);
			audit.setServiceType(serviceType);
			audit.setTimestamp(new Date());
			audit.setResultMessage(resultMessage);
			auditLogger.info("1.deviceIMEI:- "+deviceIMEI+" 2.apkVersion:- "+apkVersion+" 3.serviceType:- "+serviceType+" 4.conNumberOrawbNumber:- "+conNumberOrawbNumber+" 5.userId:- "+userId +" 6.pickupScheduleIdOrpdcNumber:- "+pickupScheduleIdOrpdcNumber+" 7.ipadd:- "+ipadd+" 8.resultMessage:- "+resultMessage+" 9.inputParameters "+inputParameters);
			auditLogger.info("Timestamp:- "+audit.getTimestamp());
			if (check(ipadd)){
				audit.setIpadd(ipadd);
			}
			if(check(userId)){
				audit.setUserId(userId);
			}
			if(pickupScheduleIdOrpdcNumber!=null){
				audit.setPickupScheduleId_pdcNumber(pickupScheduleIdOrpdcNumber.toString());
			}
			if(conNumberOrawbNumber!=null&&conNumberOrawbNumber.size()>0){
			audit.setConNumber_awbNumber(conNumberOrawbNumber.toString());
			}
			if(check(inputParameters)){
				audit.setInputParameters(inputParameters);
			}
			manageTransaction.saveObject(audit);
			auditLogger.info("Audit Log Saved Successfully");
			auditLogger.info("*********END**********");
			return Constants.TRUE;
		} catch (Exception e){
			e.printStackTrace();
			auditLogger.info("Exception in saving audit log "+e);
			auditLogger.info("*********END**********");
			return Constants.FALSE;
			
		}finally{
			manageTransaction.close();
		}
		
	}
	
	
	public static boolean checkLatestApkVersion(Integer apkVersion){
		ManageTransaction manageTransaction=null;
		try{
			manageTransaction=new ManageTransaction();
		String query="select max(version) from device_apk_versions";
		Query q=manageTransaction.createNativeQuery(query);
		
		Object o=q.getSingleResult();
	
		if(apkVersion<(int) o){
			return Constants.FALSE;
		}else{
		return Constants.TRUE;
		}
		}catch(Exception e){
			e.printStackTrace();
			return Constants.FALSE;
		}finally{
			if(manageTransaction!=null)
			manageTransaction.close();
		}
	}
	
	public static GoogleResponse convertToLatLong(String fullAddress,Logger logger) throws IOException {
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{
		 String URL = "https://maps.googleapis.com/maps/api/geocode/json";
		  /*
		   * Create an java.net.URL object by passing the request URL in
		   * constructor. Here you can see I am converting the fullAddress String
		   * in UTF-8 format. You will get Exception if you don't convert your
		   * address in UTF-8 format. Perhaps google loves UTF-8 format. :) In
		   * parameter we also need to pass "sensor" parameter. sensor (required
		   * parameter) � Indicates whether or not the geocoding request comes
		   * from a device with a location sensor. This value must be either true
		   * or false.
		   */
		/*  URL url = new URL(URL + "?address="
		    + URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false&key=AIzaSyB-ygaQWSNOj4jI2Xns8-W4LA5L5tNda3M");AIzaSyDc0s9ta3ylw9D3EJMgUHciobnjLPf5Y44*/
		  URL url = new URL(URL + "?address="
				    + URLEncoder.encode(fullAddress, "UTF-8") + "&sensor=false&key=AIzaSyDCqU7Sw_Ks1bjGSSoRMT1FrLjHum3PRLY");
		  logger.info("Google URL with  address  "+url);
		  // Open the Connection
		  URLConnection conn = url.openConnection();
		  conn.setConnectTimeout(10000);
		  conn.setReadTimeout(10000);
		 /* InputStream in = conn.getInputStream() ;
		  ObjectMapper mapper = new ObjectMapper();
		  GoogleResponse response = (GoogleResponse)mapper.readValue(in,GoogleResponse.class);
		  in.close();*/
		  
		  String response1 = null;StringBuilder stringBuilder = new StringBuilder();
		  InputStreamReader streamReader = new InputStreamReader(conn.getInputStream());
          BufferedReader bufferedReader = new BufferedReader(streamReader);
		  while ((response1 = bufferedReader.readLine()) != null) {
              stringBuilder.append(response1);
          }
		  GoogleResponse response=gson.fromJson(stringBuilder.toString(),GoogleResponse.class);
		  
		  return response;
		  
		}catch(Exception e){
			logger.info("Exception in converting address to lat/lng "+e);
			return null;
		}
		
		 }
	
	public static GoogleResponse convertLatLongToAddress(Double latitude,Double longitude,Logger logger) throws IOException {
		Gson gson = new GsonBuilder().serializeNulls().create();
		try{
		 String URL = "https://maps.googleapis.com/maps/api/geocode/json";
		  /*
		   * Create an java.net.URL object by passing the request URL in
		   * constructor. Here you can see I am converting the fullAddress String
		   * in UTF-8 format. You will get Exception if you don't convert your
		   * address in UTF-8 format. Perhaps google loves UTF-8 format. :) In
		   * parameter we also need to pass "sensor" parameter. sensor (required
		   * parameter) � Indicates whether or not the geocoding request comes
		   * from a device with a location sensor. This value must be either true
		   * or false.
		   */
		  URL url = new URL(URL + "?latlng="
		    +latitude+","+longitude  + "&sensor=false&key=AIzaSyDCqU7Sw_Ks1bjGSSoRMT1FrLjHum3PRLY");
		
		  logger.info("Google URL with  lat/long "+url);
		  // Open the Connection
		  URLConnection conn = url.openConnection();

		 /* InputStream in = conn.getInputStream() ;
		  ObjectMapper mapper = new ObjectMapper();
		  GoogleResponse response = (GoogleResponse)mapper.readValue(in,GoogleResponse.class);
		  in.close();*/
		  
		  String response1 = null;StringBuilder stringBuilder = new StringBuilder();
		  InputStreamReader streamReader = new InputStreamReader(conn.getInputStream());
          BufferedReader bufferedReader = new BufferedReader(streamReader);
		  while ((response1 = bufferedReader.readLine()) != null) {
              stringBuilder.append(response1);
          }
		  GoogleResponse response=gson.fromJson(stringBuilder.toString(),GoogleResponse.class);
		  
		  return response;
		  
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception in converting  lat/lng to address ",e);
			return null;
		}
		
		 }
	
	
	public static int getLastRouteVersion(ManageTransaction mt,
			String userName, Logger logger2) {
		// TODO Auto-generated method stub
		try{
		Calendar todaysDate=Calendar.getInstance();
		todaysDate.setTime(new Date());
		String query="Select max(c.routeVersion) from CustomerOptimalPath c where c.username=?1 "
				+ "and FUNC('DAY', c.createdTimestamp)=?2 and FUNC('MONTH', c.createdTimestamp)=?3 and FUNC('YEAR', c.createdTimestamp)=?4 ";
		TypedQuery<Integer> tq2=mt.createQuery(Integer.class, query).setParameter(1, userName);
		tq2.setParameter(2, todaysDate.getTime().getDate());tq2.setParameter(3, todaysDate.getTime().getMonth()+1);tq2.setParameter(4, todaysDate.get(Calendar.YEAR));	
		
		Integer currentRouteVersion=tq2.getSingleResult();
		logger2.info("currentRouteVersion "+currentRouteVersion);
		return currentRouteVersion;
		}catch(NoResultException e){
			logger2.info("No Route found "+e);
			return 0;
		}catch(Exception e){
			logger2.info("Exception in getting current route version "+e);
			return 0;
		}
	}
	
	public static void sendEmailForFailedPickups(PickupUpdationData pickupUpdationData,ManageTransaction mt,String result,Gson gson,Logger pickupUpdationLogger) {
		PickupUpdationData newPickupUpdationData=null;
		try{
	String q="select con_entry_id FROM pud.mailed_pickup_cons";
		Query qu=mt.createNativeQuery(q);
		@SuppressWarnings("unchecked")
		List<Object> mailedConsList=qu.getResultList();
		if(mailedConsList!=null&&mailedConsList.size()>0){
		List<ConDetailsData> newConData=new ArrayList<ConDetailsData>();
		for(int i=0;i<pickupUpdationData.getConDetails().size();i++){
			if(!mailedConsList.contains((pickupUpdationData.getConDetails().get(i).getConEntryID()))){
				newConData.add(pickupUpdationData.getConDetails().get(i));
			}
			
		}
		if(newConData!=null&&newConData.size()>0){
			newPickupUpdationData=new PickupUpdationData();
			newPickupUpdationData.setConDetails(newConData);
		}
		}else{
			newPickupUpdationData=pickupUpdationData;
		}
		
		if(newPickupUpdationData!=null){
			String mailMessage="Hi Everyone,\n\n ERP failed to sent response to the following Pickup Updation data.Please check and recycle the ERP server if needed to receive this data again.\n\n "+gson.toJson(newPickupUpdationData)+"\n\n Thanks&Regards,\n Apptmyz.";
			boolean mailResult= Mailer.send(FilesUtil.getProperty("mailusername"),FilesUtil.getProperty("mailpassword"),"suman.kumar@spoton.co.in","ERP Response Failed For Pickup Updation",mailMessage,pickupUpdationLogger);
		if(mailResult){
	
			for(int i=0;i<newPickupUpdationData.getConDetails().size();i++){
				MailedPickupCon mp=new MailedPickupCon();
				mp.setConEntryId(newPickupUpdationData.getConDetails().get(i).getConEntryID());
				mt.persist(mp);
				
			}
			mt.commit();
			result = gson.toJson(new GeneralResponse(Constants.TRUE,"ERP timed out..Mail Sent Successfully",null));
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..Failed To send Mail",null));
		}
		}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..No New Data found To send Mail",null));
		}
		
		}catch(Exception e1){
			e1.printStackTrace();
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					"ERP timed out..Exception in sending mail",null));
			pickupUpdationLogger.info("ERP timed out..Exception in sending mail "+e1);
			}
	
		
	}
	
	
	public static boolean sendPushNotification(String title, String body, List<String> pushtokens,Logger logger, Gson gson, PushDataModel type){
		boolean result = false;
		try{
			logger.info("**********sending push**********");
			URL url = new URL(Constants.FCM_API_URL);
            
			PushNotificationModel push = new PushNotificationModel();
			push.setRegistration_ids(pushtokens);
			push.setData(type);
			FCMNotificationContentModel content = new FCMNotificationContentModel();
			content.setBody(body);
			content.setTitle(title);
			push.setNotification(content);
		
			
			if(type.getId()==20) {
			logger.info("Title " + title+" body "+body);
			}else {
				logger.info("Notification sent " + gson.toJson(push));
			}
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + Constants.FCM_SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(gson.toJson(push));
			wr.flush();
			wr.close();
            if(conn.getResponseCode() == 200)
            	result = true;
            else
            	result = false;
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            
			logger.info("Push response :" + response);  
			FCMPushResponse pushResponse=gson.fromJson(response.toString(), FCMPushResponse.class);
			
			if(pushResponse.getSuccess()==1) {
				result = true;
			}else {
				result = false;
			}
			
			conn.disconnect();        
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
			result = false;
		}
		
		return result;
	}
	
	public static String validateSession(String sessionId,ManageTransaction mt,Gson gson,Logger logger) {
		String result=null;
		try {
			String query="Select p from PushLoginUsersDataEntity p where p.sessionId=?1";
			TypedQuery<PushLoginUsersDataEntity> tq=mt.createQuery(PushLoginUsersDataEntity.class, query).setParameter(1, sessionId);
			try {
			PushLoginUsersDataEntity pu=tq.getSingleResult();
			Date currentTime=new Date();
			if(currentTime.after(pu.getSessionEnd())) {
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.SESSION_EXPIRED, null));
				
			}else {
				//extend session to 30 mins
				Calendar sessionEnd=Calendar.getInstance();
				sessionEnd.setTime(pu.getSessionEnd());
				sessionEnd.add(Calendar.MINUTE, 30);
				pu.setSessionEnd(sessionEnd.getTime());
				mt.persist(pu);
				mt.commit();
			}
			}catch(NoResultException e) {
				e.printStackTrace();
				logger.error("Exception in Session Validation ",e);
				result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.SESSION_NOT_FOUND, null));

			}
			
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception in Session Validation ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,"Exception in Session Validation", null));

		}
		logger.info("Validate Session Result "+result);
		return result;
	}
}
