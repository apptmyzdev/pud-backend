package com.spoton.common.utilities;

public class Correct {

	public static final boolean status = true;


	
	
	
	

	/*
	 * This is the basic details of the correct
	 */
	public class CORRECT_REQUEST_COMPLETED {
		public static final int code = 900001;
		public static final String message = "Request Completed......";
	}
	
	
	
	public class CORRECT_SESSION {

		private static final int code = 901000;

		public class CORRECT_SESSION_USER_SESSION_CLEARED {
			public static final int code = CORRECT_SESSION.code + 1;
			public static final String message = "User Session Cleared......";
		}

		public class CORRECT_SESSION_NO_USER_SESSION_AVAILABLE {
			public static final int code = CORRECT_SESSION.code + 2;
			public static final String message = "No User Session availabel......";
		}

	}

	public class CORRECT_DEVICE {

		private static final int code = 902000;

		public class CORRECT_DEVICE_ID {
			public static final int code = CORRECT_DEVICE.code + 1;
			public static final String message = "Correct Device Id......";
		}

	}

	public class CORRECT_DEVICEID_PUSHTOKEN {

		private static final int code = 903000;

		public class CORRECT_DEVICE_ID_PUSHTOKEN_MAPPING {
			public static final int code = CORRECT_DEVICEID_PUSHTOKEN.code + 1;
			public static final String message = "Correct Device Id and push token mapping......";
		}

		public class CORRECT_DEVICE_ID_PUSHTOKEN_UPDATED {
			public static final int code = CORRECT_DEVICEID_PUSHTOKEN.code + 2;
			public static final String message = "Device Id and push token mapping updated......";
		}
	}
	
	public class CORRECT_DATA_UNAVAILABLE {

		private static final int code = 904000;

		public class CORRECT_DEVICEID_UNAVAILABLE_IN_SERVER {
			public static final int code = CORRECT_DATA_UNAVAILABLE.code + 1;
			public static final String message = "No device id in the server......";
		}
		
		public class CORRECT_DATA_UNAVAILABLE_SOME_PUSH_TOKEN_MISSING {
			public static final int code = CORRECT_DATA_UNAVAILABLE.code + 2;
			public static final String message = "Some push tokens are unavailable, they are supported in Android 2.2+......";
		}
	}
	
	
	public class CORRECT_LAST{
		private static final int code = 905000;

		public class CORRECT_LAST_LOCATION_BUT_NO_TOKEN {
			public static final int code = CORRECT_LAST.code + 1;
			public static final String message = "This is last location but instant location is not supported in the device......";
		}
		
		public class CORRECT_NO_LAST_LOCATION_NO_TOKEN {
			public static final int code = CORRECT_LAST.code + 2;
			public static final String message = "No last location available and also instant location is not supported in the device......";
		}
		
		public class CORRECT_NO_LAST_LOCATION_BUT_TOKEN {
			public static final int code = CORRECT_LAST.code + 3;
			public static final String message = "Last location unavailable but sent message to device for instant locaiton, please try after some time again for location......";
		}
	}
	
	
	
	public class CORRECT_MESSAGE{
		private static final int code = 906000;
		
		public class CORRECT_MESSAGE_SENT {
			public static final int code = CORRECT_MESSAGE.code + 1;
			public static final String message = "Message sent";
		}
		
		public class CORRECT_MESSAGE_UPDATED_TIME_READ {
			public static final int code = CORRECT_MESSAGE.code + 2;
			public static final String message = "Message timeread updated";
		}
	}
	

	public class CORRECT_PASSWORD {

		private static final int code = 908000;
		
		public class PASSWORD_UPDATED_SUCCESSFULLY {
			public static final int code = CORRECT_PASSWORD.code + 1;
			public static final String message = "Password Updated......";
		}

		
		public class OLD_PASSWORD_MISMATCH {
			public static final int code = CORRECT_PASSWORD.code + 2;
			public static final String message = "Old password mismatched......";
		}
	}

	

}
