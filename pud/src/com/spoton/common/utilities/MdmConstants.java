package com.spoton.common.utilities;

public class MdmConstants {
	public static String CONSTANTS_LOCK = "LOCK";
	public static String CONSTANTS_SCREAM = "SCREAM";
	public static String CONSTANTS_APP_LIST = "APPS_LIST";
	public static String CONSTANTS_WIPE = "WIPE";
	public static String CONSTANTS_CAMERA = "CAMERA";
	public static String CONSTANTS_CONTACT = "BACKUP_CONTACTS";
	public static String CONSTANTS_SMS = "BACKUP_SMS";
	public static String CONSTANTS_WIFI = "WIFI";
	public static String CONSTANTS_BLUETOOTH = "BLUETOOTH";
	public static String CONSTANTS_STORAGE = "STORAGE";
	public static String CONSTANTS_PUSH = "PUSH";
	public static String CONSTANTS_PUSH_IMAGE="PUSH_IMAGE";
	public static String CONSTANTS_DEVICE_INFO = "DEVICE_INFO";
	public static String CONSTANTS_PUSH_APP_URL = "PUSH_APP_URL";
	public static String CONSTANTS_APPLOCKER_SETTINGS = "APPLOCKER_SETTINGS";
	public static String CONSTANTS_GPS = "GPS";
	public static String CONSTANTS_APP_UNINSTALL = "APP_UNINSTALL";
	public static String CONSTANTS_GET_APPLOCKER_SETTINGS = "GET_APPLOCKER_SETTINGS";
	public static String CONSTANTS_GEO_FENCE_SET = "GEOFENCE_SET";
	public static String CONSTANTS_GEO_FENCE_CLEAR = "GEOFENCE_CLEAR";
	
	
	
	public static String CONSTANTS_PASSWORD = "PASSWORD";
	public static String CONSTANTS_PASSWORD_COMPLEX = "COMPLEX";
	public static String CONSTANTS_PASSWORD_MEDIUM = "MEDIUM";
	public static String CONSTANTS_PASSWORD_WEAK = "WEAK";
	public static String CONSTANTS_PASSWORD_NONE = "NONE";
	public static String CONSTANTS_PASSWORD_RESET = "PSWDRESET";

	public static String CONSTANTS_TRUE = "TRUE";
	public static String CONSTANTS_FALSE = "FALSE";

	public static int CONSTANTS_AJAX_TIMEOUT = 10000;

	public static final String CMD_APPOLOCKER_SETTING = "CMD_APPLOCKER_SETTINGS";

	public final static String CMD_GET_APPLOCKER_SETTINGS = "CMD_GET_APPLOCKER_SETTINGS";
	public final static String CMD_APPLOCKER_RESET_MASTER_PWD = "CMD_APPLOCKER_RESET_MASTER_PWD";

	public static String getAppLockerSettingOptMsg(String json) {
		return MdmConstants.CMD_APPOLOCKER_SETTING + ",AppLocker(" + json + ")";
	}

}