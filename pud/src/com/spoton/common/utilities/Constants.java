package com.spoton.common.utilities;

public class Constants {
	
	public static final String CONTENT_TYPE_JSON = "application/json";
//	public static final String CONTENT_TYPE_CSV = "text/csv";

	public static final String CHARACTER_ENCODING_UTF_8 = "UTF-8";
	
//	public static final String SUCCESSFUL = "successful";
	public static final boolean FAILED = false;
	public static final String ERROR_INCOMPLETE_DATA = "Incomplete data sent to the server";
	public static final String ERRORS_EXCEPTION_IN_SERVER = "Exception in server";
	public final static String ERROR_PARSING_REQUEST_DATA = "Error parsing request data ";
	public static final String ERROR_NO_DATA_AVAILABLE = "No Data Available";
	public static final String ERROR_AUTH_FAILED = "Authentication Failed";
	public static final String ERROR_NO_DATA = "No data available";
	public static final String REQUEST_COMPLETED = "Request Completed";
	public static final boolean TRUE = true;
	public static final boolean FALSE = false;
	public static final String EMPTY_FINAL_SUBMIT = "Final Submit Done with no Scanned Packets";
	public static final String INTERMEDIATE_SUBMIT = "Intermediate Submission Successful";
	public static final String FINAL_SUBMIT_DONE = "Final Submit for this Sheet is already completed";
	public static final String ERROR_INVAILD_SESSION = "Signout/Session Expired";
	public static final String LOGIN_DENIED = "Already logged in another device";
	public static final String UPDATE_AVAILABLE = "New update is available";
	public static final String NO_UPDATES = "No new updates";
	public static final String GCM_FAILURE = "Failed to send push notification";
	public static final String NO_USERS_LOGGED_IN = "There are no users logged in currently";
	public static final String UPDATE_TO_LATEST_VERSION="Please Update to latest Version";
	public static final String EXTRA_PKT = "PER";
	public static final String SHORT_PKT = "PSR";
	public static final String PKT_MISMATCH = "PPM";
	public static final String SESSION_EXPIRED = "Session is Expired.Please Login";
	public static final String SESSION_NOT_FOUND = "Session Not Found.Please Login";
	public static final String SUCCESSFUL = "Success";
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String FCM_API_URL = "https://fcm.googleapis.com/fcm/send";
	public static final String FCM_SERVER_KEY = "AAAAd_8oCt0:APA91bHxXGEVxg6xz6PvUvZ4i3ISVMk9uXATCabJpGOJKPhROdetL75NEmExvVk6dwTj8COCjMc2KQ1V4WQzHVj9xGynnj-rdje2cbZH44Mxm80I7vuOUzJgJsO_jROW0GUK91WeMy1z";
	//public static final String FCM_SERVER_KEY ="AAAAuLuurgM:APA91bEGmy_jS468f8_MLXyYnm80Krvtlv1VAH3EYV5PaOhYhISc-LDBh6WmUEIN61NReL8I_0x8_5PsUtU6Kb2xKCjuW8kolhv6XNO7HEjzcvJkDjjq0wY_K5vtM_4R1v1CTNobKZbo";
	//public static final String FCM_SERVER_KEY ="AAAAQELGguU:APA91bE59osuMjggRUOmatxYe9q6EItx492J0qJCsRjC7jIoWf2aUMyqkIEcfPKhOuexJvBXVHB7YnGO7R_yVVvWS9iWYPtUBQSN4hkAfOB8IZ7QlfaqvNa1hMpe8isiwKpDO_4XV9XhdUdQNjaZhAy4RAYc4BLf9A";

	public static final String GOOGLE_API_KEY = "AIzaSyDCqU7Sw_Ks1bjGSSoRMT1FrLjHum3PRLY";
	
}
