package com.spoton.common.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PudSubscriptionModel;
import com.spoton.pud.jpa.ConsStatus;

public class PostConSubscriptionMessage {
	
	public static GeneralResponse postConSubscriptionMessage(String url,List<PudSubscriptionModel> pudSubscriptionList,Logger logger) {
		Gson gson = new GsonBuilder().serializeNulls().create();
		HttpURLConnection con=null; URL obj =null;
		StringBuilder stringBuilder = null;
		if(pudSubscriptionList!=null&&pudSubscriptionList.size()>0) {
			logger.info("DataSent To  pudSubcriptionCallBack api "+gson.toJson(pudSubscriptionList));
			
			try {
				obj = new URL(url);
				con = (HttpURLConnection) obj.openConnection();
				/*con.setConnectTimeout(5000);
				con.setReadTimeout(10000);*/
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(pudSubscriptionList));
				  streamWriter.flush();
				  streamWriter.close();
				 logger.info("Response Code : " +con.getResponseCode());
				  if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1 + "\n");
		                }
		                bufferedReader.close();
		                streamReader.close();
		                //System.out.println("Data Received From  pudSubcriptionCallBack api "+stringBuilder.toString());
		                logger.info("Data Received From  pudSubcriptionCallBack api "+stringBuilder.toString());
		                GeneralResponse apiResponse=gson.fromJson(stringBuilder.toString(), GeneralResponse.class);
		                return apiResponse;
		    			
		    		}
			}catch(Exception e) {
				e.printStackTrace();
				logger.error("Exception while posting pud subscription list ", e);
			}finally{
				
				
				if(con!=null){con.disconnect();}
			}
		}
		
		
		return null;
		
	}

}
