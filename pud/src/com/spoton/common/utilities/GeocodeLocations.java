package com.spoton.common.utilities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;


import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeocodedAddressComponents;
import com.spoton.pud.data.GoogleResponse;


public class GeocodeLocations {
	
	private static String totalConsigneeAddress=null;
	static Double latitude=0.0;static Double longitude=0.0;
	static String latlong=null;
	
	public static String geocodeAddressCityAndPincode(String address,String city,String pincode,ManageTransaction mt,Logger logger) {
		try {
		if(address!=null&&address!=""&&!address.isEmpty()&&city!=null&&city!=""&&!city.isEmpty()){
			totalConsigneeAddress=address+","+city+","+pincode;
			totalConsigneeAddress=totalConsigneeAddress.replaceAll("[\\t\\n\\r]+"," ");	
			logger.info("totalConsigneeAddress (Address, City and Pincode) "+totalConsigneeAddress);
			logger.info("**************Converting totalConsigneeAddress to lat/lng**************");
			GoogleResponse res =CommonTasks.convertToLatLong(totalConsigneeAddress,logger);
			logger.info("Status "+res!=null?res.getStatus():"Exception in converting lat/lng");
			if(res!=null&&res.getStatus().equals("OK"))//converting address+pincode success
			  {
				if(res.getResults()[0].getFormatted_address()!=null) {
					boolean checkIfIndiaLocation=checkIfIndiaLocation(res.getResults()[0].getFormatted_address(),logger);
					if(checkIfIndiaLocation) {
					latitude=(Double.parseDouble(res.getResults()[0].getGeometry().getLocation().getLat()));
					longitude=(Double.parseDouble(res.getResults()[0].getGeometry().getLocation().getLng()));
					latlong=latitude+"/"+longitude;
					logger.info("latitude= "+latitude+" Longitude "+longitude);
					}else {
						logger.info("******Not India Location So will geocode city and pincode********");
						latlong=geocodeCityAndPincode(city, pincode, mt,logger);
					}
					
				}else {
					logger.info("********Formatted Address Is Null**************");
					latlong=geocodeCityAndPincode(city, pincode, mt,logger);
					if(latlong==null) {
						 logger.info("**************Failed Converting Address +City+Pincode to lat/lng.Now will geocode Pincode**************");
						latlong=geocodePincode(pincode, mt,logger);	
					}
				}
			  }else if(city!=null&&city!=""&&!city.isEmpty()){
					  logger.info("**************Failed Converting ConsigneeAddress+City+Pincode to lat/lng.Now will geocode City and Pincode**************");
					  latlong=geocodeCityAndPincode(city, pincode, mt,logger);
						if(latlong==null) {
							 logger.info("**************Failed Converting City+Pincode to lat/lng.Now will geocode Pincode**************");
							latlong=geocodePincode(pincode, mt,logger);	
						}
			
			
			  }
		}else if(city!=null&&city!=""&&!city.isEmpty()){
			  logger.info("**************Consignee Address is empty.Now will geocode City and Pincode**************");
			 latlong=geocodeCityAndPincode(city, pincode, mt,logger);
				if(latlong==null) {
					logger.info("**************Failed Converting City+Pincode to lat/lng.Now will geocode Pincode**************");
					latlong=geocodePincode(pincode, mt,logger);	
				}
	
	}else if(pincode!=null&&pincode!=""){
		  logger.info("**************Consignee City is empty.So will geocode only Pincode**************");
		 latlong=geocodePincode(pincode,mt,logger);
			  }
		return latlong;
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Exception in geocoding address city and pincode ",e);
            return null;
		}
		
		
		
	}
	
	public static String geocodeCityAndPincode(String city,String pincode,ManageTransaction mt,Logger logger) {
		try {
			 totalConsigneeAddress=city+","+pincode;
				totalConsigneeAddress=totalConsigneeAddress.replaceAll("[\\t\\n\\r]+"," ");	
				logger.info("totalConsigneeAddress(City and Pincode) "+totalConsigneeAddress);
				logger.info("**************Converting (City and Pincode) to lat/lng**************");
		GoogleResponse res2 =CommonTasks.convertToLatLong(totalConsigneeAddress,logger);
		logger.info("Status "+res2!=null?res2.getStatus():"Exception in converting lat/lng");
		if(res2!=null&&res2.getStatus().equals("OK"))
		  {
			boolean checkIfIndiaLocation=checkIfIndiaLocation(res2.getResults()[0].getFormatted_address(),logger);
			if(checkIfIndiaLocation) {
			latitude=(Double.parseDouble(res2.getResults()[0].getGeometry().getLocation().getLat()));
			longitude=(Double.parseDouble(res2.getResults()[0].getGeometry().getLocation().getLng()));
			latlong=latitude+"/"+longitude;
			logger.info("latitude= "+latitude+" Longitude "+longitude); 
			}else {
				logger.info("******Not India Location So will geocode only  pincode********");
				latlong=geocodePincode(pincode,mt,logger);
			}
	     }else if(pincode!=null&&pincode!=""){
	     logger.info("**************Failed Converting ConsigneeCity+Pincode to lat/lng.Now will geocode only Pincode**************");
	        latlong=geocodePincode(pincode,mt,logger);
		  }
		return latlong;
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Exception in geocoding city and pincode ",e);
			return null;
		}
		
		
	}
	
	public static String geocodePincode(String pincode,ManageTransaction mt,Logger logger) {
		try {
		  String query="Select p.latitude,p.longitude from pincode_locations_data p where p.pincode=?1 order by p.id asc limit 1";
		  Query nq=mt.createNativeQuery(query).setParameter(1, Integer.parseInt(pincode));
		  try {
		  Object[] location=(Object[]) nq.getSingleResult();
		  if(location!=null&&(double)location[0]!=0&&(double)location[1]!=0) {
			  logger.info("*********Found in Pincode Locations Table*****");
			  latlong=(double)location[0]+"/"+(double)location[1];
		  }
		  }catch(Exception e) {
			 // e.printStackTrace();
			  logger.info("*********Not found in Pincode Locations Table So Geocoding*****");
			logger.info("totalConsigneeAddress(Pincode) "+pincode);
			logger.info("**************Converting Pincode to lat/lng**************");
			GoogleResponse res3 =CommonTasks.convertToLatLong(totalConsigneeAddress,logger);
			logger.info("Status "+res3!=null?res3.getStatus():"Exception in converting lat/lng");
			if(res3!=null&&res3.getStatus().equals("OK"))
			  {
				latitude=(Double.parseDouble(res3.getResults()[0].getGeometry().getLocation().getLat()));
				longitude=(Double.parseDouble(res3.getResults()[0].getGeometry().getLocation().getLng()));
				logger.info("latitude= "+latitude+" Longitude "+longitude); 
				latlong=latitude+"/"+longitude;
			  }
		  }
		  return latlong;
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Exception in geocoding pincode ",e);
		return null;
		}
		
	}
	
	private static boolean checkIfIndiaLocation(String address,Logger logger) {
		try {
		String array[]=address.split(",");
		int length=array.length;
		logger.info("**********Name of Country "+array[length-1]);
		
		if(length>0&&array[length-1].equalsIgnoreCase(" India")) {
			return true;
		}else {
		return false;
		}
		}catch(Exception e) {
			e.printStackTrace();
			logger.error("Exception while validating the location ", e);
			return false;
		}
		
	}
	
	public static List<String> getGeocodedAddress(double latitude,double longitude,Logger logger) {
		String address=null;String pincodeString=null;List<String> finalList=null;
		try {
			Gson gson = new GsonBuilder().serializeNulls().create();
			GoogleResponse res =CommonTasks.convertLatLongToAddress(latitude, longitude, logger);
			logger.info("*********Google api response Status******" +(res!=null?res.getStatus():null));
			//logger.info(res!=null?res.getStatus():null);
			//logger.info("Status "+res!=null?res.getStatus():"Exception in converting lat/lng");
			
			if(res!=null&&res.getStatus().equals("OK"))//converting address+pincode success
			  {
				if(res.getResults()[0].getFormatted_address()!=null) {
					finalList=new ArrayList<String>();
					address=res.getResults()[0].getFormatted_address();
					logger.info("Geocoded Address****  "+address);
					
					finalList.add(address);
					List<GeocodedAddressComponents> addressComponents=res.getResults()[0].getAddress_components();
					pincodeString=addressComponents.get(addressComponents.size()-1).getLong_name();
					logger.info("Geocoded Pincode****  "+pincodeString);
					finalList.add(pincodeString);
				}
				}
			
			/*System.out.println("address "+address+" pincode "+pincodeString);*/
			
		/*	String query="Select p.pincode from pincode_locations_data p where p.latitude like ?1 and p.longitude like ?2 order by p.id asc limit 1";
			  Query nq=mt.createNativeQuery(query).setParameter(1, latitude).setParameter(2, longitude);
			  try {
			  Object pincode=(Object) nq.getSingleResult();
			  if(pincode!=null&&(int)pincode!=0) {
				  logger.info("*********Found in Pincode Locations Table*****");
				  address=String.valueOf(pincode);
			  }
		
		
		
	}catch(Exception e) {
	e.printStackTrace();
	logger.error("Exception while geocoding the location ", e);
	}*/
	}catch(Exception e) {
		e.printStackTrace();
		logger.error("Exception while geocoding the location ", e);
		}
		return finalList;

}
}
