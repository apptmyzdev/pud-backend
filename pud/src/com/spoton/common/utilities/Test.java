package com.spoton.common.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.BankMasterErpModal;
import com.spoton.pud.data.BankMasterModal;

import com.spoton.pud.data.GoogleResponse;
import com.spoton.pud.data.PushDataModel;
import com.spoton.pud.jpa.Piece;
import com.spoton.pud.data.GoogleDirectionsResponse;

public class Test {

	public static void main(String[] args) throws IOException, ParseException {
		
		String date="2021-04-12T18:30:00";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	 ///format.setTimeZone(TimeZone.getTimeZone("Asia/Dubai"));
		
		Date dt=format.parse(date);
		System.out.println("dt "+dt);
		//getDubaiTime(new Date());
		
		/*List<String> base64ImagesList=new ArrayList<String>();
		File f=new File("C:/Users/twink/Desktop/475.jpg");
		FileInputStream fis = new FileInputStream(f);
		byte byteArray[] = new byte[(int)f.length()];
		fis.read(byteArray);
		String imageString = Base64.encodeBase64String(byteArray);
		System.out.println(imageString);base64ImagesList.add(imageString);
		File f1=new File("C:/Users/twink/Desktop/111.jpg");
		FileInputStream fis1 = new FileInputStream(f1);
		byte byteArray1[] = new byte[(int)f1.length()];
		fis1.read(byteArray1);
		String imageString1 = Base64.encodeBase64String(byteArray1);
		System.out.println(imageString1);
		System.out.println(base64ImagesList.contains(imageString1));*/
		
		
	/*PushDataModel pd=new PushDataModel();
		pd.setId(5);
		Logger pickupRegistrationLogger = Logger.getLogger("PickupRegistrationService");
		Gson gson = new GsonBuilder().serializeNulls().create();
		List<String> pushList=new ArrayList<String>();
		pushList.add("eXSHKXJMMOY:APA91bH_UUoq3eH_kjC7EdNCjOWZ5DPAdEWhKBEWwP9eipTi1rcdhv0FVOQsTYmVnBbUYPdQq1hN-h94lKESE01IMw0mOZauekHIfmt75J34XS6sB5-VMopXtpuVMU8uPRm8kuGCbwCf");
		boolean pushReturn = CommonUtility.sendPushNotification("You have new pickup in title", "You have new pickup in body", pushList , pickupRegistrationLogger, gson, pd);
		System.out.println(pushReturn);
		*/
		/*Gson gson = new GsonBuilder().serializeNulls().create();
		
		 String URL = "https://maps.googleapis.com/maps/api/directions/json";
		 String origin="19.674673, 78.538183";String destination="20.277911, 78.718030";//String destination="19.664746, 78.530653";
		 URL url = new URL(URL + "?origin="
				    + URLEncoder.encode(origin, "UTF-8") +"&destination=" + URLEncoder.encode(destination, "UTF-8")+"&key=AIzaSyDCqU7Sw_Ks1bjGSSoRMT1FrLjHum3PRLY");
		 System.out.println("Google URL  "+url);
		 URLConnection conn = url.openConnection();
		  String response1 = null;StringBuilder stringBuilder = new StringBuilder();
		  InputStreamReader streamReader = new InputStreamReader(conn.getInputStream());
          BufferedReader bufferedReader = new BufferedReader(streamReader);
		  while ((response1 = bufferedReader.readLine()) != null) {
              stringBuilder.append(response1);
          }
		  System.out.println(stringBuilder.toString());
		Response res=gson.fromJson(stringBuilder.toString(),Response.class);
		System.out.println("**********************" +gson.toJson(res));
		String duration=res.getRoutes()[0].getLegs()[0].getDuration().getText();
		System.out.println("duration "+duration);
		System.out.println("aray "+duration.split(" ").length);
		for(int i=0;i<duration.split(" ").length;i++) {
			System.out.println(duration.split(" ")[i]);
		}
		Calendar cd=Calendar.getInstance();
		if(duration.contains("hour")&&duration.contains("mins")) {
			System.out.println("*******in hr mins ");
			
			System.out.println("hr "+cd.getTime().getHours() +"mins "+cd.getTime().getMinutes());
			cd.add(Calendar.HOUR_OF_DAY,Integer.parseInt(duration.split(" ")[0].trim()));
			cd.add(Calendar.MINUTE,Integer.parseInt(duration.split(" ")[2].trim()));
		}
		else if(duration.contains("mins")) {
			System.out.println("*******in  mins ");
			cd.add(Calendar.MINUTE,Integer.parseInt(duration.split(" ")[2].trim()));
		}
		System.out.println(new Date());
		System.out.println(cd.getTime());
		SimpleDateFormat format=new SimpleDateFormat("HH:mm");
		System.out.println(format.format(cd.getTime()));*/
		
		/*Calendar yesterday=Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		System.out.println(Math.round(268.42999267578125*100)/100.0);*/
		/*ManageTransaction mt=new ManageTransaction();
		Piece p=new Piece();
		p.setConNumber("1234");
		p.setCreatedTimestamp(new Date());
		p.setPcrKey(123);
		mt.persist(p);
		System.out.println("id "+p.getId());
		mt.commit();
		System.out.println("id after commit "+p.getId());
		
		Map<Integer,List<Integer>> map=new HashMap<Integer,List<Integer>>();
		List<Integer> list=new ArrayList<Integer>();
		list.add(1);
		map.put(1, list);
		list.add(2);
		map.put(1, list);
		list.add(3);
		map.put(1, list);
		list.add(4);
		map.put(1, list);
		
		System.out.println("map "+map);*/
		
		/*String address="Ashok road"+"\\\\"+"Pavan Tent house"+"\\\\"+"Flower market"+"\\\\"+"Adilabad"+"\\\\"+"Telangana"+"\\\\"+"504001";
		String us=address.split("\\")[0];
		System.out.println("address "+us);
		*/
		/*DateFormat engDateFormat = new SimpleDateFormat("MMM dd,yyyy", Locale.ENGLISH);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String fromDate="2019-12-10";String toDate="2020-01-10";
		String period=engDateFormat.format(dateFormat.parse(fromDate))+"-"+engDateFormat.format(dateFormat.parse(toDate));
		System.out.println("period "+period);*/
		/*String year=(new Date().getYear()+1900)-1+"-"+(new Date().getYear()+1900);
		
		System.out.println("year "+year);*/
	}

	
	public static Date getDubaiTime(Date time){
		Date currentTime = new Date(time.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentTime);
		System.out.println("currentTime "+cal.getTime());
		long fromOffset = TimeZone.getDefault().getOffset(cal.getTime().getTime());
		System.out.println("fromOffset "+fromOffset);
		long toOffset = TimeZone.getTimeZone("Asia/Dubai").getOffset(cal.getTime().getTime());
		System.out.println("toOffset "+toOffset);
		long offsetDifference = toOffset - fromOffset;
		System.out.println("offsetDifference "+offsetDifference);
		currentTime = new Date(cal.getTime().getTime()+offsetDifference);
		System.out.println("currentDubaiTime "+currentTime);
		return currentTime;
	}
}
