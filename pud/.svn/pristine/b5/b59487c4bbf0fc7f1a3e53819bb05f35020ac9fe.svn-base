package com.spoton.pud.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.ConDetailsData;

import com.spoton.pud.data.ConDetailsModalV3;
import com.spoton.pud.data.EwayBillDetails;
import com.spoton.pud.data.EwayBillNo;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.PickupUpdateStatusModal;
import com.spoton.pud.data.PickupUpdatinResponse;
import com.spoton.pud.data.PickupUpdationData;

import com.spoton.pud.data.PickupUpdationModalV3;
import com.spoton.pud.data.PieceEntryData;
import com.spoton.pud.data.PieceEntryModal;
import com.spoton.pud.data.PieceVolumeData;
import com.spoton.pud.data.PieceVolumeModal;
import com.spoton.pud.data.Pieces;
import com.spoton.pud.data.PiecesData;
import com.spoton.pud.data.PiecesImages;
import com.spoton.pud.data.PiecesImagesData;
import com.spoton.pud.data.WSTPiecesRefListModal;
import com.spoton.pud.jpa.ConDetail;
import com.spoton.pud.jpa.PickupUpdationStatus;
import com.spoton.pud.jpa.PieceVolume;
import com.spoton.pud.jpa.UpdatedEwayBillNumber;

/**
 * Servlet implementation class GetAndPostErpPickupsV2
 */
@WebServlet("/getAndPostErpPickupsV2")
public class GetAndPostErpPickupsV2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetAndPostErpPickupsV2");
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAndPostErpPickupsV2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		logger.info("**********START OF GET METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		String userName= request.getParameter("userName");
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion+" userName "+userName);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
			boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			logger.info("checkapkVersion "+checkapkVersion);
			if(checkapkVersion){
			if(CommonTasks.check(userName)){
				mt=new ManageTransaction();
				//To get Piece Entry data
				String pieceEntry="select pe.pcr_key,pe.from_piece_no,pe.to_piece_no,pe.total_pieces,pe.con_id from piece_entry pe,con_details c "
						+ "where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";
				Query pieceEntryQuery=mt.createNativeQuery(pieceEntry).setParameter(1, userName);
				List<Object[]> pieceEntryList=pieceEntryQuery.getResultList();
				PieceEntryModal pieceEntryData=null;
				List<PieceEntryModal> pieceEntryDataList=null;
				Map<Integer,List<PieceEntryModal>> pieceEntryMap=new HashMap<Integer, List<PieceEntryModal>>();
				for(Object[] o:pieceEntryList){
					pieceEntryData=new PieceEntryModal();
					pieceEntryData.setPcrKey(Integer.toString((int) o[0]));
					pieceEntryData.setFromPieceNo((String) o[1]);
					pieceEntryData.setToPieceNo((String) o[2]);
					pieceEntryData.setTotalPieces((String) o[3]);
					if(pieceEntryMap.containsKey((int) o[4])){
						pieceEntryDataList=pieceEntryMap.get((int) o[4]);
						pieceEntryDataList.add(pieceEntryData);
					}else{
						pieceEntryDataList=new ArrayList<PieceEntryModal>();
						pieceEntryDataList.add(pieceEntryData);
						pieceEntryMap.put((int) o[4], pieceEntryDataList);
					}
					
					
				}
				
				//To get Pieces data
				String pieces="select pe.pcr_key,pe.last_piece_no,pe.total_used,pe.con_id "
						+ "from pieces pe,con_details c where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";	
				Query piecesQuery=mt.createNativeQuery(pieces).setParameter(1, userName);
				List<Object[]> piecesList=piecesQuery.getResultList();
				Pieces piecesData=null;
				List<Pieces> piecesDataList=null;
				Map<Integer,List<Pieces>> piecesDataMap=new HashMap<Integer, List<Pieces>>();
				for(Object[] o:piecesList){
					piecesData=new Pieces();
					piecesData.setPcrKey(Integer.toString((int) o[0]));
					piecesData.setLastPieceNumber((String) o[1]);
					piecesData.setTotalUsed((String) o[2]);
					if(piecesDataMap.containsKey((int) o[3])){
						piecesDataList=piecesDataMap.get((int) o[3]);
						piecesDataList.add(piecesData);
						
					}else{
						piecesDataList=new ArrayList<Pieces>();
						piecesDataList.add(piecesData);
						piecesDataMap.put((int) o[3], piecesDataList);
						
					}
					
				}
				
				//To get Piece Volume data
				String pieceVolume="select pe.no_of_pieces,pe.total_vol_weight,pe.vol_breadth,pe.vol_height,pe.vol_length,pe.con_id,pe.id "
						+ "from piece_volume pe,con_details c where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";	
				Query pieceVolumeQuery=mt.createNativeQuery(pieceVolume).setParameter(1, userName);
				List<Object[]> pieceVolumeList=pieceVolumeQuery.getResultList();
				PieceVolumeModal pieceVolumeData=null;
				List<PieceVolumeModal> pieceVolumeDataList=null;
				Map<Integer,List<PieceVolumeModal>> pieceVolumeDataMap=new HashMap<Integer, List<PieceVolumeModal>>();
				for(Object[] o:pieceVolumeList){
					pieceVolumeData=new PieceVolumeModal();
					pieceVolumeData.setNoOfPieces((int) o[0]);
					pieceVolumeData.setTotalVolWeight((double) o[1]);
					pieceVolumeData.setVolBreadth((double) o[2]);
					pieceVolumeData.setVolHeight((double) o[3]);
					pieceVolumeData.setVolLength((double) o[4]);
					pieceVolumeData.setId((int) o[6]);
					if(pieceVolumeDataMap.containsKey((int) o[5])){
						pieceVolumeDataList=pieceVolumeDataMap.get((int) o[5]);
						pieceVolumeDataList.add(pieceVolumeData);
					}else{
						pieceVolumeDataList=new ArrayList<PieceVolumeModal>();
					    pieceVolumeDataList.add(pieceVolumeData);
						pieceVolumeDataMap.put((int) o[5], pieceVolumeDataList);
					}
					
				}
				
				//To get Piece Images data
				String pieceImage="select pe.pieces_images_url,pe.con_id from pieces_images pe,con_details c "
						+ "where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";
				Query pieceImageQuery=mt.createNativeQuery(pieceImage).setParameter(1, userName);
				List<Object[]> pieceImageList=pieceImageQuery.getResultList();
				PiecesImages pieceImageData=null;
				List<PiecesImages> pieceImagesDataList=null;
				Map<Integer,List<PiecesImages>> pieceImagesDataMap=new HashMap<Integer, List<PiecesImages>>();
				for(Object[] o:pieceImageList){
					pieceImageData=new PiecesImages();
					pieceImageData.setPiecesImagesURL((String) o[0]);
					if(pieceImagesDataMap.containsKey((int) o[1])){
						pieceImagesDataList=pieceImagesDataMap.get((int) o[1]);
						pieceImagesDataList.add(pieceImageData);
					}else{
						pieceImagesDataList=new ArrayList<PiecesImages>();
						pieceImagesDataList.add(pieceImageData);
						pieceImagesDataMap.put((int) o[1], pieceImagesDataList);
					}
					
				}
				
				
				//To get wst pieces
				String wstPieces="select w.con_number,w.piece_number,w.piece_ref_number,w.piece_serial_number,w.con_id from wst_pieces_ref_list w,con_details c "
						+ "where w.con_id=c.id and c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";
				Query wstPiecesQuery=mt.createNativeQuery(wstPieces).setParameter(1, userName);
				List<Object[]> wstPiecesList=wstPiecesQuery.getResultList();
				WSTPiecesRefListModal wstPiecesData=null;
				List<WSTPiecesRefListModal> wstPiecesDataList=null;
				Map<Integer,List<WSTPiecesRefListModal>> wstPiecesDataMap=new HashMap<Integer, List<WSTPiecesRefListModal>>();
				for(Object[] o:wstPiecesList){
					wstPiecesData=new WSTPiecesRefListModal();
					wstPiecesData.setConNumber((String) o[0]);
					wstPiecesData.setPieceNo((String) o[1]);
					wstPiecesData.setPieceRefNo((String)o[2]);
					wstPiecesData.setPieceSlNo((String) o[3]);
					if(wstPiecesDataMap.containsKey((int) o[4])) {
						wstPiecesDataList=wstPiecesDataMap.get((int)o[4]);
						wstPiecesDataList.add(wstPiecesData);
					}else {
						wstPiecesDataList=new ArrayList<WSTPiecesRefListModal>();
						wstPiecesDataList.add(wstPiecesData);
						wstPiecesDataMap.put((int) o[4], wstPiecesDataList);
						
					}
					
					
				}
				
				
				/*//To get eway bill numbers data
				 String addedEwayBillNumbers="select pe.eway_bill_number,pe.con_id from updated_eway_bill_numbers pe,con_details c "
							+ "where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1";
					Query addedEwayBillNumbersQuery=mt.createNativeQuery(addedEwayBillNumbers).setParameter(1, userName);
					List<Object[]> ewayNumbersList=addedEwayBillNumbersQuery.getResultList();
					List<String> ewaysList=null;
					Map<Integer,List<String>> ewayNumbersDataMap=new HashMap<Integer, List<String>>();
					for(Object[] o:ewayNumbersList){
						
						if(ewayNumbersDataMap.containsKey((int) o[1])){
							ewaysList=ewayNumbersDataMap.get((int) o[1]);
							ewaysList.add((String)o[0]);
						}else{
							ewaysList=new ArrayList<String>();
							ewaysList.add((String)o[0]);
							ewayNumbersDataMap.put((int) o[1], ewaysList);
						}
					}*/
				Map<String,PickupUpdationModalV3> pickupsMap=new HashMap<String, PickupUpdationModalV3>();
					String conQuery="Select c.con_entry_id,c.con_number,c.ref_number,c.pickup_date,c.product,c.origin_pincode,c.destination_pincode,"
							+ "c.actual_weight,c.apply_dc,c.apply_nf_form,c.apply_vtv,c.consignment_type,c.crm_schedule_id,c.customer_code,"
							+ "c.declared_value,c.gate_pass_time,c.image1_url,c.image2_url,c.lat_value,"
							+ "c.long_value,c.no_of_package,c.order_no,c.package_type,c.pan_no,c.payment_basis,c.receiver_name,c.receiver_phone_no,"
							+ "c.risk_type,c.shipment_image_url,c.special_instruction,c.tin_no,c.total_vol_weight,c.user_id,"
							+ "c.user_name,c.vol_type,c.vtc_amount,c.id,c.customer_name,c.vehicle_number,c.product_type,c.is_box_piece_mapping "
							+ "from con_details c where c.pickup_type=1 and c.user_name=?1 and date(c.pickup_date)=curdate()";
					Query query1=mt.createNativeQuery(conQuery).setParameter(1, userName);
					List<Object[]> consList=query1.getResultList();
					if(consList!=null&&consList.size()>0){
						logger.info("consList size "+consList.size());
						List<ConDetailsModalV3> conDetailslList=null;
						
						for(Object[] o:consList){
							ConDetailsModalV3 conData=new ConDetailsModalV3();
							  conData.setConEntryId((int) o[36]);
							  conData.setConNumber( o[1]!=null?(String) o[1]:"");
							  conData.setRefNumber(o[2]!=null?(String) o[2]:"");
							  if(o[3]!=null)
				    		    conData.setPickupDate(format.format((Date) o[3]));
								conData.setProduct(o[4]!=null?(String) o[4]:"");
								conData.setOriginPinCode(o[5]!=null?(String) o[5]:"");
								conData.setDestinationPinCode(o[6]!=null?(String) o[6]:"");
							    conData.setActualWeight(o[7]!=null?(String) o[7]:"");
			    				conData.setApplyDC(o[8]!=null?(String) o[8]:"");
			    				conData.setApplyNfForm((String) o[9]);
			    				conData.setApplyVTV((String) o[10]);
			    			    conData.setConsignmentType((String) o[11]);
			    				conData.setCrmScheduleId((String) o[12]);
			    				conData.setCustomerCode((String) o[13]);
			    				conData.setDeclaredValue((String) o[14]);
			    				conData.setGatePassTime((String) o[15]);
			    				conData.setImage1URL((String) o[16]);
			    				conData.setImage2URL((String) o[17]);
			    				conData.setLatValue((String) o[18]);
			    				conData.setLongValue((String) o[19]);
			    				conData.setNoOfPackage((String) o[20]);
			    				conData.setOrderNo((String) o[21]);
			    			    conData.setPackageType((String) o[22]);
			    				conData.setPanNo((String) o[23]);
			    				conData.setPaymentBasis((String) o[24]);
			    				conData.setReceiverName((String) o[25]);
			    				conData.setReceiverPhoneNo((String) o[26]);
			    				conData.setRiskType((String) o[27]);
			    				conData.setShipmentImageURL((String) o[28]);
			    				conData.setSpecialInstruction((String) o[29]);
			    				conData.setTinNo((String) o[30]);
			    				conData.setTotalVolWeight((String) o[31]);
			    				conData.setUserId((String) o[32]);
			    				conData.setUserName((String) o[33]);
			    				conData.setVolType((String) o[34]);
			    				conData.setVtcAmount((String) o[35]);
			    				conData.setCustomerName((String) o[37]);
			    				conData.setVehicleNumber((String) o[38]);
			    				conData.setProductType((String) o[39]);
			    				conData.setIsBoxPieceMapping((String) o[40]);
			    				if(pieceEntryMap.get(conData.getConEntryId())!=null)
			    				conData.setPieceEntry(pieceEntryMap.get(conData.getConEntryId()));
			    				
			    				if(piecesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPieces(piecesDataMap.get(conData.getConEntryId()));
			    				
			    				if(pieceVolumeDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPieceVolume(pieceVolumeDataMap.get(conData.getConEntryId()));
			    				if(wstPiecesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setWstPiecesRefList(wstPiecesDataMap.get(conData.getConEntryId()));
			    				
			    				if(pieceImagesDataMap.get(conData.getConEntryId())!=null)
			    					conData.setPiecesImages(pieceImagesDataMap.get(conData.getConEntryId()));
			    				/*if(ewayNumbersDataMap.get(conData.getConEntryId())!=null)
			    					conData.seteWayBillNumberList(ewayNumbersDataMap.get(conData.getConEntryId()));*/
			    				if(pickupsMap.containsKey(conData.getCrmScheduleId())){
			    					PickupUpdationModalV3 pickupData=pickupsMap.get(conData.getCrmScheduleId());
			    					conDetailslList=pickupData.getConDetails();
			    					conDetailslList.add(conData);
			    				}else{
			    					PickupUpdationModalV3 pickupData=new PickupUpdationModalV3();
			    					conDetailslList=new ArrayList<ConDetailsModalV3>();
			    					conDetailslList.add(conData);
			    					pickupData.setConDetails(conDetailslList);
				    				pickupsMap.put(conData.getCrmScheduleId(),pickupData);
			    				}
			    				
						}// end of main for
					
						 result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED, pickupsMap.values()));
					
					}else{
						result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_NO_DATA_AVAILABLE, null));
					}
					
					
					
				/*if(responseList!=null&&responseList.getConDetails()!=null&&responseList.getConDetails().size()>0){
                	List<ConDetailsModalV2> conDetails=new ArrayList<ConDetailsModalV2>(); 
                	for(ConDetailsData a:responseList.getConDetails()){
                		ConDetailsModalV2 conDetailsModal=new ConDetailsModalV2();
                		conDetailsModal.setConEntryId(a.getConEntryID());
          			    conDetailsModal.setActualWeight(a.getActualWeight());
          			    conDetailsModal.setApplyDC(a.getApplyDC());
          			    conDetailsModal.setApplyNfForm(a.getApplyNFForm());
          			    conDetailsModal.setApplyVTV(a.getApplyVTV());
          			    conDetailsModal.setConNumber(a.getConNumber());
          			    conDetailsModal.setConsignmentType(a.getConsignmentType());
          			    conDetailsModal.setCrmScheduleId(a.getCRMScheduleID());
          			    conDetailsModal.setCustomerCode(a.getCustomerCode());
          			    conDetailsModal.setDeclaredValue(a.getDeclaredValue());
          			    conDetailsModal.setDestinationPinCode(a.getDestinationPINCode());
          			    conDetailsModal.setGatePassTime(a.getGatePassTime());
          			    conDetailsModal.setImage1URL(a.getImage1URL());
          			    conDetailsModal.setImage2URL(a.getImage2URL());
          			    conDetailsModal.setLatValue(a.getLatvalue());
          			    conDetailsModal.setLongValue(a.getLongvalue());
          			    conDetailsModal.setNoOfPackage(a.getNoofPackage());
          			    conDetailsModal.setOrderNo(a.getOrderNo());
          			    conDetailsModal.setOriginPinCode(a.getOriginPINCode());
          			    conDetailsModal.setPackageType(a.getPackageType());
          			    conDetailsModal.setPanNo(a.getPANNo());
          			    conDetailsModal.setPaymentBasis(a.getPaymentbasis());
          			    conDetailsModal.setPickupDate(a.getPickupdate());
          				conDetailsModal.setProduct(a.getProduct());
          				conDetailsModal.setReceiverName(a.getReceiverName());
          				conDetailsModal.setReceiverPhoneNo(a.getReceiverPhoneNo());
          				conDetailsModal.setRefNumber(a.getRefNumber());
          				conDetailsModal.setRiskType(a.getRiskType());
          				conDetailsModal.setShipmentImageURL(a.getShipmentImageURL());
          				conDetailsModal.setSpecialInstruction(a.getSpecialInstruction());
          				conDetailsModal.setTinNo(a.getTINNo());
          				conDetailsModal.setTotalVolWeight(a.getTotalVolWeight());
          				conDetailsModal.setUserId(a.getUserID());
          				conDetailsModal.setUserName(a.getUserName());
          				conDetailsModal.setVolType(a.getVolType());
          				conDetailsModal.setVehicleNumber(a.getVehicleNo());
          				conDetailsModal.setVtcAmount(a.getVTCAmount());
          				if(a.getPieces()!=null&&a.getPieces().size()>0){
          				List<Pieces> pieces=new ArrayList<Pieces>();
          				for(PiecesData p:a.getPieces()){
          					Pieces piece=new Pieces();
          					piece.setLastPieceNumber(p.getLastPieceNumber());
          					piece.setPcrKey(p.getPCR_Key());
          					piece.setTotalUsed(p.getTotalUsed());
          					pieces.add(piece);
          				}
          				conDetailsModal.setPieces(pieces);
          				}
          				if(a.getPieceEntry()!=null&&a.getPieceEntry().size()>0){
          				List<PieceEntryModal> pieceEntry=new ArrayList<PieceEntryModal>();
          				
          				for(PieceEntryData p:a.getPieceEntry()){
          					PieceEntryModal pieceEntryModal=new PieceEntryModal();
          					pieceEntryModal.setFromPieceNo(p.getFromPieceNo());
          					pieceEntryModal.setPcrKey(p.getPCR_Key());
          					pieceEntryModal.setToPieceNo(p.getToPieceNo());
          					pieceEntryModal.setTotalPieces(p.getTotalPieces());
          					pieceEntry.add(pieceEntryModal);
          				}
          				conDetailsModal.setPieceEntry(pieceEntry);
          				}
          				
          				if(a.getPiecesImages()!=null&&a.getPiecesImages().size()>0){
          					List<PiecesImages> piecesImages=new ArrayList<PiecesImages>();
          				for(PiecesImagesData p:a.getPiecesImages()){
          					PiecesImages images=new PiecesImages();
          					images.setPiecesImagesURL(p.getPiecesImagesURL());
          					piecesImages.add(images);
          				}
          				
          				conDetailsModal.setPiecesImages(piecesImages);
          				}
          				if(a.getPieceVolume()!=null&&a.getPieceVolume().size()>0){
          					List<PieceVolumeModal> pieceVolumes=new ArrayList<PieceVolumeModal>();
          					for(PieceVolumeData p:a.getPieceVolume()){
          						PieceVolumeModal pv=new PieceVolumeModal();
          						pv.setNoOfPieces(p.getNoofPieces());
          						pv.setTotalVolWeight(p.getTotalVolWeight());
          						pv.setVolBreadth(p.getVolbreadth());
          						pv.setVolHeight(p.getVolHeight());
          						pv.setVolLength(p.getVolLength());
          						pieceVolumes.add(pv);
          					
          					}
          					conDetailsModal.setPieceVolume(pieceVolumes);
          				}
          				if(a.getEwayBillNoList()!=null&&a.getEwayBillNoList().size()>0){
          					List<String> eWayBillNumberList=new ArrayList<String>();
          					for(EwayBillNo e:a.getEwayBillNoList()){
          						eWayBillNumberList.add(e.getEwayBillNum());
          					}
          					conDetailsModal.seteWayBillNumberList(eWayBillNumberList);
          				}
          				conDetails.add(conDetailsModal);
                	}//end of main for
                	
                }else{
   				 result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
    			}*/
			
			
		}else{
			 result = gson.toJson(new GeneralResponse(Constants.FALSE,Constants.ERROR_INCOMPLETE_DATA, null));
		}
			
			}else{
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.UPDATE_TO_LATEST_VERSION, null));
		}	
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null)
				mt.close();
		}
		
		logger.info("result "+result);
		logger.info("**********END OF GET METHOD***********");
		response.getWriter().write(result);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		logger.info("**********START OF POST METHOD***********");
		response.setContentType(Constants.CONTENT_TYPE_JSON);
		String result = "";
		Gson gson = new GsonBuilder().serializeNulls().create();
		ManageTransaction mt = null;
		String deviceIMEI = request.getHeader("imei");
		String apkVersion= request.getHeader("apkVersion");
		HttpURLConnection con=null; URL obj =null;
		String urlString=FilesUtil.getProperty("pickupUpdationDataFromErp");
		StringBuilder stringBuilder = null;
		logger.info("deviceIMEI "+deviceIMEI+" apkVersion "+apkVersion);
		try{
			/*boolean checkapkVersion=CommonTasks.checkLatestApkVersion((int)(Float.parseFloat(apkVersion)));
			if(checkapkVersion){*/
		String datasent=request.getReader().readLine();
		logger.info("Datasent from Mobile "+datasent);
		Type type = new TypeToken<PickupUpdationModalV3>(){}.getType();
        PickupUpdationModalV3 updationList=gson.fromJson(datasent,type);
        
        if(updationList!=null&&updationList.getConDetails()!=null&&updationList.getConDetails().size()>0)
		{
        	mt=new ManageTransaction();
        	/*Map<Integer,List<Integer>> piecesVolumeMap=new HashMap<Integer, List<Integer>>();
        String pieceVolume="select pe.id,pe.con_id "
					+ "from piece_volume pe,con_details c where pe.con_id=c.id and c.pickup_type=1 and c.user_name=?1";	
			Query pieceVolumeQuery=mt.createNativeQuery(pieceVolume).setParameter(1, updationList.getConDetails().get(0).getUserName());
			List<Object[]> pieceVolumeList=pieceVolumeQuery.getResultList();
			if(pieceVolumeList!=null&&pieceVolumeList.size()>0){
				List<Integer> idsList=null;
				for(Object[] o:pieceVolumeList){
					if(piecesVolumeMap.containsKey((int) o[1])){
						idsList=piecesVolumeMap.get((int) o[1]);
						idsList.add((int) o[0]);
					}else{
						idsList=new ArrayList<Integer>();
						idsList.add((int) o[0]);
						piecesVolumeMap.put((int) o[1], idsList);
					}
					
				}
			}*/
        	PickupUpdationData pickupUpdationData=new PickupUpdationData();
        	List<ConDetailsData> conDetailslList=new ArrayList<ConDetailsData>();
			for(ConDetailsModalV3 conList:updationList.getConDetails())
			{
        		ConDetail cd=mt.find(ConDetail.class, conList.getConEntryId());
        		cd.setVehicleNumber(conList.getVehicleNumber());
        		cd.setPickupType(2);
        		cd.setActualWeight(conList.getActualWeight());
        		mt.persist(cd);
        		PieceVolume pieceVolume=null;
				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
    				for(PieceVolumeModal pv:conList.getPieceVolume()){
    					try{
    					pieceVolume=mt.find(PieceVolume.class, pv.getId());
    					}catch(Exception e){
    						pieceVolume=new PieceVolume();
    					}
    					if(pieceVolume==null){
    						pieceVolume=new PieceVolume();
    					}
    					pieceVolume.setConDetail(cd);
    					pieceVolume.setNoOfPieces(pv.getNoOfPieces());
    					pieceVolume.setTotalVolWeight(pv.getTotalVolWeight());
    					pieceVolume.setVolBreadth(pv.getVolBreadth());
    					pieceVolume.setVolHeight(pv.getVolHeight());
    					pieceVolume.setVolLength(pv.getVolLength());
    					pieceVolume.setCreatedTimestamp(new Date());
	    				mt.persist(pieceVolume);	
    				}
				if(conList.geteWayBillNumberList()!=null&&conList.geteWayBillNumberList().size()>0){
					for(EwayBillDetails e:conList.geteWayBillNumberList()){
						UpdatedEwayBillNumber updatedEway=new UpdatedEwayBillNumber();
						updatedEway.setConDetail(cd);
						updatedEway.setCreatedTimestamp(new Date());
						updatedEway.setEwayBillNumber(e.getEwayBillNum());
						updatedEway.setInvoiceAmount(e.getInvoiceAmount());
						updatedEway.setInvoiceNumber(e.getInvoiceNo());
						updatedEway.setIsExempted(e.getIsExempted());
						mt.persist(updatedEway);
					}
				}
        		mt.begin();mt.commit();
				ConDetailsData conData=new ConDetailsData();
				conData.setConEntryID(conList.getConEntryId());
				conData.setActualWeight(conList.getActualWeight());
				conData.setApplyDC(conList.getApplyDC());
				conData.setApplyNFForm(conList.getApplyNfForm());
				conData.setApplyVTV(conList.getApplyVTV());
				conData.setConNumber(conList.getConNumber());
				conData.setConsignmentType(conList.getConsignmentType());
				conData.setCRMScheduleID(conList.getCrmScheduleId());
				conData.setCustomerCode(conList.getCustomerCode());
				conData.setDeclaredValue(conList.getDeclaredValue());
				conData.setDestinationPINCode(conList.getDestinationPinCode());
				conData.setGatePassTime(conList.getGatePassTime());
				conData.setLatvalue(conList.getLatValue());
				conData.setLongvalue(conList.getLongValue());
				conData.setNoofPackage(conList.getNoOfPackage());
				conData.setOrderNo(conList.getOrderNo());
				conData.setOriginPINCode(conList.getOriginPinCode());
				conData.setPackageType(conList.getPackageType());
				conData.setPANNo(conList.getPanNo());
				conData.setPaymentbasis(conList.getPaymentBasis());
				conData.setEwayBillNo(conList.geteWayBillNumber());
				
				if(conList.getPickupDate()!=null)
				conData.setPickupdate(conList.getPickupDate());
				conData.setProduct(conList.getProduct());
				conData.setReceiverName(conList.getReceiverName());
				conData.setReceiverPhoneNo(conList.getReceiverPhoneNo());
				conData.setRefNumber(conList.getRefNumber());
				conData.setRiskType(conList.getRiskType());
				
				
				conData.setSpecialInstruction(conList.getSpecialInstruction());
				conData.setTINNo(conList.getTinNo());
				conData.setTotalVolWeight(conList.getTotalVolWeight());
				conData.setUserID(conList.getUserId());
				conData.setUserName(conList.getUserName());
				conData.setVolType(conList.getVolType());
				conData.setVTCAmount(conList.getVtcAmount());
			
				conData.setVehicleNo(conList.getVehicleNumber());
				PiecesData pieceData=null;
				List<PiecesData> piecesList=new ArrayList<PiecesData>();
				if(conList.getPieces()!=null&&conList.getPieces().size()>0){
    				for(Pieces pc:conList.getPieces()){
    					pieceData=new PiecesData();
    					pieceData.setLastPieceNumber(pc.getLastPieceNumber());
	    				pieceData.setPCR_Key(pc.getPcrKey());
	    				pieceData.setTotalUsed(pc.getTotalUsed());
	    				piecesList.add(pieceData);
    				}
    				}
				PieceEntryData pieceEntryData=null;
				List<PieceEntryData> pieceEntryList=new ArrayList<PieceEntryData>();
				if(conList.getPieceEntry()!=null&&conList.getPieceEntry().size()>0)
    				for(PieceEntryModal pe:conList.getPieceEntry()){
    					pieceEntryData=new PieceEntryData();
    					pieceEntryData.setFromPieceNo(pe.getFromPieceNo());
	    				pieceEntryData.setPCR_Key(pe.getPcrKey());
	    				pieceEntryData.setToPieceNo(pe.getToPieceNo());
	    				pieceEntryData.setTotalPieces(pe.getTotalPieces());
	    				pieceEntryList.add(pieceEntryData);
    				}
			
				PieceVolumeData pieceVolumeData=null;
				List<PieceVolumeData> pieceVolumeList=new ArrayList<PieceVolumeData>();
				if(conList.getPieceVolume()!=null&&conList.getPieceVolume().size()>0)
				for(PieceVolumeModal pv:conList.getPieceVolume()){
					pieceVolumeData=new PieceVolumeData();
					pieceVolumeData.setNoofPieces(pv.getNoOfPieces());
    				pieceVolumeData.setTotalVolWeight(pv.getTotalVolWeight());
    				pieceVolumeData.setVolbreadth(pv.getVolBreadth());
    				pieceVolumeData.setVolHeight(pv.getVolHeight());
    				pieceVolumeData.setVolLength(pv.getVolLength());
    				pieceVolumeList.add(pieceVolumeData);
				}
				
				PiecesImagesData piecesImagesData=null;
				List<PiecesImagesData> piecesImagesDataList=new ArrayList<PiecesImagesData>();
				if(conList.getPiecesImages()!=null&&conList.getPiecesImages().size()>0){
					for(PiecesImages pi:conList.getPiecesImages()){
						piecesImagesData=new PiecesImagesData();
						piecesImagesData.setPiecesImagesURL(pi.getPiecesImagesURL());
	    				  piecesImagesDataList.add(piecesImagesData);
					}
				}
				
				EwayBillNo ewayNumber=null;
				List<EwayBillNo> ewayList=new ArrayList<EwayBillNo>();
				if(conList.geteWayBillNumberList()!=null&&conList.geteWayBillNumberList().size()>0){
					for(EwayBillDetails s:conList.geteWayBillNumberList()){
						ewayNumber=new EwayBillNo();
						ewayNumber.setEwayBillNum(s.getEwayBillNum());
						ewayNumber.setInvoiceAmount(s.getInvoiceAmount());
						ewayNumber.setInvoiceNo(s.getInvoiceNo());
						ewayNumber.setIsExempted(s.getIsExempted());
						ewayList.add(ewayNumber);
						}}
				conData.setPieces(piecesList);
				conData.setPieceEntry(pieceEntryList);
				conData.setPieceVolume(pieceVolumeList);
				conData.setPiecesImages(piecesImagesDataList);
				conData.setEwayBillNoList(ewayList);
				conDetailslList.add(conData);
			}//end of main for
			pickupUpdationData.setConDetails(conDetailslList);
			logger.info("DataSent to spoton "+gson.toJson(pickupUpdationData));
			try{
				obj = new URL(urlString);
				con = (HttpURLConnection) obj.openConnection();
				con.setConnectTimeout(5000);
				con.setReadTimeout(10000);
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", Constants.CONTENT_TYPE_JSON);
				con.setRequestProperty("Accept", "application/json");
				con.setDoOutput(true);
				
				OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
				  streamWriter.write(gson.toJson(pickupUpdationData));
				  streamWriter.flush();
				  
				  logger.info("Response Code : " +con.getResponseCode());
		            if (con.getResponseCode() == HttpURLConnection.HTTP_OK){
		            	stringBuilder=new StringBuilder();
		                InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
		                BufferedReader bufferedReader = new BufferedReader(streamReader);
		                String response1 = null;
		                while ((response1 = bufferedReader.readLine()) != null) {
		                    stringBuilder.append(response1);
		                }
		                bufferedReader.close();
		               // System.out.println("stringBuilder.toString()"+stringBuilder.toString());
		                logger.info("Response From Spoton : " +stringBuilder.toString());
		              
		                PickupUpdatinResponse responseList=gson.fromJson(stringBuilder.toString(),PickupUpdatinResponse.class);
		                if(responseList!=null&&responseList.getPickUpUpdateStatus()!=null&&responseList.getPickUpUpdateStatus().size()>0){
	    					PickupUpdationStatus ps=null;
	    					for(PickupUpdateStatusModal pm:responseList.getPickUpUpdateStatus()){
	    						ps=new PickupUpdationStatus();
	    						ConDetail c=mt.find(ConDetail.class, pm.getConEntryId());
	  						    c.setErpUpdated(1);
	    						ps.setConEntryId(pm.getConEntryId());
	    						ps.setTransactionResult(pm.getTransactionResult());
	    						ps.setCreatedTimestamp(new Date());	
	    						mt.persist(ps);
	    						mt.commit();
	    					}
	    					result = gson.toJson(new GeneralResponse(Constants.TRUE,Constants.REQUEST_COMPLETED,responseList));
		                }else{
    		            logger.info("No Response from spoton");
    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"No Response from spoton", null));
    		          }
	    				
		               
		            }else{
    		            logger.info("Response failed from spoton");
    		            result = gson.toJson(new GeneralResponse(Constants.FALSE,"Response Failed From ERP", null));
    		          }
		            }catch (SocketTimeoutException e){		
    					e.printStackTrace();
    					logger.error("SocketTimeoutException  From ERP",e);
    					
    					result = gson.toJson(new GeneralResponse(Constants.FALSE,"SocketTimeoutException From ERP", null));
		            }catch(Exception e){
		    			e.printStackTrace();
		    			logger.error("Exception e ",e);
		    			result = gson.toJson(new GeneralResponse(Constants.FALSE,
		    					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		    		}
		}
        /*}else{
				result = gson.toJson(new GeneralResponse(Constants.FALSE,
						Constants.UPDATE_TO_LATEST_VERSION, null));
			}	*/
        
        
        
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception e ",e);
			result = gson.toJson(new GeneralResponse(Constants.FALSE,
					Constants.ERRORS_EXCEPTION_IN_SERVER, null));
		}finally{
			if(mt!=null)
				mt.close();
		}
		logger.info("result "+result);
		logger.info("**********END OF POST METHOD***********");
		response.getWriter().write(result);
		}
	
}
