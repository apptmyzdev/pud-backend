package com.spoton.pud.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spoton.common.utilities.CommonTasks;
import com.spoton.common.utilities.Constants;
import com.spoton.common.utilities.FilesUtil;
import com.spoton.pud.connmgr.ManageTransaction;
import com.spoton.pud.data.GeneralResponse;
import com.spoton.pud.data.OdaConRemarksErpModal;
import com.spoton.pud.data.OdaConRemarksErpResponseModal;
import com.spoton.pud.data.OdaConRemarksModal;
import com.spoton.pud.jpa.OdaConRemark;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

@WebServlet(value={"/odaConRemarks"})
public class OdaConRemarks
extends HttpServlet {
    private static final long serialVersionUID = 1;
    Logger logger = Logger.getLogger((String)"OdaConRemarks");

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashMap<Integer, String> conMap;
        String pdc;
        String deviceIMEI;
        String userId;
        String resultMessage;
        String ipadd;
        String apkVersion;
        String result;
        block8 : {
            this.logger.info((Object)"**************START*******************");
            response.setContentType("application/json");
            result = "";
            Gson gson = new GsonBuilder().serializeNulls().create();
            String datasent = "";
            ManageTransaction mt = null;
            deviceIMEI = request.getHeader("imei");
            apkVersion = request.getHeader("apkVersion");
            ipadd = request.getRemoteAddr();
            HttpURLConnection con = null;
            URL obj = null;
            StringBuilder stringBuilder = null;
            userId = "";
            pdc = "";
            String urlString = FilesUtil.getProperty((String)"odaConRemarksUrl");
            resultMessage = "";
            this.logger.info((Object)("deviceIMEI:- " + deviceIMEI + " apkVersion:- " + apkVersion + " ipadd:- " + ipadd));
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            conMap = new HashMap<Integer, String>();
            try {
                datasent = request.getReader().readLine();
                this.logger.info((Object)("Datasent from Mobile " + datasent));
                mt = new ManageTransaction();
                OdaConRemarksModal inputJson = (OdaConRemarksModal)gson.fromJson(datasent, OdaConRemarksModal.class);
                OdaConRemark entity = new OdaConRemark();
                entity.setConNumber(inputJson.getConNo());
                entity.setCreatedTimestamp(new Date());
                entity.setLatitude(inputJson.getLatitude());
                entity.setLongitude(inputJson.getLongitude());
                entity.setRemarks(inputJson.getRemarks());
                entity.setRemarksDate(format.parse(inputJson.getRemarksDate()));
                entity.setUserId(inputJson.getUserId());
                entity.setPdc(inputJson.getPdc());
                mt.persist((Object)entity);
                mt.commit();
                conMap.put(entity.getTransactionId(), inputJson.getConNo());
                userId = inputJson.getUserId();
                pdc = inputJson.getPdc();
                OdaConRemarksErpModal data = new OdaConRemarksErpModal();
                data.setConNo(inputJson.getConNo());
                data.setLatitude(inputJson.getLatitude());
                data.setLongitude(inputJson.getLongitude());
                data.setRemarks(inputJson.getRemarks());
                data.setRemarksDate(inputJson.getRemarksDate());
                data.setTransactionID(entity.getTransactionId());
                data.setUserID(inputJson.getUserId());
                try {
                    this.logger.info((Object)("Spoton URL Called " + urlString));
                    this.logger.info((Object)("DataSent to spoton " + gson.toJson((Object)data)));
                    obj = new URL(urlString);
                    con = (HttpURLConnection)obj.openConnection();
                    con.setConnectTimeout(5000);
                    con.setReadTimeout(60000);
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    OutputStreamWriter streamWriter = new OutputStreamWriter(con.getOutputStream());
                    streamWriter.write(gson.toJson((Object)data));
                    streamWriter.flush();
                    this.logger.info((Object)("Response Code : " + con.getResponseCode()));
                    if (con.getResponseCode() == 200) {
                        stringBuilder = new StringBuilder();
                        InputStreamReader streamReader = new InputStreamReader(con.getInputStream());
                        BufferedReader bufferedReader = new BufferedReader(streamReader);
                        String response1 = null;
                        while ((response1 = bufferedReader.readLine()) != null) {
                            stringBuilder.append(String.valueOf(response1) + "\n");
                        }
                        bufferedReader.close();
                        this.logger.info((Object)("Response From Spoton : " + stringBuilder.toString()));
                        OdaConRemarksErpResponseModal erpResponse = (OdaConRemarksErpResponseModal)gson.fromJson(stringBuilder.toString(), OdaConRemarksErpResponseModal.class);
                        OdaConRemark record = (OdaConRemark)mt.find(OdaConRemark.class, (Object)erpResponse.getTransactionID());
                        record.setTransactionResult(erpResponse.getTransationResult() ? 1 : 0);
                        record.setTransactionMessage(erpResponse.getTransationMessage());
                        record.setErpResponseTimestamp(new Date());
                        mt.persist((Object)record);
                        mt.commit();
                        if (erpResponse.getTransationResult()) {
                            result = gson.toJson((Object)new GeneralResponse(Constants.TRUE, Constants.REQUEST_COMPLETED, null));
                            resultMessage = "Request Completed";
                        } else {
                            result = gson.toJson((Object)new GeneralResponse(Constants.FALSE, erpResponse.getTransationMessage(), null));
                            resultMessage = erpResponse.getTransationMessage();
                        }
                        break block8;
                    }
                    this.logger.info(("Response failed from spoton with response code " + con.getResponseCode()));
                    result = gson.toJson((Object)new GeneralResponse(Constants.FALSE, "Response Failed From ERP", null));
                    resultMessage = "Response Failed From ERP";
                }
                catch (Exception e) {
                    e.printStackTrace();
                    resultMessage = "Exception in server";
                    result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));
                    this.logger.error((Object)"Exception in server ", (Throwable)e);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
                resultMessage = "Exception in server";
                result = gson.toJson(new GeneralResponse(Constants.FALSE, Constants.ERRORS_EXCEPTION_IN_SERVER, null));                this.logger.error((Object)"Exception in server ", (Throwable)e);
            }finally {
            	if(mt!=null) {
            		mt.close();
            	}
            }
        }
        boolean auditlogStatus = CommonTasks.saveAuditLog((String)deviceIMEI, (String)apkVersion, (String)"ODA CON REMARK UPDATED", conMap.values(), (String)userId, (String)pdc, (String)ipadd, (String)resultMessage, (String)null);
        this.logger.info((Object)("auditlogStatus " + auditlogStatus));
        this.logger.info((Object)("Result " + result));
        this.logger.info((Object)"**************END*******************");
        response.getWriter().write(result);
    }
}